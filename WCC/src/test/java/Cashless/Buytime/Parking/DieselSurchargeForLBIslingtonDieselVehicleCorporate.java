package Cashless.Buytime.Parking;

import static GenericComponents.constants.Symbols.POUND;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;
import static Utilities.StringUtils.getPriceFromLabel;
import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;

public class DieselSurchargeForLBIslingtonDieselVehicleCorporate extends RingGoScenario {
    private static final String VEHICLE_TYPE = "Car";

    private String carVrn;

	@BeforeMethod
    public void setUp() throws Exception {

    	createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;

        RestApiHelper.setCarType(email, password, "Car");
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void DieselSurchargeForLBIslingtonDieselVehicleCorporate(String TCID, Map<String, String> data) {
        StartTest(TCID, "Diesel Surcharge only for LB of Islington - Diesel Vehicle - pay from corporate");

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
                carVrn, data.get("yearOfManufacture"),
                data.get("co2"), data.get("engineSize"),
                VEHICLE_TYPE, data.get("fuelType"), _driver);

        CorporateHelper.addUserToCorporate(data.get("CorporateLogin"), data.get("CorporatePassword"), carVrn, mobile, _driver);

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);

        NavigationHelper.openMyRingo(_driver);
        LogStep("Step 2", "Click 'Login'");
        CheckResult(ClickControl(homePage.getUpperRightNavigationMenu().getLoginLink(), "Login link"), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link");

        LogStep("Step 3", "Input 'Mobile number or E-mail' & password and click 'Log In'");
        CheckResult(enterText(loginRegisterPage.getMobileOrEmailInput(), email, "Mobile/Email input"), "Input 'Mobile number or E-mail");
        CheckResult(enterText(loginRegisterPage.getPasswordOrPinInput(), password, "Password input"), "Input 'Password'");
        CheckResult(ClickControl(loginRegisterPage.getLogInButton(), "Login button"), "Click 'Log In'");
        delay(10000);
        LogStep("Step 4", "Click 'Park' menu in the top of the home page");
        CheckResult(ClickControl(homePage.getMyRingGoMainMenuNavigation().getParkMenuLink(), "'Park' menu"), "Click 'Park' menu");

        LogStep("Step 5", "Enter '61175' in 'Which location do you want to park in?' input field " +
                "and select the diesel vehicle (Corporate Linked) from the 'Which vehicle do you want to park? ' drop down list");
        BookParkingPage bookParkingPage = new BookParkingPage(_driver);
        CheckResult(enterText(bookParkingPage.getBookBlock().getZoneInput(), data.get("zone"), "Zone input"),
                "Enter zone in 'Which location do you want to park in?' input field");

        LogStep("Step 6", "And click 'Next' button");
        CheckResult(ClickControl(bookParkingPage.getBookBlock().getNextButton(), "Next button"), "Click Next button");

        LogStep("Step 7", "Select 'Now' button in the 'When do you want to park?' page and click 'Next' button");
        CheckResult(ClickControl(bookParkingPage.getBookBlock().getBookNowRadioButton(), "Now radio button"), "Select 'Now' button");
        CheckResult(ClickControl(bookParkingPage.getBookBlock().getNextButton(), "Next button"), "Click 'Next' button");

        LogStep("Step 8", "Select '1 Hour' and click 'Next' button");
        CheckResult(selectItemFromDropdownByTextValue(bookParkingPage.getDurationSessionBlock().getDurationTimeDropdown(), data.get("sessionDuration"), "Duration dropdown"),
                "Select '1 Hour' from dropdown");
        CheckResult(ClickControl(bookParkingPage.getDurationSessionBlock().getNextButton(), "Next button"), "Click 'Next' button");

        LogStep("Step 9", "Select 'Yes' option and click 'Next'");
        CheckResult(ClickControl(bookParkingPage.getPayFromCorporateBlock().getYesRadioButton(), "Yes radiobutton"), "Click Yes");
        CheckResult(ClickControl(bookParkingPage.getPayFromCorporateBlock().getNextButton(), "Next button"), "Click Next button");

        String price = getPriceFromLabel(bookParkingPage.getPayCorporateConfirmBlock().getCostLabel().getText());
        CheckResult(ClickControl(bookParkingPage.getPayCorporateConfirmBlock().getLinkToPopUp(), "Fuel type price adjustment"), "Click on 'Fuel type price adjustment'");
        
        // TODO Need to make the surcharge dynamic - perhaps use insight to read the operator settings \ CO2 Charging for comparison below
        CheckBool(bookParkingPage.getPayCorporateConfirmBlock().getPopUpWindow().getText()
                .contains("A " + POUND.getAsUTF8() + "3.00 Fuel type surcharge applies in this zone"), "Text in pop up exists and surcharge matches " + POUND.getAsUTF8() + "3.00 (harcdoded)");

        LogStep("Step 10", "Click 'Next' button");
        CheckResult(ClickControl(bookParkingPage.getPayCorporateConfirmBlock().getNextButton(), "Next button"), "Click on Next button");
        CheckResult(ClickControl(bookParkingPage.getFinishBlock().getFinishButton(), "Finish button"), "Click on ''Finish button");
        CheckBool(homePage.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.DESCRIPTION).contains("61175"), "Zone number");
        CheckBool(homePage.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.AMOUNT).contains(price), "Price value");
    }
}
