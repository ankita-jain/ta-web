package Cashless.Buytime.Parking;

import static Utilities.TimerUtils.delay;
import static java.lang.String.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCHelpCentreChangeVehiclePage;
import PageObjects.WCCHome;
import PageObjects.WCCMyVehiclesPage;
import PageObjects.WCCStatementsPage;

public class WCCVerifyChangingVehicleOnSessionTest extends RingGoScenario {
    private static final String NEW_CAR_VRN = randomAlphabetic(6);

    private static final String ERROR_MESSAGE = "Failed to change vehicle. Sorry, you've missed your opportunity to change the vehicle on this session - you have 15 minutes from when the session starts.";
    private String carVrn;
    private String vrnType;
    private String vrnManufacture;
    private String colour;
    private String zoneNumber;
    private String zoneId;
    private String hours;
    private String minutes;

	@BeforeMethod()
    public void setUp() throws Exception {
    	
    	createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void verifyChangingVehicleOnSessionTest(String TCID, Map<Object, Object> data) {
        StartTest(TCID, "Verify editing an existing session through desktop site");
        
        vrnType = data.get("vrn_type").toString();
        vrnManufacture = data.get("vrn_manufacture").toString();
        colour = data.get("colour").toString();
        zoneNumber = data.get("zone").toString();
        zoneId = data.get("zoneid").toString();
        hours = data.get("hours").toString();
        minutes = data.get("minutes").toString();

        LogStep("1", "Login to WCC");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("1", "Login to WCC");
        String vehicleID = RestApiHelper.getVehicleId(RestApiHelper.getInfoAboutVehicle(email, password));

        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
        String paymentMethod = (String) usersPayment.get("Id");

        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);

        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickVehiclesLink(), "Click on vehicle link");

        WCCMyVehiclesPage wccMyVehiclesPage = new WCCMyVehiclesPage(_driver);
        CheckResult(wccMyVehiclesPage.changeVehicleOnSession(), "Click on change vehicle on session");

        LogStep("2", "Select a session from the dropdown and click on Next");
        WCCHelpCentreChangeVehiclePage wccHelpCentreChangeVehiclePage = new WCCHelpCentreChangeVehiclePage(_driver);
        CheckResult(wccHelpCentreChangeVehiclePage.selectSession(zoneNumber), format("Select session with %s number", zoneNumber));
        CheckResult(wccHelpCentreChangeVehiclePage.clickNext(), "Click next");
        LogStep("3", "Change the VRN to another existing VRN or add a new VRN and click on Next");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().enterVehicleNumber(NEW_CAR_VRN), "Enter new VRN");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().selectVrnType(vrnType), "Select VRN type");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().selectVrnColour(colour), "Select VRN colour");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().selectVrnManufacture(vrnManufacture), "Select  vrn manufacture");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().clickNext(), "Click next");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().clickFinish(), "Click finish");

        LogStep("4", "Go to Statement and Receipts and look for the session");
        CheckResult(wccHelpCentreChangeVehiclePage.getWCCNavbarUnderUpperMenu().clickOnStatementsNav(), "Click on Statement menu");

        delay(900000);

        WCCStatementsPage wccStatementsPage = new WCCStatementsPage(_driver);
        CheckBool(wccStatementsPage.getTableAsElement().getCellValue(1, WCCStatementsPage.MyStatementsTable.VEHICLE)
                .contains("** " + NEW_CAR_VRN.toUpperCase()), "Click on Statement menu");

        LogStep("5", "Go to active session and try changing the vehicle details of already booked session after 15 minutes");
        CheckResult(wccHome.clickVehiclesLink(), "Click on vehicle link");
        CheckResult(wccMyVehiclesPage.changeVehicleOnSession(), "Click on change vehicle on session");
        CheckResult(wccHelpCentreChangeVehiclePage.selectSession(zoneNumber), format("Select session with %s number", zoneNumber));
        CheckResult(wccHelpCentreChangeVehiclePage.clickNext(), "Click next");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().selectVrnFromDropdown(carVrn), "Select car's vrn");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().clickNext(), "Click next");
        
        CheckBool(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().getErrorNotification().getText().
                        contains(ERROR_MESSAGE),
                "Check that user has an error");

    }
}
