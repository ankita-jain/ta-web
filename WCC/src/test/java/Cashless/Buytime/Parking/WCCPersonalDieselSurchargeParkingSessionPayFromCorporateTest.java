package Cashless.Buytime.Parking;

import static GenericComponents.constants.Symbols.POUND;
import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCBookParkingSession;
import PageObjects.WCCCorporateConfirmBookParkingSession;
import PageObjects.WCCCorporatePaymentDetailsBookParkingSession;
import PageObjects.WCCDurationParkingSession;
import PageObjects.WCCFinishBookingSession;
import PageObjects.WCCHome;
import PageObjects.WCCStatementsPage;
import PageObjects.Corporate.WCCCorporateAddEmployeePage;
import PageObjects.Corporate.WCCCorporateAssetsPage;
import PageObjects.Corporate.WCCCorporateEmployeesPage;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporateProfilePage;
import PageObjects.Corporate.WCCSetupPage;
import PageObjects.pagecomplexobjects.WCCNavbarUnderUpperMenu;

public class WCCPersonalDieselSurchargeParkingSessionPayFromCorporateTest extends RingGoScenario {
    private String carVrn;

	@BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");
        
        carVrn = vehicleVrn;
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void payFromCorporate(String TCID, Map<Object, String> data) {
        StartTest(TCID, "WCC Diesel surcharge paying via corporate");

        LogStep("Preconditions", "Must have a diesel vehicle with a DOM of 2014 or before on the account. Account must be linked to a corporate account");
        RestApiHelper.setCarType(email, password, data.get("vehicleType"));

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword, carVrn, data.get("yearOfManufacture"),
                data.get("CO2"), data.get("engineSize"),
                data.get("vehicleType"), data.get("fuelType"), _driver);


        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(corporateLogin, corporatePassword, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Setup");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        CheckResult(wccSetupPage.clickAssets(), "Clicking on Assets");

        WCCCorporateAssetsPage wccCorporateAssetsPage = new WCCCorporateAssetsPage(_driver);
        CheckResult(wccCorporateAssetsPage.enterCli(mobile), String.format("Enter cli -> %s", mobile));
        CheckResult(wccCorporateAssetsPage.addCli(), "Click add Cli");
        CheckResult(wccCorporateAssetsPage.enterVRN(carVrn), String.format("Enter vrn -> %s", carVrn));
        CheckResult(wccCorporateAssetsPage.addVrn(), "Click add vrn");
        CheckResult(wccCorporateAssetsPage.save(), "Click save");
        
        delay(2000);
        CheckResult(wccCorporateAssetsPage.clickSetup(), "Click Setup");

        CheckResult(wccSetupPage.clickEmployees(), "Clicking on Employees");

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        CheckResult(wccCorporateEmployeesPage.addNewEmployee(), "Click add new Employee");

        String name = RandomStringUtils.randomAlphabetic(6);
        String surname = RandomStringUtils.randomAlphabetic(6);
        WCCCorporateAddEmployeePage wccCorporateAddEmployeePage = new WCCCorporateAddEmployeePage(_driver);
        CheckResult(wccCorporateAddEmployeePage.enterFirstName(name), String.format("Enter first name -> %s", name));
        CheckResult(wccCorporateAddEmployeePage.enterSurname(surname), String.format("Enter second name -> %s", surname));
        CheckResult(wccCorporateAddEmployeePage.enterEmail(email), String.format("Enter email -> %s", email));
        CheckResult(wccCorporateAddEmployeePage.tickWelcomeEmail(), "Tick Welcome email");
        CheckResult(wccCorporateAddEmployeePage.enterPhoneNumber(mobile), String.format("Enter cli -> %s", mobile));
        CheckResult(wccCorporateAddEmployeePage.addCli(), "Click Add cli");
        CheckResult(wccCorporateAddEmployeePage.enterVehicle(carVrn), String.format("Enter vrn -> %s", carVrn));
        CheckResult(wccCorporateAddEmployeePage.addVrn(), "Click add vrn");
        CheckResult(wccCorporateAddEmployeePage.clickSave(), "Click save");

        LogStep("Step - 1", "Go to 'Pay To Park' and create a new session");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);
        
        delay(10000);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        LogStep("Step - 2", "Enter the location code 8142. Select your diesel vehicle with a DOM of 2014 or before. Enter any duration and select next");
        WCCBookParkingSession wccBookParkingSession = new WCCBookParkingSession(_driver);
        CheckResult(wccBookParkingSession.enterZone(data.get("zone")), "Enter zone number");
        CheckResult(wccBookParkingSession.selectVehicle(carVrn), "Select vehicle");
        CheckResult(wccBookParkingSession.next(), "Click next button");

        WCCDurationParkingSession wccDurationParkingSession = new WCCDurationParkingSession(_driver);
        CheckResult(wccDurationParkingSession.selectDuration(data.get("time")), "Select time to park");
        CheckResult(wccDurationParkingSession.tickSMSConfirm(), "Select SMS confirm");
        CheckResult(wccDurationParkingSession.tickReminderSMS(), "Select reminder SMS");
        CheckResult(wccDurationParkingSession.next(), "Click next button");

        LogStep("Step - 3", "Select 'yes' and click next");
        WCCCorporatePaymentDetailsBookParkingSession wccCorporatePaymentDetailsBookParkingSession = new WCCCorporatePaymentDetailsBookParkingSession(_driver);
        CheckResult(wccCorporatePaymentDetailsBookParkingSession.clickYes(), "Click Yes");
        CheckResult(wccCorporatePaymentDetailsBookParkingSession.next(), "Click next");

        LogStep("Step - 4", "Check the 'cost' section");
        WCCCorporateConfirmBookParkingSession wccCorporateConfirmBookParkingSession = new WCCCorporateConfirmBookParkingSession(_driver);
        String price = wccCorporateConfirmBookParkingSession.getPrice();

        LogStep("Step - 5", "Click 'Diesel surcharge'");
        CheckResult(wccCorporateConfirmBookParkingSession.clickFuelTypeSurchargeLink(), "Click diesel surcharge link");
        CheckContains(wccCorporateConfirmBookParkingSession.getFuelTypeSurchargeTooltipText(),
                "A 50% (" + POUND.getAsUTF8() + "1.85) Fuel type surcharge applies in this zone", "Check Diesel surcharge message");
        CheckResult(wccCorporateConfirmBookParkingSession.next(), "Click select next button");

        LogStep("Step - 6", "Complete the session");
        WCCFinishBookingSession wccFinishBookingSession = new WCCFinishBookingSession(_driver);
        CheckResult(wccFinishBookingSession.finish(), "Click finish button");

        WCCNavbarUnderUpperMenu wccNavbarUnderUpperMenu = new WCCNavbarUnderUpperMenu(_driver);
        CheckResult(wccNavbarUnderUpperMenu.clickOnStatementsNav(), "Click Statements link");

        WCCStatementsPage wccStatementsPage = new WCCStatementsPage(_driver);
        CheckContains(wccStatementsPage.getAmount(1), price, "Check price");
    }
}
