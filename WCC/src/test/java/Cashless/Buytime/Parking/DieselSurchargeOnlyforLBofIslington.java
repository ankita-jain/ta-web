package Cashless.Buytime.Parking;

import static GenericComponents.constants.Symbols.POUND;
import static Utilities.StringUtils.getPriceFromLabel;
import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;

public class DieselSurchargeOnlyforLBofIslington extends RingGoScenario {
    private static final String VEHICLE_TYPE = "Car";
    private String carVrn;

	@BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;

        RestApiHelper.setCarType(email, password, VEHICLE_TYPE);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void dieselSurchargeOnlyforLBofIslington(String TCID, Map<String, String> data) {
        StartTest(TCID, "Diesel Surcharge only for LB of Islington - 1 hour session");

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword, carVrn, data.get("yearOfManufacture"), data.get("co2"), data.get("engineSize"), VEHICLE_TYPE, data.get("fuelType"), _driver);

        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);
        BookParkingPage bookParkingPage = new BookParkingPage(_driver);

        LogStep("Step - 1", "Go to MyRingGo");
        NavigationHelper.openMyRingo(_driver);

        LogStep("Step - 2", "Click 'Login'");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link");
        CheckBool(loginRegisterPage.getCurrentTitle().equals("RingGo Cashless Parking Solution"), "Login or Register with RingGo page should be loaded");

        LogStep("Step - 3", "Input 'Mobile number or E-mail' & password and click 'Log In'");
        CheckResult(loginRegisterPage.enterMobileOrEmail(email), "Input 'Mobile number or E-mail' ");
        CheckResult(loginRegisterPage.enterPassword(password), "Input 'Password'");
        CheckResult(loginRegisterPage.clickLogin(), "Click 'Log in' button");

        CheckBool(homePageMyRinggo.getCurrentTitle().equals("RingGo Cashless Parking Solution: Dashboard"), "RingGo user 'home' page should be loaded");

        LogStep("Step - 4", "Click 'Park' menu in the top of the home page");
        CheckResult(homePageMyRinggo.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu");
        CheckBool(bookParkingPage.getCurrentTitle().equals("RingGo Cashless Parking Solution: Book a Parking Session"), "User should be taken to 'Book a Parking Session' page");

        LogStep("Step - 5", String.format("Enter '%s' in 'Which location do you want to park in?' input field and select the diesel vehicle from the 'Which vehicle do you want to park? ' drop down list.", data.get("zone")));
        CheckResult(bookParkingPage.getBookBlock().enterZone(data.get("zone")), "Enter text on 'Zone' input");
        CheckResult(bookParkingPage.getBookBlock().selectVRN(carVrn), "Select vehicle");

        LogStep("Step - 6", " And click 'Next' button ");
        CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");
        CheckBool(bookParkingPage.getBookBlock().getWhendDoYouParkText().getText().equals("When do you want to park?"),
                "User should be able to enter location and vehicle successfully.");

        LogStep("Step - 7", "Select 'Now' button in the 'When do you want to park?' and click 'Next' button");
        CheckResult(bookParkingPage.getBookBlock().selectNowRadioButton(), "Click on 'Now' radio button");
        CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");
        CheckBool(bookParkingPage.getDurationSessionBlock().getHowLongWantParkLabel().getText().equals("*How long do you want to park for?\n "), "User should be taken to 'How long do you want to park for?' page");

        LogStep("Step - 8", String.format("Select '%s' and click 'Next' button", data.get("sessionDuration")));
        CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(data.get("sessionDuration")), "Select duration");
        CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button");
        CheckBool(bookParkingPage.getPayBlock().getLinkToPopUp().getText().equals("Fuel type price adjustment"), "Fuel type price adjustment link exists");

        String price = getPriceFromLabel(bookParkingPage.getPayBlock().getPriceLabel().getText());
        CheckResult(bookParkingPage.getPayBlock().clickDieselSurcharge(), "Click on 'Fuel type price adjustment'");
        
        // TODO Need to make the surcharge dynamic - perhaps use insight to read the operator settings \ CO2 Charging for comparison below
        CheckBool(bookParkingPage.getPayBlock().getPopUpWindow().getText().contains("A " + POUND.getAsUTF8() + "3.00 Fuel type surcharge applies in this zone"), "Text in pop up exists and surcharge matches " + POUND.getAsUTF8() + "3.00  (hardcoded)");

        CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button");

        LogStep("Step - 9", "And enter the card details & pay for the parking session");
        CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button");
        CheckResult(bookParkingPage.getPayBlock().enterCvvCode(data.get("CVV2_Code")), "Enter CVV code");
        CheckResult(bookParkingPage.getPayBlock().clickPayConfirmation(), "'Click on 'Pay Confirmation' button");

        delay(3000);
        CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button");

        CheckBool(homePageMyRinggo.getActiveSessionsBlock().getTable().getRows().size() == 1, "There is only one active session which has been successfully created");

        CheckBool(homePageMyRinggo.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.DESCRIPTION).contains(data.get("zone")), "Zone number");
        CheckBool(homePageMyRinggo.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.AMOUNT).contains(price), "Price value");
    }
}
