package Cashless.Buytime.Parking;

import java.util.Map;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.WalletsPage;
import PageObjects.myringo.HomePageMyRinggo;

public class RingGoBuySessionWithAWallet extends RingGoScenario {

	public String userid,vehicleVrn;
	
	@BeforeMethod
	public void setUp() throws Exception {
		
        createUser("defaultNewUser.json");
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(mobile, password, RestApiHelper.defaultNewVehicle());
        userid = (String) newUserJson.get("UserID");
        Map<Object, Object> securityQuestion = RestApiHelper.userSecurityQuestion(userid);
        RestApiHelper.changeUsersecurityQuestion(mobile,password,securityQuestion);
	}
	
	@Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	public void createWalletAndBuySession(String TCID, Map<Object, Object> data) throws Exception {
		
		StartTest(TCID, data);

		String zone = data.get("zone").toString();
		String duration = data.get("duration").toString();
		String wallet = data.get("wallet").toString();
		String type =  data.get("type").toString();
		String session = data.get("session").toString();
		String walletText = data.get("walletText").toString();
		
		LogStep("Step - 1", "Open RingGoWeb");
	    NavigationHelper.openMyRingo(_driver);
	    	
	    LogStep("Step - 2", "Enter login and password");
	    LoginHelper.loginMyRingo(email, password, _driver);
	    	
	    LogStep("Step - 3", "Click on Wallets link");
	    HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
	    CheckResult(homePageMyRinggo.clickWalletsLink(), "Click Wallets");
	    
	    LogStep("Step - 4", "Click on Create a new Wallet");
    	WalletsPage walletsPage = new WalletsPage(_driver);
    	CheckResult(walletsPage.clickCreateWallet(),"Click create option");
	    
    	LogStep("Step - 5", "Enter details for a new Wallet and click Next");
    	walletsPage.selectWalletOperator(wallet);
    	walletsPage.selectWalletType(type);
    	walletsPage.selectWalletSessionType(session);
    	CheckResult(walletsPage.clickNext(),"Click Next button");
    	
    	LogStep("Step - 6", "Click to Confirm a new Wallet is created");
    	CheckResult(walletsPage.clickCreate(),"Click Create button");
    	
    	LogStep("Step - 7", "Click on Finish to return to the Wallet Management page");
    	CheckResult(walletsPage.clickFinish(),"Click Finish button");
    	
    	LogStep("Step - 8", "Click on Top up a Wallet link");
    	CheckResult(walletsPage.clickTopUpWallet(),"Click top up option");
    	
    	LogStep("Step - 9", "Click Next to go to the Card Select screen");
    	CheckResult(walletsPage.clickNext(),"Click Next button");
    	
    	LogStep("Step - 10", "Click Next again to go to the payment screen");
    	CheckResult(walletsPage.clickCardSelectNext(),"Accept the defaulted card");
    	
    	LogStep("Step - 11", "Accept the defaulted payment amount");
    	CheckResult(walletsPage.clickPayButton(),"Click Pay button");
    	
    	LogStep("Step - 12", "Click on Finish to return to the Wallet Management page");
    	CheckResult(walletsPage.clickFinish(),"Click Finish button");
    	
    	LogStep("Step - 13", "Click 'Park' menu in the top of the home page");
	    CheckResult(homePageMyRinggo.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu and book parking");
	    
	    BookParkingPage bookParkingPage = new BookParkingPage(_driver);
	    CheckBool(bookParkingPage.getCurrentTitle().equals("RingGo Cashless Parking Solution: Book a Parking Session"), "User should be taken to 'Book a Parking Session' page");
	    CheckResult(bookParkingPage.getBookBlock().enterZone(zone), "Enter text on 'Zone' input");
	    // accept the default vehicle - no need to select one
	    CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");
	     
	    LogStep("Step - 14", "Enter the duration and click Next button");
	    CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(duration), "Select duration");
	    CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button");
	    
	    LogStep("Step - 15", "Select the defaulted Wallet payment and click Next");
	    CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button");
	    
	    LogStep("Step -16", "Check the pay by wallet message appears");
	    CheckContains(bookParkingPage.getPayBlock().getWalletText(),walletText,"WalletText");
	    
	    LogStep("Step - 17", "Click Pay button to pay with the Wallet then Finish");
	    CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button");
	
	    CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button");
	}

}
