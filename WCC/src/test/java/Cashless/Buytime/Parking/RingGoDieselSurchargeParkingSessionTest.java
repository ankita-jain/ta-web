package Cashless.Buytime.Parking;

import static GenericComponents.constants.Symbols.POUND;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.myringo.mobile.RingoMobileBookParkingSession;
import PageObjects.myringo.mobile.RingoMobileCardCVVBookingSession;
import PageObjects.myringo.mobile.RingoMobileDurationParkingSession;
import PageObjects.myringo.mobile.RingoMobileFinishBookingSession;
import PageObjects.myringo.mobile.RingoMobilePayParkingSession;
import PageObjects.myringo.mobile.RingoMobileStatements;

public class RingGoDieselSurchargeParkingSessionTest extends RingGoScenario {
    private String petrolCarVrn;
    private String cvv;

	@BeforeMethod
    public void setUp() throws Exception {

        cvv = wccproperties.getProperty("CVV2");

        createUser("islingtonNewUser.json");
        petrolCarVrn = vehicleVrn;
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void dieselSurcharge(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "WCC Diesel surcharge - RingGo mobile website");

        RestApiHelper.setCarType(email, password, data.get("vehicleType"));

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
                petrolCarVrn, data.get("yearOfManufacture"),
                data.get("CO2"), data.get("engineSize"),
                data.get("vehicleType"), data.get("fuelType"), _driver);

        LogStep("Step - 1", "Go to my ringo mobile. Log in");
        NavigationHelper.openRingoGoMobile(_driver);
        LoginHelper.loginMyRingGoMobile(email, password, _driver);

        LogStep("Step - 2", "Click on 'Parking' and book a new session");
        RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
        CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");

        LogStep("Step - 3", "Book at location code 8142 and select your diesel vehicle. Choose any duration. Continue to the payment section - check the price");
        RingoMobileBookParkingSession ringoMobileBookParkingSession = new RingoMobileBookParkingSession(_driver);

        CheckResult(ringoMobileBookParkingSession.enterZone(data.get("zone")), "Enter zoneID");
        CheckResult(ringoMobileBookParkingSession.selectVehicle(petrolCarVrn), "Click select vehicle");
        CheckResult(ringoMobileBookParkingSession.next(), "Click next button");

        RingoMobileDurationParkingSession ringoMobileDurationParkingSession = new RingoMobileDurationParkingSession(_driver);

        CheckResult(ringoMobileDurationParkingSession.selectDuration(data.get("time")), "Select duration");
        CheckResult(ringoMobileDurationParkingSession.tickSMSConfirm(), "Select SMS confirm");
        CheckResult(ringoMobileDurationParkingSession.tickReminderSMS(), "Select reminder SMS");
        CheckResult(ringoMobileDurationParkingSession.next(), "Click duration next button");

        RingoMobilePayParkingSession ringoMobilePayParkingSession = new RingoMobilePayParkingSession(_driver);

        String price = ringoMobilePayParkingSession.getPrice();
        LogStep("Step - 4", "Click 'Diesel Surcharge'");
        CheckResult(ringoMobilePayParkingSession.clickFuelTypeSurchargeLink(), "Click diesel surcharge link");

        CheckContains(ringoMobilePayParkingSession.getFuelTypeSurchargeTooltipText(),
                "A 50% (" + POUND.getAsUTF8() + "1.85) Fuel type surcharge applies in this zone", "Check Diesel surcharge message");

        CheckResult(ringoMobilePayParkingSession.next(), "Click select next button");

        LogStep("Step - 5", "Complete the booking");
        RingoMobileCardCVVBookingSession ringoMobileCardCVVBookingSession = new RingoMobileCardCVVBookingSession(_driver);

        CheckResult(ringoMobileCardCVVBookingSession.pay(), "Click pay button");
        CheckResult(ringoMobileCardCVVBookingSession.enterCVV(cvv), "Enter CVV2 code");
        //CheckResult(ringoMobileCardCVVBookingSession.tickTermsAndConditions(), "Click terms and condition");
        CheckResult(ringoMobileCardCVVBookingSession.clickPay(), "Click pay button");

        RingoMobileFinishBookingSession ringoMobileFinishBookingSession = new RingoMobileFinishBookingSession(_driver);
        CheckResult(ringoMobileFinishBookingSession.finish(), "Click finish button");

        CheckResult(ringgomobilehomepage.clickStatements(), "Click statements");

        RingoMobileStatements ringoMobileStatements = new RingoMobileStatements(_driver);
        CheckContains(ringoMobileStatements.getPrice(0), price, "Check price");
    }
}
