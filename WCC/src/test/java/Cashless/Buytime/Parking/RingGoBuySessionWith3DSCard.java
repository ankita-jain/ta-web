package Cashless.Buytime.Parking;
import java.util.Map;

import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;

public class RingGoBuySessionWith3DSCard extends RingGoScenario {

	@Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	public void buySession(String TCID, Map<Object, Object> data) throws Exception {
		
		StartTest(TCID, data);
		
		String email = data.get("email").toString();
		String password = data.get("password").toString();
		String zone = data.get("zone").toString();
		String duration = data.get("duration").toString();
		String cvv = data.get("CVV").toString();
		
		LogStep("Step - 1", "Open RingGoWeb");
	    NavigationHelper.openMyRingo(_driver);
	    	
	    LogStep("Step - 2", "Enter login and password");
	    LoginHelper.loginMyRingo(email, password, _driver);
	    
    	LogStep("Step - 3", "Click 'Park' menu in the top of the home page");
        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
	    CheckResult(homePageMyRinggo.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu and book parking");
	    
	    BookParkingPage bookParkingPage = new BookParkingPage(_driver);
	    CheckBool(bookParkingPage.getCurrentTitle().equals("RingGo Cashless Parking Solution: Book a Parking Session"), "User should be taken to 'Book a Parking Session' page");
	    
	    LogStep("Step - 4", "Select a new session, enter the zone and click 'Next'");
	    CheckResult(bookParkingPage.getOptionsBlock().clickNew(), "Click on the 'New' button");
	    
	    CheckResult(bookParkingPage.getBookBlock().enterZone(zone), "Enter text on 'Zone' input");
	    // accept the default vehicle - no need to select one
	    CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");
	     
	    LogStep("Step - 5", "Enter the duration and click Next button");
	    CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(duration), "Select duration");
	    CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button");
	    
	    LogStep("Step - 6", "Select the 3DS payment card defaulted form this user's account and click Next");
	    CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button");
	     
	    LogStep("Step - 7", "Click Pay button");
	    CheckResult(bookParkingPage.getPayBlock().clickPay(), "Click on 'Pay' button");
	    
	    LogStep("Step - 8", "Enter the CVV code & pay for the parking session");
        CheckResult(bookParkingPage.getPayBlock().enterCvvCode(cvv), "Enter CVV code");
        CheckResult(bookParkingPage.getPayBlock().clickPayConfirmation(), "Click on 'Pay Confirmation' button");
        
        LogStep("Step - 9","Click on the Authenticated button");
        CheckResult(bookParkingPage.clickAuthenticated(), "Click on 'Authenticated' button");
        
        LogStep("Step - 10","Click on the Finish button");
        CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on 'Finish' button");
	}
}
