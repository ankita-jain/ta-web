package Cashless.Buytime.Parking;

import static GenericComponents.constants.Symbols.POUND;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;
import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;

public class DieselSurchargeForLBIslingtonExtendSessionFor50Minutes extends RingGoScenario {
    private static final String VEHICLE_TYPE = "Car";
    private String carVrn;

	@BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;

        RestApiHelper.setCarType(email, password, VEHICLE_TYPE);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void dieselSurchargeForLBIslingtonExtendSessionFor50Minutes(String TCID, Map<String, String> data) {
        StartTest(TCID, "Diesel Surcharge only for LB of Islington - Extend parking session for 50 mins");

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
                carVrn, data.get("yearOfManufacture"),
                data.get("co2"), data.get("engineSize"),
                VEHICLE_TYPE, data.get("fuelType"), _driver);

        CashlessHelper.createSession(email, password, data.get("zone"), data.get("sessionDuration"), data.get("CVV2_Code"), carVrn, _driver);
        delay(240000); //TODO - eliminate these types of waits

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        BookParkingPage bookParkingPage = new BookParkingPage(_driver);

        LogStep("Step 4", "Click 'Park' menu in the top of the home page");
        CheckResult(ClickControl(homePage.getMyRingGoMainMenuNavigation().getParkMenuLink(), "'Park' menu"), "Click 'Park' menu");

        LogStep("Step 5", "Click 'Extend' button on the active session created");
        CheckResult(ClickControl(bookParkingPage.getOptionsBlock().getExtendButton(), "Extend button"),
                "Click 'Extend' button on the active session");

        LogStep("Step 6 ", String.format("Select '%s' and click 'Next' button", data.get("sessionDurationToExtend")));
        CheckResult(selectItemFromDropdownByTextValue(bookParkingPage.getDurationSessionBlock().getDurationTimeDropdown(), data.get("sessionDurationToExtend"), "Duration dropdown"),
                "Select duration");
        CheckResult(ClickControl(bookParkingPage.getDurationSessionBlock().getNextButton(), "Next button on duration block"),
                "Click on 'Next' button");
        CheckBool(bookParkingPage.getPayBlock().getLinkToPopUp().getText().equals("Fuel type price adjustment"),
                "Fuel type price adjustment link exists");

        CheckResult(ClickControl(bookParkingPage.getPayBlock().getLinkToPopUp(), "Fuel type price adjustment"),
                "Click on 'Fuel type price adjustment'");
        
        // TODO Need to make the surcharge dynamic - perhaps use insight to read the operator settings \ CO2 Charging for comparison below
        CheckBool(bookParkingPage.getPayBlock().getPopUpWindow().getText().contains("A " + POUND.getAsUTF8() + "3.00 Fuel type surcharge applies in this zone"),
                "Text in pop up exists and surcharge matches " + POUND.getAsUTF8() + "3.00 (hardcoded)");

        CheckResult(ClickControl(bookParkingPage.getPayBlock().getNextButton(), "Next button on payment block"),
                "Click on 'Next' button");
        
        LogStep("Step 6", "And enter the card details & pay for the parking session");
        CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button");        
        CheckResult(bookParkingPage.getPayBlock().enterCvvCode(data.get("CVV2_Code")), "Enter CVV code");
        CheckResult(bookParkingPage.getPayBlock().clickPayConfirmation(), "'Click on 'Pay Confirmation' button");        
        
        delay(3000);
        CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button");

        CheckBool(homePage.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.DESCRIPTION).contains(data.get("zone")),
                "Zone number");
    }
}
