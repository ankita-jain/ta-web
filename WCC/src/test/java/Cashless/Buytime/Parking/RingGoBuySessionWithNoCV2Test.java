package Cashless.Buytime.Parking;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;

public class RingGoBuySessionWithNoCV2Test extends RingGoScenario {
	
	public String vehicleVrn;
	public String zone = "23856";
	public String duration = "1 Hour";
	
	@BeforeMethod
	public void setUp() throws Exception {
		
		createUser("defaultNewUser.json");		
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(mobile, password, RestApiHelper.defaultNewVehicle());
       
	}
	
	//TODO - Remove all hardcoded test data
	
	@Test
	public void buySessionWithNoCV2() {
		
		 StartTest("134161", "Buy a session with no CV2");

	     LogStep("Step - 1", "Login to RingGo website (https://myrgo-preprod.ctt.co.uk)");
	     NavigationHelper.openMyRingo(_driver);
	     
	     HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
	     CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Click Login");
	     CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link");
	     
	     LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);
	     CheckBool(loginRegisterPage.getCurrentTitle().equals("RingGo Cashless Parking Solution"), "Login or Register with RingGo page should be loaded");
	     CheckResult(loginRegisterPage.enterMobileOrEmail(mobile), "Input 'Mobile number or E-mail' ");
	     CheckResult(loginRegisterPage.enterPassword(password), "Input 'Password'");
	     CheckResult(loginRegisterPage.clickLogin(), "Click 'Log in' button");
	     
	     CheckBool(homePageMyRinggo.getCurrentTitle().equals("RingGo Cashless Parking Solution: Dashboard"), "RingGo user 'home' page should be loaded");

	     LogStep("Step - 2", "Click 'Park' menu in the top of the home page & Create a parking session at zone (23856) using any VRN and click Next");
	     CheckResult(homePageMyRinggo.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu");
	     
	     BookParkingPage bookParkingPage = new BookParkingPage(_driver);
	     CheckBool(bookParkingPage.getCurrentTitle().equals("RingGo Cashless Parking Solution: Book a Parking Session"), "User should be taken to 'Book a Parking Session' page");
	     CheckResult(bookParkingPage.getBookBlock().enterZone(zone), "Enter text on 'Zone' input");
	     CheckResult(bookParkingPage.getBookBlock().selectVRN(vehicleVrn.toUpperCase()), "Select vehicle");
	     
	     CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");
	     
	     LogStep("Step - 3", "Select 1-hour duration and click Next button");
	     CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(duration), "Select duration");
	     CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button");
	     
	     LogStep("Step - 4", "Select the payment card and click Next");
	     CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button");
	     
	     LogStep("Step - 5", "Click Pay button");
	     CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button");
	
	     CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button");
	}

}