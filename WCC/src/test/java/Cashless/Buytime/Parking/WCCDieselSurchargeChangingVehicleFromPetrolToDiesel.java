package Cashless.Buytime.Parking;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCHelpCentreChangeVehiclePage;
import PageObjects.WCCHome;
import PageObjects.WCCMyVehiclesPage;

public class WCCDieselSurchargeChangingVehicleFromPetrolToDiesel extends RingGoScenario {
    private static final String VEHICLE_TYPE = "Car";

    @BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");
        
        RestApiHelper.setCarType(email, password, VEHICLE_TYPE);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void DieselSurchargeChangingVehicleFromPetrolToDisel(String TCID, Map<String, String> data) {
        StartTest(TCID, "WCC Diesel Surcharge - changing vehicle from petrol to diesel");

        String dieselVRM = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
        		dieselVRM, data.get("yearOfManufacture"),
                data.get("co2"), data.get("engineSize"),
                VEHICLE_TYPE, data.get("fuelType"), _driver);

        LogStep("Step - 1", "Login to WCC");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
        String vehicleID = RestApiHelper.getVehicle(0,vehicles).get("VehicleID").toString();
                
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, data.get("zoneID"));
        String paymentMethod = (String) usersPayment.get("Id");

        LogStep("Step - 2", "On the WCC site book a parking session at location number 8142  using a petrol vehicle");
        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID,  data.get("zoneID"), data.get("hours"), data.get("minutes"));

        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickVehiclesLink(), "Click on 'Vehicles' menu");

        WCCMyVehiclesPage wccMyVehiclesPage = new WCCMyVehiclesPage(_driver);
        CheckResult(wccMyVehiclesPage.changeVehicleOnSession(), "Click on Change Vehicle on Session/Permit button");

        LogStep("Step - 3", "Select the active session at 8142 and click next");
        WCCHelpCentreChangeVehiclePage wccHelpCentreChangeVehiclePage = new WCCHelpCentreChangeVehiclePage(_driver);
        CheckResult(wccHelpCentreChangeVehiclePage.selectSession( data.get("zone")), String.format("Select session -> %s from drop-down",  data.get("zone")));
        CheckResult(wccHelpCentreChangeVehiclePage.clickNext(), "Click Next button");

        LogStep("Step - 4", "Select your diesel from the drop-down menu and click next");
        CheckResult(wccHelpCentreChangeVehiclePage.getChoosingVehicleStage().selectVrnFromDropdown(dieselVRM.toUpperCase()), String.format("Select Vehicle -> %s from drop-down", dieselVRM));
        CheckResult(wccHelpCentreChangeVehiclePage.clickNext(), "Click Next button");

        CheckValue(wccHelpCentreChangeVehiclePage.getErrorMessageText(), "Failed to change vehicle. Sorry, vehicle not available due to location or vehicle restrictions.", "Check error message");
    }
}
