package Cashless.Buytime.Parking;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.ChangeVehiclesOnYourPermitSessionPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingGoVehiclesPage;

public class DieselSurchargeChangingVehicleFromDieselToPetrol extends RingGoScenario {
    private static final String VEHICLE_TYPE = "Car";
    private String petrolCarVrn;

    @BeforeMethod
    public void setUp() throws Exception {
        
        createUser("defaultNewUser.json");       
            
        petrolCarVrn = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());

        RestApiHelper.setCarType(email, password, VEHICLE_TYPE);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void DieselSurchargeChangingVehicleFromDieselToPetrol(String TCID, Map<String, String> data) {
        StartTest(TCID, "Diesel Surcharge only for LB of Islington - changing vehicle from diesel to petrol");

        String newVRM = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
        		newVRM, data.get("yearOfManufacture"),
                data.get("co2"), data.get("engineSize"),
                VEHICLE_TYPE, data.get("fuelType"), _driver);

        LogStep("Step - 1", "Login to RingGo'");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(email, password, _driver);

        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();

        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, data.get("zoneId"));
        String paymentMethod = (String) usersPayment.get("Id");

        LogStep("Step - 2", "On the RingGo PreProd site book a parking session at location number 61175 using a diesel vehicle");
        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, data.get("zoneId"), data.get("hours"), data.get("minutes"));

        LogStep("Step - 3", "Go to 'Account' then 'Vehicles' then 'Change Vehicle on Session/Permit");
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePage.getUpperRightNavigationMenu().getAccountToggleMenu().clickOnVehiclesLink(), "Click on 'Vehicles' menu");

        MyRingGoVehiclesPage myVehiclePage = new MyRingGoVehiclesPage(_driver);
        CheckResult(myVehiclePage.clickChangeVehicleOnSessionPermit(), "Click on Change Vehicle on Session // Permit button");

        LogStep("Step - 4", "Select the active session at 61175 and click next");
        ChangeVehiclesOnYourPermitSessionPage changeVehicles = new ChangeVehiclesOnYourPermitSessionPage(_driver);
        CheckResult(changeVehicles.selectSessionFromDropdown(data.get("zone")), "Select " + data.get("zone") + " from dropdown");
        CheckResult(changeVehicles.clickNextButton(), "Click Next button");

        LogStep("Step - 5", "Select your petrol vehicle from the drop-down menu and click next");
        CheckResult(changeVehicles.selectVehicleFromDropdown(petrolCarVrn.toUpperCase()), "Select vehicle " + petrolCarVrn + " from dropdown");
        CheckResult(changeVehicles.clickNextButton(), "Click Next button");

        CheckContains(changeVehicles.getTextFromPage(), "Your vehicle(s) associated with your session have been successfully updated.",
                "Check message text");
    }
}
