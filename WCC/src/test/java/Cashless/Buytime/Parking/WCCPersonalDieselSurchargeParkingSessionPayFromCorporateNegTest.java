package Cashless.Buytime.Parking;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Corporate.*;
import PageObjects.*;
import PageObjects.pagecomplexobjects.WCCNavbarUnderUpperMenu;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Map;

import static GenericComponents.constants.Symbols.POUND;
import static Utilities.TimerUtils.delay;

public class WCCPersonalDieselSurchargeParkingSessionPayFromCorporateNegTest extends RingGoScenario {
    private String carVrn;

	@BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void payFromCorporate(String TCID, Map<Object, String> data) {
        StartTest(TCID, "WCC Diesel surcharge paying via corporate - Negative");

        LogStep("Preconditions", "Must have a diesel vehicle with a DOM of 2015 or sooner or a petrol vehicle on the account. Account must be linked to a corporate account");
        RestApiHelper.setCarType(email, password, data.get("vehicleType"));

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(corporateLogin, corporatePassword, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Setup");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        CheckResult(wccSetupPage.clickAssets(), "Clicking on Assets");

        WCCCorporateAssetsPage wccCorporateAssetsPage = new WCCCorporateAssetsPage(_driver);
        CheckResult(wccCorporateAssetsPage.enterCli(mobile), String.format("Enter cli -> %s", mobile));
        CheckResult(wccCorporateAssetsPage.addCli(), "Click add Cli");
        CheckResult(wccCorporateAssetsPage.enterVRN(carVrn), String.format("Enter vrn -> %s", carVrn));
        CheckResult(wccCorporateAssetsPage.addVrn(), "Click add vrn");
        CheckResult(wccCorporateAssetsPage.save(), "Click save");
        
        delay(2000);
        CheckResult(wccCorporateAssetsPage.clickSetup(), "Click Setup");

        CheckResult(wccSetupPage.clickEmployees(), "Clicking on Employees");

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        CheckResult(wccCorporateEmployeesPage.addNewEmployee(), "Click add new Employee");

        String name = RandomStringUtils.randomAlphabetic(6);
        String surname = RandomStringUtils.randomAlphabetic(6);
        WCCCorporateAddEmployeePage wccCorporateAddEmployeePage = new WCCCorporateAddEmployeePage(_driver);
        CheckResult(wccCorporateAddEmployeePage.enterFirstName(name), String.format("Enter first name -> %s", name));
        CheckResult(wccCorporateAddEmployeePage.enterSurname(surname), String.format("Enter second name -> %s", surname));
        CheckResult(wccCorporateAddEmployeePage.enterEmail(email), String.format("Enter email -> %s", email));
        CheckResult(wccCorporateAddEmployeePage.tickWelcomeEmail(), "Tick Welcome email");
        CheckResult(wccCorporateAddEmployeePage.enterPhoneNumber(mobile), String.format("Enter cli -> %s", mobile));
        CheckResult(wccCorporateAddEmployeePage.addCli(), "Click Add cli");
        CheckResult(wccCorporateAddEmployeePage.enterVehicle(carVrn), String.format("Enter vrn -> %s", carVrn));
        CheckResult(wccCorporateAddEmployeePage.addVrn(), "Click add vrn");
        CheckResult(wccCorporateAddEmployeePage.clickSave(), "Click save");

        LogStep("Step - 1", "Go to 'Pay To Park' and create a new session");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);
        delay(10000);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        LogStep("Step - 2", "Book at location code 8142. Select a petrol vehicle or diesel with a DOM of 2015 or sooner. Select any duration and click next");
        WCCBookParkingSession wccBookParkingSession = new WCCBookParkingSession(_driver);
        CheckResult(wccBookParkingSession.enterZone(data.get("zone")), "Enter zone number");
        CheckResult(wccBookParkingSession.selectVehicle(carVrn), "Select vehicle");
        CheckResult(wccBookParkingSession.next(), "Click next button");

        WCCDurationParkingSession wccDurationParkingSession = new WCCDurationParkingSession(_driver);
        CheckResult(wccDurationParkingSession.selectDuration(data.get("time")), "Select time to park");
        CheckResult(wccDurationParkingSession.tickSMSConfirm(), "Select SMS confirm");
        CheckResult(wccDurationParkingSession.tickReminderSMS(), "Select reminder SMS");
        CheckResult(wccDurationParkingSession.next(), "Click next button");

        LogStep("Step - 3", "Select 'yes' and click next");
        WCCCorporatePaymentDetailsBookParkingSession wccCorporatePaymentDetailsBookParkingSession = new WCCCorporatePaymentDetailsBookParkingSession(_driver);
        CheckResult(wccCorporatePaymentDetailsBookParkingSession.clickYes(), "Click Yes");
        CheckResult(wccCorporatePaymentDetailsBookParkingSession.next(), "Click next");

        LogStep("Step - 4", "Check the 'cost' section");
        WCCCorporateConfirmBookParkingSession wccCorporateConfirmBookParkingSession = new WCCCorporateConfirmBookParkingSession(_driver);
        String priceSectionText = wccCorporateConfirmBookParkingSession.getPriceSection().getText();
        String price = wccCorporateConfirmBookParkingSession.getPrice();

        CheckBool(priceSectionText.matches(String.format("^Cost: %s\\d+.\\d+$", POUND.getAsUTF8())), "Check price");
        CheckResult(wccCorporateConfirmBookParkingSession.next(), "Click select next button");

        LogStep("Step - 5", "Complete the session");
        WCCFinishBookingSession wccFinishBookingSession = new WCCFinishBookingSession(_driver);
        CheckResult(wccFinishBookingSession.finish(), "Click finish button");

        WCCNavbarUnderUpperMenu wccNavbarUnderUpperMenu = new WCCNavbarUnderUpperMenu(_driver);
        CheckResult(wccNavbarUnderUpperMenu.clickOnStatementsNav(), "Click Statements link");

        WCCStatementsPage wccStatementsPage = new WCCStatementsPage(_driver);
        CheckContains(wccStatementsPage.getAmount(1), price, "Check price");
    }
}
