package Cashless.Buytime.Parking;

import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCParkingSessionPage;

public class WCCVerifyBookingExtendingSessionThroughDesktop extends RingGoScenario {
    private static final String ERROR_MESSAGE = "There is an active session in progress that was purchased very recently.";

    private String carVrn;
    private String last4CardNumber;
    private String cvvCode;
    private String timeToPark;
    private String zoneID;

	@BeforeMethod()
    public void setUp() throws Exception {

    	createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void wccVerifyBookingExtendingSessionThroughDesktop(String TCID, Map<Object, Object> data) {
        StartTest(TCID, "Verify Booking/Extending of the session through desktop site");
        
        last4CardNumber =  data.get("last_4_digits").toString();;
        cvvCode =  data.get("cvv").toString();;
        timeToPark = data.get("time_to_park").toString();;
        zoneID = data.get("zone").toString();;

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("1", "Go to Pay-to-Park");
        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        WCCParkingSessionPage wccparkingsessionpage = new WCCParkingSessionPage(_driver);

        LogStep("2", "Provide the Location(Postcode) and select/add a vehicle and click on Next");
        CheckResult(wccparkingsessionpage.enterZone(zoneID), "Enter zone number");
        CheckResult(wccparkingsessionpage.selectVehicle(carVrn), "Select vehicle");
        CheckResult(wccparkingsessionpage.clickNextButton(), "Click next button");
        LogStep("3", "Choose the duration from the dropdown and click on next");
        CheckResult(wccparkingsessionpage.selectDuration(timeToPark), "Select time to park");
        CheckResult(wccparkingsessionpage.selectSMSConfirm(), "Select SMS confirm");
        CheckResult(wccparkingsessionpage.selectReminderSMS(), "Select reminder SMS");
        CheckResult(wccparkingsessionpage.clickDurationNextButton(), "Click next button");
        LogStep("4", "Select the payment method and click on next");
        wccparkingsessionpage.clickPaymentNowTab();
        CheckResult(wccparkingsessionpage.clickBasketNextButton(), "Click next step button");
        LogStep("5", "Provide/select the card details, provide security code and click on Pay");
        CheckResult(wccparkingsessionpage.clickCardSelectNextButton(), "Click select next button");
        CheckResult(wccparkingsessionpage.clickPay(), "Click pay button");
        CheckResult(wccparkingsessionpage.enterCVV(cvvCode), "Enter CVV2 code");
        CheckResult(wccparkingsessionpage.clickPayConfirm(), "Click pay button");
       
        CheckResult(wccparkingsessionpage.clickFinish(), "Click finish button");

        LogStep("6", "Verify viewing the active session");
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        LogStep("7", "Click on extend Button and select the duration for which session needs to be extended");
        CheckResult(wccparkingsessionpage.clickExtendLastSession(), "Click extend session button");
        CheckResult(wccparkingsessionpage.selectDuration(timeToPark), "Select time to park");
        CheckResult(wccparkingsessionpage.clickDurationNextButton(), "Click next button");

        CheckBool(wccparkingsessionpage.getErrorMessage().getText().contains(ERROR_MESSAGE),
                "Check error message when user try to extend session right after booking");
        LogStep("8", "Wait 4 minutes, return to the pay to park screen, and attempt to extend an active session.");
        delay(240000);
        CheckResult(wccparkingsessionpage.getWccNavbarUnderUpperMenu().clickPayToParkNav(), "Click next button");

        LogStep("9", "Pay for the extended duration");
        CheckResult(wccparkingsessionpage.clickExtendLastSession(), "Click extend session button");
        CheckResult(wccparkingsessionpage.selectDuration(timeToPark), "Select time to park");
        CheckResult(wccparkingsessionpage.clickDurationNextButton(), "Click next button");
        wccparkingsessionpage.clickPaymentNowTab();
        CheckResult(wccparkingsessionpage.clickBasketNextButton(), "Click next button");

        
        CheckResult(wccparkingsessionpage.clickCardSelectNextButton(), "Click select next button");
        CheckResult(wccparkingsessionpage.clickPay(), "Click pay button");
        CheckResult(wccparkingsessionpage.enterCVV(cvvCode), "Enter CVV2 code");
        CheckResult(wccparkingsessionpage.clickPayConfirm(), "Click pay button");
       
        CheckResult(wccparkingsessionpage.clickFinish(), "Click finish button");

        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        String startedSessionTime = wccparkingsessionpage.getStartedOrExpiresTime(0, "Started").getText();
        String expiresSessionTime = wccparkingsessionpage.getStartedOrExpiresTime(1, "Expires").getText();

        CheckBool(cutOnlyDate(startedSessionTime).equals(cutOnlyDate(expiresSessionTime)), "Check that we extend our session correctly");
    }

    private String cutOnlyDate(String date) {
        return date.replaceAll("\\w+: ", "");
    }
}
