package Cashless.Buytime.Parking;

import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;

public class RingGoExtendSessionWithNoCV2Test extends RingGoScenario {
	
	public String vehicleVrn;	
	public String zoneid = "226688";
	public String hour = "1";
	public String min = "5";
	public String extend_time ="1 Hour";
	
	@BeforeMethod
	public void setUp() throws Exception {
		
		createUser("defaultNewUser.json");
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(mobile, password, RestApiHelper.defaultNewVehicle());
        
        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(mobile, password);
        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();        
        
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(mobile, password, zoneid);
        String paymentMethod = (String) usersPayment.get("Id");

        RestApiHelper.bookSession(mobile, password, paymentMethod, vehicleID, zoneid, hour, min);
       
	}
	
	//TODO - remove all hardcoded test data
	
	@Test
	public void extendSessionWithNoCV2() {
		
		 StartTest("134174", "Extend a session with no CV2");

	     LogStep("Step - 1", "Login to RingGo website (https://myrgo-preprod.ctt.co.uk)");
	     	     
	     NavigationHelper.openMyRingo(_driver);
	     
	     /*
	      *  For documentation purpose 
	      */
	     
	     LogStep("Step - 2", "Create a parking session at zone (23856) using any VRN and click Next");
	     LogStep("Step - 3", "Select 1-hour duration and click Next button");
	     LogStep("Step - 4", "Select the payment card and click Next");
	     LogStep("Step - 5", "Click Pay button");
	     
	     HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
	     CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Click Login");
	     CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link");
	     
	     LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);
	     CheckBool(loginRegisterPage.getCurrentTitle().equals("RingGo Cashless Parking Solution"), "Login or Register with RingGo page should be loaded");
	     CheckResult(loginRegisterPage.enterMobileOrEmail(mobile), "Input 'Mobile number or E-mail' ");
	     CheckResult(loginRegisterPage.enterPassword(password), "Input 'Password'");
	     CheckResult(loginRegisterPage.clickLogin(), "Click 'Log in' button");
	     
	     CheckBool(homePageMyRinggo.getCurrentTitle().equals("RingGo Cashless Parking Solution: Dashboard"), "RingGo user 'home' page should be loaded");

	    
	     CheckResult(homePageMyRinggo.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu");
	     
	     BookParkingPage bookParkingPage = new BookParkingPage(_driver);
	     CheckBool(bookParkingPage.getCurrentTitle().equals("RingGo Cashless Parking Solution: Book a Parking Session"), "User should be taken to 'Book a Parking Session' page");
	     
	     LogStep("Step - 6", "Wait for 3 mins and Go to the Parking page & click Extend button");
	     delay(240000);
	     CheckResult(bookParkingPage.getOptionsBlock().clickExtend(), "Click 'Extend' button on the active session");
	     
         CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");
	     
	     LogStep("Step - 7", "Select 1-hour duration and click Next button");
	     CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(extend_time), "Select duration");
	     CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button");
	     
	     LogStep("Step - 8", "Select the payment card and click Next");
	     CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button");
	     
	     LogStep("Step - 9", "Click Pay button");
	     CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button");
	
	     CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button");
		
	
	}

}