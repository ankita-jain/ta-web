package Cashless.Buytime.Parking;

import static Utilities.StringUtils.getPriceFromLabel;
import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;
import SeleniumHelpers.CommonProperties;
import SeleniumHelpers.WaitMethods;

public class DieselSurchargeLBofIslingtonPetrolVehicleNewSession extends RingGoScenario {
    private String carVrn;

	@BeforeMethod
    public void setUp() throws Exception {
    	
    	createUser("islingtonNewUser.json");

        carVrn = vehicleVrn;

        RestApiHelper.setCarType(email, password, "Car");
    }


    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void dieselSurchargeLBofIslingtonPetrolVehicleNewSession(String TCID, Map<String, String> data) {

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);

        StartTest(TCID, "Diesel Surcharge only for LB of Islington - Petrol Vehicle - New Parking session - Negative Test");

        LogStep("Step - 1", "Go to myRingGo");
        NavigationHelper.openMyRingo(_driver);

        LogStep("Step - 2", "Click 'Login'");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link");

        LogStep("Step - 3", "Input 'Mobile number or E-mail' & password and click 'Log In'");
        CheckResult(loginRegisterPage.enterMobileOrEmail(email), "Input 'Mobile number or E-mail' ");
        CheckResult(loginRegisterPage.enterPassword(password), "Input 'Password'");
        CheckResult(loginRegisterPage.clickLogin(), "Click 'Log in' button");

        LogStep("Step - 4", "Click 'Park' menu in the top of the home page");
        CheckResult(homePage.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu");

        LogStep("Step - 5", "Enter '61175' in 'Which location do you want to park in?' input field " +
                "and select the diesel vehicle from the 'Which vehicle do you want to park? ' drop down list.");
        BookParkingPage bookParkingPage = new BookParkingPage(_driver);
        CheckResult(bookParkingPage.getBookBlock().enterZone(data.get("zone")), "Enter text on 'Zone' input");
        CheckResult(bookParkingPage.getBookBlock().selectVRN(carVrn), "Select vehicle");

        LogStep("Step - 6", "And click 'Next' button");
        CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");

        LogStep("Step - 7", "Select 'Now' button in the 'When do you want to park?' page and click 'Next' button");
        CheckResult(bookParkingPage.getBookBlock().selectNowRadioButton(), "Click on 'Now' radio button");
        CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'");

        LogStep("Step - 8", "Select '1 Hour' and click 'Next' button");
        CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(data.get("sessionDuration")), "Select '1 Hour' from dropdown");
        CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button");

        CheckBool(!WaitMethods.WaitVisible(_driver, CommonProperties.WebDriverWait(), bookParkingPage.getPayBlock().getLinkToPopUp()), "Check 'Diesel surcharge' text is not displayed");
        String price = getPriceFromLabel(bookParkingPage.getPayBlock().getPriceLabel().getText());
        CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button");

        LogStep("Step - 9", "And enter the card details & pay for the parking session");
        CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button");
        CheckResult(bookParkingPage.getPayBlock().enterCvvCode(data.get("CVV2_Code")), "Enter CVV code");
        CheckResult(bookParkingPage.getPayBlock().clickPayConfirmation(), "'Click on 'Pay Confirmation' button");
        
        delay(3000);
        CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button");

        CheckBool(homePage.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.DESCRIPTION).contains(data.get("zone")), "Zone number");
        CheckBool(homePage.getActiveSessionsBlock().getTable().getCellValue(1, HomePageMyRinggo.ActiveSessionHeaders.AMOUNT).contains(price), "Price value");
    }
}
