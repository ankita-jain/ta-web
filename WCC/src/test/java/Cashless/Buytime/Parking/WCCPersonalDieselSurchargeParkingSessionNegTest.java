package Cashless.Buytime.Parking;

import static GenericComponents.constants.Symbols.POUND;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCBookParkingSession;
import PageObjects.WCCCardCVVBookingSession;
import PageObjects.WCCDurationParkingSession;
import PageObjects.WCCFinishBookingSession;
import PageObjects.WCCHome;
import PageObjects.WCCPayParkingSession;
import PageObjects.WCCPaymentDetailsBookParkingSession;
import PageObjects.WCCStatementsPage;
import PageObjects.pagecomplexobjects.WCCNavbarUnderUpperMenu;

public class WCCPersonalDieselSurchargeParkingSessionNegTest extends RingGoScenario {

    private String petrolCarVrn;
    private String cvv;

	@BeforeMethod
    public void setUp() throws Exception {

        cvv = wccproperties.getProperty("CVV2");

        createUser("islingtonNewUser.json");

        petrolCarVrn = vehicleVrn;
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void dieselSurchargeNegTest(String TCID, Map<String, String> data) {
        StartTest(TCID, "WCC Diesel surcharge - Negative scenario");

        RestApiHelper.setCarType(email, password, data.get("vehicleType"));

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
                petrolCarVrn, data.get("yearOfManufacture"),
                data.get("CO2"), data.get("engineSize"),
                data.get("vehicleType"), data.get("fuelType"), _driver);

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        WCCBookParkingSession wccBookParkingSession = new WCCBookParkingSession(_driver);
        CheckResult(wccBookParkingSession.enterZone(data.get("zone")), "Enter zone number");
        CheckResult(wccBookParkingSession.selectVehicle(petrolCarVrn), "Select vehicle");
        CheckResult(wccBookParkingSession.next(), "Click next button");

        WCCDurationParkingSession wccDurationParkingSession = new WCCDurationParkingSession(_driver);
        CheckResult(wccDurationParkingSession.selectDuration(data.get("time")), "Select time to park");
        CheckResult(wccDurationParkingSession.tickSMSConfirm(), "Select SMS confirm");
        CheckResult(wccDurationParkingSession.tickReminderSMS(), "Select reminder SMS");
        CheckResult(wccDurationParkingSession.next(), "Click next button");

        WCCPaymentDetailsBookParkingSession wccPaymentDetailsBookParkingSession = new WCCPaymentDetailsBookParkingSession(_driver);
        wccPaymentDetailsBookParkingSession.clickPaymentNow();
        CheckResult(wccPaymentDetailsBookParkingSession.next(), "Click next button");

        WCCPayParkingSession wccPayParkingSession = new WCCPayParkingSession(_driver);
        String price = wccPayParkingSession.getPriceSectionText();

        CheckBool(price.matches(String.format("^%s\\d+.\\d+$", POUND.getAsUTF8())), "Check price");

        CheckResult(wccPayParkingSession.next(), "Click select next button");

        WCCCardCVVBookingSession wccCardCVVBookingSession = new WCCCardCVVBookingSession(_driver);
        CheckResult(wccCardCVVBookingSession.pay(), "Click pay button");
        CheckResult(wccCardCVVBookingSession.enterCVV(cvv), "Enter CVV2 code");
        CheckResult(wccCardCVVBookingSession.clickPay(), "Click pay confirmation button");

        WCCFinishBookingSession wccFinishBookingSession = new WCCFinishBookingSession(_driver);
        CheckResult(wccFinishBookingSession.finish(), "Click finish button");

        WCCNavbarUnderUpperMenu wccNavbarUnderUpperMenu = new WCCNavbarUnderUpperMenu(_driver);
        CheckResult(wccNavbarUnderUpperMenu.clickOnStatementsNav(), "Click Statements link");

        WCCStatementsPage wccStatementsPage = new WCCStatementsPage(_driver);
        CheckContains(wccStatementsPage.getAmount(1), price, "Check price");
    }
}
