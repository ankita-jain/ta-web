package Cashless.Login.Registration.Management;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.ForgetPassword;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import Utilities.DateUtils;

public class WCCPasswordResettingNegativeTests extends RingGoScenario  {
	
	private String unknown_email;
	private String unknown_mobile_number; 
	
	//TODO - remove all hardcoded test data - should be injected by DP
	
	@Test
	public void passwordResetWithUnknownEmailId() {
		    
		    delay(60000);
		    
		    StartTest("134105", "EURINGAND-2303 - Trouble logging in - Enter Invalid entry for Email, CLI and Password validations");
		    unknown_email = DateUtils.TimeStamp() +"@mail.com";

		    LogStep("Step - 1", "Click 'log in' > 'personal'");
	        NavigationHelper.openWestMinster(_driver);
	        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
	        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

	        LogStep("Step - 2", "Click 'I've forgotten my password'");
	        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
	        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");
	        
	        LogStep("Step - 3", "Provide the email/CLI and VRN of a registered user and click on Next");
	        ForgetPassword forgetPassword = new ForgetPassword(_driver);
	        CheckResult(forgetPassword.pasteUserIdentifier(unknown_email), String.format("Paste user email/CLI -> %s", unknown_email));
	        CheckResult(forgetPassword.next(), "Click Next");
	        
	        CheckValue(forgetPassword.getErrorMessages(),"Sorry, we can't find any RingGo accounts using that mobile number or email address. Please try again.","Checking error message");

	}
	
    @Test
    public void passwordResetWithUnknownCLI() {
    	
    	delay(60000);
    	
    	StartTest("134105", "EURINGAND-2303 - Trouble logging in - Enter Invalid entry for Email, CLI and Password validations");
    	unknown_mobile_number = DateUtils.UKTelFromTimeStamp(DateUtils.TimeStamp());
    
	    LogStep("Step - 1", "Click 'log in' > 'personal'");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");
        
        LogStep("Step - 3", "Provide the email/CLI and VRN of a registered user and click on Next");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(unknown_mobile_number), String.format("Paste user email/CLI -> %s", unknown_mobile_number));
        CheckResult(forgetPassword.next(), "Click Next");
        
        CheckValue(forgetPassword.getErrorMessages(),"Sorry, we can't find any RingGo accounts using that mobile number or email address. Please try again.","Checking error message");
    	
    }
    
    @Test
    public void maxPasswordResetWithWrongVRNOnAccount() throws Exception {
    	
    	delay(60000);
    	
        String randomVRN;
        String warningMessage;
        String incorrect_vrn_three_times_message ="Sorry, you've entered an incorrect VRM 3 times. Please select another recovery option.";
        
        createUser("defaultNewUser.json");
        randomVRN = randomAlphabetic(6);
        
        
        StartTest("134111", "EURINGAND-2303 -Update password reset user journey- Maximum wrong VRN attempt");

        LogStep("Step - 1", "Click 'log in' > 'personal'");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        for(int i=1,maxAttempt=3;i<=3;i++,maxAttempt--) {
        	
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide the email/CLI registered user and click on Next");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        
        LogStep("Step - 4", "Provide the Wrong/Random VRN registered user and click on Next");
        forgetPassword.clickProvideAnyVRNOnAccount();
        CheckResult(forgetPassword.pasteVehicleVrn(randomVRN), String.format("Paste Vehicle VRN -> %s", randomVRN));
        CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
        
        warningMessage="Oops, your VRM is not recognised. Please try again. You have "+(maxAttempt-1)+" more attempts.";
        
        if(!(maxAttempt == 1))
        CheckValue(forgetPassword.getWarningNotificationText(),warningMessage,"Checking error message");
        
        else 
        CheckValue(forgetPassword.getWarningNotificationText(),incorrect_vrn_three_times_message,"Checking error message");	
        }
        
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 5", "Provide the email of a registered user and click on Next");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        
        LogStep("Step - 6", "Try Wrong VRM again - No VRN option should be visible");
        CheckBool(!forgetPassword.isProvideAnyVRNonAccountVisible(),"The user can't see the VRN option avail on account");
    }
    
    @Test
    public void maxPasswordResetWithWrongSecurityQuestionAnswer() throws Exception {
    	
    	delay(60000);
    	
    	String userid, warningMessage;
    	String wrongSecruityAnswer ="batman";
    	String incorrect_sq_three_times_message ="Sorry, you've entered your answer incorrectly 3 times. Please select another recovery option.";
    	createUser("defaultNewUser.json");
        userid = (String) newUserJson.get("UserID");
        
        
        
        Map<Object, Object> securityQuestion = RestApiHelper.userSecurityQuestion(userid);
        RestApiHelper.changeUsersecurityQuestion(mobile,password,securityQuestion);
        
        StartTest("134117", "Update password reset user journey- Maximum wrong Security Question attempt");

        LogStep("Step - 1", "Click 'log in' > 'personal'");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");
        
        LogStep("Step - 2", "Click 'I've forgotten my password'");
        for(int i=1,maxAttempt=3;i<=3;i++,maxAttempt--) {
        	
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");
        
        LogStep("Step - 3", "Enter CLI/Email and click the Next button");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        
        LogStep("Step - 4", "Select Security question radio button and Enter a wrong security question");
        CheckResult( forgetPassword.clickAnswerMySecreteQuestion(), "Click Security Questions");
        CheckResult(forgetPassword.pasteSecretQuestion(wrongSecruityAnswer), "Answer my secret question");
        CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
        
        warningMessage="Oops, answer entered incorrectly. Please try again. You have "+(maxAttempt-1)+" more attempts.";
        
        if(!(maxAttempt == 1))
        CheckValue(forgetPassword.getWarningNotificationText(),warningMessage,"Checking error message");
        
        else 
        CheckValue(forgetPassword.getWarningNotificationText(),incorrect_sq_three_times_message,"Checking error message");		
        }
        
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 7", "Try again to enter the wrong SQ");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        
        CheckBool(!forgetPassword.isAnswerMySQVisbile(),"The user can't see the Answer my SQ option avail on account");
      
    }

}