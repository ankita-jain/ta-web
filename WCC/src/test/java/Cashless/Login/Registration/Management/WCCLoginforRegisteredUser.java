package Cashless.Login.Registration.Management;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;

public class WCCLoginforRegisteredUser extends RingGoScenario {
	
	@BeforeMethod
    public void setUp() throws Exception {
	   
		createUser("defaultNewUser.json");      
    }
	//TODO - remove all hardcoded test data - should be injected by DP
	
	@Test
    public void loginLogoutForRegisteredUsser() {
		
		  StartTest("134101", "Login for registered User");
		  
		  LogStep("1", "Go to http://westminster.myrgo-preprod.ctt.co.uk'");
		  NavigationHelper.openWestMinster(_driver);
		  
	      LogStep("2", "Enter {email} or {CLI} into \"Phone or email\" input");
	      LogStep("3", "Enter {password} into \"Password or PIN\" input for current user");
	      LogStep("4", "Click \"Log in\" button");

	      LoginHelper.loginWestminster(mobile, password, _driver);

	      WCCHome wccHome = new WCCHome(_driver);
	      LogStep("5", "Go to \"Account\" -> \"My details\"");
	      CheckResult(wccHome.clickMyDetails(), "Click my details button");
	      
	      CheckResult(wccHome.logout(), "Click my details button");
		
	}

}