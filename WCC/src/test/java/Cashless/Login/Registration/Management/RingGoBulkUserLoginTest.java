package Cashless.Login.Registration.Management;

import java.util.Map;

import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.myringo.LoginRegisterPage;

public class RingGoBulkUserLoginTest extends RingGoScenario {
	
	@Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	public void bulkUserLogin(String TCID, Map<String, String> data)
	{
		StartTest(TCID, "RingGo Bulk User Login Test");
		
		LogStep("Step - 1", "Log in to MyRingGo (http://myrgo-preprod.ctt.co.uk/)");
	    NavigationHelper.openMyRingo(_driver);
	    LoginHelper.loginMyRingo(data.get("CLI"), data.get("PIN"), _driver);
	    
	    LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);
	    CheckContains(loginRegisterPage.getErrorMessage(), "There is an error in the form. Please check for details below.", "I am checking error message here...");
	    
	    
	}
}
