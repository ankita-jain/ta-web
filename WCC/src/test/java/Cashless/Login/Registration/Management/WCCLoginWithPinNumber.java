package Cashless.Login.Registration.Management;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;

public class WCCLoginWithPinNumber extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("islingtonNewUser.json");
    }

    //TODO - remove all hardcoded test data - should be injected by DP
    @Test
    public void loginWithPinumber() {

        StartTest("134107", "Login to system with PIN with already set password for account");

        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);

        LogStep("Step - 2", "Enter Insight username and password and click 'Log in'.");
        LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);

        LogStep("Step - 3", "Enter {email} or {CLI} into 'Search term'");
        InsightSearch insightSearch = new InsightSearch(_driver);
        CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI into 'Search term'");

        LogStep("Step - 4", "Click on 'Search'");
        CheckResult(insightSearch.clickSubmit(), "Click on 'Search'");

        LogStep("Step - 5", "In the 'Customer' section look for PIN value and save it - {pin}");
        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
        String currentUserPin = insightProfilePage.getMemberPIN();

        LogStep("Step - 6", "Go to https://westminster.myrgo-preprod.ctt.co.uk/ and click 'Personal' sub menu from 'Log in' menu.");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 7", "Enter {email} or {CLI} into 'Phone or email' input.'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.inputEmailOrPhonenumer(mobile), "Enter email or phone number");

        LogStep("Step - 8", "Enter valid pin into 'Password or PIN' input for current user.'");
        CheckResult(wccloginpage.enterPassword(currentUserPin), "Enter password");

        LogStep("Step - 9", "Click on 'Log in'.");
        CheckResult(wccloginpage.clickLoginButton(), "Click login button");
        CheckContains(wccloginpage.getErrorText(), "Log in details are incorrect. Your account has a password set, please use that instead of your PIN to login.",
                "Error text");
    }
}
