package Cashless.Login.Registration.Management;

import static Utilities.TimerUtils.delay;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCSetPasswordPage;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;

public class RingGoLoginWithPINTest  extends RingGoScenario {
	
	   private String newpassword = "Password123";

	    @BeforeMethod
	    public void setUp() throws Exception {
	        createUser("islingtonNewUser.json");
	    }
	    //TODO - remove all hardcoded test data - should be injected by DP
	    @Test
	    public void loginwithPin() {
	    	
	    	 StartTest("14688", "Logging in with pin number (Not valid for Auth microservice testing) RingGo Regression Suite � Web � Cashless");

	         LogStep("Step - 1", "Go to Insight");
	         NavigationHelper.openInsight(_driver);

	         LogStep("Step - 2", "Enter Insight username and password and click 'Log in'.");
	         LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);

	         LogStep("Step - 3", "Enter {email} or {CLI} into 'Search term'");
	         InsightSearch insightSearch = new InsightSearch(_driver);
	         CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI into 'Search term'");

	         LogStep("Step - 4", "Click on 'Search'");
	         CheckResult(insightSearch.clickSubmit(), "Click on 'Search'");

	         LogStep("Step - 5", "In the 'Customer' section look for PIN value and save it - {pin}");
	         InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
	         String currentUserPin = insightProfilePage.getMemberPIN();
	        	         
	         CheckResult(insightProfilePage.clickDeletePasswordButton(), "Click on 'Delete' password");
	         CheckResult(insightProfilePage.clickOnSubmitDeletePassword(), "Confirm Delete password");
	         
	         delay(5000);
	         LogStep("Step - 6", "Go to https://westminster.myrgo-preprod.ctt.co.uk/ and click 'Personal' sub menu from 'Log in' menu.");
	         NavigationHelper.openWestMinster(_driver);
	         WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
	         CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

	         LogStep("Step - 7", "Enter {email} or {CLI} into 'Phone or email' input.'");
	         WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
	         CheckResult(wccloginpage.inputEmailOrPhonenumer(mobile), "Enter email or phone number");

	         LogStep("Step - 8", "Enter valid pin into 'Password or PIN' input for current user.'");
	         CheckResult(wccloginpage.enterPassword(currentUserPin), "Enter password");

	         LogStep("Step - 9", "Click on 'Log in'.");
	         CheckResult(wccloginpage.clickLoginButton(), "Click login button");	
	         
	         WCCSetPasswordPage wccsetpasswordpage = new WCCSetPasswordPage(_driver);
	         CheckResult(wccsetpasswordpage.enterNewPassword(newpassword), "Enter new password");
	         CheckResult(wccsetpasswordpage.enterConfirmPassword(newpassword), "Enter confirm password");
	         CheckResult(wccsetpasswordpage.clickSaveButton(), "Click Save button");
	         
	         WCCHome wccHome = new WCCHome(_driver);
	         wccHome.logout();
	                 
	    	
	    }

}
