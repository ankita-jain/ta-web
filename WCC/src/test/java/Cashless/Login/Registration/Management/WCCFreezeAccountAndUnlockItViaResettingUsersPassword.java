package Cashless.Login.Registration.Management;

import static DataProviderHelpers.RingGoDBHelpers.getPasswordResetCode;
import static Utilities.TimerUtils.delay;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviderHelpers.DBHelper;
import DataProviderHelpers.DatabaseProperties;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.FinishResetPassword;
import PageObjects.ForgetPassword;
import PageObjects.ForgetPasswordVerificationCode;
import PageObjects.ResetPasswordNewPassword;
import PageObjects.WCCAccountLockedPage;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;

public class WCCFreezeAccountAndUnlockItViaResettingUsersPassword extends RingGoScenario {
    private String vrn;
    private String answer;
    private Connection connection;

    @BeforeClass
    public void createConnection() throws Exception {
        connection = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);
        answer = RandomStringUtils.randomAlphabetic(10);
    }

	@BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        vrn = vrm;
    }
    //TODO - remove all hardcoded test data - should be injected by DP
    @Test
    public void freezeAccount() throws SQLException {
        String invalidPassword = RandomStringUtils.randomAlphabetic(10);
        final int stepsRepeatAmount = 3;
        int questionIndex = 1;

        StartTest("134121", "Freeze account and unlock it via resetting user's password");

        LogStep("Step - 1", "Log in to http://westminster.myrgo-preprod.ctt.co.uk");
        NavigationHelper.openWestMinster(_driver);

        LogStep("Step - 2", "Click 'Log In' in upper menu -> click 'Personal'");
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckContains(wccloginpage.getPageTitle(), "Log in", "Login Page Title");

        enterInvalidLoginSeveralTimes(wccloginpage, invalidPassword, stepsRepeatAmount);

        WCCAccountLockedPage accountLockedPage = new WCCAccountLockedPage(_driver);
        CheckContains(accountLockedPage.getAccountLockedHeader(), "Account Locked", "Header text");

        LogStep("Step - 3", "Click 'Reset your password' link");
        CheckResult(accountLockedPage.clickResetPasswordLink(), "Click reset your password link");
        ForgetPassword forgetPasswordPage = new ForgetPassword(_driver);
        CheckContains(forgetPasswordPage.getForgetPasswordHeader(), "I've forgotten my password", "Header text");

        LogStep("Step - 4", String.format("Provide %s email of a registered user and click on Next", email));
        CheckResult(forgetPasswordPage.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPasswordPage.next(), "Click Next");

        LogStep("Step - 5", "Choose vrm option");
        CheckContains(forgetPasswordPage.getForgetPasswordHeader(), "Quick Security Check", "Header text");
        CheckResult(forgetPasswordPage.clickProvideAnyVRNOnAccount(), "Click on vrm option");

        LogStep("Step - 6", String.format("Provide %s vrn of a registered user", vrn));
        CheckResult(forgetPasswordPage.pasteVehicleVrn(vrn), String.format("Paste Vehicle VRN -> %s", vrn));
        CheckResult(forgetPasswordPage.securityCheckNextButtonClick(), "Click Next");

        LogStep("Step - 7", "Choose sms option and click on 'Next' button");
        CheckContains(forgetPasswordPage.getForgetPasswordHeader(), "Verify Your Account", "Header text");
        CheckResult(forgetPasswordPage.clickSMS(), "Click on sms option");
        CheckResult(forgetPasswordPage.verifyAccountNextButtonClick(), "Click Next");

        delay(3000);
        int code = getPasswordResetCode(email, connection);

        LogStep("Step - 8", "Obtain correct code from the Database (dbo.RingGo_Members_Reset) and enter (see SQL in description)");
        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %s", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");

        LogStep("Step - 9", String.format("Enter a password that meets the criteria and a matching confirm - %s", password));
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.pasteNewPassword(password), String.format("Enter new password -> %s", password));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(password), String.format("Enter confirm password -> %s", password));
        CheckResult(resetPasswordNewPassword.chooseQuestionByIndex(questionIndex), String.format("Choose question with index -> %s", questionIndex));
        CheckResult(resetPasswordNewPassword.pasteSecretAnswer(answer), String.format("Enter answer -> %s", answer));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");

        LogStep("Step - 10", "Click 'Finish' button");
        FinishResetPassword finishResetPassword = new FinishResetPassword(_driver);
        CheckResult(finishResetPassword.finish(), "Click finish");

        LogStep("Step - 11", "Log in with new password");
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickAccount(), "Click Account");
        CheckValue(wccHome.dashboardSubMenuTitle(), "Dashboard", "Check Dashboard sub-menu presence");
    }

    private void enterInvalidLoginSeveralTimes(WCCLoginPage wccLoginPage, String invalidPassword, int attemptAmount) {
        int availableAttempAmout = attemptAmount - 1;
        for (int index = 0; index < attemptAmount; index++) {
            LogStep("Step - 3", "Enter email into 'Phone or email' input");
            CheckResult(wccLoginPage.inputEmailOrPhonenumer(email), "Enter email or phone number");

            LogStep("Step - 4", "Enter invalid password into 'Password or PIN' input for current user");
            CheckResult(wccLoginPage.enterPassword(invalidPassword), "Enter password");

            LogStep("Step - 5", "Click 'Log in' button 3 times in a row");
            CheckResult(wccLoginPage.clickLoginButton(), "Click login button");
            if (index != 2) {
                CheckContains(wccLoginPage.getErrorText(),"Log in details are incorrect, please check and try again" ,"Error text");
                --availableAttempAmout;
            }
        }
    }
}
