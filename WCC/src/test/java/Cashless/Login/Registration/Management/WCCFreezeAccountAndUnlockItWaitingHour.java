package Cashless.Login.Registration.Management;

import static org.testng.AssertJUnit.assertTrue;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCAccountLockedPage;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;

public class WCCFreezeAccountAndUnlockItWaitingHour extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }
    // TODO - remove all hardcoded test data - should be injected by DP
    @Test
    public void freezeAccountAndUnlockItWaitingHour() throws InterruptedException {
        final int stepsRepeatAmount = 3;
        String wrongPassword = RandomStringUtils.randomAlphabetic(10);

        StartTest("134110", "Freeze account and unlock it waiting for 1 hour");

        LogStep("Step - 1", "Log in to http://westminster.myrgo-preprod.ctt.co.uk");
        NavigationHelper.openWestMinster(_driver);

        LogStep("Step - 2", "Click 'Log In' in upper menu -> click 'Personal'");
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckContains(wccloginpage.getPageTitle(), "Log in", "Login Page Title");

        enterInvalidLoginSeveralTimes(wccloginpage, wrongPassword, stepsRepeatAmount);

        WCCAccountLockedPage accountLockedPage = new WCCAccountLockedPage(_driver);
        CheckContains(accountLockedPage.getAccountLockedHeader(), "Account Locked", "Header text");

        LogStep("Step - 6", "Wait for 1 hour");
        TimeUnit.HOURS.sleep(1);

        LogStep("Step - 7", "Go to login page for personal account");
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 8", "Enter {username} into 'Phone or email' input");
        CheckResult(wccloginpage.inputEmailOrPhonenumer(email), "Enter email or phone number");

        LogStep("Step - 9", "Enter {password} into 'Password or PIN' input for current user");
        CheckResult(wccloginpage.enterPassword(password), "Enter password");

        LogStep("Step - 10", "Click 'Log in'");
        CheckResult(wccloginpage.clickLoginButton(), "Click login button");

        WCCHome wcchome = new WCCHome(_driver);
        assertTrue("Home page title", wcchome.getHomePageTitle().contains("Welcome"));

        CheckResult(wcchome.logout(), "Click logout button");
    }

    private void enterInvalidLoginSeveralTimes(WCCLoginPage wccLoginPage, String invalidPassword, int attemptAmount) {
        int availableAttempAmout = attemptAmount - 1;
        for (int index = 0; index < attemptAmount; index++) {
            LogStep("Step - 3", "Enter email into 'Phone or email' input");
            CheckResult(wccLoginPage.inputEmailOrPhonenumer(email), "Enter email or phone number");

            LogStep("Step - 4", "Enter invalid password into 'Password or PIN' input for current user");
            CheckResult(wccLoginPage.enterPassword(invalidPassword), "Enter password");

            LogStep("Step - 5", "Click 'Log in' button 3 times in a row");
            CheckResult(wccLoginPage.clickLoginButton(), "Click login button");
            if (index != 2) {
                CheckContains(wccLoginPage.getErrorText(), String.format("Login failed. Please make sure your login details are correct. " +
                                "You have %s remaining attempts left before your account is locked. " +
                                "Your account will be locked on your next login failure.", availableAttempAmout),
                        "Error text");
                --availableAttempAmout;
            }
        }
    }
}
