package Cashless.Login.Registration.Management;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCLoginPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightSearch;

public class LoginToSystemWithPINWithAlreadySetPassword extends RingGoScenario {

   @BeforeMethod
    public void setUp() throws Exception {
        createUser("islingtonNewUser.json");
    }


    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void loginToSystemWithPinWithAlreadySetPassword(String TCID, Map<Object, Object> Data) {
        StartTest(TCID, "Add Address - Positive test");

        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);
        LoginHelper.loginInsight(Data.get("insightLogin").toString(), Data.get("password").toString(), _driver);

        LogStep("Step - 2", "Enter {email} or {CLI} into Search term");
        InsightSearch insightsearch = new InsightSearch(_driver);
        CheckResult(insightsearch.enterSearchTerm(email), String.format("Entering {%s} to Search term field", email));
        CheckResult(insightsearch.selectSearchType("Email"), "Select Email as Search type");

        LogStep("Step - 3", "Click Search button");
        CheckResult(insightsearch.clickSubmit(), "Click on Submit button");

        LogStep("Step - 4", "In the Customer section look for PIN value and save it - {pin}");
        String pin = insightsearch.getPinElementText();

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();

        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();

        LogStep("Step - 5", "Go to Wesminster");
        NavigationHelper.openWestMinster(_driver);

        LogStep("Step - 6", "Click Log In in upper menu -> click Personal");
        LoginHelper.loginWestminster(email, pin, _driver);
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckContains(wccloginpage.getErrorText(), "Log in details are incorrect. Your account has a password set, please use that instead of your PIN to login.",
                "Error text");

        WCCHome wccHome = new WCCHome(_driver);
        wccHome.logout();
    }
}
