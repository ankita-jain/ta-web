package Cashless.Login.Registration.Management;

import static DataProviderHelpers.RingGoDBHelpers.getArchiveResetCode;
import static DataProviderHelpers.RingGoDBHelpers.getPasswordResetCode;
import static Utilities.TimerUtils.delay;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviderHelpers.DBHelper;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.FinishResetPassword;
import PageObjects.ForgetPassword;
import PageObjects.ForgetPasswordVerificationCode;
import PageObjects.ResetPasswordNewPassword;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import Utilities.PropertyUtils;

public class WCCResettingThePasswordPositive extends RingGoScenario {
    private String newPassword;
    private String answer;
    private String vehicleVrn;
    private Connection connection;
    private String userid;
    private String secretanswer ="Superman";

    @BeforeClass
    public void createConnection() throws Exception {
        newPassword = RandomStringUtils.randomAlphabetic(4).toLowerCase() + RandomStringUtils.randomAlphabetic(4).toUpperCase() + RandomStringUtils.randomNumeric(4);
        answer = RandomStringUtils.randomAlphabetic(10);  
        //Properties prop = PropertyUtils.Load("Environment.properties");
        connection = DBHelper.connectToMSSQLServer(properties);
    }

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("defaultNewUser.json");
        userid = (String) newUserJson.get("UserID");
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
    }
    //TODO - remove all hardcoded test data - needs to be injected by DP
    @Test
    public void resetPassword() throws SQLException {
        int questionIndex = 1;
        
        StartTest("134113", "Reset password via SMS code - Happy path");

        LogStep("Step - 1", "Click 'log in' > 'personal' & Click 'I've forgotten my password ");
                NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 2", "Enter CLI/Email and click NEXT");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        
        LogStep("Step - 3", "Select the VRM option to recover and enter correct VRM in account, click Next");
        forgetPassword.clickProvideAnyVRNOnAccount();
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
        
        LogStep("Step - 4", "Select mobile number to send verification code");
        CheckResult(forgetPassword.clickSMS(), "Click Next");
        CheckResult(forgetPassword.verifyAccountNextButtonClick(), "Click Next"); 

        LogStep("Step - 4", "Obtain correct code from the Database (dbo.RingGo_Members_Reset) and enter");
        delay(3000);
        int code = getPasswordResetCode(email, connection);
       
        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %s", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");

        LogStep("Step - 5", "Set new password & leave new secret ques and answer");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.pasteNewPassword(newPassword), String.format("Enter new password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(newPassword), String.format("Enter confirm password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.chooseQuestionByIndex(questionIndex), String.format("Choose question with index -> %s", questionIndex));
        CheckResult(resetPasswordNewPassword.pasteSecretAnswer(answer), String.format("Enter answer -> %s", answer));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");

        FinishResetPassword finishResetPassword = new FinishResetPassword(_driver);
        CheckResult(finishResetPassword.finish(), "Click finish");

        LogStep("Step - 6", "Log in with new password");
        LoginHelper.loginWestminster(email, newPassword, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickAccount(), "Click Account");
        CheckValue(wccHome.dashboardSubMenuTitle(), "Dashboard", "Check Dashboard sub-menu presence");
    }
    
    @Test
    public void resetPasswordResetInvalidSMSCodeMaxmiumTries() throws SQLException {
    	
    	 StartTest("134118", "Password reset - Maximum attempt with Wrong SMS code");

         LogStep("Step - 1", "Click 'log in' > 'personal'");
                 NavigationHelper.openWestMinster(_driver);
         WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
         CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

         LogStep("Step - 2", "Click 'I've forgotten my password'");
         WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
         CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

         LogStep("Step - 3", "Enter CLI/Email and click the Next button");
         ForgetPassword forgetPassword = new ForgetPassword(_driver);
         CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
         CheckResult(forgetPassword.next(), "Click Next");
         
         LogStep("Step - 4", "Enter VRM on the user account and click NEXT");
         forgetPassword.clickProvideAnyVRNOnAccount();
         CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
         CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
         CheckResult(forgetPassword.clickSMS(), "Click Next");
         CheckResult(forgetPassword.verifyAccountNextButtonClick(), "Click Next"); 
         
         LogStep("Step - 5", "Enter a Wrong SMS code and Click NEXT button");
         String invalidSMS = RandomStringUtils.randomNumeric(4);
         ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
         
         for(int i=1;i<=3;i++) {
        
       
         CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(invalidSMS), String.format("Enter Verification code -> %s", invalidSMS));
         CheckResult(forgetPasswordVerificationCode.next(), "Click Next");
         
         CheckContains(forgetPasswordVerificationCode.getVerificationCodeErrorText(), "Could not find Code for User", "Check wrong sms error text");
         }
         LogStep("Step - 6", "Repeat Step 5 three times");
         
         LogStep("Step - 7", "Enter wrong SMS code 4 attempt");
         CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(invalidSMS), String.format("Enter Verification code -> %s", invalidSMS));
         CheckResult(forgetPasswordVerificationCode.next(), "Click Next");
         
         CheckContains(forgetPasswordVerificationCode.getVerificationCodeErrorText(), "Sorry, you have entered the code incorrectly too many times, please try again later", "Check wrong sms error text");
    	
    }

    @Test
    public void resetPasswordAndLoginWithOld() throws SQLException {
        int questionIndex = 1;

        StartTest("134120", "Resetting the password and login with old password");

        LogStep("Step - 1", "Click 'log in' > 'personal'");
                NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide the email/CLI and VRN of a registered user and click on Next");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        forgetPassword.clickProvideAnyVRNOnAccount();
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
        CheckResult(forgetPassword.clickSMS(), "Click Next");
        CheckResult(forgetPassword.verifyAccountNextButtonClick(), "Click Next"); 

        LogStep("Step - 4", "Obtain correct code from the Database (dbo.RingGo_Members_Reset) and enter");
        delay(3000);
        int code = getPasswordResetCode(email, connection);

        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %s", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");

        LogStep("Step - 5", "Enter a password that meets the criteria and a matching confirm");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.pasteNewPassword(newPassword), String.format("Enter new password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(newPassword), String.format("Enter confirm password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.chooseQuestionByIndex(questionIndex), String.format("Choose question with index -> %s", questionIndex));
        CheckResult(resetPasswordNewPassword.pasteSecretAnswer(answer), String.format("Enter answer -> %s", answer));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        FinishResetPassword finishResetPassword = new FinishResetPassword(_driver);
        CheckResult(finishResetPassword.finish(), "Click finish");

        LogStep("Step - 6", "After resetting the password try to quickly login with the old password. See EUWEBUI-2898 for more details");
        LoginHelper.loginWestminster(email, password, _driver);
        CheckContains(wccindexpage.getLoginErrorText(), "Log in details are incorrect, please check and try again", "Check login error text");
    } 
    
    @Test
    public void deleteMyAccountAndStartAgain() throws Exception {
    	
    	 StartTest("134119", "Password Reset - Delete Account and Start Again");
    	 
         LogStep("Step - 1", "Click 'log in' > 'personal'");
                 NavigationHelper.openWestMinster(_driver);
         WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
         CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

         LogStep("Step - 2", "Click 'I've forgotten my password'");
         WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
         CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

         LogStep("Step - 3", "Provide the email/CLI registered user and click on Next");
         ForgetPassword forgetPassword = new ForgetPassword(_driver);
         CheckResult(forgetPassword.pasteUserIdentifier(mobile), String.format("Paste user email/CLI -> %s", mobile));
         CheckResult(forgetPassword.next(), "Click Next");
         
         LogStep("Step - 4", "Select 'Delete my account and start again' and Click 'NEXT' button");
         CheckResult(forgetPassword.clickArchiveAccount(), "Click Delete my account and start account");
         CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next on Security Check page"); 
         
         LogStep("Step - 5", "Select 'Send me a text message to *********222' and Click NEXT");
         CheckResult(forgetPassword.clickSMS(), "Click Next");
         CheckResult(forgetPassword.verifyAccountOnArchiveNextButtonClick(), "Click Next"); 
         
         LogStep("Step - 6", "Go to '[RingGo_PreProd].[dbo].[RingGo_Members_Reset]' Db and find ',[ArchiveCode]' and paste in the sms code field and Click NEXT");
         delay(3000);
         
         Properties prop = PropertyUtils.Load("Environment.properties");
         int code = getArchiveResetCode(mobile,prop);
         
         ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
         CheckResult(forgetPasswordVerificationCode.pasteArchiveCode(Integer.toString(code)), String.format("Enter Verification code -> %s", code));
         CheckResult(forgetPasswordVerificationCode.verifyArchiveCodeNext(), "Click Next");
         
         LogStep("Step - 7", "Select 'I confirm that I want to close my account' and Click NEXT");  
         CheckResult(forgetPasswordVerificationCode.clickCloseAccountTermsAndConditions(), "Agree Terms and Conditions");
         CheckResult(forgetPasswordVerificationCode.clickCloseAccountButton(), "Agree Terms and Conditions");
         
         delay(3000);
         LogStep("Step - 8", "Try login with Old account details");
         LoginHelper.loginWestminster(mobile, password, _driver);
         CheckContains(wccindexpage.getLoginErrorText(), "Log in details are incorrect.", "Check login error text");   
    	
    } 
    
    @Test
    public void answerMySecreteQuestion() throws Exception { 
    	
    	Map<Object, Object> securityQuestion = RestApiHelper.userSecurityQuestion(userid);
        RestApiHelper.changeUsersecurityQuestion(mobile,password,securityQuestion);
          
        StartTest("134114", "Password reset - with correct secret answer - Happy path");

        LogStep("Step - 1", "Click 'log in' > 'personal'");
                NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide the email/CLI and VRN of a registered user and click on Next");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        CheckResult(forgetPassword.clickAnswerMySecreteQuestion(), "Answer my secret question");
        CheckResult(forgetPassword.pasteSecretQuestion(secretanswer), "Answer my secret question");
        CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
        
        LogStep("Step - 4", "Enter a password that meets the criteria and a matching confirm");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.pasteNewPassword(newPassword), String.format("Enter new password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(newPassword), String.format("Enter confirm password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");

        FinishResetPassword finishResetPassword = new FinishResetPassword(_driver);
        CheckResult(finishResetPassword.finish(), "Click finish");

        LogStep("Step - 5", "Log in with new password");
        LoginHelper.loginWestminster(email, newPassword, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickAccount(), "Click Account");
        CheckValue(wccHome.dashboardSubMenuTitle(), "Dashboard", "Check Dashboard sub-menu presence");
    	
    }
    
    @Test
    public void resetPasswordCheckingErrorMessages() throws SQLException {
    	
    	String invalidPassword ="Password";
    	String inCorrectPassword ="Password123";
    	String confirm_inCorrectPassword ="Password12345";
 
        Map<Object, Object> securityQuestion = RestApiHelper.userSecurityQuestion(userid);
        RestApiHelper.changeUsersecurityQuestion(mobile,password,securityQuestion);
      
        StartTest("134105", "EURINGAND-2303 - Trouble logging in - Enter Invalid entry for Email, CLI and Password validations");

        LogStep("Step - 1", "Click 'log in' > 'personal'");
                NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide the email/CLI and VRN of a registered user and click on Next");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.next(), "Click Next");
        forgetPassword.clickProvideAnyVRNOnAccount();
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.securityCheckNextButtonClick(), "Click Next"); 
        CheckResult(forgetPassword.clickSMS(), "Click Next");
        CheckResult(forgetPassword.verifyAccountNextButtonClick(), "Click Next"); 
        
        LogStep("Step - 4", "Obtain correct code from the Database (dbo.RingGo_Members_Reset) and enter");
        delay(3000);
        int code = getPasswordResetCode(email, connection);
       
        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %s", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");
        
        LogStep("Step - 5", "Checking error messahes are showing up");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        
        CheckValue(resetPasswordNewPassword.getPasswordErrorText(), "New Password is required", "Password error message");
        CheckValue(resetPasswordNewPassword.getConfirmPasswordErrorText(), "Confirm Password is required", "Confirm password error message");
        
        
        CheckResult(resetPasswordNewPassword.pasteNewPassword(invalidPassword), String.format("Enter new password -> %s", invalidPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(invalidPassword), String.format("Enter confirm password -> %s", invalidPassword));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        
        CheckValue(resetPasswordNewPassword.getPasswordErrorText(), "The password does not meet the correct format.", "New password doesn;t meet the criteria message");
        
        CheckResult(resetPasswordNewPassword.pasteNewPassword(inCorrectPassword), String.format("Enter new password -> %s", inCorrectPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(confirm_inCorrectPassword), String.format("Enter confirm password -> %s", confirm_inCorrectPassword));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        
        CheckValue(resetPasswordNewPassword.getConfirmPasswordErrorText(), "Password and confirm password are not the same", "Password and confirm password are not the same Error Message");
    }

   
    @AfterClass
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
