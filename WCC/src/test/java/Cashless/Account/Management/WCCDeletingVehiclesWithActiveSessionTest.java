package Cashless.Account.Management;

import static SeleniumHelpers.WaitMethods.WaitVisible;

import java.util.Map;
import java.util.Objects;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyVehiclesPage;

public class WCCDeletingVehiclesWithActiveSessionTest extends RingGoScenario {
    private String carVrn;
    private String vehicleID;
    private String zoneId,hours,minutes;

    @BeforeMethod()
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        carVrn = vehicleVrn;
        vehicleID = RestApiHelper.getVehicleId(RestApiHelper.getInfoAboutVehicle(email, password));
    }

    private void bookSession(String zoneId, String hours, String minutes) {
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
        String paymentMethod = (String) usersPayment.get("Id");

        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void deletingVehicleWithActiveSessionTest(String TCID, Map<Object, Object> data) {
        StartTest(TCID, "Deleting a vehicle in an active parking session");
        
        zoneId = data.get("zoneId").toString();
        hours  = data.get("hours").toString(); 
        minutes = data.get("minutes").toString();

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        bookSession(zoneId, hours, minutes);

        WCCHome wccHome = new WCCHome(_driver);
        wccHome.clickVehiclesLink();

        WCCMyVehiclesPage wccMyVehiclesPage = new WCCMyVehiclesPage(_driver);
        CheckBool(wccMyVehiclesPage.getTable().getCellValue(1, WCCMyVehiclesPage.MyVehicleTable.VEHICLE).equals(carVrn),
                "Check that table contain correct vehicle VRN");

        CheckBool(Objects.isNull(WaitVisible(_driver, 5, wccMyVehiclesPage.getRemoveXPathLink(vehicleID))), "No delete button");
    }

}
