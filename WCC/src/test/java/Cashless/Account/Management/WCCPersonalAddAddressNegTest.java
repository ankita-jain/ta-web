package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class WCCPersonalAddAddressNegTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCPersonalAddAddressNegTest.class);

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void addAddressNegTest(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "Personal - Add Address Neg Test");


        try {
            NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(data.get("username").toString()), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(data.get("password").toString()), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Home page title", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickAddress(), "Click addresses button");
            

            WCCMyAddressPage wccmyaddresspage = new WCCMyAddressPage(_driver);
            assertTrue("My Address page title", wccmyaddresspage.getAddressPageTitle().contains("My Addresses"));
            if (!wccmyaddresspage.isAddAddressButtondispalyed()) {
                LogError("Personal_add_address");
            }
            wccmyaddresspage.clickAddAddressButton();

            WCCAddAddressPage wccaddaddresspage = new WCCAddAddressPage(_driver);
            assertTrue("Add Address page title", wccaddaddresspage.getAddAddressPageTitle().contains("Add Address"));
            wccaddaddresspage.clickSaveButton();

            List<String> expectedErrorMessages = Arrays.asList(data.get("error").toString().split("\\-", -1));

            assertEquals(expectedErrorMessages, wccaddaddresspage.getErrorMessages());
            assertTrue("Validaing warning message", wccaddaddresspage.getWarningNotification().contains("There is an error in the form. Please check for details below."));

            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
