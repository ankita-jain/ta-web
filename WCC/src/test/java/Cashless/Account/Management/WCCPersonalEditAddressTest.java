package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCAddAddressPage;
import PageObjects.WCCHome;
import PageObjects.WCCMyAddressPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static GenericComponents.helper.RestApiHelper.addNewAddress;

public class WCCPersonalEditAddressTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void editAddress(String TCID, Map<Object, Object> Data) throws IOException {
        Map<Object,Object> address = RestApiHelper.buildAddress(Data);
        String addressLine1 = Data.get("addressLine").toString();
        addNewAddress(email, password, address);

        StartTest(TCID, "Edit Address - Positive test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickAddress(), "Click addresses button");

        WCCMyAddressPage wccmyaddresspage = new WCCMyAddressPage(_driver);
        CheckResult(wccmyaddresspage.clickEditAddress(addressLine1), String.format("Click edit address -> %s", addressLine1));

        WCCAddAddressPage wccaddaddresspage = new WCCAddAddressPage(_driver);
        CheckResult(wccaddaddresspage.selectAddressType(Data.get("newAddressType").toString()), String.format("Select address type -> %s", Data.get("newAddressType").toString()));
        CheckResult(wccaddaddresspage.selectCounty(Data.get("county").toString()), String.format("Select county type -> %s", Data.get("county").toString()));
        CheckResult(wccaddaddresspage.enterPostcode(Data.get("newPostcode").toString()), String.format("Enter postcode -> %s", Data.get("newPostcode").toString()));
        CheckResult(wccaddaddresspage.clickSaveButton(), "Click Save button");

        CheckContains(wccmyaddresspage.getSuccessNotification(), "successfully edited", "Check my Address success notification message");

        CheckResult(wcchome.logout(), "Click logout button");

    }

}
