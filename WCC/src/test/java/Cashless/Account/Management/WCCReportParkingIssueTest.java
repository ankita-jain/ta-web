package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCReportParkingIssuePage;
import TestScenario.Corporate.Corporate_Settings;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;


public class WCCReportParkingIssueTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Corporate_Settings.class);

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void reportParkingIssue(String TCID, Map<String, String> data) throws IOException {
        StartTest("134154", "Verify raising a fault ( Parking issue)");


        try {
            NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Checking WCC Login page title", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(data.get("username")), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(data.get("password")), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Checking WCC Home page title", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickReportParkingIssue(), "Click Report parking issue button");


            WCCReportParkingIssuePage wccreportparkingissuepage = new WCCReportParkingIssuePage(_driver);
            assertTrue("Checking WCC Parking Report page title", wccreportparkingissuepage.getHomePageTitle().contains("Report a parking issue"));

            wccreportparkingissuepage.clickParkingIssue(data.get("parking_issue"));
            wccreportparkingissuepage.clickParkingIssue(data.get("parking_issue_details"));
            wccreportparkingissuepage.enterRoadName(data.get("road_name"));
            wccreportparkingissuepage.uploadPhotoOfParkingIssue(data.get("proof"));

            wccreportparkingissuepage.clickNextButton();
            wccreportparkingissuepage.clickFinishButton();

            wcchome.logout();

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }
}
