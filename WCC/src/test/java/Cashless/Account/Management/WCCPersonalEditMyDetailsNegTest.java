package Cashless.Account.Management;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;

public class WCCPersonalEditMyDetailsNegTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCPersonalEditMyDetailsNegTest.class);

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void editMyPersonalDetails(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "Edit users's contact details - negative");


        try {

        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(data.get("username").toString()), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(data.get("password").toString()), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Home page title", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickMyDetails(), "Click my details button");

            WCCContactDetailsPage wcccontactdetailspage = new WCCContactDetailsPage(_driver);
            assertTrue("Check Contact Details title", wcccontactdetailspage.getContactDetailsPageTitle().contains("Contact Details"));
            wcccontactdetailspage.removeCacheValuesFromMandatoryFields();
            wcccontactdetailspage.clickSaveButton();
            assertTrue("Checking Error message in My Details page", wcccontactdetailspage.CheckErrorMessages(data.get("error").toString()));
            assertTrue("Checking error notification in my details page", wcccontactdetailspage.getNotificationMessage().contains("There is an error in the form. Please check for details below."));

            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
