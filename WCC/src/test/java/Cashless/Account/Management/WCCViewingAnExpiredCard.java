package Cashless.Account.Management;

import static java.lang.String.format;

import java.util.Map;

import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCDashboardPage;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCViewingAnExpiredCard extends RingGoScenario {

    private static final String ERROR_MESSAGE_FOR_PAYMENT_SECTION =
            "Your current payment card ending in %s has expired. " +
                    "Please update this cards details in order to be able to use this payment card";

    private static final String ERROR_MESSAGE_FOR_DASHBOARD_SECTION =
            "Your current payment card ending in %s has expired. " +
                    "Please update this card's details in order to be able to use this payment card";


    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void wccViewingAnExpiredCardInPaymentSection(String TCID, Map<String, String> data) {

        StartTest(TCID, "Viewing an expired card in payment section");

        NavigationHelper.openWestMinster(_driver);
        LogStep("1", "Log in to an account that has an expired card ");
        LoginHelper.loginWestminster(data.get("email"), data.get("password"), _driver);


        WCCHome wccHome = new WCCHome(_driver);
        LogStep("2", "Go to 'Account' > 'Payment' ");
        CheckResult(wccHome.clickPayment(), "Click 'Payment'");

        WCCPaymentDetailsPage wccPaymentDetailsPage = new WCCPaymentDetailsPage(_driver);
        CheckBool(wccPaymentDetailsPage.getErrorNotification().equals(format(ERROR_MESSAGE_FOR_PAYMENT_SECTION, data.get("last4ExpiredCardNumber"))),
                "Check error message");
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void wccViewingAnExpiredCardInDashboardSection(String TCID, Map<String, String> data) {
        StartTest(TCID, "Viewing an expired card in dashboard section");

        NavigationHelper.openWestMinster(_driver);
        LogStep("1", "Log in to an account that has an expired card ");
        LoginHelper.loginWestminster(data.get("email"), data.get("password"), _driver);

        WCCHome wccHome = new WCCHome(_driver);
        LogStep("2", "Go to 'Account' > 'Dashboard' ");
        CheckResult(wccHome.clickDashBoard(), "Click 'Dashboard'");

        WCCDashboardPage wccDashboardPage = new WCCDashboardPage(_driver);
        CheckBool(wccDashboardPage.getErrorNotification().equals(format(ERROR_MESSAGE_FOR_DASHBOARD_SECTION, data.get("last4ExpiredCardNumber"))),
                "Check error message");
    }

}
