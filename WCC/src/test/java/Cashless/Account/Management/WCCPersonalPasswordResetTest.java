package Cashless.Account.Management;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCDashboardPage;
import PageObjects.WCCHome;
import PageObjects.WCCSecurityOptions;

public class WCCPersonalPasswordResetTest extends RingGoScenario {
    private String newPassword;

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        newPassword = RandomStringUtils.randomAlphabetic(4).toLowerCase() + RandomStringUtils.randomAlphabetic(4).toUpperCase() + RandomStringUtils.randomNumeric(4);
    }
    //TODO - remove all hardcoded test data
    @Test
    public void passwordReset() {
        StartTest("20508", "Change password on user account - Positive");

        LogStep("Step - 1", "Login westminster");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("Step - 2", "Go to Account -> Security Options");
        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickSecurityOptions(), "Click security options button");

        LogStep("Step - 3", "Enter 'Old Password' , 'New Password' and 'Confirm Password' and click 'Save'");
        WCCSecurityOptions wccsecurityoptions = new WCCSecurityOptions(_driver);

        CheckResult(wccsecurityoptions.enterOldPassword(password), String.format("Enter password -> %s", password));
        CheckResult(wccsecurityoptions.enterNewPassword(newPassword), String.format("Enter new password -> %s", newPassword));
        CheckResult(wccsecurityoptions.enterConfirmPassword(newPassword), String.format("Enter confirm password -> %s", newPassword));
        CheckResult(wccsecurityoptions.clickSaveButton(), "Click save button");

        WCCDashboardPage wccDashboardPage = new WCCDashboardPage(_driver);

        CheckContains(wccDashboardPage.getDashboardPageTitle(), "Dashboard", "Check dashboard page title");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}
