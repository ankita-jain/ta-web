package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCParkingSessionPage;
import org.testng.annotations.Test;

import java.util.Map;

import static SeleniumHelpers.WaitMethods.WaitVisible;
import static SeleniumHelpers.WebElementMethods.dropdownContainsTextValue;

public class WCCPayingWithExpiredCard extends RingGoScenario {
    
    /*
     * Marking this test and redundant, 
     * because there is no update functionality in the new payment workflow.
     */
    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class, enabled=false)
    public void wccPayingWithExpiryCard(String TCID, Map<String, String> data) {
        StartTest(TCID, "Paying with an expired card");

        LogStep("1", "Login to Westminster");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(data.get("email"), data.get("password"), _driver);

        LogStep("2", "Click 'Pay to park'");
        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        LogStep("3", "Click 'book a new session' and follow the steps till you get to the 'Pay' section");
        WCCParkingSessionPage wccparkingsessionpage = new WCCParkingSessionPage(_driver);

        if (wccparkingsessionpage.isBookNewVisible())
            CheckResult(wccparkingsessionpage.clickBookNew(), "Click book new");

        CheckResult(wccparkingsessionpage.enterZone("8482"), "Enter zone number");
        CheckResult(wccparkingsessionpage.clickNextButton(), "Click next button");
        CheckResult(wccparkingsessionpage.selectDuration("1 Hour"), "Select time to park");
        CheckResult(wccparkingsessionpage.selectSMSConfirm(), "Select SMS confirm");
        CheckResult(wccparkingsessionpage.selectReminderSMS(), "Select reminder SMS");
        CheckResult(wccparkingsessionpage.clickDurationNextButton(), "Click next button");
        CheckResult(wccparkingsessionpage.clickTakePaymentNow(), "Click take payment now button");
        CheckResult(wccparkingsessionpage.clickBasketNextButton(), "Click next button");

        LogStep("4", "Try to find the expired card in the drop down menu");
        
        CheckBool(wccparkingsessionpage.isNoActiveCardMessageVisible(),"No Active card in the user account");

    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void wccPayingWithOnlyOneExistedExpiredCard(String TCID, Map<String, String> data) {

        StartTest(TCID, "Paying with only one existed expired card");

        LogStep("1", "Login to Westminster");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(data.get("email"), data.get("password"), _driver);

        LogStep("2", "Click 'Pay to park'");
        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayToPark(), "Click pay to park button");

        LogStep("3", "Click 'book a new session' and follow the steps till you get to the 'Pay' section");
        WCCParkingSessionPage wccparkingsessionpage = new WCCParkingSessionPage(_driver);

        if (wccparkingsessionpage.isBookNewVisible())
            CheckResult(wccparkingsessionpage.clickBookNew(), "Click book new");

        CheckResult(wccparkingsessionpage.enterZone("8142"), "Enter zone number");
        CheckResult(wccparkingsessionpage.clickNextButton(), "Click next button");
        CheckResult(wccparkingsessionpage.selectDuration("1 Hour"), "Select time to park");
        CheckResult(wccparkingsessionpage.selectSMSConfirm(), "Select SMS confirm");
        CheckResult(wccparkingsessionpage.selectReminderSMS(), "Select reminder SMS");
        CheckResult(wccparkingsessionpage.clickDurationNextButton(), "Click next button");
        CheckResult(wccparkingsessionpage.clickTakePaymentNow(), "Click take payment now button");
        CheckResult(wccparkingsessionpage.clickBasketNextButton(), "Click next button");

         /*
          *  If the user has only one card and the card is expired one,
          *  then, the user should see 'No Active card in the user account'      
          */
        CheckBool(wccparkingsessionpage.isNoActiveCardMessageVisible(),"No Active card in the user account");

    }
}
