package Cashless.Account.Management;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.HelpPage;
import PageObjects.myringo.HomePageMyRinggo;

public class RingGoDeleteUserWithActiveSession extends RingGoScenario{
	
    String zoneId = "127952";
	String hours = "1";
	String minutes = "0";

    @BeforeMethod
    public void setUp() throws Exception {
    
        createUser("defaultNewUser.json");
        
        RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
        
        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();
        
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
        String paymentMethod = (String) usersPayment.get("Id");
        
        //Creates a user session
        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);	        
    }
    
    //TODO - remove all hardcoded test data
   @Test
    public void deleteUserWithActiveSession() throws Exception {
    StartTest("134168", "Test to Ensure user with active session cannot be deleted");
	LogStep("Step - 1", "Open RingGoWeb");
	NavigationHelper.openMyRingo(_driver);
	
	LogStep("Step - 2", "Enter login and password");
	LoginHelper.loginMyRingo(email, password, _driver);
	
	LogStep("Step - 3", "Click on Help");
	HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
	CheckResult(homePageMyRinggo.clickHelpLink(), "Click Help");
	
	LogStep("Step - 4", "Click on Account Closure Request");
	HelpPage helpPage = new HelpPage(_driver);
	helpPage.clickAccountClosureRequest();
	
	LogStep("Step - 5", "Give reason for Account Closure ");
	helpPage.selectAccountClosureReason("I had trouble registering my vehicle");
	
	LogStep("Step - 6", "Click on Next ");
	helpPage.clickNext();
	
	LogStep("Step - 7", "Confirm Account Closure ");
	helpPage.confirmClosure();
	
	LogStep("Step - 8", "Click on Next after Confirming ");
	helpPage.confirmClosureNext();
	
	LogStep("Step - 9", "Check for error message");
	CheckContains(helpPage.getAccountClosureText(),"Your account could not be closed because you have active sessions or payments pending.","AccountClosureText");
	
   }
}
