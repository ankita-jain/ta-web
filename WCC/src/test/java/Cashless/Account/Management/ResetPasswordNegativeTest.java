package Cashless.Account.Management;

import static DataProviderHelpers.RingGoDBHelpers.getPasswordResetCode;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviderHelpers.DBHelper;
import DataProviderHelpers.DatabaseProperties;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import PageObjects.ForgetPassword;
import PageObjects.ForgetPasswordVerificationCode;
import PageObjects.ResetPasswordNewPassword;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;

public class ResetPasswordNegativeTest extends RingGoScenario {

    private Connection connection;

    @BeforeClass
    public void createConnection() throws Exception {
        connection = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);
    }

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("islingtonNewUser.json");
    }
    // TODO - remove all hardcoded test data
    @Test
    public void resetPasswordWithIncorrectVerificationCode() {
        String incorrectVerificationCode = RandomStringUtils.randomNumeric(4);

        StartTest("20524", "Resetting the password with invalid verification code");

        LogStep("Step - 1", "Go to http://westminster.myrgo-preprod.ctt.co.uk/ and click 'Personal' sub menu from 'Log in' menu");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide Email address or CLI and Vehicle on account & Click 'Next'");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.next(), "Click Next");

        LogStep("Step - 4", "Provide an incorrect verification code and click 'next'");
        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(incorrectVerificationCode), String.format("Enter Verification code -> %s", incorrectVerificationCode));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");
        CheckContains(forgetPasswordVerificationCode.getErrorNotificationText(), "Invalid verification code", "Check error message");
    }

    @Test
    public void resetPasswordWithIncorrectPasswordFormat() throws SQLException {
        String wrongPassword = RandomStringUtils.randomAlphabetic(1);

        StartTest("20525", "Resetting the password with incorrect password format");

        LogStep("Step - 1", "Go to http://westminster.myrgo-preprod.ctt.co.uk/ and click 'Personal' sub menu from 'Log in' menu");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide Email address or CLI and Vehicle on account & Click 'Next'");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.next(), "Click Next");

        LogStep("Step - 4", "Provide the correct verification code (Obtain correct code from the Database (dbo.RingGo_Members_Reset) and click 'Next'");
        int code = getPasswordResetCode(email, connection);

        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %d", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");

        LogStep("Step - 5", "Enter one character in the 'New Password' and 'Confirm password'");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.pasteNewPassword(wrongPassword), String.format("Enter new password -> %s", wrongPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(wrongPassword), String.format("Enter confirm password -> %s", wrongPassword));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        CheckContains(resetPasswordNewPassword.getPasswordErrorText(),
                "\"Password must be at least eight characters long, and contain at\nleast one uppercase letter, one lowercase letter and at least one number",
                "Check error message");
    }

    @Test
    public void resetPasswordWithIncorrectConfirmPassword() throws SQLException {
        String newPassword = RandomStringUtils.randomAlphabetic(4).toLowerCase() + RandomStringUtils.randomAlphabetic(4).toUpperCase() + RandomStringUtils.randomNumeric(4);
        String wrongConfirmPassword = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
        int questionIndex = 2;
        String answer = RandomStringUtils.randomAlphabetic(5);

        StartTest("20526", "Resetting the password with incorrect confirm password");

        LogStep("Step - 1", "Go to http://westminster.myrgo-preprod.ctt.co.uk/ and click 'Personal' sub menu from 'Log in' menu");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide Email address or CLI and Vehicle on account & Click 'Next'");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.next(), "Click Next");

        LogStep("Step - 4", "Provide the correct verification code (Obtain correct code from the Database (dbo.RingGo_Members_Reset) and click 'Next'");
        int code = getPasswordResetCode(email, connection);

        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %d", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");

        LogStep("Step - 5", "Enter a password matches all criteria (least eight characters long," +
                " and contain at least one uppercase letter, one lowercase letter and at least one number) and enter a wrong password in 'Confirm Password' and click 'Next'");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.pasteNewPassword(newPassword), String.format("Enter new password -> %s", newPassword));
        CheckResult(resetPasswordNewPassword.pasteConfirmPassword(wrongConfirmPassword), String.format("Enter confirm password -> %s", wrongConfirmPassword));
        CheckResult(resetPasswordNewPassword.chooseQuestionByIndex(questionIndex), String.format("Choose question with index -> %s", questionIndex));
        CheckResult(resetPasswordNewPassword.pasteSecretAnswer(answer), String.format("Enter answer -> %s", answer));
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        CheckContains(resetPasswordNewPassword.getErrorNotificationText(), "Password and confirm password are not the same", "Check error notification text");
    }

    @Test
    public void resetPasswordWithEmptyPasswordInputs() throws SQLException {

        StartTest("15163", "Resetting the password with empty password inputs");

        LogStep("Step - 1", "Go to http://westminster.myrgo-preprod.ctt.co.uk/ and click 'Personal' sub menu from 'Log in' menu");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 2", "Click 'I've forgotten my password'");
        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        CheckResult(wccloginpage.clickForgetPassword(), "Click Forget password link");

        LogStep("Step - 3", "Provide Email address or CLI and Vehicle on account & Click 'Next'");
        ForgetPassword forgetPassword = new ForgetPassword(_driver);
        CheckResult(forgetPassword.pasteUserIdentifier(email), String.format("Paste user email/CLI -> %s", email));
        CheckResult(forgetPassword.pasteVehicleVrn(vehicleVrn), String.format("Paste Vehicle VRN -> %s", vehicleVrn));
        CheckResult(forgetPassword.next(), "Click Next");

        LogStep("Step - 4", "Provide the correct verification code (Obtain correct code from the Database (dbo.RingGo_Members_Reset) and click 'Next'");
        int code = getPasswordResetCode(email, connection);

        ForgetPasswordVerificationCode forgetPasswordVerificationCode = new ForgetPasswordVerificationCode(_driver);
        CheckResult(forgetPasswordVerificationCode.pasteVerificationCode(Integer.toString(code)), String.format("Enter Verification code -> %d", code));
        CheckResult(forgetPasswordVerificationCode.next(), "Click Next");

        LogStep("Step - 5", "Empty both 'New Password' & 'Confirm Password' and click 'Next'");
        ResetPasswordNewPassword resetPasswordNewPassword = new ResetPasswordNewPassword(_driver);
        CheckResult(resetPasswordNewPassword.next(), "Click Next");
        CheckContains(resetPasswordNewPassword.getPasswordErrorText(), "New Password is required", "Check Password error text");
        CheckContains(resetPasswordNewPassword.getConfirmPasswordErrorText(), "Confirm Password is required", "Check confirm password error text");
    }

    @AfterClass
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
