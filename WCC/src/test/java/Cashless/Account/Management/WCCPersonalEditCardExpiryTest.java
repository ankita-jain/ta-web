package Cashless.Account.Management;

import static GenericComponents.helper.RestApiHelper.addNewPaymentCard;

import java.time.LocalDate;

import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.enums.Month;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCEditPaymentDetailsPage;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCPersonalEditCardExpiryTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }

    @Test(enabled=true)
    public void editCard() throws Exception {
        String cardNumber = wccproperties.getProperty("PAYMENT_CARD_NUMBER");
        String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        Month expMonth = Month.values()[RandomUtils.nextInt(0, 12)];
        String expYearForEdit = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5)).substring(2);
        Month expMonthForEdit = Month.values()[RandomUtils.nextInt(0, 12)];
        String lastFourDigitOfCardNumber = cardNumber.substring(12);
        
        addNewPaymentCard(email, password, cardNumber, expMonth.getMonthNumber() + "-" + expYear);

        StartTest("20431", "Editing a Card - Positive Test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");

        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);

        CheckResult(wccpaymentdetailspage.clickPaymentCardEdit(lastFourDigitOfCardNumber), "Click payment card edit");

        WCCEditPaymentDetailsPage wcceditpaymentdetailspage = new WCCEditPaymentDetailsPage(_driver);

        CheckResult(wcceditpaymentdetailspage.selectCardExpiryMonth(expMonthForEdit.toString()), String.format("Select card Expiry Month -> %s", expMonthForEdit.toString()));
        CheckResult(wcceditpaymentdetailspage.selectCardExpiryYear(expYearForEdit), String.format("Select card Expiry year -> %s", expYearForEdit));
        CheckResult(wcceditpaymentdetailspage.clickSaveButton(), "Click save button");

        CheckContains(wccpaymentdetailspage.getSuccessNotification(),
                "card ending " + lastFourDigitOfCardNumber + " was successfully edited", "Successfully add card message");

        String monthAndYearOfCard = expMonthForEdit.getMonthNumber() + "/" + expYearForEdit;

        CheckContains(wccpaymentdetailspage.getCardExpDate(lastFourDigitOfCardNumber), monthAndYearOfCard, "Checking Month and year of the added card");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}
