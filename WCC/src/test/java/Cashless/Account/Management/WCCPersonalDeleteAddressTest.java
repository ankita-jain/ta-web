package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyAddressPage;
import PageObjects.WCCPersonalDeleteConfirmationPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static GenericComponents.helper.RestApiHelper.addNewAddress;

public class WCCPersonalDeleteAddressTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void deleteAddress(String TCID, Map<Object, Object> Data) throws IOException {
        Map<Object,Object> address = RestApiHelper.buildAddress(Data);
        addNewAddress(email, password, address);
        String deleteLine = Data.get("addressToDeleteLine").toString();
        address.put("AddressLine1", deleteLine);
        address.put("Type", Data.get("addressTypeToDelete").toString());
        addNewAddress(email, password, address);

        StartTest(TCID, "Delete Address - Positive test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickAddress(), "Click addresses button");

        WCCMyAddressPage wccmyaddresspage = new WCCMyAddressPage(_driver);
        CheckResult(wccmyaddresspage.clickDeleteAddress(deleteLine), String.format("Click delete address -> %s", deleteLine));

        WCCPersonalDeleteConfirmationPage wccPersonalDeleteConfirmationPage = new WCCPersonalDeleteConfirmationPage(_driver);
        CheckResult(wccPersonalDeleteConfirmationPage.clickDeleteConfirmButton(), "Click Yes");

        CheckContains(wccmyaddresspage.getSuccessNotification(), "successfully deleted", "Check my Address success notification message");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}
