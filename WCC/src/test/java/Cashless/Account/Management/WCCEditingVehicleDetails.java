package Cashless.Account.Management;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCEditVehicleDetails;
import PageObjects.WCCHome;
import PageObjects.WCCMyVehiclesPage;
import PageObjects.myringo.AddVehiclePage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WCCEditingVehicleDetails extends RingGoScenario {
    private String petrolCarVrn;

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        petrolCarVrn = vehicleVrn;
        RestApiHelper.setCarType(email, password, "Car");
    }

    @Test
    public void verifyEditingVehicleDetails() {
        String colorForChanging = AddVehiclePage.Colour.Maroon.toString();

        StartTest("134139", "Verify editing vehicle details");

        LogStep("Step - 1", "Go to 'Account' > 'Vehicles'");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickVehiclesLink(), "Click on 'Vehicles' menu");

        LogStep("Step - 2", "Click the pen and paper icon");
        WCCMyVehiclesPage wccMyVehiclesPage = new WCCMyVehiclesPage(_driver);
        CheckResult(wccMyVehiclesPage.clickEditVehicle(petrolCarVrn), String.format("Click Edit link where CarVRN -> %s", petrolCarVrn));

        LogStep("Step - 3", "Change the colour. Click Save");
        WCCEditVehicleDetails wccEditVehicleDetails = new WCCEditVehicleDetails(_driver);
        CheckResult(wccEditVehicleDetails.chooseColour(colorForChanging), String.format("Choose %s colour from dropdown", colorForChanging));
        CheckResult(wccEditVehicleDetails.clickSave(), "Click Save button");

        LogStep("Step - 4", "Check the vehicle just updated on the table");
        CheckBool(wccMyVehiclesPage.getTable().getCellValue(wccMyVehiclesPage.getTable().getRowIndexByCellValue(petrolCarVrn, WCCMyVehiclesPage.MyVehicleTable.VEHICLE),
                WCCMyVehiclesPage.MyVehicleTable.COLOUR).equalsIgnoreCase(colorForChanging),
                "Check text in the colour cell");
    }
}
