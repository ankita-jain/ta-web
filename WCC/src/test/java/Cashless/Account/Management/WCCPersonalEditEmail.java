package Cashless.Account.Management;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;
import Utilities.DateUtils;

public class WCCPersonalEditEmail extends RingGoScenario {
    private String newEmail;

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");;
        newEmail = DateUtils.TimeStamp("unix") + "@mail.com";
    }
    //TODO - remove hardcoded test data
    @Test
    public void personalEmailEditing() {
        StartTest("134124", "Edit user's email");

        NavigationHelper.openWestMinster(_driver);
        

        LogStep("1", "Click 'log in' > 'personal'");
        LogStep("2", "Enter {email} or {CLI} into \"Phone or email\" input");
        LogStep("3", "Enter {password} into \"Password or PIN\" input for current user");
        LogStep("4", "Click \"Log in\" button");

        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        LogStep("5", "Go to \"Account\" -> \"My details\"");
        CheckResult(wccHome.clickMyDetails(), "Click my details button");

        WCCContactDetailsPage wcccontactdetailspage = new WCCContactDetailsPage(_driver);
        CheckResult(wcccontactdetailspage.enterMemberFirstName(randomAlphabetic(5)), "Enter first name");
        CheckResult(wcccontactdetailspage.enterMemberSurname(randomAlphabetic(5)), "Enter surname");
        LogStep("6", "Enter {newrandomemail} value to \"Email address\" input");
        CheckResult(wcccontactdetailspage.enterMemberEmailAddress(newEmail), "Enter new email");
        LogStep("7", "Enter {newrandomemail} value to \"Confirm Email address\" input");
        CheckResult(wcccontactdetailspage.enterConfirmMemberEmailAddress(newEmail), "Enter confirm email address");
        LogStep("8", "Click \"Save\" button");
        CheckResult(wcccontactdetailspage.clickSaveButton(), "Click save button");

        LogStep("9", "Go to Account -> Logout");
        CheckResult(wccHome.logout(), "Click logout button");

        LogStep("10", "Click \"Log in\" > \"Personal\"");
        LogStep("11", "Enter {newrandomemail} value to \"Phone or email\" input");
        LogStep("12", "Enter {password} into \"Password or PIN\" input for current user");
        LogStep("13", "Click \"Log in\" button");
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("14", "Go to \"Account\" -> \"My details\"");
        CheckResult(wccHome.clickMyDetails(), "Click my details button");
        LogStep("15", "Get value from \"Email address\" input");
        CheckBool(wcccontactdetailspage.getMemberEmailValue().equals(newEmail),
                "Check new email presents in 'Contact Details' page");
    }
}
