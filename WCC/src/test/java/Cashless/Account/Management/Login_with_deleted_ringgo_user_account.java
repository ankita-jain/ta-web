package Cashless.Account.Management;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.myringo.HelpPage;
import PageObjects.myringo.HomePageMyRinggo;

public class Login_with_deleted_ringgo_user_account extends RingGoScenario {
    
	@BeforeMethod
    public void setUp() throws Exception {
        createUser("defaultNewUser.json");
    }
    //TODO - remove hardcoded test data
    @Test
    public void ringGoDeleteUserandLoginBack() throws Exception {
    	
    	StartTest("134104", "Test that a deleted user (Member Status is 5) cannot be logged in again");
    	
    	LogStep("Step - 1", "Open RingGoWeb");
    	NavigationHelper.openMyRingo(_driver);
    	
    	LogStep("Step - 2", "Enter login and password");
    	LoginHelper.loginMyRingo(email, password, _driver);
    	
    	LogStep("Step - 3", "Click on Help");
    	HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
    	CheckResult(homePageMyRinggo.clickHelpLink(), "Click Help");
    	
    	LogStep("Step - 4", "Click on Account Closure Request");
    	HelpPage helpPage = new HelpPage(_driver);
    	helpPage.clickAccountClosureRequest();
    	
    	LogStep("Step - 5", "Give reason for Account Closure ");
    	helpPage.selectAccountClosureReason("I had trouble registering my vehicle");
    	
    	LogStep("Step - 6", "Click on Next ");
    	helpPage.clickNext();
    	
    	LogStep("Step - 7", "Confirm Account Closure ");
    	helpPage.confirmClosure();
    	
    	LogStep("Step - 8", "Click on Next after Confirming ");
    	helpPage.confirmClosureNext();
    	
    	LogStep("Step - 9", "Check for Success Message for Account Closure");
    	CheckContains(helpPage.getAccountClosureSuccessText(),"Your account has now been closed. In order to use the RingGo service in the future you will need to re-register.","AccountClosureSuccessText");
    	
    	LogStep("Step - 10", "Click on Finish");
    	helpPage.clickFinish();
    	
    	LogStep("Step - 11", "Enter the same login and password");
    	LoginHelper.loginMyRingo(email, password, _driver);
    	CheckContains(homePageMyRinggo.getErrorMessageText(),"Login details are incorrect please check they are correct or try entering your mobile number","error message");    	
    }

}
