package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyAddressPage;
import PageObjects.WCCPersonalDeleteConfirmationPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static GenericComponents.helper.RestApiHelper.addNewAddress;

public class WCCPersonalDeleteAddressNegativeTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("defaultNewUser.json");    
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void deleteAddress(String TCID, Map<Object, Object> Data) throws IOException {
    	
    	/*
    	 *  Add two addressess to the user account and then try to delete one of them 
    	 */
    	
    	Map<Object,Object> address = RestApiHelper.buildAddress(Data);    	
        addNewAddress(mobile, password, address);
        String addressLine1 = Data.get("addressToDeleteLine").toString();
        address.put("AddressLine1", addressLine1);
        address.put("Type", Data.get("addressTypeToDelete").toString());
        addNewAddress(mobile, password, address);
       
        StartTest(TCID, "Delete Address - Negative test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(mobile, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickAddress(), "Click addresses button");

        WCCMyAddressPage wccmyaddresspage = new WCCMyAddressPage(_driver);
        CheckResult(wccmyaddresspage.clickDeleteAddress(addressLine1), String.format("Click delete address -> %s", addressLine1));

        WCCPersonalDeleteConfirmationPage wccPersonalDeleteConfirmationPage = new WCCPersonalDeleteConfirmationPage(_driver);

        CheckResult(wccPersonalDeleteConfirmationPage.clickDeleteCancelButton(), "Click No");

        CheckContains(wccmyaddresspage.getTextFromPage(), addressLine1, "Check my Address success notification message");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}