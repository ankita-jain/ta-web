package Cashless.Account.Management;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCChangeSecurityQuestion;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;

public class ChangeSecurityQuestion extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {  
        createUser("islingtonNewUser.json");
    }

    //TODO - remove all hardcoded test data
    
    @Test
    public void changeSecurityQuestion() {
        int questionIndex = 4;
        String secretAnswer = RandomStringUtils.randomAlphabetic(10);

        StartTest("134130", "Change security question");

        LogStep("Step - 1", "Log in to http://westminster.myrgo-preprod.ctt.co.uk");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'My Details'");
        WCCHome wccHome = new WCCHome(_driver);
        CheckResult(wccHome.clickMyDetails(), "Click My Details");

        LogStep("Step - 3", "Click 'Change Security Question' above the details table");
        WCCContactDetailsPage wccContactDetailsPage = new WCCContactDetailsPage(_driver);
        CheckResult(wccContactDetailsPage.changeSecurityQuestion(), "Click change security question");

        WCCChangeSecurityQuestion wccChangeSecurityQuestion = new WCCChangeSecurityQuestion(_driver);

        LogStep("Step - 4", "Select the secret question drop down");
        CheckResult(wccChangeSecurityQuestion.chooseQuestionByIndex(questionIndex), String.format("Choose question with index -> %s", questionIndex));

        LogStep("Step - 5", "Select a question");
        CheckResult(wccChangeSecurityQuestion.pasteSecretAnswer(secretAnswer), String.format("Enter secret answer -> %s", secretAnswer));

        LogStep("Step - 6", "Add your password");
        CheckResult(wccChangeSecurityQuestion.pastePassword(password), String.format("Enter password -> %s", password));

        LogStep("Step - 7", "Click 'Save'");
        CheckResult(wccChangeSecurityQuestion.clickSave(), "Click Save");
    }
}
