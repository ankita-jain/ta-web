package Cashless.Account.Management;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;

public class EditUsersMobilePhone extends RingGoScenario {

    @BeforeMethod()
    public void setUp() throws Exception {
        createUser("islingtonNewUser.json");
    }
    //TODO - remove hardcoded test data
    @Test
    public void editUsersMobilePhone() {
        StartTest("134126", "Edit user's mobile phone");

        LogStep("Step 1-4", "Click Log In in upper menu -> click Personal. " +
                "Enter {email} or {CLI} into Phone or email input. " +
                "Enter {password} into Password or PIN input for current user." +
                "Click Log in button");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);
        LogStep("Step 5", "Go to Account -> My details");
        WCCHome homePage = new WCCHome(_driver);
        CheckResult(homePage.clickMyDetails(), "Clicking My Details");

        LogStep("Step 6", "Enter {newrandomephone} value to Primary phone number input");
        WCCContactDetailsPage wccContactDetailsPage = new WCCContactDetailsPage(_driver);
        String newPhoneNumber = RandomStringUtils.randomNumeric(13);
        CheckResult(wccContactDetailsPage.enterMemberCLI(newPhoneNumber), String.format("Enter {%s} to Phone number", newPhoneNumber));
        CheckResult(wccContactDetailsPage.enterConfirmMemberEmailAddress(email), "Enter Confirmation email");
        CheckResult(wccContactDetailsPage.enterMemberFirstName("Test"), "Enter First name");
        CheckResult(wccContactDetailsPage.enterMemberSurname("Test"), "Enter Surname");

        LogStep("Step 7", "Click Save button");
        CheckResult(wccContactDetailsPage.clickSaveButton(), "Click Save button");

        LogStep("Step 8", "Go to Account -> Logout");
        CheckResult(homePage.logout(), "Clicking Log out");

        LogStep("Step 9-12", "Click Log in > Personal." +
                "Enter {newrandomephone} value to Phone or email input" +
                "Enter {password} into Password or PIN input for current user" +
                "Click Log in button");
        LoginHelper.loginWestminster(newPhoneNumber, password, _driver);
        CheckContains(homePage.getHomePageTitle(), "Welcome", "Checking Home page is displayed");

        homePage.logout();
    }
}
