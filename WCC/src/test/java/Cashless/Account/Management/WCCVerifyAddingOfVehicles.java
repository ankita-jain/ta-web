package Cashless.Account.Management;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCAddVehiclePage;
import PageObjects.WCCIndexPage;
import PageObjects.WCCMyVehiclesPage;

public class WCCVerifyAddingOfVehicles extends RingGoScenario {
    
    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void wccVerifyAddingOfVehicles(String TCID, Map<Object, Object> data) {
        StartTest(TCID, data);

        String numberPlate = data.get("number_plate").toString();
        String colour = data.get("colour").toString();
        String make = data.get("make").toString();
        String type =  data.get("type").toString();
     
         
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("Step - 1", "Go to Account -> Vehicles -> Add a vehicle");
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickVehicles(), "Click vehicle button");

        WCCMyVehiclesPage wccMyVehiclesPage = new WCCMyVehiclesPage(_driver);
        CheckResult(wccMyVehiclesPage.clickAddAVehicleLink(), "Click Add a Vehicle link");

        LogStep("Step - 2", "Provide the mandatory details in 'Add a Vehicle' page. Enter a VRN.");
        if(numberPlate != null && !numberPlate.isEmpty())
        CashlessHelper.fillMandatoryFieldsAddVehicle(numberPlate, colour, make, type, _driver);

        LogStep("Step - 3", "Click on Save");
        WCCAddVehiclePage addVehiclePage = new WCCAddVehiclePage(_driver);
        addVehiclePage.clickSave();
        
        //Assert success message
        if(data.get("Type").toString().equals("Positive"))
        CheckBool(wccMyVehiclesPage.getTable().IsRowWithValueExists(numberPlate, WCCMyVehiclesPage.MyVehicleTable.VEHICLE), "Vehicle with number " + numberPlate + " exists");
        
        //Assert error message
        else if(data.get("Type").toString().equals("Negative")) {
        	
        	//Repeat test & assert error message
        	if((data.get("Iteration").toString().equals("2"))) {
        		
        		LogStep("Step - 4", "Using steps 1, 2 and 3, Try to add the Vehicle with same VRN again( keep all other details different)");
        		CheckResult(wccMyVehiclesPage.clickAddAVehicleLink(), "Click Add a Vehicle link");
                CashlessHelper.fillMandatoryFieldsAddVehicle(numberPlate, colour, make, type, _driver);
                addVehiclePage.clickSave();
        	}
        	
        	CheckValue(addVehiclePage.getNumberPlateErrorMessageText(), data.get("Error_message").toString(),
                    "Number plate error message");
        }
            
    }

}
