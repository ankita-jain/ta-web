package Cashless.Account.Management;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCEditPaymentDetailsPage;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCPersonalEditPrimaryCardTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        System.out.println(email);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void editCard(String TCID, Map<String, String> data)  {
       
        String lastFourDigitOfCardNumber = data.get("primary_card").substring(12);
        String expYearLastTwoDigits = data.get("new_expiry_year").substring(2);
        String maskedCardNumber = "************" + lastFourDigitOfCardNumber;
        String exp_month = data.get("new_expiry_month");
        String formattedMonthAndYear= exp_month+"/"+expYearLastTwoDigits;        
        
       
        StartTest(TCID, "Editing the primary Card");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");
        
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        CheckResult(wccpaymentdetailspage.clickPaymentCardEdit(lastFourDigitOfCardNumber), String.format("Enter card number -> %s", lastFourDigitOfCardNumber));
        
        WCCEditPaymentDetailsPage wcceditpaymentdetailspage = new WCCEditPaymentDetailsPage(_driver);
        CheckContains(wcceditpaymentdetailspage.getPaymenPageTitle(), "Edit Payment Details", "Checking title");
        CheckResult(wcceditpaymentdetailspage.selectCardExpiryMonthByValue(exp_month), "Selecting expiry month");
        CheckResult(wcceditpaymentdetailspage.selectCardExpiryYear(expYearLastTwoDigits), "Selecting expiry year");
        CheckResult(wcceditpaymentdetailspage.clickSaveButton(), "Selecting expiry year");
    
        CheckContains(wccpaymentdetailspage.getSuccessNotification(),
                "card ending " + lastFourDigitOfCardNumber + " was successfully edited", "Check Successfully edit card message");

        CheckContains(wccpaymentdetailspage.getCardExpDate(lastFourDigitOfCardNumber), formattedMonthAndYear, "Checking Month and year of the added card");
        CheckContains(wccpaymentdetailspage.getCardNumber(lastFourDigitOfCardNumber), maskedCardNumber, "Checking Card is encrypted");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}
