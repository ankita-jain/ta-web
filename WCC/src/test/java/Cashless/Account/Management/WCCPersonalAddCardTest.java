package Cashless.Account.Management;

import java.time.LocalDate;

import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCPersonalAddCardTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }
    //TODO - remove all hardcoded test data
    @Test
    public void addCard() throws Exception {
        String cardNumber = wccproperties.getProperty("PAYMENT_CARD_NUMBER");
        String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        int expMonth = RandomUtils.nextInt(1, 12);
        String lastFourDigitOfCardNumber = cardNumber.substring(12);
        String expYearLastTwoDigits = expYear.substring(2);
        String maskedCardNumber = "************" + lastFourDigitOfCardNumber;
        String formattedMonthAndYear=expMonth+"/"+expYearLastTwoDigits;        
        String formattedMonth=((expMonth < 10)? String.format("%02d", expMonth):Integer.toString(expMonth));
        String monthAndYearOfCard = formattedMonth +expYearLastTwoDigits;
       
        StartTest("134145", "Adding a Card - Positive Test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");
        
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
                
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.enterCardNumber(cardNumber), String.format("Enter card number -> %s", cardNumber));
        CheckResult(wccaddpaymentcardpopup.enterCardExpires(monthAndYearOfCard), String.format("Enter card expires month and year -> %s", monthAndYearOfCard));
        CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
   
        CheckContains(wccpaymentdetailspage.getSuccessNotification(),
                "Visa card ending " + lastFourDigitOfCardNumber + " was successfully added", "Check Successfully add card message");

        CheckContains(wccpaymentdetailspage.getCardExpDate(lastFourDigitOfCardNumber), formattedMonthAndYear, "Checking Month and year of the added card");
        CheckContains(wccpaymentdetailspage.getCardNumber(lastFourDigitOfCardNumber), maskedCardNumber, "Checking Card is encrypted");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}
