package Cashless.Account.Management;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.enums.Month;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCEditPaymentDetailsPage;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCAddingAnExpiredCard extends RingGoScenario {
    private static final String ERROR_MESSAGE = "The expiry date must be set to a date in the future.";
    private static final String MONTH = Month.JAN.getMonthName();

    @BeforeMethod()
    public void setUp() throws Exception {
        createUser("islingtonNewUser");
    }
    // TODO - remove all hardcoded test data
    @Test(enabled=false)
    public void addingAnExpiredCard() {
        StartTest("134146", "Verify Booking/Extending of the session through desktop site");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("1", "Go to Account -> Payment Cards");
        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment");

        LogStep("2", "Click 'Add a new card'");
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();

        WCCEditPaymentDetailsPage wccEditPaymentDetailsPage = new WCCEditPaymentDetailsPage(_driver);
        LogStep("3", "Enter a valid card number");
        CheckResult(wccEditPaymentDetailsPage.enterCardNumber(userCardNumber), "Enter card number");

        LogStep("4", "Choose a month in the past");
        CheckResult(wccEditPaymentDetailsPage.selectCardExpiryMonth(MONTH), "Select card expire month");

        CheckResult(wccEditPaymentDetailsPage.clickSaveButton(), "Click Save button");

        CheckBool(wccEditPaymentDetailsPage.getErrorInExpireDateSectionAsAText().equals(ERROR_MESSAGE), "Check error message");
    }
}
