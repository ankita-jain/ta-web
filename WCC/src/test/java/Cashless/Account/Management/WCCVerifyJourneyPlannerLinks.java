package Cashless.Account.Management;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCTrafficAndPublicTransportPage;

public class WCCVerifyJourneyPlannerLinks extends RingGoScenario {
    @Test
    public void verifyJourneyPlannerLinks() {
        StartTest("134155", "Verify Journey Planner Links");
        NavigationHelper.openWestMinster(_driver);
        WCCHome wccHome = new WCCHome(_driver);
        WCCTrafficAndPublicTransportPage WCCTrafficAndPublicTransportPage = new WCCTrafficAndPublicTransportPage(_driver);

        LogStep("Step - 1", "Go to Traffic and Public Transport");
        CheckResult(wccHome.getPermitLinkContainersInLine().clickOnTrafficAndPublicTransportLink(),
                "Click on 'Traffic and public' link");

        LogStep("Step - 2", "Click on Traffic news");
        CheckResult(WCCTrafficAndPublicTransportPage.clickTrafficNewsLink(), "Click of 'Traffic news' link");
        switchToANewTabAndReturnBack("https://tfl.gov.uk/traffic/status/");

        LogStep("Step - 3", "Click on Bus Timetable and Routes");
        CheckResult(WCCTrafficAndPublicTransportPage.clickBusTimetableAndRoutersLink(),
                "Click on 'Bus time table and routers' link");
        switchToANewTabAndReturnBack("https://tfl.gov.uk/modes/buses/");

        LogStep("Step - 3", "Click on Tube, DLR and Overground");
        CheckResult(WCCTrafficAndPublicTransportPage.clickTubeDlrLink(),
                "Click on 'Tube, DLR and Overground' link");
        switchToANewTabAndReturnBack("https://tfl.gov.uk/tube-dlr-overground/status/");

        LogStep("Step - 4", "Click on How to get around");
        CheckResult(WCCTrafficAndPublicTransportPage.clickHowToGetAroundLink(),
                "Click on 'How to Get Around' link");
        switchToANewTabAndReturnBack("https://tfl.gov.uk/");
    }
}
