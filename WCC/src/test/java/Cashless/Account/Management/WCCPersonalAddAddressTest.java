package Cashless.Account.Management;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCAddAddressPage;
import PageObjects.WCCHome;
import PageObjects.WCCMyAddressPage;

public class WCCPersonalAddAddressTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void addAddress(String TCID, Map<Object, Object> Data) throws IOException {
        StartTest(TCID, "Add Address - Positive test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickAddress(), "Click addresses button");

        WCCMyAddressPage wccmyaddresspage = new WCCMyAddressPage(_driver);
        CheckResult(wccmyaddresspage.clickAddAddressButton(), "Click add address button");

        WCCAddAddressPage wccaddaddresspage = new WCCAddAddressPage(_driver);

        CheckResult(wccaddaddresspage.selectAddressType(Data.get("addressType").toString()), String.format("Select address type -> %s", Data.get("addressType").toString()));
        CheckResult(wccaddaddresspage.enterAddressLine(Data.get("addressLine").toString()), String.format("Enter address -> %s", Data.get("addressLine").toString()));
        CheckResult(wccaddaddresspage.enterTown(Data.get("town").toString()), String.format("Enter town -> %s", Data.get("town").toString()));
        CheckResult(wccaddaddresspage.selectCounty(Data.get("county").toString()), String.format("Select county type -> %s", Data.get("county").toString()));
        CheckResult(wccaddaddresspage.enterPostcode(Data.get("postcode").toString()), String.format("Enter postcode -> %s", Data.get("postcode").toString()));
        CheckResult(wccaddaddresspage.clickSaveButton(), "Click Save button");

        CheckContains(wccmyaddresspage.getSuccessNotification(), "successfully added", "My Address success notification message");

        CheckResult(wcchome.logout(), "Click logout button");
    }
}
