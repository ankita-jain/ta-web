package Cashless.Account.Management;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;
import Utilities.DateUtils;

public class WCCEditUserEmailTest extends RingGoScenario {
    private String firstUserEmail;
    private String firstUserPassword;

    private String secondUserEmail;

    @BeforeMethod
    public void setUp() throws Exception {
    	
    	createUser("defaultUserData");
        
        firstUserEmail = email;
        firstUserPassword = password;
        
        secondUserEmail =  DateUtils.TimeStamp("unix")+"@mail.com";
    }
    // TODO - remove hardcoded test data
    @Test
    public void editUsersEmailWithAlreadyExistingOne() {
        StartTest("134124", "Edit users firstUserEmail with new email id");

        NavigationHelper.openWestMinster(_driver);

        LogStep("1", "Click 'log in' > 'personal'");
        LogStep("2", "Enter {firstUserEmail} or {CLI} into \"Phone or firstUserEmail\" input");
        LogStep("3", "Enter {firstUserPassword} into \"Password or PIN\" input for current user");
        LogStep("4", "Click \"Log in\" button");

        LoginHelper.loginWestminster(firstUserEmail, firstUserPassword, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        LogStep("5", "Go to \"Account\" -> \"My details\"");
        CheckResult(wccHome.clickMyDetails(), "Click my details button");

        WCCContactDetailsPage wcccontactdetailspage = new WCCContactDetailsPage(_driver);
        CheckResult(wcccontactdetailspage.enterMemberFirstName(randomAlphabetic(5)), "Enter first name");
        CheckResult(wcccontactdetailspage.enterMemberSurname(randomAlphabetic(5)), "Enter surname");

        LogStep("6", "Enter {alreadyexisitngemail} value to \"Email address\" input");

        CheckResult(wcccontactdetailspage.enterMemberEmailAddress(secondUserEmail), "Enter new email id");

        LogStep("7", "Enter {alreadyexisitngemail} value to \"Confirm Email address\" input");
        CheckResult(wcccontactdetailspage.enterConfirmMemberEmailAddress(secondUserEmail), "Confirm email id");
        LogStep("8", "Click \"Save\" button");

        CheckResult(wcccontactdetailspage.clickSaveButton(), "Click save button");
        CheckResult(wccHome.logout(), "Logout from WCC");
        
        
        LogStep("9", "Use new email id to login to wcc");
        LoginHelper.loginWestminster(secondUserEmail, firstUserPassword, _driver);
        
        CheckResult(wccHome.logout(), "Logout from WCC");
    
    }
}
