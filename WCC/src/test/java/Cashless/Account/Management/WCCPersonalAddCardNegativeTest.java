package Cashless.Account.Management;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCPersonalAddCardNegativeTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }
    
    //TODO - remove all hardcoded test data
    
    @Test
    public void clickAddCardWithoutAnyCardNumberAndExpiriesEmpty()
    {
    	StartTest("134144", "Adding a Card - Negative Test");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");
        
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
        
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
        
        CheckContains(wccaddpaymentcardpopup.getCardNumberErrorMessage(), "Please enter a valid card number", "Checking Card Number error message");
        CheckContains(wccaddpaymentcardpopup.getCardExpiryErrorMessage(), "Invalid date", "Checking Card Expries error message");
        CheckResult(wccaddpaymentcardpopup.clickAddPaymentPopupWindowClose(), "Click add new card");
        CheckResult(wcchome.logout(), "Click logout button");

    }
    
    @Test
    public void addPaymentCardWithNoExpiresDate() throws IOException
    {
		String cardNumber = wccproperties.getProperty("PAYMENT_CARD_NUMBER");
    	StartTest("134144", "Adding a Card - Negative Test");

    	NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");
        
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
        
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.enterCardNumber(cardNumber), String.format("Enter card number -> %s", cardNumber));
        CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
        CheckContains(wccaddpaymentcardpopup.getCardExpiryErrorMessage(), "Invalid date", "Checking Card Expries error message");
        CheckResult(wccaddpaymentcardpopup.clickAddPaymentPopupWindowClose(), "Click add new card");
        CheckResult(wcchome.logout(), "Click logout button");
        
    }
    
    @Test
    public void checkingNoExpiriesDateMessage()
    {
    	String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        int expMonth = RandomUtils.nextInt(1, 12);
        String expYearLastTwoDigits = expYear.substring(2);
        String formattedMonth=((expMonth < 10)? String.format("%02d", expMonth):Integer.toString(expMonth));
        String monthAndYearOfCard = formattedMonth +expYearLastTwoDigits;
        
    	StartTest("134144", "Adding a Card - Negative Test");

    	NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");
        
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
        
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.enterCardExpires(monthAndYearOfCard), String.format("Enter card expires month and year -> %s", monthAndYearOfCard));
        CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
        
        CheckContains(wccaddpaymentcardpopup.getCardNumberErrorMessage(), "Please enter a valid card number", "Checking Card Number error message");
        CheckResult(wccaddpaymentcardpopup.clickAddPaymentPopupWindowClose(), "Click add new card");
        CheckResult(wcchome.logout(), "Click logout button");
    }
    
    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void addMoreThanThreeCards(String TCID, Map<String, String> data)
    {
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Second_card"), data.get("Second_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Third_card"), data.get("Third_card_expiries"));
    	
    	StartTest(TCID, "Adding a Card - Negative Test");
    	
    	NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");
         
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        CheckBool(!wccpaymentdetailspage.isAddNewCardVisible(), "Checking Add new card visible or not");
        CheckResult(wcchome.logout(), "Click logout button");
    }

}
