package Cashless.Account.Management;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;

public class EditUsersMobilePhoneWithAlreadyExistingOne extends RingGoScenario {

    @BeforeMethod()
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }
    // TODO - remove all hardcoded test data

    @Test
    public void editUsersMobilePhoneWithAlreadyExistingOne() {
        StartTest("134127", "Edit user's mobile phone with already existing one");

        LogStep("Step 1-4", "Click Log In in upper menu -> click Personal. " +
                "Enter {email} or {CLI} into Phone or email input. " +
                "Enter {password} into Password or PIN input for current user." +
                "Click Log in button");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        LogStep("Step 5", "Go to Account -> My details");
        WCCHome homePage = new WCCHome(_driver);
        CheckResult(homePage.clickMyDetails(), "Clicking My Details");

        LogStep("Step 6", "Enter {alreadyexisitngphone} value to Primary phone number input");
        WCCContactDetailsPage wccContactDetailsPage = new WCCContactDetailsPage(_driver);
        CheckResult(wccContactDetailsPage.enterMemberCLI(mobile), String.format("Enter {%s} to Phone number", mobile));
        CheckResult(wccContactDetailsPage.enterConfirmMemberEmailAddress(email), "Enter Confirmation email");
        CheckResult(wccContactDetailsPage.enterMemberFirstName("Test"), "Enter First name");
        CheckResult(wccContactDetailsPage.enterMemberSurname("Test"), "Enter Surname");

        LogStep("Step 7", "Click Save button");
        CheckResult(wccContactDetailsPage.clickSaveButton(), "Click Save button");

        CheckValue(wccContactDetailsPage.getNotificationMessage(), "There is an error in the form. Please check for details below.", "Checking notification message");
        CheckValue(wccContactDetailsPage.getErrorMessages().get(0).getText(), "Phone number already registered. Please contact us for help", "Checking error message");

        homePage.logout();
    }
}
