package Cashless.Account.Management;

import static SeleniumHelpers.WaitMethods.WaitVisible;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyVehiclesPage;


public class WCCDeletingVehicleTest extends RingGoScenario {
    private String carVrn;
    private String vehicleID;

    @BeforeMethod()
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        carVrn = vehicleVrn;

        vehicleID = RestApiHelper.getVehicleId(RestApiHelper.getInfoAboutVehicle(email, password));
    }
    //TODO - remove hardcoded test data
    @Test
    public void deletingVehicle() {
        StartTest("134137", "Verify deleting of vehicles");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        wccHome.clickVehiclesLink();

        WCCMyVehiclesPage wccMyVehiclesPage = new WCCMyVehiclesPage(_driver);
        CheckBool(wccMyVehiclesPage.getTable().getCellValue(1, WCCMyVehiclesPage.MyVehicleTable.VEHICLE).equals(carVrn),
                "Check that table contains correct car's VRN");
        CheckResult(wccMyVehiclesPage.clickRemoveVehicle(vehicleID), "Click remove vehicle");
        CheckResult(wccMyVehiclesPage.getConfirmDeletionForm().clickYesButton(), "Click 'Yes' button on confirmation Deletion stage");

        CheckBool(wccMyVehiclesPage.getSuccessNotificationText().getText().equals(String.format("Vehicle %s was deleted successfully", carVrn)),
                "Check notification message");

        CheckBool(!WaitVisible(_driver, 5, wccMyVehiclesPage.getTable().getTableAsAnElement()), "Check that table doesn't contain vehicle after deleting");
    }

}