package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.Corporate_Settings;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class WCCRaiseAnEnquiryTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Corporate_Settings.class);
    private String username;
    private String password; 
    private String query;
    
    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void raiseEnquiry(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Raising an enquiry");
        
        username = data.get("Username");
        password = data.get("Password");
        query = data.get("Query");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Checking WCC Login page title", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Checking WCC Home page title", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickHelp(), "Click help button");
            

            WCCHelpCentrePage wcchelpcentrepage = new WCCHelpCentrePage(_driver);
            assertTrue("Checking Help page title", wcchelpcentrepage.getHelpPageTitle().contains("Help centre"));
            wcchelpcentrepage.clickEnquiries();
            wcchelpcentrepage.clickRaiseNewEnquiry();
            wcchelpcentrepage.continueRaiseNewEnquiry();

            WCCHelpCentreGeneralEnquiryPage wcchelpcentregeneralenquiry = new WCCHelpCentreGeneralEnquiryPage(_driver);
            assertTrue("Checking Help page title - General Enquiry", wcchelpcentregeneralenquiry.getGeneralEnquiryPageTitle().contains("Help centre - General Enquiry"));
            wcchelpcentregeneralenquiry.submitQueryProcess(query);
            assertTrue("Checking success message", wcchelpcentregeneralenquiry
                    .getQuerySubmitSuccessMessage()
                    .contains("Thank you for contacting us. Your enquiry has been sent, and our staff will get back to you with regard to any queries."));
            wcchelpcentregeneralenquiry.clickFinishButton();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }
}
