package Cashless.Account.Management;
import java.util.Map;

import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.myringo.HelpPage;
import PageObjects.myringo.HomePageMyRinggo;

public class RingGoDeleteUserWithOutstandingDebt extends RingGoScenario {
	private String email;
    private String password;
	

	@Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	public void outstandingDebtUser(String TCID, Map<String, String> data) throws Exception {
		StartTest(TCID, "Test to Ensure user with outstanding debt cannot be deleted");
		email=data.get("email");
		password=data.get("password");
		
		LogStep("Step - 1", "Open RingGoWeb");
		NavigationHelper.openMyRingo(_driver);
		
		LogStep("Step - 2", "Enter login and password");
		LoginHelper.loginMyRingo(email, password, _driver);
		
		LogStep("Step - 3", "Click on Help");
		HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
		CheckResult(homePageMyRinggo.clickHelpLink(), "Click Help");
		
		LogStep("Step - 4", "Click on Account Closure Request");
		HelpPage helpPage = new HelpPage(_driver);
		helpPage.clickAccountClosureRequest();
		
		LogStep("Step - 5", "Give reason for Account Closure ");
		helpPage.selectAccountClosureReason("I had trouble registering my vehicle");
		
		LogStep("Step - 6", "Click on Next ");
		helpPage.clickNext();
		
		LogStep("Step - 7", "Confirm Account Closure ");
		helpPage.confirmClosure();
		
		LogStep("Step - 8", "Click on Next after Confirming ");
		helpPage.confirmClosureNext();
		
		LogStep("Step - 9", "Check for error message");
		CheckContains(helpPage.getAccountClosureText(),"Your account could not be closed because you have active sessions or payments pending.","AccountClosureText");
		
	}

}
