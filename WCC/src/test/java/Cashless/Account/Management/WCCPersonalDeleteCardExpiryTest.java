package Cashless.Account.Management;

import static GenericComponents.helper.RestApiHelper.addNewPaymentCard;

import java.time.LocalDate;

import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.enums.Month;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentDetailsPage;

public class WCCPersonalDeleteCardExpiryTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }
    //TODO - remove hardcoded test data
    @Test
    public void deleteCard() throws Exception {
        String cardNumber = wccproperties.getProperty("PAYMENT_CARD_NUMBER");
        String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        Month expMonth = Month.values()[RandomUtils.nextInt(0, 12)];
        String lastFourDigitOfCardNumber = cardNumber.substring(12);

        StartTest("134149", "Deleting a card - Positive Test");

        addNewPaymentCard(email, password, cardNumber, expMonth.getMonthNumber() + "-" + expYear);

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginWestminster(email, password, _driver);

        WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.clickPayment(), "Click payment button");

        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);

        CheckResult(wccpaymentdetailspage.clickPaymentCardDelete(lastFourDigitOfCardNumber), "Click delete payment card");

        wccpaymentdetailspage.clickDeleteOKButton();
        //CheckResult(wccpaymentdetailspage.clickDeleteOKButton(), "Click 'yes' button");
        CheckContains(wccpaymentdetailspage.getPaymenPageTitle(), "Payment Details", "Check Payment page title");

        CheckResult(wcchome.logout(), "Click logout button");
    }

}