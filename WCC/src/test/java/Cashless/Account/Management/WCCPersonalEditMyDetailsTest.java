package Cashless.Account.Management;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class WCCPersonalEditMyDetailsTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCPersonalEditMyDetailsTest.class);

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void editMyPersonalDetails(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Edit users's contact details");


        try {

        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(data.get("username")), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(data.get("password")), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Home page title", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickMyDetails(), "Click my details button");

            WCCContactDetailsPage wcccontactdetailspage = new WCCContactDetailsPage(_driver);
            assertTrue("Check Contact Details title", wcccontactdetailspage.getContactDetailsPageTitle().contains("Contact Details"));
            wcccontactdetailspage.editMyPersonalDetails(
            		data.get("title"), 
            		data.get("firstname"),
            		data.get("surname"),
            		data.get("email"),
            		data.get("email"),
            		data.get("phone_number"));

            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
