package Cashless.Account.Management;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCContactDetailsPage;
import PageObjects.WCCHome;

public class WCCEditUsersEmailWithAlreadyExistingOne extends RingGoScenario {
    private String firstUserEmail;
    private String firstUserPassword;

    private String secondUserEmail;

    @BeforeMethod
    public void setUp() throws Exception {       
        createUser("islingtonNewUser.json");
        firstUserEmail = email;
        firstUserPassword = password;
        createUser("islingtonNewUser.json");
        secondUserEmail = email;
    }
    //TODO - remove hardcoded test data
    @Test
    public void editUsersEmailWithAlreadyExistingOne() {
        StartTest("134125", "Edit user's firstUserEmail with already existing one");

        NavigationHelper.openWestMinster(_driver);

        LogStep("1", "Click 'log in' > 'personal'");
        LogStep("2", "Enter {firstUserEmail} or {CLI} into \"Phone or firstUserEmail\" input");
        LogStep("3", "Enter {firstUserPassword} into \"Password or PIN\" input for current user");
        LogStep("4", "Click \"Log in\" button");

        LoginHelper.loginWestminster(firstUserEmail, firstUserPassword, _driver);

        WCCHome wccHome = new WCCHome(_driver);
        LogStep("5", "Go to \"Account\" -> \"My details\"");
        CheckResult(wccHome.clickMyDetails(), "Click my details button");

        WCCContactDetailsPage wcccontactdetailspage = new WCCContactDetailsPage(_driver);
        CheckResult(wcccontactdetailspage.enterMemberFirstName(randomAlphabetic(5)), "Enter first name");
        CheckResult(wcccontactdetailspage.enterMemberSurname(randomAlphabetic(5)), "Enter surname");

        LogStep("6", "Enter {alreadyexisitngemail} value to \"Email address\" input");

        CheckResult(wcccontactdetailspage.enterMemberEmailAddress(secondUserEmail), "Enter already existing email");

        LogStep("7", "Enter {alreadyexisitngemail} value to \"Confirm Email address\" input");
        CheckResult(wcccontactdetailspage.enterConfirmMemberEmailAddress(secondUserEmail), "Enter already existing email");
        LogStep("8", "Click \"Save\" button");

        CheckResult(wcccontactdetailspage.clickSaveButton(), "Click save button");

        CheckValue(wcccontactdetailspage.getNotificationMessage(), "There is an error in the form. Please check for details below.",
                "Checking error notification in my details page");

        CheckValue(wcccontactdetailspage.getEmailAddressTextError().getText(), "Email address is in use",
                "Checking error for email address input");

        CheckResult(wccHome.logout(), "Logout from WCC");
    }
}
