package Cashless.Mobile.Site;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.RingGoParkingSessionMobilePage;
import PageObjects.WCCParkingSessionPage;

public class RingGoMobileSessionWithNoCV2Test extends RingGoScenario {
	
	public String vehicleVrn;
	public String zone = "23856";
	public String duration = "1 Hour";
	
	@BeforeMethod
	public void setUp() throws Exception {
		
		createUser("defaultNewUser.json");	
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(mobile, password, RestApiHelper.defaultNewVehicle()); 
        vehicleVrn = vehicleVrn.toUpperCase()+" WHITE BMW";
	}
	//TODO - remove hardcoded test data - should be injected by DP
	
	@Test
	public void buySessionWithNoCV2() {
		
		 StartTest("134205", "Buy a session with no CV2 - RingGo Mobile");

	     LogStep("Step - 1", "Login to RingGo website (https://m.myrgo-preprod.ctt.co.uk)");
	     NavigationHelper.openRingoGoMobile(_driver);	     
	     LogStep("Step - 2", "Click 'Login'. Input 'Mobile number or E-mail' & password and click 'Log In'");
	     LoginHelper.loginMyRingGoMobile(mobile, password, _driver);

	     LogStep("Step - 3", "Click the 'Parking' icon on the home page");
	     RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
	     CheckResult(ringgomobilehomepage.clickParking(), "Click Account tile");
	     
	     LogStep("Step - 4", "Create a parking session at zone (23856) using any VRN and click Next");
	     RingGoParkingSessionMobilePage ringgoparkingsessionmobilepage = new RingGoParkingSessionMobilePage(_driver);
	     CheckResult(ringgoparkingsessionmobilepage.enterZone(zone), "Enter zoneID");
	     CheckResult(ringgoparkingsessionmobilepage.selectVehicle(vehicleVrn), "Click select vehicle");
	     CheckResult(ringgoparkingsessionmobilepage.clickNextButton(), "Click next button");
	     
	     LogStep("Step - 7", "Select '1 Hour' and click 'Next' button");
	     CheckResult(ringgoparkingsessionmobilepage.selectDuration("1 Hour"), "Select duration");	    
	     CheckResult(ringgoparkingsessionmobilepage.clickDurationNextButton(), "Click card select next button");
	     
	     LogStep("Step - 8", "Select the payment card and click 'Next'");      
	     CheckResult(ringgoparkingsessionmobilepage.clickCardSelectNextButton(), "Click card select next button");
	     
	     LogStep("Step - 9", "Click Pay button");      
	     WCCParkingSessionPage wccparkingsessionpage = new WCCParkingSessionPage(_driver);
	     CheckResult(wccparkingsessionpage.clickPay(), "Click pay button");	   
	     CheckResult(ringgoparkingsessionmobilepage.clickFinish(), "Click finish button");
	     CheckResult(ringgomobilehomepage.clickLogout(), "Click logout button");	     
	}

}