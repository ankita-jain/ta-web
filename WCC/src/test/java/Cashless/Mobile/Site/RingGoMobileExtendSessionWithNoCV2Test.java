package Cashless.Mobile.Site;

import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.RingGoParkingSessionMobilePage;
import PageObjects.WCCParkingSessionPage;

public class RingGoMobileExtendSessionWithNoCV2Test extends RingGoScenario {
	
	public String vehicleVrn;	
	public String zoneid = "226688";
	public String hour = "1";
	public String min = "5";
	public String extend_time ="1 Hour";
	public String vehicleID,paymentMethod;
	
	@BeforeMethod
	public void setUp() throws Exception {
		
		createUser("defaultNewUser.json");
        
        
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(mobile, password, RestApiHelper.defaultNewVehicle());
        
        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(mobile, password);
        vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();        
        
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(mobile, password, zoneid);
        paymentMethod = (String) usersPayment.get("Id");

             
	}
	
	//TODO - remove hardcoded test data - should be injected by DP
	
	@Test
	public void extendSession() {
		
		 StartTest("134206", "Extend a session with no CV2 - RingGo Mobile");

	     LogStep("Step - 1", "Login to RingGo website (https://m.myrgo-preprod.ctt.co.uk)");
	     NavigationHelper.openRingoGoMobile(_driver);	
	     LoginHelper.loginMyRingGoMobile(mobile, password, _driver);
	     
	     /*
	      *  For documentation purpose 
	      */
	     
	     LogStep("Step - 2", "Create a parking session at zone (23856) using any VRN and click Next");
	     LogStep("Step - 3", "Select 1-hour duration and click Next button");
	     LogStep("Step - 4", "Select the payment card and click Next");
	     LogStep("Step - 5", "Click Pay button");
	     RestApiHelper.bookSession(mobile, password, paymentMethod, vehicleID, zoneid, hour, min);
	     
	     LogStep("Step - 6", "Wait for 2 mins, and click Parking tile again & select the existing parking session");
	     RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
	     CheckResult(ringgomobilehomepage.clickParking(), "Click Account tile");
	     delay(240000);
	     
	     LogStep("Step - 7", "Create a parking session at zone (23856) using any VRN and click Next");
	     RingGoParkingSessionMobilePage ringgoparkingsessionmobilepage = new RingGoParkingSessionMobilePage(_driver);
	     CheckResult(ringgoparkingsessionmobilepage.selectLastSessionToExtend(), "Select Existing session");
	     CheckResult(ringgoparkingsessionmobilepage.clickExtendNextButton(), "Click 'Next' button");
	     
	     LogStep("Step - 8", "Select '1 Hour' and click 'Next' button");
	     CheckResult(ringgoparkingsessionmobilepage.selectDuration("1 Hour"), "Select duration");	    
	     CheckResult(ringgoparkingsessionmobilepage.clickDurationNextButton(), "Click card select next button");
	     
	     LogStep("Step - 9", "Select the payment card and click 'Next'");      
	     CheckResult(ringgoparkingsessionmobilepage.clickCardSelectNextButton(), "Click card select next button");
	     
	     LogStep("Step - 10", "Click Pay button");      
	     WCCParkingSessionPage wccparkingsessionpage = new WCCParkingSessionPage(_driver);
	     CheckResult(wccparkingsessionpage.clickPay(), "Click pay button");	   
	     CheckResult(ringgoparkingsessionmobilepage.clickFinish(), "Click finish button");
	     CheckResult(ringgomobilehomepage.clickLogout(), "Click logout button");	  
	    
		
	}

}
