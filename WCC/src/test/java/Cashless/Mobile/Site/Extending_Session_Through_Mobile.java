package Cashless.Mobile.Site;

import static Utilities.TimerUtils.delay;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.RingGoMobileIndexPage;
import PageObjects.RingGoParkingSessionMobilePage;
import PageObjects.WCCLoginPage;

public class Extending_Session_Through_Mobile extends RingGoScenario {
    private static final String CVV = "111";
    private static final String CARD_DETAILS = "1111";
    private static final String TIME = "1 Hour";
    private static final String ZONE_ID = "8142";

    private String carVrn;

	@BeforeMethod()
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
        carVrn = vehicleVrn;
    }
    // TODO - move hardcoded data into input JSON

    @Test
    public void ringGoBookingExtendingSessionTest() {
        StartTest("134202", "Verify Booking/Viewing/Extending the session through Mobile Site");

        NavigationHelper.openRingoGoMobile(_driver);
        LogStep("1", "Go to mobile version of MyRingGo and Login");

        RingGoMobileIndexPage ringgomobileindexpage = new RingGoMobileIndexPage(_driver);
        ringgomobileindexpage.clickLogin();

        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);

        CheckResult(wccloginpage.inputEmailOrPhonenumer(email), "Enter email or phone number");
        CheckResult(wccloginpage.enterPassword(password), "Enter password");
        CheckResult(wccloginpage.clickLoginButton(), "Click login button");

        LogStep("2", "Go to Park and Try to Book a session");
        RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
        CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");

        RingGoParkingSessionMobilePage ringgoparkingsessionmobilepage = new RingGoParkingSessionMobilePage(_driver);

        CheckResult(ringgoparkingsessionmobilepage.enterZone(ZONE_ID), "Enter zoneID");
        CheckResult(ringgoparkingsessionmobilepage.selectVehicle(carVrn), "Click select vehicle");
        CheckResult(ringgoparkingsessionmobilepage.clickNextButton(), "Click next button");
        CheckResult(ringgoparkingsessionmobilepage.selectDuration(TIME), "Select duration");
        CheckResult(ringgoparkingsessionmobilepage.selectSMSConfirm(), "Select SMS confirm");
        CheckResult(ringgoparkingsessionmobilepage.selectReminderSMS(), "Select reminder SMS");
        CheckResult(ringgoparkingsessionmobilepage.clickDurationNextButton(), "Click duration next button");

        CheckResult(ringgoparkingsessionmobilepage.clickCardSelectNextButton(), "Click card select next button");
        CheckResult(ringgoparkingsessionmobilepage.clickPay(), ":Click pay button");
        CheckResult(ringgoparkingsessionmobilepage.enterCVV(CVV), "Enter CVV2 code");
      
        CheckResult(ringgoparkingsessionmobilepage.clickPayConfirm(), ":Click pay button");
        CheckResult(ringgoparkingsessionmobilepage.clickFinish(), "Click finish button");

        delay(240000);

        LogStep("3", "Try to Extend a session");
        CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");

        CheckResult(ringgoparkingsessionmobilepage.selectLastSessionToExtend(), "Click on last session to extend it");
        CheckResult(ringgoparkingsessionmobilepage.clickExtendNextButton(), "Click next button extend");


        CheckResult(ringgoparkingsessionmobilepage.selectDuration(TIME), "Select duration");
        CheckResult(ringgoparkingsessionmobilepage.selectSMSConfirm(), "Select SMS confirm");
        CheckResult(ringgoparkingsessionmobilepage.selectReminderSMS(), "Select reminder SMS");
        CheckResult(ringgoparkingsessionmobilepage.clickDurationNextButton(), "Click duration next button");

        CheckResult(ringgoparkingsessionmobilepage.clickCardSelectNextButton(), "Click card select next button");
        CheckResult(ringgoparkingsessionmobilepage.clickPay(), ":Click pay button");
        CheckResult(ringgoparkingsessionmobilepage.enterCVV(CVV), "Enter CVV2 code");
        CheckResult(ringgoparkingsessionmobilepage.clickPayConfirm(), ":Click pay button");
        CheckResult(ringgoparkingsessionmobilepage.clickFinish(), "Click finish button");

        CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");
        LogStep("4", "View an already booked location");

        String startedSessionTime = ringgoparkingsessionmobilepage.getStartedOrExpiresTime(0, "Started").getText();
        String expiresSessionTime = ringgoparkingsessionmobilepage.getStartedOrExpiresTime(1, "Expires").getText();

        CheckBool(cutOnlyDate(startedSessionTime).equals(cutOnlyDate(expiresSessionTime)), "Check that we extend our session correctly");

    }

    private String cutOnlyDate(String date) {
        return date.replaceAll("\\w+: ", "");
    }

}

