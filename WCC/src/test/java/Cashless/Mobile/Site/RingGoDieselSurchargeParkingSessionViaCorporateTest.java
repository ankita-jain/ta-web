package Cashless.Mobile.Site;

import static GenericComponents.constants.Symbols.POUND;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static Utilities.StringUtils.getPriceFromLabel;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.mobile.RingoMobileBookParkingSession;
import PageObjects.myringo.mobile.RingoMobileDurationParkingSession;

public class RingGoDieselSurchargeParkingSessionViaCorporateTest extends RingGoScenario {
	
    private String vehicleVrn, fullVrn;
    
    @BeforeMethod
    public void setUp() throws Exception {
        
        createUser("defaultNewUser.json");
        
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
        fullVrn =vehicleVrn.toUpperCase()+" WHITE BMW";
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void dieselSurcharge(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "WCC Diesel surcharge - RingGo mobile website");

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
        		vehicleVrn, data.get("yearOfManufacture"),
                data.get("co2"), data.get("engineSize"),
                data.get("vehicleType"), data.get("fuelType"), _driver);
        
        CorporateHelper.addUserToCorporate(data.get("CorporateLogin"), data.get("CorporatePassword"), vehicleVrn, mobile, _driver);

        LogStep("Step - 1", "Go to my ringo mobile. Log in");
        NavigationHelper.openRingoGoMobile(_driver);
        LoginHelper.loginMyRingGoMobile(email, password, _driver);

        LogStep("Step - 2", "Click on 'Parking' and book a new session");
        RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
        CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");

        LogStep("Step - 3", "Book at location code 8142 and select your diesel vehicle. Choose any duration. Continue to the payment section - check the price");
        RingoMobileBookParkingSession ringoMobileBookParkingSession = new RingoMobileBookParkingSession(_driver);

        CheckResult(ringoMobileBookParkingSession.enterZone(data.get("zone")), "Enter zoneID");
        CheckResult(ringoMobileBookParkingSession.selectVehicle(fullVrn), "Click select vehicle");
        CheckResult(ringoMobileBookParkingSession.next(), "Click next button");

        RingoMobileDurationParkingSession ringoMobileDurationParkingSession = new RingoMobileDurationParkingSession(_driver);

        CheckResult(ringoMobileDurationParkingSession.selectDuration(data.get("sessionDuration")), "Select duration");
        CheckResult(ringoMobileDurationParkingSession.tickSMSConfirm(), "Select SMS confirm");
        CheckResult(ringoMobileDurationParkingSession.tickReminderSMS(), "Select reminder SMS");
        CheckResult(ringoMobileDurationParkingSession.next(), "Click duration next button");
        
        BookParkingPage bookParkingPage = new BookParkingPage(_driver);
        
        LogStep("Step 9", "Select 'Yes' option and click 'Next'");
        CheckBool(bookParkingPage.getPayFromCorporateBlock().isCorporatePaymentVisible().isDisplayed(),  "Corproate Payment option displayed");
        CheckResult(ClickControl(bookParkingPage.getPayFromCorporateBlock().getYesRadioButton(), "Yes radiobutton"), "Click Yes");
        CheckResult(ClickControl(bookParkingPage.getPayFromCorporateBlock().getNextButton(), "Next button"), "Click Next button");

        String price = getPriceFromLabel(bookParkingPage.getPayCorporateConfirmBlock().getCostLabel().getText());
        CheckResult(ClickControl(bookParkingPage.getPayCorporateConfirmBlock().getLinkToPopUp(), "Fuel type price adjustment"), "Click on 'Fuel type price adjustment'");
        CheckBool(bookParkingPage.getPayCorporateConfirmBlock().getPopUpWindow().getText()
                .contains("A 50% (" + POUND.getAsUTF8() + "1.85) Fuel type surcharge applies in this zone"), "Text in pop up exists");

        LogStep("Step 10", "Click 'Next' button");
        CheckResult(ClickControl(bookParkingPage.getPayCorporateConfirmBlock().getNextButton(), "Next button"), "Click on Next button");
        CheckResult(ClickControl(bookParkingPage.getFinishBlock().getFinishButton(), "Finish button"), "Click on ''Finish button");
       
    }
}
