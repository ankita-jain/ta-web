package Cashless.Mobile.Site;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.RingGoMobileUserAccountPage;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.WCCPaymentDetailsPage;

public class RingGoMobileAddPaymentCardTest extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
    	createUser("islingtonNewUser.json");
    }
    // TODO - need to remove hardcoded test data - should be injected by DP
    @Test()
    public void addPaymentCardErrorMessageCheckTest() {

        StartTest("134204", "RingGo Mobile - Add new card workflow");

        LogStep("Step - 1", "Go to http://m.myrgo-preprod.ctt.co.uk/");
        NavigationHelper.openRingoGoMobile(_driver);

        LogStep("Step - 2", "Click 'Login'. Input 'Mobile number or E-mail' & password and click 'Log In'");
        LoginHelper.loginMyRingGoMobile(email, password, _driver);

        LogStep("Step - 3", "Click 'Account' icon in the home page");
        RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
        CheckResult(ringgomobilehomepage.clickAccount(), "Click Account tile");
       
        LogStep("Step - 4", "Click 'edit' button in the 'Payment Details' section");
        RingGoMobileUserAccountPage ringomobileuseraccountpage = new RingGoMobileUserAccountPage(_driver);
        CheckResult(ringomobileuseraccountpage.clickEditPayment(), "Click edit link in the account page"); 
        
        LogStep("Step - 5", "Click 'Add a new card' button");
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
        
        LogStep("Step - 6", "Add card details and click 'Add card'");
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
        
        CheckContains(wccaddpaymentcardpopup.getCardNumberErrorMessage(), "Please enter a valid card number", "Checking Card Number error message");
        CheckContains(wccaddpaymentcardpopup.getCardExpiryErrorMessage(), "Invalid date", "Checking Card Expries error message");
        CheckResult(wccaddpaymentcardpopup.clickAddPaymentPopupWindowClose(), "Click Close Add card popup");
        
        CheckResult(wccaddpaymentcardpopup.clickRingGoLogo(), "Click RingGo Logo.");
        CheckResult(ringgomobilehomepage.clickLogout(), "Click logout button");
    }
    
    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void addPaymentCardTest(String TCID, Map<String, String> data) {
    	
    	StartTest(TCID, "RingGo Mobile - Add new card workflow");

        LogStep("Step - 1", "Go to http://m.myrgo-preprod.ctt.co.uk/");
        NavigationHelper.openRingoGoMobile(_driver);

        LogStep("Step - 2", "Click 'Login'. Input 'Mobile number or E-mail' & password and click 'Log In'");
        LoginHelper.loginMyRingGoMobile(email, password, _driver);

        LogStep("Step - 3", "Click 'Account' icon in the home page");
        RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
        CheckResult(ringgomobilehomepage.clickAccount(), "Click Account tile");
       
        LogStep("Step - 4", "Click 'edit' button in the 'Payment Details' section");
        RingGoMobileUserAccountPage ringomobileuseraccountpage = new RingGoMobileUserAccountPage(_driver);
        CheckResult(ringomobileuseraccountpage.clickEditPayment(), "Click edit link in the account page"); 
        
        LogStep("Step - 5", "Click 'Add a new card' button");
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
        
        LogStep("Step - 6", "Add card details and click 'Add card'");
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.enterCardNumber(data.get("visa_card")),"Enter card number"); 
        CheckResult(wccaddpaymentcardpopup.enterCardExpires(data.get("card_expiries")),"Enter card expiries");
        CheckResult(wccaddpaymentcardpopup.clickAddButton(),"Click Add button.");
        
        CheckBool(wccpaymentdetailspage.getSuccessNotification().equals( String.format("Visa card ending %s was successfully added", data.get("visa_card").substring(12))), "Add card successfull message");
        
        CheckResult(wccaddpaymentcardpopup.clickRingGoLogo(), "Click RingGo Logo.");
        CheckResult(ringgomobilehomepage.clickLogout(), "Click logout button");
    	
    }
}

