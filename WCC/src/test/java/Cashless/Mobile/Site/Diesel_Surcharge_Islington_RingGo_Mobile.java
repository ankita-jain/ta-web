package Cashless.Mobile.Site;

import static GenericComponents.constants.Symbols.POUND;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CashlessHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.RingGoParkingSessionMobilePage;
import PageObjects.WCCParkingSessionPage;


public class Diesel_Surcharge_Islington_RingGo_Mobile extends RingGoScenario {
    private static final String VEHICLE_TYPE = "Car";
    private String petrolCarVrn;
    private String carEndingNumber;

	@BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");
        petrolCarVrn = vehicleVrn;
        carEndingNumber = userCardNumber.substring(userCardNumber.length() - 4);
        RestApiHelper.setCarType(email, password, VEHICLE_TYPE);
    }

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void parkingSessionTest(String TCID, Map<String, String> data) {
        StartTest(TCID, "Diesel Surcharge only for LB of Islington - Diesel Vehicle - RingGo Mobile Webpage");

        CashlessHelper.changeCarPetrolType(insightUserLogin, insightPassword,
                petrolCarVrn, data.get("yearOfManufacture"),
                data.get("co2"), data.get("engineSize"),
                VEHICLE_TYPE, data.get("fuelType"), _driver);

        LogStep("Step - 1", "Go to http://m.myrgo-preprod.ctt.co.uk/");
        NavigationHelper.openRingoGoMobile(_driver);

        LogStep("Step - 2", "Click 'Login'. Input 'Mobile number or E-mail' & password and click 'Log In'");
        LoginHelper.loginMyRingGoMobile(email, password, _driver);

        LogStep("Step - 3", "Click 'Parking' icon in the home page");
        RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
        CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");

        LogStep("Step - 4", "Enter '61175' in 'Which location do you want to park in?' input field and select the diesel vehicle from the 'Which vehicle do you want to park? ' drop down list");
        RingGoParkingSessionMobilePage ringgoparkingsessionmobilepage = new RingGoParkingSessionMobilePage(_driver);
        CheckResult(ringgoparkingsessionmobilepage.enterZone(data.get("zone")), "Enter zoneID");
        CheckResult(ringgoparkingsessionmobilepage.selectVehicle(petrolCarVrn), "Click select vehicle");

        LogStep("Step - 5", " Click 'Next' button");
        CheckResult(ringgoparkingsessionmobilepage.clickNextButton(), "Click next button");

        LogStep("Step - 6", "Select 'Now' button in the 'When do you want to park?' page and click 'Next' button");
        CheckResult(ringgoparkingsessionmobilepage.chooseWhenToPark(RingGoParkingSessionMobilePage.WhenToPark.NOW), "Choose when to park");
        CheckResult(ringgoparkingsessionmobilepage.whenToParkNext(), "Click next button");

        LogStep("Step - 7", "Select '1 Hour' and click 'Next' button");
        CheckResult(ringgoparkingsessionmobilepage.selectDuration("1 Hour"), "Select duration");
        CheckResult(ringgoparkingsessionmobilepage.selectSMSConfirm(), "Select SMS confirm");
        CheckResult(ringgoparkingsessionmobilepage.selectReminderSMS(), "Select reminder SMS");
        CheckResult(ringgoparkingsessionmobilepage.clickDurationNextButton(), "Click duration next button");
        CheckBool(ringgoparkingsessionmobilepage.isDieselSurchargeDisplayed(), "Check Diesel Surcharge visibility");
        CheckResult(ringgoparkingsessionmobilepage.clickDieselSurchagre(), "Click diesel surcharge");
        
        // TODO Need to make the surcharge dynamic - perhaps use insight to read the operator settings \ CO2 Charging for comparison below
        CheckContains(ringgoparkingsessionmobilepage.getDieselSurchargeToolTipText(), "A " + POUND.getAsUTF8() + "3.00 Fuel type surcharge applies in this zone",
                "Check diesel surcharge message surcharge matches " + POUND.getAsUTF8() + "3.00 (hardcoded)");

        LogStep("Step - 8", "Select the payment card and click 'Next'");      
        CheckResult(ringgoparkingsessionmobilepage.clickCardSelectNextButton(), "Click card select next button");
        
        WCCParkingSessionPage wccparkingsessionpage = new WCCParkingSessionPage(_driver);
        CheckResult(wccparkingsessionpage.clickPay(), "Click pay button");
        CheckResult(wccparkingsessionpage.enterCVV(data.get("CV2")), "Enter CVV2 code");
        CheckResult(wccparkingsessionpage.clickPayConfirm(), "Click pay button");

        LogStep("Step - 8", "Enter the CVV number, accept T&C and click 'Pay' button");
        CheckResult(ringgoparkingsessionmobilepage.clickFinish(), "Click finish button");
        CheckResult(ringgomobilehomepage.clickLogout(), "Click logout button");
    }
}
