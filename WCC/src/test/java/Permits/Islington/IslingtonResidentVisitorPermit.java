package Permits.Islington;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Islington.IslingtonAboutYouPage;
import PageObjects.Islington.IslingtonFinishPage;
import PageObjects.Islington.IslingtonHomePage;
import PageObjects.Islington.IslingtonLoginPage;
import PageObjects.Islington.IslingtonMyPermitApplicationPage;
import PageObjects.Islington.IslingtonProofPage;
import PageObjects.Islington.IslingtonPurchaseARVPCardSelectPage;
import PageObjects.Islington.IslingtonPurchaseARVPFinishPage;
import PageObjects.Islington.IslingtonPurchaseARVPPage;
import PageObjects.Islington.IslingtonPurchaseARVPPayPage;
import PageObjects.Islington.IslingtonRVPAddressSearchPage;
import PageObjects.myringo.PaymentCardCVVPage;

public class IslingtonResidentVisitorPermit extends RingGoScenario{
	
	private String VRN;
	
	@Test(dataProvider="regressionJsonData", dataProviderClass=DataProviders.RegressionDP.class)
	public void userAppliesForAResidentVisitorPermit(String TCID, Map<String, String> data) throws IOException {
		StartTest(TCID, "Islington Resident Visitor Permit - skinned site");
		NavigationHelper.openIslington(_driver);
		LoginHelper.loginIslington(getSuiteStartDate(), _driver);
		IslingtonLoginPage islingtonloginpage = new IslingtonLoginPage(_driver);
		CheckResult(islingtonloginpage.clickLogin(), "Click the login button");
		IslingtonHomePage islinghomepage = new IslingtonHomePage(_driver);
		CheckResult(islinghomepage.clickApplyForPermitLink(), "Clicking the apply for a new permit link");
		CheckResult(islinghomepage.clickResidentVisitorApplication(), "Clicking the Resident visitor application link");
		IslingtonRVPAddressSearchPage islingtonrvpaddresssearchpage = new IslingtonRVPAddressSearchPage(_driver);
		CheckContains(islingtonrvpaddresssearchpage.getPageHeader(), "Resident Visitor Permit Application", "Checking the page header of the page");
		
		CheckResult(islingtonrvpaddresssearchpage.enterPostCode(data.get("incorrectPostcode")), "user enters wrong postcode");
		islingtonrvpaddresssearchpage.clickFindAddress();
		CheckContains(islingtonrvpaddresssearchpage.getPostcodeError(), "Sorry, Resident Visitor permits are not allowed for this postcode. If you feel this is a mistake please contact LB of Islington.",
				"Checking the postcode error is visible");
		CheckResult(islingtonrvpaddresssearchpage.enterPostCode(data.get("postcode")), "user enters correct postcode");
		islingtonrvpaddresssearchpage.clickFindAddress();
		islingtonrvpaddresssearchpage.selectAddress(data.get("Address"));
		delay(3000);
		islingtonrvpaddresssearchpage.clickNext();
		IslingtonAboutYouPage islingtonaboutyoupage = new IslingtonAboutYouPage(_driver);
		CheckContains(islingtonaboutyoupage.getPageSubHeader(), "Your Details", "Getting the page sub heading");
		String CLI = islingtonaboutyoupage.getClI();
		islingtonaboutyoupage.fillInPersonalDetails(data.get("firstname"),data.get("surname"), data.get("title"));
		islingtonaboutyoupage.selectTermAndConditions();
		IslingtonProofPage islingtonproofpage = new IslingtonProofPage(_driver);
		CheckContains(islingtonproofpage.getPageSubHeader(), "Proof of Address ", "checking the proof page title");
		islingtonproofpage.selectProof(data.get("proof"));
		islingtonproofpage.uploadProof(data.get("prooffile"));
		delay(3000);
		CheckResult(islingtonproofpage.clickConfirm(), "Clicking the confirm button");
		IslingtonFinishPage islingtonfinishpage = new IslingtonFinishPage(_driver);
		CheckContains(islingtonfinishpage.getThankYouText(), "Thank you for registering", "Checking if the user is on the finish page");
		CheckResult(islingtonfinishpage.clickFinish(), "Click the finish button");
		IslingtonMyPermitApplicationPage islingtonmypermitapplicationpage = new IslingtonMyPermitApplicationPage(_driver);
		CheckContains(islingtonmypermitapplicationpage.getpageHeader(), "My Permit Applications for LB of Islington", "Checking the page title is correct.");
		CheckContains(islingtonmypermitapplicationpage.getPermitStatus(), "PENDING", "Checking the permit is in a pending status");
		
		//Authorise permit via insight
		LogStep("Step - 1", "Login to Insight");

		NavigationHelper.openInsight(_driver);
		LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);
		
		InsightSearch insightsearch = new InsightSearch(_driver);
        insightsearch.selectOperatorProcess(data.get("operatorstatus"), data.get("operator"));
        InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
        CheckValue(insightdefaultoperatorpage.getPageHeader(), "Parking Zones",
                "Checking the page title for insight operator page is correct");
        
        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
        insightservicemenupage.loadPermitSearchForm();
		InsightHelper.searchPermitByCLIAndEdit(data.get("permitStatus"), CLI, _driver);
		InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
		insightpermitapplicationpage.editPermitStatusAndSave("Authorised");
		NavigationHelper.openIslington(_driver);
		CheckContains(islingtonmypermitapplicationpage.getPermitStatus(), "AUTHORISED", "Checking the permit is in a authorised status");
		CheckResult(islingtonmypermitapplicationpage.clickPurchase(), "Click purchase a permit icon");
		
		IslingtonPurchaseARVPPage islingtonpurchaseARVPPage = new IslingtonPurchaseARVPPage(_driver);
		islingtonpurchaseARVPPage.getpageHeader();
		VRN =randomAlphabetic(6).toUpperCase();
		islingtonpurchaseARVPPage.addANewVehicle(VRN, data.get("make"), data.get("colour"));
		islingtonpurchaseARVPPage.selectDuration(data.get("time"));
		islingtonpurchaseARVPPage.takePaymentNow();
		islingtonpurchaseARVPPage.clickNextForPayment();
		IslingtonPurchaseARVPCardSelectPage islingtonpurchasarvpcardselectpage = new IslingtonPurchaseARVPCardSelectPage(_driver);
		islingtonpurchasarvpcardselectpage.clickNext();
		IslingtonPurchaseARVPPayPage islingtonpurchasearvppaypage = new IslingtonPurchaseARVPPayPage(_driver);
		islingtonpurchasearvppaypage.clickPay();
		PaymentCardCVVPage paymentcardcvvpage = new PaymentCardCVVPage(_driver);
		paymentcardcvvpage.finishPayingForPermit(data.get("CVV"));
		IslingtonPurchaseARVPFinishPage islingtonpurchasearvpfinishpage = new IslingtonPurchaseARVPFinishPage(_driver);
		CheckResult(islingtonpurchasearvpfinishpage.clickfinish(), "Click the finish button");
	}
}