package Permits.WCC.ECO;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

import static Utilities.DateUtils.TimeStamp;
import static Utilities.DateUtils.convertDateFormatToAnotherFormat;
import static Utilities.FileUtils.writeToTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class Resident_ECO_Permit_Temporary_Permit_Status_Verify_From_Insight extends RingGoScenario {
    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
    	NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_insight_eco_resident_permit_temp_permit_from_insight", dataProviderClass = DataProviders.DP_WCC.class, groups = {"temp_eco_permit_details_via_insight"}, dependsOnGroups = {"temp_eco_resident_permit"})
    public void authorisePermitApplication(String Username, String Password, String Operator_Status, String Operator, String VRM, String Permit_Status, String Change_Permit_Status_To, String vehicle_country, String temp_permit_type, String reponse) throws IOException, ParseException {
        StartTest("", "WCC- Resident Permit- Zone Changed status");

        try {
            String permit_number;
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            permit_number = InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            /*
             * Write permit_number(PermitId) to a local storage for further use.
             *
             */

            writeToTestDataFile(permit_number, RESIDENT_PERMIT_ECO_TXT_FILE);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Resident Low Emission Permit Application"));

            assertTrue("Permit application start date", convertDateFormatToAnotherFormat(insightpermitapplicationpage.getPermitValidFromDate(), "dd MMM yyyy", "dd/MM/yyyy").contains(TimeStamp("dd/MM/yyyy")));
            //assertTrue("Permit application end date",convertDateFormatToAnotherFormat(insightpermitapplicationpage.getPermitValidToDate(), "dd MMM yyyy", "dd/MM/yyyy").contains(DateFromOffset("364", "dd/MM/yyyy", false)));

            insightpermitapplicationpage.editEcoPermitStatusToTempPermitAndSave(Change_Permit_Status_To, vehicle_country, temp_permit_type, reponse);

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            assertTrue(insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }
}
