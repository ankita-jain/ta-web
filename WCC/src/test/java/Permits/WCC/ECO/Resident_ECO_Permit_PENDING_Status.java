package Permits.WCC.ECO;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

public class Resident_ECO_Permit_PENDING_Status extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Resident_ECO_Permit_PENDING_Status.class);

    @Test(dataProvider = "dp_wcc_eco_resident_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"eco_permit_application"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone,
                                          String Residency_type, String Vehicle, String Ownership, String address_proof_type_one,
                                          String address_proof_one, String address_proof_type_two, String address_proof_two,
                                          String Car_insurance_schedule_proof_type, String Car_insurance_schedule_proof,
                                          String Certificate_of_insurance, String Certificate_of_insurance_proof,
                                          String Vehicle_registration_document, String Vehicle_registration_document_proof) throws IOException {
        StartTest("355", "WCC- Resident ECO Permit- PENDING status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickEcoPermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident Low Emission permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Postcode, Address, Parking_zone);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            wccresidentpermitdetailspage.fillPermitApplicationDetails(Residency_type, Vehicle, Ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Eco permit proof pagetitle", wccresidentpermitproofpage.getProofPageTitle().contains("Resident Low Emission permit application"));
            wccresidentpermitproofpage.completeEcoPermitApplicationProofs(address_proof_type_one, address_proof_one, address_proof_type_two, address_proof_two, Car_insurance_schedule_proof_type, Car_insurance_schedule_proof, Certificate_of_insurance, Certificate_of_insurance_proof, Vehicle_registration_document, Vehicle_registration_document_proof);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            wccpermitdetailsconfirmationpage.confirmPermitDetails();

            WCCEcoPermitPurchaseFinishPage wccecopermitpurchasefinishpage = new WCCEcoPermitPurchaseFinishPage(_driver);
            assertTrue("Eco permit free purchase pay", wccecopermitpurchasefinishpage.getPageTitle().contains("Purchase a resident low emission permit"));
            wccecopermitpurchasefinishpage.clickConfirm();

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Eco permit finish pagetitle", wccpermitpurchasefinishpage.getPageTitle().contains("Purchase a resident low emission permit"));
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
