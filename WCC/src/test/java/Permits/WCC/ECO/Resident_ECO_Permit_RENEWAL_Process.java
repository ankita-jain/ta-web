package Permits.WCC.ECO;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class Resident_ECO_Permit_RENEWAL_Process extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);
    String permitid;

    @Test(dataProvider = "dp_wcc_renew_expired_eco_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"eco_premit_renewal"}, dependsOnGroups = {"insight_eco_permit_expire"})
    public void residentPermitApplication(String Username, String Password, String residentTypes, String vrm, String ownership, String Address_proof_type1, String Address_proof1, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("365", "WCC- Resident Permit- Renewal Process");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickMyPermitsFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_ECO_TXT_FILE);
            System.out.println("Permit Id=" + permitid);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            //assertTrue("Permit valid to=",wccmypermitpage.getPermitValidTo(permitid).contains(TimeStamp("dd/MM/yyyy")));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Expired"));

            /*
             * If the permit is 'Expired', then proceed with permit Renewal Process
             *
             */

            wccmypermitpage.clickOrderReplacement(permitid);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            assertTrue("Permit renewal page", wccresidentpermitdetailspage.getResidentPermitDetailsPageTitle().contains("Resident Low Emission permit renewal"));
            wccresidentpermitdetailspage.fillPermitApplicationDetails(residentTypes, vrm, ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Permit renewal page", wccresidentpermitproofpage.getProofPageTitle().contains("Resident Low Emission permit renewal"));
            wccresidentpermitproofpage.permitRenewalUploadProofs(Address_proof_type1, Address_proof1);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            assertTrue("Permit confirmation page title", wccpermitdetailsconfirmationpage.getConfirmationPageTitle().contains("Purchase a resident low emission permit"));
            wccpermitdetailsconfirmationpage.confirmPermitDetails();

            WCCEcoPermitPurchaseFinishPage wccecopermitpurchasefinishpage = new WCCEcoPermitPurchaseFinishPage(_driver);
            assertTrue("Eco permit free purchase pay", wccecopermitpurchasefinishpage.getPageTitle().contains("Purchase a resident low emission permit"));
            wccecopermitpurchasefinishpage.clickConfirm();

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Purchase resident permit & finish page", wccpermitpurchasefinishpage.getPageTitle().contains("Purchase a resident low emission permit"));
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
