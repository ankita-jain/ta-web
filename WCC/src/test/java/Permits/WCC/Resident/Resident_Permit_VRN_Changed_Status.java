package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class Resident_Permit_VRN_Changed_Status extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Resident_Permit_VRN_Changed_Status.class);
    String permitid;

    @Test(dataProvider = "dp_wcc_resident_permit_perm_vrn_change", dataProviderClass = DataProviders.DP_WCC.class, groups = {"permitapplication_perm_vrn_change"}, dependsOnGroups = {"permit_application_details_check"})
    public void residentPermitApplication(String Username, String Password, String permanent_vrn, String Ownership_type, String Proof_type, String proof, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("349", "WCC- Resident Permit- Permanent VRN change status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_TXT_FILE);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));

            wccmypermitpage.clickViewDetails(permitid);

            WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
            assertTrue("View permit title check", wccviewpermitdetailspage.getViewPermitTitle().contains("View permit: Resident permit"));
            wccviewpermitdetailspage.clickManageVehicleButton();

            WCCPermitManageVehiclesPage wccpermitmanagevehiclepage = new WCCPermitManageVehiclesPage(_driver);
            assertTrue("Verify Manage vehicle page title", wccpermitmanagevehiclepage.getManageVehicleTitle().contains("Manage vehicles: Resident permit "));
            wccpermitmanagevehiclepage.changePermanentVRNChangesProcess(permanent_vrn, Ownership_type);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Verify proofs page title", wccresidentpermitproofpage.getProofPageTitle().contains("Manage proofs"));
            wccresidentpermitproofpage.changeVRNPermanentlyProofs(Proof_type, proof);

            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            assertTrue("Payment page title", wccpaymentcarddetailspage.getPageTitle().contains("Payment"));
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCardInAccount(credit_card, exp_month, exp_year);

            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            assertTrue("Payment page title", wccpaymentcardcvvpage.getPageTitle().contains("Payment"));
            wccpaymentcardcvvpage.enterCV2AndProceedPermitCharges(CV2);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Finish page title check", wccpermitpurchasefinishpage.getPageTitle().contains("Payment"));
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            wcchome.logout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
