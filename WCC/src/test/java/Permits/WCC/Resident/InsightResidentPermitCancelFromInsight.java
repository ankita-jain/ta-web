package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class InsightResidentPermitCancelFromInsight extends RingGoScenario {

    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
        NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "wcc_resident_permit_cancel_from_insight", dataProviderClass = DataProviders.WCCResidentPermitCancelFromInsight.class, groups = {"insight_cancel_resident_permit_from_insight"}, dependsOnGroups = {"perimit_cancel"})
    public void cancelPermit(String TCID, Map<Object, Object> Data) throws IOException {
    	String Username,Password,Operator_Status,Operator,VRM,Permit_Status,Change_Permit_Status_To,Cancelation_reason,Response_to_customer;
        StartTest(TCID, "WCC- Resident Permit- Cancel permit from insight");

        try {
        	
        	Username = Data.get("email").toString();
        	Password = Data.get("password").toString();
        	Operator_Status = Data.get("operator_status").toString();
        	Operator = Data.get("operator").toString();
        	VRM = Data.get("vrm").toString();
        	Permit_Status = Data.get("permit_status").toString();
        	Change_Permit_Status_To = Data.get("change_permit_status_to").toString();
        	Cancelation_reason = Data.get("cancellation_reason").toString();
        	Response_to_customer = Data.get("response_to_customer").toString();
                 	
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Resident Permit Application"));
            insightpermitapplicationpage.cancelPermitFromInsight(Change_Permit_Status_To, Cancelation_reason, Response_to_customer);

            InsightEndPermitSessionPage insightendpermitsessionpage = new InsightEndPermitSessionPage(_driver);
            assertTrue("End permit sessions page title", insightendpermitsessionpage.getPageTitle().contains("End Permit Sessions?"));
            insightendpermitsessionpage.clickEndSession();

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            assertTrue(insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }
}
