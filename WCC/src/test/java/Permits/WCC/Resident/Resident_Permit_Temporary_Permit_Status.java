package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

public class Resident_Permit_Temporary_Permit_Status extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);

    @Test(dataProvider = "dp_wcc_temp_resident_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"temp_resident_permit"}, dependsOnGroups = {"insight_cancel_resident_permit_from_insight"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone, String Residency_type, String Vehicle, String Ownership, String Address_proof_type1, String Address_proof1, String vehicle_registered_country_UK, String vehicle_proof_type2, String Vehicle_proof2, String credit_card, String exp_month, String exp_year, String CV2) throws IOException {
        StartTest("346", "WCC- Resident Permit- Temporary resident permit status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickStandardResidentPermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Postcode, Address, Parking_zone);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            wccresidentpermitdetailspage.fillPermitApplicationDetails(Residency_type, Vehicle, Ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Checking proof page title", wccresidentpermitproofpage.getProofPageTitle().contains("Resident permit application"));
            wccresidentpermitproofpage.completeTemporaryPermitApplicaionProofs(Address_proof_type1, Address_proof1, vehicle_registered_country_UK, vehicle_proof_type2, Vehicle_proof2);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            assertTrue("Purchase resident permit page title", wccpermitdetailsconfirmationpage.getConfirmationPageTitle().contains("Purchase a resident permit"));
            wccpermitdetailsconfirmationpage.confirmPermitDetails();

            WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
            String purchasepermitpagetitle = wccpurchasepermitpaymenttype.getPageTitle();
            assertTrue(purchasepermitpagetitle.contains("Purchase a resident permit"));
            wccpurchasepermitpaymenttype.processPaymentNow();

            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            assertTrue("Payment page title", wccpaymentcarddetailspage.getPageTitle().contains("Purchase a resident permit"));
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCardInAccount(credit_card, exp_month, exp_year);

            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            assertTrue("Payment page title", wccpaymentcardcvvpage.getPageTitle().contains("Purchase a resident permit"));
            wccpaymentcardcvvpage.enterCV2AndProceed(CV2);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            String permitpurchasefinishpagetitle = wccpermitpurchasefinishpage.getPageTitle();
            assertTrue(permitpurchasefinishpagetitle.contains("Purchase a resident permit"));
            wccpermitpurchasefinishpage.clickFinish();

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
