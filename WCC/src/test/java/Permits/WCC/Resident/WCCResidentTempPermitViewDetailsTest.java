package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;
import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class WCCResidentTempPermitViewDetailsTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);
    String permitid;

    @Test(dataProvider = "dp_wcc_temp_resident_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"temp_resident_permit_application_details"}, dependsOnGroups = {"temp_permit_details_via_insight"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone, String Residency_type, String Vehicle, String Ownership, String Address_proof_type1, String Address_proof1, String vehicle_registered_country_UK, String vehicle_proof_type2, String Vehicle_proof2, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("", "WCC- Resident Permit- Authorised - Permit Details check");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_TXT_FILE);
            System.out.println("Permit Id=" + permitid);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit valid from =", wccmypermitpage.getPermitValidFrom(permitid).contains(TimeStamp("dd/MM/yyyy")));
            assertTrue("Permit valid to=", wccmypermitpage.getPermitValidTo(permitid).contains(DateFromOffset("41", "dd/MM/yyyy", false)));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Temporary permit"));
            assertTrue("Temporary permit upload button visible", wccmypermitpage.isUploadButtonVisible(permitid));


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
