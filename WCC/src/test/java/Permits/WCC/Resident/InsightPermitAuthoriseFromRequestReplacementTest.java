package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.testng.AssertJUnit.assertTrue;

public class InsightPermitAuthoriseFromRequestReplacementTest extends RingGoScenario {
    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
        NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_insight_resident_permit_auth_from_request_replacement", dataProviderClass = DataProviders.DP_WCC.class, groups = {"insight_aut_permit_from_request_replacement"}, dependsOnGroups = {"insight_auth_permit_from_perm_vrn_change"})
    public void authorisePermitApplication(String Username, String Password, String Operator_Status, String Operator, String VRM, String Permit_Status, String Change_Permit_Status_To) throws IOException {
        StartTest("", "WCC- Resident Permit- Authorise permit from Request Replacement");

        try {
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Resident Permit Application"));
            insightpermitapplicationpage.editPermitStatusAndSave(Change_Permit_Status_To);

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            assertTrue(insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }
}
