package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class Resident_Permit_Cancel_from_Authorised_Status extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);
    String permitid;

    @Test(dataProvider = "dp_resident_permit_cancel", dataProviderClass = DataProviders.DP_WCC.class, groups = {"perimit_cancel"}, dependsOnGroups = {"insight_aut_permit_from_request_replacement"})
    public void residentPermitApplication(String Username, String Password, String reason) throws IOException, ParseException {
        StartTest("470", "WCC- Resident permit -Cancel permit");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");
            

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_TXT_FILE);
            System.out.println("Permit Id=" + permitid);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));

            wccmypermitpage.clickCancelResidentPermit(permitid);

            WCCPermitCancelPage wccpermitcancelpage = new WCCPermitCancelPage(_driver);
            System.out.println(wccpermitcancelpage.getCancelPagetitle());
            assertTrue("Checking cancel page title", wccpermitcancelpage.getCancelPagetitle().contains("Cancellation request"));
            wccpermitcancelpage.cancelPermitProcess(reason);


            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Cancelled"));

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
