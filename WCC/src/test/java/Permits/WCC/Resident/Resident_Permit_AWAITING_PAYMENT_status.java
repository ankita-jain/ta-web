package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.testng.AssertJUnit.assertTrue;

public class Resident_Permit_AWAITING_PAYMENT_status extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);

    @Test(dataProvider = "dp_wcc_resident_permit_awaiting_payment", dataProviderClass = DataProviders.DP_WCC.class, groups = {"permit_application_awp"})
    public void residentPermitApplication(String Username, String Password,
                                          String Postcode, String Address, String Parking_zone,
                                          String Residency_type, String Vehicle, String Ownership,
                                          String Address_proof_type1, String Address_proof1, String Address_proof_type2, String Address_proof2,
                                          String Car_insurance_schedule_proof, String Certificate_of_insurance, String Vehicle_registration_document) throws IOException {
        StartTest("345", "WCC- Resident permit - AWAITING PAYMENT status");
        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            wccindexpage.clickPersonalFromLogin();

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            wccloginpage.inputEmailOrPhonenumer(Username);
            wccloginpage.enterPassword(Password);
            wccloginpage.clickLoginButton();

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickStandardResidentPermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Postcode, Address, Parking_zone);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            wccresidentpermitdetailspage.fillPermitApplicationDetails(Residency_type, Vehicle, Ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            wccresidentpermitproofpage.completePermitApplicationProofs(Address_proof_type1, Address_proof1, Address_proof_type2, Address_proof2, Car_insurance_schedule_proof, Certificate_of_insurance, Vehicle_registration_document);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            wccpermitdetailsconfirmationpage.clickPayLater();
            
            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Verify permit status", wccmypermitpage.isPermitAwaitingPayment());

            //user logout
            wcchome.logout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
