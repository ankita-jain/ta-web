package Permits.WCC.Resident;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;
import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class Resident_Permit_Authorised_Status_Check extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);
    String permitid;

    @Test(dataProvider = "dp_wcc_resident_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"permitapplicationdetails"}, dependsOnGroups = {"authorise_permit_via_insight"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone, String Residency_type, String Vehicle, String Ownership, String Address_proof_type1, String Address_proof1, String Address_proof_type2, String Address_proof2, String Car_insurance_schedule_proof, String Certificate_of_insurance, String Vehicle_registration_document, String CV2) throws IOException, ParseException {
        StartTest("348", "WCC- Resident Permit- Authorised - Permit Details check");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_TXT_FILE);
            
            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit valid from =", wccmypermitpage.getPermitValidFrom(permitid).contains(TimeStamp("dd/MM/yyyy")));
            assertTrue("Permit valid to=", wccmypermitpage.getPermitValidTo(permitid).contains(DateFromOffset("365", "dd/MM/yyyy", false)));
            
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));
                        
            wccmypermitpage.clickViewDetails(permitid);

            WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
            assertTrue("View permit title check", wccviewpermitdetailspage.getViewPermitTitle().contains("View permit: Resident permit"));
            assertTrue("Permit Detail section visible", wccviewpermitdetailspage.isPermitDetailSectionVisible());
            assertTrue("Active permit session visible", wccviewpermitdetailspage.isActivePermitSessionVisible());
            assertTrue("Name and Address visible", wccviewpermitdetailspage.isNameAddressSectionVisible());
            assertTrue("Vehicle section visible", wccviewpermitdetailspage.isVehicleSectionVisible());
            assertTrue("Proofs section visible", wccviewpermitdetailspage.isProofsSectionVisible());

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
