package Permits.WCC.Motorcycle;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitEditAddressPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCResidentPermitProofPage;
import PageObjects.WCCViewPermitDetailsPage;

import com.relevantcodes.extentreports.LogStatus;

public class Resident_MOTORCYCLE_Permit_ZONE_Changed_Status extends RingGoScenario {

    String permitid;

    @Test(dataProvider = "dp_wcc_resident_motorcycle_permit_zone_change", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motorcycle_permit_application_edit_zone"}, dependsOnGroups = {"insight_motorcycle_permit_address_change"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone, String Address_proof_type1, String Address_proof1, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("374", "WCC- Resident Motorcycle Permit- Zone change status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_MOTO_TXT_FILE);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));

            wccmypermitpage.clickViewDetails(permitid);

            WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
            assertTrue("View permit title check", wccviewpermitdetailspage.getViewPermitTitle().contains("View permit: Resident Motorcycle permit"));
            wccviewpermitdetailspage.clickEditAddressButton();

            WCCPermitEditAddressPage wccpermiteditaddresspage = new WCCPermitEditAddressPage(_driver);
            assertTrue("Verify edit address page title", wccpermiteditaddresspage.getEditAddressPageTitle().contains("Edit address: Resident motorcycle permit"));
            wccpermiteditaddresspage.changeAddressOnPermit(Postcode, Address, Parking_zone);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Manage proofs title", wccresidentpermitproofpage.getProofPageTitle().contains("Manage proofs"));
            wccresidentpermitproofpage.editAddressonEcoPermitAndCompleteUploadProofs(Address_proof_type1, Address_proof1);

            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            assertTrue("Payment page title", wccpaymentcarddetailspage.getPageTitle().contains("Payment"));
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCardInAccount(credit_card, exp_month, exp_year);

            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            assertTrue("Payment page title", wccpaymentcardcvvpage.getPageTitle().contains("Payment"));
            wccpaymentcardcvvpage.enterCV2AndProceedPermitCharges(CV2);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Finish page title check", wccpermitpurchasefinishpage.getPageTitle().contains("Payment"));
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
