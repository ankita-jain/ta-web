package Permits.WCC.Motorcycle;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

import static Utilities.FileUtils.writeToTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class InsightTemporaryMotorcycleResidentPermitDetailsTest extends RingGoScenario {
    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
    	NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_insight_wcc_resident_motorcycle_temp_permit_details", dataProviderClass = DataProviders.DP_WCC.class, groups = {"temp_motorcycle_permit_details_via_insight"}, dependsOnGroups = {"motocycle_temp_permit"})
    public void authorisePermitApplication(String Username, String Password, String Operator_Status, String Operator, String VRM, String Permit_Status, String Change_Permit_Status_To, String reponse) throws IOException, ParseException {
        StartTest("", "WCC- Resident Motorcycle Permit- Zone Changed status");

        try {
            String permit_number;
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            permit_number = InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            /*
             * Write permit_number(PermitId) to a local storage for further use.
             *
             */

            writeToTestDataFile(permit_number, RESIDENT_PERMIT_MOTO_TXT_FILE);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Resident Motorcycle Permit Application"));

            //assertTrue("Permit application start date",convertDateFormatToAnotherFormat(insightpermitapplicationpage.getPermitValidFromDate()).contains(TimeStamp("dd/MM/yyyy")));
            //assertTrue("Permit application end date",convertDateFormatToAnotherFormat(insightpermitapplicationpage.getPermitValidToDate(), "dd MMM yyyy", "dd/MM/yyyy").contains(DateFromOffset("364", "dd/MM/yyyy", false)));

            insightpermitapplicationpage.editPermitStatusToTempPermitAndSave(Change_Permit_Status_To, reponse);

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            assertTrue(insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
