package Permits.WCC.Motorcycle;

import static Utilities.DateUtils.TimeStamp;
import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCResidentPermitProofPage;

import com.relevantcodes.extentreports.LogStatus;

public class Resident_MOTORCYCLE_Permit_RENEWAL_Process extends RingGoScenario {

    String permitid;

    @Test(dataProvider = "dp_wcc_resident_motorcycle_permit_renew", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motorcycle_premit_renewal"}, dependsOnGroups = {"insight_motorcycle_permit_expire"})
    public void residentPermitApplication(String Username, String Password, String Address_proof_type1, String Address_proof1, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("378", "WCC- Resident MOTORCYCLE Permit - RENEWAL process ");
        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickMyPermitsFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_MOTO_TXT_FILE);
            System.out.println("Permit Id=" + permitid);


            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit valid to=", wccmypermitpage.getPermitValidTo(permitid).contains(TimeStamp("dd/MM/yyyy")));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Expired"));

            /*
             * If the permit is 'Expired', then proceed with permit Renewal Process
             *
             */

            wccmypermitpage.clickOrderReplacement(permitid);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            assertTrue("Permit renewal page", wccresidentpermitdetailspage.getResidentPermitDetailsPageTitle().contains("Resident Motorcycle permit renewal"));
            wccresidentpermitdetailspage.permitRenewalProcess();

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Permit renewal page", wccresidentpermitproofpage.getProofPageTitle().contains("Resident Motorcycle permit renewal"));
            wccresidentpermitproofpage.permitRenewalUploadProofs(Address_proof_type1, Address_proof1);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            assertTrue("Permit confirmation page title", wccpermitdetailsconfirmationpage.getConfirmationPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpermitdetailsconfirmationpage.confirmPermitDetails();

            WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
            assertTrue("Purchase resident motorcycle permit", wccpurchasepermitpaymenttype.getPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpurchasepermitpaymenttype.processPaymentNow();

            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            assertTrue("Purchase resident motorcycle permit", wccpaymentcarddetailspage.getPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCardInAccount(credit_card, exp_month, exp_year);

            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            assertTrue("Purchase resident motorcycle permit", wccpaymentcardcvvpage.getPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpaymentcardcvvpage.enterCV2AndProceed(CV2);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Purchase resident motorcycle permit & finish page", wccpermitpurchasefinishpage.getPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            wcchome.logout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
