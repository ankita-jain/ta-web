package Permits.WCC.Motorcycle;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitCancelPage;
import PageObjects.WCCPermitPage;

import com.relevantcodes.extentreports.LogStatus;

public class Resident_MOTORCYCLE_Permit_Cancellation_Request_Status extends RingGoScenario {

      String permitid;

    @Test(dataProvider = "dp_wcc_resident_motorcycle_permit_cancel_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motorcycle_permit_cancel"}, dependsOnGroups = {"insight_auth_motorcycle_permit_from_perm_vrn_change"})
    public void residentPermitApplication(String Username, String Password, String reason) throws IOException, ParseException {
        StartTest("376", "WCC- Resident Motorcycle Permit- Cancel Permit");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_MOTO_TXT_FILE);
            System.out.println("Permit Id=" + permitid);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));

            /*
             * Cancelling motorcycle permit
             *
             */

            wccmypermitpage.clickMotorcyclePermitCancelButton(permitid);

            WCCPermitCancelPage wccpermitcancelpage = new WCCPermitCancelPage(_driver);
            assertTrue("Checking cancel page title", wccpermitcancelpage.getCancelPagetitle().contains("Cancellation request"));
            wccpermitcancelpage.cancelPermitProcess(reason);


            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Cancelled"));

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
