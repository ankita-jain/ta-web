package Permits.WCC.Motorcycle;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCViewPermitDetailsPage;

import com.relevantcodes.extentreports.LogStatus;

public class WCCResidentMotorcyclePermitViewDetailsAfterTempVRNChangeTest extends RingGoScenario {

    String permitid;

    @Test(dataProvider = "dp_wcc_resident_motorcycle_permit_change_vrn_temporarily", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motorcycle_application_details_check"}, dependsOnGroups = {"motorcycle_permit_application_temp_vrn_change"})
    public void residentPermitApplication(String Username, String Password, String vrn_to_edit, String temp_vrn, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("", "WCC- Resident Motorcycle Permit- Temp VRN chnage - Permit Details check");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_MOTO_TXT_FILE);
            System.out.println("Permit Id=" + permitid);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            //assertTrue("Permit valid from =",wccmypermitpage.getPermitValidFrom(permitid).contains(TimeStamp("dd/MM/yyyy")));
            //assertTrue("Permit valid to=",wccmypermitpage.getPermitValidTo(permitid).contains(DateFromOffset("364", "dd/MM/yyyy", false)));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));

            wccmypermitpage.clickViewDetails(permitid);

            WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
            assertTrue("View permit title check", wccviewpermitdetailspage.getViewPermitTitle().contains("View permit: Resident Motorcycle permit "));
            assertTrue("Permit Detail section visible", wccviewpermitdetailspage.isPermitDetailSectionVisible());
            assertTrue("Active permit session visible", wccviewpermitdetailspage.isActivePermitSessionVisible());
            assertTrue("Name and Address visible", wccviewpermitdetailspage.isNameAddressSectionVisible());
            assertTrue("Temporary Vehicle section visible", wccviewpermitdetailspage.isTemporaryVehicleChangeSectionVisible());
            assertTrue("Check permit vechicle", wccviewpermitdetailspage.getCurrentVRNOnPermit().contains(vrn_to_edit));
            assertTrue("Check current temporary vechicle on permit", wccviewpermitdetailspage.getTemporaryVRNOnPermit().contains(temp_vrn.split("\\s+")[0])); //Check VRN, Trim VRN from VRN COLOUR & MAKE
            assertTrue("Proofs section visible", wccviewpermitdetailspage.isProofsSectionVisible());

            /*
             *  Revert the Temporary VRN change
             */

            wccviewpermitdetailspage.clickRevertTemporaryVRNChange();

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
