package Permits.WCC.Motorcycle;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.testng.AssertJUnit.assertTrue;

public class InsightMotorcyclePermitAuthoriseFromZoneChangedTest extends RingGoScenario {
    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
    	NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_insight_motor_cycle_resident_permit_auth_from_zone_change", dataProviderClass = DataProviders.DP_WCC.class, groups = {"insight_motorcycle_permit_Zone_change"}, dependsOnGroups = {"motorcycle_permit_application_edit_zone"})
    public void authorisePermitApplication(String Username, String Password, String Operator_Status, String Operator, String VRM, String Permit_Status, String Change_Permit_Status_To) throws IOException {
        StartTest("", "WCC- Insight - Auth Motorcycle Permit from Zone Changed");


        try {
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue("Operator Loading Page - Insight parking Zone", insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue("Permit application page title", insightwcchomepagetitle.contains("Permit Applications"));
            InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue("Edit permit page title", insightpermitapplicationedipagetitle.contains("Edit a Resident Motorcycle Permit Application"));
            insightpermitapplicationpage.editPermitStatusAndSave(Change_Permit_Status_To);

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            System.out.println("queue title=" + insightpermitworkflowqueuepage.getPageTitle());
            assertTrue("Permit workflow Queue page", insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }
}
