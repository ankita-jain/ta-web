package Permits.WCC.Motorcycle;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.text.ParseException;

import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCResidentPermitProofPage;

import com.relevantcodes.extentreports.LogStatus;

public class Resident_MOTORCYCLE_Permit_Pending_Temp_Permit_Status extends RingGoScenario {

    String permitid;

    @Test(dataProvider = "dp_wcc_resident_motorcycle_temp_permit_all_proofs", dataProviderClass = DataProviders.DP_WCC.class, groups = {"temp_resident_motorcycle_permit_application_upload_full_proofs"}, dependsOnGroups = {"temp_motorcycle_permit_details_via_insight"})
    public void residentPermitApplication(String Username, String Password, String address_proof, String address_proofpath, String address_proof_two, String address_proof_two_path, String insurnace_proof, String insurance_proof_path, String vehicle_proof, String vehicle_proofpath) throws IOException, ParseException {
        StartTest("371", "WCC- Resident Motorcycle Permit- Upload all proofs for Temporary permit ");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_MOTO_TXT_FILE);
            System.out.println("Permit Id=" + permitid);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            wccmypermitpage.clickTempPermitUploadButton(permitid);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Checking proof page title", wccresidentpermitproofpage.getProofPageTitle().contains("Manage proofs"));
            wccresidentpermitproofpage.uploadMoreProofsForTemporaryMotorcyclePermit(address_proof, address_proofpath, address_proof_two, address_proof_two_path, insurnace_proof, insurance_proof_path, vehicle_proof, vehicle_proofpath);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
