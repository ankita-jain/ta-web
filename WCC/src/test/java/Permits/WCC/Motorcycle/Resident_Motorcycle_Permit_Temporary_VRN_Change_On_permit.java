package Permits.WCC.Motorcycle;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class Resident_Motorcycle_Permit_Temporary_VRN_Change_On_permit extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Resident_Motorcycle_Permit_Temporary_VRN_Change_On_permit.class);
    String permitid;

    @Test(dataProvider = "dp_wcc_resident_motorcycle_permit_change_vrn_temporarily", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motorcycle_permit_application_temp_vrn_change"}, dependsOnGroups = {"insight_motorcycle_permit_Zone_change"})
    public void residentEcoPermitTempVrnChange(String Username, String Password, String vrn_to_edit, String New_Vrn, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("12691", "WCC- Resident Motorcycle Permit- Temporary VRN change status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_MOTO_TXT_FILE);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatusFromTooltip().contains("Authorised"));

            wccmypermitpage.clickViewDetails(permitid);

            WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
            assertTrue("View permit title check", wccviewpermitdetailspage.getViewPermitTitle().contains("View permit: Resident Motorcycle permit"));
            wccviewpermitdetailspage.clickManageVehicleButton();

            WCCPermitManageVehiclesPage wccpermitmanagevehiclepage = new WCCPermitManageVehiclesPage(_driver);
            assertTrue("Verify Manage vehicle page title", wccpermitmanagevehiclepage.getManageVehicleTitle().contains("Manage vehicles: Resident motorcycle permit"));
            wccpermitmanagevehiclepage.changeTemporaryVRNChangesProcess(vrn_to_edit, New_Vrn);

            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            assertTrue("Payment page title", wccpaymentcarddetailspage.getPageTitle().contains("Payment"));
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCardInAccount(credit_card, exp_month, exp_year);

            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            assertTrue("Payment page title", wccpaymentcardcvvpage.getPageTitle().contains("Payment"));
            wccpaymentcardcvvpage.enterCV2AndProceedPermitCharges(CV2);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Finish page title check", wccpermitpurchasefinishpage.getPageTitle().contains("Payment"));
            wccpermitpurchasefinishpage.clickFinish();


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
