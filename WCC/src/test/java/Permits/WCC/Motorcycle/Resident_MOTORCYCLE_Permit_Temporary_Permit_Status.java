package Permits.WCC.Motorcycle;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCResidentPermitInfoPage;
import PageObjects.WCCResidentPermitPage;
import PageObjects.WCCResidentPermitProofPage;

import com.relevantcodes.extentreports.LogStatus;

public class Resident_MOTORCYCLE_Permit_Temporary_Permit_Status extends RingGoScenario {

    @Test(dataProvider = "dp_wcc_resident_motorcycle_temp_permit", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motocycle_temp_permit"}, dependsOnGroups = {"motorcycle_permit_cancel"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone, String Residency_type, String Vehicle, String Ownership, String Address_proof_type1, String Address_proof1, String vehicle_registered_country_UK, String vehicle_proof_type2, String Vehicle_proof2, String credit_card, String exp_month, String exp_year, String CV2) throws IOException {
        StartTest("370", "WCC- Resident Motorcycle Permit- Temporary permit status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickMotorCyclePermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident Motorcycle permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Postcode, Address, Parking_zone);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            wccresidentpermitdetailspage.fillPermitApplicationDetailsMotorcylePermit(Vehicle, Ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Checking proof page title", wccresidentpermitproofpage.getProofPageTitle().contains("Resident Motorcycle permit application"));
            wccresidentpermitproofpage.completeTemporaryMotorcyclePermitApplicationProofs(Address_proof_type1, Address_proof1, vehicle_proof_type2, Vehicle_proof2);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            assertTrue("Purchase resident permit page title", wccpermitdetailsconfirmationpage.getConfirmationPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpermitdetailsconfirmationpage.confirmPermitDetails();

            WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
            String purchasepermitpagetitle = wccpurchasepermitpaymenttype.getPageTitle();
            assertTrue(purchasepermitpagetitle.contains("Purchase a resident motorcycle permit"));
            wccpurchasepermitpaymenttype.processPaymentNow();

            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            assertTrue("Payment page title", wccpaymentcarddetailspage.getPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCardInAccount(credit_card, exp_month, exp_year);

            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            assertTrue("Payment page title", wccpaymentcardcvvpage.getPageTitle().contains("Purchase a resident motorcycle permit"));
            wccpaymentcardcvvpage.enterCV2AndProceed(CV2);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            String permitpurchasefinishpagetitle = wccpermitpurchasefinishpage.getPageTitle();
            assertTrue(permitpurchasefinishpagetitle.contains("Purchase a resident motorcycle permit"));
            wccpermitpurchasefinishpage.clickFinish();

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
