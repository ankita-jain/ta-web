package Permits.WCC.Trade;

import DataProviders.PermitDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Purchase_Trade_Permit_via_Corporate_Account extends RingGoScenario {

    private String vehicleVrn, fullVRN;
    private String name = RandomStringUtils.randomAlphabetic(4).toLowerCase();
    private String title ="Mr";
   
	
    @BeforeMethod
    public void setUp() throws Exception {

    	createUser("defaultNewUser.json");
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
        fullVRN = vehicleVrn.toUpperCase()+" WHITE BMW";
    }

    @Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = PermitDP.class)
    public void tradePermitApplication(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "WCC- Trade - Permit");
        try {
     
        	CorporateHelper.addUserToCorporate(data.get("CorporateLogin").toString(), 
        			 			data.get("CorporatePassword").toString(), 
        			 			vehicleVrn, mobile, _driver);
        	
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            wccindexpage.clickPersonalFromLogin();

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            wccloginpage.inputEmailOrPhonenumer(email);
            wccloginpage.enterPassword(password);
            wccloginpage.clickLoginButton();

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickTradePermit();

            WCCTradePermitInfoPage wcctradepermitinfopage = new WCCTradePermitInfoPage(_driver);
            assertTrue("Trade permit info page title check", wcctradepermitinfopage.getTradePermitTitle().contains("Trades permit application: eligibility and process"));
            wcctradepermitinfopage.clickTradePermitApply();

            WCCTradePermitApplicationPage wcctradepermitapplicationpage = new WCCTradePermitApplicationPage(_driver);
            assertTrue("Trades permit application title check", wcctradepermitapplicationpage.getTradePermitApplicationPageTitle().contains("Trades permit application"));
            wcctradepermitapplicationpage.applyTradePermit(title,
            											   name,
            											   name,
            											   data.get("zone").toString(), 
            											   data.get("reason").toString(), 
            											   fullVRN);

            WCCTradePermitDetailsPage wcctradepermitdetailspage = new WCCTradePermitDetailsPage(_driver);
            assertTrue("Trades permit details page title", wcctradepermitdetailspage.getTradePermitDetailsPageTitle().contains("Purchase a trades permit"));
            wcctradepermitdetailspage.fillTradePermitDetails(data.get("duration").toString());

            WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
            assertTrue("Trade permit payment page title", wccpurchasepermitpaymenttype.getPageTitle().contains("Purchase a trades permit"));
            wccpurchasepermitpaymenttype.payViaCorporate();

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            assertTrue("Trade permit payment finish details", wccpermitpurchasefinishpage.getPageTitle().contains("Purchase a trades permit"));
            wccpermitpurchasefinishpage.clickFinish();

            //user logout
            wcchome.logout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
