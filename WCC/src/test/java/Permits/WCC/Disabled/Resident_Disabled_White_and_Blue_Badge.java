package Permits.WCC.Disabled;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

/*
 * Disable resident permit - Blue Badge - Child under 2 years  of age
 * This test can run for all Disable resident permit with Blue badge,
 * the only difference is the 'WCCDisableResidentPermitDetailsPage' fields changes with respect to the 'Disability type'
 * */

public class Resident_Disabled_White_and_Blue_Badge extends RingGoScenario {
    @Test(dataProvider = "dp_wcc_disable_permit_white_blue_badge", dataProviderClass = DataProviders.DP_WCC_DISABLE.class, groups = {"disable_resident_permit_white_blue_badge_application"})
    public void residentDisablePermitApplication(String Username, String Password, String Title,
                                                 String Firstname, String Surname, String Address_line_1, String Town,
                                                 String County, String Postcode, String Gender, String Telephone_number,
                                                 String National_insurnace_number, String dob_date, String dob_month,
                                                 String dob_year, String badge_type, String disability_type, String mobility_assessment, String Vrm,
                                                 String driver_or_passenger, String vehiclekeepername, String address1, String address2, String address3,
                                                 String Vehicle_keeper_town, String Vehicle_keeper_county, String Vehicle_keeper_postcode,
                                                 String does_child_need_medical_equipment, String medical_equipment, String does_child_need_vehicle_near, String medical_condition,
                                                 String proof_of_add_type_one, String proof_of_add1, String proof_of_add_type_two, String proof_of_add2,
                                                 String proof_of_eligibility, String other_proof_types, String other_proofs) throws IOException {
        StartTest("380", "WCC- Resident Disable Permit - White & Blue Badge");

        try {
            NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue("Home Page title", pageTitle.contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            CheckResult(wccpermitpage.clickDisablePermitTile(), "Click Disable permit button");


            WCCDisablePermitPage wccdisablepermitpage = new WCCDisablePermitPage(_driver);
            wccdisablepermitpage.clickResidentDisablePermit();

            WCCDisableResidentPermitApplicationPage wcdisableresidentpermitapplicationpage = new WCCDisableResidentPermitApplicationPage(_driver);
            wcdisableresidentpermitapplicationpage.fillApplicantDetailsWhiteAndBlue(Title, Firstname, Surname,
                    Address_line_1, Town, County, Postcode, Telephone_number,
                    National_insurnace_number, dob_date, dob_month, dob_year, badge_type, disability_type,
                    mobility_assessment, Vrm, driver_or_passenger, vehiclekeepername,
                    address1, address2, address3, Vehicle_keeper_town, Vehicle_keeper_county, Vehicle_keeper_postcode);

            WCCDisableResidentPermitDetailsPage wccdisableresidentpermitdetailspage = new WCCDisableResidentPermitDetailsPage(_driver);
            wccdisableresidentpermitdetailspage.fillDisabilityDetailsOfChildUnder2Yrs(does_child_need_medical_equipment, medical_equipment, does_child_need_vehicle_near, medical_condition);

            WCCDisablePermitProofPage wccdisablepermitproofpage = new WCCDisablePermitProofPage(_driver);
            wccdisablepermitproofpage.uploadProofsBlueBadge(proof_of_add_type_one, proof_of_add1, proof_of_add_type_two, proof_of_add2, proof_of_eligibility, other_proof_types, other_proofs);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            wccpermitpurchasefinishpage.clickFinish();
        } catch (AssertionError e) {

            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }

}
