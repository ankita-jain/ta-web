package Permits.WCC.Disabled;

import GenericComponents.BaseClass;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

/*
 * Disable resident permit - Blue Badge - Child under 2 years  of age
 * This test can run for all Disable resident permit with Blue badge,
 * the only difference is the 'WCCDisableResidentPermitDetailsPage' fields changes with respect to the 'Disability type'
 * */

public class Resident_Disable_Blue_Badge extends RingGoScenario {
    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);

    @Test(dataProvider = "dp_wcc_disable_permit", dataProviderClass = DataProviders.DP_WCC_DISABLE.class, groups = {"disablepermitapplication"})
    public void residentDisablePermitApplication(String Username, String Password, String Title,
                                                 String Firstname, String Surname, String Address_line_1, String Town,
                                                 String County, String Postcode, String Gender, String Telephone_number,
                                                 String National_insurnace_number, String dob_date, String dob_month,
                                                 String dob_year, String badge_type, String disability_type, String mobility_assessment,
                                                 String does_child_need_medical_equipment, String medical_equipment, String does_child_need_vehicle_near, String medical_condition,
                                                 String proof_of_add_type_one, String proof_of_add1, String proof_of_add_type_two, String proof_of_add2,
                                                 String proof_of_eligibility, String other_proof_types, String other_proofs) throws IOException {
       

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            wccindexpage.clickPersonalFromLogin();

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue("Home Page title", pageTitle.contains("Log in"));

            wccloginpage.inputEmailOrPhonenumer(Username);
            wccloginpage.enterPassword(Password);
            wccloginpage.clickLoginButton();

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickDisablePermit();

            WCCDisablePermitPage wccdisablepermitpage = new WCCDisablePermitPage(_driver);
            wccdisablepermitpage.clickResidentDisablePermit();

            WCCDisableResidentPermitApplicationPage wcdisableresidentpermitapplicationpage = new WCCDisableResidentPermitApplicationPage(_driver);
            wcdisableresidentpermitapplicationpage.fillApplicantDetails(Title, Firstname, Surname, Address_line_1, Town, County, Postcode, Telephone_number, National_insurnace_number, dob_date, dob_month, dob_year, badge_type, disability_type, mobility_assessment);

            WCCDisableResidentPermitDetailsPage wccdisableresidentpermitdetailspage = new WCCDisableResidentPermitDetailsPage(_driver);
            wccdisableresidentpermitdetailspage.fillDisabilityDetailsOfChildUnder2Yrs(does_child_need_medical_equipment, medical_equipment, does_child_need_vehicle_near, medical_condition);

            WCCDisablePermitProofPage wccdisablepermitproofpage = new WCCDisablePermitProofPage(_driver);
            wccdisablepermitproofpage.uploadProofsBlueBadge(proof_of_add_type_one, proof_of_add1, proof_of_add_type_two, proof_of_add2, proof_of_eligibility, other_proof_types, other_proofs);

            WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
            wccpermitpurchasefinishpage.clickFinish();
        } catch (AssertionError e) {
        	 LogError("Failed due -> " + e.getMessage());
             Log.add(LogStatus.FAIL, "Test is failed");
             Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }

}
