package Permits.WCC.Disabled;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static Utilities.FileUtils.writeToTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class InsightDisableResidentPermitWhiteAndBlueBadgeAuthoriseTest extends RingGoScenario {

    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
        NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_insight_wcc_disable_resident_permit_white_blue_details", dataProviderClass = DataProviders.DP_WCC_DISABLE.class, groups = {"authorise_disable_permit_white_blue_badge_via_insight"}, dependsOnGroups = {"disable_resident_permit_white_blue_badge_application"})
    public void authorisePermitApplication(String Username, String Password, String Operator_Status, String Operator, String VRM, String Permit_Status, String Change_Permit_Status_To) throws IOException {
        StartTest("380", "WCC- Insight - Insight Disable resident permit white & Blue badge");

        try {
            String permit_number;
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            permit_number = InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            System.out.println("WCC Disable Resident Permit (White & BLUE BADGE)=" + permit_number);

            /*
             * Write permit_number(PermitId) to a local storage for further use.
             *
             */

            writeToTestDataFile(permit_number, RESIDENT_WHITE_AND_BLUE_TXT_FILE);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Westminster Disabled Badge (Resident) Permit Application"));
            insightpermitapplicationpage.editPermitStatusAndSave(Change_Permit_Status_To);

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            assertTrue(insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
