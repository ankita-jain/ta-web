package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;


public class RingGoMobileHomePage extends Base_Page {

    @FindBy(xpath = "//li[2]/a[@href='/bookparking']")
    private WebElement parking;
    
    @FindBy(xpath = "//li[2]/a[@href='/profile']")
    private WebElement account;

    @FindBy(xpath = "//li[2]/a[@href='/logout']")
    private WebElement logout;

    @FindBy(xpath = "//li[2]/a[contains(@href,'/statements')]")
    private WebElement statementsLink;

    public RingGoMobileHomePage(WebDriver driver) {
        super(driver);
    }

    public Result clickParking() {
        WaitVisible(parking);
        return ClickControl(parking, "Parking button");
    }
    
    public Result clickAccount() {
        WaitVisible(account);
        return ClickControl(account, "Parking button");
    }


    public Result clickLogout() {
        WaitVisible(logout);
        return ClickControl(logout, "Logout button");
    }

    public Result clickStatements() {
        WaitVisible(statementsLink);
        return ClickControl(statementsLink, "Statements link");
    }
}
