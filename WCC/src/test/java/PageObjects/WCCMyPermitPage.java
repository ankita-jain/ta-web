package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static Utilities.TimerUtils.delay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCMyPermitPage extends PageClass {

    public WebDriver driver;
    private String permitId = "//*[contains(text(), 'xxxxxx')]";
    private String permit_valid_from = "//*[contains(text(),'xxxxxx')]//following::td[2]";
    private String permit_status = "//tr['xxxxxx']//td[2]";
    private String permit_valid_to = "//*[contains(text(),'xxxxxx')]//following::td[6]";
    private String view_details = "//*[contains(text(), 'xxxxxx')]//following::div[1]/ul/li[1]/a";
    private String order_replacement_or_renew_permit = "//*[contains(text(), 'xxxxxx')]//following::div[1]/ul/li[2]/a";
    public String cancel_permit = "//*[contains(text(), 'xxxxxx')]//following::div[1]/ul/li[3]/a";
    private String temp_permit_upload_proofs = "//*[@href='/editapplication/edit/xxxxxx/1/3']";
    private String motorcycle_permit_cancel = "//*[contains(text(), 'xxxxxx')]//following::div[1]/ul/li[2]/a";
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pageTitle;
    @FindBy(xpath = "//*[contains(text(), '� View details')]")
	public WebElement viewDetails;
    @FindBy(xpath = "//*[contains(text(), '� Cancel permit')]")
    @CacheLookup
	public WebElement cancelPermit;
    @FindBy(xpath = "//*[contains(text(), 'Awaiting payment')]")
    public WebElement awaiting_payment;
    private String permitTable = "//tr[xxxxxx]//td[pppppp]";
    public String cancel_pending_permit = "//*[contains(text(), 'xxxxxx')]//following::div[1]/ul/li[2]/a";
    @FindBy(xpath = "//a[@href='/permits']")
    @CacheLookup
	public WebElement permitLink;
    @FindBy(xpath = "//a[@class='button']")
    @CacheLookup
    private WebElement upload_button;
    @FindBy(id = "//*[contains(text(), 'This permit is in an interim state until')")
    @CacheLookup
    private WebElement interim_message;
    @FindBy(xpath = "//a[@href='/home']")
    @CacheLookup
	public WebElement homeBreadCrumb;
    @FindBy(xpath = "//*[contains(text(), '� Renew permit')]")
	public WebElement RenewPermit;
    @FindBy(xpath = "//*[contains(text(), 'Your replacement permit request has been submitted for approval')]")
    @CacheLookup
	public WebElement ReplacementMessage;
    
    @FindBy(xpath = "//a[@class='ringgo-tooltip']")
    public WebElement status_tool_tip;
    
    public WCCMyPermitPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getMyPermitPageTitle() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public String getPermitStatus(String permitid) {
        WebElement permitStatus = prepareWebElementWithDynamicXpath(permit_status, permitid);
        return permitStatus.getText();
    }
    
    public String getPermitStatusFromTooltip() {
    	WaitVisible(status_tool_tip);
        return status_tool_tip.getText();
    }

    public String getPermitValidFrom(String permitid) {
        WebElement permitValidFrom = prepareWebElementWithDynamicXpath(permit_valid_from, permitid);
        return permitValidFrom.getText();
    }

    public String getPermitValidTo(String permitid) {
        WebElement permitValidTo = prepareWebElementWithDynamicXpath(permit_valid_to, permitid);
        return permitValidTo.getText();
    }

    public void clickViewDetails(String permitid) {
        WebElement permitViewDetails = prepareWebElementWithDynamicXpath(view_details, permitid);
        permitViewDetails.click();
    }

    public void clickOrderReplacement(String permitid) {
        WebElement permitViewDetails = prepareWebElementWithDynamicXpath(order_replacement_or_renew_permit, permitid);
        permitViewDetails.click();
    }

    public void clickCancelPermit(String permitid) {
        WebElement permitViewDetails = prepareWebElementWithDynamicXpath(cancel_pending_permit, permitid);
        permitViewDetails.click();
    }
    
    public void clickCancelResidentPermit(String permitid) {
        WebElement permitViewDetails = prepareWebElementWithDynamicXpath(cancel_permit, permitid);
        permitViewDetails.click();
    }

    public boolean isPermitAwaitingPayment() {
         return awaiting_payment.isDisplayed();
    }
    
    public boolean isUploadButtonVisible(String permitid) {
        WebElement temp_permi_uplaod_proofs_button = prepareWebElementWithDynamicXpath(temp_permit_upload_proofs, permitid);
        return temp_permi_uplaod_proofs_button.isDisplayed();
    }

    public void clickTempPermitUploadButton(String permitid) {
        WebElement temp_permi_uplaod_proofs_button = prepareWebElementWithDynamicXpath(temp_permit_upload_proofs, permitid);
        temp_permi_uplaod_proofs_button.click();
    }

    public void clickMotorcyclePermitCancelButton(String permitid) {
        WebElement cancel_button = prepareWebElementWithDynamicXpath(motorcycle_permit_cancel, permitid);
        cancel_button.click();
    }
    
    public void selectViewDetails() {
    	WaitVisible(viewDetails);
    	jsClick(viewDetails);
    }
    
    public String getPermitId(String permit, String another) {
    	
    	String p = permitTable.replace("xxxxxx", permit).replace("pppppp", another);
    	return driver.findElement(By.xpath(p)).getText();
    }
    
    public Result clickPermitBreadCrumb() {
        WaitVisible(permitLink);
        return ClickControl(permitLink, "permit link clicked");
    }
    
    public boolean uploadButtonIsdisplayed() {
		WaitVisible(upload_button);
		return upload_button.isDisplayed();
	}
    
    public boolean interimMessageIsdisplayed() {
		WaitVisible(interim_message);
		return interim_message.isDisplayed();
	}
    
    public Result clickUploadButton() {
		WaitVisible(upload_button);
		return ClickControl(upload_button, "upload button clicked");
	}
    
    public Result clickHomeBreadCrumb() {
        WaitVisible(homeBreadCrumb);
        return ClickControl(homeBreadCrumb, "home link clicked");
    }
    
    public void selectRenewPermit() {
    	WaitVisible(RenewPermit);
    	jsClick(RenewPermit);
    }
    
    public String getReplaceMentSuccessMessage() {
    	WaitVisible(ReplacementMessage);
        return ReplacementMessage.getText();
    }
}