package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCChangeSecurityQuestion extends Base_Page {
    @FindBy(xpath = "//select[@name='SecretQuestion']")
    private WebElement questionSelect;

    @FindBy(xpath = "//input[@id='field-SecretAnswer']")
    private WebElement secretAnswerInput;

    @FindBy(xpath = "//input[@id='field-Password']")
    private WebElement yourPasswordInput;

    @FindBy(xpath = "//input[@name='submits[submit]']")
    private WebElement saveButton;

    public WCCChangeSecurityQuestion(WebDriver driver) {
        super(driver);
    }

    public Result pasteSecretAnswer(String answer) {
        WaitVisible(secretAnswerInput);
        return enterText(secretAnswerInput, answer, "Secret answer input");
    }

    public Result pastePassword(String password) {
        WaitVisible(yourPasswordInput);
        return enterText(yourPasswordInput, password, "Password input");
    }

    public Result clickSave() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button");
    }

    public Result chooseQuestionByIndex(int index) {
        WaitVisible(questionSelect);
        return selectItemFromDropdownByIndex(questionSelect, index, "Question select");
    }
}
