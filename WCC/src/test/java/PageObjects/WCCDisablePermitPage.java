package PageObjects;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCDisablePermitPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='permitwidget'][1]/a")
    @CacheLookup
    private WebElement residentdisablepermit;
    @FindBy(xpath = "//*[@class='permitwidget'][2]/a")
    @CacheLookup
    private WebElement nonresidentdisablepermit;
    @FindBy(xpath = "//*[@class='permitwidget'][3]/a")
    @CacheLookup
    private WebElement organisationdisablepermit;

    public WCCDisablePermitPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickResidentDisablePermit() {
        return ClickControl(residentdisablepermit, "Resident Disable permit button");
    }

    public Result clickNoNResidentDisablePermit() {
        return ClickControl(nonresidentdisablepermit, "Non Resident Disable permit button");
    }

    public Result clickOrganisationDisablePermit() {
        return ClickControl(organisationdisablepermit, "Resident Disable permit button");
    }


}
