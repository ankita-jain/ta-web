package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCPermitCancelPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement cancel_permit_page_title;
    @FindBy(xpath = "//*[@id='notes']")
    @CacheLookup
    private WebElement cancel_reason;
    @FindBy(xpath = "//*[@name='confirm-understanding']")
    @CacheLookup
    private WebElement cancel_tAndc;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement cancel_button;

    public WCCPermitCancelPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCancelPagetitle() {
        WaitVisible(cancel_permit_page_title);
        return cancel_permit_page_title.getText();
    }

    public void enterCancelReason(String reason) {
        WaitVisible(cancel_reason);
        cancel_reason.sendKeys(reason);
    }

    public void clickCancelTandC() {
        jsClick(cancel_tAndc);
    }

    public void clickCancelPermitButton() {
        WaitVisible(cancel_button);
        cancel_button.click();
    }

    public void cancelPermitProcess(String reason) {
        enterCancelReason(reason);
        clickCancelTandC();
        clickCancelPermitButton();

    }

}
