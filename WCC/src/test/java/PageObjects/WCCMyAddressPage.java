package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCMyAddressPage extends Base_Page {

    private String myAddressEditButtonPath = "//*[contains(text(), '%s')]//following::td[2]";
    private String myAddressDeleteButtonPath = "//*[contains(text(), '%s')]//following::td[3]";
    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    private WebElement myAddressPageTitle;
    @FindBy(xpath = "//a[@href='/useraddress/add']")
    private WebElement addAddress;
    @FindBy(xpath = "//*[@class='success-notification']")
    private WebElement successNotification;
    @FindBy(xpath = "//div[@id='content']")
    private WebElement myAddressesContent;

    public WCCMyAddressPage(WebDriver driver) {
        super(driver);
    }

    public String getAddressPageTitle() {
        WaitVisible(myAddressPageTitle);
        return myAddressPageTitle.getText();
    }

    public String getSuccessNotification() {
        WaitVisible(successNotification);
        return successNotification.getText();
    }

    public boolean isAddAddressButtondispalyed() {
        WaitVisible(addAddress);
        return addAddress.isDisplayed();
    }

    public Result clickAddAddressButton() {
        WaitVisible(addAddress);
        return ClickControl(addAddress, "Add address link");
    }

    public Result clickEditAddress(String address) {
        WebElement cardEditButton = _driver.findElement(By.xpath(String.format(myAddressEditButtonPath, address)));
        return ClickControl(cardEditButton, "Edit card button");
    }

    public Result clickDeleteAddress(String address) {
        WebElement cardDeleteButton = _driver.findElement(By.xpath(String.format(myAddressDeleteButtonPath, address)));
        return ClickControl(cardDeleteButton, "Delete card button");
    }

    public String getTextFromPage() {
        WaitVisible(myAddressesContent);
        return myAddressesContent.getText();
    }
}