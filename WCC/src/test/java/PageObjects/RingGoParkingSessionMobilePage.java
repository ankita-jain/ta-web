package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.checkCheckBox;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByContainsText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import Structures.Result;

public class RingGoParkingSessionMobilePage extends Base_Page {
    @FindBy(xpath = "//*[@id='content']/h1")
    private WebElement parkingSessionPageTitle;
    @FindBy(xpath = "//*[contains(text(),'Book New Session')]//preceding::input[1]")
    private WebElement book_new;
    @FindBy(xpath = "//*[@name='labyrinth_Extend_next']")
    private WebElement next_button_extend;
    @FindBy(xpath = "//input[@class='extendEnabled']")
    private WebElement extendSessionRadioButton;
    @FindBy(xpath = "//*[@id='zone']")
    private WebElement zone;
    @FindBy(xpath = "//*[@id='vehicleDropDown']")
    private WebElement vrn;
    @FindBy(xpath = "//*[@id='labcancel']")
    private WebElement cancel_button;
    @FindBy(xpath = "//*[@name='labyrinth_ZoneVehicleSelect_next']")
    private WebElement nextButton;
    @FindBy(xpath = "//*[@id='tariffdropdown']")
    private WebElement tariff;
    @FindBy(xpath = "//*[@name='SMSConfirm']")
    private WebElement sms_checkbox;
    @FindBy(xpath = "//*[@name='SMSEnd']")
    private WebElement reminder_sms;
    @FindBy(xpath = "//*[@name='labyrinth_Duration_back']")
    private WebElement previous_page_button;
    @FindBy(xpath = "//*[@name='labyrinth_Duration_next']")
    private WebElement duration_next_button;
    @FindBy(xpath = "//*[contains(text(),'Take payment now')]//preceding::input[1]")
    private WebElement take_payment_now;
    @FindBy(xpath = "//*[contains(text(),'Yes')]//preceding::input[1]")
    private WebElement corporate_pay;
    @FindBy(xpath = "//*[@name='labyrinth_CorpPayment_next']")
    private WebElement corporate_pay_next;
    @FindBy(xpath = "//*[@name='labyrinth_CorpConfirm_next']")
    private WebElement corporate_confirm_next;
    @FindBy(xpath = "//*[@name='labyrinth_Basket_next']")
    private WebElement next_button_basket;
    @FindBy(xpath = "//a[contains(@class, 'ringgo-tooltip-click')]")
    private WebElement diesel_surcharge;
    @FindBy(xpath = "//span[@class='tooltiptext']")
    private WebElement diesel_surcharge_tool_tip;
    @FindBy(xpath = "//*[contains(text(),'Existing Card')]//preceding::input[1]")
    private WebElement existing_card;
    @FindBy(xpath = "//*[@id='field-cardSelect']")
    private WebElement card_select;
    @FindBy(xpath = "//*[@name='labyrinth_CardSelect_next']")
    private WebElement card_select_next_button;
    @FindBy(xpath = "//*[@id='field-cv2']")
    private WebElement cv2;
    @FindBy(xpath = "//*[@name='labyrinth_Payment_next']")
    private WebElement pay_button;
    @FindBy(xpath = "//*[@name='labyrinth_Finish_next']")
    private WebElement finish_button;
    @FindBy(xpath = "//*[@id='tandcCheck']")
    private WebElement terms_and_condition;
    @FindBy(xpath = "//ul[contains(@class,'activesession')]")
    private List<WebElement> listExtendSessions;
    private String extendSessionList = "./li[contains(text(),'%s')]";
    @FindBy(xpath = "//input[@name='timescale[when]']")
    private List<WebElement> whereToParkRadioButton;
    @FindBy(xpath = "//input[@name='labyrinth_AdvanceParkingOption_next']")
    private WebElement whenToParkNextButton;
    @FindBy(xpath = "//input[@name='securityCode']")
    private WebElement cvvInput;
    @FindBy(xpath = "//*[@id='paymentConfirmButton']")
    private WebElement pay_amount_button;

    public RingGoParkingSessionMobilePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getListExtendSessions(int index) {
        return listExtendSessions.get(index);
    }

    public WebElement getStartedOrExpiresTime(int indexOfActiveSessions, String startedOrExpiresDate) {
        return getListExtendSessions(indexOfActiveSessions).findElement(By.xpath(String.format(extendSessionList, startedOrExpiresDate)));
    }

    public String getParkingSessionPageTitle() {
        WaitVisible(parkingSessionPageTitle);
        return parkingSessionPageTitle.getText();
    }

    public boolean isBookNewVisible() {
        return WaitVisible(book_new);
    }

    public Result clickTermsAndCondition() {
        WaitVisible(terms_and_condition);
        return ClickControl(terms_and_condition, "Finish button");
    }

    public Result clickBookNew() {
        WaitVisible(book_new);
        return ClickControl(book_new, "Finish button");
    }

    public Result selectLastSessionToExtend() {
        WaitVisible(extendSessionRadioButton);
        return ClickControl(extendSessionRadioButton, "Extend session button");
    }

    public Result clickExtendNextButton() {
        WaitVisible(next_button_extend);
        return ClickControl(next_button_extend, "Extend button");
    }

    public Result enterZone(String zoneid) {
        WaitVisible(zone);
        return enterText(zone, zoneid, "Zone id");
    }

    public Result selectVehicle(String vehicle) {
        WaitVisible(vrn);
        return selectItemFromDropdownByContainsText(vrn, vehicle, "Vehicle selection");
    }

    public Result clickNextButton() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }

    public <T> Result chooseWhenToPark(T value) {
        WebElement radioButton = whereToParkRadioButton
                .stream()
                .filter(x -> x.getAttribute("value").equals(value.toString()))
                .findFirst()
                .get();
        return ClickControl(radioButton, "Where to park radio button");
    }

    public Result whenToParkNext() {
        WaitVisible(whenToParkNextButton);
        return ClickControl(whenToParkNextButton, "Next button");
    }

    public Result selectDuration(String time) {
        WaitVisible(tariff);
        return selectItemFromDropdownByTextValue(tariff, time, "Tariff selection");
    }

    public Result selectSMSConfirm() {
        WaitVisible(sms_checkbox);
        return checkCheckBox(sms_checkbox, "Reminder SMS checkbox");
    }

    public Result selectReminderSMS() {
        WaitVisible(reminder_sms);
        return checkCheckBox(reminder_sms, "Reminder SMS checkbox");
    }

    public Result clickDurationNextButton() {
        WaitVisible(duration_next_button);
        return ClickControl(duration_next_button, "Duration next button");
    }

    public Result clickTakePaymentNow() {
        WaitVisible(take_payment_now);
        return ClickControl(take_payment_now, "Take payment now button");
    }

    public Result clickCorporatePay() {
        WaitVisible(corporate_pay);
        return ClickControl(corporate_pay, "Corporate pay button");
    }

    public Result clickCorporatePayNext() {
        WaitVisible(corporate_pay_next);
        return ClickControl(corporate_pay_next, "Corporate pay next button");
    }

    public Result clickCorporateConfirmNext() {
        WaitVisible(corporate_confirm_next);
        return ClickControl(corporate_confirm_next, "Corporate confirm next button");
    }

    public Result clickBasketNextButton() {
        WaitVisible(next_button_basket);
        return ClickControl(next_button_basket, "Next button");
    }

    public boolean isDieselSurchargeDisplayed() {
        return WaitVisible(diesel_surcharge);
    }

    public Result clickDieselSurchagre() {
        WaitVisible(diesel_surcharge);
        return ClickControl(diesel_surcharge, "Finish button");
    }

    public String getDieselSurchargeToolTipText() {
        WaitVisible(diesel_surcharge_tool_tip);
        return diesel_surcharge_tool_tip.getText().replaceAll("\n", " ");
    }

    public Result clickExistingCard() {
        WaitVisible(existing_card);
        return ClickControl(existing_card, "Existing card button");
    }

    public Result selectCard(String card_details) {
        WaitVisible(card_select);
        return selectItemFromDropdownByContainsText(card_select, card_details, "Card details");
    }

    public Result clickCardSelectNextButton() {
        WaitVisible(card_select_next_button);
        return ClickControl(card_select_next_button, "Next button");
    }

    public Result enterCV2(String cvv) {
        WaitVisible(cv2);
        return enterText(cv2, cvv, "CVV code");
    }

    public Result clickPay() {
        WaitVisible(pay_button);
        return ClickControl(pay_button, "Pay button");
    }

    public Result clickFinish() {
        WaitVisible(finish_button);
        return ClickControl(finish_button, "Finish button");
    }
    
    public Result enterCVV(String cvv) {
        WaitVisible(cvvInput);
        return enterText(cvvInput, cvv, "CVV input");
    }


    public Result clickPayConfirm() {
        WaitVisible(pay_amount_button);
        return ClickControl(pay_amount_button, "Click Pay button");
    }

    public void parkingSessionProcess(String zoneid, String vehicle, String time, String card_details, String cvv) {
        enterZone(zoneid);
        selectVehicle(vehicle);
        clickNextButton();
        selectDuration(time);
        selectSMSConfirm();
        selectReminderSMS();
        clickDurationNextButton();
        clickTakePaymentNow();
        clickBasketNextButton();
        clickExistingCard();
        selectCard(card_details);
        clickCardSelectNextButton();
        enterCV2(cvv);
        clickPay();
        clickFinish();
    }

    public enum WhenToPark {
        NOW("1"),
        LATER("2");

        private String value;

        WhenToPark(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public class SessionActiveBlock {
        @FindBy(xpath = "//ul[contains(@class,'activesession')]")
        private List<WebElement> activeSessionBlocksInfo;


    }
}