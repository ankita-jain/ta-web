package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import PageObjects.POHelpers.PaymentBasketTable;
import Structures.Result;

public class WCCBasketConfirmationPage extends PageClass{
	
	
	public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement confirmBasketPageTitle;
    @FindBy(id="labyrinth_Payment_back")
    @CacheLookup
    private WebElement previousButton;
    @FindBy(id="labyrinth_cancel")
    @CacheLookup
    private WebElement cancelBasket;
    @FindBy(id="labyrinth_Payment_next")
    @CacheLookup
    private WebElement payBasket;
    
  

    public WCCBasketConfirmationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getConfirmBasketPageTitle() {
        WaitVisible(confirmBasketPageTitle);
        return confirmBasketPageTitle.getText();
    }

    public void clickPreviousButton() {
        jsClick(previousButton);
       }
    
    public String cancelBasketPayment() {
        WaitVisible(cancelBasket);
        return cancelBasket.getText();
    }

    public Result confirmPayViaBasket() {
        WaitVisible(payBasket);
        return ClickControl(payBasket, "Pay Via Basket");
        
    }
   


}
