package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class ForgetPassword extends Base_Page { 

    @FindBy(xpath = "//input[@name='Identifier']") 
    private WebElement numberOrEmailAddressInput;
    
    @FindBy(className="error") 
    private WebElement errors;
    
    @FindBy(xpath = "//*[@value='vrm']")
    private WebElement provide_any_vehicle_registration_on_account;
    
    @FindBy(xpath = "//*[@value='secretAnswer']")
    private WebElement answer_my_secret_answer;
    
    @FindBy(xpath = "//*[@value='sms']")
    private WebElement sms;
    
    @FindBy(xpath = "//*[@value='archive']")
    private WebElement archive;
    
    @FindBy(xpath = "//input[@name='VRN']")
    private WebElement vehicleVrnInput;
    
    @FindBy(xpath = "//input[@name='secretAnswer']")
    private WebElement secretAnswer;

    @FindBy(xpath = "//input[@name='labyrinth_accountInfo_next']")
    private WebElement nextButton;
    
    @FindBy(xpath = "//*[@id='labyrinth_securityCheck_next']")
    private WebElement securityCheckNextButton;
    
    @FindBy(xpath = "//*[@id='labyrinth_verificationChoice_next']")
    private WebElement verifyAccountNextButton;  
    
    @FindBy(xpath = "//*[@id='labyrinth_archiveVerificationTransportation_next']")
    private WebElement verifyAccountOnArchiveNextButton;

    @FindBy(xpath = "//ul[@class='error-notification']")
    private WebElement errorNotification;
    
    @FindBy(xpath = "//ul[@class='warning-notification']")
    private WebElement warningNotification;

    @FindBy(xpath = "//*[@class='page-head']")
    private WebElement pageHeader;

    public ForgetPassword(WebDriver driver) {
        super(driver);
    }

    public Result pasteUserIdentifier(String identifier) {
        WaitVisible(numberOrEmailAddressInput);
        return enterText(numberOrEmailAddressInput, identifier, "Email/CLI input");
    }

    public Result pasteVehicleVrn(String vrn) {
        WaitVisible(vehicleVrnInput);
        return enterText(vehicleVrnInput, vrn, "VRN input");
    }
    
    public Result pasteSecretQuestion(String question) {
        WaitVisible(secretAnswer);
        return enterText(secretAnswer, question, "Secrete question input");
    }
    
    public Result clickSMS() {
        WaitVisible(sms);
        return ClickControl(sms, "Send SMS");
    }
    
    public Result clickArchiveAccount() {
        WaitVisible(archive);
        return ClickControl(archive, "Send SMS");
    }
    
    public Result clickProvideAnyVRNOnAccount() {
        WaitVisible(provide_any_vehicle_registration_on_account);
        return ClickControl(provide_any_vehicle_registration_on_account, "Provide any vehicle registration on my account");
    }
    
    public boolean isProvideAnyVRNonAccountVisible() {
    	return WaitVisible(provide_any_vehicle_registration_on_account);
    }
    
    public Result clickAnswerMySecreteQuestion() {
        WaitVisible(answer_my_secret_answer);
        return ClickControl(answer_my_secret_answer, "Answer my question");
    }
    
    public boolean isAnswerMySQVisbile() {
    	return WaitVisible(answer_my_secret_answer);
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }
    
    public Result securityCheckNextButtonClick() {
        WaitVisible(securityCheckNextButton);
        return ClickControl(securityCheckNextButton, "Next button");
    }
    
    public Result verifyAccountNextButtonClick() {
        WaitVisible(verifyAccountNextButton);
        return ClickControl(verifyAccountNextButton, "Next button");
    }
    
    public Result verifyAccountOnArchiveNextButtonClick() {
        WaitVisible(verifyAccountOnArchiveNextButton);
        return ClickControl(verifyAccountOnArchiveNextButton, "Next button");
    }
    
    public String getErrorMessages() {
        WaitVisible(errors);
        return errors.getText();
    }
    
    public String getErrorNotificationText() {
        WaitVisible(errorNotification);
        return errorNotification.getText();
    }
    
    public String getWarningNotificationText() {
        WaitVisible(warningNotification);
        return warningNotification.getText();
    }

    public String getForgetPasswordHeader() {
        WaitVisible(pageHeader);
        return pageHeader.getText();
    }
}
