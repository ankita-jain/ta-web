package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class WCCViewPermitDetailsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement viewpermittitle;
    @FindBy(xpath = "//*[contains(text(),'Edit address')]")
    @CacheLookup
    private WebElement editAddressButton;
    @FindBy(xpath = "//*[@id='templet-container']/li[2]/div/div[2]/div[2]/ul/li[3]/a")
    @CacheLookup
    private WebElement manageVehiclesButton;
    @FindBy(xpath = "//*[@id='revert_button']")
    @CacheLookup
    private WebElement revert_button;
    @FindBy(xpath = "//*[@id='header1']")
    @CacheLookup
    private WebElement permitDetailsSection;
    @FindBy(xpath = " //*[@id='activepermitsession']")
    @CacheLookup
    private WebElement acivePermitSession;
    @FindBy(xpath = "//*[@id='header2']")
    @CacheLookup
    private WebElement name_address_section;
    @FindBy(xpath = "//*[@id='header3']")
    @CacheLookup
    private WebElement temporary_vehicle_change_section;
    @FindBy(xpath = "//*[@id='vehicles']")
    @CacheLookup
    private WebElement vehicles_section;
    @FindBy(xpath = "//*[@id='proofs']")
    @CacheLookup
    private WebElement proofs_section;
    @FindBy(xpath = "/ //*[contains(text(),'Order a replacement')]")
    private WebElement order_replacement;
    @FindBy(xpath = " //*[contains(text(),'Cancel permit')]")
    @CacheLookup
    private WebElement cancel_permit;
    @FindBy(xpath = "//*[contains(text(),'Permit vehicle')]//following::div[1]")
    @CacheLookup
    private WebElement permit_vrn;
    @FindBy(xpath = " //*[contains(text(),'Current temporary vehicle')]//following::div[1]")
    @CacheLookup
    private WebElement currently_temp_vrn;
    @FindBy(xpath = "//*[@id='proofs']/ol/div[1]/div[2]/p")
    @CacheLookup
    private WebElement interim_message;
    @FindBy(xpath = "//*[@id='proofs']/legend/div/a")
    @CacheLookup
    private WebElement upload_proof;
    @FindBy(xpath = "//table[@class='prooftable']//tr['xxxxxx']//td['pppppp']")
    @CacheLookup
    private WebElement proof_status_table_delete;
    @FindBy(xpath = "TBD")
    @CacheLookup
    private WebElement proof_status_table_first_row;
    private String STATUS = "awaiting proof";
    private String proof_status_table = "//*[@id='dgridtable1']//tr['xxxxxx']//td[3]";
    @FindBy(xpath = "//*[@id='header1']/ol/li[5]/div/div")
    private WebElement permitStatusTable;
    @FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/div[2]/div[2]/ul/li[5]/a")
    private WebElement clickAddSecondVehicle;
    @FindBy(xpath ="//*[@id=\"templet-container\"]/li[2]/div/div[2]/div[2]/ul/li[4]/a")
    private WebElement clickPermanentVehicleChange;
    @FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/div[2]/div[2]/ul/li[3]/a")
    private WebElement clickTemporaryVehicleChange;
    @FindBy(xpath = "//*[@id='revert_button']")
    private WebElement revertButton;
    private String proofVehicle_status_table = "//*[@id='dgridtable2']//tr['xxxxxx']//td[3]";
    @FindBy(xpath = "//*[contains(text(),'� Order a replacement')]")
    private WebElement replacement;
    @FindBy(xpath = "//a[@href='/permitapplications']")
    @CacheLookup
	public WebElement myPermitPageLink;

    public WCCViewPermitDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getViewPermitTitle() {
        WaitVisible(viewpermittitle);
        return viewpermittitle.getText();
    }

    public Result clickEditAddressButton() {
    	WaitVisible(editAddressButton);
    	ScrollToElement(editAddressButton);
        WaitClickable(editAddressButton);
        return ClickControl(editAddressButton, "click edit address button");
    }

    public void clickManageVehicleButton() {
    	WaitVisible(manageVehiclesButton);
    	ScrollToElement(manageVehiclesButton);
        WaitClickable(manageVehiclesButton);
        manageVehiclesButton.click();
    }

    public void clickRevertTemporaryVRNChange() {
    	WaitVisible(revert_button);
    	ScrollToElement(revert_button);
        WaitClickable(revert_button);
        revert_button.click();
    }

    public boolean isPermitDetailSectionVisible() {
    	WaitVisible(permitDetailsSection);
        return permitDetailsSection.isDisplayed();
    }

    public boolean isActivePermitSessionVisible() {
    	WaitVisible(acivePermitSession);
        return acivePermitSession.isDisplayed();
    }

    public boolean isNameAddressSectionVisible() {
    	WaitVisible(name_address_section);
        return name_address_section.isDisplayed();
    }

    public boolean isTemporaryVehicleChangeSectionVisible() {
    	WaitVisible(temporary_vehicle_change_section);
        return temporary_vehicle_change_section.isDisplayed();
    }

    public boolean isVehicleSectionVisible() {
    	WaitVisible(vehicles_section);
        return vehicles_section.isDisplayed();
    }

    public boolean isProofsSectionVisible() {
    	WaitVisible(proofs_section);
        return proofs_section.isDisplayed();
    }

    public void clickOrderReplacement() {
    	WaitVisible(order_replacement);
    	ScrollToElement(order_replacement);
        WaitClickable(order_replacement);
        order_replacement.click();
    }

    public Result clickCancelPermit() {
    	WaitVisible(cancel_permit);
    	ScrollToElement(cancel_permit);
        WaitClickable(cancel_permit);
    	return ClickControl(cancel_permit, "Clicking cancel");
    }

    public String getCurrentVRNOnPermit() {
    	WaitVisible(permit_vrn);
        return permit_vrn.getText();
    }

    public String getTemporaryVRNOnPermit() {
    	WaitVisible(currently_temp_vrn);
        return currently_temp_vrn.getText();
    }
    
    public boolean interimMessageDisplayed() {
    	WaitVisible(interim_message);
        return interim_message.isDisplayed();
    }
    
    public boolean uploadProofButtonIsDisplayed() {
    	WaitVisible(upload_proof);
        return upload_proof.isDisplayed();
    }
    
    public void clickUploadProofButton() {
    	WaitVisible(upload_proof);
    	ScrollToElement(upload_proof);
        WaitClickable(upload_proof);
    	upload_proof.click();
    }
    
    public String getProofStatus(String position) {
    	WebElement proofStatus = prepareWebElementWithDynamicXpath(proof_status_table, position);
    	WaitVisible(proofStatus);
		return proofStatus.getText();	
    }
    
    public String getPermitStatus() {
    	WaitVisible(permitStatusTable);
    	return permitStatusTable.getText();
    }
    
    public void clickAddSecondVehicleButton() {
    	WaitVisible(clickAddSecondVehicle);
    	ScrollToElement(clickAddSecondVehicle);
        WaitClickable(clickAddSecondVehicle);
        clickAddSecondVehicle.click();
    }
    
    public Result clickPermanentVehicleChange() {
    	WaitVisible(clickPermanentVehicleChange);
    	ScrollToElement(clickPermanentVehicleChange);
        WaitClickable(clickPermanentVehicleChange);
    	return ClickControl(clickPermanentVehicleChange, "Click permanent vehicle link");
    }
    
    public Result clickTemporaryVehicleChange()
    {
    	WaitVisible(clickTemporaryVehicleChange);
    	ScrollToElement(clickTemporaryVehicleChange);
        WaitClickable(clickTemporaryVehicleChange);
    	return ClickControl(clickTemporaryVehicleChange, "Click temporary vehicle link");
    }

    public boolean verifyRevertButton()
    {
    	WaitVisible(revertButton);
    	return revertButton.isDisplayed();
	}
    
    public String getVehcileProofStatus(String tr)
    {
    	WebElement proofStatus = prepareWebElementWithDynamicXpath(proofVehicle_status_table, tr);
    	WaitVisible(proofStatus);
    	return proofStatus.getText();
    }
    
    public void JSclickOrderReplacement() {
    	WaitVisible(replacement);
    	ScrollToElement(replacement);
        WaitClickable(replacement);
        jsClick(replacement);
    }
    
    public void clickMyPermitBreadcrumb()
    {
    	WaitVisible(myPermitPageLink);
    	ScrollToElement(myPermitPageLink);
        WaitClickable(myPermitPageLink);
    	jsClick(myPermitPageLink);
    }
}