package PageObjects;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WCCStatementsPage extends Base_Page {
    @FindBy(xpath = "//table[@id='dgridtable1']")
    private WebElement statementTable;
    private Table table;

    public WCCStatementsPage(WebDriver driver) {
        super(driver);
        table = new Table(statementTable);
    }

    public Table getTableAsElement() {
        return table;
    }

    public String getAmount(int i) {
        return table.getCellValue(i, MyStatementsTable.AMOUNT);
    }

    public enum MyStatementsTable {
        PAID("Paid"),
        DESCRIPTION("Description"),
        AMOUNT("Amount"),
        VEHICLE("Vehicle"),
        START_TIME("Start Time"),
        END_TIME("End Time");

        String headerValue;

        MyStatementsTable(String header) {
            headerValue = header;
        }

        @Override
        public String toString() {
            return headerValue;
        }
    }
}
