package PageObjects.CallCentre;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class CallCentreLoginPage  extends Base_Page {

	public CallCentreLoginPage(WebDriver driver) {
		super(driver);
	}
	
	 @FindBy(xpath = "//input[@name='username']")
     private WebElement username;
	 
	 @FindBy(xpath = "//input[@name='password']")
     private WebElement password;
	 
	 @FindBy(xpath = "//input[@value='Log In']")
     private WebElement submit_button;
	 
	 public Result enterUsername(String callcentre_username) {
		 WaitVisible(username);
         return WebElementMethods.enterText(username, callcentre_username, "Enter callcentre username");
     }
	 
	 public Result enterPassword(String callcentre_password) {
		 WaitVisible(username);
         return WebElementMethods.enterText(password, callcentre_password, "Enter callcentre password");
     }
	 
		public Result clickSubmitButton() {
			 WaitVisible(submit_button);
			 WaitClickable(submit_button);
	      return WebElementMethods.ClickControl(submit_button, "Click Submit button");
	  }
	


}
