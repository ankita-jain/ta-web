package PageObjects.CallCentre;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class CallCentreIndexpage extends Base_Page {

	public CallCentreIndexpage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//*[@name='Member_CLI']")
    private WebElement phone_number;
	
	@FindBy(xpath = "//select[@name='Country']")
    private WebElement country;
	
	@FindBy(xpath = "(//input[@name='activesession'])[1]")
    private WebElement extend_session_radio_button;
	
	@FindBy(xpath = "//input[@name='amendvehicle']")
    private WebElement amend_vrn_on_session;
	
	@FindBy(xpath = "//input[@value='Next']")
    private WebElement next;
	
	@FindBy(xpath = "//select[@name='savedvehicle']")
    private WebElement selectVRN;
	
	@FindBy(xpath = "//select[@id='vehicleDropDown']")
    private WebElement change_vrn_vehicle_dropdown;
	
	@FindBy(xpath = "//input[@id='zone']")
    private WebElement zone;
	
	@FindBy(xpath = "//*[@name='confirmcarpark']")
    private WebElement confirm_parking_check_box;
	
	@FindBy(xpath = "//select[@name='Parking_Days']")
    private WebElement parking_duration;
	
	@FindBy(xpath = "//select[@name='cardSelect[]']")
    private WebElement select_card;
	
	@FindBy(xpath = "//input[@name='securityCode']")
    private WebElement security_code;
	
	@FindBy(xpath = "//*[@id='paymentConfirmButton']")
    private WebElement payment_confirmation_button;
	
	@FindBy(xpath = "//input[@name='confirmpayment']")
    private WebElement confirm_payment_checkbox;
	
	@FindBy(xpath = "//input[@value='Take payment now']")
    private WebElement takePaymentNowButton;	
	
	@FindBy(xpath = "//*[contains(text(),'Thank you, payment has been taken. Your parking session has now started.')]")
    private WebElement parking_confirm_button;	
	
	@FindBy(xpath = "//*[contains(text(),'Thank you, your parking session has been extended.')]")
    private WebElement parking_extend_confirm_button;	
	
	@FindBy(xpath = "//*[contains(text(),'Thank you, the session has been updated. You may proceed with this booking or finish now.')]")
    private WebElement session_update_message;	
	
	@FindBy(xpath = "//input[@value='Finish']")
    private WebElement finish_button;	
	
	public Result enterCLI(String mobile_number) {
		 WaitVisible(phone_number);
        return WebElementMethods.enterText(phone_number, mobile_number, "Enter CLI");
    }
	 
	public Result selectCountry(String Country) {
		 WaitVisible(country);
       return WebElementMethods.selectItemFromDropdownByContainsText(country, Country, "Select country");
   }
	
	public Result clickNextButton() {
		 WaitVisible(next);
		 WaitClickable(next);
      return WebElementMethods.ClickControl(next, "Click NEXT button");
  }
	
	public Result selectVRN(String vrm) {
		 WaitVisible(selectVRN);
      return WebElementMethods.selectItemFromDropdownByContainsText(selectVRN, vrm, "Select VRM");
  }
	
	public Result selectVRNForAmendSession(String vrn_detail) {
		 WaitVisible(change_vrn_vehicle_dropdown);
   return WebElementMethods.selectItemFromDropdownByContainsText(change_vrn_vehicle_dropdown, vrn_detail, "Select vrn_detail");
}
	 
	public Result enterZone(String Zone) {
		 WaitVisible(zone);
       return WebElementMethods.enterText(zone, Zone, "Enter Zone");
   }
	
	public Result clickExtendSessionOption() {
		   return WebElementMethods.ClickControl(extend_session_radio_button, "Click extend_session_radio_button button");
}
	
	public Result clickAmendVehicleonSession() {
		   return WebElementMethods.ClickControl(amend_vrn_on_session, "Click amend_vrn_on_session button");
}
	
		public Result clickConfirmParkTAndCCheckBox() {
		   return WebElementMethods.ClickControl(confirm_parking_check_box, "Click TAndC button");
 }
	
	public Result selectParkingDuration(String duration) {
		 WaitVisible(parking_duration);
     return WebElementMethods.selectItemFromDropdownByContainsText(parking_duration, duration, "Select Duration");
 }
	
	public Result selectCard(String card_details) {
		 WaitVisible(select_card);
    return WebElementMethods.selectItemFromDropdownByContainsText(select_card, card_details, "Select card");
}
	
	public Result enterCVV(String cvv) {
		 WaitVisible(security_code);
       return WebElementMethods.enterText(security_code, cvv, "Enter CVV");
   }
	
	public Result clickConfirmPay() {
		 WaitVisible(payment_confirmation_button);
		 WaitClickable(payment_confirmation_button);
     return WebElementMethods.ClickControl(payment_confirmation_button, "Click Payment confirmation button");
 }
	
	public Result clickConfirmPayment() {
		 WaitVisible(confirm_payment_checkbox);
		 WaitClickable(confirm_payment_checkbox);
		   return WebElementMethods.ClickControl(confirm_payment_checkbox, "Click Confirm payment button");
}
	public Result clickTakePaymentNow() {
		 WaitVisible(takePaymentNowButton);
		 WaitClickable(takePaymentNowButton);
      return WebElementMethods.ClickControl(takePaymentNowButton, "Click Payment confirmation button");
}
	
	public boolean isParkingSessionStarted() {
		 WaitVisible(parking_confirm_button);
		 return parking_confirm_button.isDisplayed(); 
	}
	
	public boolean isParkingExtendSessionStarted() {
		 WaitVisible(parking_extend_confirm_button);
		 return parking_extend_confirm_button.isDisplayed(); 
	}
	
	public boolean isVRNOnSessionChangedMessage() {
		 WaitVisible(session_update_message);
		 return session_update_message.isDisplayed(); 
	}
	
	public Result clickFinishButton() {
		 WaitVisible(finish_button);
		 return WebElementMethods.ClickControl(finish_button, "Click Finish button");
	}

}
