package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCEcoPermitPurchaseFinishPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement ecoPermitPurchaseFinishPageTitle;
    @FindBy(xpath = "//*[@name='labyrinth_FreePay_next']")
    @CacheLookup
    private WebElement confirm_button;

    public WCCEcoPermitPurchaseFinishPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(ecoPermitPurchaseFinishPageTitle);
        return ecoPermitPurchaseFinishPageTitle.getText();
    }

    public void clickConfirm() {
        WaitVisible(confirm_button);
        confirm_button.click();
    }
}
