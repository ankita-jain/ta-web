package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonProofPage extends PageClass {

	public IslingtonProofPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//*[contains(text(), 'Proof of Address ')]")
	private WebElement proofSubHeading;
	@FindBy(xpath = "//*[@class='input-medium proof-select']")
	private WebElement selectProof;
	@FindBy(xpath = "//*[@class='proofSection_16']")
	private WebElement uploadBtn;
	@FindBy(id = "labyrinth_RvpProofs_next")
	private WebElement confirmBtn;
	
	
	public String getPageSubHeader() {
		if (WaitVisible(proofSubHeading))
		{
        return proofSubHeading.getText();
		}
		else return "Title not visible";
	}
	
	public void selectProof(String proof) {
        WaitVisible(selectProof);
        Select addresslist = new Select(selectProof);
        addresslist.selectByVisibleText(proof);
    }
	
	public void uploadProof(String ProofFile) {
		uploadBtn.sendKeys(getCurrentDirectoryPath() + ProofFile);
	}
	
	public Result clickConfirm() {
		WaitVisible(confirmBtn);
		return ClickControl(confirmBtn, "Click confirm button");
	}
}