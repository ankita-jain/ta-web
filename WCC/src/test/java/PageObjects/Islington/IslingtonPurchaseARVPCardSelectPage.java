package PageObjects.Islington;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonPurchaseARVPCardSelectPage extends PageClass {

	public IslingtonPurchaseARVPCardSelectPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(id = "labyrinth_CardSelect_next")
	private WebElement nextBtn;
	
	public Result clickNext() {
		WaitVisible(nextBtn);
		return ClickControl(nextBtn, "Click next button");
	}
}
