package PageObjects.Islington;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonPurchaseARVPFinishPage extends PageClass {

	public IslingtonPurchaseARVPFinishPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(id = "labyrinth_finish")
	private WebElement finishBtn;
	
	public Result clickfinish() {
		WaitVisible(finishBtn);
		return ClickControl(finishBtn, "Click finish button");
	}

}
