package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonFinishPage extends PageClass {

	public IslingtonFinishPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//*[contains(text(), 'Thank you for registering')]")
	private WebElement thankYouText;
	@FindBy(id = "labyrinth_finish")
	private WebElement finishBtn;
	
	public String getThankYouText() {
		if (WaitVisible(thankYouText))
		{
        return thankYouText.getText();
		}
		else return "text not visible";
	}
	
	public Result clickFinish() {
		WaitVisible(finishBtn);
		return ClickControl(finishBtn, "Click finish button");
	}
}