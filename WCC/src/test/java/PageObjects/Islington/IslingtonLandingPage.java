package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonLandingPage extends PageClass {

	public IslingtonLandingPage(WebDriver _driver) {
		super(_driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//*[@id='siteTypeChoice']/ul/li[1]/a")
    private WebElement personal_log_in_link;
	
	
	public Result ClickLogIn() {
		WaitVisible(personal_log_in_link);
		return ClickControl(personal_log_in_link, "Click personal login link");
	}

}
