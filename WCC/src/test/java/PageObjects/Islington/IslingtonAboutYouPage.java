package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import static SeleniumHelpers.WebElementMethods.enterText;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonAboutYouPage extends PageClass {

	public IslingtonAboutYouPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//*[contains(text(), 'Your Details')]")
	private WebElement youDetailsSubHeading;
	@FindBy(xpath = "//input[@name='Terms']")
	private WebElement terms_and_conditions;
	@FindBy(id = "labyrinth_Options_next")
	private WebElement nextBtn;
	@FindBy(id = "field-Member_Title")
	private WebElement titleDropDown;
	@FindBy(id = "field-Member_Firstname")
	private WebElement fNameField;
	@FindBy(id = "field-Member_Lastname")
	private WebElement sNameField;
	@FindBy(id = "field-cli")
	private WebElement phoneNumberField;
	
	
	public String getPageSubHeader() {
		if (WaitVisible(youDetailsSubHeading))
		{
        return youDetailsSubHeading.getText();
		}
		else return "Title not visible";
	}
	
	public void selectTermAndConditions() {
        jsClick(terms_and_conditions);
        clickNextButton();
    }
	
	public Result clickNextButton() {
		WaitVisible(nextBtn);
		return ClickControl(nextBtn, "Clicking the next button");
	}
	
	public void fillInPersonalDetails(String name, String lastname, String title) {
		fillInFirstname(name);
		fillInSurname(lastname);
		selectTitle(title);
	}
	
	public Result fillInFirstname(String name) {
		WaitVisible(fNameField);
		return enterText(fNameField, name, "Enter firstname");
	}
	
	public Result fillInSurname(String lastname) {
		WaitVisible(sNameField);
		return enterText(sNameField, lastname, "Enter surname");
	}

	public void selectTitle(String title) {
		WaitVisible(titleDropDown);
        Select addresslist = new Select(titleDropDown);
        addresslist.selectByVisibleText(title);
	}
	
	public String getClI() {
		WaitVisible(phoneNumberField);
		return phoneNumberField.getText();
	}
}
