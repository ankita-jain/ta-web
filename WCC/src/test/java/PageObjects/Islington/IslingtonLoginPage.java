package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonLoginPage extends PageClass {
	
	public IslingtonLoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "field-cli")
    private WebElement emailPhoneField;
	@FindBy(id = "field-pin")
    private WebElement PasswordField;
	@FindBy(xpath = "//*[@id='currentMembers']/ol/li[4]/div/div/input")
    private WebElement LogInButton;
	
	public Result enterCLI(String cli) {
		WaitVisible(emailPhoneField);
		return enterText(emailPhoneField, cli, "Enter the CLI");
	}
	
	public Result enterPassword(String password) {
		WaitVisible(emailPhoneField);
		return enterText(PasswordField, password, "Enter the password");
	}
	
	public Result clickLogin() {
		WaitVisible(LogInButton);
		return ClickControl(LogInButton, "Click log in butotn");
	}
	
	public void fillInLoginDetails(String user, String password) {
		enterCLI(user);
		enterPassword(password);
	}
}