package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonMyPermitApplicationPage extends PageClass {

	public IslingtonMyPermitApplicationPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//h1[contains(text(),'My Permit Applications for LB of Islington')]")
	private WebElement permitApplicationPageHeader;
	@FindBy(xpath = "//table[@id='dgridtable1']//tr[1]//td[8]")
	private WebElement PermitStatus;
	@FindBy(xpath = "//img[@src=\"/images/money.png\"]")
	private WebElement purchaseIcon;
	
	public String getpageHeader() {
		if (WaitVisible(permitApplicationPageHeader))
		{
        return permitApplicationPageHeader.getText();
		}
		else return "Title not visible";
	}
	
	public String getPermitStatus() {
		if (WaitVisible(PermitStatus))
		{
        return PermitStatus.getText();
		}
		else return "Status is blank";
	}
	
	public Result clickPurchase() {
		WaitVisible(purchaseIcon);
		return ClickControl(purchaseIcon, "Clicking the purchase icon");
	}
}