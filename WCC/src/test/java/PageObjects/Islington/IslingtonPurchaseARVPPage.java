package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static SeleniumHelpers.WebElementMethods.enterText;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonPurchaseARVPPage extends PageClass{

	public IslingtonPurchaseARVPPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//h1[contains(text(),'Purchase a Resident Visitor Permit')]")
	private WebElement purchaseRVPPageHeader;
	@FindBy(id = "newVehLink")
	private WebElement addAVehicleLink;
	@FindBy(id = "field-vrn")
	private WebElement numberPlateField;
	@FindBy(id = "field-make")
	private WebElement selectManufacturer;
	@FindBy(id = "field-colour")
	private WebElement selectColour;
	@FindBy(id = "labyrinth_RVPTariff_next")
	private WebElement nextBtn;
	@FindBy(id = "tariffdropdown")
	private WebElement durationDropDown;
	@FindBy(xpath = "//*[contains(text(),'Take payment now')]//preceding::input[1]")
	private WebElement takePaymentNowRadioButton;
	@FindBy(id = "labyrinth_LABasket_next")
	private WebElement paymentNextBtn;
	
	public String getpageHeader() {
		if (WaitVisible(purchaseRVPPageHeader))
		{
        return purchaseRVPPageHeader.getText();
		}
		else return "Title not visible";
	}
	
	public Result clickAddANewVehicleLink() {
		WaitVisible(addAVehicleLink);
		return ClickControl(addAVehicleLink, "Click add a new vehicle link");
	}
	
	public Result enterVRN(String vrn) {
		WaitVisible(addAVehicleLink);
		return enterText(numberPlateField, vrn, "Enter a vrn");
	}
	
	public void selectMake(String manufacturer) {
		WaitVisible(selectManufacturer);
        Select addresslist = new Select(selectManufacturer);
        addresslist.selectByVisibleText(manufacturer);
    }
	
	public void selectColour(String colour) {
		WaitVisible(selectColour);
        Select addresslist = new Select(selectColour);
        addresslist.selectByVisibleText(colour);
    }
	
	public Result clickNext() {
		WaitVisible(nextBtn);
		return ClickControl(nextBtn, "Click next button");
	}
	
	public void addANewVehicle(String vrn, String make, String colour) {
		clickAddANewVehicleLink();
		enterVRN(vrn);
		selectMake(make);
		selectColour(colour);
	}
	
	public void selectDuration(String time) {
		WaitVisible(durationDropDown);
        Select addresslist = new Select(durationDropDown);
        addresslist.selectByVisibleText(time);
        clickNext();
    }
	
	public Result takePaymentNow() {
		WaitVisible(takePaymentNowRadioButton);
		return ClickControl(takePaymentNowRadioButton, "select take payment now radio button");
	}
	
	public Result clickNextForPayment() {
		WaitVisible(paymentNextBtn);
		return ClickControl(paymentNextBtn, "Click next button");
	}
}