package PageObjects.Islington;

import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonRVPAddressSearchPage extends PageClass{

	
	public IslingtonRVPAddressSearchPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//h1[contains(text(),'Resident Visitor Permit Application')]")
	private WebElement residentVisitorPermitApplicationHeader;
	@FindBy(id = "field-PostcodeSearch")
	private WebElement postCode;
	@FindBy(id = "postcodeError")
	private WebElement postCodeError;
	@FindBy(id = "submitPostcode")
	private WebElement findAddressBtn;
	@FindBy(id = "SelectAddress")
	private WebElement selectAddress;
	@FindBy(id = "labyrinth_addresssearch_next")
	private WebElement nextBtn;
	
	
	public String getPageHeader() {
		if (WaitVisible(residentVisitorPermitApplicationHeader))
		{
        return residentVisitorPermitApplicationHeader.getText();
		}
		else return "Title not visible";
	}
	
	public Result enterPostCode(String postcode) {
		WaitVisible(postCode);
		return enterText(postCode, postcode, "Enter the postcode");
	}
	
	public String getPostcodeError() {
		if (WaitVisible(postCodeError))
		{
        return postCodeError.getText();
		}
		else return "Error not visible";
	}
	
	public Result clickFindAddress() {
		WaitVisible(findAddressBtn);
		return ClickControl(findAddressBtn, "Click find address button");
	}

	public void selectAddress(String address) {
        WaitVisible(selectAddress);
        Select addresslist = new Select(selectAddress);
        addresslist.selectByVisibleText(address);
    }
	
	public Result clickNext() {
		WaitVisible(nextBtn);
		return ClickControl(nextBtn, "Click next button");
	}
}