package PageObjects.Islington;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonHomePage extends PageClass{

	public IslingtonHomePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	
	@FindBy (xpath = "//*[contains(text(),'Apply for a new Permit')]")
	private WebElement ApplyForPermitLink;
	
	@FindBy(xpath = "//a[@href=\'/islington/permitapplications/add/94200/V\']")
	private WebElement residentVisitorApplicationLink;
	
	
	public Result clickApplyForPermitLink() {
		WaitVisible(ApplyForPermitLink);
		return ClickControl(ApplyForPermitLink, "Clicking the apply for a new permit link");
	}
	
	public Result clickResidentVisitorApplication() {
		WaitVisible(ApplyForPermitLink);
		return ClickControl(residentVisitorApplicationLink, "Clicking the resident visitor application link");
	}
}
