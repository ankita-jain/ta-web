package PageObjects.Islington;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class IslingtonPurchaseARVPPayPage extends PageClass {

	public IslingtonPurchaseARVPPayPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(id = "labyrinth_Payment_next")
	private WebElement payBtn;
	
	public Result clickPay() {
		WaitVisible(payBtn);
		return ClickControl(payBtn, "Click pay button");
	}
}
