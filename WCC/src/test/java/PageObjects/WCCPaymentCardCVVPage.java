package PageObjects;

import static Utilities.TimerUtils.delay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class WCCPaymentCardCVVPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
    
    @FindBy(xpath = "//*[@name='securityCode']")
    private WebElement CVV;
    
    @FindBy(xpath = "//*[@id='paymentConfirmButton']")
    private WebElement pay_amount_button;
    
    @FindBy(xpath = "//*[@name='cv2']")
    @CacheLookup
    private WebElement cv2;
    @FindBy(xpath = "//*[@name='CV2']")
    @CacheLookup
    private WebElement CV2; //CV2 field in the permit charges section is different from permit buying section
    @FindBy(xpath = "//*[@value='Pay']")
    private WebElement pay;
    @FindBy(id="paymentConfirmButton")
    private WebElement confirmPayment;
    
    @FindBy(xpath = "//*[@value='Previous']")
    private WebElement previous_button;
    @FindBy(xpath = "//*[@name='labyrinth_cancel']")
    private WebElement cancel;
    @FindBy(xpath = "//*[@class='modal-title']")
    private WebElement cvvPageHeader;

    public WCCPaymentCardCVVPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public void enterCV2(String CVTwo) {
        WaitVisible(cv2);
        cv2.sendKeys(CVTwo);
    }

    public void enterCVTwo(String CVTwo) {
        WaitVisible(CV2);
        CV2.sendKeys(CVTwo);
    }

    public void enterCVVCode(String cvv)
    {
        WaitVisible(CVV);
    	CVV.sendKeys(cvv.split("\\.")[0]);     	
    }
    
    public void clickPay() {
        WaitVisible(pay);
        //WaitVisible(pay);
        pay.click();
    }
    
    public void clickPayAmount()
    {
        WaitVisible(pay_amount_button);
    	pay_amount_button.click();
    }

    public void clickPreviousButton() {
        WaitVisible(previous_button);
        previous_button.click();
    }

    public void clickCancelButton() {
        WaitVisible(cancel);
        cancel.click();
    }

    public void enterCV2AndProceed(String CVTwo) {
       // enterCV2(CVTwo);
        clickPay();
        delay(3000);
        enterCVVCode(CVTwo);
        delay(3000);
        clickPayAmount();
        delay(3000);
    }

    public void enterCV2AndProceedPermitCharges(String CVTwo) {
        //enterCVTwo(CVTwo);
        clickPay();
        delay(3000);
        enterCVVCode(CVTwo);
        delay(3000);
        clickPayAmount();
        delay(3000);
    }

    public void enterCV2AndProceedPermitBasketPayment(String CVTwo) {
    	 enterCVVCode(CVTwo);
         delay(3000);
    	 clickPayAmount();	
         delay(3000);
    }
    
    public String getPageHeader() {
        WaitVisible(cvvPageHeader);
        return cvvPageHeader.getText();
    }
}
