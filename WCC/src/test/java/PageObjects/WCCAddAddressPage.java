package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCAddAddressPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    private WebElement addAddressPageTitle;
    @FindBy(xpath = "//*[@id='field-AddressType']")
    private WebElement addressType;
    @FindBy(xpath = "//*[@id='field-MemberAddress1']")
    private WebElement addressLine1;
    @FindBy(xpath = "//*[@id='field-MemberTown']")
    private WebElement addressTown;
    @FindBy(xpath = "//*[@id='field-MemberCou']")
    private WebElement addressCounty;
    @FindBy(xpath = "//*[@id='field-MemberPostcode']")
    private WebElement postcodeInput;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@class='error']")
    private List<WebElement> errorMessages;
    @FindBy(xpath = "//*[@class='warning-notification']")
    private WebElement warning_notification;

    public WCCAddAddressPage(WebDriver driver) {
        super(driver);
    }

    public String getAddAddressPageTitle() {
        WaitVisible(addAddressPageTitle);
        return addAddressPageTitle.getText();
    }

    public Result selectAddressType(String type) {
        WaitVisible(addressType);
        return selectItemFromDropdownByTextValue(addressType, type, "Address type select");
    }

    public Result enterAddressLine(String addressline1) {
        WaitVisible(addressLine1);
        return enterText(addressLine1, addressline1, "Address line 1 input");
    }

    public Result enterTown(String town) {
        WaitVisible(addressTown);
        return enterText(addressTown, town, "Town input");
    }

    public Result selectCounty(String county) {
        WaitVisible(addressCounty);
        return selectItemFromDropdownByTextValue(addressCounty, county, "County select");
    }

    public Result enterPostcode(String postdcode) {
        WaitVisible(postcodeInput);
        return enterText(postcodeInput, postdcode, "Postcode input");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button");
    }

    public Result clickCanelButton() {
        WaitVisible(cancelButton);
        return ClickControl(cancelButton, "Cancel button");
    }

    public String getWarningNotification() {
        WaitVisible(warning_notification);
        return warning_notification.getText();
    }

    public List<String> getErrorMessages() {
        return errorMessages.stream().map(WebElement::getText).collect(Collectors.toList());
    }
}