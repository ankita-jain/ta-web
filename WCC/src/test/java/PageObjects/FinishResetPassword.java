package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import Structures.Result;

public class FinishResetPassword extends Base_Page {
    @FindBy(xpath = "//input[@name='labyrinth_finish']")
    private WebElement finishButton;

    public FinishResetPassword(WebDriver driver) {
        super(driver);
    }

    public Result finish() {
        WaitVisible(finishButton);
        return ClickControl(finishButton, "Finish button");
    }
}
