package PageObjects;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCPermitPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement permitPageTitle;
    @FindBy(xpath = "//*[@id='navbarpermitslink']")
    @CacheLookup
    private WebElement permitMenu;
    @FindBy(xpath = "//li[@class='navbarmenuli']//a[@href='/permitapplications']")
    @CacheLookup
    private WebElement seeAllPermit;
    @FindBy(xpath = "//*[@id='navbarpermitsmenuleft']/span/a")
    @CacheLookup
    private WebElement myPermits;
    @FindBy(xpath = "//div[@class='permitwidget']//a[contains(@href,'H')]")
    @CacheLookup
    private WebElement residentPermit;
    @FindBy(xpath = "//div[@class='permitwidget']//a[@href='/disabledpermits']")
    @CacheLookup
    private WebElement disablePermit;
    @FindBy(xpath = "//div[@class='permitwidget']//a[contains(@href,'TR')]")
    @CacheLookup
    private WebElement tradePermit;
    @FindBy(id = "uploadbutton")
    @CacheLookup
    private WebElement upload_button;
    @FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/div[2]/div[1]/h2/span[2]/a")
    private WebElement see_all_permits_link;
    @FindBy(id = "//*[contains(text(), 'This permit is in an interim state until')")
    @CacheLookup
    private WebElement interim_message;
    
    @FindBy(id = "/html/body/div[5]/a[3]")
    @CacheLookup
    private WebElement permitLink;
    
    public WCCPermitPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPermitPageTitle() {
        WaitVisible(permitPageTitle);
        return permitPageTitle.getText();
    }

    public void clickSeeAllPermitFromPermitMenu() {
        clickPermitMenu();
        moveToMenuAndClickSubMenu(permitMenu, seeAllPermit);
    }

    public void clickMyPermitsFromPermitMenu() {
        clickPermitMenu();
        moveToMenuAndClickSubMenu(permitMenu, myPermits);
    }

    public void clickPermitMenu() {
        WaitVisible(permitMenu);
        permitMenu.click();
    }


    public Result clickResidentPermit() {
        WaitVisible(residentPermit);
        return ClickControl(residentPermit, "Click resident permit");
    }

    public void clickDisablePermit() {
        WaitVisible(disablePermit);
        disablePermit.click();
    }

    public Result clickDisablePermitTile() {
        return ClickControl(disablePermit, "Login button");
    }

    public void clickTradePermit() {
        WaitVisible(tradePermit);
        tradePermit.click();
    }

	public Result clickUploadButton() {
		WaitVisible(upload_button);
		return ClickControl(upload_button, "upload button clicked");
	}
	
	public Result selectSeeAllPermitsLink() {
		WaitVisible(see_all_permits_link);
		return ClickControl(see_all_permits_link, "see all permits link");
	}
	
	public boolean uploadButtonIsdisplayed() {
		WaitVisible(upload_button);
		return upload_button.isDisplayed();
	}
	
	public boolean interimMessageIsdisplayed() {
		WaitVisible(interim_message);
		return interim_message.isDisplayed();
	}
	
	public Result clickPermitBreadCrumb() {
		WaitVisible(permitLink);
        return ClickControl(permitLink, "permit link clicked");
    }
}
