package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import SeleniumHelpers.WaitMethods;

public class WCCResidentPermitProofPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement proofPageTile;
    @FindBy(xpath = "//*[@id='proofs67']")
    @CacheLookup
    private WebElement proof_of_address_one;
    @FindBy(xpath = "//*[@id='proof67']")
    @CacheLookup
    private WebElement proof_of_address_one_uploader;
    @FindBy(xpath = "//*[@id='proofs66']")
    @CacheLookup
    private WebElement proof_of_address_two;
    @FindBy(xpath = "//*[@id='proof66']")
    @CacheLookup
    private WebElement proof_of_address_two_uploader;
    @FindBy(xpath = "//*[@id='proofVRN-a-1']")
    @CacheLookup
    private WebElement proof3_uploader;
    @FindBy(xpath = "//*[@id='proofVRN-b-1']")
    @CacheLookup
    private WebElement proof4_uploader;
    @FindBy(xpath = "//*[@id='proofVRN-d-1']")
    @CacheLookup
    private WebElement proof5_uploader;
    @FindBy(xpath = "//*[@id='proofVRN-type-a-1']")
    @CacheLookup
    private WebElement permanent_vrn_change_proof_type;
    @FindBy(xpath = "//*[@id='proofVRN-a-1']")
    @CacheLookup
    private WebElement permanent_vrn_change_proof_upload;
    @FindBy(xpath = "//*[@id='proofVRN-type-b-1']")
    @CacheLookup
    private WebElement certificate_of_insurance;
    @FindBy(xpath = "//*[@name='temp_PermitStage_160']")
    @CacheLookup
    private WebElement temp_permit_proof_of_address_tAndC;

    /*
     *
     * TEMPORARY PERMIT FIELDS
     */
    @FindBy(xpath = "//*[@name='temp_PermitStage_v1']")
    @CacheLookup
    private WebElement temp_permit_proof_of_vehicle_tAndC;
    @FindBy(xpath = "//*[@id='proofs171']")
    @CacheLookup
    private WebElement temp_proof_of_address_proof_type;
    @FindBy(xpath = "//*[@id='proof171']")
    @CacheLookup
    private WebElement temp_proof_of_address_proof_type_upload;
    @FindBy(xpath = "//*[@id='VehicleCountryType_v1']")
    @CacheLookup
    private WebElement is_this_vehicle_uk_registered;
    @FindBy(xpath = "//*[@id='proofVRN-type-c-1']")
    @CacheLookup
    private WebElement temp_proof_of_vehicle_type;
    @FindBy(xpath = "//*[@id='proofVRN-c-1']")
    @CacheLookup
    private WebElement temp_proof_of_address_vehicle_type_upload;
    @FindBy(xpath = "//*[@class='input-medium proof-select']")
    private WebElement renewal_permit_address_proof_types;

    /*
     *  Permit renewal proofs
     *
     */
    @FindBy(xpath = "//*[@class='proofSection_160']")
    @CacheLookup
    private WebElement renewal_permit_address_proof_type_upload;
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[3]/div/div/select")
    @CacheLookup
    private WebElement address_proof_one_eco_permit_temp_permit;

    /*
     * ECO permit Address of Proof page,
     *
     */
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[3]/div/div/input")
    @CacheLookup
    private WebElement address_proof_one_eco_permit_temp_permit_upload;
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[4]/div/div/select")
    @CacheLookup
    private WebElement address_proof_one_eco_permit;
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[4]/div/div/input")
    @CacheLookup
    private WebElement address_proof_one_eco_permit_upload;
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[5]/div/div/select")
    @CacheLookup
    private WebElement address_proof_two_eco_permit;
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[5]/div/div/input")
    @CacheLookup
    private WebElement address_proof_two_eco_permit_upload;
    @FindBy(xpath = "//*[@id='proofVRN-type-d-1']")
    @CacheLookup
    private WebElement eco_VR5;
    @FindBy(xpath = "//*[@id='proofVRN-d-1']")
    @CacheLookup
    private WebElement eco_VR5_upload;
    /*
     * ECO Permit & Motorcycle 'Temporary' permit - proof
     *
     */
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[6]/div/div/select")
    @CacheLookup
    private WebElement address_proof_eco_temp_permit;
    @FindBy(xpath = "//*[@id='proofSection160']/ol/li[6]/div/div/input")
    @CacheLookup
    private WebElement address_proof_eco_temp_permit_uploader;
    @FindBy(xpath = "//*[@class='input-medium proof-select']")
    @CacheLookup
    private WebElement eco_address_change_proof_type;


    /*
     * ECO permit Address change proofs
     *
     */
    @FindBy(xpath = "//*[@class='proofSection_160']")
    @CacheLookup
    private WebElement eco_address_change_proof;
    @FindBy(xpath = "//*[@name='labyrinth_RvpProofs_next']")
    private WebElement next_button;
    @FindBy(xpath = "//input[@value='Next Step']")
    private WebElement next_button_in_edit_address_page;
    @FindBy(id = "labyrinth_Options_back")
    @CacheLookup
    private WebElement back_button;
    @FindBy(xpath = "//*[@id='proofs59']")
    private WebElement motorcycle_address_proof_one;

    /*
     * Motorcycle permit proofs
     *
     */
    @FindBy(xpath = "//*[@id='proof59']")
    @CacheLookup
    private WebElement motorcycle_address_proof_one_upload;
    @FindBy(xpath = "//*[@id='proofs58']")
    @CacheLookup
    private WebElement motorcycle_address_proof_two;
    @FindBy(xpath = "//*[@id='proof58']")
    @CacheLookup
    private WebElement motorcycle_address_proof_two_upload;
    
    @FindBy(id = "yesbutton")
    @CacheLookup
	public WebElement yes_super_pack;
    @FindBy(id = "nobutton")
    @CacheLookup
    private WebElement no_super_pack;
    @FindBy(id = "residentproofone")
    @CacheLookup
    public WebElement residential_proof_pack_question_one;
    @FindBy(id = "residentprooftwo")
    @CacheLookup
    private WebElement residential_proof_pack_question_two;

    public WCCResidentPermitProofPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getProofPageTitle() {
        WaitVisible(proofPageTile);
        return proofPageTile.getText();
    }

    public void selectProofOneType(String proofOne) {
        WaitVisible(proof_of_address_one);
        new Select(proof_of_address_one).selectByVisibleText(proofOne);
    }

    public void selectProofTwoType(String proofTwo) {
        WaitVisible(proof_of_address_two);
        new Select(proof_of_address_two).selectByVisibleText(proofTwo);
    }

    public void selectProofForPermanentVRNChange(String proof) {
        WaitVisible(permanent_vrn_change_proof_type);
        new Select(permanent_vrn_change_proof_type).selectByVisibleText(proof);
    }

    public void selectCertificateOfInsurance(String proof) {
        WaitVisible(certificate_of_insurance);
        new Select(certificate_of_insurance).selectByVisibleText(proof);
    }

    public void sendUploadPathToPremanentProof(String proofpath) {
    	WaitVisible(permanent_vrn_change_proof_upload);
        permanent_vrn_change_proof_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void sendUploadPathToProofOne(String proofonepath) {
    	WaitVisible(proof_of_address_one_uploader);

        proof_of_address_one_uploader.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }

    public void sendUploadPathToProofTwo(String proofonepath) {
    	WaitVisible(proof_of_address_two_uploader);

        proof_of_address_two_uploader.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }

    public void sendUploadPathToProofThree(String proofpath) {
    	WaitVisible(proof3_uploader);

        proof3_uploader.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void sendUploadPathToProofFour(String proofpath) {
    	WaitVisible(proof4_uploader);
        proof4_uploader.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void sendUploadPathToProofFive(String proofpath) {

    	WaitVisible(proof5_uploader);
        proof5_uploader.sendKeys(getCurrentDirectoryPath() + proofpath);
    }


    public void click_Cannot_provide_requested_proofs_of_address() {
    	WaitVisible(temp_permit_proof_of_address_tAndC);
        jsClick(temp_permit_proof_of_address_tAndC);
    }

    public void click_Cannot_provide_request_proof_of_vehicles() {
    	WaitVisible(temp_permit_proof_of_vehicle_tAndC);
        jsClick(temp_permit_proof_of_vehicle_tAndC);
    }

    public void selectTempPermitProofOfAddress(String proof) {
        WaitVisible(temp_proof_of_address_proof_type);
        new Select(temp_proof_of_address_proof_type).selectByVisibleText(proof);
    }

    public void sendUploadTempAddressProof(String proofpath) {
    	WaitVisible(temp_proof_of_address_proof_type_upload);
        temp_proof_of_address_proof_type_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void selectIsThisVehicleUKRegistered(String option) {
        WaitVisible(is_this_vehicle_uk_registered);
        new Select(is_this_vehicle_uk_registered).selectByVisibleText(option);
    }

    public void selectTempPermitProofOfVehicle(String proof) {
        WaitVisible(temp_proof_of_vehicle_type);
        new Select(temp_proof_of_vehicle_type).selectByVisibleText(proof);
    }

    public void selectPermitRenewalAddressProofType(String proof) {
        WaitVisible(renewal_permit_address_proof_types);
        new Select(renewal_permit_address_proof_types).selectByVisibleText(proof);
    }

    public void sendUploadProofsForRenewalProof(String proofpath) {
    	WaitVisible(renewal_permit_address_proof_type_upload);
        renewal_permit_address_proof_type_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void sendUploadTempVehicleProof(String proofpath) {
    	WaitVisible(temp_proof_of_address_vehicle_type_upload);
        temp_proof_of_address_vehicle_type_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void selectECOPermitAdressProofOne(String proof) {
        WaitVisible(address_proof_one_eco_permit);
        new Select(address_proof_one_eco_permit).selectByVisibleText(proof);
    }

    public void selectECOPermitAddressProofOneTempPermit(String proof) {
        WaitVisible(address_proof_one_eco_permit_temp_permit);
        new Select(address_proof_one_eco_permit_temp_permit).selectByVisibleText(proof);
    }

    public void uploadEcoPermitAddressProofOneTempPermit(String proofpath) {
    	WaitVisible(address_proof_one_eco_permit_temp_permit_upload);
        address_proof_one_eco_permit_temp_permit_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void selectECOPermitAddressProofTwo(String proof) {
        WaitVisible(address_proof_two_eco_permit);
        new Select(address_proof_two_eco_permit).selectByVisibleText(proof);
    }

    public void selectAddressProofEcoTempPermit(String proof) {
        WaitVisible(address_proof_eco_temp_permit);
        new Select(address_proof_eco_temp_permit).selectByVisibleText(proof);
    }

    public void uploadAddressProofEcoTempPermit(String proofpath) {
    	WaitVisible(address_proof_eco_temp_permit_uploader);
        address_proof_eco_temp_permit_uploader.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void sendUploadPathToEcoAddressProofOne(String proofonepath) {
    	WaitVisible(address_proof_one_eco_permit_upload);

        address_proof_one_eco_permit_upload.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }

    public void sendUploadPathToEcoAddressProofTwo(String proofonepath) {
    	WaitVisible(address_proof_two_eco_permit_upload);

        address_proof_two_eco_permit_upload.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }

    public void selectEcoVR5(String proof) {
        WaitVisible(eco_VR5);
        new Select(eco_VR5).selectByVisibleText(proof);
    }

    public void sendUploadPathToEcoVR5path(String proofonepath) {

    	WaitVisible(eco_VR5_upload);
        eco_VR5_upload.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }

    public void selectChangeAddressProof(String proof) {
        WaitVisible(eco_address_change_proof_type);
        new Select(eco_address_change_proof_type).selectByVisibleText(proof);
    }

    public void sendUploadPathToEcoPermitAddressChange(String proofpath) {
    	WaitVisible(eco_address_change_proof);
        eco_address_change_proof.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    /*
     * Motorcycle permit proofs
     */


    public void selectMotorcycleAddressProofOne(String proof) {
        WaitVisible(motorcycle_address_proof_one);
        new Select(motorcycle_address_proof_one).selectByVisibleText(proof);
    }

    public void uploadMotroCycleAddressOneProof(String proofpath) {
    	WaitVisible(motorcycle_address_proof_one_upload);
        motorcycle_address_proof_one_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void selectMotorcycleAddressProofTwo(String proof) {
        WaitVisible(motorcycle_address_proof_two);
        new Select(motorcycle_address_proof_two).selectByVisibleText(proof);
    }

    public void uploadMotroCycleAddressTwoProof(String proofpath) {
    	WaitVisible(motorcycle_address_proof_two_upload);
        motorcycle_address_proof_two_upload.sendKeys(getCurrentDirectoryPath() + proofpath);
    }

    public void clickNextButton() {
        WaitVisible(next_button);
        next_button.click();
    }

    public void clickNextButtonInEditAddressPage() {
        WaitVisible(next_button_in_edit_address_page);
        next_button_in_edit_address_page.click();
    }

    public void completePermitApplicationProofs(String proof1, String proof1path, String proof2, String proof2path, String proof3path, String proof4path, String proof5path) {

        selectProofOneType(proof1);
        sendUploadPathToProofOne(proof1path);
        selectProofTwoType(proof2);
        sendUploadPathToProofTwo(proof2path);
        sendUploadPathToProofThree(proof3path);
        sendUploadPathToProofFour(proof4path);
        sendUploadPathToProofFive(proof5path);
        clickNextButton();

    }

    public void completeEcoPermitApplicationProofs(String address_proof_type_one, String address_proof_one, String address_proof_type_two, String address_proof_two,
                                                   String vehicle_proofs_one_type, String vehicle_proofs_one,
                                                   String vehicle_proofs_two_type, String vehicle_proofs_two,
                                                   String vehicle_proof_three_type, String vehicle_proof_three) {
        selectECOPermitAdressProofOne(address_proof_type_one);
        sendUploadPathToEcoAddressProofOne(address_proof_one);
        selectECOPermitAddressProofTwo(address_proof_type_two);
        sendUploadPathToEcoAddressProofTwo(address_proof_two);
        selectProofForPermanentVRNChange(vehicle_proofs_one_type);
        sendUploadPathToProofThree(vehicle_proofs_one);
        selectCertificateOfInsurance(vehicle_proofs_two_type);
        sendUploadPathToProofFour(vehicle_proofs_two);
        selectEcoVR5(vehicle_proof_three_type);
        sendUploadPathToEcoVR5path(vehicle_proof_three);
        clickNextButton();
    }

    public void completeMotorcyclePermitApplicationProofs(String addressProofOne, String addressProofOnePath, String addressProofTwo, String addressProofTwoPath,
                                                          String vehicle_proofs_one_type, String vehicle_proofs_one,
                                                          String vehicle_proofs_two_type, String vehicle_proofs_two,
                                                          String vehicle_proof_three_type, String vehicle_proof_three) {
        selectMotorcycleAddressProofOne(addressProofOne);
        uploadMotroCycleAddressOneProof(addressProofOnePath);
        selectMotorcycleAddressProofTwo(addressProofTwo);
        uploadMotroCycleAddressTwoProof(addressProofTwoPath);
        selectProofForPermanentVRNChange(vehicle_proofs_one_type);
        sendUploadPathToProofThree(vehicle_proofs_one);
        selectCertificateOfInsurance(vehicle_proofs_two_type);
        sendUploadPathToProofFour(vehicle_proofs_two);
        selectTempPermitProofOfVehicle(vehicle_proof_three_type);
        sendUploadTempVehicleProof(vehicle_proof_three);
        clickNextButton();
    }


    public void editAddressOnPermitAndCompleteUploadProofs(String proof1, String proof1path) {
        selectProofOneType(proof1);
        sendUploadPathToProofOne(proof1path);
        clickNextButton();
    }

    public void editAddressonEcoPermitAndCompleteUploadProofs(String proof1, String proof1path) {
        selectChangeAddressProof(proof1);
        sendUploadPathToEcoPermitAddressChange(proof1path);
        clickNextButton();
    }

    public void editAddressonMotorcyclePermitAndCompleteUploadProofs(String proof, String proofpath) {
        selectMotorcycleAddressProofOne(proof);
        uploadMotroCycleAddressOneProof(proofpath);
        clickNextButton();
    }

    public void changeVRNPermanentlyProofs(String prooftype, String proofpath) {
        selectProofForPermanentVRNChange(prooftype);
        sendUploadPathToPremanentProof(proofpath);
        clickNextButton();
    }

    public void completeTemporaryPermitApplicaionProofs(String address_proof, String address_proofpath, String option, String vehicle_proof, String vehicle_proofpath) {
        click_Cannot_provide_requested_proofs_of_address();
        selectTempPermitProofOfAddress(address_proof);
        sendUploadTempAddressProof(address_proofpath);
        click_Cannot_provide_request_proof_of_vehicles();
        selectIsThisVehicleUKRegistered(option);
        selectTempPermitProofOfVehicle(vehicle_proof);
        sendUploadTempVehicleProof(vehicle_proofpath);
        clickNextButton();

    }

    public void completeTemporaryEcoPermitApplicationProofs(String address_proof, String address_proofpath, String option, String vehicle_proof, String vehicle_proofpath) {
        click_Cannot_provide_requested_proofs_of_address();
        selectAddressProofEcoTempPermit(address_proof);
        uploadAddressProofEcoTempPermit(address_proofpath);
        click_Cannot_provide_request_proof_of_vehicles();
        selectIsThisVehicleUKRegistered(option);
        selectTempPermitProofOfVehicle(vehicle_proof);
        sendUploadTempVehicleProof(vehicle_proofpath);
        clickNextButton();
    }

    public void completeTemporaryMotorcyclePermitApplicationProofs(String address_proof, String address_proofpath, String vehicle_proof, String vehicle_proofpath) {
        click_Cannot_provide_requested_proofs_of_address();
        selectAddressProofEcoTempPermit(address_proof);
        uploadAddressProofEcoTempPermit(address_proofpath);
        click_Cannot_provide_request_proof_of_vehicles();
        selectEcoVR5(vehicle_proof);
        sendUploadPathToEcoVR5path(vehicle_proofpath);
        clickNextButton();
    }


    public void uploadMoreProofsForTemporaryPermit(String proofTwo, String proofonepath, String proof, String proofpath, String vehicle_proof, String vehicle_proofpath) {
        selectProofTwoType(proofTwo);
        sendUploadPathToProofTwo(proofonepath);
        selectCertificateOfInsurance(proof);
        sendUploadPathToProofFour(proofpath);
        selectTempPermitProofOfVehicle(vehicle_proof);
        sendUploadTempVehicleProof(vehicle_proofpath);
        clickNextButtonInEditAddressPage();
    }

    public void uploadMoreProofsForTemporaryEcoPermit(String tempProof, String tempProofPath, String proofTwo, String proofonepath, String proof, String proofpath, String vehicle_proof, String vehicle_proofpath) {
        selectECOPermitAddressProofOneTempPermit(tempProof);
        uploadEcoPermitAddressProofOneTempPermit(tempProofPath);
        selectECOPermitAdressProofOne(proofTwo);
        sendUploadPathToEcoAddressProofOne(proofonepath);
        selectCertificateOfInsurance(proof);
        sendUploadPathToProofFour(proofpath);
        selectTempPermitProofOfVehicle(vehicle_proof);
        sendUploadTempVehicleProof(vehicle_proofpath);
        clickNextButton();
    }

    public void uploadMoreProofsForTemporaryMotorcyclePermit(String tempProof, String tempProofPath, String proofTwo, String proofonepath, String proof, String proofpath, String vehicle_proof, String vehicle_proofpath) {
        selectECOPermitAddressProofOneTempPermit(tempProof);
        uploadEcoPermitAddressProofOneTempPermit(tempProofPath);
        selectECOPermitAdressProofOne(proofTwo);
        sendUploadPathToEcoAddressProofOne(proofonepath);
        selectProofForPermanentVRNChange(proof);
        sendUploadPathToPremanentProof(proofpath);
        selectTempPermitProofOfVehicle(vehicle_proof);
        sendUploadTempVehicleProof(vehicle_proofpath);
        clickNextButtonInEditAddressPage();
    }


    public void permitRenewalUploadProofs(String proof, String proofpath) {
        selectPermitRenewalAddressProofType(proof);
        sendUploadProofsForRenewalProof(proofpath);
        clickNextButton();
    }
    
    /**
     * Function to select yes to a super pack question
     */
    public void selectYesToSuperPack() {
     	WaitVisible(yes_super_pack);
    	 yes_super_pack.click(); 
    }
    
    /**
     * Function to select no to a super pack question 
     */
    public void selectNoToSuperPack() {
   	 WaitVisible(no_super_pack);
   	no_super_pack.click();
   }
    
    public boolean isTheResidentialProofPackQuestionDisplayed() {
     	WaitVisible(residential_proof_pack_question_one);
        return residential_proof_pack_question_one.isDisplayed();
    }
    
    /**
     * Function to click on the back button
     */
    public void navigateBack() {
     	WaitVisible(back_button);
    	back_button.click();
    }
}