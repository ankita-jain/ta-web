package PageObjects;

import static GenericComponents.constants.Symbols.POUND;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.CommonProperties;
import Utilities.OSUtils;

public class PageClass extends Base_Page{

    private static long DRIVER_WAIT_TIME = 10;
    public WebDriver driver;
    public WebDriverWait wait;
    
    static {
    	try {
    		// Try to obtain the driver wait time set in the environment.properties file
    		DRIVER_WAIT_TIME = CommonProperties.WebDriverWait();
    	} catch (Exception e) {
    		// Default to 10 seconds on exception
    		DRIVER_WAIT_TIME = 10;
    	}
    }
    
    public PageClass(WebDriver driver) {
    	super(driver);
        this.driver = driver;
    }


    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    /**
     * Returns the current page title from page
     */
    public String getCurrentPageTitle() {
        return driver.getTitle();
    }

    public boolean checkPageTitleContains(String title) {
        return new WebDriverWait(driver, DRIVER_WAIT_TIME).until(ExpectedConditions.titleContains(title));
    }

    public WebElement waitForExpectedElement(final By by) {
        return wait.until(visibilityOfElementLocated(by));
    }

    public void waitForRobotToLoad() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    protected ExpectedCondition<WebElement> visibilityOfElementLocated(final By by) throws NoSuchElementException {
        return new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {

                }
                WebElement element = driver.findElement(by);
                return element.isDisplayed() ? element : null;
            }
        };
    }

    public void waitFor() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {

        }
    }

    public void waitForRobotToRespond() {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {

        }
    }

    public String getCurrentDirectoryPath() {
        return OSUtils.getFullTestDataPath() + "uploads/";
    }

    public WebElement waitForElement(WebElement element) {
        return (new WebDriverWait(driver, DRIVER_WAIT_TIME)).until(ExpectedConditions.visibilityOf(element));
    }


    public WebElement waitForElementTOBeClickable(WebElement element) {
        return (new WebDriverWait(driver, DRIVER_WAIT_TIME)).until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitForTextToPresentOnElement(WebElement element, String text) {
        (new WebDriverWait(driver, DRIVER_WAIT_TIME)).until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    public void moveToMenuAndClickSubMenu(WebElement menu, WebElement submenu) {

        Actions builder = new Actions(driver);
        WebElement main_menu = new WebDriverWait(driver, DRIVER_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(menu));
        builder.moveToElement(main_menu).build().perform();
        WebElement sub_menu = new WebDriverWait(driver, DRIVER_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(submenu));
        builder.moveToElement(sub_menu).click().build().perform();

    }

    public void moveToMenuClickAndClickSubMenu(WebElement menu, WebElement submenu) {

        Actions builder = new Actions(driver);
        WebElement main_menu = new WebDriverWait(driver, DRIVER_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(menu));
        builder.moveToElement(main_menu).click().build().perform();
        WebElement sub_menu = new WebDriverWait(driver, DRIVER_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(submenu));
        builder.moveToElement(sub_menu).click().build().perform();

    }

    public void jsClick(WebElement element) {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("arguments[0].click();", element);
    }


    public void scrollDownPage() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    @Override
	public void hardPageRefresh() {
        JavascriptExecutor js = ((JavascriptExecutor) driver);
        js.executeScript("window.location = window.location.href;");
    }

    public void clicking(WebElement ele) {
        waitForElement(ele);
        ele.click();
    }

    public void enteringData(WebElement ele, String text) {
        waitForElement(ele);
        ele.sendKeys(text);
    }

    public void uploadData(WebElement ele, String text) {
        ele.sendKeys(text);
    }

    public void selectingOptions(WebElement ele, String text) {
        waitForElement(ele);
        new Select(ele).selectByVisibleText(text);
    }

    public WebElement prepareWebElementWithDynamicXpath(String xpathValue, String permitnumber) {
        return driver.findElement(By.xpath(xpathValue.replace("xxxxxx", permitnumber)));
    }

    public boolean isPresentAndDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }


    public boolean checkBalanceOnCorporateAccount(String permitcharges, String CorporateAccountBalance) {
        /*
         * Logic of this function is to trim the permit cost & corporate account balance from a string
         * For example, if the text is like "In order to apply for this permit type you need to pay a fee of £78.00.",
         * This function will trim the above string to '78.00' and do the rest of operation.
         */

        double permitCost, corporateAccountBalance;

        permitcharges = permitcharges.replaceAll("[-+^:,]", "");
        CorporateAccountBalance = CorporateAccountBalance.replaceAll("[-+^:,]", "");  //Removing all the unwanted characters from string.

        permitCost = Double.parseDouble(permitcharges.substring(permitcharges.lastIndexOf(POUND.getAsUTF8()) + 1, permitcharges.length() - 1));
        corporateAccountBalance = Double.parseDouble(CorporateAccountBalance.substring(CorporateAccountBalance.lastIndexOf(POUND.getAsUTF8()) + 1, CorporateAccountBalance.length() - 1));

        if (permitCost <= corporateAccountBalance)
            return true;

        else
            return false;

    }

    public boolean compareErrorMessages(List<WebElement> errorMessages, String errormessages) {

        String[] arrayErrorMessages = errormessages.split("\\-", -1);
        int i = 0;
        boolean flagCheck = true;

        for (WebElement errormsg : errorMessages) {

            if (!errormsg.getText().equals(arrayErrorMessages[i])) {
                flagCheck = false;
                break;
            }
            i++;
        }

        return flagCheck;
    }

    /*
     * Function 'getCurrentBalanceFromText' will take a string and
     * Returns current balance from text, For instance, if the string is ""Current funds balance: £7,771.70 Top Up"
     * Then, it will return '7771.70' from the string.
     */


    public String getCurrentBalanceFromText(String currentBalText) {
        String[] arrayOfStrings = currentBalText.split("\\s", -1);
        String currentBalance = null;
        for (String words : arrayOfStrings) {

            if (words.contains(POUND.getAsUTF8())) {
                currentBalance = words.replaceAll("[-+^:," + POUND.getAsUTF8() + "]", "");
                break;
            }

        }
        return currentBalance;

    }

    /*
     * This function will mask the card details and return a string of last four digits of the card, Expiry date and Year
     * For example, this function will return strings like 'Visa ending 1111, exp: 08/25'
     */

    public String maskCreditCard(String credit_card, String exp_month, String exp_year) {

        String[] cardNumbersArray = credit_card.split("\\s", -1);
        String lastFourDigitsOfCard = cardNumbersArray[3]; //Last 4 digits of the card
        exp_year = exp_year.substring(2); //Last 2 digits of the year
        String maskedCardDetails = "Visa ending " + lastFourDigitsOfCard + ", exp: " + convertMonthsToNumbers(exp_month) + "/" + exp_year;
        return maskedCardDetails;
    }

    public String convertMonthsToNumbers(String months) {
        String mnth = null;

        switch (months) {

            case "Jan":
                mnth = "01";
                break;
            case "Feb":
                mnth = "02";
                break;
            case "Mar":
                mnth = "03";
                break;
            case "Apr":
                mnth = "04";
            case "May":
                mnth = "05";
                break;
            case "Jun":
                mnth = "06";
                break;
            case "Jul":
                mnth = "07";
                break;
            case "Aug":
                mnth = "08";
                break;
            case "Sep":
                mnth = "09";
                break;
            case "Oct":
                mnth = "10";
                break;
            case "Nov":
                mnth = "11";
                break;
            case "Dec":
                mnth = "12";
                break;

        }

        return mnth;

    }

    /*
     * Function 'randomStringGenerator' will generate a random string
     *
     */

    public String randomStringGenerator(int count) {
        String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder builder = new StringBuilder();

        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        return builder.toString();

    }

    /*
     * Function 'randomNumberGenerator' will generate a random string
     *
     */
    public String randomNumberGenerator(int count) {
        String ALPHA_NUMERIC_STRING = "1234567890";
        StringBuilder builder = new StringBuilder();

        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        return builder.toString();

    }

    /*
     * Function 'randomVRNGenerator' will generate a VRN,
     * Each registration index consists of seven characters with a defined format.
     * The format here is used 'AREACODE' followed by 'AGE IDENTIFIER' followed by 'RANDOME LETTER'.
     * For example, the below function will return VRN in the following format 'ER17TRD'.
     *
     */

    public String randomVRNGenerator() {
        return randomStringGenerator(2) + randomNumberGenerator(2) + randomStringGenerator(3);
    }

    /*
     * The following function 'currentDatePlusOne()' will add one day to current date and return a future date in the format of '06 Sep 2017'
     *
     */

    public String currentDatePlusOne() {
        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
        String DateToStr;

        Calendar c = Calendar.getInstance();
        c.setTime(curDate);
        c.add(Calendar.DATE, 1); //1 day to current date

        Date currentDatePlusOne = c.getTime();
        DateToStr = format.format(currentDatePlusOne);

        return DateToStr;
    }
    
    public WebElement arrangeWebElementWithDynamicXpath(String xpathValue, String permitnumber) {
        return driver.findElement(By.xpath(xpathValue.replace("pppppp", permitnumber)));
    }
}
