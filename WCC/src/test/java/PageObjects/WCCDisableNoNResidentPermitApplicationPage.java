package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebDriverMethods;
import Structures.Result;

public class WCCDisableNoNResidentPermitApplicationPage extends Base_Page {

	public WebDriver driver;
	
	public WCCDisableNoNResidentPermitApplicationPage(WebDriver driver) {
		super(driver);
		
	}

	@FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement pageheader;
	
	@FindBy(xpath = "//*[@id='ApplicantTitle_3054']")
    @CacheLookup
    private WebElement title;
	
	@FindBy(xpath = "//*[@id='ApplicantFirstName_3121']")
    @CacheLookup
    private WebElement firstname;
	
	@FindBy(xpath = "//*[@id='ApplicantSurname_3122']")
    @CacheLookup
    private WebElement surname;
	
	@FindBy(xpath = "//*[@id='addAddressLink1']")
    @CacheLookup
    private WebElement addnewaddress;
	
	@FindBy(xpath = "//*[@id='ApplicantAddress_3124_Address1']")
    @CacheLookup
    private WebElement add_new_address_of_driver;
	
	@FindBy(xpath = "//*[@id='ApplicantAddress_3124_Address1']")
    @CacheLookup
    private WebElement addressline1;
	
	@FindBy(xpath = "//*[@id='ApplicantAddress_3124_Town']")
    @CacheLookup
    private WebElement town;
	
	@FindBy(xpath = "//*[@id='ApplicantAddress_3124_County']")
    @CacheLookup
    private WebElement county;

	@FindBy(xpath = "//*[@id='ApplicantAddress_3124_Postcode']")
    @CacheLookup
    private WebElement postcode;
		
	@FindBy(xpath = "//*[@id='ApplicantGender_3123_male']")
    @CacheLookup
    private WebElement gender_male;
	
	@FindBy(xpath = "//*[@id='ApplicantGender_3123_female']")
    @CacheLookup
    private WebElement gender_female;
	
	@FindBy(xpath = "//*[@id='ApplicantHomePhone_3126']")
    @CacheLookup
    private WebElement telephonenumber;
	
	@FindBy(xpath = "//*[@id='ApplicantNINumber_3128']")
    @CacheLookup
    private WebElement NInumber;
	
	@FindBy(xpath = "//*[@name='ApplicantDOB_3129[d]']")
    @CacheLookup
    private WebElement DOBDay;
	
	@FindBy(xpath = "//*[@name='ApplicantDOB_3129[M]']")
    @CacheLookup
    private WebElement DOBMonth;
	
	@FindBy(xpath = "//*[@name='ApplicantDOB_3129[Y]']")
    @CacheLookup
    private WebElement DOBYear;	
	
	@FindBy(xpath = "//*[@id='ApplicationReason_3072']")
    @CacheLookup
    private WebElement reason_for_permit;		
	
	@FindBy(xpath = "//*[@id='DisabilityType_3136']")
    @CacheLookup
    private WebElement disability_type;
	
	@FindBy(xpath = "//*[@id='MobilityAssessment_3109']")
    @CacheLookup
    private WebElement mobilityassessment;
	
	@FindBy(xpath = "//*[@id='field-vehicleoptions1']")
    @CacheLookup
    private WebElement VRM;
	
	@FindBy(xpath = "//*[@id='VehicleUser_3145']")
    @CacheLookup
    private WebElement disabled_applicant_dirver_or_passenger;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperName_3143']")
    @CacheLookup
    private WebElement driver_name;	
	
	@FindBy(xpath="//*[@id='addAddressLink3']")
	private WebElement add_new_address_vehicle_keeper;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperAddress_3144_Address1']")
    @CacheLookup
    private WebElement vehicle_keeper_address1;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperAddress_3144_Address2']")
    @CacheLookup
    private WebElement vehicle_keeper_address2;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperAddress_3144_Address3']")
    @CacheLookup
    private WebElement vehicle_keeper_address3;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperAddress_3144_Town']")
    @CacheLookup
    private WebElement vehicle_keeper_town;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperAddress_3144_County']")
    @CacheLookup
    private WebElement vehicle_keeper_county;
	
	@FindBy(xpath = "//*[@id='VehicleKeeperAddress_3144_Postcode']")
    @CacheLookup
    private WebElement vehicle_keeper_postcode;
	
	@FindBy(xpath = "//*[@id='buttons']/ol/li[1]/div/div/input[2]")
    @CacheLookup
    private WebElement back_button;
	
	@FindBy(xpath = "//*[@id='buttons']/ol/li[1]/div/div/input[1]")
    @CacheLookup
    private WebElement next_step_button;
	
	public String getPageHeader()
	{			
		WaitVisible(pageheader);
		return pageheader.getText();
	}
	
	public Result selectTitle(String Title)
	{
		WaitVisible(title);
		return selectItemFromDropdownByTextValue(title, Title, "Title selected");
	}
	
	public Result enterFirstName(String Fristname)
	{
		WaitVisible(firstname);
		return enterText(firstname, Fristname, "Firstname entered");
	}
	
	public Result enterSurName(String Lastname)
	{
		WaitVisible(surname);
		firstname.clear();
		return enterText(surname, Lastname, "Secondname entered");
	}
		
	public Result clickAddNewAddress()
	{
		WaitVisible(addnewaddress);
		return ClickControl(addnewaddress, "Click Add new Address");		
	}
	
	public Result clickAddNewAddressOfDriver()
	{
		WaitVisible(add_new_address_of_driver);
		return ClickControl(add_new_address_of_driver, "Click Add new Address of driver");
				
	}
	
	public Result enterAddressLine1(String AddressLine1)
	{
		WaitVisible(addressline1);
		return enterText(addressline1, AddressLine1, "Enter Address Line 1");
	}
	
	public Result enterTown(String Town)
	{
		WaitVisible(town);
		return enterText(town, Town, "Enter Town");
	}
	
	public Result selectCounty(String County)
	{
		WaitVisible(county);
		return selectItemFromDropdownByTextValue(county, County, "County selected");
	}
	
	public Result enterPostCode(String Postcode)
	{
		WaitVisible(postcode);
		return enterText(postcode, Postcode, "Enter Postman");
	}
	
	public void clickGenderMale()
	{
		WebDriverMethods.executeJavascript("arguments[0].click();",gender_male,_driver);
	}
	
	public void clickGenderFemale()
	{   
		WebDriverMethods.executeJavascript("arguments[0].click();", gender_female,_driver);
	}
	
	public Result enterTelephoneNumber(String Phonenumber)
	{
		WaitVisible(telephonenumber);
		return enterText(telephonenumber, Phonenumber, "Enter Phonenumber");
	}
	
	public Result enterNINumber(String NINumber)
	{
		WaitVisible(NInumber);
		return enterText(NInumber, NINumber, "Enter NINumber");
		
	}
	
	public Result selectDobDay(String dobday)
	{
		WaitVisible(DOBDay);
		return selectItemFromDropdownByTextValue(DOBDay, dobday, "dobday selected");
	}
	
	public Result selectDobMonth(String dobmonth)
	{		
		WaitVisible(DOBMonth);
		return selectItemFromDropdownByTextValue(DOBMonth, dobmonth, "dobmonth selected");		
	}
	
	public Result selectDobYear(String dobyear)
	{
		WaitVisible(DOBYear);
		return selectItemFromDropdownByTextValue(DOBYear, dobyear, "dobyear selected");
	}
	
	public Result selectReasonForPermit(String reason)
	{
		WaitVisible(reason_for_permit);
		return selectItemFromDropdownByTextValue(reason_for_permit, reason, "Reason for disable permit selected");
	}
	
	
	public Result selectDisabilityType(String disability)
	{		
		WaitVisible(disability_type);
		return selectItemFromDropdownByTextValue(disability_type, disability, "disability type selected");
	}
	
	public Result selectMobilityAssessment(String mobility_assessment)
	{
		WaitVisible(mobilityassessment);
		return selectItemFromDropdownByTextValue(mobilityassessment, mobility_assessment, "Willing to take a mobility assessment");
	}
	
	public Result selectVRM(String Vrm)
	{	
		WaitVisible(VRM);
		return selectItemFromDropdownByTextValue(VRM, Vrm, "Vrm selected");
	}
	
	public Result selectApplicantDriverOrPassenger(String driver_or_passenger)
	{
		WaitVisible(disabled_applicant_dirver_or_passenger);
		return selectItemFromDropdownByTextValue(disabled_applicant_dirver_or_passenger, driver_or_passenger, "Driver or Passenger selected");
	}
	
	public Result enterDriverName(String name)
	{
		WaitVisible(driver_name);
		return enterText(driver_name, name, "Driver name selected");
	}
	
	public Result clickAddNewDriverAddress()
	{
		WaitVisible(add_new_address_vehicle_keeper);
		return ClickControl(add_new_address_vehicle_keeper, "Adding driver new address");
	}
	
	public Result enterDriverAddressLine1(String addressOne)
	{
		WaitVisible(vehicle_keeper_address1);
		return enterText(vehicle_keeper_address1, addressOne, "Enter Driver addressOne");
	}

	public Result enterDriverTown(String driverTown)
	{
		WaitVisible(vehicle_keeper_town);
		return enterText(vehicle_keeper_town, driverTown, "Vehicle keeper town entered");
	}
	
	public Result selectDriverCounty(String county)
	{
		WaitVisible(vehicle_keeper_county);
		return selectItemFromDropdownByTextValue(vehicle_keeper_county, county, "Driver county selected");
	}
	
	public Result enterDriverPostcode(String postcode)
	{
		WaitVisible(vehicle_keeper_postcode);
		return enterText(vehicle_keeper_postcode, postcode, "Vehicle keeper postcode entered");
	}	
	
	public Result clickBackButton()
	{
		WaitVisible(back_button);
		return ClickControl(back_button, "Click next_step_button");		
	}	
	
	public Result clickNextStepButton()
	{
		WaitVisible(next_step_button);
		return ClickControl(next_step_button, "Click next_step_button");
	}


	
	public void fillApplicantDetails(String title, String firstname, String lastname, String addressline1, String town,
			 						 String county, String postcode, String gender,String phonenumber, String ninumber, String dobday, 
			 						 String dobmonth, String dobyear, String Reason_for_permit, String disability, String mobility_assessment, String Vrm,
			 						 String driver_or_passenger,String vehiclekeepername,String address1,
			 						 String Vehicle_keeper_town,String Vehicle_keeper_county, String Vehicle_keeper_postcode)
	{
		selectTitle(title);
		enterFirstName(firstname);
		enterSurName(lastname);
		clickAddNewAddress();
		enterAddressLine1(addressline1);
		enterTown(town);
		selectCounty(county);
		enterPostCode(postcode);
		clickGenderMale();
		enterTelephoneNumber(phonenumber);
		enterNINumber(ninumber);
		selectDobDay(dobday);
		selectDobMonth(dobmonth);
		selectDobYear(dobyear);		
		selectReasonForPermit(Reason_for_permit);
		selectDisabilityType(disability);
		selectMobilityAssessment(mobility_assessment);
		
		selectVRM(Vrm);		
		selectApplicantDriverOrPassenger(driver_or_passenger);
		enterDriverName(vehiclekeepername);
		clickAddNewDriverAddress();
		enterDriverAddressLine1(address1);
		enterDriverTown(Vehicle_keeper_town);
		selectDriverCounty(Vehicle_keeper_county);
		enterDriverPostcode(Vehicle_keeper_postcode);
		
		clickNextStepButton();	
		
	}
	
}
