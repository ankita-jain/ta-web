package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCResidentPermitPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement residentPermitPageTitle;
    @FindBy(xpath = "//*[@id='field-PostcodeSearch']")
    @CacheLookup
    private WebElement postcode;
    @FindBy(xpath = "//*[@id='submitPostcode']")
    @CacheLookup
    private WebElement findAddressButton;
    @FindBy(xpath = "//*[@id='SelectAddress']")
    @CacheLookup
    private WebElement selectAddress;
    @FindBy(xpath = "//*[@id='SelectParkingZone']")
    @CacheLookup
    private WebElement selectParkingZone;
    @FindBy(xpath = "//input[@value='Next Step']")
    private WebElement nextButton;

    public WCCResidentPermitPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterPostcode(String Postcode) {
        WaitVisible(postcode);
        postcode.clear();
        postcode.sendKeys(Postcode);
    }

    public String getResidentPermitPageTitle() {
        WaitVisible(residentPermitPageTitle);
        return residentPermitPageTitle.getText();
    }

    public void clickFindAddressButton() {
        WaitVisible(findAddressButton);
        findAddressButton.click();
    }

    public void selectAddress(String address) {
        WaitVisible(selectAddress);
        Select addresslist = new Select(selectAddress);
        addresslist.selectByVisibleText(address);
    }

    public void selectZone(String zone) {
        WaitVisible(selectParkingZone);
        Select zonelist = new Select(selectParkingZone);
        zonelist.selectByValue(zone);

    }

    public void clickNextButton() {
        waitForRobotToRespond();
        waitForElementTOBeClickable(nextButton);
        WaitVisible(nextButton);
        nextButton.click();

    }

    public void applyResidentPermitApplication(String postcode, String address, String zone) {
        enterPostcode(postcode);
        clickFindAddressButton();
        selectAddress(address);
        selectZone(zone);
        clickNextButton();

    }


}
