package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCTrafficPublicTransportPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement traffic_public_transport_title;
    @FindBy(xpath = "//*[contains(text(),'Traffic News')]")
    @CacheLookup
    private WebElement traffic_news;
    @FindBy(xpath = "//*[contains(text(),'Bus Timetable and Routes')]")
    @CacheLookup
    private WebElement bus_timetable_and_route;
    @FindBy(xpath = "//*[contains(text(),'Tube, DLR and Overground')]")
    @CacheLookup
    private WebElement tube_dlr_and_overground;
    @FindBy(xpath = "//*[contains(text(),'How to Get Around')]")
    @CacheLookup
    private WebElement how_to_get_around;

    public WCCTrafficPublicTransportPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTrafficPublicTransportTitle() {
        WaitVisible(traffic_public_transport_title);
        return traffic_public_transport_title.getText();
    }

    public void clickTrafficNews() {
        WaitVisible(traffic_news);
        traffic_news.click();
    }

    public void clickBusTimetableRoute() {
        WaitVisible(bus_timetable_and_route);
        bus_timetable_and_route.click();
    }

    public void clickTubeDLROverground() {
        WaitVisible(tube_dlr_and_overground);
        tube_dlr_and_overground.click();
    }

    public void clickHowToGetAround() {
        WaitVisible(how_to_get_around);
        how_to_get_around.click();
    }

    public String getTrafficNewsLink() {
        WaitVisible(traffic_news);
        return traffic_news.getAttribute("href");
    }

    public String getBusTimetableAndRouteLink() {
        WaitVisible(bus_timetable_and_route);
        return bus_timetable_and_route.getAttribute("href");
    }

    public String getTubDLROvergroundLink() {
        WaitVisible(tube_dlr_and_overground);
        return tube_dlr_and_overground.getAttribute("href");
    }

    public String getHowToGetAroundLink() {
        WaitVisible(how_to_get_around);
        return how_to_get_around.getAttribute("href");
    }


}






