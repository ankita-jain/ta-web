package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCTradePermitInfoPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement tradepermitInfoPageTitle;
    @FindBy(xpath = "//*[@name='labyrinth_rvpinfo_next']")
    @CacheLookup
    private WebElement trade_permit_apply;
    @FindBy(xpath = "//*[@name='labyrinth_cancel']")
    @CacheLookup
    private WebElement trade_permit_cancel;

    public WCCTradePermitInfoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTradePermitTitle() {
        WaitVisible(tradepermitInfoPageTitle);
        return tradepermitInfoPageTitle.getText();
    }

    public void clickTradePermitApply() {
        WaitVisible(trade_permit_apply);
        trade_permit_apply.click();
    }

    public void clickTradePermitCancel() {
        WaitVisible(trade_permit_cancel);
        trade_permit_cancel.click();
    }


}
