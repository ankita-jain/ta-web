package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;


public class WCCLoginPage extends Base_Page {

    @FindBy(xpath = "//*[@id='field-cli']")
    private WebElement email_or_phone;
    @FindBy(xpath = "//*[@id='field-pin']")
    private WebElement password;
    @FindBy(xpath = "//input[@id='labyrinth_Login_next']")
    private WebElement loginButton;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
    @FindBy(xpath = "//a[@href='/resetpassword']")
    private WebElement forgetPasswordLink;
    @FindBy(xpath = "//span[@class='error']")
    private WebElement errorElement;

    public WCCLoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result inputEmailOrPhonenumer(String email_or_phonenumber) {
        WaitVisible(email_or_phone);
        return enterText(email_or_phone, email_or_phonenumber, "Email or phone number input");
    }

    public Result enterPassword(String passkey) {
        WaitVisible(password);
        return enterText(password, passkey, "Password input");
    }

    public Result clickLoginButton() {
        WaitVisible(loginButton);
        return ClickControl(loginButton, "Login button");
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public Result clickForgetPassword() {
        WaitVisible(forgetPasswordLink);
        return ClickControl(forgetPasswordLink, "Forget password link");
    }

    public String getErrorText() {
        WaitVisible(errorElement);
        return errorElement.getText();
    }
}
