package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;


public class RingGoMobileUserAccountPage extends Base_Page {

    @FindBy(xpath = "//a[@href='/payment']")
    private WebElement edit_payment_details;
    
    
    public RingGoMobileUserAccountPage(WebDriver driver) {
        super(driver);
    }

    public Result clickEditPayment() {
        WaitVisible(edit_payment_details);
        return ClickControl(edit_payment_details, "Parking button");
    }

  
}
