package PageObjects.NewCorporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeDashboardFilterPopup extends Base_Page {

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Reference')]")
    public WebElement referenceTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//input[contains(@name, 'reference')]")
    public WebElement referenceInput;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Tel. number')]")
    public WebElement telNumberTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//input[contains(@name, 'cli')]")
    public WebElement telNumberInput;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Can book parking')]")
    public WebElement canParkTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//input[contains(@name, 'canPark') and @value='0']")
    public WebElement canParkInput;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Vehicles')]")
    public WebElement vehiclesTabLink;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Cost Centre')]")
    public WebElement costCentreTabLink;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Name')]")
    public WebElement nameTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//input[contains(@name, 'firstName')]")
    public WebElement firstNameInput;
    @FindBy(xpath = "//div[@class='modal-body']//input[contains(@name, 'surname')]")
    public WebElement secondNameInput;

    @FindBy(xpath = "//div[@class='modal-body']//input[@name='submit']")
    public WebElement saveButton;

    public EmployeeDashboardFilterPopup(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickReference(){
        return WebElementMethods.ClickControl(referenceTabLink, "Reference Tab");
    }
    public Result enterTextToReferenceInput(String reference) {
        return WebElementMethods.enterText(referenceInput, reference, "Reference input");
    }

    public Result clickTelNumber(){
        return WebElementMethods.ClickControl(telNumberTabLink, "Telephone Number Tab");
    }
    public Result enterTextToTelNumberInput(String telNumber) {
        return WebElementMethods.enterText(telNumberInput, telNumber, "Telephone Number input");
    }

    public Result clickCanPark(){
        return WebElementMethods.ClickControl(canParkTabLink, "Can Park Tab");
    }
    public Result setCanParkFalse() {
        return WebElementMethods.ClickControl(canParkInput, "Can Park input");
    }

    public Result clickVehicle(){
        return WebElementMethods.ClickControl(vehiclesTabLink, "Vehicles Tab");
    }
    public Result checkVehicle(String vehicle) {
        WebElement checkBox = _driver.findElement(By.xpath("//div[@class='modal-body']//input[@class='specific-vrn' and " +
                "contains(@name, '" + vehicle.toUpperCase() + "') and @value='1']"));
        return WebElementMethods.checkCheckBox(checkBox, "Vehicle checkbox");
    }

    public Result clickCostCentre(){
        return WebElementMethods.ClickControl(costCentreTabLink, "Vehicles Tab");
    }
    public Result checkCostCentre(String costCentre) {
        WebElement checkBox = _driver.findElement(By.xpath("//div[@class='modal-body']//label[contains(text(), '" + costCentre + "')]/preceding-sibling::input"));
        return WebElementMethods.checkCheckBox(checkBox, "Vehicle checkbox");
    }

    public Result clickName(){
        return WebElementMethods.ClickControl(nameTabLink, "Name Tab");
    }
    public Result enterFirstName(String firstName) {return WebElementMethods.enterText(firstNameInput, firstName, "First Name input");}
    public Result enterSurname(String surname) {return WebElementMethods.enterText(secondNameInput, surname, "Surname input");}

    public Result clickSaveButton(){
        return WebElementMethods.ClickControl(saveButton, "Save button");
    }
}
