package PageObjects.NewCorporate;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeDashboardPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//table[@id='dgridtable1']")
    public WebElement employeeTableObject;
    @FindBy(xpath = "//a[text()='Delete']")
    public WebElement deleteButton;
    @FindBy(xpath = "//a[text()='Assign to any vehicle']")
    public WebElement assignToAnyVehicleButton;
    @FindBy(xpath = "//a[text()='Remove from all vehicles']")
    public WebElement removeFromAllVehiclesButton;
    @FindBy(xpath = "//a[text()='Add employee']")
    public WebElement addEmployeeBuuton;
    @FindBy(xpath = "//a[text()='Filter']")
    public WebElement filterButton;
    @FindBy(xpath = "//a[text()='Import']")
    public WebElement importButton;
    @FindBy(xpath = "//a[text()='Export']")
    public WebElement exportButton;
    @FindBy(xpath = "//a[text()='Update']")
    public WebElement updateButton;
    @FindBy(xpath = "//a[text()='Send welcome email']")
    public WebElement sendWelcomeEmailButton;
    @FindBy(xpath = "//ul[@class='success-notification']")
    public WebElement successNotification;
    @FindBy(xpath = "//ul[@class='notice-notification']")
    public WebElement noticeNotification;
    @FindBy(className = "checkbox-column")
    public WebElement allEmployeesCheckbox;
    @FindBy(xpath = "//ul[@class='error-notification']")
    public WebElement errorNotification;

    public EmployeeDashboardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getSuccessNotificationMessage(){
        return successNotification.getText();
    }

    public String getNoticeNotificationMessage(){
        return noticeNotification.getText();
    }

    public String getErrorNotificationMessage(){
        return errorNotification.getText();
    }

    public Result clickDeleteButton(){
        return WebElementMethods.ClickControl(deleteButton, "Delete button");
    }

    public Result clickAssignToAnyVrn(){
        return WebElementMethods.ClickControl(assignToAnyVehicleButton, "Assign To Any Vehicle button");
    }

    public Result clickAllEmployeesCheckbox() {
        return WebElementMethods.checkCheckBox(allEmployeesCheckbox, "Select all employees checkbox");
    }

    public Result clickRemoveFromAllVrn(){
        return WebElementMethods.ClickControl(removeFromAllVehiclesButton, "Remove From All Vehicle button");
    }

    public Result clickFilterButton(){
        return WebElementMethods.ClickControl(filterButton, "Filter button");
    }

    public Result clickOnColumnHeader (EmployeeTableColumns column) {
        WebElement link = _driver.findElement(By.xpath("//a[text()='" + column.toString() + "']"));
        return WebElementMethods.ClickControl(link, "Clicking on column header");
    }

    public Table employeeTable = new Table(employeeTableObject);
    public CheckBox checkbox;
    public KebabMenuLink kebabMenuLink;
    public KebabMenu kebabMenu;
    public GetMoreTelephonesLink getMoreTelephonesLink;
    public GetMoreVehiclesLink getMoreVehiclesLink;
    public ModalBox modalBox;

    public enum EmployeeTableColumns {
        NAME("Name", 1),
        REFERENCE("Reference", 2),
        COST_CENTRE("Cost centre", 3),
        WELCOME_EMAIL("Welcome email", 4),
        TELEPHONE_NUMBERS("Telephone numbers", 5),
        VEHICLES("Vehicles", 6);
        String headerValue;
        int columnPosition;

        EmployeeTableColumns(String header, int position) {
            headerValue = header;
            columnPosition = position;
        }

        @Override
        public String toString() {
            return headerValue;
        }

        public int getPosition() { return  columnPosition; }
    }

    public static class CheckBox {
        private WebElement checkBoxElement;

        public CheckBox() {
        }

        public CheckBox(WebElement row, int rowIndex){
            checkBoxElement = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td/input[@type='checkbox']"));
        }

        public Result clickCheckBox(){
            return WebElementMethods.checkCheckBox(checkBoxElement, "Select employee checkbox");
        }
    }

    public static class GetMoreTelephonesLink {
        private WebElement getMoreTelephonesLink;

        public GetMoreTelephonesLink(WebElement row, int rowIndex){
            getMoreTelephonesLink = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td/a[@class='get-more-modal' and text()='...']"));
        }

        public Result clickGetMoreTelephonesLink(){
            return WebElementMethods.ClickControl(getMoreTelephonesLink, "Click on More Telephones link");
        }
    }

    public static class GetMoreVehiclesLink {
        private WebElement getMoreTelephonesLink;

        public GetMoreVehiclesLink(WebElement row, int rowIndex){
            getMoreTelephonesLink = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]//a[@class='get-more-modal' and contains(text(),'more')]"));
        }

        public Result clickGetMoreVehiclesLink(){
            return WebElementMethods.ClickControl(getMoreTelephonesLink, "Click on More Vehicles link");
        }
    }

    public static class KebabMenuLink {
        private WebElement kebabMenuElement;

        public KebabMenuLink(WebElement row, int rowIndex){
            kebabMenuElement = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td/a[@class='kebab-link']"));
        }

        public Result clickKebabMenu(){
            return WebElementMethods.ClickControl(kebabMenuElement, "Click kebab menu");
        }
    }

    public static class KebabMenu {
        public WebElement editLink;
        public WebElement deleteLink;
        public WebElement assignToAnyVehicleLink;
        public WebElement removeFromAllVehiclesLink;

        public KebabMenu(WebElement row, int rowIndex){
            editLink = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td[8]/div/div/ul/li[1]/a"));
            deleteLink = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td[8]/div/div/ul/li[2]/a"));
            assignToAnyVehicleLink = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td[8]/div/div/ul/li[3]/a"));
            removeFromAllVehiclesLink = row.findElement(By.xpath("//tr[@id=" + rowIndex + "]/td[8]/div/div/ul/li[4]/a"));
        }

        public Result clickDeleteLink() {
            return WebElementMethods.ClickControl(deleteLink, "Delete link");
        }

        public Result clickAssignToAnyVehicleLink() {
            return WebElementMethods.ClickControl(assignToAnyVehicleLink, "Assign to Any Vehicle link");
        }

        public Result clickRemoveFromAllVehiclesLink() {
            return WebElementMethods.ClickControl(removeFromAllVehiclesLink, "Remove from All Vehicles link");
        }
    }

    public static class ModalBox {
        private WebElement modalBox;

        public ModalBox(WebDriver driver) {
            modalBox = driver.findElement(By.xpath("//div[@class='modal-content']"));
        }

        public String getHeaderText(){
            WebElement header = modalBox.findElement(By.xpath("//div[@class='modal-header']"));
            return header.getText();
        }

        public String getBodyText(){
            WebElement header = modalBox.findElement(By.xpath("//div[@class='modal-body']"));
            return header.getText();
        }

        public Result clickOnCloseButton() {
            return  WebElementMethods.ClickControl(modalBox.findElement(By.className("close")), "Click on Close");
        }
    }
}
