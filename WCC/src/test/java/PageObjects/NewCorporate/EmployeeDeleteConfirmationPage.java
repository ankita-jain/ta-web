package PageObjects.NewCorporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeDeleteConfirmationPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//input[@value='Yes']")
    public WebElement yesButton;
    @FindBy(xpath = "//input[@value='No']")
    public WebElement noButton;

    public EmployeeDeleteConfirmationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickYes(){
        return WebElementMethods.ClickControl(yesButton, "Yes button");
    }

    public Result clickNo(){
        return WebElementMethods.ClickControl(noButton, "No button");
    }
}
