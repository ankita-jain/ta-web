package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCHelpCentreGeneralEnquiryPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='info[text]']")
    @CacheLookup
    private WebElement query;
    @FindBy(xpath = "//*[@id='labcancel']")
    @CacheLookup
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@name='labyrinth_ContactQuery_next']")
    @CacheLookup
    private WebElement nextButton;
    @FindBy(xpath = "//*[@class='box-content']/p[1]")
    @CacheLookup
    private WebElement submitQueryMessage;
    @FindBy(xpath = "//*[@name='labyrinth_finish']")
    @CacheLookup
    private WebElement finishButton;

    public WCCHelpCentreGeneralEnquiryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getGeneralEnquiryPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterQuery(String queryString) {
        WaitVisible(query);
        query.clear();
        query.sendKeys(queryString);
    }

    public void clickCancelButton() {
        WaitVisible(cancelButton);
        cancelButton.click();
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void clickFinishButton() {
        WaitVisible(finishButton);
        finishButton.click();
    }

    public void submitQueryProcess(String queryString) {
        enterQuery(queryString);
        clickNextButton();
    }

    public String getQuerySubmitSuccessMessage() {
        WaitVisible(submitQueryMessage);
        return submitQueryMessage.getText();
    }
}
