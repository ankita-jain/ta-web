package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class RingGoMobileLoginPage extends Base_Page {
    @FindBy(xpath = "//*[@id='field-cli']")
    private WebElement emailOrPhoneInput;
    @FindBy(xpath = "//*[@id='field-pin']")
    private WebElement password;
    @FindBy(xpath = "//input[@value='Log in']")
    private WebElement loginButton;

    public RingGoMobileLoginPage(WebDriver driver) {
        super(driver);
    }

    public Result enterEmailOrPhoneNumber(String emailOrPhoneNumber) {
        WaitVisible(emailOrPhoneInput);
        return enterText(emailOrPhoneInput, emailOrPhoneNumber, "Email or phone number input");
    }

    public Result enterPassword(String passkey) {
        WaitVisible(password);
        return enterText(password, passkey, "Password input");
    }

    public Result clickLoginButton() {
        WaitVisible(loginButton);
        return ClickControl(loginButton, "Login button");
    }
}
