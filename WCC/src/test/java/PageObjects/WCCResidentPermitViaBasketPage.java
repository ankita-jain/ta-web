package PageObjects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import SeleniumHelpers.Base_Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import Structures.Result;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import PageObjects.POHelpers.PaymentBasketTable;
import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCResidentPermitViaBasketPage extends PageClass{
	
	
	public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement basketPageTitle;
    @FindBy(xpath = "//a[@class='right']//button")
    @CacheLookup
    private WebElement continueShopping;
    @FindBy(xpath = "//a[@class='left']//button")
    @CacheLookup
    private WebElement payViaBasket;
    @FindBy(xpath = "//*[@id='1']/td[3]/p")
    @CacheLookup
    private WebElement verifyBasket;
    @FindBy(id="dgridtable1")
    @CacheLookup
    private WebElement basketTable;
    
  

    public WCCResidentPermitViaBasketPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getBasketPageTitle() {
        WaitVisible(basketPageTitle);
        return basketPageTitle.getText();
    }

    public void clickContinueButton() {
        jsClick(continueShopping);
       }
    
    public String verifyBasket() {
        WaitVisible(verifyBasket);
        return verifyBasket.getText();
    }

    public Result purchaseViaBasket() {
        WaitVisible(payViaBasket);
        return ClickControl(payViaBasket, "Pay Via Basket");
        
    }
    
    public List<String> basketTableHeaders() {
    	WaitVisible(basketTable);
    	List<String> headingList = new ArrayList<String>();
    	for (WebElement tableHeading : basketTable.findElements(By.tagName("th"))) {
    		if(!tableHeading.getText().isEmpty()) {
    			headingList.add(tableHeading.getText());
    		}
    	}
    	
    	return headingList;
    	
    }
    
    public List<PaymentBasketTable> basketContents() {
    	WaitVisible(basketTable);
    	List<PaymentBasketTable> tableContents = new ArrayList<PaymentBasketTable>();
    	for (WebElement tableRow : basketTable.findElements(By.tagName("tbody"))) {
    		PaymentBasketTable basketRow = new PaymentBasketTable();
    		
    				basketRow.Qty = tableRow.findElements(By.tagName("td")).get(1).getText();
    				basketRow.Item = tableRow.findElements(By.tagName("td")).get(2).getText();
    				basketRow.Cost = tableRow.findElements(By.tagName("td")).get(3).getText();
    				basketRow.Total = tableRow.findElements(By.tagName("td")).get(4).getText();
    				tableContents.add(basketRow);
    			}
    			
    		return tableContents;
    	}

 
}
