package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class RingGoMobileIndexPage extends Base_Page {

    @FindBy(xpath = "//li[2]/a[@href='/login']")
    private WebElement login;
    @FindBy(xpath = "//*[@id='homelink_position']")
    private WebElement burger_menu;
    @FindBy(xpath = "//a[@href='/logout']")
    private WebElement logout;

    public RingGoMobileIndexPage(WebDriver driver) {
        super(driver);
    }

    public Result clickLogin() {
        WaitVisible(login);
        return ClickControl(login, "Login button");
    }

    public Result openMenu() {
        WaitVisible(burger_menu);
        return ClickControl(burger_menu, "Click burger menu");
    }

    public Result clickLogout() {
        WaitVisible(logout);
        return ClickControl(logout, "Logout button");
    }
}
