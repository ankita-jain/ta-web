package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCPersonalDeleteConfirmationPage extends Base_Page {
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pageTitle;
    @FindBy(xpath = "//*[@value='Yes']")
    private WebElement deleteYeButton;
    @FindBy(xpath = "//*[@value='No']")
    private WebElement deleteNoButton;

    public WCCPersonalDeleteConfirmationPage(WebDriver driver) {
        super(driver);
    }

    public String getPageTitle() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public Result clickDeleteConfirmButton() {
        WaitVisible(deleteYeButton);
        return ClickControl(deleteYeButton, "Delete yes button");
    }

    public Result clickDeleteCancelButton() {
        WaitVisible(deleteNoButton);
        return ClickControl(deleteNoButton, "Delete No button");
    }

}