package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCDisablePermitProofPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@id='proofs110']")
    @CacheLookup
    private WebElement disable_permit_address_proof_type_one;
    @FindBy(xpath = "//*[@id='proofs109']")
    @CacheLookup
    private WebElement disable_permit_address_proof_type_two;
    @FindBy(xpath = "//*[@id='proof110']")
    @CacheLookup
    private WebElement disable_permit_address_proof_one;
    @FindBy(xpath = "//*[@id='proof109']")
    @CacheLookup
    private WebElement disable_permit_address_proof_two;
    @FindBy(xpath = "//*[@id='proofs119']")
    @CacheLookup
    private WebElement disable_permit_proof_of_eligibility;
    @FindBy(xpath = "//*[@id='proofs115']")
    @CacheLookup
    private WebElement disable_permit_other_proofs_types;
    @FindBy(xpath = "//*[@id='proof115']")
    @CacheLookup
    private WebElement disable_permit_other_proofs;
    @FindBy(xpath = "//*[@name='PostProof']")
    @CacheLookup
    private WebElement submit_prrof_by_post;
    @FindBy(xpath = "//*[@name='labyrinth_RvpProofs_next']")
    @CacheLookup
    private WebElement disable_permit_next_button;

    public WCCDisablePermitProofPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void selectProofofAddressOne(String proof_one) {
        WaitVisible(disable_permit_address_proof_type_one);
        new Select(disable_permit_address_proof_type_one).selectByVisibleText(proof_one);
    }

    public void uploadProofOfAddressOne(String proof) {
        disable_permit_address_proof_one.sendKeys(getCurrentDirectoryPath() + proof);
    }

    public void selectProofofAddressTwo(String proof_two) {
        WaitVisible(disable_permit_address_proof_type_two);
        new Select(disable_permit_address_proof_type_two).selectByVisibleText(proof_two);
    }

    public void uploadProofOfAddressTwo(String proof) {
        disable_permit_address_proof_two.sendKeys(getCurrentDirectoryPath() + proof);
    }

    public void uploadProofOfEligibilityOne(String proof) {
        disable_permit_proof_of_eligibility.sendKeys(getCurrentDirectoryPath() + proof);
    }

    public void selectProofofEligibilityTwo(String proof_one) {
        WaitVisible(disable_permit_other_proofs_types);
        new Select(disable_permit_other_proofs_types).selectByVisibleText(proof_one);
    }

    public void uploadProofOfEligibilityTwo(String proof) {
        disable_permit_other_proofs.sendKeys(getCurrentDirectoryPath() + proof);
    }

    public void clickNextStep() {
        WaitVisible(disable_permit_next_button);
        disable_permit_next_button.click();
    }

    public void uploadProofsBlueBadge(String proof_one, String proof1, String proof_two, String proof2, String proof3, String proof_four, String proof4) {
        selectProofofAddressOne(proof_one);
        uploadProofOfAddressOne(proof1);
        selectProofofAddressTwo(proof_two);
        uploadProofOfAddressTwo(proof2);
        uploadProofOfEligibilityOne(proof3);
        selectProofofEligibilityTwo(proof_four);
        uploadProofOfEligibilityTwo(proof4);
        clickNextStep();

    }


}
