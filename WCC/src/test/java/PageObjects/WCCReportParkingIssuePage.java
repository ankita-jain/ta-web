package PageObjects;

import SeleniumHelpers.WaitMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCReportParkingIssuePage extends PageClass {

    public WebDriver driver;
    private String parking_issue = "//*[contains(text(), 'xxxxxx')]";
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement reportingpageTitle;
    @FindBy(xpath = "//*[@id='roadNames']")
    @CacheLookup
    private WebElement road_name;
    @FindBy(xpath = "//*[@id='field-faultpic']")
    @CacheLookup
    private WebElement upload_photo;
    @FindBy(xpath = "//*[@value='Next']")
    @CacheLookup
    private WebElement next_button;
    @FindBy(xpath = "//*[@value='Finish']")
    @CacheLookup
    private WebElement finish_button;

    public WCCReportParkingIssuePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickParkingIssue(String issue) {
        WebElement parking_issue_button = prepareWebElementWithDynamicXpath(parking_issue, issue);
        parking_issue_button.click();
    }

    public String getHomePageTitle() {
        WaitVisible(reportingpageTitle);
        return reportingpageTitle.getText();
    }

    public void enterRoadName(String roadname) {
        WaitVisible(road_name);
        road_name.sendKeys(roadname);
    }

    public void uploadPhotoOfParkingIssue(String path) {
        upload_photo.sendKeys(getCurrentDirectoryPath() + path);
    }

    public void clickNextButton() {
        WaitVisible(next_button);
        next_button.click();
    }

    public void clickFinishButton() {
        WaitVisible(finish_button);
        finish_button.click();
    }


}
