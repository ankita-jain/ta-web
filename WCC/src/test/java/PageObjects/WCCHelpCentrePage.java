package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCHelpCentrePage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//div[@id='helparticlesbox']/a")
    private WebElement enquiries;
    @FindBy(xpath = "//a[@class='popuplink']")
    private WebElement raiseNewEnquiry;
    @FindBy(xpath = "//*[@id='areyousure']/div/p[2]/a")
    private WebElement continue_raise_new_enquiry;

    public WCCHelpCentrePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getHelpPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickEnquiries() {
        WaitVisible(enquiries);
        enquiries.click();
    }

    public void clickRaiseNewEnquiry() {
        WaitVisible(raiseNewEnquiry);
        raiseNewEnquiry.click();
    }

    public void continueRaiseNewEnquiry() {
        WaitVisible(continue_raise_new_enquiry);
        continue_raise_new_enquiry.click();
    }


}
