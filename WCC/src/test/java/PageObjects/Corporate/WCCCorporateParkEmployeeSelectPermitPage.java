package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateParkEmployeeSelectPermitPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[contains(text(),'Book a standard parking session')]//preceding::input[1]")
    @CacheLookup
    private WebElement standardParkingSession;
    @FindBy(xpath = "//*[@name='labyrinth_SelectPermit_next']")
    @CacheLookup
    private WebElement nextButton;

    public WCCCorporateParkEmployeeSelectPermitPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkEmployeePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void selectStandardParkingSession() {
        WaitVisible(standardParkingSession);
        standardParkingSession.click();
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void selectStandardParkingSessionAndClickNextButon() {
        selectStandardParkingSession();
        clickNextButton();
    }
}
