package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateParkingTransactionsHistoryPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='start']")
    @CacheLookup
    private WebElement startDate;
    @FindBy(xpath = "//*[@name='end']")
    @CacheLookup
    private WebElement endDate;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement searchButton;
    @FindBy(xpath = "//*[@class='box-content']/p[2]/a")
    @CacheLookup
    private WebElement exportCSVButton;
    @FindBy(xpath = "//*[@id='dgridtable1']")
    private WebElement searchResultTable;
    @FindBy(xpath = "//*[@id='dgridtable1']/tfoot/tr/td[2]")
    private WebElement searchResultCount;
    @FindBy(xpath = "//*[@class='box-content']/p[2]")
    private WebElement emptySearchResult;
    @FindBy(xpath = "//*[@name='employee']")
    @CacheLookup
    private WebElement employees;
    @FindBy(xpath = "//*[@name='costcentre']")
    @CacheLookup
    private WebElement costCentre;
    @FindBy(xpath = "//*[@name='method']")
    @CacheLookup
    private WebElement paymentChannel;
    @FindBy(xpath = "//*[@name='CorpType']")
    @CacheLookup
    private WebElement paymentType;

    public WCCCorporateParkingTransactionsHistoryPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkingTransactionHistoryPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void selectEmployee(String employee) {
        WaitVisible(employees);
        new Select(employees).selectByVisibleText(employee);
    }

    public void selectCostCentre(String costcentre) {
        WaitVisible(costCentre);
        new Select(costCentre).selectByVisibleText(costcentre);
    }

    public void selectPaymentChannel(String channel) {
        WaitVisible(paymentChannel);
        new Select(paymentChannel).selectByVisibleText(channel);
    }

    public void selectPaymentType(String paymenttype) {
        WaitVisible(paymentType);
        new Select(paymentType).selectByVisibleText(paymenttype);
    }

    public boolean isDataTableVisible() {
        return isPresentAndDisplayed(searchResultTable);
    }

    public boolean isSearchResultCountVisible() {
        return isPresentAndDisplayed(searchResultCount);
    }

    public String getEmptySearchResult() {
        return emptySearchResult.getText();
    }

    public void enterStartDate(String startdate) {
        WaitVisible(startDate);
        startDate.clear();
        startDate.sendKeys(startdate);
    }

    public void enterEndDate(String enddate) {
        WaitVisible(endDate);
        endDate.clear();
        endDate.sendKeys(enddate);
    }

    public void clickSearchButton() {
        WaitVisible(searchButton);
        searchButton.click();
    }

    public void clickExportCSVButton() {
        WaitVisible(exportCSVButton);
        exportCSVButton.click();
    }

    public void searchParkingTransactionHistory() {
        /*selectEmployee(employee);
        selectCostCentre(costcentre);
        selectPaymentChannel(channel);
        selectPaymentType(paymenttype);
        enterStartDate(startdate);
        enterEndDate(enddate);*/
        clickSearchButton();
    }

}
