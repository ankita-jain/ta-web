package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateEditDetailsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='field-AccountName']")
    @CacheLookup
    private WebElement accountName;
    @FindBy(xpath = "//*[@id='field-CompanyName']")
    @CacheLookup
    private WebElement companyName;
    @FindBy(xpath = "//*[@id='field-Department']")
    @CacheLookup
    private WebElement department;
    @FindBy(xpath = "//*[@id='field-CompanyNumber']")
    @CacheLookup
    private WebElement companyNumber;
    @FindBy(xpath = "//*[@id='field-VATRegistrationNumber']")
    @CacheLookup
    private WebElement companyRegNumber;
    @FindBy(xpath = "//*[@id='field-Address1']")
    @CacheLookup
    private WebElement companyAddress;
    @FindBy(xpath = "//*[@id='field-Postcode']")
    @CacheLookup
    private WebElement companyPostCode;
    @FindBy(xpath = "//*[@id='field-Country']")
    @CacheLookup
    private WebElement companyCountry;
    @FindBy(xpath = "//*[@id='field-Employees']")
    @CacheLookup
    private WebElement companyEmployees;
    @FindBy(xpath = "//*[@id='field-Tel']")
    @CacheLookup
    private WebElement companyTelephone;
    @FindBy(xpath = "//*[@value='Save']")
    @CacheLookup
    private WebElement saveButton;
    @FindBy(xpath = "//*[@value='Cancel']")
    @CacheLookup
    private WebElement cancelButton;

    public WCCCorporateEditDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getEditDetailsPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterAccountName(String accountname) {
        WaitVisible(accountName);
        accountName.clear();
        accountName.sendKeys(accountname);
    }

    public void enterDepartment(String deptname) {
        WaitVisible(department);
        department.clear();
        department.sendKeys(deptname);
    }

    public void enterCompanyName(String companyname) {
        WaitVisible(companyName);
        companyName.clear();
        companyName.sendKeys(companyname);
    }

    public void enterCompanyNumber(String companynumber) {
        WaitVisible(companyNumber);
        companyNumber.clear();
        companyNumber.sendKeys(companynumber);
    }

    public void enterCompanyRegNumber(String companyregnumber) {
        WaitVisible(companyRegNumber);
        companyRegNumber.clear();
        companyRegNumber.sendKeys(companyregnumber);
    }

    public void enterCompanyAddress(String companyaddress) {
        WaitVisible(companyAddress);
        companyAddress.clear();
        companyAddress.sendKeys(companyaddress);
    }

    public void enterCompanyPostcode(String postcode) {
        WaitVisible(companyPostCode);
        companyPostCode.clear();
        companyPostCode.sendKeys(postcode);
    }

    public void selectCompanyCountry(String country) {
        WaitVisible(companyCountry);
        new Select(companyCountry).selectByVisibleText(country);
    }

    public void selectNoOfEmployee(String employeeNo) {
        WaitVisible(companyEmployees);
        new Select(companyEmployees).selectByVisibleText(employeeNo);
    }

    public void enterCompanyTelephoneNumber(String telephone) {
        WaitVisible(companyTelephone);
        companyTelephone.clear();
        companyTelephone.sendKeys(telephone);
    }

    public void clickSaveButton() {
        WaitVisible(saveButton);
        saveButton.click();
    }

    public void clickCancelButton() {
        WaitVisible(cancelButton);
        cancelButton.click();
    }

    public void editCompanyDetails(String accountname, String companyname, String deptname, String companynumber, String companyregnumber, String companyaddress, String postcode, String country, String employeeNo, String telephone) {
        enterAccountName(accountname);
        enterCompanyName(companyname);
        enterDepartment(deptname);
        enterCompanyNumber(companynumber);
        enterCompanyRegNumber(companyregnumber);
        enterCompanyAddress(companyaddress);
        enterCompanyPostcode(postcode);
        selectCompanyCountry(country);
        selectNoOfEmployee(employeeNo);
        enterCompanyTelephoneNumber(telephone);
        clickSaveButton();

    }


}
