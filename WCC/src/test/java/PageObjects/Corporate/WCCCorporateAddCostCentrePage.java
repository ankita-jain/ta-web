package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;


public class WCCCorporateAddCostCentrePage extends Base_Page {

    @FindBy(xpath = "//div[@class='page-head']//h1")
    private WebElement costcenterPageTitle;
    @FindBy(xpath = "//input[@id='field-CostCentreName']")
    private WebElement costCentreName;
    @FindBy(xpath = "//input[@id='field-CostCentreNumber']")
    private WebElement costCentreNumber;
    @FindBy(xpath = "//input[@id='field-CostCentreOwner']")
    private WebElement costCentreOwner;
    @FindBy(xpath = "//input[@name='submits[submit]']")
    private WebElement costCentreSaveButton;
    @FindBy(xpath = "//input[@name='submits[cancel]']")
    private WebElement costCentreCancelButton;

    public WCCCorporateAddCostCentrePage(WebDriver driver) {
        super(driver);
    }

    public String getCostCentrePageTitle() {
        WaitVisible(costcenterPageTitle);
        return costcenterPageTitle.getText();
    }

    public Result enterCostCentreName(String costCenter) {
        WaitVisible(costCentreName);
        return enterText(costCentreName, costCenter, "Cost center name input");
    }

    public Result enterCostCentreNumber(String number) {
        WaitVisible(costCentreNumber);
        return enterText(costCentreNumber, number, "Number input");
    }

    public Result enterCostCentreOwner(String owner) {
        WaitVisible(costCentreOwner);
        return enterText(costCentreOwner, owner, "Owner input");
    }

    public Result save() {
        WaitVisible(costCentreSaveButton);
        return ClickControl(costCentreSaveButton, "Save button");
    }

    public Result cancel() {
        WaitVisible(costCentreCancelButton);
        return ClickControl(costCentreCancelButton, "Cancel button");
    }
}
