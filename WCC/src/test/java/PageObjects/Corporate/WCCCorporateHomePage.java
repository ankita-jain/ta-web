package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateHomePage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement corporateHomePageTitle;

    @FindBy(xpath = "//*[@class='permitlinkcontainer'][5]/div[2]")
    private WebElement help;

    @FindBy(xpath = "//a[@href='/setup']")
    private WebElement setup;

    @FindBy(xpath = "//a[@href='/wccfunds']")
    private WebElement funds;

    @FindBy(xpath = "//a[@href='/wccaccount']")
    private WebElement account;

    @FindBy(xpath = "//a[@href='/reports']")
    private WebElement reports;

    @FindBy(xpath = "//*[@class='permitwidgetcontainer']/div[5]/a")
    private WebElement permits;

    @FindBy(xpath = "//*[@class='permitwidgetcontainer']/div[6]/a")
    private WebElement suspension_and_dispensation;

    @FindBy(xpath = "//*[@href='/park']")
    private WebElement park;

    public WCCCorporateHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCoporateHomePageTitle() {
        WaitVisible(corporateHomePageTitle);
        return corporateHomePageTitle.getText();
    }

    public Result clickReports() {
        WaitVisible(reports);
        return WebElementMethods.ClickControl(reports, "Reports");
    }

    public Result clickPermit() {
        WaitVisible(permits);
        return WebElementMethods.ClickControl(permits, "Permits");
    }

    public Result clickSuspensionLink() {
        WaitVisible(suspension_and_dispensation);
        return WebElementMethods.ClickControl(suspension_and_dispensation, "Suspension link");
    }

    public String getSuspensionLink() {
        WaitVisible(suspension_and_dispensation);
        return suspension_and_dispensation.getAttribute("href");
    }

    public Result clickHelp() {
        WaitVisible(help);
        return WebElementMethods.ClickControl(help, "Help");
    }

    public Result clickSetUp() {
        WaitVisible(setup);
        return WebElementMethods.ClickControl(setup, "Setup");
    }

    public Result clickFunds() {
        WaitVisible(funds);
        return WebElementMethods.ClickControl(funds, "Funds");
    }

    public Result clickAccounts() {
        WaitVisible(account);
        return WebElementMethods.ClickControl(account, "Accounts");
    }

    public Result clickPark() {
        WaitVisible(park);
        return WebElementMethods.ClickControl(park, "Park");
    }


}
