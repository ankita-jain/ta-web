package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateAccountActivityPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='startdate']")
    @CacheLookup
    private WebElement startDate;
    @FindBy(xpath = "//*[@name='enddate']")
    @CacheLookup
    private WebElement endDate;
    @FindBy(xpath = "//*[@name='type']")
    @CacheLookup
    private WebElement accountActivityType;
    @FindBy(xpath = "//*[@id='dgridtable1']")
    private WebElement searchResultTable;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement searchButton;

    public WCCCorporateAccountActivityPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getAccountActivityPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterStartDate(String startdate) {
        WaitVisible(startDate);
        startDate.clear();
        startDate.sendKeys(startdate);
    }

    public void enterEndDate(String enddate) {
        WaitVisible(endDate);
        endDate.clear();
        endDate.sendKeys(enddate);
    }

    public void selectActivityType(String type) {
        WaitVisible(accountActivityType);
        new Select(accountActivityType).selectByVisibleText(type);
    }

    public void clickSearchButton() {
        WaitVisible(searchButton);
        searchButton.click();
    }

    public boolean isDataTableVisible() {
        return isPresentAndDisplayed(searchResultTable);
    }

    public void searchAccountActivity() {
        //enterStartDate(startdate);
        //enterEndDate(enddate);
       // selectActivityType(type);
        clickSearchButton();
    }


}
