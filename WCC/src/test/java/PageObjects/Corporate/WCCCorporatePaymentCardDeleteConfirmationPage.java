package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporatePaymentCardDeleteConfirmationPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1[1]")
    private WebElement DeletionPageTitle;
    @FindBy(xpath = "//*[@value='Yes']")
    private WebElement deleteConfirmationButton;
    @FindBy(xpath = "//*[@value='No']")
    private WebElement deleteCancelButton;

    public WCCCorporatePaymentCardDeleteConfirmationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCostCentreDeletionConfirmationPageTitle() {
        WaitVisible(DeletionPageTitle);
        return DeletionPageTitle.getText();
    }

    public Result clickDeleteConfirmationYesButton() {
        WaitVisible(deleteConfirmationButton);
        return ClickControl(deleteConfirmationButton, "Delete confirm button");
    }

    public Result clickDeleteConfirmationNoButton() {
        WaitVisible(deleteCancelButton);
        return ClickControl(deleteCancelButton, "Delete cancel button");
    }


}
