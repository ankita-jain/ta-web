package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateDeleteConfirmationPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement pageTitle;
    @FindBy(xpath = "//*[@value='Yes']")
    @CacheLookup
    private WebElement deleteYeButton;
    @FindBy(xpath = "//*[@value='No']")
    @CacheLookup
    private WebElement deleteNoButton;

    public WCCCorporateDeleteConfirmationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public Result clickDeleteConfirmButton() {
        WaitVisible(deleteYeButton);
        return WebElementMethods.ClickControl(deleteYeButton, "Yes button");
    }

    public Result clickDeleteCancelButton() {
        WaitVisible(deleteNoButton);
        return WebElementMethods.ClickControl(deleteNoButton, "No button");
    }
}
