package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateSkipLicencePermitApplicationPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='field-Member_Firstname']")
    @CacheLookup
    private WebElement first_name;
    @FindBy(xpath = "//*[@id='field-Member_Lastname']")
    @CacheLookup
    private WebElement second_name;
    @FindBy(xpath = "//*[@id='memberEmail']")
    @CacheLookup
    private WebElement primary_email;
    @FindBy(xpath = "//*[@id='field-Member_Email_Confirm']")
    @CacheLookup
    private WebElement confirm_email;
    @FindBy(xpath = "//*[@id='roadname']")
    @CacheLookup
    private WebElement road_name;
    @FindBy(xpath = "//*[@id='NoOfVehicles']")
    @CacheLookup
    private WebElement no_of_vehicle;
    @FindBy(xpath = "//*[@id='SkipMaterialSelect']")
    @CacheLookup
    private WebElement skip_material;
    @FindBy(xpath = "//*[@id='SkipTCs']")
    @CacheLookup
    private WebElement skip_tc;
    @FindBy(xpath = "//*[@id='labcancel']")
    @CacheLookup
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@name='labyrinth_Options_back']")
    @CacheLookup
    private WebElement backButton;
    @FindBy(xpath = "//*[@name='labyrinth_Options_next']")
    @CacheLookup
    private WebElement nextButton;
    @FindBy(xpath = "//*[@class='element div-static1']/div")
    @CacheLookup
    private WebElement permitcharge;
    @FindBy(xpath = "//*[@class='element div-corporateBalanceMessage']/div")
    @CacheLookup
    private WebElement corporateAccountBalance;

    public WCCCorporateSkipLicencePermitApplicationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getSkipLicencePermitPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public String getPermitCharge() {
        WaitVisible(permitcharge);
        return permitcharge.getText();
    }

    public String getCorporateAccountBalance() {
        WaitVisible(corporateAccountBalance);
        return corporateAccountBalance.getText();
    }

    public void enterFristName(String firstname) {
        WaitVisible(first_name);
        first_name.clear();
        first_name.sendKeys(firstname);
    }

    public void enterLastName(String secondname) {
        WaitVisible(second_name);
        second_name.clear();
        second_name.sendKeys(secondname);
    }

    public void enterPrimaryEmail(String primaryemail) {
        WaitVisible(primary_email);
        primary_email.clear();
        primary_email.sendKeys(primaryemail);
    }

    public void enterConfirmPrimaryEmail(String confirmprimaryemail) {
        WaitVisible(primary_email);
        confirm_email.clear();
        confirm_email.sendKeys(confirmprimaryemail);
    }

    public void enterRoad(String roadname) {
        WaitVisible(road_name);
        road_name.clear();
        road_name.sendKeys(roadname);
    }

    public void enterSkips(String skipnumber) {
        WaitVisible(no_of_vehicle);
        no_of_vehicle.clear();
        no_of_vehicle.sendKeys(skipnumber);
    }

    public void selectMaterials(String materials) {
        WaitVisible(skip_material);
        new Select(skip_material).selectByVisibleText(materials);
    }

    public void selectSkipTC() {
        WaitVisible(skip_tc);
        skip_tc.click();
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void applySkipLicenceProcess(String firstname, String secondname, String primaryemail, String confirmprimaryemail, String roadname, String skipnumber, String materials) {
        enterFristName(firstname);
        enterLastName(secondname);
        enterPrimaryEmail(primaryemail);
        enterConfirmPrimaryEmail(confirmprimaryemail);
        enterRoad(roadname);
        enterSkips(skipnumber);
        selectMaterials(materials);
        selectSkipTC();
        clickNextButton();

    }

    public boolean isCorporateAccountHasEnoughBalanace() {
        return checkBalanceOnCorporateAccount(getPermitCharge(), getCorporateAccountBalance());
    }
}
