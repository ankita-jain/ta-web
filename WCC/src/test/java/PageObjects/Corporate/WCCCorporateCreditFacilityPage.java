package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateCreditFacilityPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement creditFacilityPageTitle;

    public WCCCorporateCreditFacilityPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCreditFacilityPageTitle() {
        WaitVisible(creditFacilityPageTitle);
        return creditFacilityPageTitle.getText();
    }

}
