package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateParkEmployee extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='labyrinth_SelectEmployee_next']")
    @CacheLookup
    private WebElement nextButton;
    @FindBy(xpath = "//*[@id='field-Employee']")
    @CacheLookup
    private WebElement employee;

    public WCCCorporateParkEmployee(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkEmployeePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void selectEmployee(String employeeName) {
        WaitVisible(employee);
        new Select(employee).selectByVisibleText(employeeName);
    }

    public void selectemployeeAndClickNext(String employeeName) {
        selectEmployee(employeeName);
        clickNextButton();
    }
}
