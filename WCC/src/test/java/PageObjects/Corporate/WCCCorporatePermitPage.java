package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporatePermitPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='permitapps']/a")
    @CacheLookup
    private WebElement apply_for_permit;
    @FindBy(xpath = "//*[@id='otherpermits']/li[1]/a")
    @CacheLookup
    private WebElement skip_licence_application;
    @FindBy(xpath = "//*[@id='otherpermits']/li[2]/a")
    @CacheLookup
    private WebElement trade_application;

    public WCCCorporatePermitPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCorporatePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickPermitButton() {
        WaitVisible(apply_for_permit);
        apply_for_permit.click();
    }

    public void clickSkipLicencePermit() {
        clickPermitButton();
        moveToMenuAndClickSubMenu(apply_for_permit, skip_licence_application);
    }

    public void clickTradePermit() {
        clickPermitButton();
        moveToMenuAndClickSubMenu(apply_for_permit, trade_application);
    }
}
