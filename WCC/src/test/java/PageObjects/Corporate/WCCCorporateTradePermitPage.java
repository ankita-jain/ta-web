package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class WCCCorporateTradePermitPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='employees']")
    @CacheLookup
    private WebElement employee;
    @FindBy(xpath = "//*[@id='CLIId']")
    private WebElement cli;
    @FindBy(xpath = "//*[@id='memberEmail']")
    private WebElement employee_email;
    @FindBy(xpath = "//*[@id='field-Member_Email_Confirm']")
    private WebElement confirm_employee_email;
    @FindBy(xpath = "//*[@id='field-ParkingZone']")
    @CacheLookup
    private WebElement parking_zone;
    @FindBy(xpath = "//*[@id='Trade2']")
    @CacheLookup
    private WebElement reason_for_trade_permit;
    @FindBy(xpath = "//*[@name='Terms']")
    @CacheLookup
    private WebElement terms_and_conditons;
    @FindBy(xpath = "//*[@id='VRNId1']")
    @CacheLookup
    private WebElement vrn;
    @FindBy(xpath = "//*[@name='labyrinth_Options_next']")
    @CacheLookup
    private WebElement confirm_button;
    @FindBy(xpath = "//*[@id='labcancel']")
    @CacheLookup
    private WebElement cancel_button;
    @FindBy(xpath = "//*[@id='datepicker']")
    @CacheLookup
    private WebElement permit_date;
    @FindBy(xpath = "//*[@id='field-tariff']")
    @CacheLookup
    private WebElement permit_duration;
    @FindBy(xpath = "//*[@name='labyrinth_SelectPermitTariff_next']")
    @CacheLookup
    private WebElement next_button;
    @FindBy(xpath = "//*[@name='labyrinth_ConfirmPermit_next']")
    @CacheLookup
    private WebElement purchase_confirm_button;
    @FindBy(xpath = "//*[@class='error']")
    @CacheLookup
    private List<WebElement> errorMessages;

    public WCCCorporateTradePermitPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTradePermitPageTitle() {
    	WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void chooseEmployee(String employeename) {
    	WaitVisible(employee);
        new Select(employee).selectByVisibleText(employeename);
    }

    public void chooseTelephoneNumber(String number) {
    	WaitVisible(cli);
        new Select(cli).selectByVisibleText(number);
    }

    public void enterEmployeeEmail(String email) {
    	WaitVisible(employee_email);
        employee_email.clear();
        employee_email.sendKeys(email);
    }

    public void confirmEmployeeEmail(String email) {
    	WaitVisible(confirm_employee_email);
        confirm_employee_email.clear();
        confirm_employee_email.sendKeys(email);
    }

    public void chooseZone(String zone) {
    	WaitVisible(parking_zone);
        new Select(parking_zone).selectByVisibleText(zone);
    }

    public void chooseVehicle(String vehicle) {
    	WaitVisible(vrn);
        new Select(vrn).selectByVisibleText(vehicle);
    }

    public void enterReasonsForPermit(String reason) {
    	WaitVisible(reason_for_trade_permit);
        reason_for_trade_permit.clear();
        reason_for_trade_permit.sendKeys(reason);
    }

    public void agreeTermsAndConditions() {
    	WaitVisible(terms_and_conditons);
        ScrollToElement(terms_and_conditons);
        WaitClickable(terms_and_conditons);

        if (!terms_and_conditons.isSelected())
            terms_and_conditons.click();
    }

    public void clickConfirmButton() {
        WaitVisible(confirm_button);
        ScrollToElement(confirm_button);
        WaitClickable(confirm_button);
        confirm_button.click();
    }

    public void enterPermitDate() {
        String nextDate = currentDatePlusOne();
        WaitVisible(permit_date);
        permit_date.clear();
        permit_date.sendKeys(nextDate);
    }

    public void selectPermitDuration(String duration) {
    	WaitVisible(permit_duration);
        new Select(permit_duration).selectByVisibleText(duration);
    }

    public void clickNextButton() {
        WaitVisible(next_button);
        ScrollToElement(next_button);
        WaitClickable(next_button);
        next_button.click();
    }

    public void clickPurchaseConfirmButton() {
        WaitVisible(purchase_confirm_button);
        ScrollToElement(purchase_confirm_button);
        WaitClickable(purchase_confirm_button);
        purchase_confirm_button.click();
    }

    public List<WebElement> getErrorMessages() {
        return errorMessages;
    }

    public void applyTradePermit(String employeename, String number, String email, String confrimEmail, String zone, String reason, String vehicle, String permit_duration) {
        chooseEmployee(employeename);
        enterEmployeeEmail(email);
        confirmEmployeeEmail(confrimEmail);
        chooseZone(zone);
        enterReasonsForPermit(reason);
        chooseTelephoneNumber(number);
        agreeTermsAndConditions();
        chooseVehicle(vehicle);
        clickConfirmButton();
        enterPermitDate();
        selectPermitDuration(permit_duration);
        clickNextButton();
        clickPurchaseConfirmButton();
    }

    public boolean CheckErrorMessages(String errorString) {
        return compareErrorMessages(errorMessages, errorString);
    }

}
