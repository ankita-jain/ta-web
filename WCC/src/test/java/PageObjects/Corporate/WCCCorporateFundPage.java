package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporateFundPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement fundPageTitle;
    @FindBy(xpath = "//a[@href='/funds']/h3")
    private WebElement topup;
    @FindBy(xpath = "//a[@href='/autotopup']/h3")
    private WebElement autoTopup;
    @FindBy(xpath = "//a[@href='/payment']/h3")
    private WebElement paymentCards;
    @FindBy(xpath = "//*[contains(text(),'Credit Facility')]")
    private WebElement creditFacility;

    public WCCCorporateFundPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getFundPageTitle() {
        WaitVisible(fundPageTitle);
        return fundPageTitle.getText();
    }

    public Result clickTopup() {
        WaitVisible(topup);
        return ClickControl(topup, "'Top up' button");
    }

    public Result clickAutoTopup() {
        WaitVisible(autoTopup);
        return ClickControl(autoTopup, "'Auto top up' button");
    }

    public Result clickPaymentCards() {
        WaitVisible(paymentCards);
        return ClickControl(paymentCards, "'Payment card' button");
    }

    public Result clickCreditFacility() {
        WaitVisible(creditFacility);
        return ClickControl(creditFacility, "'Credit facility' button");
    }


}
