package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateRequestCallBackPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;

    public WCCCorporateRequestCallBackPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getRequestPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }


}
