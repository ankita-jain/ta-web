package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateReportsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//a[@href='/history']")
    @CacheLookup
    private WebElement parkingHistory;
    @FindBy(xpath = "//a[@href='/ccsummary']")
    @CacheLookup
    private WebElement costCentreSummary;
    @FindBy(xpath = "//a[@href='/fundshistory']")
    @CacheLookup
    private WebElement accountActivity;
    @FindBy(xpath = "//div[@class='permitlinkslight']//a[@href='/invoices']")
    @CacheLookup
    private WebElement vatInvoices;

    public WCCCorporateReportsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getReportPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickParkingHistory() {
        WaitVisible(parkingHistory);
        parkingHistory.click();
    }

    public void clickCostCentreSummary() {
        WaitVisible(costCentreSummary);
        costCentreSummary.click();
    }

    public void clickAccountActivity() {
        WaitVisible(accountActivity);
        accountActivity.click();
    }

    public void clickVATInvoices() {
        WaitVisible(vatInvoices);
        vatInvoices.click();
    }

}
