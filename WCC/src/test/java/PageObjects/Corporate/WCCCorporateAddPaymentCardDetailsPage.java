package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCCorporateAddPaymentCardDetailsPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement paymentCardAddPageTitle;
    @FindBy(xpath = "//*[@id='field-CcNumber']")
    private WebElement creditCardNumber;
    @FindBy(xpath = "//*[@id='field-CcExpire']")
    private WebElement paymentCardExpiryMonth;
    @FindBy(xpath = "//*[@name='CcExpire[Y]']")
    private WebElement paymentCardExpiryYear;
    @FindBy(xpath = "//*[@id='field-Address1']")
    private WebElement paymentCardAddressOne;
    @FindBy(xpath = "//*[@id='field-Town']")
    private WebElement paymentCardTown;
    @FindBy(xpath = "//*[@id='field-County']")
    private WebElement paymentCardAddCounty;
    @FindBy(xpath = "//*[@id='field-Corporate-County']")
    private WebElement paymentCardAddCountyNew;
    @FindBy(xpath = "//*[@id='field-Postcode']")
    private WebElement paymentCardPostcode;
    @FindBy(xpath = "//*[@id='field-Corporate-Postcode']")
    private WebElement paymentCardPostcodeNew;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    private WebElement paymentCardSaveButton;
    @FindBy(xpath = "//*[@id='labyrinth_CorpPermitCardNew_next']")
    private WebElement paymentCardSaveNextButton;
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement paymentCardCancelButton;
    @FindBy(xpath = "//*[@class='error']")
    private List<WebElement> errorMessages;
    @FindBy(className = "gp-card-trigger")
    private WebElement addPaymentCardButton;
    

    public WCCCorporateAddPaymentCardDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPaymentAddPageTitle() {
        WaitVisible(paymentCardAddPageTitle);
        return paymentCardAddPageTitle.getText();
    }

    public List<WebElement> getErrorMessages() {
        return errorMessages;
    }

    public Result enterCreditCardNumber(String ccNumber) {
        WaitVisible(creditCardNumber);
        return enterText(creditCardNumber, ccNumber, "Card number input");
    }

    public Result selectCardExpiryMonth(String expMonth) {
        WaitVisible(paymentCardExpiryMonth);
        return selectItemFromDropdownByContainsText(paymentCardExpiryMonth, expMonth, "Expiry card month dropdown");
    }

    public Result selectCardExpiryYear(String expYear) {
        WaitVisible(paymentCardExpiryYear);
        return selectItemFromDropdownByContainsText(paymentCardExpiryYear, expYear, "Expiry card year dropdown");
    }

    public Result enterCardAddress(String address) {
        WaitVisible(paymentCardAddressOne);
        return enterText(paymentCardAddressOne, address, "Address input");
    }

    public Result enterCardTown(String town) {
        WaitVisible(paymentCardTown);
        return enterText(paymentCardTown, town, "Card town");
    }

    public Result selectCardCounty(String county) {
        WaitVisible(paymentCardAddCounty);
        return selectItemFromDropdownByContainsText(paymentCardAddCounty, county, "Country dropdown");
    }
    
    public Result selectCardCountyCoporatePermit(String county) {
        WaitVisible(paymentCardAddCountyNew);
        return selectItemFromDropdownByContainsText(paymentCardAddCountyNew, county, "Country dropdown");
    }

    public Result enterCardPostcode(String postcode) {
        WaitVisible(paymentCardPostcode);
        return enterText(paymentCardPostcode, postcode, "Postcode input");
    }
    
    public Result enterCardPostcodeCorporatePermit(String postcode) {
        WaitVisible(paymentCardPostcodeNew);
        return enterText(paymentCardPostcodeNew, postcode, "Postcode input");
    }
    
    public Result clickAddPaymentCard() {
        WaitVisible(addPaymentCardButton);
        return ClickControl(addPaymentCardButton, "add PaymentCard Button button");
    }
    
    public Result clickAddPaymentCardNextButton() {
        WaitVisible(paymentCardSaveNextButton);
        return ClickControl(paymentCardSaveNextButton, "add PaymentCard Button button");
    }

    public Result clickSave() {
        WaitVisible(paymentCardSaveButton);
        return ClickControl(paymentCardSaveButton, "Payment card save button");
    }

    public Result clickCancel() {
        WaitVisible(paymentCardCancelButton);
        return ClickControl(paymentCardCancelButton, "Cancel button");
    }
}
