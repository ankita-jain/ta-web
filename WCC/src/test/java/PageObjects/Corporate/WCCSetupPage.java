package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCSetupPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//a[@href='/employees']")
    @CacheLookup
    private WebElement employees;
    @FindBy(xpath = "//a[@href='/assets']")
    @CacheLookup
    private WebElement assets;
    @FindBy(xpath = "//a[@href='/corppermissions']")
    @CacheLookup
    private WebElement permissions;
    @FindBy(xpath = "//a[@href='/corppermits']")
    @CacheLookup
    private WebElement permits;
    @FindBy(xpath = "//a[@href='/costcentres']")
    @CacheLookup
    private WebElement costCentres;
    @FindBy(xpath = "//a[@href='/alerts']")
    @CacheLookup
    private WebElement alerts;
    @FindBy(xpath = "//a[@href='/adminusers']")
    @CacheLookup
    private WebElement administrator;
    @FindBy(xpath = "//a[@href='/settings']")
    @CacheLookup
    private WebElement settings;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement setupPageTitle;
    @FindBy(xpath = "//*[@class='success-notification']")
    @CacheLookup
    private WebElement successNotification;

    public WCCSetupPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getSetUpPageTitle() {
        WaitVisible(setupPageTitle);
        return setupPageTitle.getText();
    }

    public String getSuccessNotification() {
        WaitVisible(successNotification);
        return successNotification.getText();
    }

    public Result clickSettings() {
        WaitVisible(settings);
        return WebElementMethods.ClickControl(settings, "Settings");
    }

    public Result clickAdministrator() {
        WaitVisible(administrator);
        return WebElementMethods.ClickControl(administrator, "Administrator");
    }

    public Result clickAlerts() {
        WaitVisible(alerts);
        return WebElementMethods.ClickControl(alerts, "Alerts");
    }

    public Result clickCostCentres() {
        WaitVisible(costCentres);
        return WebElementMethods.ClickControl(costCentres, "Cost Centres");
    }

    public Result clickAssets() {
        WaitVisible(assets);
        return WebElementMethods.ClickControl(assets, "Assets");
    }

    public Result clickEmployees() {
        WaitVisible(employees);
        return WebElementMethods.ClickControl(employees, "Employees");
    }
}


