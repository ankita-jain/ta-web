package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateLoginPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1[1]")
    private WebElement corporateLoginPageTitle;
    @FindBy(xpath = "//*[@id='field-Email']")
    private WebElement emailAddress;
    @FindBy(xpath = "//*[@id='field-Password']")
    private WebElement passwordField;
    @FindBy(xpath = "//*[@value='Log In']")
    private WebElement logInButton;

    public WCCCorporateLoginPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCorporateLoginPageTitle() {
        WaitVisible(corporateLoginPageTitle);
        return corporateLoginPageTitle.getText();
    }

    public Result enterEmailAddress(String email) {
        WaitVisible(emailAddress);
        return WebElementMethods.enterText(emailAddress, email, "Email address");
    }

    public Result enterPassword(String password) {
        WaitVisible(passwordField);
        return WebElementMethods.enterText(passwordField, password, "Password");
    }

    public Result clickLoginInButton() {
        WaitVisible(logInButton);
        return WebElementMethods.ClickControl(logInButton, "Login button");
    }
}
