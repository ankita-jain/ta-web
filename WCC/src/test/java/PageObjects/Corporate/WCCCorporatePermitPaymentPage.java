package PageObjects.Corporate;

import PageObjects.PageClass;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporatePermitPaymentPage extends PageClass {

    @FindBy(xpath = "//input[@name='cardId'][1]")
    private WebElement existingPaymentCard;
    
    String existingPaymentCardxpath = "//*[contains(text(),'Visa ending xxxx')]//preceding::input[1]";
    @FindBy(xpath = "//*[contains(text(),'Visa ending 3127')]//preceding::input[1]")
    private WebElement existingPaymentCardEnding;

    @FindBy(xpath = "//div[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@value='corporate']")
    @CacheLookup
    private WebElement coporateFundRadioButton;
    @FindBy(xpath = "//*[@id='selectExistingCard']")
    @CacheLookup
    private WebElement existingCardRadioButton;
    @FindBy(xpath = "//*[@value='card']")
    @CacheLookup
    private WebElement credit_card;
    @FindBy(xpath = "//input[@value='new']")
    @CacheLookup
    private WebElement newCardRadioButton;
    @FindBy(xpath = "//*[@value='Add Payment Card']")
    private WebElement addPaymentCardButton;    
    @FindBy(xpath = "//*[@name='labyrinth_CorpPermitCardSelect_next']")
    private WebElement nextButton;
    @FindBy(xpath = "//*[@name='labyrinth_CorpPermitCardSelect_back']")
    @CacheLookup
    private WebElement previousButtton;
    @FindBy(xpath = "//*[@name='labyrinth_CorpPermitPayment_next']")
    @CacheLookup
    private WebElement payButton;
    @FindBy(xpath = "//*[@name='labyrinth_CorpPermitPayment_back']")
    @CacheLookup
    private WebElement paypreviousButtton;
    @FindBy(xpath = "//*[@id='labcancel']")
    @CacheLookup
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@class='showbreaks']/p[2]")
    @CacheLookup
    private WebElement permitSucessMessage;
    @FindBy(xpath = "//*[@name='labyrinth_finish']")
    @CacheLookup
    private WebElement finishButton;
    @FindBy(xpath = "//*[@id='cardSelect']")
    @CacheLookup
    private WebElement cardSelect;
    @FindBy(xpath = "//*[@class='error-notification']")
    @CacheLookup
    
    private WebElement errorNotification;     
    public WCCCorporatePermitPaymentPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickExistingPaymentCard(String lastNumber) {
        WaitVisible(existingPaymentCard);
        return ClickControl(existingPaymentCard, "'Click existing card' button");
    }

    public void clickExistingPaymentCardEnding(String ending) {
    	WebElement existingPaymentCardEnd = _driver.findElement(By.xpath(existingPaymentCardxpath.replace("xxxx", ending)));
        WaitVisible(existingPaymentCardEnd);
            jsClick(existingPaymentCardEnd);
    }

    public void clickExistingPaymentCardEnding() {
        WaitVisible(existingPaymentCardEnding);
            jsClick(existingPaymentCardEnding);
    }
    
    public String getRequestPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickPayButton() {
    	WaitVisible(payButton);
        payButton.click();
    }

    public String getErrorNotification() {
    	WaitVisible(errorNotification);
        return errorNotification.getText();
    }


    public String getPermitSuccessText() {
    	WaitVisible(permitSucessMessage);
        return permitSucessMessage.getText();
    }

    public void selectCorporatePaymentOption() {
    	WaitVisible(coporateFundRadioButton);
        coporateFundRadioButton.click();
    }
        
    public void selectCreditCardPaymentOption() {
    	WaitVisible(credit_card);
        credit_card.click();
    }
    
    public void clickAddPaymentCardButton() {
    	WaitVisible(addPaymentCardButton);
        addPaymentCardButton.click();
    }
    
    public void selectExistingCardPaymentOption() {
    	WaitVisible(existingCardRadioButton);
        existingCardRadioButton.click();
    }

    public void selectNewCardPaymentOption() {
    	WaitVisible(newCardRadioButton);
        newCardRadioButton.click();
    }

    public void clickNextButton() {
    	WaitVisible(nextButton);
        nextButton.click();
    }

    public void clickFinishButton() {
    	WaitVisible(finishButton);
        finishButton.click();
    }

    public void clickPreviousButton() {
    	WaitVisible(previousButtton);
        previousButtton.click();
    }

    public void payPermitViaCard(String carddetails) {
    	selectCreditCardPaymentOption();
    	clickExistingPaymentCardEnding(carddetails.substring(carddetails.length()-4));
        clickNextButton();
        
    }

    public void payPermitViaNewCard() {
    	
    	selectCreditCardPaymentOption();
        selectNewCardPaymentOption();
        clickNextButton();
        clickAddPaymentCardButton();
      
    }
}
