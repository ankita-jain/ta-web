package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateDocumentationPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(linkText = "here")
    @CacheLookup
    private WebElement onlineterms;

    public WCCCorporateDocumentationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCorporateDocumentationPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickOnlineTerms() {
        WaitVisible(onlineterms);
        onlineterms.click();
    }


}
