package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static SeleniumHelpers.WebElementMethods.*;


public class WCCCorporateAutoTopupPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement autoTopUpPageTitle;

    @FindBy(xpath = "//*[@class='elemErrorWrap']/span")
    private WebElement autoTopUpCurrentStatus;

    @FindBy(xpath = "//*[@id='field-EnableAutoTopup']")
    private WebElement autoTopUpStatusSelector;

    @FindBy(xpath = "//*[@name='submits[submit]']")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement cancelButton;

    @FindBy(xpath = "//*[@id='amount']")
    private WebElement autoTopUpAmount;

    @FindBy(xpath = "//*[@id='notify']")
    private WebElement autoTopUpNotifyAmount;

    @FindBy(xpath = "//*[@class='warning-notification']")
    private WebElement autoTopUpWarningNotification;

    @FindBy(xpath = "//*[@class='success-notification']")
    private WebElement autoTopUpSuccessNotification;

    public WCCCorporateAutoTopupPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getAutoTopupPageTile() {
        WaitVisible(autoTopUpPageTitle);
        return autoTopUpPageTitle.getText();
    }

    public String getautoTopUpSuccessNotification() {
        WaitVisible(autoTopUpSuccessNotification);
        return autoTopUpSuccessNotification.getText();
    }

    public String getautoTopUpWarningNotification() {
        WaitVisible(autoTopUpWarningNotification);
        return autoTopUpWarningNotification.getText();
    }

    public String getAutoTopupCurrentStatus() {
        hardPageRefresh();
        WaitVisible(autoTopUpCurrentStatus);
        return autoTopUpCurrentStatus.getText();
    }

    public String getAutoTopupEnableStatus() {
        hardPageRefresh();
        WaitMethods.Wait(_driver, 5, ExpectedConditions.textToBePresentInElement(autoTopUpCurrentStatus, "Enabled"));
        return autoTopUpCurrentStatus.getText();
    }

    public String getAutoTopupDisableStatus() {
        hardPageRefresh();
        WaitMethods.Wait(_driver, 5, ExpectedConditions.textToBePresentInElement(autoTopUpCurrentStatus, "Disabled"));
        return autoTopUpCurrentStatus.getText();
    }

    public Result selectAutoTopupStatus(String status) {
        WaitVisible(autoTopUpStatusSelector);
        return selectItemFromDropdownByTextValue(autoTopUpStatusSelector, status, "Select status");
    }

    public String getAutoTopupAmount() {
        WaitVisible(autoTopUpAmount);
        return autoTopUpAmount.getText();
    }

    public boolean isAutoTopupAmountDisplay() {
        return autoTopUpAmount.isDisplayed();
    }

    public boolean isAutoTopupNotifyAmountDisplay() {
        return autoTopUpNotifyAmount.isDisplayed();
    }

    public Structures.Result enterAutoTopUpAmount(String topupamount) {
        WaitVisible(autoTopUpAmount);
        return enterText(autoTopUpAmount, topupamount, "Auto top up amount");
    }

    public Result enterAutoTopUpNotifyAmount(String topupnotifyamount) {
        WaitVisible(autoTopUpNotifyAmount);
        return enterText(autoTopUpNotifyAmount, topupnotifyamount, "Auto top up motify amount");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button");
    }

    public Result clickCancelButton() {
        WaitVisible(cancelButton);
        return ClickControl(cancelButton, "Cancel button");
    }
}
