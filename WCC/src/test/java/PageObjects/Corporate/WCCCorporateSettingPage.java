package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateSettingPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement settingPageTitle;
    @FindBy(xpath = "//*[@id='AllowEmpParking']")
    @CacheLookup
    private WebElement allow_Employees_to_park_themselves_Checkbox;
    @FindBy(xpath = "//*[@value='Save']")
    @CacheLookup
    private WebElement saveButton;
    @FindBy(xpath = "//ul[@class='success-notification']/p")
    @CacheLookup
    private WebElement saveSuccessNotification;

    public WCCCorporateSettingPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getSettingPageTitle() {
        WaitVisible(settingPageTitle);
        return settingPageTitle.getText();
    }

    public void checkAllowEmployeesToPark() {
        WaitVisible(allow_Employees_to_park_themselves_Checkbox);

        if (!allow_Employees_to_park_themselves_Checkbox.isSelected()) {
            allow_Employees_to_park_themselves_Checkbox.click();
        }
    }

    public void clickSaveButton() {
        WaitVisible(saveButton);
        saveButton.click();
    }

    public String getSaveSuccessMessage() {
        //WaitVisible(saveSuccessNotification);
        return saveSuccessNotification.getText();
    }

    public void allowEmployeeToParkThemselves() {
        checkAllowEmployeesToPark();
        clickSaveButton();
    }
}
