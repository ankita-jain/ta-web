package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateParkEmployeeSelectZonePage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='zone']")
    @CacheLookup
    private WebElement zone;
    @FindBy(xpath = "//*[@name='labyrinth_SelectZone_next']")
    @CacheLookup
    private WebElement nextButton;

    public WCCCorporateParkEmployeeSelectZonePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkEmployeePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterZone(String zoneId) {
        WaitVisible(zone);
        zone.clear();
        zone.sendKeys(zoneId);
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void enterZoneAndClickNext(String zoneId) {
        enterZone(zoneId);
        clickNextButton();
    }
}
