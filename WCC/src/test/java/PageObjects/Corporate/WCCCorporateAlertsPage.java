package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateAlertsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement corporateAlertPageTitle;
    @FindBy(xpath = "//*[@id='field-notify']")
    @CacheLookup
    private WebElement alertBalance;
    @FindBy(xpath = "//*[@id='field-EmailFrequency']")
    @CacheLookup
    private WebElement emailOption;
    @FindBy(xpath = "//*[@id='field-submit']")
    @CacheLookup
    private WebElement saveAlertButton;
    @FindBy(xpath = "//ul[@class='success-notification']/p")
    @CacheLookup
    private WebElement alertSaveMessage;

    public WCCCorporateAlertsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getAlertSaveMessage() {
        WaitVisible(alertSaveMessage);
        return alertSaveMessage.getText();
    }

    public String getPagetTitle() {
        WaitVisible(corporateAlertPageTitle);
        return corporateAlertPageTitle.getText();
    }

    public void enterMiniumBalanceForAlert(String minBalance) {
        WaitVisible(alertBalance);
        alertBalance.clear();
        alertBalance.sendKeys(minBalance);
    }

    public void selectParkingNotification(String alertsOptions) {
        WaitVisible(emailOption);
        Select alertsSelectOptions = new Select(emailOption);
        alertsSelectOptions.selectByVisibleText(alertsOptions);
    }

    public void clickSave() {
        WaitVisible(saveAlertButton);
        saveAlertButton.click();
    }


}
