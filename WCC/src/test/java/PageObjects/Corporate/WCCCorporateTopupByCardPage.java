package PageObjects.Corporate;

import PageObjects.PageClass;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCCorporateTopupByCardPage extends PageClass {

    @FindBy(xpath = "//div[@class='page-head']//h1")
    private WebElement PageTitle;

    @FindBy(xpath = "//*[@id='Card']/fieldset[1]/ol/li[2]/div/div")
    private WebElement no_valid_card;

    @FindBy(xpath = "//*[contains(text(),'Visa ending')][1]//preceding::input[1]")
    private WebElement existingCard;

    @FindBy(xpath = "//*[contains(text(),'Add a new card')]//preceding::input[1]")
    private WebElement newCard;

    @FindBy(xpath = "//*[@id='field-CcNumber']")
    private WebElement cardNumber;

    @FindBy(xpath = "//*[@id='field-CcExpire']")
    private WebElement cardExpiryMonth;

    @FindBy(xpath = "//*[@name='CcExpire[Y]']")
    private WebElement cardExpiryYear;

    @FindBy(xpath = "//*[@id='field-Address1']")
    private WebElement cardAddress;

    @FindBy(xpath = "//*[@id='field-Town']")
    private WebElement cardAddressTown;

    @FindBy(xpath = "//*[@id='field-County']")
    private WebElement county;

    @FindBy(xpath = "//*[@id='field-Postcode']")
    private WebElement cardPostcode;

    @FindBy(xpath = "//input[@type='cc-csc']")
    private WebElement cv2;

    @FindBy(xpath = "//*[@name='_qf_Card_next']")
    private WebElement nextButton;

    @FindBy(xpath = "//*[@name='_qf_Card_back']")
    private WebElement backButton;

    private String specificCard = "//label[contains(text(),'%s')][1]//preceding::input[1]";

    @FindBy(xpath = "//*[@name='_qf_Finalise_next']")
    private WebElement finalButton;

    @FindBy(xpath = "//*[@class='error']")
    private List<WebElement> errorMessages;

    @FindBy(xpath = " //*[@id='submitPay']")
    private WebElement submitPay;
    
    @FindBy(xpath = "//*[@value='Pay']")
    private WebElement payButton;

    @FindBy(xpath = " //button[@id='paymentConfirmButton']")
    private WebElement payButtonOnPopUpForm;

    public WCCCorporateTopupByCardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCorporateTopupByCardPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public Result clickPay() {
        WaitVisible(submitPay);
        return ClickControl(submitPay, "Pay button");
    }
    
    public Result clickPayButton() {
        WaitVisible(payButton);
        return ClickControl(payButton, "Pay button");
    }

    public Result clickPayOnPopUpForm() {
        WaitVisible(payButtonOnPopUpForm);
        return ClickControl(payButtonOnPopUpForm, "Pay button on pop up form");
    }

    public Result clickExistingCard() {
        WaitVisible(existingCard);
        return ClickControl(existingCard, "Existing card radio button");
    }

    public Result clickNewCard() {
        WaitVisible(newCard);
        return ClickControl(newCard, "New card radio button");
    }

    public Result enterCardNumber(String number) {
        WaitVisible(cardNumber);
        return enterText(cardNumber, number, "Card number input");
    }

    public Result selectExpiryMonth(String month) {
        WaitVisible(cardExpiryMonth);
        return selectItemFromDropdownByTextValue(cardExpiryMonth, month, "Month dropdown");
    }

    public Result selectExpiryYear(String year) {
        WaitVisible(cardExpiryYear);
        return selectItemFromDropdownByTextValue(cardExpiryYear, year, "Year dropdown");
    }

    public Result enterCardAddress(String address) {
        WaitVisible(cardAddress);
        return enterText(cardAddress, address, "Card address input");
    }

    public Result enterCardTown(String town) {
        WaitVisible(cardAddressTown);
        return enterText(cardAddressTown, town, "Town input");
    }

    public Result selectCounty(String countyName) {
        WaitVisible(county);
        return selectItemFromDropdownByTextValue(county, countyName, "Country dropdown");
    }

    public Result enterPostCode(String postcode) {
        WaitVisible(cardPostcode);
        return enterText(cardPostcode, postcode, "Post code input");
    }

    public Result enterCV2(String cv2Value) {
        WaitVisible(cv2);
        return enterText(cv2, cv2Value, "CVV2 input");
    }

    public Result selectCard(String cardNumber, String expMonth, String expYear) {
        String rowWithCardDetails = String.format("Visa ending " + cardNumber + ", exp: %s/%s", expMonth, expYear);
        By specificCardBy = By.xpath(String.format(specificCard, rowWithCardDetails));
        WebElement selectCard = _driver.findElement(specificCardBy);

        return ClickControl(selectCard, "Card row");
    }
    
    public void selectPaymentCard(String cardNumber, String expMonth, String expYear)
    {
    	String rowWithCardDetails = String.format("Visa ending " + cardNumber + ", exp: %s/%s", expMonth, expYear);
        By specificCardBy = By.xpath(String.format(specificCard, rowWithCardDetails));
        WebElement selectCard = _driver.findElement(specificCardBy);
        
        jsClick(selectCard);

    }

    public Result clickNextButton() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }

    public Result clickFinalButton() {
        WaitVisible(finalButton);
        return ClickControl(finalButton, "Final button");
    }

    public List<WebElement> getErrorMessages() {
        return errorMessages;
    }

    //TODO this method always returns true, it seems like behaviour has been changed
    //TODO should refactor it
    public boolean checkErrorMessages(String errorString) {
        String[] arrayErrorMessages = errorString.split("\\-", -1);
        int i = 0;
        boolean flagCheck = true;

        for (WebElement errormsg : errorMessages) {

            if (!errormsg.getText().equals(arrayErrorMessages[i])) {
                flagCheck = false;
                break;
            }
            i++;
        }

        return flagCheck;
    }
}
