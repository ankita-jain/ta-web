package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateOnlineTermsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1[1]")
    private WebElement PageTitle;
    
    @FindBy(xpath="//*[contains(text(),'Privacy')]")
    private WebElement privacy;
    
    @FindBy(xpath="//*[contains(text(),'Archive of Previous Privacy Policies')]")
    private WebElement previous_privacy_policies;

    public WCCCorporateOnlineTermsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getOnlinePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }
    
    public void clickPrivacyLink() {
    	WaitVisible(privacy);
    	privacy.click();
    }
    
    public void clickPrivacyPolicies() {
    	WaitVisible(previous_privacy_policies);
    	previous_privacy_policies.click();
    }
}
