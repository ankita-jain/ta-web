package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCCorporateAddEmployeePage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pageTitle;
    @FindBy(xpath = "//*[@id='field-Title']")
    private WebElement title;
    @FindBy(xpath = "//*[@id='FirstName']")
    private WebElement firstName;
    @FindBy(xpath = "//*[@id='Surname']")
    private WebElement surName;
    @FindBy(xpath = "//*[@id='field-Email']")
    private WebElement email;
    @FindBy(xpath = "//*[@name='SendWelcomeEmail']")
    private WebElement sendWelcomeEmailCheckbox;
    @FindBy(xpath = "//*[@id='new-cli']")
    private WebElement cli;
    @FindBy(xpath = "//*[@id='add-cli']")
    private WebElement addCliButton;
    @FindBy(xpath = "//*[@id='new-vrn']")
    private WebElement vrn;
    @FindBy(xpath = "//*[@id='add-vrn']")
    private WebElement addVrnButton;
    @FindBy(xpath = "//*[@name='CanPark']")
    private WebElement canParkCheckbox;
    @FindBy(xpath = "//*[@id='climsg']")
    private WebElement climsg;
    @FindBy(xpath = "//*[@id='vrnmsg']")
    private WebElement vrnMsg;
    @FindBy(xpath = "//*[@id='save-button']")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement cancelButton;

    public WCCCorporateAddEmployeePage(WebDriver driver) {
        super(driver);
    }

    public String getAddEmployeePageTitle() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public Result selectTitle(Title titleText) {
        WaitVisible(title);
        return selectItemFromDropdownByTextValue(title, titleText.toString(), "Title select");
    }

    public Result enterFirstName(String name) {
        WaitVisible(firstName);
        return enterText(firstName, name, "First name input");
    }

    public Result enterSurname(String secondName) {
        WaitVisible(surName);
        return enterText(surName, secondName, "Second name input");
    }

    public Result enterEmail(String userEmail) {
        WaitVisible(email);
        return enterText(email, userEmail, "Email input");
    }

    public Result tickWelcomeEmail() {
        WaitVisible(sendWelcomeEmailCheckbox);
        return checkCheckBox(sendWelcomeEmailCheckbox, "Welcome email checkbox");
    }

    public Result enterPhoneNumber(String phoneNumber) {
        WaitVisible(cli);
        return enterText(cli, phoneNumber, "CLI input");
    }

    public Result addCli() {
        WaitVisible(addCliButton);
        return ClickControl(addCliButton, "Add cli button");
    }

    public Result enterVehicle(String vehicleNo) {
        WaitVisible(vrn);
        return enterText(vrn, vehicleNo, "Vehicle input");
    }

    public Result addVrn() {
        WaitVisible(addVrnButton);
        return ClickControl(addVrnButton, "Add vrn button");
    }

    public String getVRNMessage() {
        WaitMethods.Wait(_driver, 5, webDriver -> vrnMsg.getText().length() > 1);
        return vrnMsg.getText();
    }

    public String getCLIMessage() {
        WaitMethods.Wait(_driver, 5, webDriver -> climsg.getText().length() > 1);
        return climsg.getText();
    }

    public Result tickCanEmployeePark() {
        WaitVisible(canParkCheckbox);
        return checkCheckBox(canParkCheckbox, "Can park checkbox");
    }

    public Result clickSave() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button");
    }

    public enum Title {
        MR("Mr"),
        MISS("Miss"),
        MRS("Mrs"),
        DR("Dr"),
        LADY("Lady"),
        LORD("Lord"),
        MS("Ms");

        private String value;

        Title(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
