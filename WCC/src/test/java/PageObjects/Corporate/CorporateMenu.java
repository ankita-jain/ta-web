package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static GenericComponents.UtilityClass.getFeatureToggleState;
import static SeleniumHelpers.WebElementMethods.ClickControl;

public class CorporateMenu extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//a[@href='/corporate']")
    private WebElement profile;
    @FindBy(xpath = "//a[@href='/corplogout']")
    private WebElement logout;

    public CorporateMenu(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    private WebElement getAccount() {
        String locator = getFeatureToggleState().equalsIgnoreCase("on") && !_driver.getCurrentUrl().contains("westminster")
                ? "//*[@id='account-link']" : "//*[@id='toggleAccountMenu']";
        return _driver.findElement(By.xpath(locator));
    }

    public Result clickAccount() {
        WaitVisible(getAccount());
        if (getFeatureToggleState().equalsIgnoreCase("on") && !_driver.getCurrentUrl().contains("westminster")) {
            Actions builder = new Actions(_driver);
            builder.moveToElement(getAccount()).perform();
            return Result.Create(true, "Account menu is open just by hover");
        }
        return ClickControl(getAccount(), "Account link");
    }

    public Result clickProfile() {
        WaitVisible(profile);
        return ClickControl(profile, "Profile link");
    }

    public Result clickLogout() {
        WaitVisible(logout);
        return ClickControl(logout, "Logout link");
    }
}
