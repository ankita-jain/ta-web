package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateSuspensionPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='sp-head']/h1")
    @CacheLookup
    private WebElement suspension_page_title;


    public WCCCorporateSuspensionPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);    // TODO Auto-generated constructor stub
    }

    public String getSuspensionPageTitle() {
        WaitVisible(suspension_page_title);
        return suspension_page_title.getText();
    }

}
