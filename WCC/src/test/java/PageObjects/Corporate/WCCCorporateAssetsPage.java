package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class WCCCorporateAssetsPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    private WebElement pageTitle;
    @FindBy(xpath = "//*[@id='add-cli']")
    private WebElement cli;
    @FindBy(xpath = "//*[@id='add-vrn']")
    private WebElement vrn;
    @FindBy(xpath = "//*[@id='add-cli-button']")
    private WebElement cliAddButton;
    @FindBy(xpath = "//*[@id='add-vrn-button']")
    private WebElement vrnAddButton;
    @FindBy(xpath = "//*[@id='updateButton']")
    private WebElement updateButton;
    @FindBy(xpath = "//*[@id='cancelButton']")
    private WebElement cancelButton;
    @FindBy(xpath = "//a[@href='/setup']")
    private WebElement setup;

    public WCCCorporateAssetsPage(WebDriver driver) {
        super(driver);
    }

    public String getAssetsPageTitle() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public Result enterCli(String cliValue) {
        WaitVisible(cli);
        return enterText(cli, cliValue, "CLI input");
    }

    public Result enterVRN(String vrnValue) {
        WaitVisible(vrn);
        return enterText(vrn, vrnValue, "VRN input");
    }

    public Result addCli() {
        WaitVisible(cliAddButton);
        return ClickControl(cliAddButton, "CLI add button");
    }

    public Result addVrn() {
        WaitVisible(vrnAddButton);
        return ClickControl(vrnAddButton, "Add vrn button");
    }

    public Result save() {
        WaitVisible(updateButton);
        return ClickControl(updateButton, "Save button");
    }
    
    public Result clickSetup() {
        WaitVisible(setup);
        return ClickControl(setup, "setup link click");
    }

    public Result cancel() {
        WaitVisible(cancelButton);
        return ClickControl(cancelButton, "");
    }
}
