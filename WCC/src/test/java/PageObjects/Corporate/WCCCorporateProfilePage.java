package PageObjects.Corporate;

import GenericComponents.UtilityClass;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class WCCCorporateProfilePage extends Base_Page {
    @FindBy(xpath = "//div[@class='page-head']//h1")
    private WebElement corporateProfilePageTitle;
    @FindBy(xpath = "//*[@class='breadcrumbs']/a[2]")
    private WebElement corporateHomeBreadCrumbs;
    @FindBy(xpath = "//a[@href='/funds']")
    private WebElement top_up;
    @FindBy(xpath = "//a[@href='/alerts']")
    private WebElement change;
    @FindBy(xpath = "//*[@class='box-content']/div[1]/ul/li[1]/p")
    private WebElement current_fund;
    @FindBy(xpath = "//a[@href='/contact']")
    private WebElement contact_us;
    @FindBy(xpath = "//a[@href='funds']")
    private WebElement top_up_in_functions;
    @FindBy(xpath = "//a[@href='employees']")
    private WebElement edit_employees;
    @FindBy(xpath = "//a[@href='callus']")
    private WebElement request_a_call_back;
    @FindBy(xpath = "//a[@href='employeepin']")
    private WebElement send_employee_pins;
    @FindBy(xpath = "//a[@href='/creditaccount']")
    private WebElement view_credit_facilities;
    @FindBy(xpath = "//a[@href='/costcentres']")
    private WebElement cost_centres;
    @FindBy(xpath = "//a[@href='/costcentres/add']")
    private WebElement add_cost_centres;
    @FindBy(xpath = "//a[@href='/adminusers']")
    private WebElement administrators;
    @FindBy(xpath = "//a[@href='/adminusers/add']")
    private WebElement add_administrators;
    @FindBy(xpath = "//a[@href='/account']")
    private WebElement edit_company_information;
    @FindBy(xpath = "//*[@class='page-head']/a")
    private WebElement back_to_perivious_page;
    @FindBy(xpath = "//*[@class='success-notification']")
    private WebElement success_notifcation;
    @FindBy(xpath = "//a[@href='/corporate']")
    private WebElement coporate_profile_page;
    @FindBy(xpath = "//a[@href='/invoices']")
    private WebElement coporate_account;

    public WCCCorporateProfilePage(WebDriver driver) {
        super(driver);
    }

    public String getSuccessNotification() {
        WaitVisible(success_notifcation);
        return success_notifcation.getText();
    }

    public Result clickGoBackToPreviousPage() {
        WaitVisible(back_to_perivious_page);
        return WebElementMethods.ClickControl(back_to_perivious_page, "Back to previous page");
    }
    
    public void clickCorporateAccount()
    {
    	 WaitVisible(coporate_account);
    	 WebElementMethods.ClickControl(coporate_account, "Corporate account page");
    }
    
    public Result clickGoToProfilePage() {
    	clickCorporateAccount();
        WaitVisible(coporate_profile_page);
        return WebElementMethods.ClickControl(coporate_profile_page, "Go to corporate profile page");
    }

    public String getCurrentFundBalance() {
        _driver.navigate().refresh();
        WaitVisible(current_fund);
        return UtilityClass.getCurrentBalanceFromText(current_fund.getText());
    }

    public Result clickChange() {
        WaitVisible(change);
        return WebElementMethods.ClickControl(change, "Change link");
    }

    public Result clickTopup() {
        WaitVisible(top_up);
        return WebElementMethods.ClickControl(top_up, "Top up link");
    }

    public Result clickContactUs() {
        WaitVisible(contact_us);
        return WebElementMethods.ClickControl(contact_us, "Contact Us");
    }

    public Result clickTopupInFuntions() {
        WaitVisible(top_up_in_functions);
        return WebElementMethods.ClickControl(top_up_in_functions, "Top Up in Functions");
    }

    public Result clickEditEmployees() {
        WaitVisible(edit_employees);
        return WebElementMethods.ClickControl(edit_employees, "Edit Employees");
    }

    public Result clickRequestCallBack() {
        WaitVisible(request_a_call_back);
        return WebElementMethods.ClickControl(request_a_call_back, "Request a call back");
    }

    public Result clickSendEmployeesPIN() {
        WaitVisible(send_employee_pins);
        return WebElementMethods.ClickControl(send_employee_pins, "Send Employees PIN");
    }

    public Result clickViewCreditFacilities() {
        WaitVisible(view_credit_facilities);
        return WebElementMethods.ClickControl(view_credit_facilities, "View credit facilities");
    }

    public Result clickCostCentres() {
        WaitVisible(cost_centres);
        return WebElementMethods.ClickControl(cost_centres, "Cost Centres");
    }

    public Result clickCostCentresAdd() {
        WaitVisible(add_cost_centres);
        return WebElementMethods.ClickControl(add_cost_centres, "Add Cost Centres");
    }

    public Result clickAdministrators() {
        WaitVisible(administrators);
        return WebElementMethods.ClickControl(administrators, "Administrators");
    }

    public Result clickAdministratorsAdd() {
        WaitVisible(add_administrators);
        return WebElementMethods.ClickControl(add_administrators, "Add Administrators");
    }

    public Result clickEditCompanyInformation() {
        WaitVisible(edit_company_information);
        return WebElementMethods.ClickControl(edit_company_information, "Edit Company information");
    }

    public String getCorporateProfilePageTitle() {
        WaitVisible(corporateProfilePageTitle);
        return corporateProfilePageTitle.getText();
    }

    public Result clickCoporateHome() {
        WaitVisible(corporateHomeBreadCrumbs);
        return WebElementMethods.ClickControl(corporateHomeBreadCrumbs, "Corporate Home");
    }

}
