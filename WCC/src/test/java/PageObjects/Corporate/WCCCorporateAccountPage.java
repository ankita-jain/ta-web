package PageObjects.Corporate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import PageObjects.PageClass;

public class WCCCorporateAccountPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//a[@href='/corporate']/h3")
    @CacheLookup
    private WebElement myAccount;
    @FindBy(xpath = "//a[@href='/documentation']/h3")
    @CacheLookup
    private WebElement documentation;
    @FindBy(xpath = "//a[@href='/password']/h3")
    @CacheLookup
    private WebElement security_options;

    public WCCCorporateAccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getAccountPageTitle() {
    	WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickMyAccount() {
        WaitVisible(myAccount);
        WaitClickable(myAccount);
        myAccount.click();
    }

    public void clickDocumentation() {
    	WaitVisible(documentation);
        WaitClickable(documentation);
        documentation.click();
    }

    public void clickSecurityOptions() {
    	WaitVisible(security_options);
        WaitClickable(security_options);
        security_options.click();
    }
}
