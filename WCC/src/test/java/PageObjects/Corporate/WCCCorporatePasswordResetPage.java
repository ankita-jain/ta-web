package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporatePasswordResetPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='field-oldpassword']")
    @CacheLookup
    private WebElement oldPassword;
    @FindBy(xpath = "//*[@id='field-newpassword']")
    @CacheLookup
    private WebElement newPassword;
    @FindBy(xpath = "//*[@id='field-verifypassword']")
    @CacheLookup
    private WebElement verifyPassword;
    @FindBy(xpath = "//*[@value='Submit']")
    @CacheLookup
    private WebElement submitButton;
    @FindBy(xpath = "//div[@class='bottom_grey_curve']/p")
    @CacheLookup
    private WebElement passwordChangeMessage;

    public WCCCorporatePasswordResetPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPasswordResetPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterOldPassword(String oldpassword) {
        WaitVisible(oldPassword);
        oldPassword.clear();
        oldPassword.sendKeys(oldpassword);
    }

    public void enterNewPassword(String newpassword) {
        WaitVisible(newPassword);
        newPassword.clear();
        newPassword.sendKeys(newpassword);

    }

    public void enterVerifyPassword(String verifypassword) {
        WaitVisible(verifyPassword);
        verifyPassword.clear();
        verifyPassword.sendKeys(verifypassword);

    }

    public void clickSubmitButton() {
        WaitVisible(submitButton);
        submitButton.click();
    }

    public String getPasswordChangeMessage() {
        WaitVisible(passwordChangeMessage);
        return passwordChangeMessage.getText();
    }


    public void resetPassword(String oldpassword, String newpassword, String verifypassword) {
        enterOldPassword(oldpassword);
        enterNewPassword(newpassword);
        enterVerifyPassword(verifypassword);
        clickSubmitButton();
    }
}
