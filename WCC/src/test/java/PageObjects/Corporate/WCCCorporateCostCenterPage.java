package PageObjects.Corporate;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporateCostCenterPage extends Base_Page {

    @FindBy(xpath = "//a[@href='/costcentres/add']")
    private WebElement addNewCostCenterButton;

    @FindBy(xpath = "//div[@class='page-head']//h1")
    private WebElement pageTitle;

    @FindBy(xpath = "//ul[@class='success-notification']")
    private WebElement costCenterPageSuccessNotification;

    @FindBy(xpath = "//table[@id='dgridtable1']")
    private WebElement htmlTable;

    @FindBy(xpath = "//ul[@class='notice-notification']")
    private WebElement noticeNotification;

    private Table table;

    public WCCCorporateCostCenterPage(WebDriver driver) {
        super(driver);
        table = new Table(htmlTable);
    }

    public String getCostCentreSuccessNotification() {
        WaitVisible(costCenterPageSuccessNotification);
        return costCenterPageSuccessNotification.getText();
    }

    public String getNoticeNotification() {
        WaitVisible(noticeNotification);
        return noticeNotification.getText();
    }

    public String getCostCenterPageTitle() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public Result addCostCentre() {
        WaitVisible(addNewCostCenterButton);
        return ClickControl(addNewCostCenterButton, "Add new Cost center button");
    }

    public Result deleteCostCenter(String costCenterName) {
        WaitVisible(htmlTable);
        WebElement cellElement = table.getCell(table.getRowIndexByCellValue(costCenterName, CostCenterTable.NAME), CostCenterTable.DELETE.value());
        return ClickControl(cellElement, "Delete button");
    }

    public Result editCostCenter(String costCenterName) {
        WaitVisible(htmlTable);
        WebElement cellElement = table.getCell(table.getRowIndexByCellValue(costCenterName, CostCenterTable.NAME), CostCenterTable.EDIT.value());
        return ClickControl(cellElement, "Edit button");
    }

    public enum CostCenterTable {
        NAME("Name", 1),
        NUMBER("Cost Centre No", 2),
        OWNER("Cost Centre Owner", 3),
        EDIT("", 4),
        DELETE("", 5);

        private int index;
        private String title;

        CostCenterTable(String title, int index) {
            this.title = title;
            this.index = index;
        }

        @Override
        public String toString() {
            return title;
        }

        public int value() {
            return index;
        }
    }
}
