package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateVATInvoicePage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='startdate']")
    @CacheLookup
    private WebElement startDate;
    @FindBy(xpath = "//*[@name='enddate']")
    @CacheLookup
    private WebElement endDate;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement searchButton;

    public WCCCorporateVATInvoicePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickSearchButton() {
        WaitVisible(searchButton);
        searchButton.click();
    }

    public void enterStartDate(String startdate) {
        WaitVisible(startDate);
        startDate.clear();
        startDate.sendKeys(startdate);
    }

    public void enterEndDate(String enddate) {
        WaitVisible(endDate);
        endDate.clear();
        endDate.sendKeys(enddate);
    }

    public String getVATInvoicesPageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void searchVATInvoice(String startdate, String enddate) {
        enterStartDate(startdate);
        enterEndDate(enddate);
        clickSearchButton();
    }

}
