package PageObjects.Corporate;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WccCorporateAdministrator extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    private WebElement corporateAdminPageTitle;
    @FindBy(xpath = "//a[@href='/adminusers/add']")
    private WebElement addAdminButton;
    @FindBy(xpath = "//ul[@class='success-notification']")
    private WebElement successNotification;
    @FindBy(xpath = "//ul[@class='notice-notification']/p")
    private WebElement noticeNotification;
    @FindBy(xpath = "//*[@id='dgridtable1']")
    private WebElement table;
    private Table administratorsTable = new Table(table);

    public WccCorporateAdministrator(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCorportaeAdministratorPageTitle() {
        WaitVisible(corporateAdminPageTitle);
        return corporateAdminPageTitle.getText();
    }

    public Result clickAddAdminButton() {
        WaitVisible(addAdminButton);
        return WebElementMethods.ClickControl(addAdminButton, "Add Admin button");
    }

    public boolean isAdminButtonVisible() {
        return addAdminButton.isDisplayed();
    }

    public String getSuccessMessage() {
        WaitVisible(successNotification);
        return successNotification.getText();
    }

    public String getNoticeMessage(){
        WaitVisible(noticeNotification);
        return noticeNotification.getText();
    }

    public Result clickAdminDelete(String adminEmail) {
        WebElement adminRow = administratorsTable.getRowByCellValue(adminEmail, AdministratorsTableColumn.EMAIL);
        WebElement deleteButton = adminRow.findElement(By.xpath(".//a[contains(@href,'/delete')]"));
        return WebElementMethods.ClickControl(deleteButton, "Delete button");
    }

    public Result clickAdminEdit(String adminEmail) {
        WebElement adminRow = administratorsTable.getRowByCellValue(adminEmail, AdministratorsTableColumn.EMAIL);
        WebElement editButton = adminRow.findElement(By.xpath(".//a[contains(@href,'edit')]"));
        return WebElementMethods.ClickControl(editButton, "Edit button");
    }

    public enum AdministratorsTableColumn {
        EMAIL("Email"),
        USER_TYPE("User Type"),
        COST_CENTRE_ACCESS_RIGHTS("Cost Centre Access Rights");
        String headerValue;

        AdministratorsTableColumn(String header) {
            headerValue = header;
        }

        @Override
        public String toString() {
            return headerValue;
        }
    }
}
