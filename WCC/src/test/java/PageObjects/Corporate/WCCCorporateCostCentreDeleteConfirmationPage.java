package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporateCostCentreDeleteConfirmationPage extends Base_Page {

    @FindBy(xpath = "//div[@class='page-head']//h1[1]")
    private WebElement costcenterDeletionPageTitle;
    @FindBy(xpath = "//*[@value='Yes']")
    private WebElement costcenterDeletionConfirmationButton;
    @FindBy(xpath = "//*[@value='No']")
    private WebElement costcenterDeletionCancelButton;

    public WCCCorporateCostCentreDeleteConfirmationPage(WebDriver driver) {
        super(driver);
    }

    public String getCostCentreDeletionConfirmationPageTitle() {
        WaitVisible(costcenterDeletionPageTitle);
        return costcenterDeletionPageTitle.getText();
    }

    public Result clickYes() {
        WaitVisible(costcenterDeletionConfirmationButton);
        return ClickControl(costcenterDeletionConfirmationButton, "Yes button");
    }

    public Result clickNo() {
        WaitVisible(costcenterDeletionCancelButton);
        return ClickControl(costcenterDeletionCancelButton, "No button");

    }


}
