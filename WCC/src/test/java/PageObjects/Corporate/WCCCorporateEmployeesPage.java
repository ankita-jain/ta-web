package PageObjects.Corporate;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import Structures.Result;

public class WCCCorporateEmployeesPage extends Base_Page {

    private String employeeEditButton = "//*[contains(text(), '%s')]/parent::td/following::td[8]";
    private String employeeDeleteButton = "//*[contains(text(), '%s')]/parent::td/following::td[9]";
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement corporateEmployeesPageTitle;
    @FindBy(xpath = "//*[@class='withmargins']/a[1]")
    private WebElement addANewEmployee;
    @FindBy(xpath = "//*[@class='withmargins']/a[2]")
    private WebElement addMultipleEmployee;
    @FindBy(xpath = "//*[@class='success-notification']")
    private WebElement successNotification;
    @FindBy(xpath = "//*[@id='field-id']")
    private WebElement costCenterFilter;
    @FindBy(xpath = "//*[@id='field-vrm']")
    private WebElement vehicleFilter;
    @FindBy(xpath = "//*[@id='field-name']")
    private WebElement nameFilter;
    @FindBy(xpath = "//*[@id='field-canPark']")
    private WebElement canParkFilter;
    @FindBy(xpath = "//*[@id='field-submit']")
    private WebElement searchButton;
    @FindBy(xpath = "//*[@id='dgridtable1']")
    private WebElement searchResultTable;
    private Table employeesTable = new Table(searchResultTable);

    public WCCCorporateEmployeesPage(WebDriver driver) {
        super(driver);
    }

    public String getEmployeePageTitle() {
        WaitVisible(corporateEmployeesPageTitle);
        return corporateEmployeesPageTitle.getText();
    }

    public Result addNewEmployee() {
        WaitVisible(addANewEmployee);
        return ClickControl(addANewEmployee, "Add new employee button");
    }

    public String getSuccessNotification() {
        WaitVisible(successNotification);
        return successNotification.getText();
    }

    public Result selectCostCentre(String costCentre) {
        WaitVisible(costCenterFilter);
        return selectItemFromDropdownByTextValue(costCenterFilter, costCentre, "Cost center select");
    }

    public Result selectVehicleFilter(String vrm) {
        WaitVisible(vehicleFilter);
        return selectItemFromDropdownByTextValue(vehicleFilter, vrm, "Vehicle select");
    }

    public Result enterName(String name) {
        WaitVisible(nameFilter);
        return enterText(nameFilter, name, "Name input");
    }

    public Result canParkFilter(String canPark) {
        WaitVisible(canParkFilter);
        return selectItemFromDropdownByTextValue(canParkFilter, canPark, "Park select");
    }

    public Result search() {
        WaitVisible(searchButton);
        return ClickControl(searchButton, "Search button");
    }

    public Result editEmployee(String employeeName) {
        WebElement employeeEdit = _driver.findElement(By.xpath(String.format(employeeEditButton, employeeName)));
        return ClickControl(employeeEdit, "Edit employee button");
    }

    public Result clickEmployeeDeleteButton(String employeeName) {
        WebElement employeeDelete = _driver.findElement(By.xpath(String.format(employeeDeleteButton, employeeName)));
        return ClickControl(employeeDelete, "Delete employee button");
    }

    public boolean isDataTableVisible() {
        return WaitVisible(searchResultTable);
    }

    public enum EmployeesTableColumn {
        FIRST_NAME("First Name"),
        SURNAME("Surname");
        String headerValue;

        EmployeesTableColumn(String header) {
            headerValue = header;
        }

        @Override
        public String toString() {
            return headerValue;
        }
    }
}
