package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateParkEmployeeSelectTariffPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@id='field-tariff']")
    @CacheLookup
    private WebElement tariff;
    @FindBy(xpath = "//*[@name='labyrinth_SelectTariff_next']")
    @CacheLookup
    private WebElement nextButton;

    public WCCCorporateParkEmployeeSelectTariffPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkEmployeePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterTariff(String duration) {
        WaitVisible(tariff);
        new Select(tariff).selectByVisibleText(duration);
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void selectTariffDurationAndClickNext(String duration) {
        enterTariff(duration);
        clickNextButton();
    }
}
