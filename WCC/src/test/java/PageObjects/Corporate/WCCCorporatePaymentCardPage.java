package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporatePaymentCardPage extends Base_Page {

    private String paymentCardExpDate = "//tr//p[contains(text(), '%s')]//following::td[1]";
    private String paymentCardDeleteButtonPath = "//tr//p[contains(text(), '%s')]//following::td[2]";
    private String paymentCardType = "//tr//p[contains(text(), '%s')]//preceding::td[1]";
    private String paymentCardNumber = "//tr//p[contains(text(), '%s')]";
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement paymentCardPageTitle;
    @FindBy(xpath = "//a[@href='/payment/add']")
    private WebElement addNewCardButton;
    @FindBy(xpath = "//*[@id='buttonCardAdd']")
    private WebElement addPaymentCardButton;
    @FindBy(xpath = "//*[@class='success-notification']/p")
    private WebElement successNotfication;
    @FindBy(xpath = "//*[@class='notice-notification']")
    private WebElement noticeNotfication;

    public WCCCorporatePaymentCardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    

    public Result clickPaymentCardDelete(String lastNumber) {
        By paymentDeleteButton = By.xpath(String.format(paymentCardDeleteButtonPath, lastNumber));
        return ClickControl(_driver.findElement(paymentDeleteButton), "'Delete card' button");
    }

    public String getCardNumber(String lastNumber) {
        By paymentCardNumberText = By.xpath(String.format(paymentCardNumber, lastNumber));
        return _driver.findElement(paymentCardNumberText).getText();
    }

    public String getCardExpDate(String lastNumber) {
        By paymentCardExpiryDate = By.xpath(String.format(paymentCardExpDate, lastNumber));
        return _driver.findElement(paymentCardExpiryDate).getText();
    }

    public String getCardType(String lastNumber) {
        By cardType = By.xpath(String.format(paymentCardType, lastNumber));
        return _driver.findElement(cardType).getText();
    }

    public String getSuccessNotification() {
        WaitVisible(successNotfication);
        return successNotfication.getText();
    }

    public String getNoticeNotification() {
        WaitVisible(noticeNotfication);
        return noticeNotfication.getText();
    }

    public String getPaymentCardTitle() {
        WaitVisible(paymentCardPageTitle);
        return paymentCardPageTitle.getText();
    }

    public Result clickAddNewCardButton() {
        WaitVisible(addNewCardButton);
        return ClickControl(addNewCardButton, "'Add new card' number");
    }
    
    public Result clickAddPaymentCardButton() {
        WaitVisible(addPaymentCardButton);
        return ClickControl(addPaymentCardButton, "'Add new payment card' number");
    }

    public boolean isAddNewCardButtonDisplayed() {
        return WaitVisible(addNewCardButton);
    }

    public By getCardNumberAsLocator(String lastNumber) {
        return By.xpath(String.format(paymentCardNumber, lastNumber));
    }
}
