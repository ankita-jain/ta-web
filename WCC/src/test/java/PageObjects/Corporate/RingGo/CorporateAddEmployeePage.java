package PageObjects.Corporate.RingGo;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class CorporateAddEmployeePage extends CorporateAddEditEmployeePage {
    @FindBy(xpath = "//*[contains(@for, 'send-email') and text()='Yes']")
    private WebElement yesWelcomeEmailButton;
    @FindBy(xpath = "//*[contains(@for, 'send-email') and text()='No']")
    private WebElement noWelcomeEmailButton;

    public CorporateAddEmployeePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickOnNoWelcomeEmail() {
        WaitVisible(noWelcomeEmailButton);
        return ClickControl(noWelcomeEmailButton, "No Welcome Email");
    }

    public Result clickOnYesWelcomeEmail() {
        return ClickControl(yesWelcomeEmailButton, "Yes Welcome Email");
    }
}
