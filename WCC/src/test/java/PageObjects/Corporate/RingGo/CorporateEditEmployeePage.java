package PageObjects.Corporate.RingGo;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class CorporateEditEmployeePage extends CorporateAddEditEmployeePage {

    @FindBy(id = "resend-button")
    private WebElement welcomeEmailButton;

    public CorporateEditEmployeePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickOnSendWelcomeEmail() {
        return ClickControl(welcomeEmailButton, "Send/Resend button");
    }
}

