package PageObjects.Corporate.RingGo.modules;

import Structures.Result;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class KebabMenu {
    private WebDriver _driver;

    private static final String EDIT_ITEM_XPATH_PATTERN = "//a[contains(@href, '/employees/edit') and contains(text(), '%s')]";

    public KebabMenu(WebDriver driver) {
        this._driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Result clickOnEditItem(String employeeName) {
        return ClickControl(_driver.findElement(By.xpath(String.format(EDIT_ITEM_XPATH_PATTERN, employeeName))), "Edit Employee Button");
    }
}
