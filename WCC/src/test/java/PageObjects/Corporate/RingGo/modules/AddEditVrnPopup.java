package PageObjects.Corporate.RingGo.modules;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Random;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

public class AddEditVrnPopup extends Base_Page {
    @FindBy(id = "field-vrn")
    protected WebElement numberPlateField;
    @FindBy(id = "field-type")
    private WebElement vrnTypeDropdown;
    @FindBy(xpath = "//*[@id='field-type']/option")
    private List<WebElement> vrnTypeOptions;
    @FindBy(id = "field-make")
    private WebElement vrnMakeDropdown;
    @FindBy(xpath = "//*[@id='field-make']/option")
    private List<WebElement> vrnMakeOptions;
    @FindBy(id = "field-colour")
    private WebElement vrnColourDropdown;
    @FindBy(xpath = "//*[@id='field-colour']/option")
    private List<WebElement> vrnColourOptions;
    @FindBy(name = "submits[submit]")
    private WebElement addVrnButton;
    @FindBy(name = "submits[cancel]")
    private WebElement cancelVrnButton;
    @FindBy(className = "close")
    private WebElement closeButton;

    public AddEditVrnPopup(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result selectVrnType(String type) {
        return selectItemFromDropdownByTextValue(vrnTypeDropdown, type, "Select VRN Type");
    }

    public String getVrnType() {
        WaitVisible(vrnTypeDropdown);
        int elementIndex = new Random().nextInt(vrnTypeOptions.size());
        elementIndex = elementIndex == 0 ? ++elementIndex : elementIndex;
        return vrnTypeOptions.get(elementIndex).getText();
    }

    public Result selectVrnMake(String make) {
        return selectItemFromDropdownByTextValue(vrnMakeDropdown, make, "Select VRN Make");
    }

    public String getVrnMake() {
        int elementIndex = new Random().nextInt(vrnMakeOptions.size());
        elementIndex = elementIndex == 0 ? ++elementIndex : elementIndex;
        return vrnMakeOptions.get(elementIndex).getText();
    }

    public Result selectVrnColour(String colour) {
        return selectItemFromDropdownByTextValue(vrnColourDropdown, colour, "Select VRN Colour");
    }

    public String getVrnColour() {
        int elementIndex = new Random().nextInt(vrnColourOptions.size());
        elementIndex = elementIndex == 0 ? ++elementIndex : elementIndex;
        return vrnColourOptions.get(elementIndex).getText();
    }

    public Result clickOnAddButton() {
        return ClickControl(addVrnButton, "Click on Add Button");
    }

    public Result clickOnCancelButton() {
        return ClickControl(cancelVrnButton, "Click on Cancel Button");
    }

    public Result clickOnCloseButton() {
        return ClickControl(closeButton, "Click Close Button");
    }

}
