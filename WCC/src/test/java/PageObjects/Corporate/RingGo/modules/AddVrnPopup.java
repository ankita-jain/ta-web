package PageObjects.Corporate.RingGo.modules;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.enterText;

public class AddVrnPopup extends AddEditVrnPopup {
    @FindBy(xpath = "//*[@id='field-vrn']/following-sibling::span")
    private WebElement numberPlateError;

    public AddVrnPopup(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getNumberPlateError() {
        WaitVisible(numberPlateError);
        return numberPlateError.getText();
    }

    public Result enterVrnNumber(String number) {
        WaitVisible(numberPlateField);
        return enterText(numberPlateField, number, "Enter Vehicle Number");
    }
}
