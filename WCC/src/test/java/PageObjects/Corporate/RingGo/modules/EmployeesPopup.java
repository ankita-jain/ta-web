package PageObjects.Corporate.RingGo.modules;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class EmployeesPopup extends Base_Page {
    @FindBy(className = "close")
    private WebElement closeButton;
    @FindBy(xpath = "//*[@class='modal-container']/div")
    private List<WebElement> employeesList;

    public EmployeesPopup(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickOnCloseButton() {
        WaitVisible(closeButton);
        return ClickControl(closeButton, "Click on Control Button");
    }

    public boolean isVrnAssignedToAppropriateEmployees(List<String> employees) {
        WaitVisible(employeesList.get(0));
        return employeesList.stream().allMatch(employee -> employees.contains(employee.getText()));
    }

    public boolean isVrnAssignedToAppropriateEmployees(String employeeName) {
        WaitVisible(employeesList.get(0));
        return employeesList.stream().anyMatch(employee -> employee.getText().equals(employeeName));
    }
}
