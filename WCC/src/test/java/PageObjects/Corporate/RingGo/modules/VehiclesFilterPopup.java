package PageObjects.Corporate.RingGo.modules;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VehiclesFilterPopup extends Base_Page {

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Number plate')]")
    private WebElement numberPlateTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//input[contains(@name, 'vrm')]")
    private WebElement numberPlateInput;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Type')]")
    private WebElement typeTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//select[contains(@name, 'type')]")
    private WebElement typeSelect;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Make')]")
    private WebElement makeTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//select[contains(@name, 'make')]")
    private WebElement makeSelect;

    @FindBy(xpath = "//div[@class='modal-body']//a[contains(text(), 'Colour')]")
    private WebElement colourTabLink;
    @FindBy(xpath = "//div[@class='modal-body']//select[contains(@name, 'colour')]")
    private WebElement colourSelect;

    @FindBy(xpath = "//input[@value='Apply']")
    private WebElement applyButton;

    public VehiclesFilterPopup(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickNumberPlate(){
        WaitVisible(numberPlateTabLink);
        return WebElementMethods.ClickControl(numberPlateTabLink, "Number Plate Tab");
    }
    public Result enterTextToVrmInput(String vehilce) {
        WaitVisible(numberPlateInput);
        return WebElementMethods.enterText(numberPlateInput, vehilce, "VRM input");
    }

    public Result clickType(){
        WaitVisible(typeTabLink);
        return WebElementMethods.ClickControl(typeTabLink, "Type Tab");
    }
    public Result selectType(String type) {
        WaitVisible(typeSelect);
        return WebElementMethods.selectItemFromDropdownByTextValue(typeSelect, type, "Type select");
    }

    public Result clickMake(){
        WaitVisible(makeTabLink);
        return WebElementMethods.ClickControl(makeTabLink, "Make Tab");
    }
    public Result selectMake(String make) {
        WaitVisible(makeSelect);
        return WebElementMethods.selectItemFromDropdownByTextValue(makeSelect, make, "Make select");
    }

    public Result clickColour(){
        WaitVisible(colourTabLink);
        return WebElementMethods.ClickControl(colourTabLink, "Colour Tab");
    }
    public Result selectColour(String colour) {
        WaitVisible(colourSelect);
        return WebElementMethods.selectItemFromDropdownByTextValue(colourSelect, colour, "Colour select");
    }

    public Result clickOnApplyButton(){
        WaitVisible(applyButton);
        return WebElementMethods.ClickControl(applyButton, "Apply button");
    }
}
