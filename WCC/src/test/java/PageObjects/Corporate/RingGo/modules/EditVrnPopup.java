package PageObjects.Corporate.RingGo.modules;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Structures.Result;

public class EditVrnPopup extends AddEditVrnPopup {
    @FindBy(xpath = "//*[@class='bootstrap-tagsinput']//input[2]")
    private WebElement employeesField;

    public EditVrnPopup(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean isVrnNumberFieldEditable() {
        WaitVisible(numberPlateField);
        return numberPlateField.isEnabled();
    }

    public Result enterEmployee(String employeeName) {
        WaitVisible(employeesField);
        enterText(employeesField, employeeName, "Enter Employee Name");
        WebElement employee = WaitVisibleElement(By.xpath(String.format("//div[text()='%s']", employeeName)));
        Actions builder = new Actions(_driver);
        builder.moveToElement(employee).perform();
        return ClickControl(employee, "Click on Employee Name");
    }
}
