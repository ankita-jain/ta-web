package PageObjects.Corporate.RingGo;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.checkCheckBox;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import GenericComponents.complexwebobjects.Table;
import PageObjects.Corporate.RingGo.modules.EmployeesPopup;
import SeleniumHelpers.Base_Page;
import Structures.Result;

public class CorporateVehiclePage extends Base_Page {
    @FindBy(xpath = "//*[contains(@title, 'Add')]")
    public WebElement addVrnButton;
    @FindBy(xpath = "//*[contains(text(), 'Filter')]")
    public WebElement filterVrnButton;
    @FindBy(xpath = ".//a[contains(text(),'Unassigned')]")
    private WebElement unassignedTab;
    @FindBy(xpath = ".//a[contains(text(),'Assigned')]")
    private WebElement assignedTab;
    @FindBy(xpath = "//tbody/tr")
    private List<WebElement> vrnRecords;
    @FindBy(xpath = "//*[@data-grid-action='delete']")
    public WebElement deleteButton;
    @FindBy(xpath = "//*[@data-grid-action='assign_all_employees']")
    public WebElement assignToAllEmployeeButton;
    @FindBy(xpath = "//*[@data-grid-action='remove_all_employees']")
    public WebElement removeFromAllEmployeeButton;
    @FindBy(xpath = "//a[text()='Number plate']")
    public WebElement numberPlateLink;
    @FindBy(className = "success-notification")
    public WebElement successNotification;
    @FindBy(xpath = "//*[@data-grid-action='assign_all_employees']")
    private WebElement assignedButton;

    @FindBy(id = "dgridtable1")
    private WebElement vrnTableObject;

    private final static String EDIT_VRN_LOCATOR_PATTERN = "//td/a[@href='/assets/editVehicle?vrn=%s']";
    private final static String REMOVE_FROM_ALL_EMPLOYEES_LOCATOR_PATTERN = "//a[@href='/assets/removeFromAllEmployees?vrn=%s']";
    private final static String KEBAB_MENU_LOCATOR_PATTERN = EDIT_VRN_LOCATOR_PATTERN + "/../following-sibling::td/a[@class='kebab-link']";
    private final static String DELETE_BUTTON_LOCATOR_PATTERN = "//a[@href='/assets/deleteVehicle?vrn=%s']";
    private final static String ASSIGN_TO_ALL_EMPLOYEES_BUTTON_LOCATOR_PATTERN = "//a[@href='/assets/assignToAllEmployees?vrn=%s']";
    private final static String CHECKBOX_LOCATOR_PATTERN = "//input[@value='%s']";

    private final static String ELLIPSIS_LOCATOR_PATTERN = EDIT_VRN_LOCATOR_PATTERN + "/../following-sibling::td/*[@class='get-more-modal']";
    //private final static String EMPLOYEES_LOCATOR_PATTERN = EDIT_VRN_LOCATOR_PATTERN + "/../following-sibling::td[4]";

    public KebabMenuLink kebabMenuLink;
    public KebabMenu kebabMenu;

    public Table vrnTabel = new Table(vrnTableObject);

    public CorporateVehiclePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickAddVrnButton() {
        WaitVisible(addVrnButton);
        return ClickControl(addVrnButton, "Click on 'Add vehicle' button");
    }

    public Result clickUnassignedTab() {
        _driver.navigate().refresh();
        WaitVisible(unassignedTab);
        return ClickControl(unassignedTab, "Click Unassigned Tab");
    }

    public Result clickAssignedTab() {
        _driver.navigate().refresh();
        WaitVisible(assignedTab);
        return ClickControl(assignedTab, "Click Assigned Tab");
    }

    public boolean isVrnPresent(String vrnNumber) {
        return vrnRecords.stream().anyMatch(record -> record.getText().toUpperCase().contains(vrnNumber.toUpperCase()));
    }

    public boolean isVrnPresent(String vrnNumber, String employeeName) {
        return vrnRecords.stream().anyMatch(record -> record.getText().toUpperCase().contains(vrnNumber.toUpperCase())
                && record.getText().contains(employeeName));
    }

    public boolean isVrnPresent(String vrnNumber, String type, String make, String colour) {
        return vrnRecords.stream().anyMatch(record ->
                record.getText().toUpperCase().contains(vrnNumber.toUpperCase())
                        && record.getText().toUpperCase().contains(type.toUpperCase())
                        && record.getText().toUpperCase().contains(make.toUpperCase())
                        && record.getText().toUpperCase().contains(colour.toUpperCase())
        );
    }

    public boolean isVrnAssignedToEmployee(String vrn, String employeeName) {
        for (WebElement record : vrnRecords) {
            if (record.getText().contains(vrn)) {
                boolean result = record.getText().contains("...") ? isEmployeeInList(vrn, employeeName) : record.getText().contains(employeeName);
                if (record.getText().contains("...") ) new EmployeesPopup(_driver).clickOnCloseButton();
                return result;
            }
        }
        return false;
    }

    public Result clickOnEditVrn(String vrnNumber) {
        WebElement editVrn = WaitVisibleElement(By.xpath(String.format(EDIT_VRN_LOCATOR_PATTERN, vrnNumber)));
        return ClickControl(editVrn, "Edit VRN Link");
    }

    public Result clickOnRemoveFromAllEmployees(String vrnNumber) {
        WebElement removeAllEmployee = WaitVisibleElement(By.xpath(String.format(REMOVE_FROM_ALL_EMPLOYEES_LOCATOR_PATTERN, vrnNumber)));
        return ClickControl(removeAllEmployee, "Remove From All Employees Link");
    }

    public Result clickOnKebabMenu(String vrnNumber) {
        WaitVisible(_driver.findElement(By.xpath(String.format(KEBAB_MENU_LOCATOR_PATTERN, vrnNumber))));
        return ClickControl(_driver.findElement(By.xpath(String.format(KEBAB_MENU_LOCATOR_PATTERN, vrnNumber))), "Edit VRN Link");
    }

    public Result clickOnDeleteButton(String vrnNumber) {
        WaitVisible(_driver.findElement(By.xpath(String.format(DELETE_BUTTON_LOCATOR_PATTERN, vrnNumber))));
        return ClickControl(_driver.findElement(By.xpath(String.format(DELETE_BUTTON_LOCATOR_PATTERN, vrnNumber))), "Edit VRN Link");
    }

    public Result clickOnDeleteButton() {
        WaitVisible(deleteButton);
        return ClickControl(deleteButton, "Click on delete button");
    }

    public Result clickAssignToAllEmployeesButton(String vrnNumber) {
        WebElement assignToAllEmployee = WaitVisibleElement(By.xpath(String.format(ASSIGN_TO_ALL_EMPLOYEES_BUTTON_LOCATOR_PATTERN, vrnNumber)));
        return ClickControl(assignToAllEmployee, "Click on Assign to All Employees Button");
    }

    public Result clickAssignToAllEmployeesButton() {
        WaitVisible(assignedButton);
        return ClickControl(assignedButton, "Click on Assign to All Employees Button");
    }

    public Result clickOnRemoveFromAllEmployeesButton() {
        WaitVisible(removeFromAllEmployeeButton);
        return ClickControl(removeFromAllEmployeeButton, "Click on Remove From All Employees button");
    }

    public Result selectCheckbox(String vrn) {
        WebElement checkbox = WaitVisibleElement(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, vrn)));
        return checkCheckBox(checkbox, "Check VRN checkbox");
    }

    public WebElement getVrnRow(String vrn) {
        Table vrnTable = new Table(this.vrnTableObject);
        return vrnTable.getRowByCellValue(vrn, VehiclesTableColumns.NUMBER_PLATE);
    }

    public Result clickOnGetMoreEmployees(String vrnNumber) {
        WebElement ellipsis = WaitVisibleElement(By.xpath(String.format(ELLIPSIS_LOCATOR_PATTERN, vrnNumber)));
        return ClickControl(ellipsis, "Click on '...' for Employees");
    }

    public Result clickOnNumberPlate() {
        WaitVisible(numberPlateLink);
        return ClickControl(numberPlateLink, "Click on 'Number plate'");
    }

    public Result clickOnFilter() {
        WaitVisible(filterVrnButton);
        return ClickControl(filterVrnButton, "Click on 'Filter' button");
    }

    public boolean isVrnTableContainsAllColumns() {
        List<String> expectedColumns = Arrays.asList(VehiclesTableColumns.NUMBER_PLATE.getHeader(), VehiclesTableColumns.TYPE.getHeader(),
                VehiclesTableColumns.MAKE.getHeader(), VehiclesTableColumns.COLOUR.getHeader(), VehiclesTableColumns.EMPLOYEES.getHeader());
        for (String value : expectedColumns) {
            if (!vrnTableObject.findElement(By.xpath("//tr")).getText().contains(value))
                return false;
        }
        return true;
    }

    private boolean isEmployeeInList(String vrnNumber, String employeeName) {
        clickOnGetMoreEmployees(vrnNumber);
        return new EmployeesPopup(_driver).isVrnAssignedToAppropriateEmployees(employeeName);
    }

    public static class KebabMenuLink {
        private final String KEBAB_MENU_LINK_LOCATOR = "//*[@class='kebab-link']";
        private WebElement row;

        public KebabMenuLink(WebElement row) {
            this.row = row;
        }

        public Result clickOnKebabMenu() {
            return ClickControl(row.findElement(By.xpath(KEBAB_MENU_LINK_LOCATOR)), "Edit VRN Link");
        }
    }

    public static class KebabMenu {
        private final String KEBAB_MENU_LOCATOR = "//*[@class='kebab-menu']";
        private final String EDIT_LINK_LOCATOR = KEBAB_MENU_LOCATOR + "//*[@data-modal-class='add-edit-vrn-modal']";
        private final String DELETE_LINK_LOCATOR = KEBAB_MENU_LOCATOR + "//*[text()='Delete']";
        private final String ASSIGN_TO_ALL_EMPLOYEES_LINK_LOCATOR = KEBAB_MENU_LOCATOR + "//*[contains(text(), 'Assign')]";
        private final String REMOVE_FROM_ALL_EMPLOYEES_LINK_LOCATOR = KEBAB_MENU_LOCATOR + "//*[contains(text(), 'Remove')]";

        public WebElement editLink;
        public WebElement deleteLink;
        public WebElement assignToAllEmployeeLink;
        public WebElement removeFromAllEmployeeLink;

        public KebabMenu(WebElement row) {
            editLink = row.findElement(By.xpath(EDIT_LINK_LOCATOR));
            deleteLink = row.findElement(By.xpath(DELETE_LINK_LOCATOR));
            assignToAllEmployeeLink = row.findElement(By.xpath(ASSIGN_TO_ALL_EMPLOYEES_LINK_LOCATOR));
            removeFromAllEmployeeLink = row.findElement(By.xpath(REMOVE_FROM_ALL_EMPLOYEES_LINK_LOCATOR));
        }
    }

    public enum VehiclesTableColumns {
        NUMBER_PLATE("Number plate", 1),
        TYPE("Type", 2),
        MAKE("Make", 3),
        COLOUR("Colour", 4),
        EMPLOYEES("Employees", 5);

        private String header;
        private int position;

        VehiclesTableColumns(String header, int position) {
            this.header = header;
            this.position = position;
        }

        @Override
        public String toString() {
            return header;
        }

        public String getHeader() {
            return header;
        }

        public int getPosition() {
            return position;
        }
    }
}
