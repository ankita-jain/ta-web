package PageObjects.Corporate.RingGo;

import SeleniumHelpers.Base_Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CorporateHomePage extends Base_Page {

    public CorporateHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
}
