package PageObjects.Corporate.RingGo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class CorporateAddEditEmployeePage extends Base_Page {

    @FindBy(id = "pageTitle")
    private WebElement pageTitle;
    @FindBy(id = "FirstName")
    private WebElement firstNameField;
    @FindBy(id = "Surname")
    private WebElement surnameField;
    @FindBy(id = "Email")
    private WebElement emailField;
    @FindBy(xpath = "//*[contains(@for, 'any-cli') and text()='Specified']")
    private WebElement specifiedPhoneButton;
    @FindBy(xpath = "//*[contains(@for, 'any-cli') and text()='Any']")
    private WebElement anyPhoneButton;
    @FindBy(xpath = "//*[contains(@for, 'any-vrn') and text()='Specified']")
    private WebElement specifiedVrnButton;
    @FindBy(xpath = "//*[contains(@for, 'any-vrn') and text()='Any']")
    private WebElement anyVrnButton;
    @FindBy(xpath = "//div[@class='element div-staticCanPark']//input[@id='can-park-off']")
    private WebElement canParkOffField;
    @FindBy(id = "field-EmployeeRef")
    private WebElement referenceField;
    @FindBy(id = "new-cli")
    private WebElement cliField;
    @FindBy(id = "new-vrn")
    private WebElement vrnField;
    @FindBy(id = "add-cli")
    private WebElement addCliButton;
    @FindBy(id = "add-vrn")
    private WebElement addVrnButton;
    @FindBy(id = "save-button")
    private WebElement saveEmployeeButton;

    @FindBy(xpath = "//*[@id='Email']/following-sibling::span")
    private WebElement emailErrorMessage;
    @FindBy(xpath = "//*[contains(@for, 'send-email')]/following-sibling::span")
    private WebElement sendWelcomeEmailErrorMessage;
    @FindBy(xpath = "//*[@id='FirstName']/following-sibling::span")
    private WebElement firstNameErrorMessage;
    @FindBy(xpath = "//*[@id='Surname']/following-sibling::span")
    private WebElement surnameErrorMessage;
    @FindBy(xpath = "//*[contains(@for, 'any-cli')]/following-sibling::span")
    private WebElement cliErrorMessage;
    @FindBy(xpath = "//*[contains(@for, 'any-vrn')]/following-sibling::span")
    private WebElement vrnErrorMessage;

    @FindBy(className = "warning-notification")
    private WebElement warningNotification;

    @FindBy(id = "label-staticCanPark")
    private WebElement canEmployeeParkingToolTip;
    @FindBy(id = "label-staticCLI")
    private WebElement cliToolTip;
    @FindBy(id = "label-staticVRN")
    private WebElement vrnToolTip;

    @FindBy(xpath = "//*[@id='specified-cli']/div/span")
    private List<WebElement> specifiedClis;
    @FindBy(xpath = "//*[@id='specified-vrn']/div/span")
    private List<WebElement> specifiedVrns;

    @FindBy(xpath = "//*[contains(@for, 'send-email') and text()='Yes']")
    private WebElement yesWelcomeEmailButton;
    @FindBy(xpath = "//*[contains(@for, 'send-email') and text()='No']")
    private WebElement noWelcomeEmailButton;

    @FindBy(id = "field-CostCentreID")
    private WebElement costCentreSelect;

    private static final String X_BUTTON_XPATH_PATTERN = "//*[contains(text(),'%s')]/span";

    public CorporateAddEditEmployeePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getFirstNameField() {
        return firstNameField;
    }

    public WebElement getSurnameField() {
        return surnameField;
    }

    public String getPageTitleText() {
        WaitVisible(pageTitle);
        return pageTitle.getText();
    }

    public String getEmailErrorMessageText() {
        WaitVisible(emailErrorMessage);
        return emailErrorMessage.getText();
    }

    public String getSendWelcomeEmailErrorMessageText() {
        WaitVisible(sendWelcomeEmailErrorMessage);
        return sendWelcomeEmailErrorMessage.getText();
    }

    public String getFirstNameErrorMessageText() {
        WaitVisible(firstNameErrorMessage);
        return firstNameErrorMessage.getText();
    }

    public String getSurnameErrorMessageText() {
        return surnameErrorMessage.getText();
    }

    public String getCliErrorMessageText() {
        return cliErrorMessage.getText();
    }

    public String getVrnErrorMessageText() {
        return vrnErrorMessage.getText();
    }

    public String getWarningNotificationText() {
        return warningNotification.getText();
    }

    public String getCanParkTooltipText() {
        return canEmployeeParkingToolTip.getAttribute("title");
    }

    public String getCliTooltipText() {
        return cliToolTip.getAttribute("title");
    }

    public String getVrnTooltipText() {
        return vrnToolTip.getAttribute("title");
    }

    public String getAnyCliTooltipText() {
        return anyPhoneButton.getAttribute("title");
    }

    public String getAnyVrnTooltipText() {
        return anyVrnButton.getAttribute("title");
    }

    public List<String> getSpecifiedClisText() {
        List<String> clis = new ArrayList<>();
        specifiedClis.stream().map(WebElement::getText).forEach(clis::add);
        return clis;
    }

    public List<String> getSpecifiedVrnsText() {
        List<String> vrns = new ArrayList<>();
        specifiedVrns.stream().map(WebElement::getText).forEach(vrns::add);
        return vrns;
    }

    public Result enterFirstName(String firstName) {
        return enterText(firstNameField, firstName, "Enter First Name");
    }

    public Result enterSurname(String surname) {
        return enterText(surnameField, surname, "Enter Surname");
    }

    public Result enterEmail(String email) {
        return enterText(emailField, email, "Enter Email");
    }

    public Result enterReference(String reference) {
        return enterText(referenceField, reference, "Enter Reference");
    }


    public Result enterCli(String cli) {
        return enterText(cliField, cli, "Enter CLI");
    }

    public Result enterVrn(String vrn) {
        return enterText(vrnField, vrn, "Enter VRN");
    }

    public Result clickOnAnyCliButton() {
        return ClickControl(anyPhoneButton, "Any CLI");
    }

    public Result clickOnSpecifiedCli() {
        return ClickControl(specifiedPhoneButton, "Specified CLI button");
    }

    public Result clickOnSpecifiedVrn() {
        return ClickControl(specifiedVrnButton, "Specified VRN button");
    }

    public Result clickOnAddCli() {
        return ClickControl(addCliButton, "Add CLI button");
    }

    public Result clickOnAddVrn() {
        return ClickControl(addVrnButton, "Add VRN button");
    }

    public Result clickOnAnyVrn() {
        return ClickControl(anyVrnButton, "Any VRN button");
    }

    public Result clickOnSaveEmployee() {
        return ClickControl(saveEmployeeButton, "Save Employee button");
    }

    public boolean isAnyVrnButtonDisable() {
        return anyVrnButton.getAttribute("class").contains("disabled-choice");
    }

    public boolean isAnyCliDisable() {
        return anyPhoneButton.getAttribute("class").contains("disabled-choice");
    }

    public Result removeSpecifiedCli(String cli) {
        return ClickControl(_driver.findElement(By.xpath(String.format(X_BUTTON_XPATH_PATTERN, cli))), "Remove specified cli");
    }

    public Result removeSpecifiedVrn(String vrn) {
        return ClickControl(_driver.findElement(By.xpath(String.format(X_BUTTON_XPATH_PATTERN, vrn))), "Remove specified vrn");
    }

    public void setCanParkOff() {
        JavascriptExecutor executor = (JavascriptExecutor) _driver;
        executor.executeScript("arguments[0].click();", canParkOffField);
    }

    public Result clickOnNoWelcomeEmail() {
        WaitVisible(noWelcomeEmailButton);
        return ClickControl(noWelcomeEmailButton, "No Welcome Email");
    }

    public Result clickOnYesWelcomeEmail() {
        return ClickControl(yesWelcomeEmailButton, "Yes Welcome Email");
    }

    public Result selectCostCentre(String costCentre) {
        return WebElementMethods.selectItemFromDropdownByTextValue(costCentreSelect, costCentre, "Cost Centre select");
    }
}
