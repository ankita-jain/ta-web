package PageObjects.Corporate.RingGo;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import PageObjects.Corporate.RingGo.modules.KebabMenu;
import SeleniumHelpers.Base_Page;
import Structures.Result;

public class CorporateEmployeesPage extends Base_Page {

    private KebabMenu kebabMenu = new KebabMenu(_driver);

    @FindBy(xpath = "//a[contains(@href, '/employees/add')]")
    private WebElement addEmployee;
    @FindBy(className = "success-notification")
    private WebElement notification;
    @FindBy(xpath = "//thead//th")
    private List<WebElement> employeeTableHeader;
    @FindBy(xpath = "//tbody/tr")
    private List<WebElement> employeeRecords;

    private static final String KEBAB_MENU_BY_EMPLOYEE_NAME = "//tr//a[contains(text(), '%s')]/../following-sibling::node()//*[@class='kebab-link']";

    public CorporateEmployeesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickOnAddEmployeeButton() {
        WaitVisible(addEmployee);
        return ClickControl(addEmployee, "Add Employee Button");
    }

    public String getNotificationText() {
        WaitVisible(notification);
        return notification.getText();
    }

    public boolean isEmployeeRecordPresentByName(String name) {
        return employeeRecords.stream().anyMatch(record -> record.getText().toUpperCase().contains(name.toUpperCase()));
    }

    public boolean isEmployeeRecordPresentByName(String name, String vrn) {
        return employeeRecords.stream().anyMatch(record -> record.getText().toUpperCase().contains(name.toUpperCase())
        && employeeRecords.stream().anyMatch(r -> r.getText().toUpperCase().contains(vrn.toUpperCase()) || r.getText().contains("Any vehicle")));
    }

    public boolean isVrnRemovedFromEmployee(String vrn) {
        return employeeRecords.stream().noneMatch(record -> record.getText().toUpperCase().contains(vrn.toUpperCase()));
    }

    public KebabMenu clickOnKebabMenu(String employeeName) {
    	WaitVisible(By.xpath(String.format(KEBAB_MENU_BY_EMPLOYEE_NAME, employeeName)));
        ClickControl(_driver.findElement(By.xpath(String.format(KEBAB_MENU_BY_EMPLOYEE_NAME, employeeName))), "Kebab Menu");
        return kebabMenu;
    }
}
