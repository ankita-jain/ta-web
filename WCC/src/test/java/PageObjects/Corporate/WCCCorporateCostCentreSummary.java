package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateCostCentreSummary extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='start']")
    @CacheLookup
    private WebElement startDate;
    @FindBy(xpath = "//*[@name='end']")
    @CacheLookup
    private WebElement endDate;
    @FindBy(xpath = "//*[@name='ZeroCost']")
    @CacheLookup
    private WebElement unusedCostCentre;
    @FindBy(xpath = "//*[@name='reportType']")
    @CacheLookup
    private WebElement reportType;
    @FindBy(xpath = "//*[@class='box-content']/p[3]/a")
    @CacheLookup
    private WebElement exportCSVButton;
    @FindBy(xpath = "//*[@id='dgridtable1']")
    private WebElement searchResultTable;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement searchButton;
    @FindBy(xpath = "//*[@id='dgridtable1']/tfoot//td[5]")
    private WebElement costTotal;

    public WCCCorporateCostCentreSummary(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getCostCentrePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void enterStartDate(String startdate) {
        WaitVisible(startDate);
        startDate.clear();
        startDate.sendKeys(startdate);
    }

    public void enterEndDate(String enddate) {
        WaitVisible(endDate);
        endDate.clear();
        endDate.sendKeys(enddate);
    }

    public void clickUnUsedCostCentre() {
        WaitVisible(unusedCostCentre);
        unusedCostCentre.click();
    }

    public boolean isDataTableVisible() {
        return isPresentAndDisplayed(searchResultTable);
    }

    public boolean isCostTotalVisible() {
        return isPresentAndDisplayed(costTotal);
    }

    public void clickExportCSVButton() {
        WaitVisible(exportCSVButton);
        exportCSVButton.click();
    }

    public void selectReportType(String reporttype) {
        WaitVisible(reportType);
        new Select(reportType).selectByVisibleText(reporttype);
    }

    public void clickSearchButton() {
        WaitVisible(searchButton);
        searchButton.click();
    }

    public void costCentreSummarySearch(String startdate, String enddate, String reporttype) {
        enterStartDate(startdate);
        enterEndDate(enddate);

        if (!unusedCostCentre.isSelected())
            clickUnUsedCostCentre();

        selectReportType(reporttype);
        clickSearchButton();

    }

}
