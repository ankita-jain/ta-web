package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCCorporateParkEmployeeSelectVRNPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='CLI']")
    @CacheLookup
    private WebElement EmployeeCLI;
    @FindBy(xpath = "//*[@name='VRN']")
    @CacheLookup
    private WebElement EmployeeVRN;
    @FindBy(xpath = "//*[@name='labyrinth_SelectCliVrn_next']")
    @CacheLookup
    private WebElement nextButton;

    public WCCCorporateParkEmployeeSelectVRNPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkEmployeePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void selectEmployeeCLI(String cli) {
        WaitVisible(EmployeeCLI);
        new Select(EmployeeCLI).selectByVisibleText(cli);
    }

    public void selectEmployeeVRN(String vrn) {
        WaitVisible(EmployeeVRN);
        new Select(EmployeeVRN).selectByVisibleText(vrn);
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public void selectEmployeeCLIAndVRN(String cli, String vrn) {
        selectEmployeeCLI(cli);
        selectEmployeeVRN(vrn);
        clickNextButton();
    }
}
