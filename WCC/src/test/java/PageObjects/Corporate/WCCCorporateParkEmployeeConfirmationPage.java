package PageObjects.Corporate;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateParkEmployeeConfirmationPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1")
    @CacheLookup
    private WebElement PageTitle;
    @FindBy(xpath = "//*[@name='labyrinth_Confirm_next']")
    @CacheLookup
    private WebElement confirmButton;
    @FindBy(xpath = "//*[@name='labyrinth_Confirm_back']")
    @CacheLookup
    private WebElement previousButton;
    @FindBy(xpath = "//*[@name='labyrinth_cancel']")
    @CacheLookup
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@name='labyrinth_finish']")
    @CacheLookup
    private WebElement finishButton;

    public WCCCorporateParkEmployeeConfirmationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getParkEmployeePageTitle() {
        WaitVisible(PageTitle);
        return PageTitle.getText();
    }

    public void clickConfirmButton() {
        WaitVisible(confirmButton);
        confirmButton.click();
    }

    public void clickPreviousButton() {
        WaitVisible(previousButton);
        previousButton.click();
    }

    public void clickCancelButton() {
        WaitVisible(cancelButton);
        cancelButton.click();

    }

    public void clickFinishButton() {
        WaitVisible(finishButton);
        finishButton.click();
    }
}
