package PageObjects.Corporate;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCCorporateAdminAddPage extends Base_Page {

    public WebDriver driver;
    @FindBy(xpath = "//div[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement corporateAdminAddPageTitle;
    @FindBy(xpath = "//*[@id='field-Email']")
    @CacheLookup
    private WebElement emailField;
    @FindBy(xpath = "//*[@id='field-FirstName']")
    @CacheLookup
    private WebElement firstName;
    @FindBy(xpath = "//*[@id='field-Surname']")
    @CacheLookup
    private WebElement surName;
    @FindBy(xpath = "//*[@id='field-NewPassword']")
    @CacheLookup
    private WebElement adminPassword;
    @FindBy(xpath = "//*[@value='Cancel']")
    @CacheLookup
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@value='Save']")
    private WebElement saveButton;
    @FindBy(xpath = "//ul[@class='warning-notification']")
    @CacheLookup
    private WebElement errorNotification;

    public WCCCorporateAdminAddPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(corporateAdminAddPageTitle);
        return corporateAdminAddPageTitle.getText();
    }

    public Result enterAdminEmailId(String email) {
        WaitVisible(emailField);
        return WebElementMethods.enterText(emailField, email, "Admin Email");
    }

    public Result enterAdminFirstName(String firstname) {
        WaitVisible(firstName);
        return WebElementMethods.enterText(firstName, firstname, "First Name");
    }

    public Result enterAdminSurName(String surname) {
        WaitVisible(surName);
        return WebElementMethods.enterText(surName, surname, "Surname");
    }

    public Result enterAdminPassword(String password) {
        WaitVisible(adminPassword);
        return WebElementMethods.enterText(adminPassword, password, "Admin Password");
    }

    public Result clickCancelButton() {
        WaitVisible(cancelButton);
        return WebElementMethods.ClickControl(cancelButton, "Cancel button");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return WebElementMethods.ClickControl(saveButton, "Save button");
    }

    public boolean isErrorNotificationVisible() {
        WaitVisible(errorNotification);
        return errorNotification.isDisplayed();
    }

    public String getErrorNotification() {
        WaitVisible(errorNotification);
        return errorNotification.getText();
    }
}
