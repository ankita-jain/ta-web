package PageObjects.Corporate;

import static GenericComponents.constants.Symbols.POUND;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import SeleniumHelpers.Base_Page;
import Structures.Result;

public class WCCCorporateTopupPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement topupPageTitle;

    @FindBy(xpath = "//*[@id='field-topup']")
    private WebElement topupAmountField;

    @FindBy(xpath = "//*[@id='paymentMethod']")
    private WebElement topupPaymentMethod;

    @FindBy(xpath = "//*[@id='field-submit']")
    private WebElement topupNextButton;

    @FindBy(xpath = "//*[@name='paymentRequest']")
    private WebElement topupPaymentRequest;

    @FindBy(xpath = "//*[@name='confirm_bank_transfer']")
    private WebElement confirm_bank_transfer_checkbox;

    @FindBy(xpath = "//*[@name='_qf_BACS_back']")
    private WebElement bacs_back_button;

    @FindBy(xpath = "//*[@name='_qf_BACS_next']")
    private WebElement bacs_next_button;

    @FindBy(xpath = "//*[@name='_qf_Finalise_next']")
    private WebElement bacs_finish_button;

    @FindBy(xpath = "//*[@class='box-content']/p[1]")
    private WebElement top_up_limit_on_account;

    @FindBy(xpath = "//*[@class='error']")
    private WebElement errorMessage;

    @FindBy(xpath = "//*[@id='label-static']")
    private WebElement topLockedOnAccount;

    public WCCCorporateTopupPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTopupPageTitle() {
        WaitVisible(topupPageTitle);
        return topupPageTitle.getText();
    }

    public String getTopupLimitOnAccountText() {
        WaitVisible(top_up_limit_on_account);
        return top_up_limit_on_account.getText();
    }

    public boolean isAccountReachedMaximumTopup() {
        return WaitVisible(topLockedOnAccount);
    }

    public String getErrorMessage() {
        WaitVisible(errorMessage);
        return errorMessage.getText();
    }

    public Result enterTopupAmountField(String top_amunt) {
        WaitVisible(topupAmountField);
        return enterText(topupAmountField, top_amunt, "Top up amount field");
    }

    public Result selectTopupPaymentMethod(String payment_method) {
        WaitVisible(topupPaymentMethod);
        return selectItemFromDropdownByTextValue(topupPaymentMethod, payment_method, "Payment method dropdown");
    }

    public Result clickNextButton() {
        WaitVisible(topupNextButton);
        return ClickControl(topupNextButton, "Next button");
    }

    public Result clickPaymentRequest() {
        WaitVisible(topupPaymentRequest);
        return ClickControl(topupPaymentRequest, "Top up payment request");
    }

    public Result clickBACSPaymentFinishButon() {
        WaitVisible(bacs_finish_button);
        return ClickControl(bacs_finish_button, "Basc payment finish button");
    }
    /*
     * Function 'getTopupLimitOnUserAccount()' will find the limit of top up on the account
     * For instance if the messages displays like this "Your current balance is £352.80, the amount available for you to park is £350.30.
     * You can top up in increments of £10, up to £7,100." The below function will find £7,100 out of the whole message.
     *
     */

    public String getTopupLimitOnUserAccount() {
        String[] limitArray = getTopupLimitOnAccountText().split(POUND.getAsUTF8(), -1);
        return limitArray[4];
    }
}
