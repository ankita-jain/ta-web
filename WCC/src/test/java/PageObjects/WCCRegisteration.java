package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class WCCRegisteration extends Base_Page {
	
	public WCCRegisteration(WebDriver driver) {
        super(driver);
    }
	
	@FindBy(xpath = "//*[@class='page-head']//h1")
	@CacheLookup
	private WebElement TitleofPage;
	
	@FindBy(xpath = "//*[@id='field-Member_CLI']")
    @CacheLookup
    private WebElement TelephoneNumber;
	
	@FindBy(xpath = "//*[@id='field-Member_CLI_confirm']")
	@CacheLookup
	private WebElement ConfirmTelephoneNumber;
	
	@FindBy(xpath = "//*[@id='field-Member_Title']")
	@CacheLookup
	private WebElement MemberTitle;
		
	@FindBy(xpath = "//*[@id='field-Member_Firstname']")
	@CacheLookup
	private WebElement MemberFirstName;
	
	@FindBy(xpath = "//*[@id='field-Member_Lastname']")
	@CacheLookup
	private WebElement MemberLastName;
	
	@FindBy(xpath = "//*[@id='field-Member_Email']")
	@CacheLookup
	private WebElement MemberEmailAddress;
	
	@FindBy(xpath = "//*[@id='field-Member_Email_confirm']")
	@CacheLookup
	private WebElement MemberConfirmEmailAddress;
	
	@FindBy(xpath = "//*[@id='field-MemberPassword']")
	@CacheLookup
	private WebElement Password;
	
	@FindBy(xpath = "//*[@id='field-MemberPassword_confirm']")
	@CacheLookup
	private WebElement ConfirmPassword;
	
	@FindBy(xpath = "//*[@id='field-SecretQuestion']")
	@CacheLookup
	private WebElement SecretQuestion;
	
	@FindBy(xpath = "//*[@id='field-SecretAnswer']")
	@CacheLookup
	private WebElement SecretAnswer;
	
	public String getPageTitle() {
    WaitVisible(TitleofPage);
    return TitleofPage.getText();
    
   /* public Result inputPhonenumber(String TelNumber) {
    WaitVisible(TelephoneNumber);
    return enterText(TelephoneNumber, TelNumber, "Email or phone number input");
    } */
        
        
    }
	
	

    }

