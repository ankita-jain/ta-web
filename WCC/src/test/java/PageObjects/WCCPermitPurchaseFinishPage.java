package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class WCCPermitPurchaseFinishPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
    @FindBy(xpath = "//*[@name='labyrinth_finish']")
    private WebElement finish_button;

    public WCCPermitPurchaseFinishPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public Result clickFinish() {
    	WaitVisible(finish_button);
        return ClickControl(finish_button, "Click finish button");
    }
}
