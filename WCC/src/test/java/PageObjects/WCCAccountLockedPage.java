package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCAccountLockedPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']")
    private WebElement pageHeader;

    @FindBy(xpath = "//a[@href='/resetpassword']")
    private WebElement resetPassword;

    public WCCAccountLockedPage(WebDriver driver) {
        super(driver);
    }

    public String getAccountLockedHeader() {
        WaitVisible(pageHeader);
        return pageHeader.getText();
    }

    public Result clickResetPasswordLink() {
        return ClickControl(resetPassword, "Reset password link");
    }
}
