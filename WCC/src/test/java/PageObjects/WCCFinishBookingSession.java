package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCFinishBookingSession extends Base_Page {
    @FindBy(xpath = "//input[@name='labyrinth_Finish_next']")
    private WebElement finishButton;

    public WCCFinishBookingSession(WebDriver driver) {
        super(driver);
    }

    public Result finish() {
        WaitVisible(finishButton);
        return ClickControl(finishButton, "Finish button");
    }
}
