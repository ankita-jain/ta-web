package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class WCCPurchasePermitPaymentType extends PageClass {
    public WebDriver driver;
    @FindBy(xpath = "//*[@name='PaymentMethod[type]'][@value=2]")
    private WebElement take_payment_now;
    @FindBy(xpath = "//*[@name='PaymentMethod[type]'][@value=1]")
    private WebElement add_to_basket;
    @FindBy(xpath="//input[@name='corporate'][ @value='1']")
    private WebElement corporate_payment;
    @FindBy(xpath="//input[@name='corporate'][2]")
    private WebElement no_corporate_payment;    
    @FindBy(xpath = "//*[@name='labyrinth_CorpPayment_next']")
    private WebElement corporate_next_button; 
    @FindBy(xpath = "//*[@name='labyrinth_CorpConfirm_next']")
    private WebElement corporate_confirm_button;
    @FindBy(xpath = "//*[@name='labyrinth_LABasket_next']")
    private WebElement next_button;
    @FindBy(xpath = "//*[@name='labyrinth_LABasket_back']")
    private WebElement previous_button;
    @FindBy(xpath = "//*[@name='labyrinth_cancel']")
    private WebElement cancel_button;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement page_title;

    public WCCPurchasePermitPaymentType(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickTakePaymentNow() {
    	WaitVisible(take_payment_now);
        jsClick(take_payment_now);
    }

    public void clickAddToBasket() {
    	WaitVisible(add_to_basket);
        jsClick(add_to_basket);
    }
    
    public void clickCorporatePayment() {
    	WaitVisible(corporate_payment);
    	 jsClick(corporate_payment);
    }
    
    public void noCoporatePayment() {
    	WaitVisible(no_corporate_payment);
    	jsClick(no_corporate_payment);
    }

    public Result clickNextButton() {
    	WaitVisible(next_button);
		return ClickControl(next_button, "Next button selected");
    }
    
    public void clickCorporateNextButton() {
    	WaitVisible(corporate_next_button);
    	corporate_next_button.click();
    }
    
    public void clickCorporateConfirmButton() {
    	WaitVisible(corporate_confirm_button);
    	corporate_confirm_button.click();
    }


    public String getPageTitle() {
    	WaitVisible(page_title);
        return page_title.getText();
    }

    public void clickPreviousButton() {
    	WaitVisible(previous_button);
        previous_button.click();
    }

    public void clickCancelButton() {
    	WaitVisible(cancel_button);
        cancel_button.click();
    }

    public void processPaymentNow() {
        clickTakePaymentNow();
        clickNextButton();
    }
    
    public void payViaBasket() {
    	clickAddToBasket();
        clickNextButton();
    }
    
    public void payViaCorporate() {
    	clickCorporatePayment();
    	clickCorporateNextButton();
    	clickCorporateConfirmButton();
    }
    
    public void notPayViaCorporate() {    	
    	noCoporatePayment();
    	clickCorporateNextButton();
      }
}