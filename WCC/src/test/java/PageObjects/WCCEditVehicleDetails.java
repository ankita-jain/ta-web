package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

public class WCCEditVehicleDetails extends Base_Page {
    @FindBy(xpath = "//select[@id = 'field-Colour']")
    private WebElement colourComboBox;

    @FindBy(xpath = "//input[@name = 'submits[submit]']")
    private WebElement submitButton;

    public WCCEditVehicleDetails(WebDriver driver) {
        super(driver);
    }

    public Result chooseColour(String colour) {
        WaitVisible(colourComboBox);
        return selectItemFromDropdownByTextValue(colourComboBox, colour, "Colour select");
    }

    public Result clickSave() {
        WaitVisible(submitButton);
        return ClickControl(submitButton, "Submit button");
    }
}
