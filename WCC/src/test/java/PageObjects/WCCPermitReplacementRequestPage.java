package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WCCPermitReplacementRequestPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement replacement_request;
    @FindBy(xpath = "//*[@name='notes[text]']")
    @CacheLookup
    private WebElement replacement_request_reason;
    @FindBy(xpath = "//*[@id='field-crimeRefNum']")
    @CacheLookup
    private WebElement replacement_request_crime_ref_no;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement replacement_request_button;
    
    public WCCPermitReplacementRequestPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getReplacementRequestPagetitle() {
        WaitVisible(replacement_request);
        return replacement_request.getText();
    }

    public void enterReplacementReason(String reason) {
        WaitVisible(replacement_request_reason);
        replacement_request_reason.sendKeys(reason);
    }

    public void enterreplacementCrimRefNumber(String crime_ref) {
        WaitVisible(replacement_request_crime_ref_no);
        replacement_request_crime_ref_no.sendKeys(crime_ref);
    }

    public void clickRequestReplacement() {
        WaitVisible(replacement_request_button);
        replacement_request_button.click();
    }

    public void requestReplacementProcess(String reason_for_replacement, String crime_ref) {
        enterReplacementReason(reason_for_replacement);
        enterreplacementCrimRefNumber(crime_ref);
        clickRequestReplacement();

    }

}
