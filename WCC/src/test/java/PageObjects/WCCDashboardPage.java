package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCDashboardPage extends PageClass {

    @FindBy(xpath = "//ul[@class='error-notification']")
    private WebElement errorNotification;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
    @FindBy(xpath = "//*[contains(text(), 'View all Vehicles')]")
    private WebElement updateVehicle;
    @FindBy(xpath = "//*[contains(text(),'Edit Details')]")
    private WebElement editDetails;
    @FindBy(xpath = "//*[contains(text(),'View monthly statement')]")
    private WebElement monthlyStatement;
    @FindBy(xpath = "//*[contains(text(),'Edit Address')]")
    private WebElement editAddress;
    @FindBy(xpath = "//*[contains(text(),'View all Addresses')]")
    private WebElement allAddresses;
    @FindBy(xpath = "//*[contains(text(),'Edit Card Expiry')]")
    private WebElement editCard;
    @FindBy(xpath = "//*[contains(text(),'View all Cards')]")
    private WebElement viewAllCards;
    @FindBy(xpath = "//*[contains(text(),'Edit Vehicle')]")
    private WebElement editVehicle;
    @FindBy(xpath = "//*[contains(text(),'View all Vehicle')]")
    private WebElement viewAllVehicle;
    @FindBy(xpath = "//*[contains(text(),'View all Vehicle')]")
    private WebElement errorMessage;

    public WCCDashboardPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getErrorNotification() {
        return errorNotification.getText();
    }

    public String getDashboardPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public Result clickUpdateVehicleLink() {
        WaitVisible(updateVehicle);
        return ClickControl(updateVehicle, "Update vehicle");
    }

    public Result clickEditDetails() {
        WaitVisible(editDetails);
        return ClickControl(editDetails, "Edit Details");
    }

    public Result clickMonthlyStatement() {
        WaitVisible(monthlyStatement);
        return ClickControl(monthlyStatement, "Monthly statement");
    }

    public Result clickEditAddress() {
        WaitVisible(editAddress);
        return ClickControl(editAddress, "Edit address");
    }

    public Result clickAllAddresses() {
        WaitVisible(allAddresses);
        return ClickControl(allAddresses, "All address");
    }

    public Result clickEditCard() {
        WaitVisible(editCard);
        return ClickControl(editCard, "Edit card");
    }

    public Result clickViewAllCards() {
        WaitVisible(viewAllCards);
        return ClickControl(viewAllCards, "View all cards");
    }

    public Result clickEditVehicle() {
        WaitVisible(editVehicle);
        return ClickControl(editVehicle, "Edit vehicle");
    }

    public Result clickViewAllVehicle() {
        WaitVisible(viewAllVehicle);
        return ClickControl(viewAllVehicle, "All vehicle");
    }
    
    public void clickEditDetailsLink() {
    	 WaitVisible(editDetails);
    	 jsClick(editDetails);
    }

}
