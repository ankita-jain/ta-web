package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class CorporateManageAssets extends Base_Page {
    @FindBy(xpath = "//input[@id='add-vrn']")
    private WebElement addNumberPlateInput;

    @FindBy(xpath = "//input[@id='add-vrn-button']")
    private WebElement addNumberPlateButton;

    @FindBy(xpath = "//input[@id='add-cli']")
    private WebElement addMobileNumberInput;

    @FindBy(xpath = "//input[@id='add-cli-button']")
    private WebElement addMobileNumberButton;

    @FindBy(xpath = "//input[@id='updateButton']")
    private WebElement saveButton;

    public CorporateManageAssets(WebDriver driver) {
        super(driver);
    }

    public WebElement getAddNumberPlateButton() {
        return addNumberPlateButton;
    }

    public WebElement getAddNumberPlateInput() {
        return addNumberPlateInput;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public Result enterNumberPlate(String carVrn) {
        return enterText(addNumberPlateInput, carVrn, "Car VRN");
    }

    public Result clickAddNumberPlate() {
        return ClickControl(addNumberPlateButton, "Add car button");
    }

    public Result enterMobileNumber(String mobileNumber) {
        return enterText(addMobileNumberInput, mobileNumber, "User's mobile number");
    }

    public Result clickMobileNumberButton() {
        return ClickControl(addMobileNumberButton, "Add mobile number button");
    }

    public Result clickSaveButton() {
        return ClickControl(saveButton, "Save button");
    }
}
