package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class MyRingoCorporateLogin extends Base_Page {
    @FindBy(xpath = "//input[@id='field-Email']")
    private WebElement mobileOrEmailInput;

    @FindBy(xpath = "//input[@id='field-Password']")
    private WebElement passwordOrPinInput;

    @FindBy(xpath = "//input[@value='Log In']")
    private WebElement logInButton;

    public MyRingoCorporateLogin(WebDriver driver) {
        super(driver);
    }

    public WebElement getMobileOrEmailInput() {
        return mobileOrEmailInput;
    }

    public Result enterEmailOrMobile(String emailOrMobile) {
        return enterText(mobileOrEmailInput, emailOrMobile, "Email or mobile input");
    }

    public Result enterPasswordOrPin(String passwordOrPin) {
        return enterText(passwordOrPinInput, passwordOrPin, "Password or pin input");
    }

    public Result clickLogin() {
        return ClickControl(logInButton, "Login button");
    }

    public WebElement getPasswordOrPinInput() {
        return passwordOrPinInput;
    }

    public WebElement getLogInButton() {
        return logInButton;
    }
}