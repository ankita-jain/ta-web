package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class ConfirmDeletionAutoContract extends Base_Page {
    @FindBy(xpath = "//form[@id='form-confirm']//input[@name='submit']")
    private WebElement yesButton;

    @FindBy(xpath = "//form[@id='form-cancel']//input[@name='submit']")
    private WebElement noButton;

    public ConfirmDeletionAutoContract(WebDriver driver) {
        super(driver);
    }

    public Result clickYes() {
        WaitVisible(yesButton);
        return ClickControl(yesButton, "Yes button");
    }

    public Result clickNo() {
        WaitVisible(noButton);
        return ClickControl(noButton, "No button");
    }
}
