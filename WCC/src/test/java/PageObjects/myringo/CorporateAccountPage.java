package PageObjects.myringo;

import PageObjects.myringo.pagecomplexmodules.CorporateUpperMenu;
import PageObjects.myringo.pagecomplexmodules.LeftNavigationMenu;
import SeleniumHelpers.Base_Page;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static GenericComponents.UtilityClass.getFeatureToggleState;

public class CorporateAccountPage extends Base_Page {
    private CorporateUpperMenu upperMenu = new CorporateUpperMenu(_driver);
    private LeftNavigationMenu leftNavigationMenu = new LeftNavigationMenu(_driver);

    public CorporateAccountPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h1[contains(text(),'Welcome to your RingGo Corporate Account')]")
    private WebElement corporateWelcomeTitle;

    public Result clickOnAddEditEmployee() {
        return getFeatureToggleState().equalsIgnoreCase("on")
                ? leftNavigationMenu.clickOnEmployees()
                : upperMenu.clickAddEditEmployees();
    }

    public Result clickOnAssets() {
        return getFeatureToggleState().equalsIgnoreCase("on")
                ? leftNavigationMenu.clickOnVehicle()
                : upperMenu.clickAssetsUseLink();
    }

    public boolean isWelcomeTitleDisplayed() {
        return corporateWelcomeTitle.isDisplayed();
    }
}
