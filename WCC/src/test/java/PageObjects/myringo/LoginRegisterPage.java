package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class LoginRegisterPage extends Base_Page {
    @FindBy(xpath = "//input[@id='field-cli']")
    private WebElement mobileOrEmailInput;

    @FindBy(xpath = "//input[@id='field-Email']")
    private WebElement emailInput;

    @FindBy(xpath = "//*[@type='password']")
    private WebElement passwordOrPinInput;

    @FindBy(xpath = "//input[@type='submit']")
    private WebElement logInButton;
    
    @FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/ul/ul")
    private WebElement errorMessage;
    
    public LoginRegisterPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getMobileOrEmailInput() {
    	 WaitVisible(mobileOrEmailInput);
        return mobileOrEmailInput;
    }

    public WebElement getEmailInput() {
    	WaitVisible(emailInput);
        return emailInput;
    }

    public WebElement getPasswordOrPinInput() {
        return passwordOrPinInput;
    }
    public String getErrorMessage() {
        return errorMessage.getText();
    }

    public WebElement getLogInButton() {
        return logInButton;
    }

    public Result enterMobileOrEmail(String mobileEmail) {
        return enterText(mobileOrEmailInput, mobileEmail, "Email input");
    }

    public Result enterPassword(String password) {
        return enterText(passwordOrPinInput, password, "Password input");
    }

    public Result clickLogin() {
        return ClickControl(logInButton, "Login button");
    }
}
