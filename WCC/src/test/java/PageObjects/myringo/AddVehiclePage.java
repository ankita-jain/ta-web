package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class AddVehiclePage extends Base_Page {

    @FindBy(xpath = "//a[contains(text(), 'Change Vehicle on Session / Permit')]")
    private WebElement changeVehicleButton;

    @FindBy(xpath = "//input[@id = 'field-VRN']")
    private WebElement numberPlateInput;
    @FindBy(xpath = "//select[@id = 'field-Colour']")
    private WebElement colourComboBox;
    @FindBy(xpath = "//select[@id = 'field-Make']")
    private WebElement makeComboBox;
    @FindBy(xpath = "//select[@id = 'field-TypeShow']")
    private WebElement typeComboBox;
    @FindBy(xpath = "//select[@id = 'field-OwnershipId']")
    private WebElement ownershipTypeComboBox;

    @FindBy(xpath = "//input[@value = 'Save']")
    private WebElement saveButton;
    @FindBy(xpath = "//input[@id = 'usercancel']")
    private WebElement cancelButton;

    public AddVehiclePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getChangeVehicleButton() {
        return changeVehicleButton;
    }

    public Select getColourComboBox() {
        return new Select(colourComboBox);
    }

    public Select getMakeComboBox() {
        return new Select(makeComboBox);
    }

    public WebElement getNumberPlateInput() {
        return numberPlateInput;
    }

    public WebElement getOwnershipTypeComboBox() {
        return ownershipTypeComboBox;
    }

    public WebElement getTypeComboBox() {
        return typeComboBox;
    }

    public enum Colour {
        Unknown("Unknown"),
        Beige("Beige"),
        Black("Black"),
        Blue("Blue"),
        Bronze("Bronze"),
        Brown("Brown"),
        Cream("Cream"),
        Gold("Gold"),
        Green("Green"),
        Grey("Grey"),
        Maroon("Maroon"),
        Mustard("Mustard"),
        Orange("Orange"),
        Pink("Pink"),
        Purple("Purple"),
        Red("Red"),
        Silver("Silver"),
        TwoTone("Two-tone"),
        White("White"),
        Yellow("Yellow");

        private final String colour;

        Colour(String colour) {
            this.colour = colour;
        }

        @Override
        public String toString() {
            return colour;
        }
    }

    public enum Type {
        Car("Car"),
        Hgv("HGV"),
        Coach("Coach"),
        Motorcycle("Motorcycle"),
        MiniBus("MiniBus"),
        MotorCaravan("Motor Caravan"),
        LgvVan("LGV Van"),
        Tractor("Tractor"),
        EelectricCar("EelectricCar"),
        ElectricMC("Electric M/C"),
        HybridCar("Hybrid Car"),
        MotorHome("Motor Home"),
        Bus("Bus"),
        TraybackUte("Trayback Ute"),
        Ute("Ute"),
        Van("Van");

        private final String type;

        Type(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    public enum OwnershipType {
        PrivateOwnedVehicle("Private Owned Vehicle"),
        CompanyOwnedVehicle("Company Owned Vehicle"),
        PrivateLeaseVehicle("Private Lease Vehicle"),
        CompanyLeaseVehicle("Company Lease Vehicle"),
        ForeignCompanyVehicle("Foreign Company Vehicle"),
        ChauffeurVehicle("Chauffeur Vehicle"),
        DiplomatVehicle("Diplomat Vehicle"),
        ForeignVehicle("ForeignVehicle");

        private final String type;

        OwnershipType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }
}
