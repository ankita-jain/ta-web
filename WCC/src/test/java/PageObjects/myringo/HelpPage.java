package PageObjects.myringo;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class HelpPage extends Base_Page {

	public HelpPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//*[@href='/newenquiry?type=9']")
    private WebElement accountClosureReqLink;
	
	@FindBy(xpath = "//select[@name='selected[1]']")
    private WebElement accountClosureReason;
	
	@FindBy(xpath = "//input[@name='labyrinth_ContactAccountClose_next']")
    private WebElement nextButton;
	
	@FindBy(xpath = "//*[@id=\"ContactAccountCloseConfirm\"]/fieldset/ol/li[1]/div/div/label[2]")
    private WebElement confirmClosure;
	
	@FindBy(xpath = "//input[@name='labyrinth_ContactAccountCloseConfirm_next']")
    private WebElement confirmNextButton;
	
	
	@FindBy(xpath = "//*[@class='box-content']//p")
    private WebElement accountClosureMsg;
	
	@FindBy(xpath = "//*[@class='warning-notification']//p")
    private WebElement warningNotficationMsg;
	
	@FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/p")
    private WebElement accountClosureSuccessMsg;
	
	@FindBy(xpath = "//*[@id=\"labyrinth_finish\"]")
    private WebElement finishBtn;
	
	
	 public Result clickAccountClosureRequest() {
	        WaitVisible(accountClosureReqLink);
	        return ClickControl(accountClosureReqLink, "Account Closure Request");
	    }
	 public Result selectAccountClosureReason(String Reason) {
		 WaitVisible(accountClosureReason);
     return WebElementMethods.selectItemFromDropdownByContainsText(accountClosureReason, Reason, "RingGo is no longer in my area");
      }
	 public Result confirmClosure() {
		 WaitVisible(confirmClosure);
     return ClickControl(confirmClosure,  "Confirmation to Close Account");
      }
	 
	 public Result clickNext() {
	        WaitVisible(nextButton);
	        return ClickControl(nextButton, "Next button after AccountClosure Reason");
	 }
	 
	 public Result confirmClosureNext() {
	        WaitVisible(confirmNextButton);
	        return ClickControl(confirmNextButton, "Next button after ConfirmClosure");
	 }
	 public String getAccountClosureText() {
	        WaitVisible(accountClosureMsg);
	        return accountClosureMsg.getText();
	    }
	 public String getWarningNotficationText() {
	        WaitVisible(warningNotficationMsg);
	        return warningNotficationMsg.getText();
	    }
	 public String getAccountClosureSuccessText() {
	        WaitVisible(accountClosureSuccessMsg);
	        return accountClosureSuccessMsg.getText();
	    }
	 public Result clickFinish() {
	        WaitVisible(finishBtn);
	        return ClickControl(finishBtn, "Finish button after AccountClosure");
	 }

}
