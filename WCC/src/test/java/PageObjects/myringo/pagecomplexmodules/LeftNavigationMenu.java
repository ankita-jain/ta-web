package PageObjects.myringo.pagecomplexmodules;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class LeftNavigationMenu extends Base_Page {

    @FindBy(xpath = "//*[@href='/employees']")
    private WebElement employeesItem;

    @FindBy(xpath = "//*[@href='/assets']")
    private WebElement vehicleItem;

    public LeftNavigationMenu(WebDriver driver) {
        super(driver);
    }

    public Result clickOnEmployees() {
        WaitVisible(employeesItem);
        return ClickControl(employeesItem, "Click on Employee");
    }

    public Result clickOnVehicle() {
        return ClickControl(vehicleItem, "Click on Vehicle");
    }
}
