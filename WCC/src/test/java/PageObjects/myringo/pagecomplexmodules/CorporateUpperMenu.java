package PageObjects.myringo.pagecomplexmodules;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class CorporateUpperMenu {
    @FindBy(xpath = "//ul[@id='moremenu']//a[text()='Assets in use']")
    private WebElement assetsInUse;

    @FindBy(xpath = "//ul[@id='moremenu']//a[text()='Add or Edit Employees']")
    private WebElement addEditEmployees;

    public CorporateUpperMenu(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public Result clickAssetsUseLink() {
        return ClickControl(assetsInUse, "Assets in use link");
    }

    public Result clickAddEditEmployees() {
        return ClickControl(addEditEmployees, "Click add/edit employees");
    }
}
