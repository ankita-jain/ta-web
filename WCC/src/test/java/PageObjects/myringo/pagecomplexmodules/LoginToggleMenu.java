package PageObjects.myringo.pagecomplexmodules;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WaitMethods.WaitVisible;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByTextValue;

public class LoginToggleMenu {
    private WebDriver _driver;
    @FindBy(xpath = "//ul[@id='accountmenu']//a[text()='Personal']")
    private WebElement personalLink;
    @FindBy(xpath = "//ul[@id='accountmenu']//a[text()='Corporate']")
    private WebElement corporateLink;

    public LoginToggleMenu(WebDriver driver) {
        _driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Result clickPersonalLink() {
        WaitVisible(_driver, 60, personalLink);
        return ClickControl(personalLink, "'Personal' link");
    }

    public Result clickCorporateLink() {
        WaitVisible(_driver, 60, corporateLink);
        return !_driver.getCurrentUrl().contains("corporate") ? ClickControl(corporateLink, "'Corporate' link")
                : Result.Create(true, "Direct corporate link is used");
    }
}
