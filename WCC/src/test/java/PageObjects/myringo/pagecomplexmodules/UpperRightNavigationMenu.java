package PageObjects.myringo.pagecomplexmodules;

import static GenericComponents.UtilityClass.getFeatureToggleState;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import SeleniumHelpers.CommonProperties;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class UpperRightNavigationMenu {

    private WebDriver driver;
    private LoginToggleMenu loginToggleMenu;
    private AccountToggleMenu accountToggleMenu;

    public UpperRightNavigationMenu(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        loginToggleMenu = new LoginToggleMenu(driver);
        accountToggleMenu = new AccountToggleMenu(driver);
    }

    public LoginToggleMenu getLoginToggleMenu() {
        return loginToggleMenu;
    }

    public AccountToggleMenu getAccountToggleMenu() {
        return accountToggleMenu;
    }

    public WebElement getLoginLink() {
        String locator = getFeatureToggleState().equalsIgnoreCase("on") && driver.getCurrentUrl().contains("corporate")
                    ? "//*[@id='header-navigation']//*[@href='/corplogin']" : "//a[@id='toggleAccountMenu']";
        WebElement e = driver.findElement(By.xpath(locator));
        WaitMethods.WaitVisible(driver, CommonProperties.WebDriverWait() + 20, e);
        return e;
    }

    public Result clickLogin() {
        return ClickControl(getLoginLink(), "Login link");
    }
}
