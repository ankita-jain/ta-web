package PageObjects.myringo.pagecomplexmodules;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import PageObjects.PageClass;
import Structures.Result;

public class AccountToggleMenu extends PageClass {
    @FindBy(xpath = "//ul[@id='accountmenu']//a[@href='/vehicles']")
    private WebElement vehiclesLink;

    @FindBy(xpath = "//a[text()='Logout']")
    private WebElement logoutLink;

    @FindBy(xpath = "//ul[@id='accountmenu']//a[@href ='/autopay']")
    private WebElement autoPayLink;
    
    @FindBy(xpath = "//*[@id=\"accountmenu\"]/li[4]/a[@href ='/payment']")
    private WebElement paymentCardsLink;

    @FindBy(xpath = "//ul[@id='accountmenu']//a[@href ='/change']")
    private WebElement securityChange;

    @FindBy(xpath = "//a[text()='Account']")
    private WebElement accountLink;

    private WebDriver _driver;

    public AccountToggleMenu(WebDriver driver) {
        super(driver);
        _driver = driver;
    }

    public Result clickAccount() {
        WaitVisible(accountLink);
        return ClickControl(accountLink, "Account link");
    }

    public Result clickOnVehiclesLink() {
        WaitVisible(vehiclesLink);
        return ClickControl(vehiclesLink, "Vehicles link");
    }

    public Result clickOnLogoutLink() {
        WaitVisible(logoutLink);
        return ClickControl(logoutLink, "Logout link");
    }

    public WebElement getVehiclesLink() {
        return vehiclesLink;
    }

    public WebElement getLogoutLink() {
        return logoutLink;
    }

    public Result openAutoPayPage() {
        WaitVisible(autoPayLink);
        return ClickControl(autoPayLink, "Autopay link");
    }
    

    public Result clickPaymentCards() {
        WaitVisible(paymentCardsLink);
        return ClickControl(paymentCardsLink, "PaymentCards link");   
    }

    public Result clickSecurityOptions() {
        WaitVisible(securityChange);
        return ClickControl(securityChange, "Security options link");
    }

    public void logout() {
        WaitVisible(accountLink);
        jsClick(accountLink);
        WaitVisible(logoutLink);
        jsClick(logoutLink);
    }
}