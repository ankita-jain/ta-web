package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class BookParkingPage extends Base_Page {
    private BookBlock bookBlock = new BookBlock();
    private DurationSessionBlock durationSessionBlock = new DurationSessionBlock();
    private PayBlock payBlock = new PayBlock();
    private FinishBlock finishBlock = new FinishBlock();
    private OptionsBlock optionsBlock = new OptionsBlock();
    private PayFromCorporateBlock payFromCorporateBlock = new PayFromCorporateBlock();
    private PayCorporateConfirmBlock payCorporateConfirmBlock = new PayCorporateConfirmBlock();

    public BookParkingPage(WebDriver driver) {
        super(driver);
    }

    public OptionsBlock getOptionsBlock() {
        return optionsBlock;
    }

    public FinishBlock getFinishBlock() {
        return finishBlock;
    }

    public PayBlock getPayBlock() {
        return payBlock;
    }

    public BookBlock getBookBlock() {
        return bookBlock;
    }

    public DurationSessionBlock getDurationSessionBlock() {
        return durationSessionBlock;
    }

    public PayFromCorporateBlock getPayFromCorporateBlock() {
        return payFromCorporateBlock;
    }

    public PayCorporateConfirmBlock getPayCorporateConfirmBlock() {
        return payCorporateConfirmBlock;
    }
    
    public Result clickAuthenticated() {
    	// need to switch to the iframe in order to click the Authenticated button for a 3DS card
    	WebElement authenticatedFrame = WaitMethods.WaitVisible(_driver, 10, By.cssSelector("iframe[src='/3dsenrollpermits']"));
    	_driver.switchTo().frame(0);
    	WebElement authenticatedButton = WaitMethods.WaitVisible(_driver, 5, By.xpath("//input[@name='choice']"));
        Result result =  ClickControl(authenticatedButton, "Authenticated button");
    	_driver.switchTo().parentFrame();
    	return result;
    }

    public class OptionsBlock {
        @FindBy(xpath = "//input[contains(@value,'Book New')]")
        private WebElement bookNewButton;

        @FindBy(xpath = "//a[contains(@value,'Extend')]")
        private WebElement extendButton;

        private OptionsBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getBookNewButton() {
            return bookNewButton;
        }

        public WebElement getExtendButton() {
            return extendButton;
        }

        public Result clickNew() {
        	return ClickControl(bookNewButton, "New button");
        }
        
        public Result clickExtend() {
            return ClickControl(extendButton, "Extend button");
        }
    }

    public class BookBlock {
        @FindBy(xpath = "//input[@id='zone']")
        private WebElement zoneInput;

        @FindBy(xpath = "//input[contains(@value,'Next')]")
        private WebElement nextButton;

        @FindBy(xpath = "//label[@for='timescale']")
        private WebElement whendDoYouParkText;
        @FindBy(xpath = "//form[@id='AdvanceParkingOption']//input[@type='radio' and @value='1']")
        private WebElement bookNowRadioButton;

        @FindBy(xpath = "//select[@id='vehicleDropDown']")
        private WebElement vehicleSelector;

        private BookBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getVehicleSelector() {
            return vehicleSelector;
        }

        public WebElement getWhendDoYouParkText() {
            return whendDoYouParkText;
        }

        public WebElement getZoneInput() {
            return zoneInput;
        }

        public WebElement getNextButton() {
            return nextButton;
        }

        public WebElement getBookNowRadioButton() {
            return bookNowRadioButton;
        }

        public Result enterZone(String zone) {
            return enterText(zoneInput, zone, "Zone input");
        }

        public Result selectVRN(String carVrn) {
            return selectItemFromDropdownByOptionValue(vehicleSelector, carVrn, "Vehicle selector");
        }

        public Result clickNext() {
            return ClickControl(nextButton, "Next button");
        }

        public Result selectNowRadioButton() {
            return ClickControl(bookNowRadioButton, "Now radio button");
        }
    }

    public class DurationSessionBlock {
        @FindBy(xpath = "//select[@id='tariffdropdown']")
        private WebElement durationTimeDropdown;

        @FindBy(xpath = "//label[@id='label-tariffdropdown']")
        private WebElement howLongWantParkLabel;
        @FindBy(xpath = "//input[contains(@value,'Next')]")
        private WebElement nextButton;

        private DurationSessionBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getHowLongWantParkLabel() {
            return howLongWantParkLabel;
        }

        public WebElement getDurationTimeDropdown() {
            return durationTimeDropdown;
        }

        public WebElement getNextButton() {
            return nextButton;
        }

        public Result selectDuration(String duration) {
            return selectItemFromDropdownByTextValue(durationTimeDropdown, duration, "Duration dropdown");
        }

        public Result clickNext() {
            return ClickControl(nextButton, "Next button");
        }
    }

    public class PayBlock {
        @FindBy(xpath = "//input[contains(@value,'Next')]")
        private WebElement nextButton;

        @FindBy(xpath = "//label[@id='label-priceLabel']/following-sibling::div//div[@class='elemErrorWrap']")
        private WebElement priceLabel;

        @FindBy(xpath = "//a[@class='ringgo-tooltip-click']")
        private WebElement linkToPopUp;


        @FindBy(xpath = "//label[@id='label-priceLabel']")
        private WebElement priceLabelTxt;

        @FindBy(xpath = "//input[contains(@value,'Pay')]")
        private WebElement payButton;
        
                
        @FindBy(xpath = "//input[@name='securityCode']")
        private WebElement cv2CodeInput;
        
        @FindBy(xpath = "//*[@id='paymentConfirmButton']")
        private WebElement pay_confirmation_button;

        @FindBy(xpath = "//span[@class='tooltiptext']")
        private WebElement popUpWindow;
        
        @FindBy(xpath = "//*[@id=\"Payment\"]/fieldset/ol/li[3]/div/div/strong")
        private WebElement walletText;
        
        private PayBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getPriceLabelTxt() {
            return priceLabelTxt;
        }
        
        public String getWalletText() {
        	return walletText.getText();
        }

        public WebElement getPopUpWindow() {
            WaitVisible(popUpWindow);

            return popUpWindow;
        }

        public WebElement getLinkToPopUp() {
            return linkToPopUp;
        }

        public WebElement getPayButton() {
            return payButton;
        }
        
        public WebElement getPayButtonConfirmation() {
            return pay_confirmation_button;
        }

        public WebElement getNextButton() {
            return nextButton;
        }

        public WebElement getCv2CodeInput() {
            return cv2CodeInput;
        }

        public WebElement getPriceLabel() {
            return priceLabel;
        }

        public Result clickDieselSurcharge() {
            return ClickControl(linkToPopUp, "Diesel surcharge link");
        }

        public Result clickNext() {
            return ClickControl(nextButton, "Next button");
        }

        public Result enterCvvCode(String CVV2_Code) {
        	WaitVisible(cv2CodeInput);
            return enterText(cv2CodeInput, CVV2_Code, "CVV code input");
        }

        public Result clickPay() {
        	WaitVisible(payButton);
            return ClickControl(payButton, "Pay button");
        }
        
        public Result clickPayConfirmation() {
        	WaitVisible(pay_confirmation_button);
            return ClickControl(pay_confirmation_button, "Pay button");
        }
        
    }

    public class FinishBlock {
        @FindBy(xpath = "//input[contains(@value,'Finish')]")
        private WebElement finishButton;

        private FinishBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getFinishButton() {
            return finishButton;
        }

        public Result clickFinish() {
        	WaitMethods.WaitVisible(_driver, 5, finishButton);
            return ClickControl(finishButton, "Finish button");
        }
    }

    public class PayFromCorporateBlock {
    	@FindBy(xpath = "//*[contains(text(),'Would you like to pay this on your Corporate Account: ')]")
    	private WebElement corporate_payment_text;
        @FindBy(xpath = "//input[@name = 'corporate' and @value = '1']")
        private WebElement yesRadioButton;
        @FindBy(xpath = "//input[contains(@value,'Next')]")
        private WebElement nextButton;

        private PayFromCorporateBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getYesRadioButton() {
            return yesRadioButton;
        }
        
        public WebElement isCorporatePaymentVisible() {
        	return corporate_payment_text;
        }

        public WebElement getNextButton() {
            return nextButton;
        }
    }

    public class PayCorporateConfirmBlock {
        @FindBy(xpath = "//input[contains(@value,'Next')]")
        private WebElement nextButton;
        @FindBy(xpath = "//a[@class='ringgo-tooltip-click']")
        private WebElement linkToPopUp;
        @FindBy(xpath = "//p[contains(text(), 'Cost:')]")
        private WebElement costLabel;
        @FindBy(xpath = "//span[@class='tooltiptext']")
        private WebElement popUpWindow;

        private PayCorporateConfirmBlock() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getLinkToPopUp() {
            return linkToPopUp;
        }

        public WebElement getCostLabel() {
            return costLabel;
        }

        public WebElement getNextButton() {
            return nextButton;
        }

        public WebElement getPopUpWindow() {
        	 WaitVisible(popUpWindow);
            return popUpWindow;
        }
    }
}

