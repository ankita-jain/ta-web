package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.*;

public class AutoPayRegisterContract extends Base_Page {
    private String vehicleNameCheckBoxPattern = "vehicles[%s]";

    @FindBy(xpath = "//input[@name='termsAndConditions']")
    private WebElement termsAndConditionsCheckBox;

    @FindBy(xpath = "//select[@id='field-operator']")
    private WebElement operatorSelect;

    @FindBy(xpath = "//input[@value='Register']")
    private WebElement registerButton;

    @FindBy(xpath = "//li[contains(@class, 'checkbox')]//span[@class='error']")
    private WebElement errorConfirmTermsAndConditions;

    @FindBy(xpath = "//input[contains(@name, 'vehicles')]")
    private List<WebElement> vehicleCheckBoxes;

    public AutoPayRegisterContract(WebDriver driver) {
        super(driver);
    }

    public Result chooseOperator(String operator) {
        WaitVisible(operatorSelect);
        return selectItemFromDropdownByTextValue(operatorSelect, operator, "Operator Select");
    }

    public Result register() {
        WaitVisible(registerButton);
        return ClickControl(registerButton, "Register button");
    }

    public boolean containsOperator(String operator) {
        WaitVisible(operatorSelect);
        return dropdownContainsTextValue(operatorSelect, operator);
    }

    public Result tickVehicle(String vrn) {
        WebElement vehicle = vehicleCheckBoxes
                .stream()
                .filter(x -> x.getAttribute("name").equals(String.format(vehicleNameCheckBoxPattern, vrn)))
                .findFirst()
                .get();
        WaitVisible(vehicle);
        return checkCheckBox(vehicle, String.format("Vehicle -> %s CheckBox", vrn));
    }

    public Result tickTermsAndConditions() {
        WaitVisible(termsAndConditionsCheckBox);
        return checkCheckBox(termsAndConditionsCheckBox, "Terms and Conditions CheckBox");
    }

    public String getTermsAndConditionsErrorText() {
        return errorConfirmTermsAndConditions.getText();
    }

    public enum Operator {
        CHILTERN_RAILWAYS("Chiltern Railways"),
        WEST_MINSTER_CITY_COUNCI("Westminster City Council");

        String operatorValue;

        Operator(String header) {
            operatorValue = header;
        }

        @Override
        public String toString() {
            return operatorValue;
        }
    }
}