package PageObjects.myringo.mobile;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class RingoMobileBookParkingSession extends Base_Page {
    @FindBy(xpath = "//input[@id='zone']")
    private WebElement zoneInput;

    @FindBy(xpath = "//select[@id='vehicleDropDown']")
    private WebElement vehicleSelect;

    @FindBy(xpath = "//input[@name='labyrinth_ZoneVehicleSelect_next']")
    private WebElement nextButton;

    public RingoMobileBookParkingSession(WebDriver driver) {
        super(driver);
    }

    public Result enterZone(String zoneid) {
        WaitVisible(zoneInput);
        return enterText(zoneInput, zoneid, "Zone input");
    }

    public Result selectVehicle(String vehicle) {
        WaitVisible(vehicleSelect);
        return selectItemFromDropdownByContainsText(vehicleSelect, vehicle, "Vehicle select");
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }
}
