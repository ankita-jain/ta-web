package PageObjects.myringo.mobile;

import SeleniumHelpers.Base_Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RingoMobileStatements extends Base_Page {
    private By amountBy = By.xpath("./li[5]");

    @FindBy(xpath = "//ul[contains(@class, 'activesession')]")
    private List<WebElement> sessionList;

    public RingoMobileStatements(WebDriver driver) {
        super(driver);
    }

    public String getPrice(int sessionIndex) {
        return sessionList.get(sessionIndex).findElement(amountBy).getText().replaceAll("[\\s+a-zA-Z]+:", "");
    }
}
