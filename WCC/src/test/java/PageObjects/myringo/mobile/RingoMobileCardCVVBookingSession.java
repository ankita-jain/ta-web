package PageObjects.myringo.mobile;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class RingoMobileCardCVVBookingSession extends Base_Page {
    @FindBy(xpath = "//input[@id='field-cv2']")
    private WebElement cv2Input;
    
    @FindBy(xpath = "//input[@name='securityCode']")
    private WebElement cvvInput;
    
    @FindBy(xpath = "//*[@id='paymentConfirmButton']")
    private WebElement pay_amount_button;

    @FindBy(xpath = "//input[@name='labyrinth_Payment_next']")
    private WebElement payButton;

    @FindBy(xpath = "//*[@id='tandcCheck']")
    private WebElement termsAndConditionCheckBox;

    public RingoMobileCardCVVBookingSession(WebDriver driver) {
        super(driver);
    }

    public Result enterCV2(String cvv) {
        WaitVisible(cv2Input);
        return enterText(cv2Input, cvv, "CVV2 input");
    }

    public Result pay() {
        WaitVisible(payButton);
        return ClickControl(payButton, "Pay button");
    }
    
    public Result enterCVV(String cvv) {
        WaitVisible(cvvInput);
        return enterText(cvvInput, cvv, "CVV input");
    }

    public Result tickTermsAndConditions() {
        WaitVisible(termsAndConditionCheckBox);
        return ClickControl(termsAndConditionCheckBox, "Terms and Conditions check box");
    }
    
    public Result clickPay() {
        WaitVisible(pay_amount_button);
        return ClickControl(pay_amount_button, "Click Pay button");
    }
}
