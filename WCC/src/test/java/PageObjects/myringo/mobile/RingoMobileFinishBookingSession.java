package PageObjects.myringo.mobile;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class RingoMobileFinishBookingSession extends Base_Page {
    @FindBy(xpath = "//input[@name='labyrinth_Finish_next']")
    private WebElement finishButton;

    public RingoMobileFinishBookingSession(WebDriver driver) {
        super(driver);
    }

    public Result finish() {
        WaitVisible(finishButton);
        return ClickControl(finishButton, "Finish button");
    }
}
