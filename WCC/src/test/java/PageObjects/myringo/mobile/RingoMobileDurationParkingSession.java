package PageObjects.myringo.mobile;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class RingoMobileDurationParkingSession extends Base_Page {
    @FindBy(xpath = "//*[@name='labyrinth_Duration_next']")
    private WebElement nextButton;

    @FindBy(xpath = "//*[@name='SMSEnd']")
    private WebElement reminderSmsCheckBox;

    @FindBy(xpath = "//*[@name='SMSConfirm']")
    private WebElement smsConfirmCheckbox;

    @FindBy(xpath = "//*[@id='tariffdropdown']")
    private WebElement durationSelect;

    public RingoMobileDurationParkingSession(WebDriver driver) {
        super(driver);
    }

    public Result selectDuration(String time) {
        WaitVisible(durationSelect);
        return selectItemFromDropdownByTextValue(durationSelect, time, "Duration select");
    }

    public Result tickSMSConfirm() {
        WaitVisible(smsConfirmCheckbox);
        return checkCheckBox(smsConfirmCheckbox, "SMS confirm checkbox");
    }

    public Result tickReminderSMS() {
        WaitVisible(reminderSmsCheckBox);
        return checkCheckBox(reminderSmsCheckBox, "SMS reminder checkbox");
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }
}
