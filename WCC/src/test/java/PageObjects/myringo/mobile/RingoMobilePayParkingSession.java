package PageObjects.myringo.mobile;

import PageObjects.WCCPayParkingSession;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByContainsText;

public class RingoMobilePayParkingSession extends Base_Page {
    @FindBy(xpath = "//a[contains(@class, 'ringgo-tooltip-click')]")
    private WebElement fuelTypeSurchargeLink;

    @FindBy(xpath = "//a[contains(@class, 'ringgo-tooltip-click')]/span")
    private WebElement diesel_surcharge_tool_tip;

    @FindBy(xpath = "//input[@name='cardType[type]']")
    private List<WebElement> cardTypes;

    @FindBy(xpath = "//select[@id='field-cardSelect']")
    private WebElement cardSelect;

    @FindBy(xpath = "//div[@class='element div-priceLabel']")
    private WebElement boolParkingSessionPriceSection;

    @FindBy(xpath = "//input[@name='labyrinth_CardSelect_next']")
    private WebElement nextButton;

    public RingoMobilePayParkingSession(WebDriver driver) {
        super(driver);
    }

    public Result clickFuelTypeSurchargeLink() {
        WaitVisible(fuelTypeSurchargeLink);
        return ClickControl(fuelTypeSurchargeLink, "Fuel type price adjustment link");
    }

    public String getFuelTypeSurchargeTooltipText() {
        WaitVisible(diesel_surcharge_tool_tip);
        return diesel_surcharge_tool_tip.getText().replaceAll("\n", " ");
    }

    public Result chooseExistingCard() {
        return ClickControl(cardTypes.get(WCCPayParkingSession.CardPayType.EXISTING_CARD.value()), "Existing card radio button");
    }

    public Result selectCard(String card) {
        WaitVisible(cardSelect);
        return selectItemFromDropdownByContainsText(cardSelect, card, "Card select");
    }

    public String getPrice() {
        WaitVisible(boolParkingSessionPriceSection);
        return boolParkingSessionPriceSection.getText().replaceAll("[\\s+a-zA-Z]+", "");
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }

    public enum CardPayType {
        EXISTING_CARD(0),
        NEW_CARD(1);

        private int index;

        CardPayType(int index) {
            this.index = index;
        }

        public int value() {
            return index;
        }
    }
}
