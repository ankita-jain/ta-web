package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ContactDetailsPage extends Base_Page {
    @FindBy(xpath = "//input[@name='Member_CLI']")
    private WebElement mobileNumberInput;

    public ContactDetailsPage(WebDriver driver) {
        super(driver);
    }

    public String getMobileNumber() {
        WaitVisible(mobileNumberInput);
        return mobileNumberInput.getAttribute("value");
    }
}