package PageObjects.myringo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class PaymentCardCVVPage extends PageClass{

	public PaymentCardCVVPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(_driver, this);
	}
	
	@FindBy(xpath = "//*[@name='securityCode']")
    private WebElement CVV;
	@FindBy(xpath = "//*[@id='paymentConfirmButton']")
    private WebElement pay_amount_button;
	
	
	public Result enterCVVCode(String cvv)
    {
		WaitVisible(CVV);
		return enterText(CVV, cvv, "Etner the CVV");
    }
	
	public Result clickPayAmount()
	{
		WaitVisible(pay_amount_button);
		return ClickControl(pay_amount_button, "Click pay button");
	 }
	
	public void finishPayingForPermit(String cvv) {
		enterCVVCode(cvv);
		clickPayAmount();
	}
}
