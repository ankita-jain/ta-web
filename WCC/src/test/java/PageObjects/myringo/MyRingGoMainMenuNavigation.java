package PageObjects.myringo;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class MyRingGoMainMenuNavigation {
    @FindBy(xpath = "//ul[contains(@class, 'mainmenu')]//a[@href='/bookparking']")
    private WebElement parkMenuLink;

    public MyRingGoMainMenuNavigation(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getParkMenuLink() {
        return parkMenuLink;
    }

    public Result clickPark() {
        return ClickControl(parkMenuLink, "Park button");
    }
}
