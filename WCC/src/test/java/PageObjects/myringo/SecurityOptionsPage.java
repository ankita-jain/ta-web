package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class SecurityOptionsPage extends Base_Page {
    @FindBy(id = "field-Member_PIN")
    private WebElement oldPinInput;
    @FindBy(id = "field-Member_PIN_new")
    private WebElement newPinInput;
    @FindBy(id = "field-Member_PIN_confirm")
    private WebElement newPinConfirmInput;
    @FindBy(name = "submits[submitPIN]")
    private WebElement saveButon;


    public Result enterOldPin(String oldPin) {
        return enterText(oldPinInput, oldPin, "Old pin input");
    }

    public Result enterNewPin(String newPin) {
        return enterText(newPinInput, newPin, "New pin input");
    }

    public Result enterNewPinConfirm(String newPin) {
        return enterText(newPinConfirmInput, newPin, "Confirm new pin input");
    }

    public Result clicksaveButton() {
        return ClickControl(saveButon, "Save button");
    }

    public SecurityOptionsPage(WebDriver driver) {
        super(driver);
    }
}
