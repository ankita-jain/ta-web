package PageObjects.myringo;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class WalletsPage extends Base_Page {

	public WalletsPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/p/a[1]")
    private WebElement createWalletLink;
	
	@FindBy(xpath = "//*[@id=\"templet-container\"]/li[2]/div/p/a[2]")
    private WebElement topupWalletLink;
	
	@FindBy(xpath = "//*[@id='field-wallet']")
	private WebElement walletOperator;
	
	@FindBy(xpath = "//*[@name='wallet[1]']")
	private WebElement walletType;
	
	@FindBy(xpath = "//*[@name='wallet[2]']")
	private WebElement walletSessionType;
	
	@FindBy(xpath = "//*[@id='labyrinth_Start_next']")
	private WebElement nextButton;
	
	@FindBy(xpath = "//*[@id='labyrinth_Confirm_next']")
	private WebElement createButton;
	
	@FindBy(xpath = "//*[@id='labyrinth_finish']")
	private WebElement finishButton;
	
	@FindBy(xpath = "//*[@id='labyrinth_CardSelect_next']")
	private WebElement cardSelectNextButton;
	
	@FindBy(xpath = "//*[@id='labyrinth_Confirm_next']")
	private WebElement payButton;
	
	public Result clickCreateWallet() {
        WaitVisible(createWalletLink);
        return ClickControl(createWalletLink, "Create Wallet Request");
    }
	
	public Result clickTopUpWallet() {
        WaitVisible(topupWalletLink);
        return ClickControl(topupWalletLink, "TopUp Wallet Request");
    }
	
	public Result selectWalletOperator(String wallet) {
		WaitVisible(walletOperator);
		return WebElementMethods.selectItemFromDropdownByContainsText(walletOperator, wallet, "Winchester City Council");
	}
	
	public Result selectWalletType(String type) {
		WaitVisible(walletType);
		return WebElementMethods.selectItemFromDropdownByContainsText(walletType, type, "Cash");
	}
	
	public Result selectWalletSessionType(String session) {
		WaitVisible(walletSessionType);
		return WebElementMethods.selectItemFromDropdownByContainsText(walletSessionType, session, "All");
	}
	
	 public Result clickNext() {
	        WaitVisible(nextButton);
	        return ClickControl(nextButton, "Next button after Wallet Creation");
	 }
	 
	 public Result clickCreate() {
	        WaitVisible(createButton);
	        return ClickControl(createButton, "Create button after Wallet Creation");
	 }
	 
	 public Result clickFinish() {
	        WaitVisible(finishButton);
	        return ClickControl(finishButton, "Finish button after Wallet Creation");
	 }
	 
	 public Result clickCardSelectNext() {
	        WaitVisible(cardSelectNextButton);
	        return ClickControl(cardSelectNextButton, "Select Card after Wallet TopUp");
	 }
	 
	 public Result clickPayButton() {
		 	WaitVisible(payButton);
	        return ClickControl(payButton, "Pay the default charge - no CV2 needed");
	 }
}
