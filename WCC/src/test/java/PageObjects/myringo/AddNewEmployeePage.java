package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class AddNewEmployeePage extends Base_Page {
    @FindBy(xpath = "//input[@id='FirstName']")
    private WebElement firstName;
    @FindBy(xpath = "//input[@id='Surname']")
    private WebElement surname;
    @FindBy(xpath = "//input[@name='SendWelcomeEmail']")
    private WebElement sendWelcomeEmail;
    @FindBy(xpath = "//input[@id='new-vrn']")
    private WebElement addNewVehicleInput;
    @FindBy(xpath = "//input[@id='add-vrn']")
    private WebElement addNewVehicleButton;
    @FindBy(xpath = "//input[@id='new-cli']")
    private WebElement addNewMobileInput;
    @FindBy(xpath = "//input[@id='add-cli']")
    private WebElement addNewMobileButton;

    @FindBy(xpath = "//input[@id='save-button']")
    private WebElement saveButton;

    public AddNewEmployeePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getAddNewVehicleButton() {
        return addNewVehicleButton;
    }

    public WebElement getAddNewVehicleInput() {
        return addNewVehicleInput;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public WebElement getSendWelcomeEmail() {
        return sendWelcomeEmail;
    }

    public WebElement getSurname() {
        return surname;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public Result clickSaveButton() {
        return ClickControl(saveButton, "Save button");
    }

    public Result enterFirstName(String firstNameValue) {
        return enterText(firstName, firstNameValue, "First name input");
    }

    public Result enterSurname(String surnameValue) {
        return enterText(surname, surnameValue, "Surname input");
    }

    public Result addNewCarInput(String carVrn) {
        return enterText(addNewVehicleInput, carVrn, "Car vrn");
    }

    public Result clickAddNewVehicle() {
        return ClickControl(addNewVehicleButton, "Add new vehicle button");
    }


    public Result clickAddNewMobile() {
        return ClickControl(addNewMobileButton, "Add new mobile button");
    }

    public Result addAddNewMobileInput(String mobile) {
        return enterText(addNewMobileInput, mobile, "User's mobile");
    }

    public Result uncheckSendWelcomeEmail() {
        return UncheckCheckBox(sendWelcomeEmail, "Send welcome email checkbox");
    }

}
