package PageObjects.myringo;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import Structures.Result;

public class AutoPayContracts extends Base_Page {
    private By moreVehiclesTooltipLink = By.xpath("//a[@class='ringgo-tooltip']");

    @FindBy(xpath = "//div[@class='box-content']//a[@class='button']")
    private WebElement addContractLink;

    @FindBy(xpath = "//table[@id='dgridtable1']")
    private WebElement contactsTable;

    @FindBy(xpath = "//span[@class='tooltiptext']")
    private WebElement vehiclesTooltip;

    @FindBy(xpath = "//ul[@class='error-notification']/p")
    private WebElement errorNotification;

    private Table table;

    public AutoPayContracts(WebDriver driver) {
        super(driver);
        table = new Table(contactsTable);
    }

    public Result addContract() {
        WaitVisible(addContractLink);
        return ClickControl(addContractLink, "Add Contract Link");
    }

    public Result showMoreVehicles(int index) {
        WaitVisible(moreVehiclesTooltipLink);
        WebElement moreVehicleLink = table.getCell(index, ContractTable.VEHICLES);
        return ClickControl(moreVehicleLink.findElement(moreVehiclesTooltipLink), "More Vehicles link");
    }

    public String getVehiclesTooltipText() {
        WaitVisible(vehiclesTooltip);
        return vehiclesTooltip.getText();
    }

    public Result deleteContract(int index) {
        WaitVisible(contactsTable);
        WebElement editLink = table.getCell(index, ContractTable.DELETE);
        return ClickControl(editLink, "Delete button");
    }

    public int getContractsCount() {
        boolean isExist = WaitVisible(contactsTable);
        return isExist ? table.getRows().size() : 0;
    }

    public String getErrorNotificationText() {
        WaitVisible(errorNotification);
        return errorNotification.getText();
    }

    public enum ContractTable {
        OPERATOR("Operator"),
        VEHICLES("Vehicles"),
        ADDED("Added On"),
        DELETE("Delete");

        String headerValue;

        ContractTable(String header) {
            headerValue = header;
        }

        @Override
        public String toString() {
            return headerValue;
        }
    }
}