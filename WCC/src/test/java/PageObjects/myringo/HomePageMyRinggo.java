package PageObjects.myringo;

import GenericComponents.complexwebobjects.Table;
import PageObjects.myringo.pagecomplexmodules.LeftNavigationMenu;
import PageObjects.myringo.pagecomplexmodules.UpperRightNavigationMenu;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class HomePageMyRinggo extends Base_Page {
    private UpperRightNavigationMenu upperRightNavigationMenu = new UpperRightNavigationMenu(_driver);
    private MyRingGoMainMenuNavigation myRingGoMainMenuNavigation = new MyRingGoMainMenuNavigation(_driver);
    private ActiveSessionsBlock activeSessionsBlock = new ActiveSessionsBlock();
    private MyDetailsBlock myDetailsBlock = new MyDetailsBlock();

    @FindBy(xpath = "//button[text()='Accept']")
    private WebElement acceptButton;
    
    @FindBy(xpath = "//*[@id=\"header\"]/div[1]/div/ul/li[1]/a")
    private WebElement helplink;
    
    @FindBy(xpath = "//*[@class='error']")
    private WebElement errormsg;
    
    @FindBy(xpath = "//*[@id=\"toggleMore\"]")
    private WebElement moreToggle;
    
    @FindBy(xpath = "//*[@id=\"moremenu\"]/li[3]/ul/li[3]/a")
    private WebElement walletslink;
    

    public HomePageMyRinggo(WebDriver driver) {
        super(driver);
    }
    
    public  Result clickHelpLink() {
    	 WaitVisible(helplink);
        return ClickControl(helplink, "'Help' link");
    }
    
    public  Result clickWalletsLink() {
   	   WaitVisible(moreToggle);
       ClickControl(moreToggle, "More Toggle");
       WaitVisible(walletslink);
       return ClickControl(walletslink, "'Wallets' link");
       }
    
    public String getErrorMessageText() {
        WaitVisible(errormsg);
        return errormsg.getText();
    }

    public ActiveSessionsBlock getActiveSessionsBlock() {
        return activeSessionsBlock;
    }

    public UpperRightNavigationMenu getUpperRightNavigationMenu() {
        return upperRightNavigationMenu;
    }

    public MyRingGoMainMenuNavigation getMyRingGoMainMenuNavigation() {
        return myRingGoMainMenuNavigation;
    }

    public MyDetailsBlock getMyDetailsBlock() {
        return myDetailsBlock;
    }

    public Result clickOnAcceptButton() {
        WaitVisible(acceptButton);
        return ClickControl(acceptButton, "Accept button");
    }

    
        
    public enum ActiveSessionHeaders {
        BOOKED("Booked"),
        DESCRIPTION("Description"),
        AMOUNT("Amount");
        String headerValue;

        ActiveSessionHeaders(String header) {
            headerValue = header;
        }

        @Override
        public String toString() {
            return headerValue;
        }
    }

    public class ActiveSessionsBlock {

        @FindBy(xpath = "//table[@id='dgridtable1']")
        private WebElement tableElement;

        public ActiveSessionsBlock() {
            PageFactory.initElements(_driver, this);
        }

        public Table getTable() {
            return new Table(tableElement);
        }
    }

    public class MyDetailsBlock {
        @FindBy(xpath = "//p[contains(text(),'My Details')]/following-sibling::ul/li[1]")
        private WebElement mobileNumberText;

        @FindBy(xpath = "//a[text()='Edit Details']")
        private WebElement editDetails;

        public WebElement getMobileNumberText() {
            WaitVisible(mobileNumberText);
            return mobileNumberText;
        }

        public Result clickOnEditDetails(){
            WaitVisible(editDetails);
            return ClickControl(editDetails, "Click in edit details!");
        }

        public MyDetailsBlock () {
            PageFactory.initElements(_driver, this);
        }
    }

}
