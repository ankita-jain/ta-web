package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ChangeVehiclesOnYourPermitSessionPage extends Base_Page {
    @FindBy(xpath = "//select[@id='field-permit']")
    private WebElement sessionSelect;
    @FindBy(xpath = "//input[@value='Next']")
    private WebElement nextButton;
    @FindBy(xpath = "//select[@id='field-ringgovehicles']")
    private WebElement myVehiclesSelect;
    @FindBy(xpath = "//*[@class='error-notification']")
    private WebElement errorMessage;
    @FindBy(xpath = "//*[@class='box-content']")
    private WebElement textBox;

    public ChangeVehiclesOnYourPermitSessionPage(WebDriver driver) {
        super(driver);
    }

    public Result selectSessionFromDropdown(String sessionIdentifier) {
        WaitVisible(sessionSelect);
        return WebElementMethods.selectItemFromDropdownByContainsText(sessionSelect, sessionIdentifier, "Session select");
    }

    public Result clickNextButton() {
        WaitVisible(nextButton);
        return WebElementMethods.ClickControl(nextButton, "Next button");
    }

    public Result selectVehicleFromDropdown(String vRN) {
        WaitVisible(myVehiclesSelect);
        return WebElementMethods.selectItemFromDropdownByContainsText(myVehiclesSelect, vRN, "My Vehicles select");
    }

    public String getErrorMessageText() {
        WaitVisible(errorMessage);
        return errorMessage.getText();
    }

    public String getTextFromPage() {
        WaitVisible(textBox);
        return textBox.getText();
    }
}
