package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyRingGoVehiclesPage extends Base_Page {
    @FindBy(xpath = "//a[contains(text(), 'Add a Vehicle')]")
    private WebElement addVehilcleButton;
    @FindBy(xpath = "//a[contains(text(), 'Change Vehicle on Session')]")
    private WebElement changeVehicleOnSessionPermit;

    public MyRingGoVehiclesPage(WebDriver driver) {
        super(driver);
    }

    public Result clickChangeVehicleOnSessionPermit() {
        return WebElementMethods.ClickControl(changeVehicleOnSessionPermit, "Change Vehicle on Session // Permit link");
    }
}