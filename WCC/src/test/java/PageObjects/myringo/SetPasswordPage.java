package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class SetPasswordPage extends Base_Page {
    @FindBy(id = "field-ringpass")
    private WebElement newPassword;

    @FindBy(id = "field-confirmringpass")
    private WebElement confirmNewPassword;

    @FindBy(id = "labyrinth_NewPassword_next")
    private WebElement nextButton;

    public SetPasswordPage(WebDriver driver) {
        super(driver);
    }

    public Result enterNewPassword(String password) {
        WaitVisible(newPassword);
        return enterText(newPassword, password, "New password input");
    }

    public Result enterConfirmNewPassword(String password) {
        WaitVisible(confirmNewPassword);
        return enterText(confirmNewPassword, password, "Confirm new password input");
    }

    public Result clickNextButton() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Click 'Next button'");
    }

}
