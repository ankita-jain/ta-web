package PageObjects.myringo;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

import static SeleniumHelpers.WaitMethods.WaitVisible;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class PaymentCards extends Base_Page{

	 @FindBy(xpath = "//*[@id=\"1\"]/td[6]/p/a/img[@title = 'Edit']")
	    private WebElement editPaymentCard;   
	 
	 @FindBy(xpath = "//input[@name='submits[submit]']")
	 private WebElement savePaymentCard;
	 
	 @FindBy(xpath = "//*[@class='error-notification']/p")
	 private WebElement errorMsg;
	 
	public PaymentCards(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	 public Result clickEdit() {
		 WaitVisible(editPaymentCard);
	        return ClickControl(editPaymentCard, "Edit");
	    }
	 public Result clickSave() {
		 WaitVisible(savePaymentCard);
	        return ClickControl(savePaymentCard, "Save");
	    }
	 public String getErrorMessageText() {
	        WaitVisible(errorMsg);
	        return errorMsg.getText();
	    }

}
