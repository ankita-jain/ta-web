package PageObjects.myringo;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class CorporateEmployeesPage extends Base_Page {
    @FindBy(xpath = "//button[text()='Add a new employee']")
    private WebElement addNewEmployee;

    public CorporateEmployeesPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getAddNewEmployee() {
        return addNewEmployee;
    }


    public Result clickAddNewEmployee() {
        return ClickControl(addNewEmployee, "Add new employee button");
    }
}
