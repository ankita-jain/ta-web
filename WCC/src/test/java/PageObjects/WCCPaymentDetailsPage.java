package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCPaymentDetailsPage extends PageClass {

    private String paymentCardExpDate = "//tr//p[contains(text(), '%s')]//following::td[1]";
    private String paymentCardEditButtonPath = "//tr//p[contains(text(), '%s')]//following::td[2]";
    private String paymentCardDeleteButtonPath = "//tr//p[contains(text(), '%s')]//following::td[3]";
    private String paymentCardNumber = "//tr//p[contains(text(), '%s')]";
    @FindBy(xpath = "//ul[@class='error-notification']")
    private WebElement errorNotification;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement paymentPageTitle;
    @FindBy(className="gp-card-trigger")
    private WebElement addNewCard;
    @FindBy(xpath = "//*[@class='success-notification']")
    private WebElement successNotification;
    @FindBy(xpath = "/html/body/div[15]/div[11]/button[2]/span")
    private WebElement deleteConfirmationYesButton;
    @FindBy(xpath = "//a[@href='/home']")
    private WebElement homeBreadCrumb;

    public WCCPaymentDetailsPage(WebDriver driver) {
        super(driver);
    }

    public String getPaymenPageTitle() {
        WaitVisible(paymentPageTitle);
        return paymentPageTitle.getText();
    }

    public void clickAddNewCard() {
        WaitVisible(addNewCard);
       jsClick(addNewCard);
    }
    
    public boolean isAddNewCardVisible()
    {
    	return WaitVisible(addNewCard);
    }

    public String getSuccessNotification() {
        WaitVisible(successNotification);
        return successNotification.getText();
    }

    public Result clickPaymentCardEdit(String lastNumber) {
        WebElement cardEditButton = _driver.findElement(By.xpath(String.format(paymentCardEditButtonPath, lastNumber)));
        return ClickControl(cardEditButton, "Card edit button");
    }

    public Result clickPaymentCardDelete(String lastNumber) {
        WebElement cardDeleteButton = _driver.findElement(By.xpath(String.format(paymentCardDeleteButtonPath, lastNumber)));
        return ClickControl(cardDeleteButton, "Card delete button");
    }

    public String getCardNumber(String lastNumber) {
        WebElement CardNumber = _driver.findElement(By.xpath(String.format(paymentCardNumber, lastNumber)));
        return CardNumber.getText();
    }

    public String getCardExpDate(String lastNumber) {
        WebElement CardExpryDate = _driver.findElement(By.xpath(String.format(paymentCardExpDate, lastNumber)));
        return CardExpryDate.getText();
    }

    public Result clickDeleteYesButton() {
        WaitVisible(deleteConfirmationYesButton);
        return ClickControl(deleteConfirmationYesButton, "Delete yes button");
    }
    /*
     *  Accept delete payment card new JavaScript popup window 
     */
    public void clickDeleteOKButton()
    {
    	_driver.switchTo().alert().accept();
    }

    public String getErrorNotification() {
        return errorNotification.getText();
    }
    
    public void clickHomeBreadCrumb()
    {
    	WaitVisible(homeBreadCrumb);
    	jsClick(homeBreadCrumb);
    }
}

