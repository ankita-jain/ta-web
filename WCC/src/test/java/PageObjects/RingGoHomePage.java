package PageObjects;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class RingGoHomePage  extends Base_Page{
   
	 @FindBy(id = "toggleAccountMenu")
	    private WebElement account;
	 
	
	public RingGoHomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	 public Result clickAccount() {
	        WaitVisible(account);
	        return ClickControl(account, "Account Link");
	    }

}
