package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class PaymentBasketFinishPage extends PageClass{

    public WebDriver driver;
    @FindBy(id="templet-container")
    @CacheLookup
    private WebElement basketpagetitle;
    @FindBy(id="labyrinth_finish")
    @CacheLookup
    private WebElement basketfinish_button;

    public PaymentBasketFinishPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(basketpagetitle);
        return basketpagetitle.getText();
    }

    public Result clickFinish() {
        WaitVisible(basketfinish_button);
        return ClickControl(basketfinish_button, "Click finish button");
    }
}
