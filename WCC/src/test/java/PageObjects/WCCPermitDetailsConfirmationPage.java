package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class WCCPermitDetailsConfirmationPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement confirmationPageTitle;
    @FindBy(xpath = "//*[@name='labyrinth_RVPTariff_next']")
    private WebElement next_button;
    @FindBy(xpath = "//*[@name='labyrinth_cancel']")
    private WebElement pay_later;

    public WCCPermitDetailsConfirmationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getConfirmationPageTitle() {
        WaitVisible(confirmationPageTitle);
        return confirmationPageTitle.getText();
    }

    public Result clickNextButton() {
        WaitVisible(next_button);
        return ClickControl(next_button, "Click the next button");
    }

    public void clickPayLater() {
        WaitVisible(pay_later);
        pay_later.click();
    }

    public void confirmPermitDetails() {
        clickNextButton();
    }

    public void payLaterForPermit() {
        clickPayLater();
    }
}
