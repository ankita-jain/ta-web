package PageObjects.Permit;//*[@id="header2"]/ol/li[2]/div/div/label[1]//*[@id="header2"]/ol/li[2]/div/div/label[3]

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCResidentialPermitProofOfVehiclesPage extends PageClass{
	 
	@FindBy(id="labyrinth_Vehicle1_back")
    @CacheLookup
    private WebElement back_button;
	@FindBy(xpath = "//*[@id='header2']/ol/li[2]/div/div/label[1]")
    private WebElement choose_first_pack;
	@FindBy(xpath = "//*[@id='header2']/ol/li[2]/div/div/label[2]")
    @CacheLookup
	public WebElement choose_second_pack;
	@FindBy(xpath = "//*[@id='header2']/ol/li[2]/div/div/label[3]")
    @CacheLookup
    private WebElement choose_third_pack;
	@FindBy(xpath = "//*[@id='header2']/ol/li[2]/div/div/label[4]/p")
    @CacheLookup
    private WebElement choose_fourth_pack;
	@FindBy(xpath = "//*[@id='labyrinth_Vehicle1_next']")
    private WebElement click_next_step;
	@FindBy(xpath = "//*[contains(text(), 'I can only provide')]")
	@CacheLookup
    private WebElement getquestions_pack;
	@FindBy(xpath = "//*[contains(text(), 'Proof of vehicle ownership - ')]")
	public WebElement vehicle_title;
	@FindBy(xpath = "//*[@name='pack-question-0'][1]")
	public WebElement yes_super_pack;
    
	@FindBy(xpath = "//*[@name='pack-question-0'][2]")
    private WebElement no_super_pack;
	@FindBy(xpath = "//*[@id='labyrinth_Vehicle2_next']")
    @CacheLookup
    private WebElement click_next_step_secondVehcile;

	
	 public WCCResidentialPermitProofOfVehiclesPage(WebDriver driver) {
	        super(driver);
	        PageFactory.initElements(driver, this);
	    
	    }

	 public void navigateBack() {
	    	WaitVisible(back_button);
	    	back_button.click();
	    }
	 
	 public Result navigateNextPage() {
	    	WaitVisible(click_next_step);
	    	return ClickControl(click_next_step, "Click next step button");
	    }
	 
	 public String getquestions_pack() {
		 return getquestions_pack.getText();
	    }
	 
	 
	 public String getTitleVehicle() {
		 WaitVisible(vehicle_title);
		 return vehicle_title.getText();
	    }
	 
	 
	 public void selectFirstProofPack() {
	    	WaitVisible(choose_first_pack);
	    	choose_first_pack.click();
	    } 
	 
	 public void selectSecondProofPack() {
	    	WaitVisible(choose_second_pack);
	    	choose_second_pack.click();
	    } 
	 
	 public void selectThirdProofPack() {
	    	WaitVisible(choose_third_pack);
	    	choose_third_pack.click();
	    } 
	 
	 public void selectFourthProofPack() {
	    	WaitVisible(choose_fourth_pack);
	    	choose_fourth_pack.click();
	    } 
	 
	 /**
	     * Function to select no to a super pack question 
	     */
	    public void selectNoToSuperPack() {
	    	jsClick(no_super_pack);
	   }
	    
	    public void clickNextStepForSecondVehicle() {
	    	WaitVisible(click_next_step_secondVehcile);
	    	click_next_step_secondVehcile.click();
	    }
}
