package PageObjects.Permit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import PageObjects.PageClass;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCResidentPermitProofOfAddressPage extends PageClass {

	@FindBy(xpath = "//*[@name='pack-question-0'][1]")
	public WebElement yes_super_pack;
    
	@FindBy(xpath = "//*[@name='pack-question-0'][2]")
    private WebElement no_super_pack;
	
	@FindBy(xpath = "//*[@name='pack-question-1'][1]")
	public WebElement second_yes_super_pack;
    
	@FindBy(xpath = "//*[@name='pack-question-1'][2]")
    private WebElement second_no_super_pack;
	
	@FindBy(xpath = "//*[contains(text(), 'I can provide two documents from the list above')]")
	@CacheLookup
    public WebElement residential_proof_pack_question_one;
	
	@FindBy(xpath = "//*[contains(text(), 'I can provide one document from the list above')]")
    @CacheLookup
    private WebElement residential_proof_pack_question_two;
    
	@FindBy(id = "labyrinth_Proofs_back")
    @CacheLookup
    private WebElement back_button;
    
	@FindBy(id = "labyrinth_AddressProofs_next")
    private WebElement next_button;

    
	@FindBy(xpath = "//ul[@class='warning-notification']/p")
    @CacheLookup
    private WebElement permit_proof_error;
	
	@FindBy(xpath = "//*[contains(text(),'Proof of Address')]")
    @CacheLookup
    private WebElement proof_of_address_pagetitle;
	
	@FindBy(xpath = "//*[contains(text(),'Accepted proofs of address')]")
	private WebElement accepted_proofs_of_address;
    
    
    public WCCResidentPermitProofOfAddressPage(WebDriver driver) {
        super(driver);
    }
    
    /**
     * Function to get page title for proof of address.
     * @return 
     */
    public String getProofOfAddressPageTitle() {
    	if (WaitVisible(proof_of_address_pagetitle))
    	{
    		return proof_of_address_pagetitle.getText();
    	}
    	else return	
    		 "Title not visible";
    }
    
    /**
     * Function to select yes to a super pack question
     */
    public void selectYesToSuperPack() {
    	 jsClick(yes_super_pack);
    }
    
    /**
     * Function to select no to a super pack question 
     */
    public void selectNoToSuperPack() {
    	jsClick(no_super_pack);
   }
    
    public boolean isTheResidentialProofPackQuestionDisplayed() {
        return residential_proof_pack_question_one.isDisplayed();
    }
    
    public boolean getPackQuestion() {
    	WaitVisible(residential_proof_pack_question_one);
    	return residential_proof_pack_question_one.isDisplayed();
    }
    
    /**
     * Function to click on the back button
     */
    public void navigateBack() {
    	WaitVisible(back_button);
    	back_button.click();
    }
    
    public void navigateNextPage() {
    	WaitVisible(next_button);
    	jsClick(next_button);
    }
    
    public boolean getSecondSuperPack() {
    	return second_yes_super_pack.isDisplayed();
    }
    
    public String getErrorMessage() {
    	WaitVisible(permit_proof_error);
    	return permit_proof_error.getText();
    }
    
    public void selectAFullProofPack() {
    	WaitVisible(residential_proof_pack_question_one);
    	residential_proof_pack_question_one.click();
    }
    
    public void selectAPartialProofPack() {
    	WaitVisible(residential_proof_pack_question_two);
    	residential_proof_pack_question_two.click();
    } 
    
    
    public boolean getProofPack() {
    	return residential_proof_pack_question_one.isDisplayed();
    }
    
    public void selectYesToSecondSuperPack() {
   	 jsClick(second_yes_super_pack);
   }
    
    public void selectNoToSecondSuperPack() {
   	 jsClick(second_no_super_pack);
   }
    
    public void getAcceptedProofOfAddressText() {
    	WaitVisible(accepted_proofs_of_address);
    }

}
