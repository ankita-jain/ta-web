package PageObjects.Permit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCResidentialPermitProofOfAddressUploadPage extends PageClass {

	public WCCResidentialPermitProofOfAddressUploadPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@name='proof-upload-type_1-file']")
    @CacheLookup
    private WebElement Proof_Two_Upload;
	
	@FindBy(xpath = "//*[@name='proof-upload-type_0-file']")
    private WebElement Proof_One_Upload;
	
    @FindBy(xpath = "//*[@name='proof-upload-type_0-file']")
	private WebElement council_tax_proof;
    
    @FindBy(xpath = "//*[@id='labyrinth_UploadAddressProofs_next']")
    private WebElement next_button;
    @FindBy(xpath = "//*[@id='labyrinth_UploadAddressProofs_back']")
    @CacheLookup
    private WebElement back_button;
    
    @FindBy(xpath = "//*[@id='document-group-0']")
    @CacheLookup
    private WebElement doc_drop_down;
    
    @FindBy(xpath = "//*[contains(text(),'Invalid file format')]")
    @CacheLookup
    private WebElement invalidFileType;
    
    @FindBy(xpath = "//*[contains(text(),'Invalid file Size')]")
    @CacheLookup
    private WebElement invalidFileSize;
    
    @FindBy(xpath = "//legend[contains(text(),'Council Tax Bill')]")
    @CacheLookup
    private WebElement councilTaxHeader;
    
    @FindBy(xpath = "//legend[contains(text(),'One Document')]")
    @CacheLookup
    private WebElement oneAcceptedDocHeader;
    
    @FindBy(xpath = "//legend[contains(text(),'Two Document')]")
    @CacheLookup
    private WebElement TwoAcceptedDocHeader;

    
    String[] ProofTypes = {"Property lease agreement", "Property purchase document", "Tenancy agreement ", "Bank Statement", "Utility Bill"};
        
    
    public boolean uploadButtonVisible() {
        return isPresentAndDisplayed(Proof_One_Upload);
    }

	public void sendUploadPathToCouncilTax(String proof) {
		council_tax_proof.sendKeys(getCurrentDirectoryPath() + proof);
	}
	
	public void sendUploadPathToProofOne(int prooftype, String proof) {
		selectProofType(ProofTypes[prooftype]);
		Proof_One_Upload.sendKeys(getCurrentDirectoryPath() + proof);
	}
	
	public void sendUploadPathToProofTwo(int prooftype, String proof) {
		selectProofType(ProofTypes[prooftype]);
		Proof_Two_Upload.sendKeys(getCurrentDirectoryPath() + proof);
	}

	public Result clickNextButton() {
		WaitVisible(next_button);
		return ClickControl(next_button, "Click the next button");
	}
	
	public void clickBackButton() {
		WaitVisible(back_button);
		back_button.click();
	}
	
	public void selectProofType(String proofType) {
		WaitVisible(doc_drop_down);
		Select proofTypes = new Select(doc_drop_down);
	    proofTypes.selectByVisibleText(proofType);
	}
	
	public boolean wrongFileError() {
		WaitVisible(invalidFileType);
		return invalidFileType.isDisplayed();
	}
	
	public boolean wrongFileSizeError() {
		WaitVisible(invalidFileType);
		return invalidFileSize.isDisplayed();
	}
	
	public String getCouncilTaxPageHeader() {
		if (WaitVisible(councilTaxHeader))
		{
        return councilTaxHeader.getText();
		}
		else return "Title not visible";
    }
	
	public String getOneDocPageHeader() {
		if (WaitVisible(oneAcceptedDocHeader))
		{
        return oneAcceptedDocHeader.getText();
		}
		else return "Title not visible";
    }
	
	public String getTwoDocPageHeader() {
		if (WaitVisible(TwoAcceptedDocHeader))
		{
        return TwoAcceptedDocHeader.getText();
		}
		else return "Title not visible";
    }
}
