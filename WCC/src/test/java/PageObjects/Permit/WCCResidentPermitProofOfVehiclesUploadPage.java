package PageObjects.Permit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class WCCResidentPermitProofOfVehiclesUploadPage extends PageClass {
	
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_all_types_one_file;
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_two_types_one_file;
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_one_type_one_file;
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_proof_first;
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_proof_second;
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_proof_third;
	 @FindBy(xpath = "TBD")
	 @CacheLookup
	 private WebElement vehicle_proof_page_Title;
	 @FindBy(xpath = "//*[@id='labyrinth_UploadVehicle1_next']")
	 @CacheLookup
	 private WebElement next_button;
	 @FindBy(xpath = "//*[contains(text(),'Invalid file format')]")
	 @CacheLookup
	 private WebElement invalidFileType;   
     @FindBy(xpath = "//*[contains(text(),'Invalid file size')]")
	 @CacheLookup
	 private WebElement invalidFileSize;

  	 @FindBy(xpath = "//*[@name='proof-upload-type_0-file']")
     private WebElement Proof_One_Upload;
     
     @FindBy(xpath = "//*[@name='proof-upload-type_1-file']")
     @CacheLookup
     private WebElement Proof_Two_Upload;
 	
 	 @FindBy(xpath = "//*[@name='proof-upload-type_2-file']")
     private WebElement Proof_Three_Upload;
 	 
 	@FindBy(xpath = "//*[@id='proof-upload-type_0-group']")
    private WebElement first_proof_dropdown;
 	
 	@FindBy(xpath = "//*[@id='proof-upload-type_1-group']")
    private WebElement second_proof_dropdown;
 	
 	@FindBy(xpath = "//*[@id='proof-upload-type_2-group']")
    private WebElement third_proof_dropdown;
 	
 	@FindBy(xpath = "//legend[contains(text(),'Proof of vehicle ownership')]")
    private WebElement vehicleUploadPageHeader;
 	
 	@FindBy(xpath = "//*[@id='labyrinth_UploadVehicle2_next']")
	 @CacheLookup
	 private WebElement secondVehcile_next_button;
 	 
 	String[] ProofTypes = {"Vehicle Registration Certificate (V5C)", "Certificate of Insurance", "Insurance Schedule", "Combined Certificate of Insurance and Insurance Schedule"};
	
	 
	
	public WCCResidentPermitProofOfVehiclesUploadPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
	
	public Result clickNextButton() {
        WaitVisible(next_button);
        return ClickControl(next_button, "Click the next button");
    }

	
	public String getProofPageTitle() {
        WaitVisible(vehicle_proof_page_Title);
        return vehicle_proof_page_Title.getText();
    }
	/* Upload all three documents in one file for first Pack */
	public void sendUploadPathToAllInOneFile(String proofonepath) {

		vehicle_all_types_one_file.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	/* Upload V5C Document for second Pack */
	public void sendUploadPathV5C(String proofonepath) {

		vehicle_one_type_one_file.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
		
	/* Upload two Insurance Documents in one file for second Pack */
	public void sendUploadPathInsuranceDocs(String proofonepath) {

		vehicle_one_type_one_file.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	/* Upload V5C as a separate file for Third Pack */
	public void sendUploadV5c(String proofonepath) {

		vehicle_proof_first.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	
	/* Upload Insurance Certificate as a separate file for third Pack*/
	public void sendUploadInsuranceCertificate(String proofonepath) {

		vehicle_proof_second.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	/* Upload Insurance Schedule as a separate file */
	public void sendUploadInsuranceSchedule(String proofonepath) {

		vehicle_proof_third.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	
	/*Uploading Proof for First Pack*/
	public void uploadProofForFirstPack(String proof1path){
		sendUploadPathToAllInOneFile(proof1path);
	    clickNextButton();
	    }
	
	/*Uploading Proof for Second Pack*/
	public void uploadProofForSecondPack(String proof1path, String proof2path){
		sendUploadPathV5C(proof1path);
		sendUploadPathInsuranceDocs(proof2path);
		clickNextButton();
	    }

	/*Uploading Proof for Third Pack*/
	public void uploadProofForSecondPack(String proof1path, String proof2path, String proof3path){
		sendUploadV5c(proof1path);
		sendUploadInsuranceCertificate(proof2path);
		sendUploadInsuranceSchedule(proof3path);
		clickNextButton();
	    }
	
	public boolean wrongFileError() {
		WaitVisible(invalidFileType);
		return invalidFileType.isDisplayed();
	}
	
	public boolean wrongFileSizeError() {
		WaitVisible(invalidFileType);
		return invalidFileSize.isDisplayed();
	}
	
	public void selectFirstProofType(String proofType) {
		WaitVisible(first_proof_dropdown);
		Select proofTypes = new Select(first_proof_dropdown);
	    proofTypes.selectByVisibleText(proofType);
	}
	
	public void selectSecondProofType(String proofType) {
		WaitVisible(second_proof_dropdown);
		Select proofTypes = new Select(second_proof_dropdown);
	    proofTypes.selectByVisibleText(proofType);
	}
	
	public void selectThirdProofType(String proofType) {
		WaitVisible(third_proof_dropdown);
		Select proofTypes = new Select(third_proof_dropdown);
	    proofTypes.selectByVisibleText(proofType);
	}
	
	public void selectDocTypeAndUploadFirstProof(String proofonepath) {

		Proof_One_Upload.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	public void selectDocTypeAndUploadSecondProof(String proofonepath) {

		Proof_Two_Upload.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }
	
	public void selectDocTypeAndUploadThirdProof(String proofonepath) {

		Proof_Three_Upload.sendKeys(getCurrentDirectoryPath() + proofonepath);
    }

	public String getVehicleUploadPageHeader() {
		WaitVisible(vehicleUploadPageHeader);
        return vehicleUploadPageHeader.getText();
	}
	
	public void selectNextForSecondVehicle() {
		WaitVisible(secondVehcile_next_button);
		secondVehcile_next_button.click();
	}
	
}
