package PageObjects.Permit;

import static SeleniumHelpers.WebElementMethods.enterText;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import Structures.Result;

public class WCCPermanentVehiclePage extends PageClass {
	
	@FindBy(xpath = "//*[@class='new-vehicle-link addVehicleLink']")
	@CacheLookup
	private WebElement clickAddSecondVehicle;
	@FindBy(xpath = "//*[@id=\'field-selectvehicle1\']")
    @CacheLookup
    private WebElement enterSecondNumberPlate;
	@FindBy(xpath = "//*[@id=\'field-OwnershipId1\']")
    @CacheLookup
    private WebElement selectOwnershipType;
	@FindBy(xpath = "//*[@id='proof-upload_0-group']")
    @CacheLookup
    private WebElement selectDocumentType;
	@FindBy(xpath = "//*[@name='proof-upload_0-file']")
    @CacheLookup
    private WebElement clickChooseButton;
	@FindBy(xpath = "//*[@id='labyrinth_permanentVehicleChangeForm_next']")
    @CacheLookup
    private WebElement clickNextButton;
	
	
	 public WCCPermanentVehiclePage(WebDriver driver) {
	        super(driver);
	        PageFactory.initElements(driver, this);

	    }

	    public Result clickAddRegisteration() {
	    	WaitVisible(clickAddSecondVehicle);
	    	return ClickControl(clickAddSecondVehicle, "Click add second vehicle link");
	    }
	    public Result AddSecondPermanentVehicle(String VRN) {
	    	WaitVisible(enterSecondNumberPlate);
	    	return enterText(enterSecondNumberPlate, VRN, "Password input");
	    }
	    
	    public void selectOwnershipType(String OwnershipType)
	    {
	    	 WaitVisible(selectOwnershipType);
	    	new Select(selectOwnershipType).selectByVisibleText(OwnershipType);
	    }
	    
	    public void selectDocumentType(String OwnershipDocs)
	    {
	    	 WaitVisible(selectDocumentType);
	     	new Select(selectDocumentType).selectByVisibleText(OwnershipDocs);
	    }
	    
	    public Result clickNextStep()
	    {
	    	WaitVisible(clickNextButton);
	    	return ClickControl(clickNextButton, "Click the next button.");
	    }
	    
	    public void uploadOneProof(String proof) {
	    	clickChooseButton.sendKeys(getCurrentDirectoryPath() + proof);
		}
	    
	    public void addSecondPermanentVehicleProcess(String vrn, String ownership, String Ownershipdocs, String proofone) {
	    	clickAddRegisteration();
	    	AddSecondPermanentVehicle(vrn);
	    	selectOwnershipType(ownership);
	    	selectDocumentType(Ownershipdocs);
	    	uploadOneProof(proofone);
	        clickNextStep();
	    }

	    

}
