package PageObjects.Permit;

import static SeleniumHelpers.WebElementMethods.enterText;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import PageObjects.PageClass;
import Structures.Result;

public class WCCTemporaryVehiclePage extends PageClass{
	
	  
    /* New Add Second Vehicle Page */
    
    @FindBy(xpath = "//*[@class='new-vehicle-link addVehicleLink']")
    @CacheLookup
    private WebElement addSecondNumberPlate;
    @FindBy(xpath = "//*[@id='field-OwnershipId1']")
    @CacheLookup
    private WebElement ownershipTypeDropDownNew;
    @FindBy(xpath = "//*[@id='field-selectvehicle1']")
    private WebElement enterVehicleNumber;
    @FindBy(xpath = "//*[@id='labyrinth_temporaryVehicleChangeForm_next']")
    @CacheLookup
    private WebElement clickNextStep;

    
    
    
    public WCCTemporaryVehiclePage(WebDriver driver) {
        super(driver);       
        PageFactory.initElements(driver, this);

    }
    
    public void clickAddVehicle() {
    	WaitVisible(addSecondNumberPlate);
    	addSecondNumberPlate.click();
    }

    public Result AddSecondVehicle(String VRN) {
    	WaitVisible(enterVehicleNumber);
    	return enterText(enterVehicleNumber, VRN, "VRN Input");
    }
    
    public void selectOwnershipType(String OwnershipType)
    {
    	 WaitVisible(ownershipTypeDropDownNew);
    	new Select(ownershipTypeDropDownNew).selectByVisibleText(OwnershipType);
    }
    

    
    public void clickNextStep()
    {
    	WaitVisible(clickNextStep);
    	clickNextStep.click();
    }

    public void TemporaryVehicleProcess(String vrn, String ownership, String Ownershipdocs, String proofone) {
    	clickAddVehicle();
    	AddSecondVehicle(vrn);
    	selectOwnershipType(ownership);
        clickNextStep();
    }

    
}
