package PageObjects.Permit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class EditAddressProofUploadPage extends PageClass {

	public EditAddressProofUploadPage(WebDriver driver) {
		 super(driver);
	        PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@name='proof-upload-type_0-file']")
    private WebElement Proof_One_Upload;
	@FindBy(xpath = "//legend[contains(text(),'One accepted document')]")
    @CacheLookup
    private WebElement AcceptedDocHeader;
	@FindBy(id = "labyrinth_AddressProofs_next")
    @CacheLookup
    private WebElement nextButton;
	
	public void uploadOneProof(String proof) {
		Proof_One_Upload.sendKeys(getCurrentDirectoryPath() + proof);
	}

	public String getPageHeader() {
		if (WaitVisible(AcceptedDocHeader))
		{
        return AcceptedDocHeader.getText();
		}
		else return "Title not visible";
	}

	public Result clickNextButton() {
		WaitVisible(nextButton);
		return ClickControl(nextButton, "Click next button");
	}
}
