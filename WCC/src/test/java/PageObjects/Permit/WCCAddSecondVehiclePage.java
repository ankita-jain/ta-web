package PageObjects.Permit;

import static SeleniumHelpers.WebElementMethods.enterText;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import PageObjects.PageClass;
import Structures.Result;

public class WCCAddSecondVehiclePage extends PageClass{
	
	  
    /* New Add Second Vehicle Page */
    
    @FindBy(xpath = "//*[@id='field-selectvehicle1']")
    @CacheLookup
    private WebElement addSecondNumberPlate;
    @FindBy(xpath = "//*[@id='field-OwnershipId1']")
    @CacheLookup
    private WebElement ownershipTypeDropDownNew;
    @FindBy(xpath = "//*[@id='proof-upload_0-group']")
    @CacheLookup
    private WebElement proofOwnershipDropdown;
    @FindBy(xpath = "//*[@name='proof-upload_0-file']")
    private WebElement chooseFileButton;
    @FindBy(xpath = "//*[@id='templet-container']/li[2]/div/div[2]/div[2]/ul/li[5]/a")
    @CacheLookup
    private WebElement addSecondVehicle;
    @FindBy(xpath = "//*[@id='labyrinth_addVehicleForm_next']")
    @CacheLookup
    private WebElement clickNextStep;
    @FindBy(xpath="//*[@id='addvehicle1']")
    private WebElement clickAddRegisteration;
    
    
    
    public WCCAddSecondVehiclePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);

    }
    
   
    public void clickAddSecondVehicleChangeTab() {
        WaitVisible(addSecondVehicle);
        addSecondVehicle.click();
    }
    public void clickAddRegisteration() {
    	WaitVisible(clickAddRegisteration);
    	clickAddRegisteration.click();
    }
    public Result AddSecondVehicle(String VRN) {
    	WaitVisible(addSecondNumberPlate);
    	return enterText(addSecondNumberPlate, VRN, "Password input");
    }
    
    public void selectOwnershipType(String OwnershipType)
    {
    	 WaitVisible(ownershipTypeDropDownNew);
    	new Select(ownershipTypeDropDownNew).selectByVisibleText(OwnershipType);
    }
    
    public void selectProofOfOwnership(String OwnershipDocs)
    {
    	 WaitVisible(proofOwnershipDropdown);
     	new Select(proofOwnershipDropdown).selectByVisibleText(OwnershipDocs);
    }
    
    public void clickNextStep()
    {
    	WaitVisible(clickNextStep);
    	clickNextStep.click();
    }
    
    public void uploadOneProof(String proof) {
    	chooseFileButton.sendKeys(getCurrentDirectoryPath() + proof);
	}
    
    public void addSecondVehicleProcess(String vrn, String ownership, String Ownershipdocs, String proofone) {
    	clickAddRegisteration();
    	AddSecondVehicle(vrn);
    	selectOwnershipType(ownership);
    	selectProofOfOwnership(Ownershipdocs);
    	uploadOneProof(proofone);
        clickNextStep();
    }

    
}
