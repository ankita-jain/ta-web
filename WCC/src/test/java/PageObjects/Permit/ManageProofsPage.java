package PageObjects.Permit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

public class ManageProofsPage extends PageClass {
	
	public ManageProofsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement ManageProofsPageTitle;
	@FindBy(xpath = "//*[@id='labyrinth_Proofs_next']")
    @CacheLookup
    private WebElement nextButton;	
	@FindBy(xpath = "//*[@id='1']/td[3]/div/input")
	public WebElement firstUploadProof;
	@FindBy(xpath = "//*[@id='labyrinth_finish']")
	public WebElement finishButton;
	@FindBy(xpath = "//*[@id='2']/td[3]/div/input")
	public WebElement secondUploadProof;
	
	
	/**
	 * Function to get the manage proof page header.
	 * @return
	 */
	public String getManageProofsPageTitle()
	{
		WaitVisible(ManageProofsPageTitle);
		return ManageProofsPageTitle.getText();
	}
	
	/**
	 * Function to save changes.
	 */
	public void saveChanges()
	{
		WaitVisible(nextButton);
		nextButton.click();
	}	
	/**
	 * Function to upload proofs.
	 * @param proof
	 */
	public void uploadProof(String proof)
	{
		firstUploadProof.sendKeys(getCurrentDirectoryPath() + proof);
		secondUploadProof.sendKeys(getCurrentDirectoryPath() + proof);
	}
	
	public boolean getUploadButton()
	{
		WaitVisible(firstUploadProof);
		return firstUploadProof.isDisplayed();
	}
	
	public Result clickFinishButton() {
		WaitVisible(finishButton);
		return ClickControl(finishButton, "Click finish button");
	}
	
	public void uploadSecondProof(String proof) {
		secondUploadProof.sendKeys(getCurrentDirectoryPath() + proof);
	}
}