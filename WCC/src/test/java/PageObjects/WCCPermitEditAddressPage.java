package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import Structures.Result;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCPermitEditAddressPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement edit_address_title;
    @FindBy(xpath = "//*[@id='enterAddressLabel']")
    @CacheLookup
    private WebElement enter_address_tab;
    @FindBy(xpath = "//*[@name='addressOptions[type]' and @value='1']")
    @CacheLookup
    private WebElement enter_address;
    @FindBy(xpath = "//*[@name='addressOptions[type]' and @value='2']")
    @CacheLookup
    private WebElement my_address;
    @FindBy(xpath = "//*[@id='postcode']")
    @CacheLookup
    private WebElement postcode;
    @FindBy(xpath = "//*[@id='updateaddress']")
    @CacheLookup
    private WebElement find_address_button;
    @FindBy(xpath = "//*[@id='field-newAddressSelect']")
    @CacheLookup
    private WebElement new_address;
    @FindBy(xpath = "//*[@id='ParkingZone']")
    @CacheLookup
    private WebElement residing_location;
    @FindBy(xpath = "//*[@name='labyrinth_editAddressForm_next']")
    private WebElement save_changes_button;

    public WCCPermitEditAddressPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getEditAddressPageTitle() {
        WaitVisible(edit_address_title);
        return edit_address_title.getText();
    }

    public void clickEnterAddressTab() {
        WaitVisible(enter_address_tab);
        enter_address_tab.click();
    }

    public void clickEnterAddress() {
        jsClick(enter_address);
    }

    public void enterPostcode(String postCode) {
        WaitVisible(postcode);
        postcode.clear();
        postcode.sendKeys(postCode);
    }

    public Result clickFindAddress() {
        WaitVisible(find_address_button);
        return ClickControl(find_address_button, "Click find address button");
    }

    public void selectAddress(String address) {
        WaitVisible(new_address);
        new Select(new_address).selectByVisibleText(address);
    }

    public void selectResidingLocation(String location) {
        WaitVisible(residing_location);
        new Select(residing_location).selectByVisibleText(location);
    }

    public Result clickSaveChanges()    {
        WaitVisible(save_changes_button);
        return ClickControl(save_changes_button, "Click save button");
    }

    public void changeAddressOnPermit(String postcode, String address, String location) {
        clickEnterAddress();
        enterPostcode(postcode);
        clickFindAddress();
        selectAddress(address);
        selectResidingLocation(location);
        clickSaveChanges();
    }
    
    public void updateAddressOnPermit(String postcode, String address, String location) {
        clickEnterAddress();
        enterPostcode(postcode);
        clickFindAddress();
        selectAddress(address);
        selectResidingLocation(location);
    }
}
