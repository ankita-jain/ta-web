package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCRegisterPersonalAccountPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement registerPageTitle;
    @FindBy(xpath = "//*[@id='field-Member_CLI']")
    @CacheLookup
    private WebElement telephoneNumberField;
    @FindBy(xpath = "//*[@id='field-Member_CLI_confirm']")
    @CacheLookup
    private WebElement confirmYourTelephoneNumberField;
    @FindBy(xpath = "//*[@id='field-Member_Title']")
    @CacheLookup
    private WebElement titleDropdown;
    @FindBy(xpath = "//*[@id='field-Member_Firstname']")
    @CacheLookup
    private WebElement firstNameField;
    @FindBy(xpath = "//*[@id='field-Member_Lastname']")
    @CacheLookup
    private WebElement surnameField;
    @FindBy(xpath = "//*[@id='field-Member_Email']")
    @CacheLookup
    private WebElement emailAddressField;
    @FindBy(xpath = "//*[@id='field-Member_Email_confirm']")
    @CacheLookup
    private WebElement confirmEmailAddressField;
    @FindBy(xpath = "//*[@id='field-MemberPassword']")
    @CacheLookup
    private WebElement passwordField;
    @FindBy(xpath = "//*[@id='field-MemberPassword_confirm']")
    @CacheLookup
    private WebElement confirmPasswordField;
    @FindBy(xpath = "//*[@id='header4']/ol/li/div/div/input")
    @CacheLookup
    private WebElement termsAndConditionsCheckbox;
    @FindBy(xpath = "//*[@id='header5']/ol/li[2]/div/div/input")
    @CacheLookup
    private WebElement westminsterParkrightTermsAndConditionsCheckbox;
    @FindBy(xpath = "//*[@id='header5']/ol/li[4]/div/div/input[1]")
    @CacheLookup
    private WebElement nextButton;
    @FindBy(xpath = "//*[@id='header1']/ol/li[1]/div/div/span")
    @CacheLookup
    private WebElement errorForTelephoneNumber;
    @FindBy(xpath = "//*[@id='header1']/ol/li[2]/div/div/span")
    @CacheLookup
    private WebElement errorForConfirmTelephoneNumber;
    @FindBy(xpath = "//*[@id='header1']/ol/li[5]/div/div/span")
    @CacheLookup
    private WebElement errorForFirstName;
    @FindBy(xpath = "//*[@id='header1']/ol/li[6]/div/div/span")
    @CacheLookup
    private WebElement errorForSurname;
    @FindBy(xpath = "//*[@id='header1']/ol/li[5]/div/div/span")
    @CacheLookup
    private WebElement errorForEmailAddress;
    @FindBy(xpath = "//*[@id='header1']/ol/li[6]/div/div/span")
    @CacheLookup
    private WebElement errorForConfirmEmailAddress;
    @FindBy(xpath = "//*[@id='header1']/ol/li[12]/div/div/span")
    @CacheLookup
    private WebElement errorForPassword;
    @FindBy(xpath = "//*[@id='header1']/ol/li[13]/div/div/span")
    @CacheLookup
    private WebElement errorForConfirmPassword;
    @FindBy(xpath = "//*[@id='header3']/ol/li[2]/div/div/span")
    @CacheLookup
    private WebElement errorForSecurityText;
    @FindBy(xpath = "//*[@id='header4']/ol/li/div/div/span")
    @CacheLookup
    private WebElement errorForTermsAndConditionsCheckbox;
    @FindBy(xpath = "//*[@id='header5']/ol/li[2]/div/div/span")
    @CacheLookup
    private WebElement errorForWestminsterParkrightTermsAndConditionsCheckbox;

    public WCCRegisterPersonalAccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getRegisterPageTitle() {
        WaitVisible(registerPageTitle);
        return registerPageTitle.getText();
    }

    public void enterTelephoneNumber(String telephoneNumber) {
        telephoneNumberField.clear();
        telephoneNumberField.sendKeys(telephoneNumber);
    }

    public void enterConfirmTelephoneNumber(String confirmTelephoneNumber) {
        confirmYourTelephoneNumberField.clear();
        confirmYourTelephoneNumberField.sendKeys(confirmTelephoneNumber);
    }

    public void selectTitle(String titleFromDropdown) {
        WaitVisible(titleDropdown);
        Select titlelist = new Select(titleDropdown);
        titlelist.selectByVisibleText(titleFromDropdown);

    }

    public void enterFirstName(String firstName) {
        firstNameField.clear();
        firstNameField.sendKeys(firstName);
    }

    public void enterSurname(String surname) {
        surnameField.clear();
        surnameField.sendKeys(surname);
    }

    public void enterEmail(String emailAddress) {
        emailAddressField.clear();
        emailAddressField.sendKeys(emailAddress);
    }

    public void enterConfirmEmail(String confirmEmailAddress) {
        confirmEmailAddressField.clear();
        confirmEmailAddressField.sendKeys(confirmEmailAddress);
    }

    public void enterPassword(String password) {
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public void enterConfirmPassword(String confirmPassword) {
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(confirmPassword);
    }

    public void selectTermsAndConditions() {
        WaitVisible(termsAndConditionsCheckbox);
        termsAndConditionsCheckbox.click();
    }

    public void selectWestminsterParkrightTermsAndConditions() {
        WaitVisible(westminsterParkrightTermsAndConditionsCheckbox);
        westminsterParkrightTermsAndConditionsCheckbox.click();
    }

    public void clickNextButton() {
        WaitVisible(nextButton);
        nextButton.click();
    }

    public String getErrorText() {
        return errorForTelephoneNumber.getText();

    }


}
