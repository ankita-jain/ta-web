package PageObjects.Insight;
import PageObjects.PageClass;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

import static SeleniumHelpers.WebElementMethods.enterText;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsightExemptVehiclesListManagement extends PageClass {
	
	@FindBy(xpath = "//*[contains(text(),'Add new Exempt Vehicles List Entry')]")
    private WebElement addexemptvehiclelist;
	
	@FindBy(xpath = "//*[@id=\"field-CLI\"]")
    private WebElement cliInput;
	
	@FindBy(xpath = "//select[@id='field-Type']")
    private WebElement selectType;
	
	@FindBy(xpath = "//*[@id=\"field-Zone\"]")
    private WebElement parkingZone;
	
	@FindBy(xpath = "//*[@id=\"field-VRN1\"]")
    private WebElement vrnInput;

	@FindBy(xpath = "//*[@id=\"startcalendar\"]")
    private WebElement validFromInput;
	
	@FindBy(xpath = "//*[@id=\"endcalendar\"]")
    private WebElement validToInput;
	
	@FindBy(xpath = "//*[@value='Save']")
    private WebElement saveButton;
	
	@FindBy(xpath = "//*[contains(text(),'V1')]//following::img[3]")
    private WebElement deleteButton;

	@FindBy(xpath = "//*[@value='Yes']")
    private WebElement confirmYesButton;
	
	@FindBy(xpath = "//*[@class=\"success-notification\"]")
    private WebElement successNotification;
	
	public InsightExemptVehiclesListManagement(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	 public void clickAddNewExemptVehicle() {
	        WaitVisible(addexemptvehiclelist);
	        addexemptvehiclelist.click();
	    }
	 public Result enterMobileNumber(String mobileNumber) {
	        return enterText(cliInput, mobileNumber, "MobileNumber input");
	    }
	 public Result selectType(String type) {
          return WebElementMethods.selectItemFromDropdownByContainsText(selectType, type, "Corporate");
      }
      
	 public Result selectZone(String zone) {
         return WebElementMethods.selectItemFromDropdownByContainsText(parkingZone, zone, "Zone A, Westminster Permits (65000)");
     }
	
	 public Result enterVRN(String vrn) {
	        return enterText(vrnInput, vrn, "VRN input");
	    }

	 public void selectJQCalanderFromDate(String fromDate) {
		 JavascriptExecutor js = ((JavascriptExecutor) driver);
		 js.executeScript("document.getElementById('startcalendar').value=\""+fromDate+"\"");
		 }
	 
	 public void selectJQCalanderToDate(String toDate) {
		 JavascriptExecutor js = ((JavascriptExecutor) driver);
		 js.executeScript("document.getElementById('endcalendar').value=\""+toDate+"\"");
		 }
	 
	 public Result clickSaveButton() {
	        
	        return WebElementMethods.ClickControl(saveButton, "saveButton button clicked"); 
	    }
	 
	 public Result clickEndSession() {
		 return WebElementMethods.ClickControl(deleteButton, "deleteButton button clicked"); 
	    }
	 
	 public Result confirmEndSession() {
		 
		 return WebElementMethods.ClickControl(confirmYesButton, "Confirm Yes Button button clicked"); 
		
	    }
	 
	 public String getSuccessText() {
	        WaitVisible(successNotification);
	        return successNotification.getText();
	    }
}
