package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class InsightDeleteCustomerPage extends Base_Page {
	
    public WebDriver driver;
    
    @FindBy(name = "Member_CLI")
    private WebElement customerCLI;
    @FindBy(name = "submits[submit]")
    private WebElement deletebutton;
    
    public InsightDeleteCustomerPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }
    
    public Result clickDelete() {
        WaitVisible(deletebutton);
        return WebElementMethods.ClickControl(deletebutton, "Delete button");
    }
}
