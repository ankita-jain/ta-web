package PageObjects.Insight;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebDriverMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class InsightCorporate extends Base_Page {

    private BankTransfer bankTransfer = new BankTransfer(_driver);
    private AccountListing accountListing = new AccountListing(_driver);
    private SearchVehicle searchVehicle = new SearchVehicle(_driver);

    public InsightCorporate(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(text(),'Bank Transfers')]")
    private WebElement BankTransfers;

    public Result clickCorporateBankTransfer() {
    	WaitVisible(BankTransfers);
        return WebElementMethods.ClickControl(BankTransfers, "BankTransfers link");
    }

    public BankTransfer getBankTransfer() {
        return bankTransfer;
    }

    public AccountListing getAccountListing() {
        return accountListing;
    }

    public SearchVehicle getSearchVehicle() {
        return searchVehicle;
    }

    public class BankTransfer {

        @FindBy(xpath = "//select[@name='Status']")
        private WebElement bacs_status;

        @FindBy(xpath = "//select[@name='CorpAccountID']")
        private WebElement corp_account;

        @FindBy(xpath = "//input[@name='submits[submit]']")
        private WebElement bacs_filter;

        @FindBy(xpath = "//input[@name='TransferDate[Start]']")
        private WebElement bacs_transfer_date;


        private BankTransfer(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }


        public Result selectStatus(String status) {
            return WebElementMethods.selectItemFromDropdownByContainsText(bacs_status, status, "Select 'BACS' status");
        }

        public Result selectCorporateAccount(String account) {
            return WebElementMethods.selectItemFromDropdownByContainsText(corp_account, account, "Select 'Corporate'account");
        }

        public void enterBACSTranferDate(String transfer_date) {
            WebDriverMethods.executeJavascript("document.getElementsByName('TransferDate[Start]').value=\"" + transfer_date + "\"", _driver);
        }

        public Result clickFilter() {
            WaitVisible(bacs_filter);
            return WebElementMethods.ClickControl(bacs_filter, "Filter button");
        }

    }

    public class AccountListing {
        @FindBy(xpath = "//input[@placeholder='Account Number']")
        private WebElement accountNumberField;
        @FindBy(xpath = "//input[@value='Filter']")
        private WebElement filterButton;

        private AccountListing(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public Result enterAccountNumber(String accountNumber) {
            WaitVisible(accountNumberField);
            return enterText(accountNumberField, accountNumber, "Enter account number");
        }

        public Result clickOnFilterButton() {
            WaitVisible(filterButton);
            return ClickControl(filterButton, "Click on Filter Button");
        }
    }

    public class SearchVehicle {
        @FindBy(xpath = "//input[@type='text']")
        private WebElement vrnInput;
        @FindBy(name = "search")
        private WebElement searchButton;
        @FindBy(xpath = "//table[@id='table-1']/tbody/tr")
        private List<WebElement> resultRows;
        @FindBy(className = "error-notification")
        private WebElement errorNotification;

        private final String DELETE_BUTTON_LOCATOR_PATTERN = "//a[contains(@href, '%s')]";

        private SearchVehicle(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public Result enterVrn(String vrn) {
            WaitVisible(vrnInput);
            return enterText(vrnInput, vrn, "Enter vehicle number");
        }

        public Result clickOnSearch() {
            WaitVisible(searchButton);
            return ClickControl(searchButton, "Search button");
        }

        public boolean isVrnPresent(String vrnNumber) {
            return resultRows.stream().anyMatch(row -> row.getText().contains(vrnNumber)) && !resultRows.isEmpty();
        }

        public boolean isVrnPartPresent(String vrnNumber) {
            return resultRows.stream().allMatch(row -> row.getText().contains(vrnNumber)) && !resultRows.isEmpty();
        }

        public Result clickOnDeleteButton(String vrnNumber) {
            WebElement deleteButton = WaitVisibleElement(By.xpath(String.format(DELETE_BUTTON_LOCATOR_PATTERN, vrnNumber)));
            return ClickControl(deleteButton, "Delete Button");
        }

        public String getErrorNotificationText() {
            WaitVisible(errorNotification);
            return errorNotification.getText();
        }

        public ConfirmDeletion getConfirmDeletion() {
            return new ConfirmDeletion(_driver);
        }

        public class ConfirmDeletion {
            @FindBy(xpath = "//input[@value='Yes']")
            private WebElement yesButton;
            @FindBy(xpath = "//input[@value='No']")
            private WebElement noButton;
            @FindBy(xpath = "//h1[text()='Confirm']/following-sibling::p[last()]")
            private WebElement confirmationMessage;


            public ConfirmDeletion(WebDriver driver) {
                PageFactory.initElements(driver, this);
            }

            public Result clickOnYesButton() {
                WaitVisible(yesButton);
                return ClickControl(yesButton, "Yes button");
            }

            public Result clickOnNoButton() {
                WaitVisible(noButton);
                return ClickControl(noButton, "No button");
            }

            public String getConfirmationText() {
                WaitVisible(confirmationMessage);
                return confirmationMessage.getText();
            }
        }
    }


}
