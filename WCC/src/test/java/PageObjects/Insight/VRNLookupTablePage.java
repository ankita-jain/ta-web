package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class VRNLookupTablePage extends Base_Page {
    @FindBy(xpath = "//input[@name='vrn']")
    private WebElement vrnInput;

    @FindBy(xpath = "//input[@name='submit']")
    private WebElement submitButton;

    @FindBy(xpath = "//p/a[contains(@href,'edit')]")
    private WebElement editThisVehicleLink;

    public VRNLookupTablePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getVrnInput() {
        return vrnInput;
    }

    public WebElement getSubmitButton() {
        return submitButton;
    }

    public WebElement getEditThisVehicleLink() {
        return editThisVehicleLink;
    }

    public Result enterVrn(String VRN) {
        return enterText(vrnInput, VRN, "VRN number input");
    }

    public Result clickSubmit() {
        return ClickControl(submitButton, "Submit button");
    }

    public Result clickEditThisVehicle() {
        return ClickControl(editThisVehicleLink, "Edit this Vehicle link");
    }
}
