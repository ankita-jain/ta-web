package PageObjects.Insight;

import static SeleniumHelpers.WebElementMethods.enterText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByContainsText;
import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByOptionValue;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class InsightRefund extends Base_Page {
	
	public WebDriver driver;

	public InsightRefund(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);	
	}
	
	 @FindBy(xpath="(//*[@id='1']/td[11]/p/a/img)[1]")
	 private WebElement refund_spanner;
	 
	 @FindBy(xpath="//*[@name='component']")
	 private WebElement transaction_type_select;
	 
	 @FindBy(xpath="//*[@name='query[query]']")
	 private WebElement search_field;
	 
	 @FindBy(xpath="//*[@name='query[type]']")
	 private WebElement search_field_type;
	 
	 @FindBy(xpath="//input[@value='Search']")
	 private WebElement search_button;
	 
	 @FindBy(xpath="//input[@name='refund_parking_amount']")
	 private WebElement refund_amount;
	 
	 @FindBy(xpath="//input[@value='Next']")
	 private WebElement next_button;
	 
	 @FindBy(xpath="//input[@value='Refund']")
	 private WebElement refund_button;
	 
	 @FindBy(xpath="//*[contains(text(),'Refund type')]//following::td[1]")
	 private WebElement refund_type;
		 	 
	 @FindBy(xpath="//textarea[@name='remarks']")
	 private WebElement remarks;
	 
	 @FindBy(xpath="//input[@name='passwordconfirm']")
	 private WebElement password_confirmation;
	 
	 @FindBy(xpath="//input[@name='confirm']")
	 private WebElement confirm_checkbox;
	 
	 @FindBy(xpath="//span[contains(text(),'Value must be equal to or less than')]")
	 private WebElement refund_error_message;
	
	 @FindBy(xpath="//*[contains(text(),'Thank you, your refund was successfully completed. You will receive a refund receipt via email shortly.')]")
	 private WebElement refund_message;
	
	
	 public Result selectTransactionType(String transaction_type) {
	        WaitVisible(transaction_type_select);
	        return selectItemFromDropdownByContainsText(transaction_type_select, transaction_type, "transaction type dropdown");
	    }
	 	 
	 public Result enterSearchTerm(String keyword) {
	        WaitVisible(search_field);
	        return enterText(search_field, keyword, "CLI input");
	    }
	 
	 public Result selectSearhFieldType(String search_field_sub_type) {
	        WaitVisible(search_field_type);
	        return selectItemFromDropdownByOptionValue(search_field_type, search_field_sub_type, "transaction type dropdown");
	    }
	 
	 public Result clickSearch() {
	        WaitVisible(search_button);
	        return WebElementMethods.ClickControl(search_button, "Search button");
	    }
	 
	 public Result enterRefundAmount(String refundAmount) {
	        WaitVisible(refund_amount);
	        return enterText(refund_amount, refundAmount, "Refund Amount");
	    }
	 	 
	 public Result clickNext() {
	        WaitVisible(next_button);
	        return WebElementMethods.ClickControl(next_button, "Next button");
	    }
	 
	 public Result clickRefundButton() {
	        WaitVisible(refund_button);
	        return WebElementMethods.ClickControl(refund_button, "Refund button");
	    }
	 
	 public Result clickRefundSpannerButton() {
	        WaitVisible(refund_spanner);
	        return WebElementMethods.ClickControl(refund_spanner, "refund button clicked");
	    }
	 
	 public String getRefundType() {
	        WaitVisible(refund_type);
	        return refund_type.getText();
	    }
	 
	 public Result enterRemarks(String remarks_text) {
	        WaitVisible(remarks);
	        return enterText(remarks, remarks_text, "Entering remarks");
	    }
	 
	 public Result enterConfirmPassword(String confirm_password) {
	        WaitVisible(password_confirmation);
	        return enterText(password_confirmation, confirm_password, "confirm password");
	    }
	 
	 public Result clickConfirm() {
	       return WebElementMethods.ClickControl(confirm_checkbox, "Search button");
	    }
	 
	 public boolean refundSuccessfulMessage() {
		 WaitVisible(refund_message);
		 return refund_message.isDisplayed();
		 
	 }
	 
	 public boolean refundErrorMessage() {
		 WaitVisible(refund_error_message);
		 return refund_error_message.isDisplayed();
		 
	 }
}
