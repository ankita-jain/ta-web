package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class InsightLogin extends Base_Page {

    @FindBy(xpath = "//input[@name='submit']")
    private WebElement logIn;

    @FindBy(xpath = "//input[@name='password']")
    private WebElement password;

    @FindBy(xpath = "//input[@name='username']")
    private WebElement username;

    public InsightLogin(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickSubmitButton() {
        WaitVisible(logIn);
        return ClickControl(logIn, "Login button");
    }

    public Result enterUsername(String Username) {
        WaitVisible(username);
        return enterText(username, Username, "User name input");
    }

    public Result enterPassword(String Password) {
        WaitVisible(password);
        return enterText(password, Password, "Password input");
    }
}
