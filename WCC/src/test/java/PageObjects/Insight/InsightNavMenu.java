package PageObjects.Insight;

import PageObjects.PageClass;
import Structures.Result;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsightNavMenu extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='navheader' and contains(text(),'Account')]")
    @CacheLookup
    private WebElement Account;
    @FindBy(xpath = "//*[@class='nav'and contains(text(),'Operator Settings')]")
    @CacheLookup
    private WebElement operatorSettings;
    @FindBy(xpath = "//*[@class='subnav'and contains(text(),'Edit this operator')]")
    @CacheLookup
    private WebElement editThisOperator;
   
    

    public InsightNavMenu(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickAccount() {
        WaitVisible(Account);
        Account.click();
    }
        
    public Result clickOperatorSettings() {
    	WaitVisible(operatorSettings);
    	return ClickControl(operatorSettings, "operator link is clicked");
    }
    
    public Result clickEditThisOperator() {
    	WaitVisible(editThisOperator);
    	return ClickControl(editThisOperator, "edit this operator link is clicked");
    }
    
    public void editOperatorSettings() {
    	clickOperatorSettings();
    	clickEditThisOperator();
    }
}
