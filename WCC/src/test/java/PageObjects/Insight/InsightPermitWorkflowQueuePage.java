package PageObjects.Insight;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsightPermitWorkflowQueuePage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//tbody/tr[4]//h1")
    private WebElement pagetitle;

    public InsightPermitWorkflowQueuePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }


}
