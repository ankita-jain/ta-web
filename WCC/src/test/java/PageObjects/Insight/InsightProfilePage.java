package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class InsightProfilePage extends Base_Page {
    @FindBy(id = "deletePassword")
    private WebElement deletePasswordButton;
    @FindBy(id = "Member_PINResult")
    private WebElement memberPin;
    @FindBy(xpath = "//div[@id='dialog-password']//input[@value='Yes']")
    private WebElement submitDeleteButton;
    @FindBy(xpath = "//*[@name='editPIN']")
    private WebElement editPinButton;
    @FindBy(xpath = "//*[@name='editFrozen']")
    private WebElement editAccountStatusButton;
    @FindBy(xpath = "//*[@id='Member_PIN']")
    private WebElement pinInput;
    @FindBy(xpath = "//div[@id='dialog-pin']//input[@id='submit']")
    private WebElement submitPinButton;
    @FindBy(xpath = "//a[contains(@href, '/operator/loginasuser/')]")
    private WebElement loginAsUserButton;
    @FindBy(xpath = "//*[@name='Member_Frozen']")
    private WebElement accountStatusDropdown;
    @FindBy(xpath = "//div[@id='dialog-frozen']//input[@id='submit']")
    private WebElement submitAccountStatusButton;
    
    @FindBy(xpath = "//*[contains(text(),'View Card Audit')]")
    private WebElement cardAudit;
    

    public InsightProfilePage(WebDriver driver) {
        super(driver);
    }

    public String getMemberPIN() {
        return memberPin.getText();
    }

    public Result clickDeletePasswordButton() {
        return ClickControl(deletePasswordButton, "Delete password button");
    }

    public Result clickOnSubmitDeletePassword() {
        return ClickControl(submitDeleteButton, "Submit delete password button");
    }

    public Result clickOnEditPinButton() {
        return ClickControl(editPinButton, "Click edit pin");
    }

    public Result clickOnEditAccountStatusButton() {
        WaitVisible(editAccountStatusButton);
        return ClickControl(editAccountStatusButton, "Click edit account status");
    }

    public Result enterNewPin(String newPin) {
        WaitVisible(pinInput);
        return enterText(pinInput, newPin, "Enter new PIN");
    }

    public Result clickOnSubmitPinButton() {
        WaitVisible(submitPinButton);
        return ClickControl(submitPinButton, "Click on 'Submit' button");
    }

    public Result clickOnLoginAsUserButton() {
        WaitVisible(loginAsUserButton);
        return ClickControl(loginAsUserButton, "Click on 'Login As a User' button");
    }

    public Result selectAccountStatus(String accountStatus) {
        WaitVisible(accountStatusDropdown);
        return selectItemFromDropdownByContainsText(accountStatusDropdown, accountStatus, String.format("Choose %s account status", accountStatus));
    }

    public Result clickOnSubmitAccountStatusButton( ){
        WaitVisible(submitAccountStatusButton);
        return ClickControl(submitAccountStatusButton, "Click on 'Submit' account status button");
        
    }
    
    public Result clickOnCardAudit() {
    	 WaitVisible(cardAudit);
    	 return ClickControl(cardAudit, "Click on 'View Card Audit' Link");
    }
}

