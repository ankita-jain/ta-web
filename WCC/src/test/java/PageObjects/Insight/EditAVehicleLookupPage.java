package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class EditAVehicleLookupPage extends Base_Page {
    @FindBy(xpath = "//input[@name='yearOfManufacture']")
    private WebElement yearOfManufactureInput;

    @FindBy(xpath = "//select[@name='fuel']")
    private WebElement fuelTypeSelect;

    @FindBy(xpath = "//input[@name='co2']")
    private WebElement co2Input;

    @FindBy(xpath = "//input[@name='enginecc']")
    private WebElement engineSizeInput;

    @FindBy(xpath = "//select[@name='VehicleType']")
    private WebElement vehicleTypeSelect;

    @FindBy(xpath = "//input[@value='Save']")
    private WebElement saveButton;

    public EditAVehicleLookupPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getYearOfManufactureInput() {
        return yearOfManufactureInput;
    }

    public WebElement getFuelTypeSelect() {
        return fuelTypeSelect;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public Result selectFuelFuelType(String fuelType) {
        return selectItemFromDropdownByTextValue(fuelTypeSelect, fuelType, "Fuel type");
    }

    public Result enterYearOfManufacture(String year) {
        return enterText(yearOfManufactureInput, year, "Year Of Manufacture input");
    }

    public Result enterCO2(String co2) {
        return enterText(co2Input, co2, "CO2 input");
    }

    public Result enterEngineSize(String engineSize) {
        return enterText(engineSizeInput, engineSize, "Engine size input");
    }

    public Result selectVehicleType(String vehicleType) {
        return selectItemFromDropdownByTextValue(vehicleTypeSelect, vehicleType, "Select vehicle type");
    }

    public Result clickSave() {
        return ClickControl(saveButton, "Save button");
    }
}
