package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class InsightCorporateSummaryPage extends Base_Page {

    @FindBy(xpath = "//*[contains(text(), 'Edit')]")
    private WebElement editCorporateAccountLink;

    public InsightCorporateSummaryPage(WebDriver driver) {
        super(driver);
    }

    public Result clickOnEditCorporateAccount() {
        WaitVisible(editCorporateAccountLink);
        return ClickControl(editCorporateAccountLink, "Click on 'Edit this corporate account' link");
    }
}
