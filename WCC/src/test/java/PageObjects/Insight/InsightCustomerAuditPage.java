package PageObjects.Insight;

import static SeleniumHelpers.WebElementMethods.enterText;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;

public class InsightCustomerAuditPage extends Base_Page {
    
	public InsightCustomerAuditPage(WebDriver driver) {
		super(driver);		
	}
	 
	 String textFinder = "//*[contains(text(),'xxx')]";
	 
	 @FindBy(xpath = "//*[@name='id']")
	    private WebElement phone_number;
	 
	 @FindBy(xpath = "//*[@name='VRM']")
	    private WebElement vrm;
	 
	 @FindBy(xpath = "//*[@value='Search']")
	    private WebElement Search;
	 
	 public Result enterPhoneNumber(String phone) {
		  WaitVisible(phone_number);
		  return enterText(phone_number, phone, "CLI input entered");
	 }
	 
	 public void clearPhoneNumberField() {		 
		 WaitVisible(phone_number);
		 phone_number.clear();
		 
	 }
	 
	 public Result enterVRM(String vehicle) {
		  WaitVisible(vrm);
		  return enterText(vrm, vehicle, "VRM input entered");
	 }
   
	  public Result clickSearch() {
          return WebElementMethods.ClickControl(Search, "Customer Audit Search click");
      }
	  
	  public Boolean isThisTextVisibleNow(String text) {
		  
		 WebElement webText = _driver.findElement(By.xpath(textFinder.replace("xxx", text)));
		 return webText.isDisplayed();
	  }
	
	
	
}
