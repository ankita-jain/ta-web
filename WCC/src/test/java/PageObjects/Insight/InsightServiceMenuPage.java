package PageObjects.Insight;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsightServiceMenuPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@id='nav']/li[1]/a")
    private WebElement services;
    @FindBy(xpath = "//*[@class='nav' and contains(text(),'Permit Applications')]")
    private WebElement permitapplications;
    @FindBy(xpath = "//*[@class='nav' and contains(text(),'Exempt Vehicles List')]")
    private WebElement exemptvehicleslist;

    public InsightServiceMenuPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void clickServiceMenu() {
        WaitVisible(services);
        //jsClick(services);
        services.click();
    }

    public void clickPermitApplicationSubmenu() {
        WaitVisible(permitapplications);
        permitapplications.click();
    }
    
    public void clickExemptVehiclesSubmenu() {
        WaitVisible(exemptvehicleslist);
        exemptvehicleslist.click();
    }

    public void loadPermitSearchForm() {
        clickServiceMenu();
        clickPermitApplicationSubmenu();
    }
}
