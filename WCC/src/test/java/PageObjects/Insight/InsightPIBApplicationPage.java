package PageObjects.Insight;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InsightPIBApplicationPage extends PageClass {

    public WebDriver driver;
    /*
     * Permit personal information
     */
    @FindBy(xpath = "//*[@class='nomargin'and contains(text(),'Parking Identifier Board')]")
    @CacheLookup
    private WebElement parking_type;
    @FindBy(xpath = "//*[@id='phone_ringgo']")
    @CacheLookup
    private WebElement phone_number;
    @FindBy(xpath = "//*[@id='Title']")
    @CacheLookup
    private WebElement title;
    @FindBy(xpath = "//*[@id='FirstName']")
    @CacheLookup
    private WebElement firstname;
    @FindBy(xpath = "//*[@id='Surname']")
    @CacheLookup
    private WebElement surname;
    @FindBy(xpath = "//*[@id='ApplicantHomePhone_3798']")
    @CacheLookup
    private WebElement contact_telephone_number;
    @FindBy(xpath = "//*[@id='PIBUser_3799']")
    @CacheLookup
    private WebElement PIB_use;

    /*
     * Permit details
     */
    @FindBy(xpath = "//*[@id='PIBPurpose_3807']")
    @CacheLookup
    private WebElement PIB_purpose;
    @FindBy(xpath = "//*[@id='ServiceText_3808']")
    @CacheLookup
    private WebElement PIB_what_service;
    @FindBy(xpath = "//*[@id='AverageTime_3810']")
    @CacheLookup
    private WebElement PIB_average_parked_time;
    @FindBy(xpath = "//*[@id='PIBJobTitle_3804']")
    private WebElement PIB_Job_Title;
    @FindBy(xpath = "//*[@id='PIBEmployeeNumber_3805']")
    private WebElement PIB_employee_number;
    @FindBy(xpath = "//*[@id='PIBDepartment_3806']")
    private WebElement PIB_employee_department;
    @FindBy(xpath = "//*[@id='ParkingZone']")
    @CacheLookup
    private WebElement PIB_parking_zone;


    /*
     * Parking Location & Vehicles details
     */
    @FindBy(xpath = "//*[@id='vehicleoptions1']")
    @CacheLookup
    private WebElement PIB_VRN;
    @FindBy(xpath = "//*[@id='VehicleMake_3813']")
    @CacheLookup
    private WebElement PIB_VRN_make;
    @FindBy(xpath = "//*[@id='VehicleType_3814']")
    @CacheLookup
    private WebElement PIB_vehicle_type;
    @FindBy(xpath = "//*[@id='VehicleDriverName_3815']")
    @CacheLookup
    private WebElement PIB_vehicle_driver_name;
    @FindBy(xpath = "//*[@id='calendar']")
    @CacheLookup
    private WebElement PIB_valid_from;

    /*
     * Validity
     */
    @FindBy(xpath = "//*[@id='calendar1']")
    @CacheLookup
    private WebElement PIB_valid_to;
    @FindBy(xpath = "//*[@id='PaymentType_3811']")
    @CacheLookup
    private WebElement PIB_payment_type;
    @FindBy(xpath = "//*[@id='AuthorisedSignatory_3812']")
    @CacheLookup
    private WebElement PIB_authorised_sign;
    @FindBy(xpath = "//*[@id='SigDepartment_3948']")
    @CacheLookup
    private WebElement PIB_department;
    @FindBy(xpath = "//*[@id='Status']")
    @CacheLookup
    private WebElement PIB_status;


    /*
     *  Status
     */
    @FindBy(xpath = "//*[@id='Save']")
    @CacheLookup
    private WebElement PIB_save;

    public InsightPIBApplicationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPermitType() {
        WaitVisible(parking_type);
        return parking_type.getText();
    }

    public void enterPhoneNumber(String phone_no) {
        WaitVisible(phone_number);
        phone_number.sendKeys(phone_no);
    }

    public void selectTitle(String Title) {
        WaitVisible(title);
        new Select(title).selectByVisibleText(Title);
    }

    public void enterFristname(String first_name) {
        WaitVisible(firstname);
        firstname.sendKeys(first_name);
    }

    public void enterSurname(String sur_name) {
        WaitVisible(surname);
        surname.sendKeys(sur_name);
    }

    public void enterContactNumber(String contact) {
        WaitVisible(contact_telephone_number);
        contact_telephone_number.sendKeys(contact);
    }

    public void selectPIBUsedFor(String pib_used_for) {
        WaitVisible(PIB_use);
        new Select(PIB_use).selectByVisibleText(pib_used_for);
    }

    public void selectPIBPurpose(String pib_purpose) {
        WaitVisible(PIB_purpose);
        new Select(PIB_purpose).selectByVisibleText(pib_purpose);
    }

    public void enterJobTitle(String job_title) {
        WaitVisible(PIB_Job_Title);
        PIB_Job_Title.sendKeys(job_title);
    }

    public void enterEmployeeNumber(String employeenNo) {
        WaitVisible(PIB_employee_number);
        PIB_employee_number.sendKeys(employeenNo);
    }

    public void enterEmployeeDepartment(String employeeDept) {
        WaitVisible(PIB_employee_department);
        PIB_employee_department.sendKeys(employeeDept);
    }

    public void enterPIBService(String service) {
        WaitVisible(PIB_what_service);
        PIB_what_service.sendKeys(service);
    }

    public void selectPIBAverageParkedTime(String time) {
        WaitVisible(PIB_average_parked_time);
        //new Select(PIB_average_parked_time).selectByVisibleText(time);
        PIB_average_parked_time.sendKeys(time);
    }

    public void selectPIBParkingZone(String parking_zone) {
        WaitVisible(PIB_parking_zone);
        new Select(PIB_parking_zone).selectByVisibleText(parking_zone);
    }

    public void selectPIBVRN(String vrn) {
        WaitVisible(PIB_VRN);
        new Select(PIB_VRN).selectByValue(vrn);
    }

    public void enterVRNModel(String model) {
        WaitVisible(PIB_VRN_make);
        PIB_VRN_make.sendKeys(model);
    }

    public void enterVehicleType(String vehicle) {
        WaitVisible(PIB_vehicle_type);
        PIB_vehicle_type.sendKeys(vehicle);
    }

    public void enterDriverName(String name) {
        WaitVisible(PIB_vehicle_driver_name);
        PIB_vehicle_driver_name.sendKeys(name);
    }


    public void enterValidFrom(String valid_from) {
        WaitVisible(PIB_valid_from);
        PIB_valid_from.sendKeys(valid_from);
    }

    public void selectPaymentType(String payment_type) {
        WaitVisible(PIB_payment_type);
        new Select(PIB_payment_type).selectByVisibleText(payment_type);
    }

    public void selectAuthroisedSignatory(String signatory) {
        WaitVisible(PIB_authorised_sign);
        new Select(PIB_authorised_sign).selectByVisibleText(signatory);
    }

    public void selectDepartment(String department) {
        WaitVisible(PIB_department);
        new Select(PIB_department).selectByVisibleText(department);
    }

    public void selectPermitStatus(String status) {
        WaitVisible(PIB_status);
        new Select(PIB_status).selectByVisibleText(status);
    }

    public void clickSaveButton() {
        WaitVisible(PIB_save);
        PIB_save.click();
    }


    public void applyPIB(String phone_no, String Title, String first_name, String sur_name, String contact,
                         String pib_used_for, String job_title, String employeenNo, String employeeDept,
                         String pib_purpose, String service, String time, String parking_zone,
                         String vrn, String model, String vehicle, String name, String payment_type,
                         String signatory, String department, String status) {

        enterPhoneNumber(phone_no);
        selectTitle(Title);
        enterFristname(first_name);
        enterSurname(sur_name);
        enterContactNumber(contact);
        selectPIBUsedFor(pib_used_for);
        enterJobTitle(job_title);
        enterEmployeeNumber(employeenNo);
        enterEmployeeDepartment(employeeDept);
        selectPIBPurpose(pib_purpose);
        enterPIBService(service);
        selectPIBAverageParkedTime(time);
        selectPIBParkingZone(parking_zone);
        selectPIBVRN(vrn);
        enterVRNModel(model);
        enterVehicleType(vehicle);
        enterDriverName(name);
        selectPaymentType(payment_type);
        selectAuthroisedSignatory(signatory);
        selectDepartment(department);
        selectPermitStatus(status);
        clickSaveButton();

    }


}
