package PageObjects.Insight;

import PageObjects.PageClass;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InsightPermitApplicationPage extends PageClass {


    @FindBy(xpath = "//tbody/tr[4]//h1")
    private WebElement pagetitle;
    @FindBy(xpath = "//*[@name='tempPermitType[0]']")
    private WebElement temp_permit_vehicle_country;
    @FindBy(xpath = "//*[@name='tempPermitType[1]']")
    private WebElement temporary_permit_type;
    @FindBy(xpath = "//*[@id='vehicleoptions1']")
    private WebElement VRN;
    @FindBy(xpath = "//*[@name='Status']")
    @CacheLookup
    private WebElement operatorstatus;
    @FindBy(xpath = "//*[@id='CancelledReason']")
    @CacheLookup
    private WebElement cancellation_reason;
    @FindBy(xpath = "//*[@id='calendar']")
    @CacheLookup
    private WebElement valid_from;
    @FindBy(xpath = "//*[@id='calendar1']")
    @CacheLookup
    private WebElement valid_to;
    @FindBy(xpath = "//*[@class='mceLayout']")
    private WebElement tiny_mce_editor;
    @FindBy(xpath = "//*[@id='Save']")
    private WebElement savePermit;
    private By applicationNotesFrame = By.xpath("//iframe[@id='ApplicantNotes_ifr']");
    
    @FindBy(xpath = "//textarea[@rows='3']")
    private WebElement permitProofReasonField;
    
    private String ptable = "//*[@class='proofsformsection']//tr[xxxxxx]//td[2]";
    @FindBy(xpath = "TBD")
    private WebElement rejectReasonMessage;
    
    @FindBy(linkText = ("Accepted"))
    private WebElement AcceptReason;

    @FindBy(xpath = ("//*[@class='proofsformsection']//option[3]"))
    private WebElement RejectReason;
    
    @FindBy(xpath = "//*[@class='proofsformsection']/table/tbody/tr[2]/td[2]/p/select")
    private WebElement proofTables;
    
    private String proofTableToReject = "//*[@id='PermitForm']/div/table/tbody//td/div/table/tbody/tr[xxxxxx]/td[2]/p/select";
    
    @FindBy(xpath = "//*[@name='tempPermitType[0]']")
    private WebElement tempPermitTypeDropDown;
    
    @FindBy(xpath = "//td[@class=\"mceIframeContainer mceFirst mceLast\"]")
    private WebElement responseToCustomer;
    
    @FindBy(xpath = "//*[@id='DeclineReason']")
    @CacheLookup
    private WebElement declined_reason;
    
    @FindBy(xpath = "//*[@name='submits[submit]']")
    private WebElement EndPermitSession;
	
    
    


    public InsightPermitApplicationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public String getPermitValidFromDate() {
    	WaitVisible(valid_from);
        return valid_from.getAttribute("value");
    }

    public String getPermitValidToDate() {
    	WaitVisible(valid_to);
        return valid_to.getAttribute("value");
    }

    public void selectVehicleCountry(String options) {
    	WaitVisible(temp_permit_vehicle_country);
        new Select(temp_permit_vehicle_country).selectByVisibleText(options);
    }

    public void selectVRN(String vrn) {
    	WaitVisible(VRN);
        new Select(VRN).selectByValue(vrn);
    }


    public void selectPermitType(String permittype) {
    	WaitVisible(temporary_permit_type);
        new Select(temporary_permit_type).selectByVisibleText(permittype);
    }

    public void editPermitStatus(String permitstatus) {
        WaitVisible(operatorstatus);
        new Select(operatorstatus).selectByVisibleText(permitstatus);
    }

    public void selectCancellationReason(String reason) {
    	WaitVisible(cancellation_reason);
        new Select(cancellation_reason).selectByVisibleText(reason);
    }

    public void clickSavePermit() {
        WaitVisible(savePermit);
        savePermit.click();
    }

    public boolean isTinyMCEEditorVisible() {
    	WaitVisible(tiny_mce_editor);
        return tiny_mce_editor.isDisplayed();
    }

    public void editPermitStatusAndSave(String permitstat) {

        editPermitStatus(permitstat);
        clickSavePermit();
    }

    public void editPermitStatusToTempPermitAndSave(String permitstat, String response) {
        editPermitStatus(permitstat);
        enterResponseToCustomer(response);
        clickSavePermit();
    }

    public void editEcoPermitStatusToTempPermitAndSave(String permitstat, String country, String permit_type, String response_to_customer) {
        editPermitStatus(permitstat);
        selectVehicleCountry(country);
        selectPermitType(permit_type);
        if (isTinyMCEEditorVisible()) {
            enterResponseToCustomer(response_to_customer);
        }

        clickSavePermit();
    }

    public void cancelPermitFromInsight(String permitstatus, String cancellation_reason, String response_to_customer) {
        editPermitStatus(permitstatus);
        selectCancellationReason(cancellation_reason);
        if (isTinyMCEEditorVisible()) {
            enterResponseToCustomer(response_to_customer);
        }

        clickSavePermit();
    }

    public void enterResponseToCustomer(String response) {
    	WaitVisible(applicationNotesFrame);
        ((JavascriptExecutor) driver).executeScript("tinyMCE.activeEditor.setContent('" + response + "');");
    }
    
    /**
     * Function to get a value from the permit proof table.
     * @return
     */
    public String getValueFromPermitProofTable(String status) {
    	
    	WebElement permitStatus = prepareWebElementWithDynamicXpath(ptable, status);
    	WaitVisible(permitStatus);
		return permitStatus.getText();	
    }
    
    public void enterRejectReason(String rejectReason) {
    	WaitVisible(permitProofReasonField);
    	permitProofReasonField.sendKeys(rejectReason);
    }
    
    public boolean rejectReasonValidationMessage()
    {
    	WaitVisible(rejectReasonMessage);
    	return rejectReasonMessage.isDisplayed();
    }
    
    public void selectReasonForPemitProof(String x,String reason)
    {
    	WebElement proofTables = prepareWebElementWithDynamicXpath(proofTableToReject, x);
    	WaitVisible(proofTables);
    	new Select(proofTables).selectByVisibleText(reason);
    }
    
    public void selectATempPermitType(String reason)
    {
    	WaitVisible(tempPermitTypeDropDown);
    	tempPermitTypeDropDown.click();
    	new Select(tempPermitTypeDropDown).selectByVisibleText(reason);
    	clickSavePermit();
    }
    
    public void completeTempPermit(String response, String reason)
    {
    	WaitVisible(responseToCustomer);
    	responseToCustomer.sendKeys(response);
    	selectATempPermitType(reason);
    }
    
    public void fillOutResponseToCustomer(String response_to_customer)
    {
    	 if (isTinyMCEEditorVisible()) {
             enterResponseToCustomer(response_to_customer);
         }
    }
    
    public void selectDeclinedReason(String reason) {
    	WaitVisible(declined_reason);
        new Select(declined_reason).selectByVisibleText(reason);
    }
    
    public void endSession()
    {
    	WaitVisible(EndPermitSession);
    	EndPermitSession.click();
    }
}