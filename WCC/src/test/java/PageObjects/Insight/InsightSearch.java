package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class InsightSearch extends Base_Page {

    public WebDriver driver;
    
    @FindBy(name = "searchterm")
    private WebElement searchfield;
    @FindBy(name = "type")
    private WebElement searchtype;  
    @FindBy(xpath = "//input[@value='Search']")
    private WebElement submitbutton;
    @FindBy(xpath = "//a[@class='opSelectLink']")
    private WebElement changeoperator;
    @FindBy(xpath = "//select[@name='operator[0]']")
    private WebElement operatorstatus;
    @FindBy(xpath = "//select[@name='operator[1]']")
    private WebElement operator;
    @FindBy(xpath = "//input[@value='Select']")
    private WebElement selectbutton;
    @FindBy(xpath="//*[@id='Member_PINResult']")
    private WebElement pinElement;
    @FindBy(xpath="//*[@id='Customer']/fieldset/legend/h2")
    private WebElement customerDetails;
    @FindBy(xpath="//*[@id='Member_CLIResults']")
    private WebElement customerDetailsCLI;
    @FindBy(xpath="//*[@id='Member_EmailResults']")
    private WebElement customerDetailsEmail;
    @FindBy(xpath="//*[contains(text(),'Found 2 users matching search criteria.')]")
    private WebElement searchResultMultipleUsers;
    @FindBy(xpath="//*[@class='nav'and contains(text(),'Refund')]")
    private WebElement refund;
    @FindBy(xpath="//*[contains(text(),'Add a new Customer')]")
    private WebElement addCustomerLink;
    @FindBy(xpath="//*[@id='actionsinner']/p[1]/a")
    private WebElement actionsDeleteLink;
    @FindBy(xpath="//*[@id='Member_StatusResult']")
    private WebElement customerStatus;
    @FindBy(xpath=" //a[contains(text(),'Support')]")
    private WebElement Support;

    public InsightSearch(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result enterSearchTerm(String keyword) {
        WaitVisible(searchfield);
        return enterText(searchfield, keyword, "User name input");
    }
    
    public Boolean isSearchResultVisible() {
        WaitVisible(customerDetails);
        return customerDetails.isDisplayed();
    }
    
    public Boolean isSearchResultMultipleUserVisible() {
        WaitVisible(searchResultMultipleUsers);
        return searchResultMultipleUsers.isDisplayed();
    }
    
    public String getUserCLI() {
        WaitVisible(customerDetailsCLI);
        return customerDetailsCLI.getText();
    }
    
    public String getUserEmail() {
        WaitVisible(customerDetailsEmail);
        return customerDetailsEmail.getText();
    }

    public Result selectSearchType(String searchbytype) {
        WaitVisible(searchtype);
        return selectItemFromDropdownByContainsText(searchtype, searchbytype, "Search Type");
    }

    public Result clickSubmit() {
        WaitVisible(submitbutton);
        return WebElementMethods.ClickControl(submitbutton, "Submit button");
    }
    
    public Result clickRefund() {
        WaitVisible(refund);
        return WebElementMethods.ClickControl(refund, "Refund button clicked");
    }
    
    public Result clickSupport() {
        WaitVisible(Support);
        return WebElementMethods.ClickControl(Support, "Support button clicked");
    }

    public void insightSearch(String searchkeyword, String searchbytype) {
        enterSearchTerm(searchkeyword);
        selectSearchType(searchbytype);
        clickSubmit();
    }

    public Result clickChangeOperator() {
        WaitVisible(changeoperator);
        return ClickControl(changeoperator, "Change Operator button");
    }

    public Result selectOperaotrStatus(String operatorstatusoption) {
        WaitVisible(operatorstatus);
        return selectItemFromDropdownByContainsText(operatorstatus, operatorstatusoption, "Operator Status");
    }

    public Result selectOperator(String operatorval) {
        WaitVisible(operator);
        return selectItemFromDropdownByContainsText(operator, operatorval, "Operator dropdown");
    }

    public Result clickSelectButton() {
        WaitVisible(selectbutton);
        return ClickControl(selectbutton, "Select button");
    }
    
    public Boolean isAddNewCustomerLinkVisible() {
        WaitVisible(addCustomerLink);
        return addCustomerLink.isDisplayed();
    }
    
    public Result clickAddNewCustomer() {
        WaitVisible(addCustomerLink);
        return ClickControl(addCustomerLink, "Add a new Customer button");
    }

    public Result clickDeleteLink() {
        WaitVisible(actionsDeleteLink);
        return ClickControl(actionsDeleteLink, "Click the Delete User link");
    }
    
    public void selectOperatorProcess(String operatorstatus, String operator) {
        clickChangeOperator();
        selectOperaotrStatus(operatorstatus);
        selectOperator(operator);
        clickSelectButton();
    }

    public String getPinElementText() {
        WaitVisible(pinElement);
        return pinElement.getText();
    }
    
    public String getCustomerStatusText() {
        WaitVisible(customerStatus);
        return customerStatus.getText();
    }
}
