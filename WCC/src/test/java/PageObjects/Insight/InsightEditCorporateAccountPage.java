package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class InsightEditCorporateAccountPage extends Base_Page {
    @FindBy(id = "menu_Setup")
    private WebElement setupTab;

    public SetupTab setupPage = new SetupTab(_driver);

    public InsightEditCorporateAccountPage(WebDriver driver) {
        super(driver);
    }

    public Result clickOnSetupTab() {
        WaitVisible(setupTab);
        return ClickControl(setupTab, "Click on 'Setup' tab");
    }

    public class SetupTab {
        private WebElement jobReferenceCheckbox = _driver.findElement(By.name("AllowJobReference"));

        @FindBy(xpath = "//*[@name='singleCLI']/../../preceding-sibling::td")
        private WebElement singleCliLabel;
        @FindBy(name = "singleCLI")
        private WebElement singleCliInput;
        @FindBy(xpath = "//*[@name='cliLimit']/../../preceding-sibling::td")
        private WebElement multipleCliLabel;
        @FindBy(name = "cliLimit")
        private WebElement multipleCliInput;
        @FindBy(xpath = "//*[@name='anyCLI']/../../preceding-sibling::td")
        private WebElement anyCliLabel;
        @FindBy(name = "anyCLI")
        private WebElement anyCliInput;
        @FindBy(name = "submits[submit]")
        private WebElement saveButton;

        @FindBy(xpath = "//p[contains(text(),'CLI Settings')]//preceding-sibling::*[@class='nomargin error']")
        private WebElement cliSettingsError;
        @FindBy(xpath = "//*[@name='cliLimit']/../preceding-sibling::*[@class='nomargin error']")
        private WebElement multipleCliError;

        public SetupTab(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public boolean isJobReferenceDisplayed() {
            return jobReferenceCheckbox.isEnabled();
        }

        public String getSingleCliLabelText() {
            return singleCliLabel.getText();
        }

        public String getMultipleCliLabelText() {
            return multipleCliLabel.getText();
        }

        public String getAnyCliLabelText() {
            return anyCliLabel.getText();
        }

        public String getSingleCliInputType() {
            return singleCliInput.getAttribute("type");
        }

        public String getMultipleCliInputType() {
            return multipleCliInput.getAttribute("type");
        }

        public String getAnyCliInputType() {
            return anyCliInput.getAttribute("type");
        }

        public Result selectAnyCheckbox() {
            return checkCheckBox(anyCliInput, "'Any' checkbox");
        }

        public Result unselectAnyCheckbox() {
            return UncheckCheckBox(anyCliInput, "'Any' checkbox");
        }
        public Result selectSingleCheckbox() {
            return checkCheckBox(singleCliInput, "'Single' checkbox");
        }

        public Result unselectSingleCheckbox() {
            return UncheckCheckBox(singleCliInput, "'Single' checkbox");
        }

        public Result clickOnSaveButton() {
            return ClickControl(saveButton, "'Save' button");
        }

        public String getCliSettingsErrorMessage() {
            WaitVisible(cliSettingsError);
            return cliSettingsError.getText();
        }

        public Result enterMultipleCliAmount(String cliAmount) {
            return enterText(multipleCliInput, cliAmount, "Enter cli amount");
        }

        public String getMultipleCliError() {
            WaitVisible(multipleCliError);
            return multipleCliError.getText();
        }
    }
}
