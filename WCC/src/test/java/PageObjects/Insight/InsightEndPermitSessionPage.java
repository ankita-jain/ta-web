package PageObjects.Insight;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsightEndPermitSessionPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//tbody/tr[4]//h1")
    @CacheLookup
    private WebElement pagetitle;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement end_session;

    public InsightEndPermitSessionPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public void clickEndSession() {
        WaitVisible(end_session);
        end_session.click();
    }

}
