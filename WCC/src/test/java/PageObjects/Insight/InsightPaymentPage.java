package PageObjects.Insight;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InsightPaymentPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//tbody/tr[4]//h1")
    @CacheLookup
    private WebElement insightPaymentPageTitle;
    @FindBy(xpath = "//*[@id='paymentType']")
    @CacheLookup
    private WebElement payment_type;
    @FindBy(xpath = "//*[contains(text(),'Visa')]//preceding::input[1]")
    @CacheLookup
    private WebElement existing_card;
    @FindBy(xpath = "//*[@name='cardType[type]' and @value= '2']")
    @CacheLookup
    private WebElement new_card;
    @FindBy(xpath = "//*[@id='cardSelect']")
    @CacheLookup
    private WebElement payment_card_details;
    @FindBy(xpath = "//input[@name='securityCode']")
    @CacheLookup
    private WebElement cv2;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    @CacheLookup
    private WebElement submit_button;
    @FindBy(xpath = "//*[@id='paymentConfirmButton']")
    @CacheLookup
    private WebElement payment_confirmation_button;    
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    @CacheLookup
    private WebElement submit_cancel;

    public InsightPaymentPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getInsightPaymentPageTitle() {
        WaitVisible(insightPaymentPageTitle);
        return insightPaymentPageTitle.getText();
    }
    
    public void clickPaymentConfirmationButton()
    {
    	WaitVisible(payment_confirmation_button);
    	payment_confirmation_button.click();
    }

    public void selectPaymentType(String paymenttype) {
        WaitVisible(payment_type);
        new Select(payment_type).selectByVisibleText(paymenttype);
    }

    public void clickExistingCard() {
        WaitVisible(existing_card);
        existing_card.click();
    }

    public void clickNewCard() {
        WaitVisible(new_card);
        new_card.click();
    }

    public void selectCardDetails(String card_details) {
        WaitVisible(payment_card_details);
        new Select(payment_card_details).selectByVisibleText(card_details);
    }

    public void enterCV2(String cvv) {
        WaitVisible(cv2);
        cv2.sendKeys(cvv.split("\\.")[0]);
    }

    public void clickSubmitButton() {
        WaitVisible(submit_button);
        submit_button.click();
    }

    public void clickCancelButton() {
        WaitVisible(submit_cancel);
        submit_cancel.click();
    }


    public void payForPermitCharges(String paymenttype, String credit_card, String exp_month, String exp_year, String cvv) {
        selectPaymentType(paymenttype);
        clickExistingCard();
        clickSubmitButton();
        //selectCardDetails(maskCreditCard(credit_card, exp_month, exp_year));
        enterCV2(cvv);
        clickPaymentConfirmationButton();
    }


}
