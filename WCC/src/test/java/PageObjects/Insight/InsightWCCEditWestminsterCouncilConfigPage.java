package PageObjects.Insight;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.checkCheckBox;

import PageObjects.PageClass;
import Structures.Result;

public class InsightWCCEditWestminsterCouncilConfigPage extends PageClass {

	@FindBy(id = "//*[@id='CouncilTaxLookup']")
    @CacheLookup
    private WebElement councilTaxLookup;
	
	@FindBy(id = "//*[@id='menu_Eligibility_Proofs']")
	@CacheLookup
	private WebElement eligibilityProofsTab;
	
	@FindBy(id = "//*[@id='menu_Permits']")
    @CacheLookup
    private WebElement permitsTab;
	
	@FindBy(xpath = "//*[@href=\"/permittypeconfig/?action=edit&opid=42152&typeid=H\"]")
    @CacheLookup
    private WebElement residentEditLink;
	
	@FindBy(name = "//*[@name=\"submits[submit]\"]")
    @CacheLookup
    private WebElement saveButton;
	
	
	public InsightWCCEditWestminsterCouncilConfigPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result clickEligibilityProofsTab() {
    	WaitVisible(eligibilityProofsTab);
    	return ClickControl(eligibilityProofsTab, "eligibility proofs tab is clicked");
    }

    public Result clickPermitTab() {
    	WaitVisible(permitsTab);
    	return ClickControl(permitsTab, "permits tab is clicked");
    }
    
    public Result clickResidentLink() {
    	WaitVisible(residentEditLink);
    	return ClickControl(residentEditLink, "edit resident link is clicked");
    }
    
    /**
     * Function to check uncheck the council tax look up
     * @return
     */
    public Result checkUncheckCouncilTaxLookUp()
    {
    	clickPermitTab();
    	clickResidentLink();
    	clickEligibilityProofsTab();
    	WaitVisible(councilTaxLookup);
    	return checkCheckBox(councilTaxLookup, "council tax look up checkbox");
    }
    
    public Result saveChanges() {
    	WaitVisible(saveButton);
    	return ClickControl(councilTaxLookup, "Save button is clicked");
    }
}
