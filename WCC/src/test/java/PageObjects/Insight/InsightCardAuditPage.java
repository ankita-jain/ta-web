package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class InsightCardAuditPage extends Base_Page {
	
	@FindBy(xpath = "//*[@id=\"main\"]/table/tbody/tr[4]/td[2]/table/tbody/tr/td[3]/h1")
    private static WebElement pageHeader;

	public InsightCardAuditPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	 public String getCardAuditHeader() {
	        WaitVisible(pageHeader);
	        return pageHeader.getText();
	    }

}
