package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WebElementMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SupportPage extends Base_Page {
    private LeftSideBar leftSideBar = new LeftSideBar(_driver);

    public SupportPage(WebDriver driver) {
        super(driver);
    }

    public LeftSideBar getLeftSideBar() {
        return leftSideBar;
    }

    public class LeftSideBar {
        @FindBy(xpath = "//p/a[text()='VRN Lookup']")
        private WebElement vrnLookupLink;
        
        @FindBy(xpath = "//p/a[text()='Corporate']")
        private WebElement corporate;
        
        @FindBy(xpath = "//p/a[text()='Customer Audit']")
        private WebElement customerAudit;
        
        @FindBy(xpath = "//p/a[text()='Callcentre Form']")
        private WebElement callCentreForm;

        @FindBy(xpath = "//p/a[text()='Search Vehicle']")
        private WebElement searchVehicle;

        private LeftSideBar(WebDriver driver) {
            PageFactory.initElements(driver, this);
        }

        public WebElement getVrnLookupLink() {
            return vrnLookupLink;
        }
        
        public Result clickCustomerAudit() {
            return WebElementMethods.ClickControl(customerAudit, "Customer Audit link");
        }
        
        public Result clickCallCentreForm() {
            return WebElementMethods.ClickControl(callCentreForm, "Call Centre  link");
        }

        public Result clickVrnLookup() {
            return WebElementMethods.ClickControl(vrnLookupLink, "VRN lookup link");
        }
        
        public Result clickCorporate() {
            return WebElementMethods.ClickControl(corporate, "Corporate link");
        }

        public Result clickOnSearchVehicle() {
            return WebElementMethods.ClickControl(searchVehicle, "Search Vehicle Link");
        }
    }
}
