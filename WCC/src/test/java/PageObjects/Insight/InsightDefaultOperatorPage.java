package PageObjects.Insight;

import PageObjects.PageClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InsightDefaultOperatorPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//td[@class='white']//script[2]//following::h1")
    @CacheLookup
    private WebElement pageheader;
    @FindBy(xpath = "//input[@name='Parking_Zone']")
    @CacheLookup
    private WebElement searchzone;
    
    public InsightDefaultOperatorPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageHeader() {
        WaitVisible(searchzone);
        return pageheader.getText();
    }
}
