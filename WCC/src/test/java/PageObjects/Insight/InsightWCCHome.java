package PageObjects.Insight;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class InsightWCCHome extends Base_Page {

    @FindBy(xpath = "//li[@class='clicked_on']//a[@href='/operator/collections/']")
    @CacheLookup
    private WebElement servicemenu;
    @FindBy(xpath = "(//ul[@id='nav']//a[@href='/operator/campaign/'])[1]")
    @CacheLookup
    private WebElement supportMenu;
    @FindBy(xpath = "//*[@id='nav']/li[15]/a")
    @CacheLookup
    private WebElement account;
    @FindBy(xpath = "//div[@id='ErrorMessage']//following::h1[1]")
    @CacheLookup
    private WebElement pagetitle;
    @FindBy(xpath = "//li[@class='clicked_on']//a[@href='/operator/permits/']")
    @CacheLookup
    private WebElement permitapplicationmenu;
    @FindBy(xpath = "//*[@name='Status']")
    @CacheLookup
    private WebElement permitstatus;
    @FindBy(xpath = "//li[@class='dropit-trigger']/a")
    @CacheLookup
    private WebElement add_new_permit_button;
    @FindBy(xpath = "//a[contains(@href,'typeId=PIB')]")
    @CacheLookup
    private WebElement PIB;
    @FindBy(xpath = "//*[@name='submit' and @value='Search']")
    @CacheLookup
    private WebElement permitsearchbutton;
    @FindBy(xpath = "//*[@name='CLI']")
    @CacheLookup
    private WebElement CLI;
    @FindBy(xpath = "//*[@name='VRM']")
    @CacheLookup
    private WebElement VRM;
    @FindBy(xpath = "//input[@name='PermitID']")
    @CacheLookup
    private WebElement permitid;
    @FindBy(xpath = "//*[@id='main']/table/tbody/tr[4]/td[2]/table/tbody/tr/td[1]/table/tbody/tr[17]/td[2]/p/a")
    @CacheLookup
    private WebElement permitapplicationsubmenulink;
    @FindBy(xpath = "//tr[@id='1']/td[2]")
    @CacheLookup
    private WebElement permit_number;
    @FindBy(xpath = "//*[@id='1']/td[20]/p/a/img")
    @CacheLookup
    private WebElement editpermit;

    public InsightWCCHome(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public Result permitStatusChangeTo(String permit) {
        WaitVisible(permitstatus);
        return selectItemFromDropdownByTextValue(permitstatus, permit, "Permit Status dropdown");
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public Result clickPermitApplicationFromServiceMenu() {
        clickServiceMenu();
        return ClickControl(permitapplicationmenu, "Permit Application menu button");
    }

    public Result clickAddNewPermitMenu() {
        WaitVisible(add_new_permit_button);
        return ClickControl(add_new_permit_button, "Add new permit menu button");
    }

    public Result clickPIB() {
        clickAddNewPermitMenu();
        return ClickControl(PIB, "PIB button");
    }

    public Result searchPermit() {
        WaitVisible(permitsearchbutton);
        return ClickControl(permitsearchbutton, "Permit search button");
    }

    public Result enterVRM(String vrm) {
        WaitVisible(VRM);
        return enterText(VRM, vrm, "VRM input");
    }

    public Result enterCLI(String cli) {
        WaitVisible(CLI);
        return enterText(CLI, cli, "CLI input");
    }

    public Result inputPermitId(String permitnumber) {
        WaitVisible(permitid);
        return enterText(permitid, permitnumber, "Permit id input");
    }

    public Result clickServiceMenu() {
        WaitVisible(servicemenu);
        return ClickControl(servicemenu, "Service menu button");
    }

    public Result clickSupportMenu() {
        WaitVisible(supportMenu);
        return ClickControl(supportMenu, "Support menu button");
    }

    public Result clickPermitApplicationSubmenu() {
        WaitVisible(permitapplicationsubmenulink);
        return ClickControl(permitapplicationsubmenulink, "Permit application submenu link");
    }

    public Result clickEditPermit() {
        WaitVisible(editpermit);
        return ClickControl(editpermit, "Edit permit button");
    }

    public String getPermitNumber() {
        WaitVisible(permit_number);
        return permit_number.getText();
    }

    /*
     * Some of the disable resident permit 'disable permit BLUE BADGE' doesn't have VRM(VRN),
     * so in those case we need CLI to find the permit from insight
     */
    public Result loadPermitSearchForm() {
        clickServiceMenu();
        return clickPermitApplicationSubmenu();
    }
}
