package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static SeleniumHelpers.WebElementMethods.selectItemFromDropdownByContainsText;

public class WCCPermitManageVehiclesPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement manageVehiclePageTitle;
    @FindBy(xpath = "//*[contains(text(),'TEMPORARY CHANGES')]")
    @CacheLookup
    private WebElement temporary_changes_tab;
    @FindBy(xpath = "//*[contains(text(),'PERMANENT CHANGES')]")
    @CacheLookup
    private WebElement permanent_changes_tab;
    @FindBy(xpath = "//*[@name='tempVRNChange'][@value='0']")
    @CacheLookup
    private WebElement permanent_changes;
    @FindBy(xpath = "//*[@name='tempVRNChange'][@value='1']")
    @CacheLookup
    private WebElement temporary_changes;
    @FindBy(xpath = "//*[@id='tempVRNSelector']")
    @CacheLookup
    private WebElement vehicle_to_edit;
    @FindBy(xpath = "//*[@id='field-vehicleoptions1']")
    @CacheLookup
    private WebElement permanent_vrn;
    @FindBy(xpath = "//*[@id='field-OwnershipId1']")
    @CacheLookup
    private WebElement permanent_vrn_ownership;
    @FindBy(xpath = "//*[@id='field-vehicleoptions1']")
    @CacheLookup
    private WebElement temp_vrn;
    @FindBy(xpath = "//*[@value='Next Step']")
    @CacheLookup
    private WebElement next_step;

    public WCCPermitManageVehiclesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getManageVehicleTitle() {
        WaitVisible(manageVehiclePageTitle);
        return manageVehiclePageTitle.getText();
    }

    public void clickTemporaryVRNChangeTab() {
        WaitVisible(temporary_changes_tab);
        temporary_changes_tab.click();
    }

    public void clickPermanentVRNChangeTab() {
        WaitVisible(permanent_changes_tab);
        permanent_changes_tab.click();
    }

    public void tempVRNChange() {
        jsClick(temporary_changes);
    }

    public void permVRNChange() {
        jsClick(permanent_changes);
    }

    public void selectVehicleTOEdit(String vrn) {
        WaitVisible(vehicle_to_edit);
        new Select(vehicle_to_edit).selectByVisibleText(vrn);
    }

    public void selectNewVrnToReplaceOldVrn(String vrn) {
        WaitVisible(temp_vrn);
        new Select(temp_vrn).selectByVisibleText(vrn);
    }

    public void selectNewVrnToReplaceOldVrnPermanently(String vrn) {
        WaitVisible(permanent_vrn);
        new Select(permanent_vrn).selectByVisibleText(vrn);
    }

    public void selectOwnershipOfVRN(String ownership) {
        WaitVisible(permanent_vrn_ownership);
        new Select(permanent_vrn_ownership).selectByVisibleText(ownership);
    }

    public void clickNextStep() {
        WaitVisible(next_step);
        next_step.click();
    }

    public void changeTemporaryVRNChangesProcess(String vrn, String new_vrn) {
        clickTemporaryVRNChangeTab();
        tempVRNChange();
        selectVehicleTOEdit(vrn);
        selectItemFromDropdownByContainsText(temp_vrn, new_vrn, "Dropdown option");
        clickNextStep();
    }

    public void changePermanentVRNChangesProcess(String vrn, String ownership) {
        clickPermanentVRNChangeTab();
        permVRNChange();
        selectNewVrnToReplaceOldVrnPermanently(vrn);
        selectOwnershipOfVRN(ownership);
        clickNextStep();
    }


}
