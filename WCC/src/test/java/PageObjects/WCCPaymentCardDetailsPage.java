package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class WCCPaymentCardDetailsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
    @FindBy(xpath = "//*[@name='cardType[type]'][@value=1]")
    @CacheLookup
    private WebElement existing_card;
    @FindBy(xpath = "//*[@class='elemErrorWrap']/label[1]")
    @CacheLookup
    private WebElement existing_card_tab;
    @FindBy(xpath = "//*[@class='elemErrorWrap']/label[2]")
    @CacheLookup
    private WebElement new_card_tab;
    @FindBy(xpath = "//*[@name='cardType[type]'][@value=2]")
    @CacheLookup
    private WebElement new_card;
    /*@FindBy(xpath = "//*[@id='field-cardSelect']")
    @CacheLookup
    private WebElement exiting_card_number;*/
    @FindBy(xpath = "//*[contains(text(),'Visa')]//preceding::input[1]")
    @CacheLookup
    private WebElement exiting_card_number;
    @FindBy(xpath = "//*[@name='Member_Ccnumber']")
    @CacheLookup
    private WebElement new_card_number;
    @FindBy(xpath = "//*[@name='Member_Ccexpire[M]']")
    @CacheLookup
    private WebElement new_card_number_expiry_month;
    @FindBy(xpath = "//*[@name='Member_Ccexpire[Y]']")
    @CacheLookup
    private WebElement new_card_number_expiry_year;
    @FindBy(xpath = "//*[@value='Next Step']")
    private WebElement next_button;
    @FindBy(xpath = "//*[@name='labyrinth_CardSelect_back']")
    private WebElement previous_button;
    @FindBy(xpath = "//*[@name='labyrinth_cancel']")
    private WebElement cancel_button;
    @FindBy(xpath = "//*[contains(text(),'ending 0789')]//preceding::input[1]")
    private WebElement secondCard;

    public WCCPaymentCardDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public void selectExistingPaymentCard() {
        WaitVisible(existing_card);
        jsClick(existing_card);
    }

    public void clickExistingCardTab() {
        WaitVisible(existing_card_tab);
        existing_card_tab.click();
    }

    public void clickNewCardTab() {
        WaitVisible(new_card_tab);
        new_card_tab.click();
    }

    public void selectExistingPaymentCardinAccount(String masked_card_details) {
        WaitVisible(exiting_card_number);
        new Select(exiting_card_number).selectByVisibleText(masked_card_details);
    }
    
    public void clickExistingPaymentCardinAccount()
    {
          WaitVisible(exiting_card_number);
    	  exiting_card_number.click();
    }

    public void selectNewPaymentCard() {
        WaitVisible(new_card);
        jsClick(new_card);
    }

    public void enterNewPaymentCradNumber(String newcardnumber) {
        WaitVisible(new_card_number);
        new_card_number.sendKeys(newcardnumber);
    }

    public void enterNewPaymentCardExpiryMonth(String expirymonth) {
        WaitVisible(new_card_number_expiry_month);
        Select expiry_month = new Select(new_card_number_expiry_month);
        expiry_month.selectByVisibleText(expirymonth);
    }

    public void enterNewPaymentCardExpiryYear(String expiryyear) {
        WaitVisible(new_card_number_expiry_year);
        Select expiry_year = new Select(new_card_number_expiry_year);
        expiry_year.selectByVisibleText(expiryyear);
    }

    public Result clickNextButton() {
        WaitVisible(next_button);
    	return ClickControl(next_button, "click next button");
    }

    public Result proceedToPaymentWithExistingCard() {
    	clickExistingPaymentCardinAccount();
        WaitVisible(next_button);
        return ClickControl(next_button, "Click next button after paying with card on account");
    }

    public void proceedToPaymentWithExistingCardInAccount(String credit_card, String exp_month, String exp_year) {
        //clickExistingCardTab();
        //selectExistingPaymentCard();
       //selectExistingPaymentCardinAccount(maskCreditCard(credit_card, exp_month, exp_year));
    	clickExistingPaymentCardinAccount();
        clickNextButton();
    }
    
    public Result selectSecondCardOnAccount()
    {
        WaitVisible(secondCard);
    	secondCard.click();
        WaitVisible(next_button);
    	return ClickControl(next_button, "Click next button after paying with card on account");
    }
}
