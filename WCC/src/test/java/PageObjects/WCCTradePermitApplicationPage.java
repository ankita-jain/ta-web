package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCTradePermitApplicationPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement trade_permit_application_page_Title;
    @FindBy(id="field-Member_Title")
    private WebElement title;
    @FindBy(id="field-Member_Firstname")
    private WebElement first_name;
    @FindBy(id="field-Member_Lastname")
    private WebElement surname;
    @FindBy(xpath = "//*[@id='field-ParkingZone']")
    @CacheLookup
    private WebElement trade_permit_zones;
    @FindBy(xpath = "//*[@id='Trade2']")
    @CacheLookup
    private WebElement trade_permit_reason;
    @FindBy(xpath = "//*[@id='field-vehicleoptions1']")
    @CacheLookup
    private WebElement trade_permit_vehicle;
    @FindBy(xpath = "//*[@name='Terms']")
    @CacheLookup
    private WebElement terms;
    @FindBy(xpath = "//*[@name='labyrinth_Options_next']")
    @CacheLookup
    private WebElement next_button;

    public WCCTradePermitApplicationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTradePermitApplicationPageTitle() {
        WaitVisible(trade_permit_application_page_Title);
        return trade_permit_application_page_Title.getText();
    }
    
    public void selectTitle(String title_string) {
    	WaitVisible(title);
    	new Select(title).selectByVisibleText(title_string);
    	
    }

    public void selectTradePermitZone(String zone) {
        WaitVisible(trade_permit_zones);
        new Select(trade_permit_zones).selectByVisibleText(zone);
    }

    public void enterTradePermitReason(String reason) {
        WaitVisible(trade_permit_reason);
        trade_permit_reason.sendKeys(reason);
    }
    
    public void enterFirstName(String name) {
    	WaitVisible(first_name);
    	first_name.sendKeys(name);
    }
    
    public void enterSecondName(String name) {
    	WaitVisible(surname);
    	surname.sendKeys(name);
    }

    public void selectTradePermitVehicle(String vrn) {
        WaitVisible(trade_permit_vehicle);
        new Select(trade_permit_vehicle).selectByVisibleText(vrn);
    }

    public void clickTerms() {
        jsClick(terms);
    }

    public void clickNextButton() {
        WaitVisible(next_button);
        next_button.click();
    }


    public void applyTradePermit(String title,String firstName, String surname, String zone, String reason, String vrn) {
    	selectTitle(title);
    	enterFirstName(firstName);
    	enterSecondName(surname);
        selectTradePermitZone(zone);
        enterTradePermitReason(reason);
        selectTradePermitVehicle(vrn);
        clickTerms();
        clickNextButton();
    }


}
