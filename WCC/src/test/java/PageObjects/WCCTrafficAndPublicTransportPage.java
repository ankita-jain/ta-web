package PageObjects;

import SeleniumHelpers.Base_Page;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCTrafficAndPublicTransportPage extends Base_Page {
    @FindBy(xpath = "//div[@class='permitlinkslight']//h3[text()='Traffic News']")
    private WebElement trafFicNewsLink;
    @FindBy(xpath = "//div[@class='permitlinkslight']//h3[text()='Bus Timetable and Routes']")
    private WebElement busTimetableAndRouters;
    @FindBy(xpath = "//div[@class='permitlinkslight']//h3[text()='Tube, DLR and Overground']")
    private WebElement tubeDlrLink;
    @FindBy(xpath = "//div[@class='permitlinkslight']//h3[text()='How to Get Around']")
    private WebElement howToGetAround;

    public WCCTrafficAndPublicTransportPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WebElement getTrafficNewsLink() {
        return trafFicNewsLink;
    }

    public WebElement getBusTimetableAndRouters() {
        return busTimetableAndRouters;
    }

    public WebElement getTubeDlrLink() {
        return tubeDlrLink;
    }

    public WebElement getHowToGetAround() {
        return howToGetAround;
    }

    public Result clickTrafficNewsLink() {
        return ClickControl(trafFicNewsLink, "Traffic news link");
    }

    public Result clickBusTimetableAndRoutersLink() {
        return ClickControl(busTimetableAndRouters, "Bus time table and routers link");
    }

    public Result clickTubeDlrLink() {
        return ClickControl(tubeDlrLink, "Tube, DLR and Overground link");
    }

    public Result clickHowToGetAroundLink() {
        return ClickControl(howToGetAround, "How to Get Around link");
    }

}
