package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class WCCSetPasswordPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
   
    @FindBy(xpath = "//input[@id='field-ringpass']")
    private WebElement newPassword;
    @FindBy(xpath = "//input[@id='field-confirmringpass']")
    private WebElement confirmPassword;
   
    @FindBy(xpath = "//*[@name='labyrinth_NewPassword_next']")
    private WebElement saveButton;

    public WCCSetPasswordPage(WebDriver driver) {
        super(driver);
    }

    public String getSecurityOptionsPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public Result enterNewPassword(String password) {
        WaitVisible(newPassword);
        return enterText(newPassword, password, "New password input");
    }

    public Result enterConfirmPassword(String password) {
        WaitVisible(confirmPassword);
        return enterText(confirmPassword, password, "Confirm password input");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button");
    }

}
