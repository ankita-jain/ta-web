package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCPaymentDetailsBookParkingSession extends PageClass {

    @FindBy(xpath = "//*[@name='labyrinth_Basket_next']")
    private WebElement nextButton;

    @FindBy(xpath = "//input[@name='PaymentMethod[type]']")
    private List<WebElement> paymentMethodsRadio;

    public WCCPaymentDetailsBookParkingSession(WebDriver driver) {
        super(driver);
    }

    public void clickPaymentNow() {
        jsClick(paymentMethodsRadio.get(PaymentMethods.PAY_NOW.value()));
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }

    public enum PaymentMethods {
        ADD_TO_BASKET(0),
        PAY_NOW(1);

        private int index;

        PaymentMethods(int index) {
            this.index = index;
        }

        public int value() {
            return index;
        }
    }
}
