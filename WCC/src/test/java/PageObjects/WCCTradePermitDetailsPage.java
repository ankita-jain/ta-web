package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCTradePermitDetailsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement trade_permit_details_page_Title;
    @FindBy(xpath = "//*[@id='tariffdropdown']")
    @CacheLookup
    private WebElement trade_permit_duration;
    @FindBy(xpath = "//*[@name='labyrinth_RVPTariff_next']")
    @CacheLookup
    private WebElement next_step;

    public WCCTradePermitDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getTradePermitDetailsPageTitle() {
        WaitVisible(trade_permit_details_page_Title);
        return trade_permit_details_page_Title.getText();
    }

    public void SelectDuration(String duration) {
        WaitVisible(trade_permit_duration);
        new Select(trade_permit_duration).selectByVisibleText(duration);
    }

    public void clickNextStep() {
        WaitVisible(next_step);
        next_step.click();
    }

    public void fillTradePermitDetails(String duration) {
        SelectDuration(duration);
        clickNextStep();

    }


}
