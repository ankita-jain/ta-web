package PageObjects.POHelpers;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import BusinessObjects.Door;
public class PaymentBasketTable {
	
	public String Qty;
	public String Item;
	public String Cost;
	public String Total;
	public boolean Delete;
	
	public static PaymentBasketTable[] Parse(List<Map<Object,Object>> rawList)
	{
		PaymentBasketTable[] rows = new PaymentBasketTable[rawList.size()];
		
		for(int i = 0; i < rawList.size(); i++)
			rows[i] = ParseRow(rawList.get(i));
		
		return rows;
	}
	
  private static PaymentBasketTable ParseRow(Map<Object,Object> rawMap)
			{
	  	PaymentBasketTable paymentBasketRow = new PaymentBasketTable();
		
	  	paymentBasketRow.Qty= rawMap.get("Qty").toString();
	  	paymentBasketRow.Item = rawMap.get("Item").toString();
	  	paymentBasketRow.Cost = rawMap.get("Cost").toString();
	  	paymentBasketRow.Total = rawMap.get("Total").toString();
		return paymentBasketRow;
			}
	
}