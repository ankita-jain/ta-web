package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCIndexPage extends Base_Page {

    @FindBy(xpath = "//*[@id='toggleAccountMenu']")
    private WebElement login;
    @FindBy(xpath = "//*[@id='accountmenu']//a[@href='/login']")
    private WebElement personal;
    @FindBy(xpath = "//*[@id='accountmenu']//a[contains(@href,'/corplogin')]")
    private WebElement corporate;
    @FindBy(xpath = "/html/body/div[2]/div[2]/ul/li[2]/a")
    private WebElement register;
    @FindBy(xpath = "//*[@id='accountmenu']//a[contains(text(), 'Vehicles')]")
    private WebElement vehicles;
    @FindBy(xpath = "//div[@class='elemErrorWrap']//span[@class='error']")
    private WebElement loginErrorNotification;

    public WCCIndexPage(WebDriver driver) {
        super(driver);
    }

    public Result clickPersonalFromLogin() {
        clickLogin();
        WaitVisible(personal);
        return ClickControl(personal, "Personal link");
    }

    public Result clickCorporateFromLogin() {
        clickLogin();
        WaitVisible(corporate);
        return ClickControl(corporate, "Corporate link");
    }

    public Result clickLogin() {
        WaitVisible(login);
        return ClickControl(login, "Login button");
    }

    public Result clickRegister() {
        return ClickControl(register, "Register button");
    }

    public Result clickVehicles() {
        clickLogin();
        WaitVisible(vehicles);
        return ClickControl(vehicles, "Vehicle button");
    }

    public String getLoginErrorText() {
        WaitVisible(loginErrorNotification);
        return loginErrorNotification.getText();
    }
}
