package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCContactDetailsPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement contactDetailsPageTitle;
    @FindBy(xpath = "//*[@id='field-Member_Title']")
    private WebElement memberTitle;
    @FindBy(xpath = "//*[@id='field-Member_Firstname']")
    private WebElement memberFirstname;
    @FindBy(xpath = "//*[@id='field-Member_Lastname']")
    private WebElement memberLastname;
    @FindBy(xpath = "//*[@id='field-Member_Email']")
    private WebElement memberEmail;
    @FindBy(xpath = "//input[@id='field-Member_Email']//following-sibling::span")
    private WebElement emailAddressTextError;
    @FindBy(xpath = "//*[@id='field-Member_Email_Confirm']")
    private WebElement memberEmailConfirm;
    @FindBy(xpath = "//*[@id='field-TwitterName']")
    private WebElement memberTwitter;
    @FindBy(xpath = "//*[@id='field-Member_CLI']")
    private WebElement memberCLI;
    @FindBy(xpath = "//*[@id='field-Landline']")
    private WebElement memberLandline;
    @FindBy(xpath = "//*[@id='dateofbirth']")
    private WebElement memberDob;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@class='error']")
    private List<WebElement> errorMessages;
    @FindBy(xpath = "//*[@class='warning-notification']")
    private WebElement notificationMessage;
    @FindBy(xpath = "//input[@name='Gender']")
    private List<WebElement> genderType;
    @FindBy(xpath = "//a[@href='/securityquestions']")
    private WebElement changeSecurityQuestionLink;
    public WCCContactDetailsPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getEmailAddressTextError() {
        return emailAddressTextError;
    }

    public String getMemberEmailValue() {
        return memberEmail.getAttribute("value");
    }

    public String getContactDetailsPageTitle() {
        WaitVisible(contactDetailsPageTitle);
        return contactDetailsPageTitle.getText();
    }

    public String getNotificationMessage() {
        WaitVisible(notificationMessage);
        return notificationMessage.getText();
    }

    public List<WebElement> getErrorMessages() {
        return errorMessages;
    }

    public Result selectMemberTitle(String title) {
        WaitVisible(memberTitle);
        return selectItemFromDropdownByTextValue(memberTitle, title, "Title select");
    }

    public Result enterMemberFirstName(String firstName) {
        WaitVisible(memberFirstname);
        return enterText(memberFirstname, firstName, "First name input");
    }

    public Result enterMemberSurname(String surname) {
        WaitVisible(memberLastname);
        return enterText(memberLastname, surname, "Surname input");
    }

    public Result enterMemberEmailAddress(String email) {
        WaitVisible(memberEmail);
        return enterText(memberEmail, email, "Email input");
    }

    public Result enterConfirmMemberEmailAddress(String email) {
        WaitVisible(memberEmailConfirm);
        return enterText(memberEmailConfirm, email, "Email confirm input");
    }

    public Result enterTwitterName(String twitter) {
        WaitVisible(memberTwitter);
        memberTwitter.clear();
        return enterText(memberTwitter, twitter, "Twitter input");
    }

    public Result enterMemberCLI(String cli) {
        WaitVisible(memberCLI);
        return enterText(memberCLI, cli, "CLI input");
    }

    public Result enterMemberPhonenumber(String phone) {
        WaitVisible(memberLandline);
        return enterText(memberLandline, phone, "Landline number input");
    }

    public Result enterMemberDOB(String dob) {
        WaitVisible(memberDob);
        return enterText(memberDob, dob, "Date of Birth input");
    }


    public Result chooseGender(String value) {
        WebElement radioButton = genderType
                .stream()
                .filter(x -> x.getAttribute("value").equals(value))
                .findFirst()
                .get();
        return ClickControl(radioButton, "Gender Type radio button");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button)");
    }

    public Result changeSecurityQuestion() {
        WaitVisible(changeSecurityQuestionLink);
        return ClickControl(changeSecurityQuestionLink, "Change security question link");
    }

    public void editMyPersonalDetails(String title, String first_name, String surname, String email, String confirm_email, String cli) {
        selectMemberTitle(title);
        enterMemberFirstName(first_name);
        enterMemberSurname(surname);
        enterMemberEmailAddress(email);
        enterConfirmMemberEmailAddress(confirm_email);
        enterMemberCLI(cli);

        clickSaveButton();
    }

    public void removeCacheValuesFromMandatoryFields() {
        memberFirstname.clear();
        memberLastname.clear();
        memberEmail.clear();
        memberEmailConfirm.clear();
        memberCLI.clear();
    }

    public boolean CheckErrorMessages(String errorString) {
        String[] arrayErrorMessages = errorString.split("\\-", -1);

        for (int i = 0; i < errorMessages.size(); i++) {
            if (!errorMessages.get(i).getText().equals(arrayErrorMessages[i])) {
                return false;
            }
        }

        return true;
    }


}
