package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class WCCSecurityOptions extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement pagetitle;
    @FindBy(xpath = "//*[@id='field-MemberPassword']")
    private WebElement oldPassword;
    @FindBy(xpath = "//*[@id='field-MemberPassword_new']")
    private WebElement newPassword;
    @FindBy(xpath = "//*[@id='field-MemberPassword_confirm']")
    private WebElement confirmPassword;
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement cancelButton;
    @FindBy(xpath = "//*[@name='submits[submitPass]']")
    private WebElement saveButton;

    public WCCSecurityOptions(WebDriver driver) {
        super(driver);
    }

    public String getSecurityOptionsPageTitle() {
        WaitVisible(pagetitle);
        return pagetitle.getText();
    }

    public Result enterOldPassword(String password) {
        WaitVisible(oldPassword);
        return enterText(oldPassword, password, "Old password input");
    }

    public Result enterNewPassword(String password) {
        WaitVisible(oldPassword);
        return enterText(newPassword, password, "New password input");
    }

    public Result enterConfirmPassword(String password) {
        WaitVisible(oldPassword);
        return enterText(confirmPassword, password, "Confirm password input");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Save button");
    }

    public Result clickCancel() {
        WaitVisible(cancelButton);
        return ClickControl(cancelButton, "Cancel button");
    }
}
