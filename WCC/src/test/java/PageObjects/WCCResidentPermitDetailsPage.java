package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import SeleniumHelpers.WaitMethods;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import Structures.Result;

public class WCCResidentPermitDetailsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement residentpermitdetailspagetitle;
    @FindBy(xpath = "//*[@id='field-SubCategoryType']")
    @CacheLookup
    private WebElement residenttype;
    @FindBy(xpath = "//*[@id='field-vehicleoptions1']")
    @CacheLookup
    private WebElement registration;
    @FindBy(id = "field-selectvehicle1")
    private WebElement enterVehicle;
    @FindBy(xpath = "//*[@id='field-OwnershipId1']")
    @CacheLookup
    private WebElement ownership;
    @FindBy(xpath = "//input[@name='Terms']")
    @CacheLookup
    private WebElement terms_and_conditions;
    @FindBy(xpath = "//*[@name='labyrinth_Options_next']")
    private WebElement nextButton;
    @FindBy(xpath = "//*[@name='labyrinth_Options_back']")
    @CacheLookup
    private WebElement backButton;
    @FindBy(id= "field-Member_Title")
    @CacheLookup
    private WebElement title;
    @FindBy(id="field-Member_Firstname")
    @CacheLookup
    private WebElement firstName;
    @FindBy(id="field-Member_Lastname")
    @CacheLookup
    private WebElement lastName;  
    @FindBy(xpath = "//button[contains(@class,'additional-vehicle-button')]")
    @CacheLookup
    private WebElement clicksecondvehiclebutton;
    @FindBy(id="field-selectvehicle2")
    @CacheLookup
    private WebElement addsecondvehicle;
    @FindBy(id="field-OwnershipId2")
    @CacheLookup
    private WebElement choosesecondownershiptype;   
    
    @FindBy(id = "addvehicle1")
    private WebElement addRegistration;
    
    @FindBy(id = "addvehicle2")
    private WebElement addSecondRegistration;
    
    @FindBy(id = "field-selectvehicle2")
    private WebElement enterSecondVehicle;
    

    public WCCResidentPermitDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getResidentPermitDetailsPageTitle() {
        WaitVisible(residentpermitdetailspagetitle);
        return residentpermitdetailspagetitle.getText();
    }
    
    public void selectUserTitleResidentPermit(String userTitle)
    {
    	WaitVisible(title);
    	Select titleList = new Select(title);
    	titleList.selectByVisibleText(userTitle);
    }
    
    public void enterUserFirstName(String userFirstName)
    {
    	WaitVisible(firstName);
    	enteringData(firstName, userFirstName);
    }
    
    public void enterUserLastName(String userLastName)
    {
    	WaitVisible(lastName);
    	enteringData(lastName, userLastName);
    }

    public void selectResidentType(String resident) {
        WaitVisible(residenttype);
        Select residentlist = new Select(residenttype);
        residentlist.selectByVisibleText(resident);
    }

    public void selectRegistration(String vrn_registration) {
        WaitVisible(registration);
        Select registrationlist = new Select(registration);
        registrationlist.selectByVisibleText(vrn_registration);
    }
    
    /* Adding first vehicle for a user*/
    public void enterVrm(String vrm) {
        WaitVisible(enterVehicle);
        enteringData(enterVehicle, vrm);
    }
 	
 	/*Adding second vehicle for a user*/
    public void enterSecondVrm(String vrm) {
        WaitVisible(addsecondvehicle);
        enteringData(addsecondvehicle, vrm);
    }
    
    /*Choosing first vehicle ownership type for a new registered user*/
    public void selectOwnershipType(String Ownership) {
        WaitVisible(ownership);
        Select ownershiplist = new Select(ownership);
        ownershiplist.selectByVisibleText(Ownership);
    }
    
    /*Choosing second vehicle ownership type for a new registered user*/
    public void selectSecondOwnershipType(String Ownership) {
        WaitVisible(choosesecondownershiptype);
        Select ownershiplist = new Select(choosesecondownershiptype);
        ownershiplist.selectByVisibleText(Ownership);
    }

    public void selectTermAndConditions() {
        jsClick(terms_and_conditions);
    }

    public Result clickNextButton() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button clicked");
    }

    public void fillPermitApplicationDetails(String residentTypes, String vrm, String ownership) {

        selectResidentType(residentTypes);
        boolean exists;
       
        try {
        	
        	exists = registration.isDisplayed();
        }
        catch (Exception e){
        	exists = false;
        }
       
       if (exists) {
    	 selectRegistration(vrm);
       }
       else {
    	   enterVrm(vrm);
       }
        selectOwnershipType(ownership);
        selectTermAndConditions();
        clickNextButton();

    }

    public void enterUserDetailsPermitDetails(String userTitle, String firstName, String lastName)
    {
    	selectUserTitleResidentPermit(userTitle);
    	enterUserFirstName(firstName);
    	enterUserLastName(lastName); 	
    }
    
    public void fillPermitApplicationDetailsMotorcylePermit(String vrm, String ownership) {

        selectRegistration(vrm);
        selectOwnershipType(ownership);
        selectTermAndConditions();
        clickNextButton();
    }
    
    public void permitRenewalProcess() {
        selectTermAndConditions();
        clickNextButton();

    }
  	
    public void fillPermitApplicationDetailsForMultipleVehicles(String residentTypes, String vrm, String vrm2, String ownership) {
        selectResidentType(residentTypes);
        boolean exists;
       
        try {
        	
        	exists = registration.isDisplayed();
        }
        catch (Exception e){
        	exists = false;
        }
       
       if (exists) {
    	 selectRegistration(vrm);
       }
       else {
    	   enterVrm(vrm);
       }
        enterSecondVrm(vrm2);
        selectSecondOwnershipType(ownership);
        selectTermAndConditions();
        clickNextButton();

    }
    
    public boolean firstNameFieldDisplayed() {
    	WaitVisible(firstName);
    	return firstName.isDisplayed();
    }
    
    public void addRegistration(String VRM) {
    	WaitVisible(addRegistration);
    	addRegistration.click();
    	enteringData(enterVehicle, VRM);
    }
    
    public String getVehcileRegistration()
    {
    	WaitVisible(enterVehicle);
    	return enterVehicle.getText();
    }
    
    public void clickSecondVehcileButton()
    {
    	WaitVisible(clicksecondvehiclebutton);
    	clicksecondvehiclebutton.click();
    }
    
    public void addYourSecondAddRegistrationVehicle()
    {
    	WaitVisible(addSecondRegistration);
    	jsClick(addSecondRegistration);
    }
    
    public void addAnAdditionalRegistration(String vrn, String ownershipType)
    {
    	addYourSecondAddRegistrationVehicle();
    	WaitVisible(enterSecondVehicle);
    	enteringData(enterSecondVehicle, vrn);
    	selectSecondOwnershipType(ownershipType);
    }

	public void clearNameFields() {
		WaitVisible(firstName);
		firstName.clear();
		lastName.clear();
	}
}