package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;

public class ResetPasswordNewPassword extends Base_Page {
    @FindBy(xpath = "//input[@name='MemberPassword']")
    private WebElement newPasswordInput;
    
    @FindBy(xpath = "//input[@name='ArchiveVerifyCode']")
    private WebElement ArchiveVerifyCodeInput;

    @FindBy(xpath = "//input[@name='MemberPassword_confirm']")
    private WebElement confirmPasswordInput;

    @FindBy(xpath = "//select[@name='SecretQuestion']")
    private WebElement questionSelect;

    @FindBy(xpath = "//input[@name='SecretAnswer']")
    private WebElement secretAnswerInput;

    @FindBy(xpath = "//input[@name='labyrinth_resetPassword_next']")
    private WebElement nextButton;

    @FindBy(xpath = "//ul[@class='success-notification']")
    private WebElement successNotification;

    @FindBy(xpath = "//li[@class='MemberPassword-wrapper']//span[@class='error']")
    private WebElement passwordErrorNotification;

    @FindBy(xpath = "//ul[@class='error-notification']")
    private WebElement errorNotification;

    @FindBy(xpath = "//li[@class='MemberPassword_confirm-wrapper']//span[@class='error']")
    private WebElement confirmPasswordErrorNotification;

    public ResetPasswordNewPassword(WebDriver driver) {
        super(driver);
    }

    public Result pasteNewPassword(String newPassword) {
        WaitVisible(newPasswordInput);
        return enterText(newPasswordInput, newPassword, "New Password input");
    }
    
    public Result pasteArchiveVerifyCode(String archiveCode) {
        WaitVisible(ArchiveVerifyCodeInput);
        return enterText(ArchiveVerifyCodeInput, archiveCode, "Archive code input");
    }

    public Result pasteConfirmPassword(String confirmPassword) {
        WaitVisible(confirmPasswordInput);
        return enterText(confirmPasswordInput, confirmPassword, "Confirm Password input");
    }

    public Result chooseQuestionByIndex(int index) {
        WaitVisible(questionSelect);
        return selectItemFromDropdownByIndex(questionSelect, index, "Question select");
    }

    public Result pasteSecretAnswer(String answer) {
        WaitVisible(secretAnswerInput);
        return enterText(secretAnswerInput, answer, "Secret Answer input");
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }

    public String getSuccessNotificationText() {
        WaitVisible(successNotification);
        return successNotification.getText();
    }

    public String getPasswordErrorText() {
        WaitVisible(passwordErrorNotification);
        return passwordErrorNotification.getText();
    }

    public String getErrorNotificationText() {
        WaitVisible(errorNotification);
        return errorNotification.getText();
    }

    public String getConfirmPasswordErrorText() {
        WaitVisible(confirmPasswordErrorNotification);
        return confirmPasswordErrorNotification.getText();
    }
}
