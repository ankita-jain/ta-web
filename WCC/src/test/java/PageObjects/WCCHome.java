package PageObjects;

import static SeleniumHelpers.WebElementMethods.ClickControl;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import Structures.Result;

public class WCCHome extends PageClass {
    private PermitLinkContainersInLine permitLinkContainersInLine = new PermitLinkContainersInLine();
    @FindBy(xpath = "//*[@id='toggleAccountMenu']")
    private WebElement account;
    @FindBy(xpath = "//a[@href='/logout']")
    private WebElement logout;
    @FindBy(xpath = "//a[@href='/permits']")
    private WebElement permits;
    @FindBy(xpath = "//div[@class='permitwidget']//a[@href='/bookparking']")
    private WebElement pay_to_park;
    @FindBy(xpath = "//a[@href='/payment']")
    private WebElement payment;
    @FindBy(xpath = "//a[@href='/details']")
    private WebElement my_details;
    @FindBy(xpath = "//ul[@class='openMenu']//a[@href='/dashboard']")
    private WebElement dashboard;
    @FindBy(xpath = "//a[@href='/useraddress']")
    private WebElement address;
    @FindBy(xpath = "//a[@href='/vehicles']")
    private WebElement vehiclesLink;
    @FindBy(xpath = "//a[@href='/change']")
    private WebElement security_options;
    @FindBy(xpath = "//a[@href='/help']//h3")
    private WebElement help;
    @FindBy(xpath = "//a[@href='/report-it']//h3")
    private WebElement report_parking_issue;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement homepageTitle;
    @FindBy(xpath = "//*[@class='page-head']/a")
    private WebElement backButtonToPreviousPage;
    @FindBy(xpath = "//a[@href='/permitapplications']")
    private WebElement see_all_permits_link;

    public WCCHome(WebDriver driver) {
        super(driver);
    }

    public String getHomePageTitle() {
        WaitVisible(homepageTitle);
        return homepageTitle.getText();
    }

    public Result clickDashBoard() {
        clickAccount();
        return ClickControl(dashboard, "Dashboard link");
    }

    public Result clickPayment() {
        clickAccount();
        WaitVisible(payment);
        return ClickControl(payment, "Payment link");
    }

    public Result clickMyDetails() {
        clickAccount();
        WaitVisible(my_details);
        return ClickControl(my_details, "My details link");
    }

    public Result clickAddress() {
        clickAccount();
        WaitVisible(address);
        return ClickControl(address, "Address link");
    }

    public Result clickSecurityOptions() {
        clickAccount();
        WaitVisible(security_options);
        return ClickControl(security_options, "Security option link");
    }

    public Result clickVehiclesLink() {
        clickAccount();
        WaitVisible(vehiclesLink);
        return ClickControl(vehiclesLink, "Vehicles link");
    }

    public Result clickPermit() {
        WaitVisible(permits);
        return ClickControl(permits, "Permits link");
    }

    public Result clickHelp() {
        WaitVisible(help);
        return ClickControl(help, "Help link");
    }

    public Result clickAccount() {
    	WaitVisible(account);
        return ClickControl(account, "Account link");
    }
    
    public void clickAccountMenu() {
    	WaitVisible(account);
    	jsClick(account);
    }

    public Result clickPayToPark() {
        WaitVisible(pay_to_park);
        return ClickControl(pay_to_park, "'Pay to park' link");
    }

    public Result clickReportParkingIssue() {
        WaitVisible(report_parking_issue);
        return ClickControl(report_parking_issue, "'Report a parking issue' link");
    }

    public Result logout() {
        clickAccount();
        WaitVisible(logout);
        return ClickControl(logout, "'Logout' link");
    }
    
    public void logoutMenu() {
    	 clickAccountMenu();
         WaitVisible(logout);
         jsClick(logout);
    }

    public Result clickGoBackToPreviousPage() {
        WaitVisible(backButtonToPreviousPage);
        return ClickControl(backButtonToPreviousPage, "'Back to previous page' link");
    }

    public PermitLinkContainersInLine getPermitLinkContainersInLine() {
        return permitLinkContainersInLine;
    }

    public String dashboardSubMenuTitle() {
        WaitVisible(dashboard);
        return dashboard.getText();
    }

    public String getAccountMenuText() {
        WaitVisible(account);
        return account.getText();
    }

    public class PermitLinkContainersInLine {
        @FindBy(xpath = "//div[@class='permitlinks']/a[contains(@href, 'trafficandtransport')]")
        private WebElement traficAndPublicTransportLink;

        private PermitLinkContainersInLine() {
            PageFactory.initElements(_driver, this);
        }

        public WebElement getTrafficAndPublicTransportLink() {
            return traficAndPublicTransportLink;
        }

        public Result clickOnTrafficAndPublicTransportLink() {
            return ClickControl(traficAndPublicTransportLink, "Traffic and public transport link");
        }
    }

	public void selectSeeAllPermitsLink() {
		WaitVisible(see_all_permits_link);
    	see_all_permits_link.click();
	}
}
