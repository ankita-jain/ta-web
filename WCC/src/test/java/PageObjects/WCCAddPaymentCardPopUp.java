package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;


public class WCCAddPaymentCardPopUp extends Base_Page {
 
	@FindBy(xpath = "//*[@id='header_logo']/a[1]")
    private WebElement home;
    @FindBy(xpath = "//*[@class='modal-title']")
    private WebElement popup_title;
    @FindBy(xpath = "//*[@id='modal-wrapper']/div/div[2]/form/div[5]/div")
    private WebElement errorInExpireDateSection;
    @FindBy(xpath = "//*[@name='cardNumberValue']")
    private WebElement cardNumber;
    @FindBy(xpath = "//*[@class='modal-body']/form/div[2]/div")
    private WebElement cardNumberErrMessage;
    @FindBy(xpath = "//*[@name='expiresValue']")
    private WebElement cardExpires;
    @FindBy(xpath = "//*[@class='modal-body']/form/div[4]/div")
    private WebElement cardExpiresErrMessage;
    @FindBy(xpath = "//*[@id='addCardBtn']")
    private WebElement addButton;   
    @FindBy(xpath = "//*[@id='close-img']")
    private WebElement closeAddPaymentCard; 

    public WCCAddPaymentCardPopUp(WebDriver driver) {
        super(driver);
    }

    public String getAddPaymenPageTitle() {
        WaitVisible(popup_title);
        return popup_title.getText();
    }
    
    public Result clickRingGoLogo() {
        WaitVisible(home);
        return ClickControl(home, "Click Add Button");
    }

    public Result enterCardNumber(String cc_number) {
        WaitVisible(cardNumber);
        return enterText(cardNumber, cc_number, "Card number input");
    }
    
    public String getCardNumberErrorMessage()
    {
    	  WaitVisible(cardNumberErrMessage);
          return cardNumberErrMessage.getText();
    }

    public Result enterCardExpires(String expires) {
        WaitVisible(cardExpires);
        return enterText(cardExpires, expires, "Card Expiry is entered");
    }
    
    public String getCardExpiryErrorMessage()
    {
    	  WaitVisible(cardExpiresErrMessage);
          return cardExpiresErrMessage.getText();
    }

    public Result clickAddButton() {
        WaitVisible(addButton);
        return ClickControl(addButton, "Click Add Button");
    }
    
    public Result clickAddPaymentPopupWindowClose() {
        WaitVisible(closeAddPaymentCard);
        return ClickControl(closeAddPaymentCard, "clickAddPaymentPopupWindowClose");
    }

    public String getErrorInExpireDateSectionAsAText() {
        WaitVisible(errorInExpireDateSection);
        return errorInExpireDateSection.getText();
    }

   
}
