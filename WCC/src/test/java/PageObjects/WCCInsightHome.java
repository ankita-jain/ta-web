package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCInsightHome extends PageClass {
    //This file need to be deleted along with other sample tests
    public WebDriver driver;
    @FindBy(xpath = "//*[@id='nav']/li[1]/a")
    @CacheLookup
    private WebElement servicemenu;
    @FindBy(xpath = "//*[@id='nav']/li[1]/ul/li[7]/a")
    @CacheLookup
    private WebElement permitapplicationmenu;
    @FindBy(xpath = "//*[@id='middlefilterbox']/p[4]/select")
    @CacheLookup
    private WebElement permitstatus;
    @FindBy(xpath = "//*[@id='bottomfilterbox']/p[10]/input")
    @CacheLookup
    private WebElement permitsearchbutton;
    @FindBy(xpath = "//*[@id='bottomfilterbox']/p[9]/input")
    @CacheLookup
    private WebElement permitid;
    @FindBy(xpath = "//*[@id='main']/table/tbody/tr[4]/td[2]/table/tbody/tr/td[1]/table/tbody/tr[17]/td[2]/p/a")
    @CacheLookup
    private WebElement permitapplicationsubmenulink;
    @FindBy(xpath = "//*[@id='1']/td[20]/p/a/img")
    @CacheLookup
    private WebElement editpermit;


    public WCCInsightHome(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void permitStatusChangeTo(String permit) {
        Select permitstatusdropdown = new Select(permitstatus);
        permitstatusdropdown.selectByVisibleText(permit);

    }

    public void clickPermitApplicationFromServiceMenu() {
        moveToMenuAndClickSubMenu(servicemenu, permitapplicationmenu);
    }

    public void searchPermit() {
        permitsearchbutton.click();
    }

    public void inputPermitId(String permitnumber) {
        permitid.clear();
        permitid.sendKeys(permitnumber);
    }

    public void clickServiceMenu() {
        servicemenu.click();
    }

    public void clickPermitApplicationSubmenu() {
        WaitVisible(permitapplicationsubmenulink);
        permitapplicationsubmenulink.click();
    }

    public void clickEditPermit() {
        WaitVisible(editpermit);
        editpermit.click();
    }

}
