package PageObjects;

import PageObjects.pagecomplexobjects.WCCNavbarUnderUpperMenu;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;
import static java.lang.String.format;

public class WCCHelpCentreChangeVehiclePage extends Base_Page {
    private ChoosingVehicleStage choosingVehicleStage = new ChoosingVehicleStage();
    private WCCNavbarUnderUpperMenu WCCNavbarUnderUpperMenu = new WCCNavbarUnderUpperMenu(_driver);

    @FindBy(xpath = "//select[@id='field-permit']")
    private WebElement selectSessionDropdown;
    @FindBy(xpath = "//input[@value='Next']")
    private WebElement nextButton;
    @FindBy(xpath = "//div[@class='box-content']//p")
    private WebElement notification;
    @FindBy(xpath = "//*[@class='error-notification']")
    private WebElement errorMessage;

    public WCCHelpCentreChangeVehiclePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(_driver, this);
    }

    public WCCNavbarUnderUpperMenu getWCCNavbarUnderUpperMenu() {
        return WCCNavbarUnderUpperMenu;
    }

    public ChoosingVehicleStage getChoosingVehicleStage() {
        return choosingVehicleStage;
    }

    public Result selectSession(String containsZoneIDInDropDown) {
        return selectItemFromDropdownByContainsText(selectSessionDropdown,
                containsZoneIDInDropDown, format("Select session with %s zone number", containsZoneIDInDropDown));
    }

    public Result clickNext() {
        return ClickControl(nextButton, "Next button");
    }

    public String getNotificationText() {
        WaitVisible(notification);
        return notification.getText();
    }

    public String getErrorMessageText() {
        WaitVisible(errorMessage);
        return errorMessage.getText();
    }

    public class ChoosingVehicleStage {
        @FindBy(xpath = "//ul[@class='error-notification']")
        private WebElement errorNotification;

        @FindBy(xpath = "//select[@id='field-ringgovehicles']")
        private WebElement vrnDropdown;

        @FindBy(xpath = "//input[@id='field-vrn']")
        private WebElement vrnInput;

        @FindBy(xpath = "//select[@id='field-type']")
        private WebElement vrnType;

        @FindBy(xpath = "//select[@id='field-make']")
        private WebElement vrnManufacture;

        @FindBy(xpath = "//select[@id='field-colour']")
        private WebElement vrnColour;

        @FindBy(xpath = "//input[@value='Next']")
        private WebElement nextLink;

        @FindBy(xpath = "//input[@value='Finish']")
        private WebElement finishLink;

        @FindBy(xpath = "//select[@id='field-ringgovehicles']")
        private WebElement addNewVehicleOption;

        private ChoosingVehicleStage() {
            PageFactory.initElements(_driver, this);
        }

        public Result enterVehicleNumber(String vrnValue) {
            return enterText(vrnInput, vrnValue, format("VRN value VRN %s", vrnValue));
        }

        public Result selectVrnType(String vrnTypeValue) {
            return selectItemFromDropdownByTextValue(vrnType, vrnTypeValue, format("Enter %s vrn type", vrnTypeValue));
        }

        public Result selectVrnManufacture(String vrnManufactureValue) {
            return selectItemFromDropdownByTextValue(vrnManufacture, vrnManufactureValue, format("Select %s vrn manufacture", vrnManufactureValue));
        }

        public Result selectVrnColour(String vrnColourValue) {
            return selectItemFromDropdownByTextValue(vrnColour, vrnColourValue, format("Select %s vrn colour", vrnColourValue));
        }

        public Result clickNext() {
            return ClickControl(nextButton, "Next button");
        }

        public Result clickFinish() {
            return ClickControl(finishLink, "Finish button");
        }

        public WebElement getErrorNotification() {
            return errorNotification;
        }

        public Result selectVrnFromDropdown(String vrnValue) {
            return selectItemFromDropdownByContainsText(vrnDropdown, vrnValue, format("VRN %s value selected", vrnValue));
        }
    }
}
