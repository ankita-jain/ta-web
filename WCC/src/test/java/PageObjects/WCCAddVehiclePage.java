package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.*;

public class WCCAddVehiclePage extends Base_Page {

	public WebDriver driver;
	@FindBy(xpath = "//*[@class='page-head']//h1")
	@CacheLookup
	private WebElement addAVehiclePageTitle;
	@FindBy(xpath = "//*[@id='field-VRN']")
	@CacheLookup
	private WebElement numberPlateField;
	@FindBy(xpath = "//*[@id='field-Colour']")
	@CacheLookup
	private WebElement colourDropDown;
	@FindBy(xpath = "//*[@id='field-Make']")
	@CacheLookup
	private WebElement makeDropDown;
	@FindBy(xpath = "//*[@id='field-TypeShow']")
	@CacheLookup
	private WebElement typeDropDown;
	@FindBy(xpath = "//*[@id='field-OwnershipId']")
	@CacheLookup
	private WebElement ownershipTypeDropDown;
	@FindBy(xpath = "//input[@value = 'Save']")
	private WebElement saveButton;
	@FindBy(xpath = "//*[@class='VRN-wrapper']//span[@class='error']")
	private WebElement numberPlateErrorMessage;
	@FindBy(xpath = "//*[@class='Colour-wrapper']//span[@class='error']")
	private WebElement colourErrorMessage;
	@FindBy(xpath = "//*[@class='Make-wrapper']//span[@class='error']")
	private WebElement makeErrorMessage;
	@FindBy(xpath = "//*[@class='TypeShow-wrapper']//span[@class='error']")
	private WebElement typeErrorMessage;
	@FindBy(xpath = "//a[@href='/home']")
	private WebElement homeBreadcrumb;

	public WCCAddVehiclePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}

	public Result enterNumberPlate(String numberPlate) {
		WaitVisible(numberPlateField);
		return enterText(numberPlateField, numberPlate, "Number plate");
	}

	public Result selectColour(String colour) {
		WaitVisible(colourDropDown);
		return selectItemFromDropdownByTextValue(colourDropDown, colour, "Colour select");
	}

	public Result selectMake(String make) {
		WaitVisible(makeDropDown);
		return selectItemFromDropdownByTextValue(makeDropDown, make, "Make select");
	}

	public Result selectType(String type) {
		WaitVisible(typeDropDown);
		return selectItemFromDropdownByTextValue(typeDropDown, type, "Type select");
	}

	public Result clickSave() {
		WaitVisible(saveButton);
		return ClickControl(saveButton, "Save button");
	}

	public String getNumberPlateErrorMessageText() {
		WaitVisible(numberPlateErrorMessage);
		return numberPlateErrorMessage.getText();
	}

	public String getColourErrorMessageText() {
		WaitVisible(colourErrorMessage);
		return colourErrorMessage.getText();
	}

	public String getMakeErrorMessageText() {
		WaitVisible(makeErrorMessage);
		return makeErrorMessage.getText();
	}

	public String getTypeErrorMessageText() {
		WaitVisible(typeErrorMessage);
		return typeErrorMessage.getText();
	}

	public void navigateHome() {
		WaitVisible(homeBreadcrumb);
		homeBreadcrumb.click();
	}
}