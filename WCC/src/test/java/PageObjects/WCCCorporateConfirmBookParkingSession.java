package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporateConfirmBookParkingSession extends Base_Page {
    @FindBy(xpath = "//a[contains(@class, 'ringgo-tooltip-click')]")
    private WebElement fuelTypeSurchargeLink;

    @FindBy(xpath = "//a[contains(@class, 'ringgo-tooltip-click')]/span")
    private WebElement dieselSurchargeToolTip;

    @FindBy(xpath = "//input[@name='labyrinth_CorpConfirm_next']")
    private WebElement nextButton;

    @FindBy(xpath = "//fieldSet//li[6]")
    private WebElement priceSection;


    public WCCCorporateConfirmBookParkingSession(WebDriver driver) {
        super(driver);
    }

    public WebElement getPriceSection() {
        return priceSection;
    }

    public Result clickFuelTypeSurchargeLink() {
        WaitVisible(fuelTypeSurchargeLink);
        return ClickControl(fuelTypeSurchargeLink, "Fuel type price adjustment link");
    }

    public String getFuelTypeSurchargeTooltipText() {
        WaitVisible(dieselSurchargeToolTip);
        return dieselSurchargeToolTip.getText().replaceAll("\n", " ");
    }

    public String getPrice() {
        WaitVisible(priceSection);
        return priceSection.getText().replaceAll("[\\s+a-zA-Z:]+", "");
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }
}
