package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCDisableResidentPermitDetailsPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@id='ElectoralRegisterNumber_3633']")
    @CacheLookup
    private WebElement electroalregistercode;
    @FindBy(xpath = "//*[@id='DisabilityDescription_3171']")
    @CacheLookup
    private WebElement description_of_disability;
    @FindBy(xpath = "//*[@id='VehicleAdaptionDetails_3172']")
    @CacheLookup
    private WebElement vehicleadaptiondetails;
    @FindBy(xpath = "//*[@id='MedicalEquipmentNeeded_3164']")
    @CacheLookup
    private WebElement medical_equipements_status;
    @FindBy(xpath = "//*[@id='MedicalEquipmentType_3165'] ")
    @CacheLookup
    private WebElement medical_equipement_details;
    @FindBy(xpath = "//*[@id='MedicalCondition_3174']")
    @CacheLookup
    private WebElement medical_conditions;
    @FindBy(xpath = "//*[@id='NearVehicle_3173']")
    @CacheLookup
    private WebElement child_near_vehicle;
    @FindBy(xpath = "//*[@id='RegisteredBlind_3168']")
    @CacheLookup
    private WebElement registeredBlind;
    @FindBy(xpath = "//*[@id='AdultServicesRef_3622']")
    @CacheLookup
    private WebElement blindregisteredcouncil;
    @FindBy(xpath = "//*[@id='AdultServicesRef_3170']")
    @CacheLookup
    private WebElement adultreferenceno;
    @FindBy(xpath = "//*[@id='labyrinth_RvpAdditionalData_next']")
    @CacheLookup
    private WebElement next_button;
    @FindBy(xpath = "//*[@id='labyrinth_RvpAdditionalData_back']")
    @CacheLookup
    private WebElement previous_btton;
    @FindBy(xpath = "//*[@id='labyrinth_cancel']")
    @CacheLookup
    private WebElement cancel_button;

    public WCCDisableResidentPermitDetailsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void enterElectoralCode(String electoralcode) {
        WaitVisible(electroalregistercode);
        electroalregistercode.sendKeys(electoralcode);
    }

    public void enterDisabilityDescription(String disabilityDesc) {
        WaitVisible(description_of_disability);
        description_of_disability.sendKeys(disabilityDesc);
    }

    public void enterVehicleAdaptionDetails(String Vehicleadaptiondetails) {
        WaitVisible(vehicleadaptiondetails);
        vehicleadaptiondetails.sendKeys(Vehicleadaptiondetails);
    }

    public void selectMedicalStatus(String status) {
        Select medical_status = new Select(medical_equipements_status);
        medical_status.selectByVisibleText(status);
    }

    public void enterMedicalEquipmentType(String equipementtype) {
        WaitVisible(medical_equipement_details);
        medical_equipement_details.sendKeys(equipementtype);
    }

    public void enterMedicalConditions(String medicalcondition) {
        WaitVisible(medical_conditions);
        medical_conditions.sendKeys(medicalcondition);
    }

    public void selectChildNearVehicle(String does_child_need_vehicle_near) {
        Select child_near_vehicle_options = new Select(child_near_vehicle);
        child_near_vehicle_options.selectByVisibleText(does_child_need_vehicle_near);
    }

    public void selectDoesUserRegisteredBlind(String status) {
        new Select(registeredBlind).selectByVisibleText(status);
    }

    public void enterBlindRegisteredCouncil(String councilname) {
        WaitVisible(blindregisteredcouncil);
        blindregisteredcouncil.sendKeys(councilname);
    }

    public void enterAdultReferenceNo(String refNo) {
        WaitVisible(adultreferenceno);
        adultreferenceno.sendKeys(refNo);
    }

    public void clickNextButton() {
        next_button.click();
    }

    //BLUE BADGE & CHILD UNDER 2 YRS AGE
    public void fillDisabilityDetailsOfChildUnder2Yrs(String medicalStatus, String medicalEquipementType, String does_child_need_vehicle_near, String medicalcondition) {
        selectMedicalStatus(medicalStatus);
        enterMedicalEquipmentType(medicalEquipementType);
        selectChildNearVehicle(does_child_need_vehicle_near);
        enterMedicalConditions(medicalcondition);
        clickNextButton();

    }

    //BLUE BADGE & ENHANCED PERSONAL INDEPENDENCE
    public void fillDisabilityDetailsOfEnhancedPersonalIndependencePaymentAllowance() {
        clickNextButton();
    }

    //BLUE BADGE & HIGHER RATE MOBILITY
    public void fillDisabilityDetailsOfHigherRateMobilityAllowance() {
        clickNextButton();
    }

    //BLUE BADGE & SEVERE VISUALLY IMPAIRED
    public void fillDisabilityDetailsOfSeverelyVisuallyImpaired(String blindStatus, String council, String refNo) {
        selectDoesUserRegisteredBlind(blindStatus);
        enterBlindRegisteredCouncil(council);
        enterAdultReferenceNo(refNo);
        clickNextButton();
    }

    //BLUE BADGE & UPPER LIMB
    public void fillDisabilityDetailsOfUpperLimbDisability(String disabilityDesc, String Vehicleadaptiondetails) {
        enterDisabilityDescription(disabilityDesc);
        enterVehicleAdaptionDetails(Vehicleadaptiondetails);
        clickNextButton();
    }

    //BLUE BADGE & WAR PENSIONERS
    public void fillDisabilityDetailsOfWarPensionerMobilitySupplement() {
        clickNextButton();
    }

    //BLUE BADGE & CAN'T WALK
    public void cannotWalkOrWalkOnlyWithSeverDisabiilty(String disabilityDesc) {
        enterDisabilityDescription(disabilityDesc);
        clickNextButton();
    }
}
