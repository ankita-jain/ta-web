package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

public class ForgetPasswordVerificationCode extends Base_Page {
    @FindBy(xpath = "//input[@name='SmsCode']")
    private WebElement verificationCodeInput;
    
    @FindBy(xpath = "//input[@name='SmsCode']//following::span[1]")
    private WebElement verificationCodeErrorMessage;    
    
    @FindBy(xpath = "//input[@name='ArchiveUser']")
    private WebElement confirm_close_account_T_and_C;    
    
    @FindBy(xpath = "//input[@name='ArchiveVerifyCode']")
    private WebElement archiveVerifyCodeInput;
    
    @FindBy(xpath = "//input[@id='labyrinth_archiveUser_next']")
    private WebElement closeAccountButton;    
                           
    @FindBy(xpath = "//input[@name='labyrinth_verifyCode_next']")
    private WebElement nextButton;
    
    @FindBy(xpath = "//input[@name='labyrinth_verifyArchiveCode_next']")
    private WebElement verifyArchiveCode_next;

    @FindBy(xpath = "//input[@name='labyrinth_verifyCode_skip']")
    private WebElement resendCodeLink;

    @FindBy(xpath = "//ul[@class='error-notification']")
    private WebElement errorNotification;

    @FindBy(xpath = "//div[@class='box-content']")
    private WebElement textBox;

    public ForgetPasswordVerificationCode(WebDriver driver) {
        super(driver);
    }

    public Result pasteVerificationCode(String code) {
        WaitVisible(verificationCodeInput);
        return enterText(verificationCodeInput, code, "Verification code input");
    }
    
    public Result pasteArchiveCode(String code) {
        WaitVisible(archiveVerifyCodeInput);
        return enterText(archiveVerifyCodeInput, code, "ArchiveVerifyCodeInput code input");
    }

    public Result next() {
        WaitVisible(nextButton);
        ScrollToElement(nextButton);
        WaitClickable(nextButton);
        return ClickControl(nextButton, "Next button");
    }
    
    public Result verifyArchiveCodeNext() {
        WaitVisible(verifyArchiveCode_next);
        ScrollToElement(nextButton);
        WaitClickable(verifyArchiveCode_next);
        return ClickControl(verifyArchiveCode_next, "Next button");
    }
    
    public Result clickCloseAccountButton() {
        WaitVisible(closeAccountButton);
        ScrollToElement(nextButton);
        WaitClickable(closeAccountButton);
        return ClickControl(closeAccountButton, "Close Account button");
    }
    
    public Result clickCloseAccountTermsAndConditions() {
        WaitVisible(confirm_close_account_T_and_C);
        ScrollToElement(nextButton);
        WaitClickable(confirm_close_account_T_and_C);
        return ClickControl(confirm_close_account_T_and_C, "Agree close account T and C");
    }

    public Result resendVerificationCode() {
        WaitVisible(resendCodeLink);
        ScrollToElement(nextButton);
        return ClickControl(resendCodeLink, "Resend code link");
    }

    public String getErrorNotificationText() {
        WaitVisible(errorNotification);
        return errorNotification.getText();
    }
    
    public String getVerificationCodeErrorText() {
        WaitVisible(verificationCodeErrorMessage);
        return verificationCodeErrorMessage.getText();
    }

    public String getTextFromPage() {
        WaitVisible(textBox);
        return textBox.getText();
    }
}
