package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCCorporatePaymentDetailsBookParkingSession extends Base_Page {

    @FindBy(xpath = "//input[@name='labyrinth_CorpPayment_next']")
    private WebElement nextButton;

    @FindBy(xpath = "//input[@name='corporate']")
    private List<WebElement> corporateChooses;

    public WCCCorporatePaymentDetailsBookParkingSession(WebDriver driver) {
        super(driver);
    }

    public Result clickYes() {
        return ClickControl(corporateChooses.get(0), "Yes button");
    }

    public Result clickNo() {
        return ClickControl(corporateChooses.get(1), "No button");
    }

    public Result next() {
        WaitVisible(nextButton);
        return ClickControl(nextButton, "Next button");
    }
}
