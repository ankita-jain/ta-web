package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import Structures.Result;

public class WCCResidentPermitInfoPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1[1]")
    @CacheLookup
    private WebElement residentPermitInfoPageTitle;
    @FindBy(xpath = "//*[@name='labyrinth_rvpinfo_next'][1]")
    @CacheLookup
    private WebElement standardResidentPermit;
    @FindBy(xpath = "//*[@name='labyrinth_rvpinfo_next'][2]")
    @CacheLookup
    private WebElement ecoPermit;
    @FindBy(xpath = "//*[@name='labyrinth_rvpinfo_next'][3]")
    @CacheLookup
    private WebElement motorCyclePermit;

    public WCCResidentPermitInfoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getResidentPermitInfoPageTitle() {
        WaitVisible(residentPermitInfoPageTitle);
        return residentPermitInfoPageTitle.getText();
    }

    public Result clickStandardResidentPermit() {
        WaitVisible(standardResidentPermit);
        return ClickControl(standardResidentPermit, "Selecting standard resident permit");
    }

    public void clickEcoPermit() {
        WaitVisible(ecoPermit);
        ecoPermit.click();
    }

    public void clickMotorCyclePermit() {
        WaitVisible(motorCyclePermit);
        motorCyclePermit.click();
    }


}
