package PageObjects;

import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static SeleniumHelpers.WebElementMethods.*;


public class WCCEditPaymentDetailsPage extends Base_Page {

    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement editPaymentPageTitle;
    @FindBy(xpath = "//div[contains(@class, 'div-Member_Ccexpire')]//span")
    private WebElement errorInExpireDateSection;
    @FindBy(xpath = "//*[@id='field-Member_Ccnumber']")
    private WebElement cardNumber;
    @FindBy(xpath = "//*[@id='field-Member_Ccexpire']")
    private WebElement cardExpiryMonth;
    @FindBy(xpath = "//*[@name='Member_Ccexpire[y]']")
    private WebElement cardExpiryYear;
    @FindBy(xpath = "//*[@name='submits[submit]']")
    private WebElement saveButton;
    @FindBy(xpath = "//*[@name='submits[cancel]']")
    private WebElement cancelButton;

    public WCCEditPaymentDetailsPage(WebDriver driver) {
        super(driver);
    }

    public String getPaymenPageTitle() {
        WaitVisible(editPaymentPageTitle);
        return editPaymentPageTitle.getText();
    }

    public Result enterCardNumber(String cc_number) {
        WaitVisible(cardNumber);
        return enterText(cardNumber, cc_number, "Card number input");
    }

    public Result selectCardExpiryMonth(String cc_expiry_month) {
        WaitVisible(cardExpiryMonth);
        return selectItemFromDropdownByTextValue(cardExpiryMonth, cc_expiry_month, "Card Expiry month select");
    }
    
    public Result selectCardExpiryMonthByValue(String cc_expiry_month) {
        WaitVisible(cardExpiryMonth);
        return selectItemFromDropdownByOptionValue(cardExpiryMonth, cc_expiry_month, "Card Expiry month select");
    }

    public Result clickSaveButton() {
        WaitVisible(saveButton);
        return ClickControl(saveButton, "Card Expiry year select");
    }


    public String getErrorInExpireDateSectionAsAText() {
        WaitVisible(errorInExpireDateSection);
        return errorInExpireDateSection.getText();
    }

    public Result selectCardExpiryYear(String cc_expiry_year) {
        WaitVisible(cardExpiryYear);
        return selectItemFromDropdownByTextValue(cardExpiryYear, cc_expiry_year, "Card expire year");
    }
}
