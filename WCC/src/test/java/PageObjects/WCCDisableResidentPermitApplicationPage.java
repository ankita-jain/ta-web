package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class WCCDisableResidentPermitApplicationPage extends PageClass {

    public WebDriver driver;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    @CacheLookup
    private WebElement pageheader;
    @FindBy(xpath = "//*[@id='field-Member_Title']")
    @CacheLookup
    private WebElement title;
    @FindBy(xpath = "//*[@id='field-Member_Firstname']")
    @CacheLookup
    private WebElement firstname;
    @FindBy(xpath = "//*[@id='field-Member_Lastname']")
    @CacheLookup
    private WebElement surname;
    @FindBy(xpath = "//*[@id='addAddressLink1']")
    @CacheLookup
    private WebElement addnewaddress;
    @FindBy(xpath = "//*[@id='addAddressLink3']")
    @CacheLookup
    private WebElement add_new_address_of_driver;
    @FindBy(xpath = "//*[@id='field-AddressLine1']")
    @CacheLookup
    private WebElement addressline1;
    @FindBy(xpath = "//*[@id='field-Town']")
    @CacheLookup
    private WebElement town;
    @FindBy(xpath = "//*[@id='field-County']")
    @CacheLookup
    private WebElement county;
    @FindBy(xpath = "//*[@id='field-Postcode']")
    @CacheLookup
    private WebElement postcode;
    @FindBy(xpath = "//*[@id='ApplicantGender_3031_male']")
    @CacheLookup
    private WebElement gender_male;
    @FindBy(xpath = "//*[@id='ApplicantGender_3031_female']")
    @CacheLookup
    private WebElement gender_female;
    @FindBy(xpath = "//*[@id='ApplicantHomePhone_3616']")
    @CacheLookup
    private WebElement telephonenumber;
    @FindBy(xpath = "//*[@id='ApplicantNINumber_3037']")
    @CacheLookup
    private WebElement NInumber;
    @FindBy(xpath = "//*[@name='ApplicantDOB_3038[d]']")
    @CacheLookup
    private WebElement DOBDay;
    @FindBy(xpath = "//*[@name='ApplicantDOB_3038[M]']")
    @CacheLookup
    private WebElement DOBMonth;
    @FindBy(xpath = "//*[@name='ApplicantDOB_3038[Y]']")
    @CacheLookup
    private WebElement DOBYear;
    @FindBy(xpath = "//*[@id='BlueBadgeOrWhiteBadges_3637']")
    @CacheLookup
    private WebElement BadgeType;
    @FindBy(xpath = "//*[@id='DisabilityType_3166']")
    @CacheLookup
    private WebElement disability_type;
    @FindBy(xpath = "//*[@id='MobilityAssessment_3176']")
    @CacheLookup
    private WebElement mobilityassessment;
    @FindBy(xpath = "//*[@id='field-vehicleoptions1']")
    @CacheLookup
    private WebElement VRM;
    @FindBy(xpath = "//*[@id='VehicleUser_3617']")
    @CacheLookup
    private WebElement disabled_applicant_dirver_or_passenger;
    @FindBy(xpath = "//*[@id='VehicleDriverName_3052']")
    @CacheLookup
    private WebElement driver_name;
    @FindBy(xpath = "//*[@id='addvehicle1']")
    @CacheLookup
    private WebElement add_vehicle_registration;
    @FindBy(xpath = "//*[@name='selectvehicle1']")
    @CacheLookup
    private WebElement new_vrn_number_plate;
    @FindBy(xpath = "//*[@id='VehicleKeeperName_3047']")
    @CacheLookup
    private WebElement vehicle_keeper_name;
    @FindBy(xpath = "//*[@id='VehicleKeeperAddress_3048']")
    @CacheLookup
    private WebElement vehicle_keeper_address1;
    @FindBy(xpath = "//*[@id='VehicleKeeperAddress_3048_Address2']")
    @CacheLookup
    private WebElement vehicle_keeper_address2;
    @FindBy(xpath = "//*[@id='VehicleKeeperAddress_3048_Address3']")
    @CacheLookup
    private WebElement vehicle_keeper_address3;
    @FindBy(xpath = "//*[@id='VehicleKeeperAddress_3048_Town']")
    @CacheLookup
    private WebElement vehicle_keeper_town;
    @FindBy(xpath = "//*[@id='VehicleKeeperAddress_3048_County']")
    @CacheLookup
    private WebElement vehicle_keeper_county;
    @FindBy(xpath = "//*[@id='VehicleKeeperAddress_3048_Postcode']")
    @CacheLookup
    private WebElement vehicle_keeper_postcode;
    @FindBy(xpath = "//*[@id='VehicleDriverAddress_3091_Address1']")
    @CacheLookup
    private WebElement vehicle_driver_address1;
    @FindBy(xpath = "//*[@id='VehicleDriverAddress_3091_Address2']")
    @CacheLookup
    private WebElement vehicle_driver_address2;
    @FindBy(xpath = "//*[@id='VehicleDriverAddress_3091_Address3']")
    @CacheLookup
    private WebElement vehicle_driver_address3;
    @FindBy(xpath = "//*[@id='VehicleDriverAddress_3091_Town']")
    @CacheLookup
    private WebElement vehicle_driver_town;
    @FindBy(xpath = "//*[@id='VehicleDriverAddress_3091_County']")
    @CacheLookup
    private WebElement vehicle_driver_county;
    @FindBy(xpath = "//*[@id='VehicleDriverAddress_3091_Postcode']")
    @CacheLookup
    private WebElement vehicle_driver_postcode;
    @FindBy(xpath = "//*[@id='buttons']/ol/li[1]/div/div/input[2]")
    @CacheLookup
    private WebElement back_button;
    @FindBy(xpath = "//*[@id='buttons']/ol/li[1]/div/div/input[1]")
    @CacheLookup
    private WebElement next_step_button;

    public WCCDisableResidentPermitApplicationPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getPageHeader() {
        WaitVisible(pageheader);
        return pageheader.getText();
    }

    public void selectTitle(String Title) {
        WaitVisible(title);
        Select Member_title = new Select(title);
        Member_title.selectByVisibleText(Title);
    }

    public void enterFirstName(String Firstname) {
        WaitVisible(firstname);
        firstname.clear();
        firstname.sendKeys(Firstname);
    }

    public void enterSurName(String Lastname) {
        WaitVisible(surname);
        surname.clear();
        surname.sendKeys(Lastname);
    }

    public void clickAddNewAddress() {
        WaitVisible(addnewaddress);
        addnewaddress.click();
    }

    public void clickAddNewAddressOfDriver() {
        WaitVisible(add_new_address_of_driver);
        add_new_address_of_driver.click();
    }

    public void enterAddressLine1(String AddressLine1) {
        WaitVisible(addressline1);
        addressline1.sendKeys(AddressLine1);
    }

    public void enterTown(String Town) {
        WaitVisible(town);
        town.sendKeys(Town);
    }

    public void selectCounty(String County) {
        Select select_county = new Select(county);
        select_county.selectByVisibleText(County);
    }

    public void enterPostCode(String Postcode) {
        WaitVisible(postcode);
        postcode.sendKeys(Postcode);
    }

    public void clickGenderMale() {
        jsClick(gender_male);
    }

    public void clickGenderFemale() {
        jsClick(gender_female);
    }

    public void enterTelephoneNumber(String Phonenumber) {
        WaitVisible(telephonenumber);
        telephonenumber.sendKeys(Phonenumber);
    }

    public void enterNINumber(String NINumber) {
        WaitVisible(NInumber);
        NInumber.sendKeys(NINumber);
    }

    public void selectDobDay(String dobday) {
        WaitVisible(DOBDay);
        Select select_dob_day = new Select(DOBDay);
        select_dob_day.selectByVisibleText(dobday);
    }

    public void selectDobMonth(String dobmonth) {
        WaitVisible(DOBMonth);
        Select select_dob_month = new Select(DOBMonth);
        select_dob_month.selectByVisibleText(dobmonth);
    }

    public void selectDobYear(String dobyear) {
        WaitVisible(DOBYear);
        Select select_dob_year = new Select(DOBYear);
        select_dob_year.selectByVisibleText(dobyear);
    }

    public void selectBadgeType(String badgetype) {
        Select badgetypes = new Select(BadgeType);
        badgetypes.selectByVisibleText(badgetype);
    }

    public void selectDisabilityType(String disability) {
        Select disabilities = new Select(disability_type);
        disabilities.selectByVisibleText(disability);
    }

    public void selectMobilityAssessment(String mobility_assessment) {
        Select mobilityassessments = new Select(mobilityassessment);
        mobilityassessments.selectByVisibleText(mobility_assessment);
    }

    public void enterVRM(String Vrm) {
        Select VRMs = new Select(VRM);
        VRMs.selectByVisibleText(Vrm);
    }

    public void selectApplicantDriverOrPassenger(String driver_or_passenger) {
        Select Driver_or_Passenger = new Select(disabled_applicant_dirver_or_passenger);
        Driver_or_Passenger.selectByVisibleText(driver_or_passenger);
    }

    public void enterDriverName(String name) {
        WaitVisible(driver_name);
        driver_name.sendKeys(name);
    }

    public void clickAddRegistration() {
        WaitVisible(add_vehicle_registration);
        add_vehicle_registration.click();
    }

    public void enterNewNumberPlate(String VRN) {
        WaitVisible(new_vrn_number_plate);
        new_vrn_number_plate.sendKeys(VRN);
    }

    public void enterVehicleKeeperName(String vehiclekeepername) {
        WaitVisible(vehicle_keeper_name);
        vehicle_keeper_name.sendKeys(vehiclekeepername);
    }

    public void enterVehicleKeeperAddress1(String address1) {
        WaitVisible(vehicle_keeper_address1);
        vehicle_keeper_address1.sendKeys(address1);
        new Select(vehicle_keeper_address1).selectByIndex(1);
        
    }

    public void enterVehicleKeeperAddress2(String address2) {
        WaitVisible(vehicle_keeper_address2);
        vehicle_keeper_address2.sendKeys(address2);
    }

    public void enterVehicleKeeperAddress3(String address3) {
        WaitVisible(vehicle_keeper_address3);
        vehicle_keeper_address3.sendKeys(address3);
    }

    public void enterVehicleKeeperTown(String Vehicle_keeper_town) {
        WaitVisible(vehicle_keeper_town);
        vehicle_keeper_town.sendKeys(Vehicle_keeper_town);
    }

    public void selectVehicleKeeperCounty(String county) {
        WaitVisible(vehicle_keeper_county);
        new Select(vehicle_keeper_county).selectByVisibleText(county);
    }

    public void enterVehicleKeeperPostcode(String postcode) {
        WaitVisible(vehicle_keeper_postcode);
        vehicle_keeper_postcode.sendKeys(postcode);
    }

    public void enterVehicleDriverAddress1(String address1) {
        WaitVisible(vehicle_driver_address1);
        vehicle_driver_address1.sendKeys(address1);
    }

    public void enterVehicleDriverAddress2(String address2) {
        WaitVisible(vehicle_driver_address2);
        vehicle_driver_address2.sendKeys(address2);
    }

    public void enterVehicleDriverAddress3(String address3) {
        WaitVisible(vehicle_driver_address3);
        vehicle_driver_address3.sendKeys(address3);
    }

    public void enterVehicleDriverTown(String town) {
        WaitVisible(vehicle_driver_town);
        vehicle_driver_town.sendKeys(town);
    }

    public void selectVehicleDriverCounty(String county) {
        Select counties = new Select(vehicle_driver_county);
        counties.selectByVisibleText(county);
    }

    public void enterDriverPostcode(String postcode) {
        WaitVisible(vehicle_driver_postcode);
        vehicle_driver_postcode.sendKeys(postcode);
    }

    public void clickBackButton() {
        WaitVisible(back_button);
        back_button.click();
    }

    public void clickNextStepButton() {
        WaitVisible(next_step_button);
        next_step_button.click();
    }


    //BLUE BADGE
    public void fillApplicantDetails(String title, String firstname, String lastname, String addressline1, String town, String county, String postcode, String phonenumber, String ninumber, String dobday, String dobmonth, String dobyear, String badgetype, String disability, String mobility_assessment) {
        selectTitle(title);
        enterFirstName(firstname);
        enterSurName(lastname);
        clickAddNewAddress();
        enterAddressLine1(addressline1);
        enterTown(town);
        selectCounty(county);
        enterPostCode(postcode);
        clickGenderMale();
        enterTelephoneNumber(phonenumber);
        enterNINumber(ninumber);
        selectDobDay(dobday);
        selectDobMonth(dobmonth);
        selectDobYear(dobyear);

        selectBadgeType(badgetype);

        selectDisabilityType(disability);

        selectMobilityAssessment(mobility_assessment);

        clickNextStepButton();
    }

    //WHITE AND BLUE BADGE
    public void fillApplicantDetailsWhiteAndBlue(String title, String firstname, String lastname, String addressline1, String town,
                                                 String county, String postcode, String phonenumber, String ninumber, String dobday,
                                                 String dobmonth, String dobyear, String badgetype, String disability, String mobility_assessment, String Vrm,
                                                 String driver_or_passenger, String vehiclekeepername, String address1, String address2, String address3,
                                                 String Vehicle_keeper_town, String Vehicle_keeper_county, String Vehicle_keeper_postcode) {
        selectTitle(title);
        enterFirstName(firstname);
        enterSurName(lastname);
        clickAddNewAddress();
        enterAddressLine1(addressline1);
        enterTown(town);
        selectCounty(county);
        enterPostCode(postcode);
        clickGenderMale();
        enterTelephoneNumber(phonenumber);
        enterNINumber(ninumber);
        selectDobDay(dobday);
        selectDobMonth(dobmonth);
        selectDobYear(dobyear);

        selectBadgeType(badgetype);
        selectDisabilityType(disability);
        selectMobilityAssessment(mobility_assessment);

        enterVRM(Vrm);

        selectApplicantDriverOrPassenger(driver_or_passenger);
        enterVehicleKeeperName(vehiclekeepername);
        clickAddNewAddressOfDriver();
        enterVehicleKeeperAddress1(address1);
        //enterVehicleKeeperAddress2(address2);
        //enterVehicleKeeperAddress3(address3);
        //enterVehicleKeeperTown(Vehicle_keeper_town);
        //selectVehicleKeeperCounty(Vehicle_keeper_county);
       // enterVehicleKeeperPostcode(Vehicle_keeper_postcode);

        clickNextStepButton();


    }

}
