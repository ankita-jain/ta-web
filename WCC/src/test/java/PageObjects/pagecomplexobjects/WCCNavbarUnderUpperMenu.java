package PageObjects.pagecomplexobjects;

import Structures.Result;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCNavbarUnderUpperMenu {

    @FindBy(xpath = "//div[@id='navbar']//a[@href='/statements']")
    private WebElement statementsNav;

    @FindBy(xpath = "//a[@href='/bookparking']")
    private WebElement payToParkNav;

    public WCCNavbarUnderUpperMenu(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public Result clickOnStatementsNav() {
        return ClickControl(statementsNav, "Statements menu");
    }

    public Result clickPayToParkNav() {
        return ClickControl(payToParkNav, "Pay to park menu");
    }

}
