package PageObjects;

import GenericComponents.complexwebobjects.Table;
import SeleniumHelpers.Base_Page;
import SeleniumHelpers.WaitMethods;
import Structures.Result;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static SeleniumHelpers.WebElementMethods.ClickControl;

public class WCCMyVehiclesPage extends Base_Page {
    private ConfirmDeletionForm confirmDeletionForm = new ConfirmDeletionForm();
    private String removeXPathLink = "//td/p/a[contains(@href, 'delete/%s')]";

    @FindBy(xpath = "//ul[@class='screennotifications']//p")
    private WebElement successNotificationText;

    @FindBy(xpath = "//div[@class='vehiclelinks']/a")
    private WebElement changeVehicleOnSession;

    @FindBy(xpath = "//table[@id='dgridtable1']")
    private WebElement vehicleTable;

    private Table table;
    @FindBy(xpath = "//a[@class='ringgo-tooltip' and contains(text(), 'Add a Vehicle')]")
    private WebElement addAVehicleLink;
    @FindBy(xpath = "//*[@class='page-head']//h1")
    private WebElement vehiclePageTitle;

    public WCCMyVehiclesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        table = new Table(vehicleTable);
    }

    public Table getTable() {
        return table;
    }

    public ConfirmDeletionForm getConfirmDeletionForm() {
        return confirmDeletionForm;
    }

    public WebElement getSuccessNotificationText() {
        return successNotificationText;
    }

    public String getVehiclePageTiltle() {
        WaitVisible(vehiclePageTitle);
        return vehiclePageTitle.getText();
    }

    public Result clickAddAVehicleLink() {
        WaitVisible(addAVehicleLink);
        return ClickControl(addAVehicleLink, "Add a Vehicle link");
    }

    public Result clickRemoveVehicle(String carVRN) {
        return ClickControl(_driver.findElement(By.xpath(String.format(removeXPathLink, carVRN))), "Delete link");
    }

    public Result clickEditVehicle(String carVRN) {
        WaitVisible(vehicleTable);
        WebElement editLink = table.getCell(table.getRowIndexByCellValue(carVRN, MyVehicleTable.VEHICLE), MyVehicleTable.EDIT);
        return ClickControl(editLink, "Edit link");
    }

    public By getRemoveXPathLink(String carVRN) {
        return By.xpath(String.format(removeXPathLink, carVRN));
    }

    public Result changeVehicleOnSession() {
        return ClickControl(changeVehicleOnSession, "Change vehicle on session link");
    }

    public enum MyVehicleTable {
        DEFAULT("Default (SMS)"),
        VEHICLE("Vehicle"),
        MAKE("Make"),
        COLOUR("Colour"),
        DETAILS("Details"),
        OWNERSHIP_TYPE("Ownership Type"),
        EDIT("Edit");
        String headerValue;

        MyVehicleTable(String header) {
            headerValue = header;
        }

        @Override
        public String toString() {
            return headerValue;
        }
    }

    public class ConfirmDeletionForm {
        @FindBy(xpath = "//input[@type='submit' and @value='Yes']")
        private WebElement yesButton;
        @FindBy(xpath = "//input[@type='submit' and @value='No']")
        private WebElement noButton;

        private ConfirmDeletionForm() {
            PageFactory.initElements(_driver, this);
        }

        public Result clickYesButton() {
            return ClickControl(yesButton, "Yes button");
        }
    }
}
