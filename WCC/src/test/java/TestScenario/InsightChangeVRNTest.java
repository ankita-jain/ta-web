package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;
import com.relevantcodes.extentreports.LogStatus;

import org.apache.poi.util.DelayableLittleEndianOutput;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

public class InsightChangeVRNTest extends RingGoScenario {

    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
    	NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_vrn_change_from_insight", dataProviderClass = DataProviders.DP_WCC.class, groups = {"vrn_change_on_permit_via_insight"}, dependsOnGroups = {"authorise_permit_via_insight_for_vrn_change"})
    public void authorisePermitApplication(String Username, String Password, String Operator_Status, String Operator, String VRM,
                                           String Permit_Status, String New_vrn, String Change_Permit_Status_To,
                                           String Payment_for, String Card_number, String Exp_month, String Exp_year, String CV2) throws IOException {
        StartTest("10568", "Change VRN from insight (All resident permit)");

        try {
            String permit_number;
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            permit_number = InsightHelper.searchPermitAndEdit(VRM, Permit_Status, _driver);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Resident Permit Application"));
            insightpermitapplicationpage.selectVRN(New_vrn);
            insightpermitapplicationpage.editPermitStatusAndSave(Change_Permit_Status_To);

            InsightPaymentPage insightpaymentpage = new InsightPaymentPage(_driver);
            assertTrue("Checking Charge for Permit Changes? title", insightpaymentpage.getInsightPaymentPageTitle().contains("Charge for Permit Changes?"));
            insightpaymentpage.payForPermitCharges(Payment_for, Card_number, Exp_month, Exp_year, CV2);
            delay(5000);

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
