package TestScenario.Insight;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightSearch;

public class Verify_Using_Search_Function  extends RingGoScenario {
	
	private String userid;
	private String vehicleVRN;
	
	 @BeforeMethod
	    public void setUp() throws Exception {
		 	createUser("defaultNewUser.json");
	        userid = (String) newUserJson.get("UserID");
	        
	    }
	 
     @Test
	 public void searchUserCLI() throws IOException {
		 
		    StartTest("20585", "Verify using the search function- Search by CLI");
		    
		    LogStep("Step - 1", "Login to Insight");
		    NavigationHelper.openInsight(_driver);
	        
			LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);
			
			LogStep("Step - 2", "Enter a valid, registered CLI in the search box and click 'Search'");
			InsightSearch insightsearch = new InsightSearch(_driver);
			CheckValue(insightsearch.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
			CheckResult(insightsearch.enterSearchTerm(mobile), "Enter CLI");
			CheckResult(insightsearch.selectSearchType("Telephone Number"), "Select 'Telephone Number' as Search type");
	        CheckResult(insightsearch.clickSubmit(), "Click 'Search' button");
	        
	        LogStep("Step - 3", "Customer information should appear");
	        CheckBool(insightsearch.isSearchResultVisible(), "Checking is customer details visible?");
	        CheckValue(insightsearch.getUserCLI(), mobile, "Checking user CLI matches with Search CLI");
	        
	        LogStep("Step - 4", "Logout insight");
	        
	        delay(2000);
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	        insightnavmenu.clickAccount();
	        
	        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	        insightlogoutpage.clickLogout();  
	 
	 }
	 
	 @Test
	 public void searchUserVRN() throws IOException {
		    
		   /*
		    * ADD a new VRN on the user account. 
		    */
		    
		    vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
		    
		    StartTest("20585", "Verify using the search function - Search by VRN");
		    
		    LogStep("Step - 1", "Login to Insight");
		    NavigationHelper.openInsight(_driver);
	        
			LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);
			
			LogStep("Step - 2", "Enter a valid, registered VRN in the search box and click 'Search'");
			InsightSearch insightsearch = new InsightSearch(_driver);
			CheckValue(insightsearch.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
			CheckResult(insightsearch.enterSearchTerm(vehicleVRN), "Enter VRN");
			CheckResult(insightsearch.selectSearchType("Vehicle Registration Number (Exact)"), "Select VRN as Search type");
	        CheckResult(insightsearch.clickSubmit(), "Click 'Search' button");
	        
	        LogStep("Step - 3", "Customer information should appear");
	        CheckBool(insightsearch.isSearchResultVisible(), "Checking is customer details visible?");
	       	  
	        delay(2000);
	        LogStep("Step - 4", "Logout insight");
	        
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	        insightnavmenu.clickAccount();
	        
	        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	        insightlogoutpage.clickLogout();  
	 
	 }
	 
	 
	 @Test
	 public void searchVRNRegisteredForMultipleUsers() throws Exception {
		 
		  String newemail, newpassword;
		  String firstemail=email;
		  String firstpassword=password;
		   /*
		    * ADD a new VRN on the user account. 
		    *
		    * */		    
		    vehicleVRN = RestApiHelper.addNewVehicleNoVRM(firstemail, firstpassword, RestApiHelper.defaultNewVehicle());
		  		    
		    /*
		     *  Create a new user with same VRN of the above user
		     * 
		     */
		    createUser("defaultNewUser.json"); // Note: at this point the inherited email/password will change
	        newemail = email;
	        newpassword = password;
	   
	        Map<Object,Object> newVehicle = RestApiHelper.defaultNewVehicle();
	        newVehicle.put("VRM", vehicleVRN);
	        RestApiHelper.addNewVehicle(newemail, newpassword, newVehicle);
		    
		    StartTest("20585", "Verify using the search function - Search by VRN associated with multiple users");
		    
		    LogStep("Step - 1", "Login to Insight");
		    NavigationHelper.openInsight(_driver);
	        
			LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);
			
			LogStep("Step - 2", "Enter a valid, registered VRN in the search box and click 'Search'");
			InsightSearch insightsearch = new InsightSearch(_driver);
			CheckValue(insightsearch.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
			CheckResult(insightsearch.enterSearchTerm(vehicleVRN), "Enter VRN");
			CheckResult(insightsearch.selectSearchType("Vehicle Registration Number (Exact)"), "Select VRN as Search type");
	        CheckResult(insightsearch.clickSubmit(), "Click 'Search' button");
	        
	        LogStep("Step - 3", "Customer information should appear");
	        CheckBool(insightsearch.isSearchResultMultipleUserVisible(), "Checking is 'Found 2 users matching search criteria. '");
	       	  
	        delay(2000);
	        LogStep("Step - 4", "Logout insight");
	        
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	        insightnavmenu.clickAccount();
	        
	        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	        insightlogoutpage.clickLogout();  
	 
	 }
	 
	 
	 
	@Test
	 public void searchByRingGoUserID() throws IOException {
		  		    
		    StartTest("20585", "Verify using the search function - Search by RingGoUserID");
		    
		    LogStep("Step - 1", "Login to Insight");
		    NavigationHelper.openInsight(_driver);

			LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);
			
			LogStep("Step - 2", "Enter a valid, registered CLI in the search box and click 'Search'");
			InsightSearch insightsearch = new InsightSearch(_driver);
			CheckValue(insightsearch.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
			CheckResult(insightsearch.enterSearchTerm(userid), "Enter Ringgo UserID");
			CheckResult(insightsearch.selectSearchType("RingGo User ID"), "Select VRN as Search type");
	        CheckResult(insightsearch.clickSubmit(), "Click 'Search' button");
	        
	        LogStep("Step - 3", "Customer information should appear");
	        CheckBool(insightsearch.isSearchResultVisible(), "Checking is customer details visible?");
	       	
	        delay(2000);
	        LogStep("Step - 4", "Logout insight");
	        
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	        insightnavmenu.clickAccount();
	        
	        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	        insightlogoutpage.clickLogout();  
	 
	 } 
	 
	 
	 @Test
	 public void searchByRingGoUserEmail() throws IOException {
		  		    
		    StartTest("20585", "Verify using the search function - Search by Email ID");
		    
		    LogStep("Step - 1", "Login to Insight");
		    NavigationHelper.openInsight(_driver);

			LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);
			
			LogStep("Step - 2", "Enter a valid, registered CLI in the search box and click 'Search'");
			InsightSearch insightsearch = new InsightSearch(_driver);
			CheckValue(insightsearch.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
			CheckResult(insightsearch.enterSearchTerm(email), "Enter Ringgo email");
			CheckResult(insightsearch.selectSearchType("Email"), "Select Email as Search type");
	        CheckResult(insightsearch.clickSubmit(), "Click 'Search' button");
	        
	        LogStep("Step - 3", "Customer information should appear");
	        CheckBool(insightsearch.isSearchResultVisible(), "Checking is customer details visible?");
	        CheckValue(insightsearch.getUserEmail(), email, "Checking user email matches with Search email");
	       	
	        delay(2000);
	        LogStep("Step - 4", "Logout insight");
	        
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	        insightnavmenu.clickAccount();
	        
	        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	        insightlogoutpage.clickLogout();  
	 
	 }


}
