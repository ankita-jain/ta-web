package TestScenario.Insight;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Insight.InsightRefund;
import PageObjects.Insight.InsightSearch;

public class Processing_Full_Refund extends RingGoScenario {
	
	String vehicleVRN;
	
    String zoneId = "127952";
	String hours = "1";
	String minutes = "0";
	
	@BeforeMethod
    public void setUp() throws Exception {		   
	   
       createUser("defaultNewUser.json");  
        
       vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());	
          
        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();
        
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
        String paymentMethod = (String) usersPayment.get("Id");
        
        //Creates a user session
        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);	
    }
	
	@Test
	public void fullRefundTest() throws IOException {
		
		  StartTest("326", "Processing a full refund");
		   
		   LogStep("1", "Open Insight");
		   NavigationHelper.openInsight(_driver);
		   
	       LogStep("2", "Log on to Insight");

	   	   LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);
		   
		   delay(5000);
		   
		   LogStep("3", "Go to 'Tools' > 'Refunds'");
		   InsightSearch insightsearch = new InsightSearch(_driver);
		   CheckResult(insightsearch.clickRefund(), "Click Refund button");
		   
		   LogStep("4", "Change the 'transaction type' drop down to 'RingGo'  Search for a CLI of an account which has recently purchased parking");
		   InsightRefund insightrefund = new InsightRefund(_driver);
		   CheckResult(insightrefund.selectTransactionType("RingGo"), "Select 'RingGo' as Transaction type");
		   CheckResult(insightrefund.enterSearchTerm(mobile), "Enter Mobile number in the 'Query' field");
		   CheckResult(insightrefund.selectSearhFieldType("cli"), "Select 'Telephone Number (not spacehopper)'");
		   CheckResult(insightrefund.clickSearch(), "Click 'Search' button");
		   
		   LogStep("5", "click the wrench icon next to any transaction");
		   CheckResult(insightrefund.clickRefundSpannerButton(), "Refund spanner button clicked");
		   
		   //Switch to new Tab
		   List<String> tabs2 = new ArrayList<>(_driver.getWindowHandles());
	       _driver.switchTo().window(tabs2.get(1));
	       
	       LogStep("6", "Enter 'Remarks' & 'Confirm password' and hit confirm button");
	       CheckResult(insightrefund.clickNext(), "Click 'Next' button");	
	       
	       CheckContains(insightrefund.getRefundType(),"Full", "Checking 'Full' refund");
	       CheckResult(insightrefund.enterRemarks("enter some remarks"), "Click 'Next' button");
	       CheckResult(insightrefund.enterConfirmPassword(insightPassword), "Enter confirm password");
	       CheckResult(insightrefund.clickConfirm(), "Click 'Confirm' button");
	       
	       delay(60000);
	       
	       CheckResult(insightrefund.clickRefundButton(), "Click 'Refund' button");
	       
	       LogStep("6", "Enter 'Remarks' & 'Confirm password' and hit confirm button");
	       CheckBool(insightrefund.refundSuccessfulMessage(), "Refund message displayed");
	       
		   
	}	
	
}
