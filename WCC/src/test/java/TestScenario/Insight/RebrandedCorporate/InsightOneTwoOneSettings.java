package TestScenario.Insight.RebrandedCorporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.*;
import Utilities.PropertyUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class InsightOneTwoOneSettings extends RingGoScenario {

    private String insightLogin;
    private String insightPassword;
    private String accountNumber;
    private final String TEXT_TYPE = "text";
    private final String CHECKBOX_TYPE = "checkbox";

    @BeforeClass
    public void loginData() throws IOException {
        insightLogin = PropertyUtils.ReadProperty("INSIGHT_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        accountNumber = PropertyUtils.ReadProperty("1-2-1_CORPORATE_NUMBER", WCC_PROPERTIES);
    }

    @Test
    public void oneTwoOneInsightOption() {
        StartTest("125322", "1-2-1 Insight option");

        navigateToInsightSetUp();
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckValue(editCorporateAccountPage.setupPage.getSingleCliLabelText(), "Single CLI", "Verify Single CLI Label");
        CheckValue(editCorporateAccountPage.setupPage.getSingleCliInputType(), CHECKBOX_TYPE, "Verify Single CLI Input Type");
        CheckValue(editCorporateAccountPage.setupPage.getMultipleCliLabelText(), "Multiple CLI", "Verify Multiple CLI Label");
        CheckValue(editCorporateAccountPage.setupPage.getMultipleCliInputType(), TEXT_TYPE, "Verify Multiple CLI Input Type");
        CheckValue(editCorporateAccountPage.setupPage.getAnyCliLabelText(), "Any CLI", "Verify Any CLI Label");
        CheckValue(editCorporateAccountPage.setupPage.getAnyCliInputType(), CHECKBOX_TYPE, "Verify Any CLI Input Type");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test
    public void chooseTwoSettingOptions() {
        StartTest("125323", "Checking if 2 or more of the fields are complete for 1-2-1 option");

        navigateToInsightSetUp();

        LogStep("Step - 7", "Check 2 option (Single CLI & Any CLI OR Single CLI & Multiple CLI OR Any CLI & Multiple CLI). And click Save");
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckResult(editCorporateAccountPage.setupPage.selectAnyCheckbox(), "Select 'Any'Checkbox");
        CheckResult(editCorporateAccountPage.setupPage.clickOnSaveButton(), "Click on 'Save' button");
        CheckValue(editCorporateAccountPage.setupPage.getCliSettingsErrorMessage(), "Please only apply 1 CLI Setting to the account (in the Setup tab).",
                "Verify 'CLI' settings error message");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test(dataProvider = "invalidData")
    public void invalidCharacterIntoMultipleCli(String invalidNumber) {
        StartTest("125324", "Multiple CLI option with invalid characters");

        navigateToInsightSetUp();

        LogStep("Step - 7", "Enter invalid numbers to Multiple CLI option (see Test Data) and click Save");
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckResult(editCorporateAccountPage.setupPage.enterMultipleCliAmount(invalidNumber), "Enter invalid cli amount");
        CheckResult(editCorporateAccountPage.setupPage.clickOnSaveButton(), "Click on 'Save' button");
        CheckValue(editCorporateAccountPage.setupPage.getMultipleCliError(), "Please amend the Multiple CLI entry, this must only be a numeric value between 2 and 99.",
                "Verify 'CLI' settings error message");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    private void navigateToInsightSetUp() {
        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);

        LogStep("Step - 2", "Enter login details and click Login button");
        LoginHelper.loginInsight(insightLogin, insightPassword, _driver);

        LogStep("Step - 3", "Go to Support -> Corporate -> Account listing");
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");
        SupportPage supportPage = new SupportPage(_driver);
        CheckResult(supportPage.getLeftSideBar().clickCorporate(), "Corporate menu selected");

        LogStep("Step - 4", "Find created corporate account");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getAccountListing().enterAccountNumber(accountNumber), "Enter Account Number");
        CheckResult(insightCorporate.getAccountListing().clickOnFilterButton(), "Click on Filter Button");

        LogStep("Step - 5", "Click on Edit this corporate account");
        InsightCorporateSummaryPage summaryPage = new InsightCorporateSummaryPage(_driver);
        CheckResult(summaryPage.clickOnEditCorporateAccount(), "Click on Edit Corporate Account");

        LogStep("Step - 6", "Click on Setup tab");
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckResult(editCorporateAccountPage.clickOnSetupTab(), "Click on 'Setup' tab");
    }

    @DataProvider
    public Object[][] invalidData() {
        return new Object[][]{{"0"}, {"100"}};
    }
}
