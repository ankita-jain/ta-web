package TestScenario.Insight.RebrandedCorporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Insight.*;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

public class InsightSearchAndDeleteVrnTool extends RingGoScenario {

    private String insightLogin;
    private String insightPassword;
    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;

    @BeforeClass
    public void loginData() throws IOException {
        insightLogin = PropertyUtils.ReadProperty("INSIGHT_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void addVrnIntoAccount() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);
    }

    @Test
    public void verifySearchVrnFunctionality() {
        StartTest("105887", "Verify Search Corporate VRN functionality");

        navigateToInsightVrnTool();

        LogStep("Step - 3", "Enter full VRN");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getSearchVehicle().enterVrn(vrnNumber), "Enter VRN");
        CheckResult(insightCorporate.getSearchVehicle().clickOnSearch(), "Click on Search button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPresent(vrnNumber), "Verify if VRN is present");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test
    public void verifyPartialSearchVrnFunctionality() {
        StartTest("105888", "Verify Partial Search Corporate VRN functionality");

        navigateToInsightVrnTool();

        LogStep("Step - 3", "Enter full VRN");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        String partialVrn = vrnNumber.substring(0, 4);
        CheckResult(insightCorporate.getSearchVehicle().enterVrn(partialVrn), "Enter VRN");
        CheckResult(insightCorporate.getSearchVehicle().clickOnSearch(), "Click on Search button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPartPresent(partialVrn), "Verify if VRN is present");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test
    public void verifyDeleteAction() {
        StartTest("105894", "Delete found VRN from Corporate Account");

        navigateToInsightVrnTool();

        LogStep("Step - 3", "Enter full VRN");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getSearchVehicle().enterVrn(vrnNumber), "Enter VRN");
        CheckResult(insightCorporate.getSearchVehicle().clickOnSearch(), "Click on Search button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPartPresent(vrnNumber), "Verify if VRN is present");

        LogStep("Step - 4", "Click on 'X' button");
        CheckResult(insightCorporate.getSearchVehicle().clickOnDeleteButton(vrnNumber), "Click on delete button");
        CheckContains(insightCorporate.getSearchVehicle().getConfirmDeletion().getConfirmationText(), String.format("Are you sure that you want to delete VRM \"%s\" from the corporate account", vrnNumber), "Verify confirmation deletion text");
        CheckContains(insightCorporate.getSearchVehicle().getConfirmDeletion().getConfirmationText(), "This will send a \"notification of vehicle removal\" email to the primary master admin of", "Verify confirmation deletion text");

        LogStep("Step - 5", "Confirm the action.");
        CheckResult(insightCorporate.getSearchVehicle().getConfirmDeletion().clickOnYesButton(), "Click on 'Yes' button");
        CheckBool(!insightCorporate.getSearchVehicle().isVrnPresent(vrnNumber), "Verify if VRN is removed");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test
    public void verifyCancelDeletionAction() {
        StartTest("105898", "Cancel found VRN from Corporate Account deletion");

        LogStep("Step - 1", "Login to Insight");
        NavigationHelper.openInsight(_driver);
        LoginHelper.loginInsight(insightLogin, insightPassword, _driver);

        LogStep("Step - 2", "Go to the Support -> Corporate -> VRNs");
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");
        SupportPage supportPage = new SupportPage(_driver);
        CheckResult(supportPage.getLeftSideBar().clickCorporate(), "Corporate menu selected");
        CheckResult(supportPage.getLeftSideBar().clickOnSearchVehicle(), "Click on Search Vehicle link");

        LogStep("Step - 3", "Enter full VRN");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getSearchVehicle().enterVrn(vrnNumber), "Enter VRN");
        CheckResult(insightCorporate.getSearchVehicle().clickOnSearch(), "Click on Search button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPartPresent(vrnNumber), "Verify if VRN is present");

        LogStep("Step - 4", "Click on 'X' button");
        CheckResult(insightCorporate.getSearchVehicle().clickOnDeleteButton(vrnNumber), "Click on delete button");
        CheckContains(insightCorporate.getSearchVehicle().getConfirmDeletion().getConfirmationText(), String.format("Are you sure that you want to delete VRM \"%s\" from the corporate account", vrnNumber), "Verify confirmation deletion text");
        CheckContains(insightCorporate.getSearchVehicle().getConfirmDeletion().getConfirmationText(), "This will send a \"notification of vehicle removal\" email to the primary master admin of", "Verify confirmation deletion text");

        LogStep("Step - 5", "Cancel the action.");
        CheckResult(insightCorporate.getSearchVehicle().getConfirmDeletion().clickOnNoButton(), "Click on 'No' button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPartPresent(vrnNumber), "Verify if VRN is removed");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test
    public void verifyVrnWithActiveParkingSessionDeletion() {
        StartTest("105900", "Delete VRN from corporate account which has active parking session");

        addVrnWithActiveParkingSession();

        navigateToInsightVrnTool();

        LogStep("Step - 3", "Enter full VRN with active parking session.");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getSearchVehicle().enterVrn(vrnNumber), "Enter VRN");
        CheckResult(insightCorporate.getSearchVehicle().clickOnSearch(), "Click on Search button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPartPresent(vrnNumber), "Verify if VRN is present");

        LogStep("Step - 4", "Click on 'X' button");
        CheckResult(insightCorporate.getSearchVehicle().clickOnDeleteButton(vrnNumber), "Click on delete button");
        CheckContains(insightCorporate.getSearchVehicle().getConfirmDeletion().getConfirmationText(), String.format("Are you sure that you want to delete VRM \"%s\" from the corporate account", vrnNumber), "Verify confirmation deletion text");
        CheckContains(insightCorporate.getSearchVehicle().getConfirmDeletion().getConfirmationText(), "This will send a \"notification of vehicle removal\" email to the primary master admin of", "Verify confirmation deletion text");
        CheckResult(insightCorporate.getSearchVehicle().getConfirmDeletion().clickOnYesButton(), "Click on 'Yes' button");
        CheckBool(insightCorporate.getSearchVehicle().isVrnPresent(vrnNumber), "Verify if VRN is removed");
        CheckValue(insightCorporate.getSearchVehicle().getErrorNotificationText(), String.format("This VRM %s has an active parking session and cannot be deleted until the session ends.", vrnNumber), "Verify error message");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    @Test
    public void verifySearchOfUnexistingVrn() {
        StartTest("105927", "Search VRN which is not existed");

        navigateToInsightVrnTool();

        LogStep("Step - 3", "Enter nonexistent VRN");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getSearchVehicle().enterVrn(RandomStringUtils.randomAlphanumeric(8).toUpperCase()), "Enter VRN");
        CheckResult(insightCorporate.getSearchVehicle().clickOnSearch(), "Click on Search button");
        CheckBool(!insightCorporate.getSearchVehicle().isVrnPresent(vrnNumber), "Verify if VRN is not found");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }

    private void navigateToInsightVrnTool() {
        LogStep("Step - 1", "Login to Insight");
        NavigationHelper.openInsight(_driver);
        LoginHelper.loginInsight(insightLogin, insightPassword, _driver);

        LogStep("Step - 2", "Go to the Support -> Corporate -> VRNs");
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");
        SupportPage supportPage = new SupportPage(_driver);
        CheckResult(supportPage.getLeftSideBar().clickCorporate(), "Corporate menu selected");
        CheckResult(supportPage.getLeftSideBar().clickOnSearchVehicle(), "Click on Search Vehicle link");
    }


    private void addVrnWithActiveParkingSession() {
    	NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");
        String employeePhone = RandomStringUtils.randomNumeric(11);
        String employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        String employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, "", Collections.singletonList(employeePhone), Collections.singletonList(vrnNumber), true, _driver);

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(corporateEmail, corporatePassword, _driver);
        WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
        CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");
        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        CheckResult(wcccorporatehomepage.clickPark(), "Clicking on Park");

        WCCCorporateParkEmployee wcccorporateparkemployee = new WCCCorporateParkEmployee(_driver);
        wcccorporateparkemployee.selectemployeeAndClickNext(employeeName + " " + employeeSurname + " ()");

        WCCCorporateParkEmployeeSelectVRNPage wcccorporateparkemployeeselectvrnpage = new WCCCorporateParkEmployeeSelectVRNPage(_driver);
        wcccorporateparkemployeeselectvrnpage.clickNextButton();

        WCCCorporateParkEmployeeSelectZonePage wcccroporateemployeeselectzonepage = new WCCCorporateParkEmployeeSelectZonePage(_driver);
        wcccroporateemployeeselectzonepage.enterZoneAndClickNext("8142");

        WCCCorporateParkEmployeeSelectTariffPage wcccorporateparkemployeeselecttariffpage = new WCCCorporateParkEmployeeSelectTariffPage(_driver);
        assertTrue("Parking Employee page title", wcccorporateparkemployeeselecttariffpage.getParkEmployeePageTitle().contains("Park an Employee"));
        wcccorporateparkemployeeselecttariffpage.selectTariffDurationAndClickNext("15 Minutes");

        WCCCorporateParkEmployeeConfirmationPage wcccorporateparkemployeeconfirmationpage = new WCCCorporateParkEmployeeConfirmationPage(_driver);
        assertTrue("Parking Employee page title", wcccorporateparkemployeeconfirmationpage.getParkEmployeePageTitle().contains("Park an Employee"));
        wcccorporateparkemployeeconfirmationpage.clickConfirmButton();
        wcccorporateparkemployeeconfirmationpage.clickFinishButton();

        delay(2000);
        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }

}
