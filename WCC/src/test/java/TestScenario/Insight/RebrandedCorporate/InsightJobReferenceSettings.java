package TestScenario.Insight.RebrandedCorporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.*;
import Utilities.PropertyUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

public class InsightJobReferenceSettings extends RingGoScenario {
    private String insightLogin;
    private String insightPassword;
    private String accountNumber;

    @BeforeClass
    public void loginData() throws IOException {
        insightLogin = PropertyUtils.ReadProperty("INSIGHT_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        accountNumber = PropertyUtils.ReadProperty("CORPORATE_ACCOUNT_NUMBER", WCC_PROPERTIES);
    }

    @Test
    public void verifyJobRefForExistingCorporateAccount() {
        StartTest("110019", "Job Reference Number setting adding");

        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);

        LogStep("Step - 2", "Enter credentials for Insight user from precondition and click Login button");
        LoginHelper.loginInsight(insightLogin, insightPassword, _driver);

        LogStep("Step - 3", "Go to Insight -> Corporate -> Search for Account");
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");
        SupportPage supportPage = new SupportPage(_driver);
        CheckResult(supportPage.getLeftSideBar().clickCorporate(), "Corporate menu selected");

        LogStep("Step - 4", "Search existing corporate account and click Edit");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getAccountListing().enterAccountNumber(accountNumber), "Enter Account Number");
        CheckResult(insightCorporate.getAccountListing().clickOnFilterButton(), "Click on Filter Button");

        LogStep("Step - 5", "Click on Setup tab");
        InsightCorporateSummaryPage summaryPage = new InsightCorporateSummaryPage(_driver);
        CheckResult(summaryPage.clickOnEditCorporateAccount(), "Click on Edit Corporate Account");
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckResult(editCorporateAccountPage.clickOnSetupTab(), "Click on 'Setup' tab");
        CheckBool(editCorporateAccountPage.setupPage.isJobReferenceDisplayed(), "Job Reference Number tick box is visible");

        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();

        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }
}
