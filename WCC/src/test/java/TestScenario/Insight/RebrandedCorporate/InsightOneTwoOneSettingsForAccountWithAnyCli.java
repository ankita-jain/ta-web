package TestScenario.Insight.RebrandedCorporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.*;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

public class InsightOneTwoOneSettingsForAccountWithAnyCli extends RingGoScenario {

    private String insightLogin;
    private String insightPassword;
    private String accountNumber;
    private String corporateEmail;
    private String corporatePassword;

    @BeforeClass
    public void loginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("1-2-1_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("1-2-1_CORPORATE_PASSWORD", WCC_PROPERTIES);
        insightLogin = PropertyUtils.ReadProperty("INSIGHT_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        accountNumber = PropertyUtils.ReadProperty("1-2-1_CORPORATE_NUMBER", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void addUser() throws IOException {
        addEmployeeWithAnyCli();
    }

    @Test
    public void chooseSingleCli() {
        StartTest("125327", "Set Single CLI option for account which has employees with Any CLI option");

        navigateToInsightSetUp();

        LogStep("Step - 7", "Check Single CLI tick box and click Save");
        setCli(true, Optional.empty());

        LoginHelper.logoutInsight(_driver);
    }

    @Test
    public void chooseMultipleCli() {
        StartTest("125328", "Set Multiple CLI option for account which has employee with Any CLI set");

        navigateToInsightSetUp();

        LogStep("Step - 7", "Check Single CLI tick box and click Save");
        setCli(false, Optional.of("2"));

        LoginHelper.logoutInsight(_driver);
    }

    private void navigateToInsightSetUp() {
        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);

        LogStep("Step - 2", "Enter login details and click Login button");
        LoginHelper.loginInsight(insightLogin, insightPassword, _driver);

        LogStep("Step - 3", "Go to Support -> Corporate -> Account listing");
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");
        SupportPage supportPage = new SupportPage(_driver);
        CheckResult(supportPage.getLeftSideBar().clickCorporate(), "Corporate menu selected");

        LogStep("Step - 4", "Find created corporate account");
        InsightCorporate insightCorporate = new InsightCorporate(_driver);
        CheckResult(insightCorporate.getAccountListing().enterAccountNumber(accountNumber), "Enter Account Number");
        CheckResult(insightCorporate.getAccountListing().clickOnFilterButton(), "Click on Filter Button");

        LogStep("Step - 5", "Click on Edit this corporate account");
        InsightCorporateSummaryPage summaryPage = new InsightCorporateSummaryPage(_driver);
        CheckResult(summaryPage.clickOnEditCorporateAccount(), "Click on Edit Corporate Account");

        LogStep("Step - 6", "Click on Setup tab");
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckResult(editCorporateAccountPage.clickOnSetupTab(), "Click on 'Setup' tab");
    }

    private void addEmployeeWithAnyCli() {
    	NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(RandomStringUtils.randomAlphabetic(8), RandomStringUtils.randomAlphabetic(8), "", Collections.singletonList("Any"), Collections.singletonList(RandomStringUtils.randomAlphanumeric(5)), true, _driver);
    }

    private void setCli(boolean isSingle, Optional<String> cliAmount) {
        InsightEditCorporateAccountPage editCorporateAccountPage = new InsightEditCorporateAccountPage(_driver);
        CheckResult(editCorporateAccountPage.setupPage.unselectAnyCheckbox(), "Unselect 'Any' Settings");
        if (isSingle) {
            CheckResult(editCorporateAccountPage.setupPage.unselectAnyCheckbox(), "Unselect 'Any' Settings");
            CheckResult(editCorporateAccountPage.setupPage.selectSingleCheckbox(), "Select 'Single' Settings");
        } else {
            CheckResult(editCorporateAccountPage.setupPage.unselectSingleCheckbox(), "Unselect 'Single' Settings");
            CheckResult(editCorporateAccountPage.setupPage.enterMultipleCliAmount(cliAmount.get()), "Enter cli amount");
        }
        CheckResult(editCorporateAccountPage.setupPage.clickOnSaveButton(), "Click on 'Save' button");
        CheckContains(editCorporateAccountPage.setupPage.getCliSettingsErrorMessage(), "The current employee setup on this account do not match the new CLI setting, please amend this before applying a new setting.",
                "Verify 'CLI' settings error message");
    }
}
