package TestScenario.Insight;
import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.InsightExemptVehiclesListManagement;
import PageObjects.Insight.InsightServiceMenuPage;

public class EndSessionFromExemptVehiclesListByNonSuperAdmin extends RingGoScenario {
	
	String mobileNumber;
	String vrn;
	String fromDate;
	String toDate;
	String zoneId ;
	
	
	String insightUserName, insightPassword;
	
	
	@Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	  public void endSessionFromExemptVehiclesList(String TCID, Map<String, String> data) throws IOException {
		  
		  StartTest(TCID, "Verify that user can End Session From Exempt VehiclesList By NonSuperAdmin");
		  insightUserName=data.get("email");
		  insightPassword=data.get("password");
		  mobileNumber=data.get("cli");
		  vrn=data.get("vehicle");
		  fromDate=data.get("FromDate");
		  toDate=data.get("ToDate");
		  zoneId=data.get("ZoneId");
			
		   LogStep("Step - 1", "Logon Insight");
		   NavigationHelper.openInsight(_driver);
	       LoginHelper.loginInsight(insightUserName, insightPassword, _driver);

	       LogStep("Step - 2", "Click on ExemptVehicles List in Services Menu");
	       InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
           insightservicemenupage.clickServiceMenu();
           insightservicemenupage.clickExemptVehiclesSubmenu();
           
           LogStep("Step - 3", "Click on Add New Exempt vehicle List");
           InsightExemptVehiclesListManagement exemptvehicle = new InsightExemptVehiclesListManagement(_driver);
           exemptvehicle.clickAddNewExemptVehicle();
           
           LogStep("Step - 4", "Add New Exempt vehicle to the List");
           exemptvehicle.enterMobileNumber(mobileNumber);
           exemptvehicle.selectType("Corporate");
           exemptvehicle.selectZone("Zone A, Westminster Permits (65000)");
           exemptvehicle.enterVRN(vrn);
           exemptvehicle.selectJQCalanderFromDate(fromDate);
           exemptvehicle.selectJQCalanderToDate(toDate);
           exemptvehicle.clickSaveButton();
           
           LogStep("Step - 4", "End Session from ExemptVehicle List");
           exemptvehicle.clickEndSession();
           
           LogStep("Step - 5", "Confirm End Session from ExemptVehicle List");
           exemptvehicle.confirmEndSession();
           
           LogStep("Step - 6", "success message validation");
           CheckContains(exemptvehicle.getSuccessText(),"successfully deleted", "Success Message");   
	}
}
