package TestScenario.Insight;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Insight.InsightCustomerAuditPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightWCCHome;
import PageObjects.Insight.SupportPage;

public class InsightCustomerAuditSearchTest  extends RingGoScenario  {
	
	String vehicleVRN;
	
	String zoneId = "223169";
	String hours = "1";
	String minutes = "0";
	
	/*
	 *   The below function, Creates a user, and creates a parking session in Zone '61175'
	 *   
	 */
	
	 @BeforeMethod
	    public void setUp() throws Exception {
		 	
		 	createUser("defaultNewUser.json");               	        
	        vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
	        
	        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
	        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();
	        
	        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
	        String paymentMethod = (String) usersPayment.get("Id");
	        
	        //Creates a user session
	        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);	        
	    }
	 
	  @Test
	  public void searchCustomerForAudit() throws IOException {
		  
		  StartTest("29789", "Verify that the customer audit displays correctly");
		  
		  NavigationHelper.openInsight(_driver);

	       LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);

	       InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
	       CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");
	       
	       SupportPage supportPage = new SupportPage(_driver);
	       CheckResult(supportPage.getLeftSideBar().clickCustomerAudit(), "Click on Customer Audit link ");
	       
	       InsightCustomerAuditPage insightcustomerauditpage = new InsightCustomerAuditPage(_driver);
	       CheckResult(insightcustomerauditpage.enterPhoneNumber(mobile),"Enter mobile number");
	       CheckResult(insightcustomerauditpage.clickSearch(), "Search button is clicked");
	       CheckBool(insightcustomerauditpage.isThisTextVisibleNow("Success"), "Checking status 'Success' result in the searchTable");
	       CheckBool(insightcustomerauditpage.isThisTextVisibleNow(vehicleVRN.toUpperCase()), "Checking status 'VRM' result in the searchTable");
	       
	       /*
	        * Search a customer using VRN
	        * 
	        */
	       
	       insightcustomerauditpage.clearPhoneNumberField();
	       CheckResult(insightcustomerauditpage.enterVRM(vehicleVRN),"Enter VRM");
	       CheckResult(insightcustomerauditpage.clickSearch(), "Search button is clicked");
	       CheckBool(insightcustomerauditpage.isThisTextVisibleNow("Success"), "Checking status 'Success' result in the searchTable");
	       CheckBool(insightcustomerauditpage.isThisTextVisibleNow(vehicleVRN.toUpperCase()), "Checking status 'VRM' result in the searchTable");
	       
	       
	        LogStep("Step - 4", "Logout insight");
	        
	        delay(2000);
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	        insightnavmenu.clickAccount();
	        
	        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	        insightlogoutpage.clickLogout();  

		  
	  }

}
