package TestScenario.Insight;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Insight.InsightRefund;
import PageObjects.Insight.InsightSearch;

public class Processing_Partial_Refund extends RingGoScenario {
	
	String vehicleVRN;
	
    String zoneId = "127952";
	String hours = "1";
	String minutes = "0";
	
	@BeforeMethod
    public void setUp() throws Exception {		   
	   
       createUser("defaultNewUser.json");
       vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());	
          
        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();
        
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
        String paymentMethod = (String) usersPayment.get("Id");
        
        //Creates a user session
        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);	
    }	
	
	@Test
	public void partialRefundTest() throws IOException { 
		
		   StartTest("19060", "Processing a Partial refund");
		   
		   LogStep("1", "Open Insight & Login to insight");
		   NavigationHelper.openInsight(_driver);
		
	       LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);
		   
		   delay(5000);
		   
		   LogStep("2", "Go to 'Tools' > 'Refunds'");
		   InsightSearch insightsearch = new InsightSearch(_driver);
		   CheckResult(insightsearch.clickRefund(), "Click Refund button");
		   
		   LogStep("3", "Change the 'transaction type' drop down to 'RingGo' & Search for a CLI of an account which has recently purchased parking");
		   InsightRefund insightrefund = new InsightRefund(_driver);
		   CheckResult(insightrefund.selectTransactionType("RingGo"), "Select 'RingGo' as Transaction type");
		   CheckResult(insightrefund.enterSearchTerm(mobile), "Enter Mobile number in the 'Query' field");
		   CheckResult(insightrefund.selectSearhFieldType("cli"), "Select 'Telephone Number (not spacehopper)'");
		   CheckResult(insightrefund.clickSearch(), "Click 'Search' button");
		   
		   LogStep("4", "Click the wrench icon next to any transaction");
		   CheckResult(insightrefund.clickRefundSpannerButton(), "Refund spanner button clicked");
		   
		   //Switch to new Tab
		   List<String> tabs2 = new ArrayList<>(_driver.getWindowHandles());
	       _driver.switchTo().window(tabs2.get(1));
	       
	       LogStep("5", "Change the refund amount to a value lower than the transaction value and click 'Next'");
	       CheckResult(insightrefund.enterRefundAmount("200"), "Click 'Next' button");	
	       CheckResult(insightrefund.clickNext(), "Click 'Next' button");
	       
	       LogStep("6", "Fill in the required fields, re-enter your password and click 'refund'");
	       CheckContains(insightrefund.getRefundType(),"Partial", "Checking 'partial' refund");
	       CheckResult(insightrefund.enterRemarks("enter some remarks"), "Click 'Next' button");
	       CheckResult(insightrefund.enterConfirmPassword(insightPassword), "Enter confirm password");
	       CheckResult(insightrefund.clickConfirm(), "Click 'Confirm' button");
	       
	       delay(60000);
	       
	       CheckResult(insightrefund.clickRefundButton(), "Click 'Refund' button");
	       
	       LogStep("7"," Click 'finish'");
	       CheckBool(insightrefund.refundSuccessfulMessage(), "Refund message displayed");
	       
		
	}	
	
	@Test
	public void refundValueMoreThanTransactionValueTest() throws IOException {
		
		 StartTest("19060", "Processing a Partial refund");
		   
		   LogStep("1", "Open Insight & Login to insight");
		   NavigationHelper.openInsight(_driver);
		   
	       LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);
		   
		   delay(5000);
		   
		   LogStep("2", "Go to 'Tools' > 'Refunds'");
		   InsightSearch insightsearch = new InsightSearch(_driver);
		   CheckResult(insightsearch.clickRefund(), "Click Refund button");
		   
		   LogStep("3", "Change the 'transaction type' drop down to 'RingGo' & Search for a CLI of an account which has recently purchased parking");
		   InsightRefund insightrefund = new InsightRefund(_driver);
		   CheckResult(insightrefund.selectTransactionType("RingGo"), "Select 'RingGo' as Transaction type");
		   CheckResult(insightrefund.enterSearchTerm(mobile), "Enter Mobile number in the 'Query' field");
		   CheckResult(insightrefund.selectSearhFieldType("cli"), "Select 'Telephone Number (not spacehopper)'");
		   CheckResult(insightrefund.clickSearch(), "Click 'Search' button");
		   
		   LogStep("4", "Click the wrench icon next to any transaction");
		   CheckResult(insightrefund.clickRefundSpannerButton(), "Refund spanner button clicked");
		   
		   //Switch to new Tab
		   List<String> tabs2 = new ArrayList<>(_driver.getWindowHandles());
	       _driver.switchTo().window(tabs2.get(1));
	       
	       LogStep("5", "Change the refund amount to a value greater than the transaction value and click 'Next'");
	       CheckResult(insightrefund.enterRefundAmount("10000"), "Click 'Next' button");	
	       CheckResult(insightrefund.clickNext(), "Click 'Next' button");
	       
	       LogStep("6","Checking error message due to higher refund value than the transaction");
	       CheckBool(insightrefund.refundErrorMessage(), "Refund message displayed");
		
	}
	
}
