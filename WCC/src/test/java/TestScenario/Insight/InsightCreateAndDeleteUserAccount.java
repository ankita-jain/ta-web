package TestScenario.Insight;

import static Utilities.TimerUtils.delay;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightAddCustomerPage;
import PageObjects.Insight.InsightDeleteCustomerPage;
import Utilities.PropertyUtils;

import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

public class InsightCreateAndDeleteUserAccount extends RingGoScenario {
	private String mobile;
	private String userstatusActive;
	private String userstatusDeleted;
    private String insightUserName;
    private String insightPassword;
    
    @BeforeMethod
    public void navigateToInsight() throws Exception {
    	
    	NavigationHelper.openInsight(_driver);
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_CALL_CENTRE_USER_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        
        mobile = "07"+randomNumeric(11);
        userstatusActive = "Active";
        userstatusDeleted = "Marked for deletion";
    }
    @Test
	 public void createThenDeleteUserCLI() throws IOException {
		 
		 StartTest("97162", "Verify creating a user account using a CLI and then mark it for deletion");
		 
		 LoginHelper.loginInsight(insightUserName, insightPassword, _driver);
		    
		 LogStep("Step - 1", "Enter a not yet registered CLI in the search box and click 'Search'");
		 InsightSearch insightsearch = new InsightSearch(_driver);
		 CheckValue(insightsearch.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
		 CheckResult(insightsearch.enterSearchTerm(mobile), "Enter CLI");
		 CheckResult(insightsearch.selectSearchType("Telephone Number"), "Select 'Telephone Number' as Search type");
	     CheckResult(insightsearch.clickSubmit(), "Click 'Search' button");
	        
	     LogStep("Step - 2", "The Link to create a new Customer should appear - click it");
	     delay(2000);
	     CheckBool(insightsearch.isAddNewCustomerLinkVisible(), "Checking is the link visible?");
	     CheckResult(insightsearch.clickAddNewCustomer(), "Click 'Add new Customer' link");
	     
	     LogStep("Step - 3", "The Add a new Customer page appears populated with the CLI that was searched with");
	     delay(2000);
	     InsightAddCustomerPage insightaddcustomerpage = new InsightAddCustomerPage(_driver);
	     CheckValue(insightaddcustomerpage.getUserCLI(), mobile, "Checking user CLI matches with Search CLI");
	     
	     LogStep("Step - 4", "Click 'Save' to create a new account for this CLI");
	     CheckResult(insightaddcustomerpage.clickSave(), "Click 'Save' button");
	     
	     LogStep("Step - 5", "Search again to see that the Customer account is now there with an 'Active' status");
	     delay(2000);
	     InsightSearch insightsearch2 = new InsightSearch(_driver);
		 CheckValue(insightsearch2.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
		 CheckResult(insightsearch2.enterSearchTerm(mobile), "Enter CLI");
		 CheckResult(insightsearch2.selectSearchType("Telephone Number"), "Select 'Telephone Number' as Search type");
	     CheckResult(insightsearch2.clickSubmit(), "Click 'Search' button");
	     CheckBool(insightsearch2.isSearchResultVisible(), "Checking are customer details visible?");
	     CheckValue(insightsearch2.getUserCLI(), mobile, "Checking user CLI matches with Search CLI");
	     CheckValue(insightsearch2.getCustomerStatusText(), userstatusActive, "Checking that the user has an 'Active' status");
	     
	     LogStep("Step - 6", "Click the 'Delete User' option to delete this user");
	     CheckResult(insightsearch2.clickDeleteLink(), "Click 'Delete' button");
	     
	     LogStep("Step - 7", "In the Delete Customer Page - confirm deletion of the account");
	     InsightDeleteCustomerPage insightdeletecustomerpage = new InsightDeleteCustomerPage(_driver);
	     CheckResult(insightdeletecustomerpage.clickDelete(), "Click 'Delete' button");
	     
	     LogStep("Step - 8", "Return to the Search page again to see that the Customer account is redisplayed with a marked for deletion status");
	     InsightSearch insightsearch3 = new InsightSearch(_driver);
		 CheckValue(insightsearch3.getCurrentTitle(), "Insight - Search", "Checking the page title for insight search page is correct.");
		 CheckValue(insightsearch3.getCustomerStatusText(), userstatusDeleted, "Checking that the user has a 'Marked for deletion' status");
		 
		 LogStep("Step - 9 ", "Search for the CLI again and confirm that the 'Add new customer' link appears");
		 CheckResult(insightsearch3.enterSearchTerm(mobile), "Enter CLI");
		 CheckResult(insightsearch3.selectSearchType("Telephone Number"), "Select 'Telephone Number' as Search type");
	     CheckResult(insightsearch3.clickSubmit(), "Click 'Search' button");
	     delay(2000);
	     CheckBool(insightsearch3.isAddNewCustomerLinkVisible(), "Checking is the 'add new customer' link visible?");

	     LogStep("Step - 10", "Logout insight");
	     delay(2000);
	     InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
	     insightnavmenu.clickAccount();
	        
	     InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
	     insightlogoutpage.clickLogout();  
    }

}
