package TestScenario.Insight;

import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;

import com.relevantcodes.extentreports.LogStatus;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

public class InsightApplyPIBTest extends RingGoScenario {
    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
    	NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "dp_wcc_pib", dataProviderClass = DataProviders.DP_WCC_PIB.class, groups = {"apply_pib"})
    public void authorisePermitApplication(String Username, String Password,
                                           String Operator_Status, String Operator,
                                           String Phone_number, String Title, String FirstName, String Surname, String Email,
                                           String Contact_number, String PIB_used_for, String Job_title, String Employee_no,
                                           String Job_Department, String PIB_Purpose, String PIB_service, String PIB_Average_Parked_time,
                                           String PIB_Parking_zone, String PIB_VRN, String VRN_Make, String VRN_Type, String Driver_name, String Payment_for,
                                           String Authroised_signatory, String Department, String Status, String Payment_type, String Card_number,
                                           String Exp_month, String Exp_year, String CV2) throws IOException {

        StartTest("368", "Apply PIB via Insight");
        try {
            LoginHelper.loginInsight(Username, Password, _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(Operator_Status, Operator);

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            CheckResult(insightwcchome.clickPIB(), "Click PIB button");

            InsightPIBApplicationPage insightpibapplicationpage = new InsightPIBApplicationPage(_driver);
            assertTrue("PIB permit type verify", insightpibapplicationpage.getPermitType().contains("Parking Identifier Board"));
            insightpibapplicationpage.applyPIB(Phone_number, Title, FirstName, Surname, Contact_number, PIB_used_for,
                    Job_title, Employee_no, Job_Department, PIB_Purpose, PIB_service, PIB_Average_Parked_time,
                    PIB_Parking_zone, PIB_VRN, VRN_Make, VRN_Type, Driver_name, Payment_for,
                    Authroised_signatory, Department, Status);


            InsightPaymentPage insightpaymentpage = new InsightPaymentPage(_driver);
            assertTrue("Checking Charge for Permit Changes? title", insightpaymentpage.getInsightPaymentPageTitle().contains("Charge for Permit Changes?"));
            insightpaymentpage.payForPermitCharges(Payment_type, Card_number, Exp_month, Exp_year, CV2);

            delay(5000);
            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
