package TestScenario.Insight;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.InsightTestDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Insight.EditAVehicleLookupPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightWCCHome;
import PageObjects.Insight.SupportPage;
import PageObjects.Insight.VRNLookupTablePage;

public class Edit_vehicle_details_using_VRN_Lookup extends RingGoScenario {
	
	String vehicleVRN;
	
	@BeforeMethod
    public void setUp() throws Exception {
		
        createUser("defaultNewUser.json");
        
        vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
    }
	
	 @Test(dataProvider = "insightTestData", dataProviderClass = InsightTestDP.class)
	 public void edit_vrn_insight_vrn_lookup(String TCID, Map<String, String> data) throws IOException {
		 
		    StartTest(TCID, "Edit vehicle details using VRN lookup");
		    
		    LogStep("Step - 1", "Log into Insight");
		    NavigationHelper.openInsight(_driver);

	        LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);
	        
	        LogStep("Step - 2", "Go to 'Support' > 'VRN lookup'");
	        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
	        CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected");

	        SupportPage supportPage = new SupportPage(_driver);
	        CheckResult(supportPage.getLeftSideBar().clickVrnLookup(), "Click on lookup link ");

	        LogStep("Step - 3", "Search for a VRN");
	        VRNLookupTablePage vrnLookupTablePage = new VRNLookupTablePage(_driver);
	        CheckResult(vrnLookupTablePage.enterVrn(vehicleVRN), "Enter VRN number");
	        CheckResult(vrnLookupTablePage.clickSubmit(), "Click on submit button");
	        
	        LogStep("Step - 4", "Click 'edit this vehicle' at the bottom of the page");
	        CheckResult(vrnLookupTablePage.clickEditThisVehicle(), "Click on 'Edit this vehicle'");
            
	        LogStep("Step - 5", "Edit the vehicle's information with valid inputs Click 'Save'");
	        EditAVehicleLookupPage editAVehicleLookupPage = new EditAVehicleLookupPage(_driver);
	        CheckResult(editAVehicleLookupPage.enterYearOfManufacture(data.get("yearOfManufacture")), "Enter Year Of Manufacture");
	        CheckResult(editAVehicleLookupPage.enterCO2(data.get("CO2")), "Enter car's CO2");
	        CheckResult(editAVehicleLookupPage.enterEngineSize(data.get("engineSize")), "Enter car's engine size");
	        CheckResult(editAVehicleLookupPage.selectVehicleType(data.get("vehicleType")), "Select car's vehicle type");
	        CheckResult(editAVehicleLookupPage.selectFuelFuelType(data.get("fuelType")), "Select Fuel Type");
	        CheckResult(editAVehicleLookupPage.clickSave(), "Click on Save button");
	        
	        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
	 
	 }

}
