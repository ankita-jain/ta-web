package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.AutoPayContracts;
import PageObjects.myringo.AutoPayRegisterContract;
import PageObjects.myringo.ConfirmDeletionAutoContract;
import PageObjects.myringo.HomePageMyRinggo;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RingGoDeletingAutoPayContract extends RingGoScenario {

    private String defaultCarVRN;
    private AutoPayRegisterContract.Operator operator = AutoPayRegisterContract.Operator.CHILTERN_RAILWAYS;

    @BeforeMethod
    public void createUserAndCars() throws Exception {
        createUser("islingtonNewUser.json");
        defaultCarVRN = ((String) ((HashMap) ((ArrayList) newUserJson.get("Vehicles")).get(0)).get("VRM")).toUpperCase();
    }


    @Test
    public void deleteAnAutoPayContract() {
        StartTest("8023", "Deleting an autopay contract ");

        LogStep("Step - 1", "Log in to MyRingGo");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(email, password, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'Auto Pay'");
        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        LogStep("Step - 3", "Click 'Add Contract'");
        AutoPayContracts autoPayContracts = new AutoPayContracts(_driver);
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        LogStep("Step - 4", "Select an operator. Choose vehicle");
        AutoPayRegisterContract autoPayRegisterContract = new AutoPayRegisterContract(_driver);
        CheckResult(autoPayRegisterContract.chooseOperator(operator.toString()), String.format("Select Operator -> %s", operator.toString()));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));

        LogStep("Step - 5", "Tick that you accept the terms and conditions. Click 'register'");
        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        LogStep("Step - 6", "In the delete section click the red cross next to any contract");
        CheckResult(autoPayContracts.deleteContract(1), "Click delete contract");

        LogStep("Step - 7", "Click 'yes' on the confirm deletion page");
        ConfirmDeletionAutoContract confirmDeletionAutoContract = new ConfirmDeletionAutoContract(_driver);
        CheckResult(confirmDeletionAutoContract.clickYes(), "Click Yes");
        CheckBool(autoPayContracts.getContractsCount() == 0, "Check contract deleting");
    }

    @Test
    public void cancelDeletingAnAutoPayContract() {
        StartTest("20502", "Canceling the deleting an autopay contract ");

        LogStep("Step - 1", "Log in to MyRingGo");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(email, password, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'Auto Pay'");
        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        LogStep("Step - 3", "Click 'Add Contract'");
        AutoPayContracts autoPayContracts = new AutoPayContracts(_driver);
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        LogStep("Step - 4", "Select an operator. Choose vehicle");
        AutoPayRegisterContract autoPayRegisterContract = new AutoPayRegisterContract(_driver);
        CheckResult(autoPayRegisterContract.chooseOperator(operator.toString()), String.format("Select Operator -> %s", operator.toString()));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));

        LogStep("Step - 5", "Tick that you accept the terms and conditions. Click 'register'");
        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        LogStep("Step - 6", "In the delete section click the red cross next to any contract");
        CheckResult(autoPayContracts.deleteContract(1), "Click delete contract");

        LogStep("Step - 7", "Click 'no' on the confirm deletion page");
        ConfirmDeletionAutoContract confirmDeletionAutoContract = new ConfirmDeletionAutoContract(_driver);
        CheckResult(confirmDeletionAutoContract.clickNo(), "Click No");
        CheckBool(autoPayContracts.getContractsCount() == 1, "Check contract deleting");
    }
}
