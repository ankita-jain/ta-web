package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.testng.AssertJUnit.assertTrue;

public class WCCResidentEcoPermitAwaitingPaymentTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCResidentEcoPermitAwaitingPaymentTest.class);

    @Test(dataProvider = "dp_wcc_eco_resident_permit_awaiting_payment", dataProviderClass = DataProviders.DP_WCC.class, groups = {"eco_permit_application_awp"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone,
                                          String Residency_type, String Vehicle, String Ownership, String address_proof_type_one,
                                          String address_proof_one, String address_proof_type_two, String address_proof_two,
                                          String Car_insurance_schedule_proof_type, String Car_insurance_schedule_proof,
                                          String Certificate_of_insurance, String Certificate_of_insurance_proof,
                                          String Vehicle_registration_document, String Vehicle_registration_document_proof) throws IOException {
        StartTest("356", "WCC- Eco permit AWAITING PAYMENT status");
        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            wccindexpage.clickPersonalFromLogin();

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            wccloginpage.inputEmailOrPhonenumer(Username);
            wccloginpage.enterPassword(Password);
            wccloginpage.clickLoginButton();

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickEcoPermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident Low Emission permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Postcode, Address, Parking_zone);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            wccresidentpermitdetailspage.fillPermitApplicationDetails(Residency_type, Vehicle, Ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Eco permit proof pagetitle", wccresidentpermitproofpage.getProofPageTitle().contains("Resident Low Emission permit application"));
            wccresidentpermitproofpage.completeEcoPermitApplicationProofs(address_proof_type_one, address_proof_one, address_proof_type_two, address_proof_two, Car_insurance_schedule_proof_type, Car_insurance_schedule_proof, Certificate_of_insurance, Certificate_of_insurance_proof, Vehicle_registration_document, Vehicle_registration_document_proof);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            wccpermitdetailsconfirmationpage.clickPayLater();


            //user logout
            wcchome.logout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
