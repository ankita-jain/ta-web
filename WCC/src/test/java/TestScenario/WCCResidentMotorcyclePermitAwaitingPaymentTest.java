package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import TestScenario.Corporate.WCCCorporateTopupNegTest;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.testng.AssertJUnit.assertTrue;

public class WCCResidentMotorcyclePermitAwaitingPaymentTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);

    @Test(dataProvider = "dp_wcc_resident_motorcycle_permit_awaiting_payment", dataProviderClass = DataProviders.DP_WCC.class, groups = {"motor_cycle_permit_awp"})
    public void residentPermitApplication(String Username, String Password, String Postcode, String Address, String Parking_zone, String Vehicle, String Ownership,
                                          String Address_proof_type1, String Address_proof1,
                                          String Address_proof_type2, String Address_proof2,
                                          String Proof_of_vehicle_type_1, String Proof_of_vehicle_1,
                                          String Proof_of_vehicle_type_2, String Proof_of_vehicle_2,
                                          String Proof_of_vehicle_type_3, String Proof_of_vehicle_3
    ) throws IOException {
        StartTest("", "WCC- Eco permit awaiting payment details check insight");
        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            wccindexpage.clickPersonalFromLogin();

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            wccloginpage.inputEmailOrPhonenumer(Username);
            wccloginpage.enterPassword(Password);
            wccloginpage.clickLoginButton();

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            wcchome.clickPermit();

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickMotorCyclePermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident Motorcycle permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Postcode, Address, Parking_zone);

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            assertTrue("Resident Motorcycle permit page title", wccresidentpermitdetailspage.getResidentPermitDetailsPageTitle().contains("Resident Motorcycle permit application"));
            wccresidentpermitdetailspage.fillPermitApplicationDetailsMotorcylePermit(Vehicle, Ownership);

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            assertTrue("Resident Motorcycle permit proof page title", wccresidentpermitproofpage.getProofPageTitle().contains("Resident Motorcycle permit application"));
            wccresidentpermitproofpage.completeMotorcyclePermitApplicationProofs(Address_proof_type1, Address_proof1, Address_proof_type2, Address_proof2, Proof_of_vehicle_type_1, Proof_of_vehicle_1, Proof_of_vehicle_type_2, Proof_of_vehicle_2, Proof_of_vehicle_type_3, Proof_of_vehicle_3);

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            wccpermitdetailsconfirmationpage.clickPayLater();

            //user logout
            wcchome.logout();
        } catch (AssertionError e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
