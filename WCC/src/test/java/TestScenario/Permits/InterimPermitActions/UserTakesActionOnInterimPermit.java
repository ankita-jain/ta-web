package TestScenario.Permits.InterimPermitActions;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.Permit.ManageProofsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;


public class UserTakesActionOnInterimPermit extends RingGoScenario {
	
	/**
	 * Test for JIRA EUMISSUK-7301
	 * 
	 * Given i have a permit in an interim state
	 * When I view those permits in the 'my permits' section of the permits page on the Westminster Portal
	 * Then the permit will be displayed with an associated info box prompting me to upload my proofs
	 * 
	 * Given i have a permit in an interim state
	 * When i click on 'Upload proofs' within the info box
	 * Then i will be navigated to the specific permit where i can upload proofs.
	 */
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void viewInterimPermitOnMyPermitPage(String TCID, Map<String, String> data)
	{
		StartTest(TCID, "Interim permit displays upload button");
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();

		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath").toString());
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		PermitsHelper.createInterimPermit(data.get("sendUploadPathInsuranceDocs"), _driver);
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		CheckValue(wccmypermitpage.uploadButtonIsdisplayed(), wccmypermitpage.uploadButtonIsdisplayed(), "Verifying if the upload button is displayed");
		CheckValue(wccmypermitpage.interimMessageIsdisplayed(), wccmypermitpage.interimMessageIsdisplayed(), "Verifying the interim message is displayed");
		wccmypermitpage.clickPermitBreadCrumb();
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		CheckValue(wccpermitpage.getPermitPageTitle(), "Permits",
                "Checking the page title for permit page is correct");
		CheckValue(wccpermitpage.uploadButtonIsdisplayed(), wccpermitpage.uploadButtonIsdisplayed(), "Verifying if the interim message is displayed");
		CheckValue(wccpermitpage.interimMessageIsdisplayed(), wccpermitpage.interimMessageIsdisplayed(), "Verifying the interim message is displayed");
		wccpermitpage.clickUploadButton();
		ManageProofsPage manageproofspage = new ManageProofsPage(_driver);
		CheckValue(manageproofspage.getManageProofsPageTitle(), "Manage proofs", "Verifying the page title for manage proofs page is correct.");
		}
}