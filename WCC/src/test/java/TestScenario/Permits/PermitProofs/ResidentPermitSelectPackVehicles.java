package TestScenario.Permits.PermitProofs;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class ResidentPermitSelectPackVehicles extends RingGoScenario {
	
	/* Test to Select First Vehicle pack for a user*/
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void residentPermitSelectFirstPackVehicle(String TCID, Map<Object,Object> Data)
	{
    StartTest("15166", "WCC- Resident Permit");
    
     PermitsHelper.setUpPermitToAddOnVehicle(getSuiteStartDate(), _driver);

     WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
 		CheckContains(wccresidentpermitproofofvehiclespage.getquestions_pack(), "Please choose one of the following", "Question for choosing Packs");
	 	wccresidentpermitproofofvehiclespage.selectFirstProofPack();
	 	wccresidentpermitproofofvehiclespage.navigateNextPage();
	 	
	 	WCCResidentPermitProofOfVehiclesUploadPage vehicleuploadproofpagefirstpack = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
	 	vehicleuploadproofpagefirstpack.uploadProofForFirstPack(Data.get("Proofs_of_Ownership").toString());
	 	
	 	
	}

	
	/* Test to Select Second Vehicle pack for a user*/
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void residentPermitSelectSecondPackVehicle(String TCID, Map<Object,Object> Data)
	{
    StartTest("15166", "WCC- Resident Permit");
    
     PermitsHelper.setUpPermitToAddOnVehicle(getSuiteStartDate(), _driver);

     
     WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
 		CheckContains(wccresidentpermitproofofvehiclespage.getquestions_pack(), "Please choose one of the following", "Question for choosing Packs");
	 	wccresidentpermitproofofvehiclespage.selectFirstProofPack();
	 	wccresidentpermitproofofvehiclespage.navigateNextPage();
	 	
	 	WCCResidentPermitProofOfVehiclesUploadPage vehicleuploadproofpagefirstpack = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
	 	vehicleuploadproofpagefirstpack.uploadProofForSecondPack(Data.get("V5C_File").toString(), Data.get("Certificate_Insurance").toString());
	 		 	
	}
}
