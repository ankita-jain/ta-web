package TestScenario.Permits.PermitProofs;

import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;

public class ArmedForcesResidentPermitProofs extends RingGoScenario {
	
	
	/**
	 * Test for JIRA EUMISSUK-7245
	 * Scenario: Given i am on the proof page
	 * and have selected a proof pack option or super pack
	 * when i click next step
	 * then the user is navigated to a different page
	 * 
	 * Scenario: Given i am on the proof page
	 * and a pack option has not been selected
	 * when i click next 
	 * then the system should present an error to the user to state they need to select a valid option
	 */
	@Test
	@Ignore
	public void ArmedForcesViewsErrorOnPermitPRoofAndFixesError() {
		StartTest("xxx", "Armed forces views error message on permit proof");
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(2, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.navigateNextPage();
		wccresidentpermitproofofaddresspage.getErrorMessage();
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		
		WCCResidentialPermitProofOfAddressUploadPage wccresidentalpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		wccresidentalpermitproofofaddressuploadpage.uploadButtonVisible();
	}
}