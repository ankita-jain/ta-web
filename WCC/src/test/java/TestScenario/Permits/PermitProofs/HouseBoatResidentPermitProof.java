package TestScenario.Permits.PermitProofs;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;

public class HouseBoatResidentPermitProof extends RingGoScenario {
	
	
	/**
	 * Test for JIRA EUMISSUK-7245
	 * Scenario:Given i am on the proof page
	 * when i click the back button
	 * then the user should be taken back to the previous page in the flow.
	 * 
	 * Scenario:Given i have no super proof packs setup
	 * when i navigate to that page
	 * Then the accepted proof text should be displayed and the other proofs packs should be displayed
	 */
	@Test
	@Ignore
	public void HouseBoatREsidentViewsProofPack() {
		StartTest("C51796", "WCC Personal user applying for a standard residency type permit (one super pack configured, clicking yes)");
	     
		 PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		 PermitsHelper.selectOwnershipTypeAndTsAndCs(1, 0, _driver);
		 WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		 assertTrue(wccresidentpermitproofofaddresspage.getPackQuestion());
		 wccresidentpermitproofofaddresspage.navigateBack();
		 
		 WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		 wccresidentpermitdetailspage.firstNameFieldDisplayed();
		// assertTrue(wccresidentpermitdetailspage.checkPageTitleContains("Resident permit application"));
	}
}