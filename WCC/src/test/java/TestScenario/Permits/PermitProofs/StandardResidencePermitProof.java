package TestScenario.Permits.PermitProofs;

import static Utilities.TimerUtils.delay;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;

public class StandardResidencePermitProof extends RingGoScenario {

	/**
	 * Test for JIRA EUMISSUK-7245 
	 * Scenario: Given i am on the proofs page
	 * When i select Yes for a super pack
	 * then no other proof packs are displayed.
	 */
	@Test
	public void PersonalUserSelectYesToSingleSuperPack(){
		 StartTest("51796", "WCC Personal user applying for a standard residency type permit (one super pack configured, clicking yes)");
	     
		 PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		 PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		 
		 WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		 wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle();
		 wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		 delay(3000);
		 CheckValue(null, wccresidentpermitproofofaddresspage.residential_proof_pack_question_one, "Checking if the residential proof pack questions are displayed.");
	}
	/**
	 * Scenario: i am on the proof page
	 * and only one super pack exists
	 * When i select NO for a super pack e.g. council tax
	 * Then the accepted proof text should be displayed and the other proof packs should be displayed
	 */
	@Test
	public void PersonalUserSelectNoToSingleSuperPack() {
		StartTest("51796", "WCC Personal user applying for a standard residency type permit (one super pack configured, clicking no)");
	     
		 PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		 PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		 WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		 wccresidentpermitproofofaddresspage.selectNoToSuperPack();
		 CheckBool(wccresidentpermitproofofaddresspage.isTheResidentialProofPackQuestionDisplayed(), "Checking if the residential proof pack questions are dispplayed.");
	}
}