package TestScenario.Permits.PermitProofs;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCResidentPermitInfoPage;
import PageObjects.WCCResidentPermitPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class StandardResidentPermitProofPageFlow extends RingGoScenario {
	
	private String Title = "Proof of vehicle ownership - AU20MAT";

	/**
	 * Test for JIRA EUMISSUK-7271 
	 * Given "use council tax lookup to validate address" is set in the config
	 * and i have entered an address that will PASS the council tax lookup
	 * and i am on the "about you" page for a permit application
	 * and i selected a residence status of standard resident
	 * and all mandatory fields are completed
	 * When i click "next"
	 * Then i will be shown the proof of Vehicle category and the council tax lookup has passed
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void PersonalUserWithValidCouncilTaxLookUp(String TCID, Map<String, String> data){
		
		StartTest(TCID, "Interim permit displays upload button");
		NavigationHelper.openWestMinster(_driver);
		LoginHelper.loginWestminster(data.get("CouncilUsername"), data.get("CouncilPassword"), _driver);
		WCCHome wcchomepage = new WCCHome(_driver);
	    CheckResult(wcchomepage.clickPermit(), "Click permit tile");
	                           
	    WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
	   				
	   	wccpermitpage.clickResidentPermit();
	   	WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
	   		
	   	CheckResult(wccresidentpermitinfopage.clickStandardResidentPermit(), "Click standard resident permit");
	   	WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
	   	wccresidentpermitpage.applyResidentPermitApplication(data.get("POSTCODE"), data.get("ADDRESS"), data.get("ZONE"));
	    WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
	    wccresidentpermitdetailspage.selectUserTitleResidentPermit(data.get("USER_TITLE"));
        wccresidentpermitdetailspage.fillPermitApplicationDetails(data.get("RESIDENT_TYPE"), data.get("VEHICLE"), data.get("OWNERSHIP"));
        //wccresidentpermitdetailspage.clickNextButton();
        WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
        CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), Title, "Verifying the title of the vehicle page is correct.");
        }
}
