package TestScenario.Permits.PermitProofs;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitCancelPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class WCCUserCancelsPermitApplication extends RingGoScenario{
	
	/**
	 * Test to cancel a permit application
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void CancelPermitWithinWCC(String TCID, Map<String, String> data)
	{
		StartTest(TCID, "WCC user is able to cancel a permit via the portal");
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String titlePermitProofPage="Proof of vehicle ownership - "+VRN;
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		System.out.println("checking what is the value="+wccresidentialpermitproofofvehiclepage.getTitleVehicle());
		System.out.println("vehicle title="+titlePermitProofPage);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
                "Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		String permitId = wccmypermitpage.getPermitId("1", "2");
		wccmypermitpage.clickCancelPermit(permitId);
		WCCPermitCancelPage wccpermitcancelpage = new WCCPermitCancelPage(_driver);
		CheckValue(wccpermitcancelpage.getCancelPagetitle(), "Cancellation request", "Checking the page title for cancellation request is correct.");
        wccpermitcancelpage.cancelPermitProcess("permit did not meet our threshold.");
        CheckValue(wccmypermitpage.getPermitStatus("2"), "Cancelled", "Checking the permit is in a cancelled status.");
	}
}