package TestScenario.Permits.PermitProofs;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;

public class DiplomatesResidencePermitProofs extends RingGoScenario{
	
	/**
	 * Test for JIRA EUMISSUK-7245 
	 * Scenario: Given i am on the proof page
	 * and multiple super packs are configured
	 * and there is at least one super pack to be displayed
	 * when i select NO for a super pack
	 * then the next super pack option should be displayed and NO other proof packs are displayed
	 * 
	 * This test is not applicable for this current go live, this will be applicable for future clients website (not WCC).
	 */
	@Test
	@Ignore
	public void PersonalUserSelectNoToMultipleSuperPack() {
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(3, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectNoToSuperPack();
		wccresidentpermitproofofaddresspage.getSecondSuperPack();
	}
	
	/**
	 * Test for JIRA EUMISSUK-7245 
	 * Scenario: Given i am on the proof page
	 * and multiple super packs are configured
	 * and there are no more super packs to be displayed
	 * when i select NO for a super pack
	 * Then the accepted proof text should be displayed and the other proofs packs should be displayed
	 * 
	 * 
	 * This test is not applicable for this current go live, this will be applicable for future clients website (not WCC).
	 */
	@Test
	@Ignore
	public void PersonalUserSelectNoToTheLastMultipleSuperPack() {
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(3, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectNoToSuperPack();
		wccresidentpermitproofofaddresspage.selectNoToSecondSuperPack();
		assertTrue(wccresidentpermitproofofaddresspage.getPackQuestion());
	}

}