package TestScenario.Permits.PermitProofs;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class UploadProofValidation extends RingGoScenario {
	
	/**
	 * Test for JIRA 7292
	 * @param TCID
	 * @param data
	 */
	@Test (dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void VerifyUploadDocTypeAndSize(String TCID, Map<String, String> data) {
        StartTest(TCID, "WCC Personal user uploading proof docs validation.");
	     
        PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		 
		 WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		 wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle();
		 wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		 wccresidentpermitproofofaddresspage.navigateNextPage();
		 
		 WCCResidentialPermitProofOfAddressUploadPage wccresidentpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		 wccresidentpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("council_tax_doc"));
		 wccresidentpermitproofofaddressuploadpage.clickNextButton();
		 
		 WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		 wccresidentialpermitproofofvehiclespage.selectThirdProofPack();
		 wccresidentialpermitproofofvehiclespage.navigateNextPage();
		 
		 WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		 wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("proof_one"));
		 wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadSecondProof(data.get("proof_two"));
		 wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadThirdProof(data.get("proof_three"));
		 CheckBool(wccresidentpermitproofofvehiclesuploadpage.wrongFileError(), "Error is not displayed");
		 CheckBool(wccresidentpermitproofofvehiclesuploadpage.wrongFileSizeError(), "Error is not displayed");
	}
}