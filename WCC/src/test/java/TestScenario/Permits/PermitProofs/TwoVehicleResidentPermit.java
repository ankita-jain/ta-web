package TestScenario.Permits.PermitProofs;

import static org.testng.AssertJUnit.assertTrue;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
	
	public class TwoVehicleResidentPermit extends RingGoScenario {
		
		/* Selecting First Pack	
		 * 1. Given i have selected more than one vehicle on the about you page
			When i navigate to the vehicle proof category 
			Then i should only be shown the proof packs for the first vehicle i selected 
			
			2.Given i have selected more than one vehicle on the about you page
			and uploaded the proofs for a vehicle
			When i click next step
			Then the proof packs for the next vehicle(s) should be displayed 
			And the next vehicle displayed is based on the order entered on the 'About You' 'Vehicles' section */
		
		@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
		public void residentPermitSelectFirstPackForTwoVehicle(String TCID, Map<Object,Object> Data)
		{
	    StartTest("15166", "WCC- Resident Permit");
	    
	    PermitsHelper.setUpPermitWithSecondVehicle(Data.get("VRM2").toString(), _driver);

	     WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
	 		CheckContains(wccresidentpermitproofofvehiclespage.getquestions_pack(), "Please choose one of the following", "Question for choosing Packs");
	 		assertTrue("Vehicle 1 of a user", wccresidentpermitproofofvehiclespage.getTitleVehicle().contains(Data.get("VRM1").toString()));
		 	wccresidentpermitproofofvehiclespage.selectFirstProofPack();
		 	wccresidentpermitproofofvehiclespage.navigateNextPage();
		 	
		 	WCCResidentPermitProofOfVehiclesUploadPage vehicleuploadproofpagefirstpack = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		 	vehicleuploadproofpagefirstpack.uploadProofForFirstPack(Data.get("Proofs_of_Ownership").toString());
		 	
		 	
		 	WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofsecondvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		 	CheckContains(wccresidentpermitproofofsecondvehiclespage.getquestions_pack(), "Please choose one of the following", "Question for choosing Packs");
		 	assertTrue("Vehicle 2 of a user", wccresidentpermitproofofvehiclespage.getTitleVehicle().contains(Data.get("VRM2").toString()));
		 	wccresidentpermitproofofsecondvehiclespage.selectFirstProofPack();
		 	wccresidentpermitproofofsecondvehiclespage.navigateNextPage();
			 	
			WCCResidentPermitProofOfVehiclesUploadPage secondvehicleuploadproofpagefirstpack = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
			secondvehicleuploadproofpagefirstpack.uploadProofForFirstPack(Data.get("Proofs_of_Ownership").toString());
			
					 	
		 	
		}

		
		/* Selecting Second Pack	
		 * 1. Given i have selected more than one vehicle on the about you page
			When i navigate to the vehicle proof category 
			Then i should only be shown the proof packs for the first vehicle i selected 
			
			2.Given i have selected more than one vehicle on the about you page
			and uploaded the proofs for a vehicle
			When i click next step
			Then the proof packs for the next vehicle(s) should be displayed 
			And the next vehicle displayed is based on the order entered on the 'About You' 'Vehicles' section */
		
		@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
		public void residentPermitSelectDifferentPackForVehicle(String TCID, Map<Object,Object> Data)
		{
	    StartTest("15166", "WCC- Resident Permit");
	    
	    PermitsHelper.setUpPermitWithSecondVehicle(Data.get("VRM2").toString(), _driver);

	     WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
	 		CheckContains(wccresidentpermitproofofvehiclespage.getquestions_pack(), "Please choose one of the following", "Question for choosing Packs");
	 		assertTrue("Vehicle 1 of a user", wccresidentpermitproofofvehiclespage.getTitleVehicle().contains(Data.get("VRM1").toString()));
		 	wccresidentpermitproofofvehiclespage.selectFirstProofPack();
		 	wccresidentpermitproofofvehiclespage.navigateNextPage();
		 	
		 	WCCResidentPermitProofOfVehiclesUploadPage vehicleuploadproofpagefirstpack = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		 	vehicleuploadproofpagefirstpack.uploadProofForFirstPack(Data.get("Proofs_of_Ownership").toString());
		 	
		 	
		 	WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofsecondvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		 	CheckContains(wccresidentpermitproofofsecondvehiclespage.getquestions_pack(), "Please choose one of the following", "Question for choosing Packs");
		 	assertTrue("Vehicle 2 of a user", wccresidentpermitproofofvehiclespage.getTitleVehicle().contains(Data.get("VRM2").toString()));
		 	wccresidentpermitproofofsecondvehiclespage.selectSecondProofPack();
		 	wccresidentpermitproofofsecondvehiclespage.navigateNextPage();
			 	
			WCCResidentPermitProofOfVehiclesUploadPage secondvehicleuploadproofpagesecondpack = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
			secondvehicleuploadproofpagesecondpack.uploadProofForSecondPack(Data.get("V5C_File").toString(), Data.get("Certificate_Insurance").toString());
			
		}
		
}
