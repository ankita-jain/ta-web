package TestScenario.Permits.ManageProofs;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.ManageProofsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;

public class WCCUserUploadsRemainingProofs extends RingGoScenario {
	
	/**
	 * Test for JIRA-7604
	 */
	@Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = DataProviders.PermitDP.class)
	public void uploadMissingProofs(String TCID, Map<String, String> data) {
		
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
        PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectNoToSecondSuperPack();
		wccresidentpermitproofofaddresspage.selectAPartialProofPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getTwoDocPageHeader(), "Two documents",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToProofOne(2, data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		PermitsHelper.createInterimPermit(data.get("sendUploadPathInsuranceDocs"), _driver);
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone A, Westminster", "Checking the view permit page title is correct.");
		wccviewpermitdetailspage.clickUploadProofButton();
		ManageProofsPage managerproofspage = new ManageProofsPage(_driver);
		CheckValue(managerproofspage.getManageProofsPageTitle(), "Manage proof", "Checking the page title for manage proofs page is correct.");
		managerproofspage.uploadProof(data.get("prooftobeuploaded"));
		String status = wccviewpermitdetailspage.getProofStatus("4");
		CheckValue(status, "awating proof", "verifying the proof NOT uplaoded is in the correct status.");
		wccviewpermitdetailspage.clickUploadProofButton();
		CheckValue(managerproofspage.getManageProofsPageTitle(), "Manage proof", "Checking the page title for manage proofs page is correct.");
		managerproofspage.uploadProof(data.get("prooftobeuploaded"));
		managerproofspage.saveChanges();
		String uploadedProofstatus = wccviewpermitdetailspage.getProofStatus("4");
		CheckValue(uploadedProofstatus, "Submitted, awaiting approval", "verifying the newly proof uplaoded is in the correct status.");
	}
}