package TestScenario.Permits.ManageProofs;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Permit.ManageProofsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import SeleniumHelpers.WaitMethods;
import Utilities.PropertyUtils;

public class WCCUserUploadsRejectedProofs extends RingGoScenario {
	
	private String proofonepath = "1.jpg";
	private String CV2 = "111";
	private String insightLogin;
	private String insightPassword;
	private String operator = "42152";
	private String operatorstatus = "1";
	private String status = "Show All";
	
	    /**
	     * Test for JIRA-7604
	     * @throws IOException 
	     */
		@BeforeMethod
			public void setUpPermitAndRejectAllProofs() throws IOException {
			String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
			PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);

			WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
			CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
			        "Checking the page title for resident permit proof of address page is correct");
			wccresidentpermitproofofaddresspage.selectYesToSuperPack();
			wccresidentpermitproofofaddresspage.navigateNextPage();
			WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
			CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
			        "Checking the page title for resident permit proof of address upload page is correct");
			wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proofonepath);
			wccresidentialpermitproofofaddressuploadpage.clickNextButton();
			WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
			wccresidentialpermitproofofvehiclespage.selectFirstProofPack();
			wccresidentialpermitproofofvehiclespage.navigateNextPage();
			WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
			wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(proofonepath);
			WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
	        wccpermitdetailsconfirmationpage.confirmPermitDetails();
		 	WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
		 	wccpurchasepermitpaymenttype.clickTakePaymentNow();
		 	wccpurchasepermitpaymenttype.clickNextButton();
	        WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
	        wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();
	        
	        WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		 	wccpaymentcardcvvpage.enterCV2AndProceed(CV2);
		 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
	        wccpermitpurchasefinishpage.clickFinish();
	        //Open insight and reject the permit proofs
	        
	        NavigationHelper.openInsight(_driver);
	        insightLogin = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
	        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
	        LoginHelper.loginInsight(insightLogin, insightPassword, _driver);
	       	        
	        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
	        insightservicemenupage.loadPermitSearchForm();
	        InsightHelper.searchPermitAndEdit(VRN, status, _driver);
	        
	        InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
	        CheckValue(insightpermitapplicationpage.getPageTitle(), "Edit a Resident Permit Application",
	                "Checking the page title for edit permit insight page is correct");
	        insightpermitapplicationpage.selectReasonForPemitProof("2", "Rejected");
		}
		
		@Test
		public void reUploadRejectedsProofs() {
			
			StartTest("", "");
			NavigationHelper.openWestMinster(_driver);
			WCCHome wcchome = new WCCHome(_driver);
			
			CheckValue(wcchome.getHomePageTitle(), "Welcome",
	                "Checking the page title for home page page is correct");
			wcchome.clickPermit();
			WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
			wccpermitpage.selectSeeAllPermitsLink();
			WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
			
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
			
			wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
			WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
			CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone A, Westminster", "Checking the view permit page title is correct");
			wccviewpermitdetailspage.clickUploadProofButton();
			ManageProofsPage manageproofspage = new ManageProofsPage(_driver);
			CheckValue(manageproofspage.getManageProofsPageTitle(), "Manage proofs", "Checking the page title for manage proofs is correct.");
			CheckValue(manageproofspage.getUploadButton(), manageproofspage.firstUploadProof, "verifying the user is able to see the upload button when the proofs have been rejected.");
		}
}