package TestScenario.Permits.ManageProofs;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.ManageProofsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;

public class WCCUserViewsMultipleTablesOnManageProofsPage extends RingGoScenario {
	
	private String numberPlate;
	
	/**
	 * Test for JIRA-7604
	 * @param TCID
	 * @param data
	 */
	@Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = DataProviders.PermitDP.class)
	public void viewMultipleVehicleTables(String TCID, Map<String, String> data)
	{
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String firstPlateNumber = "Proof of vehicle ownership - " + VRN;
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		numberPlate =randomAlphabetic(6).toUpperCase();
		wccresidentpermitdetailspage.addRegistration(numberPlate);
        PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectNoToSuperPack();
		wccresidentpermitproofofaddresspage.selectAPartialProofPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getTwoDocPageHeader(), "Two documents",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToProofOne(2, data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		PermitsHelper.createInterimPermit(data.get("sendUploadPathInsuranceDocs"), _driver);
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone A, Westminster", "Checking the view permit page title is correct.");
		wccviewpermitdetailspage.clickUploadProofButton();
		ManageProofsPage managerproofspage = new ManageProofsPage(_driver);
		CheckValue(managerproofspage.getManageProofsPageTitle(), "Manage proof", "Checking the page title for manage proofs page is correct.");
	}
}
