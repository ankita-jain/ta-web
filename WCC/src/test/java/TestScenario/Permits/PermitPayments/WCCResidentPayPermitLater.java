package TestScenario.Permits.PermitPayments;

import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;

public class WCCResidentPayPermitLater extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void userDecidesToPayPermitLater(String TCID, Map<String, String> data)
	{
		StartTest(TCID, "User pays for permit via basket.");
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();

		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath").toString());
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		PermitsHelper.createInterimPermit(data.get("sendUploadPathInsuranceDocs").toString(), _driver);
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehicleuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
	 	wccresidentpermitproofofvehicleuploadpage.clickNextButton();
	 	WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
        wccpermitdetailsconfirmationpage.clickPayLater();
        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
        String permitId = wccmypermitpage.getPermitId("1", "2");
        CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits", "Verifying the page title on my permits page is correct.");
        CheckValue(wccmypermitpage.getPermitStatus("2"), "Awaiting payment", "Checking the permit is in an Awaiting payment status.");
	}
}