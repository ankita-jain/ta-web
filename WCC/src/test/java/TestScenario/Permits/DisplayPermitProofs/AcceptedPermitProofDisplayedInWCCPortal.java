package TestScenario.Permits.DisplayPermitProofs;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import Utilities.PropertyUtils;

public class AcceptedPermitProofDisplayedInWCCPortal extends RingGoScenario {
	
	/**
	 * Test for JIRA 7564
	 * @param TCID
	 * @param data
	 */
	
	private String insightUserName;
	private String insightPassword;
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void WCCUserViewsAcceptedStatusAgainstProof(String TCID, Map<String, String> data) throws IOException
	{
		StartTest(TCID, "Rejected status is populated against the correct proof");
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String titlePermitProofPage = "Proof of vehicle ownership - " + VRN;
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
				"Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		
		//Insight user accepts permit proofs.
		NavigationHelper.openInsight(_driver);
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        
		LoginHelper.loginInsight(insightUserName, insightPassword, _driver);
		        
        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
        insightservicemenupage.loadPermitSearchForm();
        InsightHelper.searchPermitAndEdit(VRN, data.get("Status"), _driver);
        
        InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
        CheckValue(insightpermitapplicationpage.getPageTitle(), "Edit a Resident Permit Application",
                "Checking the page title for edit permit insight page is correct");
        insightpermitapplicationpage.selectReasonForPemitProof("2", "Accepted");
        
        NavigationHelper.openWestMinster(_driver);
		
        WCCHome wcchome = new WCCHome(_driver);
		
		CheckValue(wcchome.getHomePageTitle(), "Welcome",
                "Checking the page title for home page page is correct");
		delay(3000);
		wcchome.clickPermit();
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		wccviewpermitdetailspage.getProofStatus("1");
		CheckValue(wccviewpermitdetailspage.getProofStatus("1"), "Accepted", "The Permit proof is not rejected");
	}

}
