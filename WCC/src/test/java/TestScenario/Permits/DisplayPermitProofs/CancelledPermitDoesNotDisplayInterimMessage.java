package TestScenario.Permits.DisplayPermitProofs;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNull;

import java.util.Map;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitCancelPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class CancelledPermitDoesNotDisplayInterimMessage extends RingGoScenario {
	/**
	 * Test for JIRA 7583
	 * @param TCID
	 * @param data
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void interimMessageIsNotDisplayedWhenViewingACancelledPermit(String TCID, Map<String, String> data) {
		
		StartTest(TCID, "Cancelled permit does not show a interim message on my permit page");
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		
		wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle();
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), "Proof of vehicle ownership - " + VRN,
                "Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);	
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.cancelPermit);
		WCCPermitCancelPage wccpermitcancelpage = new WCCPermitCancelPage(_driver);
		
		CheckValue(wccpermitcancelpage.getCancelPagetitle(), "Cancellation request",
                "Checking the page title for my permit page is correct");
		
		wccpermitcancelpage.cancelPermitProcess("This can be hard coded, it does not matter");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		
		CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone F, Westminster",
                "Checking the page title for view my permit page is correct");
		
		CheckValue(null, wccviewpermitdetailspage.interimMessageDisplayed(), "Interim message displayed");
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Cancelled", "The status of the permit should be cancelled");
	}
}
