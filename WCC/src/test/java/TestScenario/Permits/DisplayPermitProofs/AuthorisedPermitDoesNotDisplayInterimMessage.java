package TestScenario.Permits.DisplayPermitProofs;

import static org.testng.Assert.assertNull;
import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class AuthorisedPermitDoesNotDisplayInterimMessage extends RingGoScenario {
	
	/**
	 * Test for JIRA 7583
	 * @param TCID
	 * @param data
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void interimMessageNotDisplayedWhenViewingAuthorisedPermit(String TCID, Map<String, String> data) {
		
		StartTest(TCID, "Authorised permit does not display interim message");
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		String vehicleReg = wccresidentpermitdetailspage.getVehcileRegistration();
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), "Proof of vehicle ownership - " + vehicleReg,
                "Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		//Insight user logs in and accepts permit.
		PermitsHelper.authorisePermitInsight(data.get("username"), data.get("password"), data.get("operatorstatus"), data.get("operator"), data.get("VRM"), data.get("permitstatus"),
				data.get("changepermitstatus"), _driver);
		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		
		CheckValue(wcchome.getCurrentPageTitle(), "Welcome",
                "Checking the page title for home page page is correct");
		
		wcchome.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		
		CheckValue(wccmypermitpage.getCurrentPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		
		CheckValue(wccviewpermitdetailspage.getCurrentPageTitle(), "View permit: Resident permit - Zone A, Westminster",
                "Checking the page title for view my permit page is correct");
		
		CheckValue(null, wccviewpermitdetailspage.interimMessageDisplayed(), "Interim message displayed");
	}
}