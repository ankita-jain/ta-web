package TestScenario;

import java.util.Map;

import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.myringo.HomePageMyRinggo;

public class RingGoAlreadyDeletedUserLoggingin extends RingGoScenario {
	
	@Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void ringGoAlreadyDeleteUserandLoginBack(String TCID, Map<String, String> data) throws Exception {
		StartTest("001", "Test to Ensure already deleted user(Member Status as 6) cannot login back");
    	String Email;
        String Password;
    	Email=data.get("email");
		Password=data.get("password");
		
    	LogStep("Step - 1", "Open RingGoWeb");
    	NavigationHelper.openMyRingo(_driver);
    	
    	LogStep("Step - 2", "Enter Already deleted username and password");
    	LoginHelper.loginMyRingo(Email, Password, _driver);
    	
    	LogStep("Step - 3", "Ensure User is not logged in");
    	HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
    	CheckContains(homePageMyRinggo.getErrorMessageText(),"Invalid user account","error message");   
      	
}

}
