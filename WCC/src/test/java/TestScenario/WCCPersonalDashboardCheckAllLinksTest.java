package TestScenario;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

public class WCCPersonalDashboardCheckAllLinksTest extends RingGoScenario {

	private String email;
	private String password;
    static Logger LOG = Logger.getLogger(WCCPersonalDashboardCheckAllLinksTest.class);

    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void checkLinksInDashboard(String TCID, Map<Object, Object> Data) throws IOException {
        StartTest(TCID, "Personal - Dashboard Check Links");
  
        email = Data.get("email").toString();
        password = Data.get("password").toString();
        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(email), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Home page title", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickDashBoard(), "Click dashboard button");

            WCCDashboardPage wccdashboardpage = new WCCDashboardPage(_driver);
            assertTrue("Dashboard page title", wccdashboardpage.getDashboardPageTitle().contains("Dashboard"));
            CheckResult(wccdashboardpage.clickViewAllVehicle(), "View all vehicle");

            WCCMyVehiclesPage wccmyvehiclepage = new WCCMyVehiclesPage(_driver);
            assertTrue("Vehilce pagetitle checking", wccmyvehiclepage.getVehiclePageTiltle().contains("My Vehicles"));
            CheckResult(wcchome.clickGoBackToPreviousPage(), "Click  go back to previous page button");
           
            assertTrue("Dashboard page title", wccdashboardpage.getDashboardPageTitle().contains("Dashboard"));
            wccdashboardpage.clickEditDetailsLink();

            WCCContactDetailsPage wcccontactdetailspage = new WCCContactDetailsPage(_driver);
            CheckBool(wcccontactdetailspage.getContactDetailsPageTitle().equals("Contact Details"), "Contact Details page title check");
            CheckResult(wcchome.clickGoBackToPreviousPage(), "Click click go back to previous page button");

            assertTrue("Dashboard page title", wccdashboardpage.getDashboardPageTitle().contains("Dashboard"));
            CheckResult(wccdashboardpage.clickAllAddresses(), "All addresses");

            WCCMyAddressPage wccmyaddresspage = new WCCMyAddressPage(_driver);
            assertTrue("Checking My Address page title", wccmyaddresspage.getAddressPageTitle().contains("My Addresses"));
            CheckResult(wcchome.clickGoBackToPreviousPage(), "Click click go back to previous page button");

            assertTrue("Dashboard page title", wccdashboardpage.getDashboardPageTitle().contains("Dashboard"));
            CheckResult(wccdashboardpage.clickViewAllCards(), "View all results");

            WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
            assertTrue("Checking payment details", wccpaymentdetailspage.getPaymenPageTitle().contains("Payment Details"));
            CheckResult(wcchome.clickGoBackToPreviousPage(), "Click click go back to previous page button");

            assertTrue("Dashboard page title", wccdashboardpage.getDashboardPageTitle().contains("Dashboard"));
            CheckResult(wccdashboardpage.clickEditVehicle(), "Edit vehicle");

            assertTrue("Vehilce pagetitle checking", wccmyvehiclepage.getVehiclePageTiltle().contains("Edit a Vehicle"));
            CheckResult(wcchome.clickGoBackToPreviousPage(), "Click click go back to previous page button");

            assertTrue("Dashboard page title", wccdashboardpage.getDashboardPageTitle().contains("Dashboard"));
            CheckResult(wccdashboardpage.clickViewAllVehicle(), "View all vehicles");

            assertTrue("Vehilce pagetitle checking", wccmyvehiclepage.getVehiclePageTiltle().contains("My Vehicles"));
            CheckResult(wcchome.clickGoBackToPreviousPage(), "Click click go back to previous page button");
            
            delay(10000);
            wcchome.logoutMenu(); 
           
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
