package TestScenario;

import DataProviderHelpers.DBHelper;
import DataProviderHelpers.DatabaseProperties;
import GenericComponents.RingGoScenario;
import Utilities.PropertyUtils;
import org.testng.annotations.BeforeSuite;

import java.sql.Connection;
import java.sql.Statement;

public class WCCPermitsBeforeSuit {

    @BeforeSuite
    public void ClearPermits() throws Exception {
        Connection connect;
        connect = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);
        String testAccountsClIs = PropertyUtils.ReadProperty("TEST_ACCOUNTS_CLIs", RingGoScenario.WCC_PROPERTIES);
        String[] cLIs = testAccountsClIs.split(",");

        Statement stmt = null;
        String query = "USE " + DatabaseProperties.DbName;
        for (String cli : cLIs) {
            query = query + " EXECUTE clearPermits @memberCLI= '" + cli + "';\n";
        }

        stmt = connect.createStatement();
        stmt.executeUpdate(query);
        connect.close();
    }
}
