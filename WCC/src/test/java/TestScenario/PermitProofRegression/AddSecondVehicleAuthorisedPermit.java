package TestScenario.PermitProofRegression;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCAddSecondVehiclePage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;


public class AddSecondVehicleAuthorisedPermit extends RingGoScenario {
	
	private String proofonepath = "1.jpg";
	private String vehicleproof = "1.jpg";
	private String CV2 = "121";
	public String secondVrn;
	
	/**
	 * Method to create a permit, upload proofs and authorise the permit in insight.
	 */
	@BeforeMethod()
	public void createAndAuthorisePermit() throws Exception {
		
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String titlePermitProofPage = "Proof of vehicle ownership - "+VRN;
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proofonepath);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
                "Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(vehicleproof);
		PermitsHelper.completePaymentOfAPermit(CV2, _driver);
		
		/*Authorising the Permit in Insight */
	    PermitsHelper.AuthorisePermitApplication(VRN, _driver);
	}
	
	/**
	 * Test to verify that when a user add second vehicle, they need to upload proofs & the status is correct.
	 * @param TCID
	 * @param data
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void customerAddSecondVehicle(String TCID, Map<String, String> data) {
		
		StartTest(TCID, "Adding Second Vehicle to an Authorised Permit.");
		NavigationHelper.openWestMinster(_driver);
		
		WCCHome wcchomepage = new WCCHome(_driver);
		CheckResult( wcchomepage.clickPermit(), "Clicking WCC Home page");
        wcchomepage.clickPermit();
        
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		
        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits", "Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone F, Westminster", "Checking the page title for view my permit page is correct");
		wccviewpermitdetailspage.clickAddSecondVehicleButton();
		
		WCCAddSecondVehiclePage wccaddSecondVehiclePage = new WCCAddSecondVehiclePage(_driver);
		secondVrn =randomAlphabetic(6).toUpperCase();
		wccaddSecondVehiclePage.addSecondVehicleProcess(secondVrn, data.get("ownership"), data.get("prooftype"), data.get("documentone"));
					
		WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
		CheckResult(wccpaymentcardetailspage.selectSecondCardOnAccount(),"Select Second card");
		
		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	 	
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
	 	CheckResult(wccpermitpurchasefinishpage.clickFinish(),"Click Finish button");
      
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Vrn changed", "The status of the permit should be VRN Changed");
		
		CheckResult(wcchomepage.logout(), "Click logout button");
	}
}