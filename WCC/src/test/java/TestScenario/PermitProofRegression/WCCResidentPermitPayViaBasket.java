package TestScenario.PermitProofRegression;

import static org.testng.AssertJUnit.assertTrue;

import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import PageObjects.PaymentBasketFinishPage;
import PageObjects.WCCBasketConfirmationPage;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCResidentPermitViaBasketPage;
import PageObjects.POHelpers.PaymentBasketTable;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import Utilities.MapUtils;
import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;

public class WCCResidentPermitPayViaBasket extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void userPaysForPermitViaBasket(String TCID, Map<Object,Object> data) {
		
		StartTest(TCID, "User pays for permit via basket.");
		PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();

		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath").toString());
		CheckResult(wccresidentialpermitproofofaddressuploadpage.clickNextButton(), "Click the next button");
		PermitsHelper.createInterimPermit(data.get("sendUploadPathInsuranceDocs").toString(), _driver);
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehicleuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
	 	CheckResult(wccresidentpermitproofofvehicleuploadpage.clickNextButton(), "Click the next button");
	 	WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
	    CheckResult(wccpermitdetailsconfirmationpage.clickNextButton(), "Click the next button");
		
	    WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
        String purchasepermitpagetitle = wccpurchasepermitpaymenttype.getPageTitle();
        assertTrue(purchasepermitpagetitle.contains("Purchase a resident permit"));
        wccpurchasepermitpaymenttype.payViaBasket();
      
        WCCResidentPermitViaBasketPage wccresidentpermitviabasket = new WCCResidentPermitViaBasketPage(_driver);
        CheckContains(wccresidentpermitviabasket.getBasketPageTitle(), "Your Basket", "Basket Page Title");
        
        Map<Object,Object> Exp_Res = MapUtils.GetChild(data, "Exp_Results");
        Map<Object, Object> headings = MapUtils.GetChild(Exp_Res, "Heading");            
        CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(0).toString(), headings.get("Qty"), "Basket Headers : Qty");
        CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(1).toString(), headings.get("Item"), "Basket Headers : Item");
        CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(2).toString(), headings.get("Cost"), "Basket Headers : Cost");
        CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(3).toString(), headings.get("Total"), "Basket Headers : Total ");
        
        PaymentBasketTable[] tableContents = PaymentBasketTable.Parse((List<Map<Object,Object>>)Exp_Res.get("Content"));
        for(int i = 0; i < tableContents.length; i++) {
        	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Qty, tableContents[i].Qty, "Basket Content : Qty");
        	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Item, tableContents[i].Item, "Basket Content : Item");
        	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Cost, tableContents[i].Cost, "Basket Content : Cost");
        	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Total, tableContents[i].Total, "Basket Content : Total");
        }

        CheckResult(wccresidentpermitviabasket.purchaseViaBasket(), "Purchase Via Basket");
        
        
    
        WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
        String purchasecarddetailspagetitle = wccpaymentcarddetailspage.getPageTitle();
        CheckValue(purchasecarddetailspagetitle, "Pay your Basket", "Asserting Page Header");
        wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();

        WCCBasketConfirmationPage wccconfirmbasketpayment = new WCCBasketConfirmationPage(_driver);
        String wccconfirmbasketpaymenttitle = wccconfirmbasketpayment.getConfirmBasketPageTitle();
        CheckValue(wccconfirmbasketpaymenttitle, "Pay your Basket", "Confirm Basket Payment");
        CheckResult(wccconfirmbasketpayment.confirmPayViaBasket(), "Click confirm pay via basket");          
        
        
        WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
        String paymentcardcvvpagetitle = wccpaymentcardcvvpage.getPageHeader();
        assertTrue(paymentcardcvvpagetitle.contains("Confirm payment"));
        wccpaymentcardcvvpage.enterCV2AndProceedPermitBasketPayment(data.get("CV2").toString());

        PaymentBasketFinishPage wccpermitpurchasefinishpage = new PaymentBasketFinishPage(_driver);
        String wccpermitpurchasefinishpagetitle = wccpermitpurchasefinishpage.getPageTitle();
        assertTrue(wccpermitpurchasefinishpagetitle.contains("Pay your Basket"));
        CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click finish button");
        
        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
        String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("364", "dd/MM/yyyy", false), "Checking the valid to date");
	}

}
