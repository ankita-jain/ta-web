package TestScenario.PermitProofRegression;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import PageObjects.Permit.WCCTemporaryVehiclePage;

public class AddTemporaryVehicleChangePermit extends RingGoScenario {
	
	private String proofonepath = "1.jpg";
	private String vehicleproof = "1.jpg";
	private String CV2 = "121";
	private String secondVrn;
	
	/**
	 * Method to create a permit, upload proofs and authorise the permit in insight.
	 */
	@BeforeMethod()
	public void createAndAuthorisePermit() throws Exception {
		
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		
		String titlePermitProofPage="Proof of vehicle ownership - "+VRN;
		
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proofonepath);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
                "Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(vehicleproof);
		PermitsHelper.completePaymentOfAPermit(CV2, _driver);
		
		/*Authorising the Permit in Insight */
	    PermitsHelper.AuthorisePermitApplication(VRN, _driver);
		
	}
	
	/**
	 * Test to verify that when a user add second vehicle, they need to upload proofs & the status is correct.
	 * @param TCID
	 * @param data
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void customerTemporaryVehicle(String TCID, Map<String, String> data) {
		
		StartTest(TCID, "Temporary VRN Change for an Authorise Permit");
		
		NavigationHelper.openWestMinster(_driver);
		
		WCCHome wcchomepage = new WCCHome(_driver);
        CheckResult(wcchomepage.clickPermit(), "Click permit tile");
		
        WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		CheckResult(wccpermitpage.selectSeeAllPermitsLink(), "Click see all permits link");
        
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone F, Westminster", "Checking the page title for view my permit page is correct");
		CheckResult(wccviewpermitdetailspage.clickTemporaryVehicleChange(), "Click temporary vehicle link");
		
		WCCTemporaryVehiclePage wcctempVehiclePage = new WCCTemporaryVehiclePage(_driver);
		secondVrn =randomAlphabetic(6).toUpperCase();
		wcctempVehiclePage.TemporaryVehicleProcess(secondVrn, data.get("ownership"), data.get("prooftype"), data.get("documentone"));
					
		WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
		CheckResult(wccpaymentcardetailspage.selectSecondCardOnAccount(), "Select second card to pay");
		
		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	 	
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
        CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click the finish button");
        
    	CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Authorised", "The status of the permit should be VRN Changed");
	
		WCCMyPermitPage wccmypermitpageagain = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpageagain.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
		wccmypermitpageagain.jsClick(wccmypermitpageagain.viewDetails);
		
		WCCViewPermitDetailsPage wccviewpermitdetailsrevertpage = new WCCViewPermitDetailsPage(_driver);
		CheckBool(wccviewpermitdetailsrevertpage.verifyRevertButton(), "Verify if the revert button is visible");
		
		CheckResult(wcchomepage.logout(), "Click logout button");
		
	}

}
