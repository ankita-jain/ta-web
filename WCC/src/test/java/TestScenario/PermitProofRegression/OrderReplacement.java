package TestScenario.PermitProofRegression;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitReplacementRequestPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class OrderReplacement extends RingGoScenario{
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void userOrdersReplacementPermit(String TCID, Map<String, String> data) throws IOException {
		
		try {
		StartTest(TCID, "Create a Resident Permit , Approve the proofs and Authorise the Permit");
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String titlePermitProofPage = "Proof of vehicle ownership - " + VRN;
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
				"Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("proofonepath"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		
		PermitsHelper.AuthorisePermitApplication(VRN, _driver);
		
		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		wcchome.clickPermit();	
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		wccmypermitpage.selectViewDetails();
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		wccviewpermitdetailspage.JSclickOrderReplacement();
		
		 WCCPermitReplacementRequestPage wccpermitreplacementrequestpage = new WCCPermitReplacementRequestPage(_driver);
		 wccpermitreplacementrequestpage.enterReplacementReason(data.get("Reason"));
		 wccpermitreplacementrequestpage.clickRequestReplacement();
		 CheckValue(wccmypermitpage.getReplaceMentSuccessMessage(), "Your replacement permit request has been submitted for approval", "Checking the replacement message is correct");
		 CheckValue(wccmypermitpage.getPermitStatus("2"), "Replacement request", "Checking the status of the permit");
		 
		    //user logout
         CheckResult(wcchome.logout(), "Click logout button");
     } catch (AssertionError | Exception e) {
         LogError("Failed due -> " + e.getMessage());
         Log.add(LogStatus.FAIL, "Test is failed");
         Assert.fail("Failed as verification failed -'" + e.getMessage());
     }
	}
}