package TestScenario.PermitProofRegression.MotorCycle;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCTemporaryVehiclePage;


public class WCCMotorcycleTemporaryVehicleChange extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void temporaryVehicleChangeMotorcycle(String TCID, Map<String, String> data) throws IOException {
	
		try {
		StartTest(TCID, "Temporary vehicle change for a motorcylce permit.");
		Map<String, String> VRNs= PermitsHelper.setUpMotorcyclePermitForNewUser(0, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullMotorCyclePermit(_driver);
	
		WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
		wccpurchasepermitpaymenttype.clickTakePaymentNow();
		CheckResult(wccpurchasepermitpaymenttype.clickNextButton(), "Clicking the next button");
		WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
		wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();

		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click the finish button");
	
	
		PermitsHelper.AuthorisePermitApplication(VRNs.get("First VRN"), _driver);

		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		CheckResult(wcchome.clickPermit(), "Clicking permit link");
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
				"Checking the page title for my permit page is correct");
		wccmypermitpage.selectViewDetails();
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
	    wccviewpermitdetailspage.clickTemporaryVehicleChange();
		
		WCCTemporaryVehiclePage wcctempVehiclePage = new WCCTemporaryVehiclePage(_driver);
		wcctempVehiclePage.TemporaryVehicleProcess(VRNs.get("Second VRN"), data.get("ownership"), data.get("prooftype"), data.get("documentone"));
					
		WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
		wccpaymentcardetailspage.selectSecondCardOnAccount();
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	    wccpermitpurchasefinishpage.clickFinish();
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	            "Checking the page title for my permit page is correct");
		wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Authorised", "The status of the permit should be VRN Changed");
	
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
		wccmypermitpage.selectViewDetails();
		
		CheckBool(wccviewpermitdetailspage.verifyRevertButton(), "Verify if the revert button is visible");
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("364", "dd/MM/yyyy", false), "Checking the valid to date");
		
		 //user logout
        CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }	
	}
}

