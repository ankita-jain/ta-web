package TestScenario.PermitProofRegression.MotorCycle;

import static Utilities.DateUtils.DateFromOffset;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import Utilities.PropertyUtils;

public class MotorcycleRenewPermit extends RingGoScenario {
	
	private String insightUserName;
	private String insightPassword;
	private String operatorstatus = "1";
	private String operator = "42152";
	private String permitStatus = "Show All";
	
	@BeforeMethod
	public void createMotorCyclePermit() throws IOException {
		Map<String, String> VRNs= PermitsHelper.setUpMotorcyclePermitForNewUser(0, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullMotorCyclePermit(_driver);
		
		//Expire permit application
		NavigationHelper.openInsight(_driver);
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        
		LoginHelper.loginInsight(insightUserName, insightPassword, _driver);
    
        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
        CheckValue(insightservicemenupage.getCurrentPageTitle(), "Insight - Parking Zones", "Checking the page title for insight service menu is correct");
        insightservicemenupage.loadPermitSearchForm();
        InsightHelper.searchPermitAndEdit(VRNs.get("First VRN"), permitStatus, _driver);
        
        InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
        CheckContains(insightpermitapplicationpage.getPageTitle(), "Edit a Resident", 
        		"Checking the page title for permit insight page is correct");
        insightpermitapplicationpage.editPermitStatusAndSave("Expired");
        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();

        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
	}
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void userRenewsAMotorcyclePermitApplication(String TCID, Map<String, String> data) {
		try {
			
		StartTest(TCID, "Motorcycle permit renewal");
		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		CheckResult(wcchome.clickPermit(), "Click permits");
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		wccmypermitpage.selectRenewPermit();
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.permitRenewalProcess();
        
        
        WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();

		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();		
		
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("proofonepath"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Pending", "The status of the permit should be pending");
		
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), DateFromOffset("1", "dd/MM/yyyy", false), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("365", "dd/MM/yyyy", false), "Checking the valid to date");
		
		 //user logout
        CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }
	}
}
