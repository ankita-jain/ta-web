package TestScenario.PermitProofRegression.MotorCycle;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCViewPermitDetailsPage;

public class WCCMotorcycleTemporaryPermit extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void motorCylceTempPermit(String TCID, Map<String, String> data) throws IOException
	{
		try {
		StartTest(TCID, "Create a temporary permit for a motorcylce.");
		Map<String, String> VRNs= PermitsHelper.setUpMotorcyclePermitForNewUser(0, getSuiteStartDate(), _driver);
		PermitsHelper.createATemporaryMotorCyclePermit(_driver);
		WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
	 	wccpurchasepermitpaymenttype.clickTakePaymentNow();
	 	wccpurchasepermitpaymenttype.clickNextButton();
        WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
        wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();
        
        WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
        wccpermitpurchasefinishpage.clickFinish();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		wccmypermitpage.selectViewDetails();
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getPermitStatus(), "Temporary permit", "Checking the permit is in an temp status.");
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("41", "dd/MM/yyyy", false), "Checking the valid to date");
		
		 //user logout
		WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }	
	}
}