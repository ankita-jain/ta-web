package TestScenario.PermitProofRegression.MotorCycle;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCPermanentVehiclePage;

public class WCCMotorcyclePermanentVehicleChange extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void motorCyclePermenantVehicleChange(String TCID, Map<String, String> data) throws IOException
	{
		try {
		StartTest(TCID, "Permenant vehicle change for a motorcycle permit.");
		Map<String, String> VRNs= PermitsHelper.setUpMotorcyclePermitForNewUser(0, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullMotorCyclePermit(_driver);
		
		WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
		wccpurchasepermitpaymenttype.clickTakePaymentNow();
		CheckResult(wccpurchasepermitpaymenttype.clickNextButton(), "Clicking the next button");
		WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
		wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();

		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CVV"));
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click the finish button");
		
		
		PermitsHelper.AuthorisePermitApplication(VRNs.get("First VRN"), _driver);

		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		CheckResult(wcchome.clickPermit(), "Clicking permit link");
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
				"Checking the page title for my permit page is correct");
		wccmypermitpage.selectViewDetails();
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		wccviewpermitdetailspage.clickPermanentVehicleChange();
		WCCPermanentVehiclePage wccpermanentSecondVehiclePage = new WCCPermanentVehiclePage(_driver);
		wccpermanentSecondVehiclePage.addSecondPermanentVehicleProcess(VRNs.get("Second VRN"), data.get("ownership"), data.get("prooftype"), data.get("documentone"));
					
		WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
		wccpaymentcardetailspage.selectSecondCardOnAccount();
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CVV"));
        wccpermitpurchasefinishpage.clickFinish();
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Vrn changed", "The status of the permit should be VRN Changed");
		
		 //user logout
        CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }	
	}
}