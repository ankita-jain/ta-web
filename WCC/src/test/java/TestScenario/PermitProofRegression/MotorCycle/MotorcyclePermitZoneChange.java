package TestScenario.PermitProofRegression.MotorCycle;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;
import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitEditAddressPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.EditAddressProofUploadPage;

public class MotorcyclePermitZoneChange extends RingGoScenario{
	
	@BeforeMethod
	public void creatAndAuthoriseMotorCyclePermit() throws IOException
	{
		Map<String, String> VRNs= PermitsHelper.setUpMotorcyclePermitForNewUser(0, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullMotorCyclePermit(_driver);
		PermitsHelper.AuthorisePermitApplication(VRNs.get("First VRN"), _driver);
	}
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void userChangesZoneForMotorcyclePermit(String TCID, Map<String, String> data) {
		try {
			StartTest(TCID, "Address change status is shown when a user changes their address zone.");
			NavigationHelper.openWestMinster(_driver);
			WCCHome wcchomepage = new WCCHome(_driver);
	        wcchomepage.clickPermit();
			WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
			wccpermitpage.selectSeeAllPermitsLink();
	        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
			String Id = wccmypermitpage.getPermitId("1", "2");
			CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
			CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("364", "dd/MM/yyyy", false), "Checking the valid to date");
			
			wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
			WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
			CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "Motorcycle", "Checking the page title for view my permit page is correct");
			CheckResult(wccviewpermitdetailspage.clickEditAddressButton(), "Click edit address button");
			
			WCCPermitEditAddressPage wccpermiteditaddresspage = new WCCPermitEditAddressPage(_driver);
			wccpermiteditaddresspage.selectResidingLocation("Zone C (65003)");
			CheckResult(wccpermiteditaddresspage.clickSaveChanges(), "Click save button");
			
			EditAddressProofUploadPage editaddressproofuploadpage = new EditAddressProofUploadPage(_driver);
			CheckValue(editaddressproofuploadpage.getPageHeader(), "One accepted document",
	                "Checking the page title for resident permit proof of address upload page is correct");
			editaddressproofuploadpage.uploadOneProof(data.get("proofonepath"));
			editaddressproofuploadpage.clickNextButton();
	        WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
	        wccpaymentcardetailspage.selectSecondCardOnAccount();
			WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
		 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
	        wccpermitpurchasefinishpage.clickFinish();
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
			wccmypermitpage.getPermitId("1", "2");
			CheckValue(wccmypermitpage.getPermitStatus("2"), "Zone changed", "The status of the permit should be address change");
			
			 //user logout
			WCCHome wcchome = new WCCHome(_driver);
	        CheckResult(wcchome.logout(), "Click logout button");
	    } catch (AssertionError | Exception e) {
	        LogError("Failed due -> " + e.getMessage());
	        Log.add(LogStatus.FAIL, "Test is failed");
	        Assert.fail("Failed as verification failed -'" + e.getMessage());
	    }
	}

}
