package TestScenario.PermitProofRegression.MotorCycle;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitCancelPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCViewPermitDetailsPage;

public class WCCMotorcycleCancelPermit extends RingGoScenario {
	
	@Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = DataProviders.PermitDP.class)
	public void motorcyclePermitCancelPermit(String TCID, Map<String, String> data) throws IOException {
		
		try {
		StartTest(TCID, "User cancels a motorcycle permit.");
		Map<String, String> VRNs= PermitsHelper.setUpMotorcyclePermitForNewUser(0, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullMotorCyclePermit(_driver);
	 	WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
	 	wccpurchasepermitpaymenttype.clickTakePaymentNow();
	 	CheckResult(wccpurchasepermitpaymenttype.clickNextButton(), "Click next button");
        WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
        CheckResult(wccpaymentcarddetailspage.proceedToPaymentWithExistingCard(), "Selecting pay with existing card and clicking next button");
        
        WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CVV"));
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
        CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click finish button");

		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.selectViewDetails();
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckResult(wccviewpermitdetailspage.clickCancelPermit(), "Cancel permit");
		WCCPermitCancelPage wccpermitcancelpage = new WCCPermitCancelPage(_driver);
		CheckValue(wccpermitcancelpage.getCancelPagetitle(), "Cancellation request", "Checking the page title for cancellation request is correct.");
        wccpermitcancelpage.cancelPermitProcess("permit did not meet our threshold.");
        CheckValue(wccmypermitpage.getPermitStatus("2"), "Cancelled", "Checking the permit is in a cancelled status.");
		
		 //user logout
        WCCHome wcchome = new WCCHome(_driver);
       CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }	
	}
}
