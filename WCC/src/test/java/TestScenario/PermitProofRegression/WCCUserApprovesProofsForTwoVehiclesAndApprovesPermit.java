package TestScenario.PermitProofRegression;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import Utilities.PropertyUtils;
import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;

	public class WCCUserApprovesProofsForTwoVehiclesAndApprovesPermit extends RingGoScenario {
	
	private String numberPlate;
	private String insightUserName;
	private String insightPassword;
	

	@Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = DataProviders.PermitDP.class)
	public void viewMultipleVehicleTables(String TCID, Map<String, String> data) throws IOException
	{
		StartTest(TCID, "Create a Resident Permit for two Vehicles and accept any one proof ");
		
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String firstPlateNumber = "Proof of vehicle ownership - " + VRN;
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		numberPlate = randomAlphabetic(6).toUpperCase();
		wccresidentpermitdetailspage.addAnAdditionalRegistration(numberPlate, data.get("ownershipType"));
		
        PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		
        WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), firstPlateNumber,"Checking the page title for resident permit proof of vehicle page is correct");
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		
        WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		wccresidentpermitproofofvehiclesuploadpage.clickNextButton();
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.clickNextStepForSecondVehicle();
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		wccresidentpermitproofofvehiclesuploadpage.selectNextForSecondVehicle();

		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
        wccpermitdetailsconfirmationpage.confirmPermitDetails();
        
	 	WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
	 	wccpurchasepermitpaymenttype.clickTakePaymentNow();
	 	wccpurchasepermitpaymenttype.clickNextButton();
	 	
        WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
        wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();
        
        WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	 	
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
        wccpermitpurchasefinishpage.clickFinish();

		//User logs into insight and rejects proofs and permit
        NavigationHelper.openInsight(_driver);
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        LoginHelper.loginInsight(insightUserName, insightPassword, _driver);

                
        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
        insightservicemenupage.loadPermitSearchForm();
        InsightHelper.searchPermitAndEdit(VRN, data.get("Status"), _driver);
        
        InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
        CheckValue(insightpermitapplicationpage.getPageTitle(), "Edit a Resident Permit Application",
                "Checking the page title for edit permit insight page is correct");
        insightpermitapplicationpage.selectReasonForPemitProof("1", "Accepted");
        insightpermitapplicationpage.editPermitStatus(data.get("AuthorisedStatus"));
        insightpermitapplicationpage.clickSavePermit();
        
        
        NavigationHelper.openWestMinster(_driver);
        
        WCCHome wcchome = new WCCHome(_driver);		
		CheckValue(wcchome.getHomePageTitle(), "Welcome", "Checking the page title for home page page is correct");
		delay(3000);
		wcchome.clickPermit();
		
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);		
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");		
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		wccviewpermitdetailspage.getProofStatus("1");
		CheckValue(wccviewpermitdetailspage.getProofStatus("1"), "Accepted", "The Permit proof is not rejected");
		CheckValue(wccviewpermitdetailspage.getPermitStatus(), "Authorised", "Checking the permit is in an authorised status.");
		
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("365", "dd/MM/yyyy", false), "Checking the valid to date");
		
		CheckResult(wcchome.logout(), "Click logout button");
	}
}
