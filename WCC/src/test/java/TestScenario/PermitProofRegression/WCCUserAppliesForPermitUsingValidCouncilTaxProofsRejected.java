package TestScenario.PermitProofRegression;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCResidentPermitInfoPage;
import PageObjects.WCCResidentPermitPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import Utilities.PropertyUtils;

public class WCCUserAppliesForPermitUsingValidCouncilTaxProofsRejected extends RingGoScenario {
	
	private String VRN;
	private String insightUserName;
	private String insightPassword;
	
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void PersonalUserWithValidCouncilTaxLookUp(String TCID, Map<String, String> data) throws IOException{
		
		StartTest(TCID, "Create a Resident Permit for a user who has passed council tax lookup and got rejection on vehicle proofs");
		NavigationHelper.openWestMinster(_driver);
		LoginHelper.loginWestminster(data.get("CouncilUsername"), data.get("CouncilPassword"), _driver);
		WCCHome wcchomepage = new WCCHome(_driver);
	    CheckResult(wcchomepage.clickPermit(), "Click permit tile");
	                           
	    WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
	   				
	   	CheckResult(wccpermitpage.clickResidentPermit(), "Click resident permit");
	   	WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
	   		
	   	CheckResult(wccresidentpermitinfopage.clickStandardResidentPermit(), "Click standard resident permit");
	   	WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
	   	wccresidentpermitpage.applyResidentPermitApplication(data.get("POSTCODE"), data.get("ADDRESS"), data.get("ZONE"));
	    
	   	WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
	   	wccresidentpermitdetailspage.clearNameFields();
	    wccresidentpermitdetailspage.enterUserDetailsPermitDetails(data.get("USER_TITLE"), data.get("FirstName"), data.get("LastName"));
	    VRN = randomAlphabetic(6).toUpperCase();
	    wccresidentpermitdetailspage.addRegistration(VRN);
	    wccresidentpermitdetailspage.selectOwnershipType(data.get("OWNERSHIP"));
	    wccresidentpermitdetailspage.selectResidentType(data.get("RESIDENT_TYPE"));
	    wccresidentpermitdetailspage.selectTermAndConditions();
	    CheckResult(wccresidentpermitdetailspage.clickNextButton(), "Click next button");
        WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
        wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
        CheckResult(wccresidentialpermitproofofvehiclepage.navigateNextPage(), "Click next button");
        WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
        CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("vehicleproof"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
        
        
        //Insight user logs in and declines the proofs
		NavigationHelper.openInsight(_driver);
        
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
        
        LoginHelper.loginInsight(insightUserName, insightPassword, _driver);
        		        
        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
        insightservicemenupage.loadPermitSearchForm();
        InsightHelper.searchPermitAndEdit(VRN, data.get("Status"), _driver);
        
        InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
        CheckValue(insightpermitapplicationpage.getPageTitle(), "Edit a Resident Permit Application",
                "Checking the page title for edit permit insight page is correct");
        insightpermitapplicationpage.selectReasonForPemitProof("1", "Rejected");
        insightpermitapplicationpage.enterRejectReason("Your permit did not match our criteria.");
        insightpermitapplicationpage.editPermitStatus(data.get("AuthorisedStatus"));
        insightpermitapplicationpage.selectDeclinedReason("Did not score enough points");
        insightpermitapplicationpage.fillOutResponseToCustomer("your permit did not match our criteria.");
        insightpermitapplicationpage.clickSavePermit();
        insightpermitapplicationpage.endSession();
        
        NavigationHelper.openWestMinster(_driver);
        WCCHome wcchome = new WCCHome(_driver);
		
		CheckValue(wcchome.getHomePageTitle(), "Welcome",
                "Checking the page title for home page page is correct");
		delay(3000);
		CheckResult(wcchome.clickPermit(), "Click permits");
		CheckResult(wccpermitpage.selectSeeAllPermitsLink(), "Click see all permits link");
		
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
		wccmypermitpage.clickViewDetails(VRN);
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getVehcileProofStatus("1"), "Rejected", "The Permit proof is not rejected");
		CheckValue(wccviewpermitdetailspage.getPermitStatus(), "Declined", "Checking the permit is in an declined status.");
		
		CheckResult(wcchomepage.logout(), "Click logout button");
        
	}
}
