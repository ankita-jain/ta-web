package TestScenario.PermitProofRegression;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import static Utilities.DateUtils.DateFromOffset;

public class RenewPermit extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void userRenewsPermitApplication(String TCID, Map<String, String> data) throws IOException
	{
		try {
		StartTest(TCID, "Create a Resident Permit , Approve the proofs and Authorise the Permit");
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String titlePermitProofPage = "Proof of vehicle ownership - " + VRN;
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
				"Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("proofonepath"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		
		PermitsHelper.expirePermitApplication(titlePermitProofPage, _driver);
		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		CheckResult(wcchome.clickPermit(), "Click permits");
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		wccmypermitpage.selectRenewPermit();
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
        assertTrue("Permit renewal page", wccresidentpermitdetailspage.getResidentPermitDetailsPageTitle().contains("Resident permit renewal"));
        wccresidentpermitdetailspage.selectResidentType(data.get("reisdentType"));
        wccresidentpermitdetailspage.permitRenewalProcess();

		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();

		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();		
		
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("proofonepath"));
		PermitsHelper.completePaymentOfAPermit(data.get("CV2"), _driver);
		
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Pending", "The status of the permit should be pending");
		
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), DateFromOffset("1", "dd/MM/yyyy", false), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("365", "dd/MM/yyyy", false), "Checking the valid to date");
		
	    //user logout
        CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }
	}
}
