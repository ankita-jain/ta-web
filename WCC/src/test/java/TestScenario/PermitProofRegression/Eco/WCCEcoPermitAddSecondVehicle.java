package TestScenario.PermitProofRegression.Eco;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCAddSecondVehiclePage;

@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
public class WCCEcoPermitAddSecondVehicle extends RingGoScenario {
	
	private String secondVrn;
	private String zone = "65007";
	
		@BeforeMethod()
		public void creatAndAuthoriseEcoPermit() throws IOException
		{
			String VRN = PermitsHelper.setUpEcoPermitForNewUser(zone, getSuiteStartDate(), _driver);
			PermitsHelper.createAFullEcoPermit(_driver);
			PermitsHelper.AuthorisePermitApplication(VRN, _driver);
			
		}
		public void ecoAddSecondVehicle(String TCID, Map<String, String> data) {
		try {
			
			StartTest("xxx","ECO Permit add second vehicle.");
			
			NavigationHelper.openWestMinster(_driver);
			
			WCCHome wcchomepage = new WCCHome(_driver);
	        wcchomepage.clickPermit();
	        
			WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
			wccpermitpage.selectSeeAllPermitsLink();
			
	        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
			wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
			
			WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
			CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident Low Emission permit - Zone F", 
					"Checking the page title is correct");
			wccviewpermitdetailspage.clickAddSecondVehicleButton();
			
			WCCAddSecondVehiclePage wccaddSecondVehiclePage = new WCCAddSecondVehiclePage(_driver);
			secondVrn =randomAlphabetic(6).toUpperCase();
			wccaddSecondVehiclePage.addSecondVehicleProcess(secondVrn, data.get("ownership"), data.get("prooftype"), data.get("documentone"));
						
			WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
			wccpaymentcardetailspage.selectSecondCardOnAccount();
			
			WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
		 	
		 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
	        wccpermitpurchasefinishpage.clickFinish();
	        
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits", "Checking the page title for my permit page is correct");
			wccmypermitpage.getPermitId("1", "2");
			CheckValue(wccmypermitpage.getPermitStatus("2"), "Vrn changed", "The status of the permit should be VRN Changed");
			
			 //user logout
			WCCHome wcchome = new WCCHome(_driver);
	        CheckResult(wcchome.logout(), "Click logout button");
	        
	    } catch (AssertionError | Exception e) {
	        LogError("Failed due -> " + e.getMessage());
	        Log.add(LogStatus.FAIL, "Test is failed");
	        Assert.fail("Failed as verification failed -'" + e.getMessage());
	    }
	}
}