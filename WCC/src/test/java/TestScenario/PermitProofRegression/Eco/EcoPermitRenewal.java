package TestScenario.PermitProofRegression.Eco;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;
import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCEcoPermitPurchaseFinishPage;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
public class EcoPermitRenewal extends RingGoScenario {
	
	private String zone = "65007";
	
	public void userRenewsEcoPermit(String TCID, Map<String, String> data) throws IOException {
		
		try {
		
		StartTest(TCID, "Create a Eco permit.");
		
		String VRN = PermitsHelper.setUpEcoPermitForNewUser(zone, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullEcoPermit(_driver);
		
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		wccmypermitpage.selectViewDetails();
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident Low Emission permit - Zone F", "Checking the page title is correct");
		CheckValue(wccviewpermitdetailspage.getPermitStatus(), "Pending", "Checking the permit is in an pending status.");
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("364", "dd/MM/yyyy", false), "Checking the valid to date");
		
		PermitsHelper.expirePermitApplication(VRN, _driver);
		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchome = new WCCHome(_driver);
		CheckResult(wcchome.clickPermit(), "Click permits");
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
		wccmypermitpage.selectRenewPermit();
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
        wccresidentpermitdetailspage.selectResidentType(data.get("reisdentType"));
        wccresidentpermitdetailspage.permitRenewalProcess();

        WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(data.get("proofonepath"));
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();		
		
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(data.get("proofonepath"));
		wccresidentpermitproofofvehiclesuploadpage.clickNextButton();
		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
		wccpermitdetailsconfirmationpage.confirmPermitDetails();
	 	WCCEcoPermitPurchaseFinishPage wccecopermitpurchasefinishpage = new WCCEcoPermitPurchaseFinishPage(_driver);
		wccecopermitpurchasefinishpage.clickConfirm();
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		wccpermitpurchasefinishpage.clickFinish();
		
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Pending", "The status of the permit should be pending");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), DateFromOffset("1", "dd/MM/yyyy", false), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("365", "dd/MM/yyyy", false), "Checking the valid to date");
			
			//user logout
	        CheckResult(wcchome.logout(), "Click logout button");
	        
	    } catch (AssertionError | Exception e) {
	        LogError("Failed due -> " + e.getMessage());
	        Log.add(LogStatus.FAIL, "Test is failed");
	        Assert.fail("Failed as verification failed -'" + e.getMessage());
	    }
	}

}
