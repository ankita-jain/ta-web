package TestScenario.PermitProofRegression.Eco;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCViewPermitDetailsPage;
import static Utilities.DateUtils.TimeStamp;
import static Utilities.DateUtils.DateFromOffset;

public class WCCEcoPermit extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void createEcoPermit(String TCID, Map<String, String> data) {
	try {
		
		StartTest(TCID, "Create a Eco permit.");
		
		PermitsHelper.setUpEcoPermitForNewUser(data.get("zone"), getSuiteStartDate(), _driver);
		PermitsHelper.createAFullEcoPermit(_driver);
		
		WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		wccmypermitpage.selectViewDetails();
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident Low Emission permit - Zone F", "Checking the page title is correct");
		CheckValue(wccviewpermitdetailspage.getPermitStatus(), "Pending", "Checking the permit is in an pending status.");
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("364", "dd/MM/yyyy", false), "Checking the valid to date");
		
		//user logout
		WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.logout(), "Click logout button");
        
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }
	}
}