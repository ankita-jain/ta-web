package TestScenario.PermitProofRegression.Eco;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitEditAddressPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.EditAddressProofUploadPage;

public class EcoPermitChangeZone extends RingGoScenario {
	
	private String zone = "65007";
	
		
		@BeforeMethod
		public void creatAndAuthoriseEcoPermit() throws IOException
		{
			String VRN = PermitsHelper.setUpEcoPermitForNewUser(zone, getSuiteStartDate(), _driver);
			PermitsHelper.createAFullEcoPermit(_driver);
			PermitsHelper.AuthorisePermitApplication(VRN, _driver);
			
		}
		
		@Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = DataProviders.PermitDP.class)
		public void userChangesThePermitZone(String TCID, Map<String, String> data) {
		try {
			
			StartTest(TCID, "Address change status is shown when a user changes their address zone.");
			
			NavigationHelper.openWestMinster(_driver);
			
			WCCHome wcchomepage = new WCCHome(_driver);
	        wcchomepage.clickPermit();
	        
			WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
			wccpermitpage.selectSeeAllPermitsLink();
			
	        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
			wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
			
			WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
			CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "Low Emission", "Checking the page title for view my permit page is correct");
			CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident Low Emission permit - Zone F", 
					"Checking the page title is correct");
			wccviewpermitdetailspage.clickEditAddressButton();
			
			WCCPermitEditAddressPage wccpermiteditaddresspage = new WCCPermitEditAddressPage(_driver);
			wccpermiteditaddresspage.selectResidingLocation("Zone C (65003)");
			CheckResult(wccpermiteditaddresspage.clickSaveChanges(), "Click save button");
			
			EditAddressProofUploadPage editaddressproofuploadpage = new EditAddressProofUploadPage(_driver);
			CheckValue(editaddressproofuploadpage.getPageHeader(), "One accepted document",
	                "Checking the page title for resident permit proof of address upload page is correct");
			editaddressproofuploadpage.uploadOneProof(data.get("proofonepath"));
			CheckResult(editaddressproofuploadpage.clickNextButton(), "Click next button");
			
			WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
			CheckResult(wccpaymentcardetailspage.selectSecondCardOnAccount(), "Pay with second card on the acocunt");
			
			WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
		 	
		 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
	        CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click finish button");
	        
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
			CheckValue(wccmypermitpage.getPermitStatus("2"), "Zone changed", "The status of the permit should be Zone change");
			
			 //user logout
			WCCHome wcchome = new WCCHome(_driver);
	        CheckResult(wcchome.logout(), "Click logout button");
	        delay(5000);
	    } catch (AssertionError | Exception e) {
	       LogError("Failed due -> " + e.getMessage());
	        Log.add(LogStatus.FAIL, "Test is failed");
	       Assert.fail("Failed as verification failed -'" + e.getMessage());
	    }
		
		
	}

}
