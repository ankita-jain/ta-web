package TestScenario.PermitProofRegression.Eco;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.WCCTemporaryVehiclePage;

public class WCCEcoPermitTemporaryVehicleChange extends RingGoScenario {
	
	private String zone = "65007";
	private String secondVrn;
	
	@BeforeMethod() 
	public void createAndAuthoriseEcoPermit() throws Exception {
		String VRN = PermitsHelper.setUpEcoPermitForNewUser(zone, getSuiteStartDate(), _driver);
		PermitsHelper.createAFullEcoPermit(_driver);
		PermitsHelper.AuthorisePermitApplication(VRN, _driver);
	}
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void ecoTemporaryVehicleChange(String TCID, Map<String, String> data) {
		try {
		StartTest(TCID, "Temporary VRN Change for an Authorise Permit");
		NavigationHelper.openWestMinster(_driver);
		WCCHome wcchomepage = new WCCHome(_driver);
        wcchomepage.clickPermit();
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		wccpermitpage.selectSeeAllPermitsLink();
        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckContains(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident Low Emission permit - Zone F", 
				"Checking the page title is correct");
		wccviewpermitdetailspage.clickTemporaryVehicleChange();
		
		WCCTemporaryVehiclePage wcctempVehiclePage = new WCCTemporaryVehiclePage(_driver);
		secondVrn =randomAlphabetic(6).toUpperCase();
		wcctempVehiclePage.TemporaryVehicleProcess(secondVrn, data.get("ownership"), data.get("prooftype"), data.get("documentone"));
					
		WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);
		wccpaymentcardetailspage.selectSecondCardOnAccount();
		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
        wccpermitpurchasefinishpage.clickFinish();
    	CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
                "Checking the page title for my permit page is correct");
		wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Authorised", "The status of the permit should be VRN Changed");
	
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
		wccmypermitpage.selectViewDetails();
		
		WCCViewPermitDetailsPage wccviewpermitdetailsrevertpage = new WCCViewPermitDetailsPage(_driver);
		CheckBool(wccviewpermitdetailsrevertpage.verifyRevertButton(), "Verify if the revert button is visible");
		wccviewpermitdetailspage.clickMyPermitBreadcrumb();
		String Id = wccmypermitpage.getPermitId("1", "2");
		CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
		CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("365", "dd/MM/yyyy", false), "Checking the valid to date");
		
		 //user logout
		WCCHome wcchome = new WCCHome(_driver);
        CheckResult(wcchome.logout(), "Click logout button");
    } catch (AssertionError | Exception e) {
        LogError("Failed due -> " + e.getMessage());
        Log.add(LogStatus.FAIL, "Test is failed");
        Assert.fail("Failed as verification failed -'" + e.getMessage());
    }	
	}
}