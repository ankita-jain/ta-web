package TestScenario.PermitProofRegression.Eco;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitCancelPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCViewPermitDetailsPage;

public class WCCEcoCancelPermit extends RingGoScenario {
	
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void EcoPermitCancel(String TCID, Map<String, String> data) throws IOException {
		
		try {
			StartTest(TCID, "Eco Permit cancellation.");
			
			String VRN = PermitsHelper.setUpEcoPermitForNewUser(data.get("zone"), getSuiteStartDate(), _driver);
			PermitsHelper.createAFullEcoPermit(_driver);
			
			WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
			CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
			wccmypermitpage.selectViewDetails();
			
			WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
			wccviewpermitdetailspage.clickCancelPermit();
			
			WCCPermitCancelPage wccpermitcancelpage = new WCCPermitCancelPage(_driver);
			CheckValue(wccpermitcancelpage.getCancelPagetitle(), "Cancellation request", "Checking the page title for cancellation request is correct.");
	        wccpermitcancelpage.cancelPermitProcess("permit did not meet our threshold.");
	        
	        CheckValue(wccmypermitpage.getPermitStatus("2"), "Cancelled", "Checking the permit is in a cancelled status.");
	        
	        PermitsHelper.CancelPermitApplication(VRN, _driver);
	        NavigationHelper.openWestMinster(_driver);
	        WCCHome wcchome = new WCCHome(_driver);
	        CheckResult(wcchome.clickPermit(), "Click permits");
	        WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);	        
	        wccpermitpage.selectSeeAllPermitsLink();
	        CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits",
	                "Checking the page title for my permit page is correct");
			 CheckValue(wccmypermitpage.getPermitStatus("2"), "Cancelled", "Checking the permit is in a cancelled status.");
			
			
			 //user logout
	        CheckResult(wcchome.logout(), "Click logout button");
	    } catch (AssertionError | Exception e) {
	        LogError("Failed due -> " + e.getMessage());
	        Log.add(LogStatus.FAIL, "Test is failed");
	        Assert.fail("Failed as verification failed -'" + e.getMessage());
	    }	
	}
}
