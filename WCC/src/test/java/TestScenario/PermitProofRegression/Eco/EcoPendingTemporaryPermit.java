package TestScenario.PermitProofRegression.Eco;

import static Utilities.DateUtils.DateFromOffset;
import static Utilities.DateUtils.TimeStamp;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import Logging.Log;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Insight.InsightDefaultOperatorPage;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Permit.ManageProofsPage;
import Utilities.PropertyUtils;

public class EcoPendingTemporaryPermit extends RingGoScenario {
	
	private String insightUserName;
	private String insightPassword;
	
	@Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = DataProviders.PermitDP.class)
	public void userCreatesAPendingTemporaryEcoPermit(String TCID, Map<String, String> data) throws IOException {
		
		try {
			StartTest(TCID, "Create temporary eco permit.");
			String VRN = PermitsHelper.setUpEcoPermitForNewUser(data.get("zone"), getSuiteStartDate(), _driver);
			PermitsHelper.createATemporaryEcoPermit(_driver);
			WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
			wccmypermitpage.selectViewDetails();
			WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
			CheckContains(wccviewpermitdetailspage.getViewPermitTitle(),
					"View permit: Resident Low Emission permit - Zone F", "Checking the page title is correct");
			CheckValue(wccviewpermitdetailspage.getPermitStatus(), "Pending",
					"Checking the permit is in an pending status.");

			NavigationHelper.openInsight(_driver);
			insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
	        insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);
	        LoginHelper.loginInsight(insightUserName, insightPassword, _driver);
	       	        
	        InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
	        CheckValue(insightservicemenupage.getCurrentPageTitle(), "Insight - Parking Zones", "Checking the page title for insight service menu is correct");
	        insightservicemenupage.loadPermitSearchForm();
			InsightHelper.searchPermitAndEdit(VRN, data.get("status"), _driver);
			InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
			CheckContains(insightpermitapplicationpage.getPageTitle(), "Edit a Resident",
					"Checking the page title for permit insight page is correct");
			insightpermitapplicationpage.editPermitStatus("Temporary permit");
			insightpermitapplicationpage.fillOutResponseToCustomer("This can be hard coded");
			insightpermitapplicationpage.selectATempPermitType("UK");

			NavigationHelper.openWestMinster(_driver);
			WCCHome wcchomepage = new WCCHome(_driver);
			wcchomepage.clickPermit();
			WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
			wccpermitpage.selectSeeAllPermitsLink();
			CheckValue(wccmypermitpage.getPermitStatus("2"), "Temporary permit",
					"Checking the status of the permit");
			wccmypermitpage.clickUploadButton();
			ManageProofsPage manageproofspage = new ManageProofsPage(_driver);
			manageproofspage.uploadProof(data.get("proof"));
			manageproofspage.saveChanges();
			CheckResult(manageproofspage.clickFinishButton(), "Click finish button");
			
			String Id = wccmypermitpage.getPermitId("1", "2");
			CheckValue(wccmypermitpage.getPermitStatus("2"), "Pending - temporary permit", "Checking the status of the permit");
			CheckValue(wccmypermitpage.getPermitValidFrom(Id), TimeStamp("dd/MM/yyyy"), "Checking the valid from date");
			CheckValue(wccmypermitpage.getPermitValidTo(Id), DateFromOffset("41", "dd/MM/yyyy", false), "Checking the valid to date");
			
			
			

			// user logout
			WCCHome wcchome = new WCCHome(_driver);
			CheckResult(wcchome.logout(), "Click logout button");
		} catch (AssertionError | Exception e) {
			LogError("Failed due -> " + e.getMessage());
			Log.add(LogStatus.FAIL, "Test is failed");
			Assert.fail("Failed as verification failed -'" + e.getMessage());
		}
	}

}
