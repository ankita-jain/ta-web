package TestScenario.PermitProofRegression;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.PermitsHelper;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.WCCHome;
import PageObjects.WCCMyPermitPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPaymentDetailsPage;
import PageObjects.WCCPermitEditAddressPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCViewPermitDetailsPage;
import PageObjects.Permit.EditAddressProofUploadPage;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;

public class WCCUserChangesAddressAddressChange extends RingGoScenario {
	
	private String proofonepath = "1.jpg";
	private String vehicleproof = "1.jpg";
	private String CV2 = "121";
	
	/**
	 * Method to create a permit, upload proofs and authorise the permit in insight.
	 * @throws IOException 
	 */
	@BeforeMethod()
	public void createAndAuthorisePermit() throws IOException {
		String VRN = PermitsHelper.setUpResidencialPermitForANewUser(getSuiteStartDate(), _driver);
		String titlePermitProofPage="Proof of vehicle ownership - "+VRN;
		PermitsHelper.selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
                "Checking the page title for resident permit proof of address page is correct");
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
                "Checking the page title for resident permit proof of address upload page is correct");
		
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proofonepath);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();
		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclepage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		
		CheckValue(wccresidentialpermitproofofvehiclepage.getTitleVehicle(), titlePermitProofPage,
                "Checking the page title for resident permit proof of vehicle page is correct");
		
		wccresidentialpermitproofofvehiclepage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclepage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		
		CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
                "Checking the page title for resident permit proof of vehicle upload page is correct");
		
		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(vehicleproof);
		PermitsHelper.completePaymentOfAPermit(CV2, _driver);
		
		PermitsHelper.AuthorisePermitApplication(VRN, _driver);
	}
	
	/**
	 * Test to verify that when a user changes their address zone, they need to upload proofs & the status is correct.
	 * @param TCID
	 * @param data
	 */
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void customerChangesAddress(String TCID, Map<String, String> data) {
		
		StartTest(TCID, "Address change status is shown when a user changes their address zone.");
		
		NavigationHelper.openWestMinster(_driver);
		
		WCCHome wcchomepage = new WCCHome(_driver);
        CheckResult(wcchomepage.clickPayment(), "Click payment button");
        
        WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
        wccpaymentdetailspage.clickAddNewCard();
                
        WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
        CheckResult(wccaddpaymentcardpopup.enterCardNumber(data.get("cardnumber")), "entering a new card number");
        CheckResult(wccaddpaymentcardpopup.enterCardExpires(data.get("expiry")), "entering card expiry date");
        CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
        
        delay(3000);
        
        wccpaymentdetailspage.clickHomeBreadCrumb();
        
        CheckResult(wcchomepage.clickPermit(), "Click permits");
        
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		CheckResult(wccpermitpage.selectSeeAllPermitsLink(), "Click see all permits link");
		
        WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits","Checking the page title for my permit page is correct");
		wccmypermitpage.jsClick(wccmypermitpage.viewDetails);
		
		WCCViewPermitDetailsPage wccviewpermitdetailspage = new WCCViewPermitDetailsPage(_driver);
		CheckValue(wccviewpermitdetailspage.getViewPermitTitle(), "View permit: Resident permit - Zone F, Westminster",
                "Checking the page title for view my permit page is correct");
		CheckResult(wccviewpermitdetailspage.clickEditAddressButton(), "Click edit address link");
		
		WCCPermitEditAddressPage wccpermiteditaddresspage = new WCCPermitEditAddressPage(_driver);
		wccpermiteditaddresspage.clickEnterAddress();
		wccpermiteditaddresspage.enterPostcode(data.get("postcode"));
		CheckResult(wccpermiteditaddresspage.clickFindAddress(), "Click find address button");
		wccpermiteditaddresspage.selectAddress(data.get("address"));
		delay(3000);
		CheckResult(wccpermiteditaddresspage.clickSaveChanges(), "Click save");
		
		EditAddressProofUploadPage editaddressproofuploadpage = new EditAddressProofUploadPage(_driver);
		CheckValue(editaddressproofuploadpage.getPageHeader(), "One accepted document","Checking the page title for resident permit proof of address upload page is correct");
		editaddressproofuploadpage.uploadOneProof(data.get("proofonepath"));
		editaddressproofuploadpage.clickNextButton();
		
        WCCPaymentCardDetailsPage wccpaymentcardetailspage = new WCCPaymentCardDetailsPage(_driver);		
        CheckResult(wccpaymentcardetailspage.selectSecondCardOnAccount(), "Pay with second card on account");
        
		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
	 	wccpaymentcardcvvpage.enterCV2AndProceed(data.get("CV2"));
	 	
	 	WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
        CheckResult(wccpermitpurchasefinishpage.clickFinish(), "Click finish button");
        
		CheckValue(wccmypermitpage.getMyPermitPageTitle(), "My permits", "Checking the page title for my permit page is correct");
		CheckValue(wccmypermitpage.getPermitStatus("2"), "Address changed", "The status of the permit should be address change");
		
		CheckResult(wcchomepage.logout(), "Click logout button");
	}
}