package TestScenario.NewCorporateRedesign;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static SeleniumHelpers.JavaScriptHelpers.acceptAlert;

public class RemoveEmployeeFromAllVrn extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private List<String> employeePhone;
    private List<String> employeeVrn;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11)});
        employeeVrn = Arrays.asList(new String[]{RandomStringUtils.randomAlphanumeric(7)});
    }

    @Test
    public void removeEmployeeFromAllVrn() {
        StartTest("99346", "Remove Employee from all VRNs");

        LogStep("Step - 1", "Login to the Corporate account.");
        NavigationHelper.openMyRingo(_driver);

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, "", employeePhone, employeeVrn, true, _driver);

        String nameString = new StringBuilder().append(employeeName).append(" ").append(employeeSurname).toString();
        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        int rowIndex = employeeDashboardPage.employeeTable.getRowIndexByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);
        WebElement row = employeeDashboardPage.employeeTable.getRowByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);

        LogStep("Step - 3", "Click on kebab menu against specific employee");
        employeeDashboardPage.kebabMenuLink = new EmployeeDashboardPage.KebabMenuLink(row, rowIndex);
        CheckResult(employeeDashboardPage.kebabMenuLink.clickKebabMenu(), "Clicking on kebab menu");
        employeeDashboardPage.kebabMenu = new EmployeeDashboardPage.KebabMenu(row, rowIndex);

        LogStep("Step - 4", "Click on Remove from all vehicle item");
        CheckResult(employeeDashboardPage.kebabMenu.clickRemoveFromAllVehiclesLink(), "Clicking on Remove from All Vehicle link");

        LogStep("Step - 5", "Confirm the confirmation message");
        CheckResult(acceptAlert(_driver, "Removing all vehicles will mean this employee cannot park " +
                "as they will not be linked to any vehicle.\n" +
                "Are you sure you wish to remove all vehicles from this employee?"), "Accept alert");
        CheckValue(employeeDashboardPage.employeeTable.getCell(rowIndex, EmployeeDashboardPage.EmployeeTableColumns.VEHICLES.getPosition()).getText(),
                "",
                "Checking Vehicles column");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
