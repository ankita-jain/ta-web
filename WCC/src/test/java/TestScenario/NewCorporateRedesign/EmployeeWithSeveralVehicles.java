package TestScenario.NewCorporateRedesign;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class EmployeeWithSeveralVehicles extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private List<String> employeePhone;
    private List<String> employeeVrn;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeVrn = Arrays.asList(new String[]{RandomStringUtils.randomAlphanumeric(7),
                RandomStringUtils.randomAlphanumeric(7), RandomStringUtils.randomAlphanumeric(7)});
        employeePhone = Arrays.asList("Any");
    }

    @Test
    public void employeeWithSeveralVehicles() {
        StartTest("104825", "Cancel Delete Employee");

        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Setup -> Add or Edit Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, "", employeePhone, employeeVrn, true, _driver);
        String nameString = new StringBuilder().append(employeeName).append(" ").append(employeeSurname).toString();

        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        int rowIndex = employeeDashboardPage.employeeTable.getRowIndexByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);
        WebElement row = employeeDashboardPage.employeeTable.getRowByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);

        LogStep("Step - 5", "Click on the cell with VRNs for employee with more then2 VRNs");
        employeeDashboardPage.getMoreVehiclesLink = new EmployeeDashboardPage.GetMoreVehiclesLink(row, rowIndex);
        CheckResult(employeeDashboardPage.getMoreVehiclesLink.clickGetMoreVehiclesLink(),
                "Clicking on Get More Vehicles link");

        employeeDashboardPage.modalBox = new EmployeeDashboardPage.ModalBox(_driver);
        CheckValue(employeeDashboardPage.modalBox.getHeaderText(), "Vehicles", "Checking modal box header");
        for (int i = 0; i < employeeVrn.size(); i++)
            CheckContains(employeeDashboardPage.modalBox.getBodyText(), employeeVrn.get(i).toUpperCase(), "Vehicles modal box");

        CheckResult(employeeDashboardPage.modalBox.clickOnCloseButton(), "Click on Close button");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}