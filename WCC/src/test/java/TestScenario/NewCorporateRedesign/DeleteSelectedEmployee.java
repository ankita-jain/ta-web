package TestScenario.NewCorporateRedesign;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.NewCorporate.EmployeeDeleteConfirmationPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class DeleteSelectedEmployee extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private List<String> employeePhone;
    private List<String> employeeVrn;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11)});
        employeeVrn = Arrays.asList("Any");
    }

    @Test
    public void deleteSelectedEmployee() {
        StartTest("104624", "Delete Selected Employees");

        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Setup -> Add or Edit Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, "", employeePhone, employeeVrn, true, _driver);
        String nameString = new StringBuilder().append(employeeName).append(" ").append(employeeSurname).toString();

        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        int rowIndex = employeeDashboardPage.employeeTable.getRowIndexByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);
        WebElement row = employeeDashboardPage.employeeTable.getRowByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);

        LogStep("Step - 5", "Select several employees");
        employeeDashboardPage.checkbox = new EmployeeDashboardPage.CheckBox(row, rowIndex);
        CheckResult(employeeDashboardPage.checkbox.clickCheckBox(), "Clicking on selection checkbox");

        LogStep("Step - 6", "Click on Delete button");
        CheckResult(employeeDashboardPage.clickDeleteButton(), "Clicking on Delete button");

        LogStep("Step - 7", "Click Ok button");
        EmployeeDeleteConfirmationPage employeeDeleteConfirmationPage = new EmployeeDeleteConfirmationPage(_driver);
        CheckResult(employeeDeleteConfirmationPage.clickYes(), "Clicking on Yes button");

        CheckValue(employeeDashboardPage.getSuccessNotificationMessage(), "1 employee(s) have been deleted",
                "Checking success notification message");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
