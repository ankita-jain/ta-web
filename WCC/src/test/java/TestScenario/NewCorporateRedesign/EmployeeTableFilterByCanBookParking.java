package TestScenario.NewCorporateRedesign;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.NewCorporate.EmployeeDashboardFilterPopup;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class EmployeeTableFilterByCanBookParking extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;

    private String employeeName;
    private String employeeSurname;
    private String reference;
    private List<String> employeePhone;
    private List<String> employeeVrn;

    private String employeeName2;
    private String employeeSurname2;
    private String reference2;
    private List<String> employeePhone2;
    private List<String> employeeVrn2;


    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);

        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11)});
        employeeVrn = Arrays.asList(new String[]{RandomStringUtils.randomAlphanumeric(7)});
        reference = RandomStringUtils.randomAlphabetic(10);

        employeeName2 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname2 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone2 = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11)});
        employeeVrn2 = Arrays.asList(new String[]{RandomStringUtils.randomAlphanumeric(7)});
        reference2 = RandomStringUtils.randomAlphabetic(10);
    }

    @Test
    public void employeeTableFilterByCanPark() {
        StartTest("107050", "Employee Table filter by Can book parking");

        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Go to the Setup -> Add or Edit Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName2, employeeSurname2, reference2, employeePhone2, employeeVrn2, false, _driver);
        CorporateHelper.addEmployee(employeeName, employeeSurname, reference, employeePhone, employeeVrn, true, _driver);

        LogStep("Step - 5", "Click on Filter button");
        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        employeeDashboardPage.clickFilterButton();

        EmployeeDashboardFilterPopup filterPopup = new EmployeeDashboardFilterPopup(_driver);
        CheckResult(filterPopup.clickCanPark(), "Clicking on Can Park tab");
        CheckResult(filterPopup.setCanParkFalse(), "Set Can Park true");
        CheckResult(filterPopup.clickSaveButton(), "Clicking on Save button");

        CheckValue(employeeDashboardPage.employeeTable.IsRowWithValueExists(reference, EmployeeDashboardPage.EmployeeTableColumns.REFERENCE),
                false, "Checking if row is displayed");
        CheckValue(employeeDashboardPage.employeeTable.IsRowWithValueExists(reference2, EmployeeDashboardPage.EmployeeTableColumns.REFERENCE),
                true, "Checking if row is not displayed");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
