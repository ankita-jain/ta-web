package TestScenario.NewCorporateRedesign;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeTableSorting extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;

    private String employeeName1;
    private String employeeSurname1;
    private List<String> employeePhone1;
    private List<String> employeeVrn1;

    private String employeeName2;
    private String employeeSurname2;
    private List<String> employeePhone2;
    private List<String> employeeVrn2;

    private String employeeName3;
    private String employeeSurname3;
    private List<String> employeePhone3;
    private List<String> employeeVrn3;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);

        employeeName1 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname1 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone1 = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11),
                RandomStringUtils.randomNumeric(11), RandomStringUtils.randomNumeric(11)});
        employeeVrn1 = Arrays.asList("Any");

        employeeName2 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname2 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone2 = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11),
                RandomStringUtils.randomNumeric(11), RandomStringUtils.randomNumeric(11)});
        employeeVrn2 = Arrays.asList("Any");

        employeeName3 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname3 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone3 = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11),
                RandomStringUtils.randomNumeric(11), RandomStringUtils.randomNumeric(11)});
        employeeVrn3 = Arrays.asList("Any");
    }

    @Test
    public void employeeTableSorting() {
    	NavigationHelper.openMyRingo(_driver);
        StartTest("102976", "Employee Table sorting");

        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Setup -> Add or Edit Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName1, employeeSurname1, "", employeePhone1, employeeVrn1, true, _driver);
        CorporateHelper.addEmployee(employeeName2, employeeSurname2, "", employeePhone2, employeeVrn2, true, _driver);
        CorporateHelper.addEmployee(employeeName3, employeeSurname3, "", employeePhone3, employeeVrn3, true, _driver);

        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        List<List<WebElement>> rows = employeeDashboardPage.employeeTable.getRows();
        List<String> names = rows.stream().map(row -> row.get(EmployeeDashboardPage.EmployeeTableColumns.NAME.getPosition()).getText()).collect(Collectors.toList());
        List<String> namesSorted = names.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        LogStep("Step - 5", "Click on arrow in Name column");
        employeeDashboardPage.clickOnColumnHeader(EmployeeDashboardPage.EmployeeTableColumns.NAME);
        rows = employeeDashboardPage.employeeTable.getRows();
        names = rows.stream().map(row -> row.get(EmployeeDashboardPage.EmployeeTableColumns.NAME.getPosition()).getText()).collect(Collectors.toList());

        CheckValue(names.equals(namesSorted), true, "Checking sorting by Name column");

        LogStep("Step - 6", "Click on arrow in Name column again");
        employeeDashboardPage.clickOnColumnHeader(EmployeeDashboardPage.EmployeeTableColumns.NAME);
        rows = employeeDashboardPage.employeeTable.getRows();
        names = rows.stream().map(row -> row.get(EmployeeDashboardPage.EmployeeTableColumns.NAME.getPosition()).getText()).collect(Collectors.toList());
        namesSorted = names.stream().sorted().collect(Collectors.toList());

        CheckValue(names.equals(namesSorted), true, "Checking sorting bu Name column");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}