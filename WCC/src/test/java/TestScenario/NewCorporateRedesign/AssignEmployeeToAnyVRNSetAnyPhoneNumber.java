package TestScenario.NewCorporateRedesign;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static SeleniumHelpers.JavaScriptHelpers.acceptAlert;

public class AssignEmployeeToAnyVRNSetAnyPhoneNumber extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private List<String> employeePhone;
    private List<String> employeeVrn;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone = Arrays.asList("Any");
        employeeVrn = Arrays.asList(new String[]{RandomStringUtils.randomAlphanumeric(7)});
    }

    @Test
    public void assignEmployeeToAnyVRNSetAnyPhoneNumber() {
        StartTest("99344", "Assign Employee to Any VRN if Phone Number is set to Any Phone Number");

        LogStep("Step - 1", "Login to the Corporate account.");
        NavigationHelper.openMyRingo(_driver);

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, "", employeePhone, employeeVrn, true, _driver);

        String nameString = new StringBuilder().append(employeeName).append(" ").append(employeeSurname).toString();
        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        int rowIndex = employeeDashboardPage.employeeTable.getRowIndexByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);
        WebElement row = employeeDashboardPage.employeeTable.getRowByCellValue(nameString,
                EmployeeDashboardPage.EmployeeTableColumns.NAME);

        LogStep("Step - 3", "Click on kebab menu against specific employee");
        employeeDashboardPage.kebabMenuLink = new EmployeeDashboardPage.KebabMenuLink(row, rowIndex);
        CheckResult(employeeDashboardPage.kebabMenuLink.clickKebabMenu(), "Clicking on kebab menu");
        employeeDashboardPage.kebabMenu = new EmployeeDashboardPage.KebabMenu(row, rowIndex);

        LogStep("Step - 4", "Click on Assign to any vehicle item");
        CheckResult(employeeDashboardPage.kebabMenu.clickAssignToAnyVehicleLink(), "Clicking on Assign TO Any Vehicle link");

        CheckResult(acceptAlert(_driver, "Please note: Using the Any Vehicle setting confirms you take liability for any parking incurred across any vehicle " +
                "in a parking session that uses a phone number specified against the employee.\n" +
                "Are you sure you wish to assign this employee to any vehicle?"), "Accept alert");
        CheckValue(employeeDashboardPage.getErrorNotificationMessage(),
                "Cannot select any vehicle when any phone number has been set",
                "Checking error message");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
