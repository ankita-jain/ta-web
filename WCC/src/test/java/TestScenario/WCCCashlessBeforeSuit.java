package TestScenario;

import DataProviderHelpers.DBHelper;
import DataProviderHelpers.DatabaseProperties;
import GenericComponents.ExcelReadWrite;
import GenericComponents.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.annotations.BeforeSuite;

import java.sql.Connection;
import java.sql.Statement;

public class WCCCashlessBeforeSuit {

    @BeforeSuite
    public void ClearPaymentCards() throws Exception {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE_BITS_AND_BOBS"));
        HSSFSheet WCC_PAYMENT_CARDS = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_BaB_ADD_CARD"));
        int RowCount = xl.getrowcount(WCC_PAYMENT_CARDS);
        Connection connect;
        connect = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String userName = xl.Readvalue(WCC_PAYMENT_CARDS, xlRow, "Username");
            Statement stmt = null;
            /**
             * This script will delete all cards for user except the one which was added on registration.
             * MemberCards table contains cards added after registration
             */
            String query = "DELETE dbo.MembersCards FROM dbo.MembersCards" +
                    "INNER JOIN dbo.RingGo_Members " +
                    "ON dbo.MembersCards.UserID = dbo.RingGo_Members.userid " +
                    "WHERE dbo.RingGo_Members.Member_Email = '" + userName + "';";

            stmt = connect.createStatement();
            stmt.executeUpdate(query);
        }
        connect.close();
    }
}
