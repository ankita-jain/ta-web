package TestScenario;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCDisableNoNResidentPermitApplicationPage;
import PageObjects.WCCDisablePermitPage;
import PageObjects.WCCDisablePermitProofPage;
import PageObjects.WCCDisableResidentPermitApplicationPage;
import PageObjects.WCCDisableResidentPermitDetailsPage;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;

/*
 * Disable resident permit - Blue Badge - Child under 2 years  of age
 * This test can run for all Disable resident permit with Blue badge, 
 * the only difference is the 'WCCDisableResidentPermitDetailsPage' fields changes with respect to the 'Disability type' 
 * */

public class WCCNoNResidentDisablePermitTest extends RingGoScenario {
	
	private String email;
	private String password;
  	
@Test(dataProvider="wcc_non_resident_disable_permit", dataProviderClass=DataProviders.WCCNonResidentDisablePermitDP.class, groups={"disable_non_resident_disable_permit"})
public void nonresidentDisablePermitApplication(String TCID, Map<Object, Object> Data) throws IOException
{
	StartTest(TCID,"WCC- Resident Disable Permit - White Badge");
	
	try
	{
		NavigationHelper.openWestMinster(_driver);
		email = Data.get("email").toString();
		password =  Data.get("password").toString();
		
		WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
		CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");
		
		WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
		String pageTitle=wccloginpage.getPageTitle();
		assertTrue("Home Page title",pageTitle.contains("Log in"));
		
		CheckResult(wccloginpage.inputEmailOrPhonenumer(email), "Enter email or phone number");
		CheckResult(wccloginpage.enterPassword(password),  "Enter password");
		CheckResult(wccloginpage.clickLoginButton(), "Click login button");
				
		WCCHome wcchome = new WCCHome(_driver);
		String homePageTitle=wcchome.getHomePageTitle();
		assertTrue(homePageTitle.contains("Welcome"));
		wcchome.clickPermit();
		
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
		String permitPageTitle=wccpermitpage.getPermitPageTitle();
		assertTrue(permitPageTitle.contains("Permits"));
		CheckResult(wccpermitpage.clickDisablePermitTile(), "Click Disable permit button");
		
		
		WCCDisablePermitPage wccdisablepermitpage = new WCCDisablePermitPage(_driver);
		wccdisablepermitpage.clickNoNResidentDisablePermit();
		
		WCCDisableNoNResidentPermitApplicationPage wccdisablenonresidentpermitapplicationpage = new WCCDisableNoNResidentPermitApplicationPage(_driver);
		wccdisablenonresidentpermitapplicationpage.fillApplicantDetails(Data.get("Title").toString(), 
																		Data.get("Firstname").toString(),
																		Data.get("Surname").toString(), 
																		Data.get("Address_line").toString(), 
																		Data.get("Town").toString(), 
																		Data.get("County").toString(), 
																		Data.get("Postcode").toString(),
																		Data.get("Gender").toString(), 
																		Data.get("Telephone_number").toString(), 
																		Data.get("National_insurance").toString(), 
																		Data.get("dob_date").toString(),
																		Data.get("dob_month").toString(),
																		Data.get("dob_year").toString(),
																		Data.get("Reason_for_permit").toString(), 
																		Data.get("Disability_type").toString(), 
																		Data.get("Willing_for_mob_assessement").toString(), 
																		Data.get("VRM").toString(), 
																		Data.get("Driver_or_passenger").toString(), 
																		Data.get("Vehicle_keeper_name").toString(), 
																		Data.get("Vehicle_keeper_address_line_1").toString(),
																		Data.get("Vehicle_keeper_town").toString(), 
																		Data.get("Vehicle_keeper_county").toString(),
																		Data.get("Vehicle_keeper_postcode").toString());
		
		/*WCCDisableResidentPermitDetailsPage wccdisableresidentpermitdetailspage = new WCCDisableResidentPermitDetailsPage(_driver);
		wccdisableresidentpermitdetailspage.fillDisabilityDetailsOfChildUnder2Yrs(does_child_need_medical_equipment, medical_equipment, does_child_need_vehicle_near, medical_condition);
			
		WCCDisablePermitProofPage wccdisablepermitproofpage = new WCCDisablePermitProofPage(_driver);
		wccdisablepermitproofpage.uploadProofsBlueBadge(proof_of_add_type_one, proof_of_add1, proof_of_add_type_two, proof_of_add2, proof_of_eligibility, other_proof_types, other_proofs);
		
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		wccpermitpurchasefinishpage.clickFinish();*/
	}
	
	catch(AssertionError e)
    {

		LogError("Failed due -> " +e.getMessage());
		Log.add(LogStatus.FAIL, "Test is failed");
		Assert.fail("Failed as verification failed -'" +e.getMessage());		
    }
}
	
}
