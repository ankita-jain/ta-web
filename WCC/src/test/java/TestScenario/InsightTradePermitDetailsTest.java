package TestScenario;

import DataProviders.PermitDP;
import GenericComponents.RingGoScenario;
import GenericComponents.UtilityClass;
import GenericComponents.helper.InsightHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Insight.*;

import com.relevantcodes.extentreports.LogStatus;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Map;

import static GenericComponents.BaseClass.dateEngine;
import static Utilities.DateUtils.convertDateFormatToAnotherFormat;
import static Utilities.FileUtils.writeToTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class InsightTradePermitDetailsTest extends RingGoScenario {
    UtilityClass utilityclass = new UtilityClass();

    @BeforeMethod
    public void navigateToInsight() throws IOException {
    	NavigationHelper.openInsight(_driver);
    }

    @Test(dataProvider = "wcc_Permits_Portal", dataProviderClass = PermitDP.class, groups = {"trade_permit_details_via_insight"}, dependsOnGroups = {"trade_permit"})
    public void checkTradePermitApplicationDetails(String TCID, Map<Object, Object> data) throws IOException, ParseException {
        StartTest("367", "WCC- Trade permit details check insight");

        try {
            String permit_number;
            LoginHelper.loginInsight(data.get("email").toString(),data.get("password").toString(), _driver);

            InsightSearch insightsearch = new InsightSearch(_driver);
            insightsearch.selectOperatorProcess(data.get("operator_status").toString(), data.get("operator").toString());

            InsightDefaultOperatorPage insightdefaultoperatorpage = new InsightDefaultOperatorPage(_driver);
            assertTrue(insightdefaultoperatorpage.getPageHeader().contains("Parking Zones"));

            InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
            insightservicemenupage.loadPermitSearchForm();

            InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
            String insightwcchomepagetitle = insightwcchome.getPageTitle();

            assertTrue(insightwcchomepagetitle.contains("Permit Applications"));
            permit_number = InsightHelper.searchPermitAndEdit(data.get("vrn").toString(), data.get("permit_status").toString(), _driver);

            /*
             * Write permit_number(PermitId) to a local storage for further use.
             *
             */

            writeToTestDataFile(permit_number, RESIDENT_TRADE_PERMIT_TXT_FILE);

            InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
            String insightpermitapplicationedipagetitle = insightpermitapplicationpage.getPageTitle();
            assertTrue(insightpermitapplicationedipagetitle.contains("Edit a Trades Permit Application"));

            assertTrue("Permit application start date", convertDateFormatToAnotherFormat(insightpermitapplicationpage.getPermitValidFromDate(), "dd MMM yyyy", "dd/MM/yyyy").contains(dateEngine("today")));
            assertTrue("Permit application end date", convertDateFormatToAnotherFormat(insightpermitapplicationpage.getPermitValidToDate(), "dd MMM yyyy", "dd/MM/yyyy").contains(dateEngine("week")));

            insightpermitapplicationpage.editPermitStatusToTempPermitAndSave(data.get("permit_status").toString(), data.get("permit_status").toString());

            InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
            assertTrue(insightpermitworkflowqueuepage.getPageTitle().contains("Queue"));

            InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
            insightnavmenu.clickAccount();

            InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
            insightlogoutpage.clickLogout();
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(getClass().getName())).findFirst().ifPresent(System.out::println);
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }
    }
}
