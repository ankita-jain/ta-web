package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.AutoPayContracts;
import PageObjects.myringo.AutoPayRegisterContract;
import PageObjects.myringo.HomePageMyRinggo;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateTheSameAutoPayContract extends RingGoScenario {
    private String emailFirstUser;
    private String passwordFirstUser;
    private String defaultCarVRN;
    private String emailSecondUser;
    private String passwordSecondUser;

    @BeforeMethod
    public void createUserAndCars() throws Exception {
        createUser("islingtonNewUser.json");
        emailFirstUser = email;
        passwordFirstUser = password;
        defaultCarVRN = ((String) ((HashMap) ((ArrayList) newUserJson.get("Vehicles")).get(0)).get("VRM")).toUpperCase();

        createUser("islingtonNewUser.json");
        emailSecondUser = email;
        passwordSecondUser = password;

        
        Map<Object,Object> defaultVehicle = RestApiHelper.defaultNewVehicle();
        defaultVehicle.put("VRM", defaultCarVRN);
        RestApiHelper.addNewVehicle(emailSecondUser, passwordSecondUser, defaultVehicle);
    }


    /***
     * disabled, because currently we are able to select only ONE operator. It is a known issue
     */
    @Test(enabled = false)
    public void makingAutoPayContractWithAnotherOperator() {
        AutoPayRegisterContract.Operator firstOperator = AutoPayRegisterContract.Operator.CHILTERN_RAILWAYS;
        AutoPayRegisterContract.Operator secondOperator = AutoPayRegisterContract.Operator.WEST_MINSTER_CITY_COUNCI;

        StartTest("8022", "Making the same autopay contract on a different account");

        LogStep("Step - 1", "On the MyRingGo site, log in to an account which has a VRM on it which is already linked to an autopay contract");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(emailFirstUser, passwordFirstUser, _driver);

        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        AutoPayContracts autoPayContracts = new AutoPayContracts(_driver);
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        AutoPayRegisterContract autoPayRegisterContract = new AutoPayRegisterContract(_driver);
        CheckResult(autoPayRegisterContract.chooseOperator(firstOperator.toString()), String.format("Select Operator -> %s", firstOperator.toString()));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));

        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().clickAccount(), "Click account");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().clickOnLogoutLink(), "Click logout");
        LoginHelper.loginMyRingo(emailSecondUser, passwordSecondUser, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'Auto Pay'");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        LogStep("Step - 3", "Click 'add contract'");
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        LogStep("Step - 4", "Select the same operator and VRM from the contact on your other account. Click register");
        CheckResult(autoPayRegisterContract.chooseOperator(secondOperator.toString()), String.format("Select Operator -> %s", secondOperator.toString()));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));
        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");
        CheckBool(autoPayContracts.getContractsCount() == 1, "Check contract adding");
    }

    @Test
    public void makingTheSameAutoPayContract() {
        AutoPayRegisterContract.Operator operator = AutoPayRegisterContract.Operator.CHILTERN_RAILWAYS;

        StartTest("20519", "Making the same autopay contract on a different account with the same operator");

        LogStep("Step - 1", "On the MyRingGo site, log in to an account which has a VRM on it which is already linked to an autopay contract");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(emailFirstUser, passwordFirstUser, _driver);

        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        AutoPayContracts autoPayContracts = new AutoPayContracts(_driver);
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        AutoPayRegisterContract autoPayRegisterContract = new AutoPayRegisterContract(_driver);
        CheckResult(autoPayRegisterContract.chooseOperator(operator.toString()), String.format("Select Operator -> %s", operator.toString()));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));

        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().clickAccount(), "Click account");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().clickOnLogoutLink(), "Click logout");
        LoginHelper.loginMyRingo(emailSecondUser, passwordSecondUser, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'Auto Pay'");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        LogStep("Step - 3", "Click 'add contract'");
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");
        autoPayRegisterContract = new AutoPayRegisterContract(_driver);

        LogStep("Step - 4", "Select the same operator and VRM from the contact on your other account. Click register");
        CheckResult(autoPayRegisterContract.chooseOperator(operator.toString()), String.format("Select Operator -> %s", operator.toString()));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));
        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        CheckContains(autoPayContracts.getErrorNotificationText(), String.format("License plates %s already exist on an Auto Pay account for this operator", defaultCarVRN), "Check error message");
    }
}
