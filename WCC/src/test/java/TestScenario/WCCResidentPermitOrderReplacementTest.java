package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.*;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

import static Utilities.FileUtils.readFromTestDataFile;
import static org.testng.AssertJUnit.assertTrue;

public class WCCResidentPermitOrderReplacementTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCResidentPermitOrderReplacementTest.class);
    String permitid;

    @Test(dataProvider = "dp_wcc_resident_permit_perm_vrn_change", dataProviderClass = DataProviders.DP_WCC.class, groups = {"permitapplication_perm_vrn_change"})
    public void residentPermitApplication(String Username, String Password, String permanent_vrn, String Ownership_type, String Proof_type, String proof, String credit_card, String exp_month, String exp_year, String CV2) throws IOException, ParseException {
        StartTest("", "WCC- Resident Permit- Permanent VRN change status");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            assertTrue("Login Page Title Check", wccloginpage.getPageTitle().contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(Username), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(Password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            assertTrue("Welcome page title check", wcchome.getHomePageTitle().contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            assertTrue(wccpermitpage.getPermitPageTitle().contains("Permits"));
            wccpermitpage.clickSeeAllPermitFromPermitMenu();

            permitid = readFromTestDataFile(RESIDENT_PERMIT_TXT_FILE);

            WCCMyPermitPage wccmypermitpage = new WCCMyPermitPage(_driver);

            assertTrue("Verify My permit page title", wccmypermitpage.getMyPermitPageTitle().contains("My permits"));
            assertTrue("Permit status=", wccmypermitpage.getPermitStatus(permitid).contains("Authorised"));

            wccmypermitpage.clickOrderReplacement(permitid);

            WCCPermitReplacementRequestPage wccpermitreplacementrequestpage = new WCCPermitReplacementRequestPage(_driver);
            assertTrue("Request Replacement page title", wccpermitreplacementrequestpage.getReplacementRequestPagetitle().contains("Replacement permit request"));


            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
