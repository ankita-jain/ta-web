package TestScenario.hooks;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.NewCorporate.EmployeeDeleteConfirmationPage;
import PageObjects.myringo.CorporateAccountPage;
import Utilities.PropertyUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;

import java.io.IOException;

public class RebrandedCorporateSuite extends RingGoScenario {

    @AfterSuite
    public void deleteAllEmployees() throws IOException {
        String corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        String corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);

        _driver = SeleniumHelpers.WebDriverMethods.init("Chrome", DesiredCapabilities.chrome(), PropertyUtils.ReadProperty("CHROME_DRIVER_PATH", ENVIRONMENT_PROPERTIES));
        _driver.manage().window().maximize();
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);

        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");

        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        employeeDashboardPage.checkbox = new EmployeeDashboardPage.CheckBox();
        CheckResult(employeeDashboardPage.clickAllEmployeesCheckbox(), "Select All Employees");
        CheckResult(employeeDashboardPage.clickDeleteButton(), "ClickDeleteButton");

        EmployeeDeleteConfirmationPage employeeDeleteConfirmationPage = new EmployeeDeleteConfirmationPage(_driver);
        CheckResult(employeeDeleteConfirmationPage.clickYes(), "Clicking on Yes button");

        _driver.quit();
    }
}
