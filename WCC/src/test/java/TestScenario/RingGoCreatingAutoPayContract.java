package TestScenario;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.AutoPayContracts;
import PageObjects.myringo.AutoPayRegisterContract;
import PageObjects.myringo.HomePageMyRinggo;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class RingGoCreatingAutoPayContract extends RingGoScenario {
    private static final String OPERATOR = "Chiltern Railways";

    private String defaultCarVRN;
    private Map<Object, Object> user;

    @BeforeMethod
    public void createUserAndCars() throws Exception {
        createUser("islingtonNewUser.json");
        defaultCarVRN = ((String) ((HashMap) ((ArrayList) newUserJson.get("Vehicles")).get(0)).get("VRM")).toUpperCase();
    }


    @Test
    public void createAnAutoPayContract() {
        StartTest("6024", "Creating an autopay contract");

        Map<Object,Object> defaultVehicle = RestApiHelper.defaultNewVehicle();
        String vehicle1 = RestApiHelper.addNewVehicleNoVRM(email, password, defaultVehicle);
        defaultVehicle.put("Colour", "Black");
        defaultVehicle.put("Make", "Audi");
        String vehicle2 = RestApiHelper.addNewVehicleNoVRM(email, password, defaultVehicle);
        defaultVehicle.put("Colour", "Blue");
        defaultVehicle.put("Make", "Saab");
        String vehicle3 = RestApiHelper.addNewVehicleNoVRM(email, password, defaultVehicle);

        LogStep("Step - 1", "Log in to MyRingGo (http://myrgo-preprod.ctt.co.uk/)");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(email, password, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'Auto Pay'");
        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        LogStep("Step - 3", "Click 'Add Contract'");
        AutoPayContracts autoPayContracts = new AutoPayContracts(_driver);
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        LogStep("Step - 4", "Select an operator. Tick at least 4 vehicles");
        AutoPayRegisterContract autoPayRegisterContract = new AutoPayRegisterContract(_driver);
        CheckResult(autoPayRegisterContract.chooseOperator(OPERATOR), String.format("Select Operator -> %s", OPERATOR));
        CheckResult(autoPayRegisterContract.tickVehicle(vehicle1), String.format("Tick Vehicle -> %s", vehicle1));
        CheckResult(autoPayRegisterContract.tickVehicle(vehicle2), String.format("Tick Vehicle -> %s", vehicle2));
        CheckResult(autoPayRegisterContract.tickVehicle(vehicle3), String.format("Tick Vehicle -> %s", vehicle3));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));

        LogStep("Step - 5", "Tick that you accept the terms and conditions. Click 'register'");
        CheckResult(autoPayRegisterContract.tickTermsAndConditions(), "Tick Terms And Conditions");
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        LogStep("Step - 6", "On the list of vehicles hover over the green 'and XX more' text");
        CheckResult(autoPayContracts.showMoreVehicles(1), "Click 'and XX more' link");
        CheckBool(Arrays.asList(vehicle1, vehicle2, vehicle3, defaultCarVRN).stream().anyMatch(x -> autoPayContracts.getVehiclesTooltipText().contains(x.toUpperCase())),
                "More vehicles tooltip should contain ticked vehicles");
    }

    @Test
    public void tryToCreateAutoPayContractWithoutAcceptingTermsAndConditions() {
        StartTest("20505", "Creating an autopay contract without accepting 'Terms and Conditions'");

        Map<Object,Object> defaultVehicle = RestApiHelper.defaultNewVehicle();
        String vehicle1 = RestApiHelper.addNewVehicleNoVRM(email, password, defaultVehicle);
        defaultVehicle.put("Colour", "Black");
        defaultVehicle.put("Make", "Audi");
        String vehicle2 = RestApiHelper.addNewVehicleNoVRM(email, password, defaultVehicle);
        defaultVehicle.put("Colour", "Blue");
        defaultVehicle.put("Make", "Saab");
        String vehicle3 = RestApiHelper.addNewVehicleNoVRM(email, password, defaultVehicle);

        LogStep("Step - 1", "Log in to MyRingGo (http://myrgo-preprod.ctt.co.uk/)");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingo(email, password, _driver);

        LogStep("Step - 2", "Go to 'Account' > 'Auto Pay'");
        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().openAutoPayPage(), "Click Auto Pay link");

        LogStep("Step - 3", "Click 'Add Contract'");
        AutoPayContracts autoPayContracts = new AutoPayContracts(_driver);
        CheckResult(autoPayContracts.addContract(), "Click Add Contract link");

        LogStep("Step - 4", "Select an operator. Tick at least 4 vehicles. Without ticking the terms and conditions box, click 'register'");
        AutoPayRegisterContract autoPayRegisterContract = new AutoPayRegisterContract(_driver);
        CheckResult(autoPayRegisterContract.chooseOperator(OPERATOR), String.format("Select Operator -> %s", OPERATOR));
        CheckResult(autoPayRegisterContract.tickVehicle(vehicle1), String.format("Tick Vehicle -> %s", vehicle1));
        CheckResult(autoPayRegisterContract.tickVehicle(vehicle2), String.format("Tick Vehicle -> %s", vehicle2));
        CheckResult(autoPayRegisterContract.tickVehicle(vehicle3), String.format("Tick Vehicle -> %s", vehicle3));
        CheckResult(autoPayRegisterContract.tickVehicle(defaultCarVRN), String.format("Tick Vehicle -> %s", defaultCarVRN));
        CheckResult(autoPayRegisterContract.register(), "Click Register button");

        CheckContains(autoPayRegisterContract.getTermsAndConditionsErrorText(), "Please confirm that you agree to the Terms and Conditions", "Check Message text");
    }
}
