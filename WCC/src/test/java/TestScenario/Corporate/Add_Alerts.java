package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Add_Alerts extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Add_Alerts.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void addAlertTest(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Setup > Alerts & C394 - Verify admin user recieves every time someone parks when Setup - Alerts - Change Email Me setting to 'Every Time Someone Parks'");
        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email").toString(), data.get("password").toString(), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue(corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickSetUp(), "Clicking on Set up");

            WCCSetupPage wccsetuppage = new WCCSetupPage(_driver);
            String setupPageTitle = wccsetuppage.getSetUpPageTitle();
            assertTrue(setupPageTitle.contains("Setup"));
            CheckResult(wccsetuppage.clickAlerts(), "Clicking on Alerts");

            WCCCorporateAlertsPage wcccorporatealertspage = new WCCCorporateAlertsPage(_driver);
            wcccorporatealertspage.enterMiniumBalanceForAlert(data.get("minimum_balance").toString());
            wcccorporatealertspage.selectParkingNotification(data.get("alert").toString());
            wcccorporatealertspage.clickSave();
            assertTrue(wcccorporatealertspage.getAlertSaveMessage().contains("Alert settings have been updated"));

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
