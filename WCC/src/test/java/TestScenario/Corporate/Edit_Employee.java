package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static Utilities.TimerUtils.delay;

import java.util.Map;

public class Edit_Employee extends RingGoScenario {
    private String email;
    private String password;
    private String name;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void editEmployee(String TCID, Map<Object, Object> Data) {
        StartTest(TCID, "Verify Home > Corporate Home > Setup >Employees > Edit employee");

        email = Data.get("email").toString();
        password = Data.get("password").toString();
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        name = "F" + RandomStringUtils.randomAlphabetic(6);
        String secondName = "S" + RandomStringUtils.randomAlphabetic(7);
        String emailId = RandomStringUtils.randomAlphabetic(5) + "@ctt.com";

        CorporateHelper.addNewEmployee(name, secondName, emailId, _driver);

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        CheckContains(wccCorporateEmployeesPage.getEmployeePageTitle(), "Employees", "Employees page title");
        CheckResult(wccCorporateEmployeesPage.enterName(name),String.format("Search delete employee -> %s", name));
        CheckResult(wccCorporateEmployeesPage.search(),"Click search button");
        
        delay(3000);
        name = name.substring(0, 1).toUpperCase() + name.substring(1); // Change frist letter of the name to captial letter
        CheckResult(wccCorporateEmployeesPage.editEmployee(name), String.format("Edit employee -> %s", name));

        String cli = "07" + RandomStringUtils.randomNumeric(9);
        String vrn = RandomStringUtils.randomAlphanumeric(7);

        WCCCorporateAddEmployeePage wccCorporateAddEmployeePage = new WCCCorporateAddEmployeePage(_driver);
        CheckContains(wccCorporateAddEmployeePage.getAddEmployeePageTitle(), "Edit Employee", "Edit Employee page title");

        CheckResult(wccCorporateAddEmployeePage.enterPhoneNumber(cli), String.format("Enter cli -> %s", cli));
        CheckResult(wccCorporateAddEmployeePage.addCli(), "Click add cli");
        CheckContains(wccCorporateAddEmployeePage.getCLIMessage(), "Phone number does not exist on your account.", "Error message text");

        CheckResult(wccCorporateAddEmployeePage.enterVehicle(vrn), String.format("Enter vehicle -> %s", vrn));
        CheckResult(wccCorporateAddEmployeePage.addVrn(), "Add vrn");
        CheckContains(wccCorporateAddEmployeePage.getVRNMessage(), "Vehicle does not exist on your account.", "Error Message text");

        CheckResult(wccCorporateAddEmployeePage.enterFirstName(name + "ed"), String.format("Enter name -> %s", name + "ed"));
        CheckResult(wccCorporateAddEmployeePage.enterSurname(secondName + "ed"), String.format("Enter surname -> %s", "ed"));
        CheckResult(wccCorporateAddEmployeePage.enterEmail(emailId), String.format("Enter email -> %s", emailId));
        CheckResult(wccCorporateAddEmployeePage.clickSave(), "Click save");
                
        CheckContains(wccCorporateEmployeesPage.getSuccessNotification(), "Employee " + name+" " + secondName + " was successfully edited",
                "Success message");
    }

    @AfterMethod
    public void deleteEmployee() {
        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");

        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        CheckResult(wccSetupPage.clickEmployees(), "Clicking on Employees");

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        name = name.substring(0, 1).toUpperCase() + name.substring(1); // Change first letter of the name to capital letter
        CheckResult(wccCorporateEmployeesPage.enterName(name + "ed"),String.format("Search delete employee -> %s", name));
        CheckResult(wccCorporateEmployeesPage.search(),"Click search button");
        
        delay(3000);
        
        CheckResult(wccCorporateEmployeesPage.clickEmployeeDeleteButton(name + "ed"), String.format("Click delete employee -> %s", name));

        WCCCorporateDeleteConfirmationPage wcccorporatedeleteconfirmationpage = new WCCCorporateDeleteConfirmationPage(_driver);
        CheckResult(wcccorporatedeleteconfirmationpage.clickDeleteConfirmButton(), "Click on Delete Confirm button");
    }
}
