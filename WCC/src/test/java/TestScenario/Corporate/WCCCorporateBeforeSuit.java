package TestScenario.Corporate;

import DataProviderHelpers.DBHelper;
import DataProviderHelpers.DatabaseProperties;
import GenericComponents.ExcelReadWrite;
import GenericComponents.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.annotations.BeforeSuite;

import java.sql.Connection;
import java.sql.Statement;

public class WCCCorporateBeforeSuit {

    @BeforeSuite
    public void ClearAdminUsers() throws Exception {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE_CORPORATE"));
        HSSFSheet WCC_CORPORATE_ADMIN = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_CORPORATE_ACCOUNT"));
        int RowCount = xl.getrowcount(WCC_CORPORATE_ADMIN);
        Connection connect;
        connect = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String companyName = xl.Readvalue(WCC_CORPORATE_ADMIN, xlRow, "Company Name");
            Statement stmt = null;
            String query = "DELETE dbo.CorpAdminUsers FROM dbo.CorpAdminUsers " +
                    "INNER JOIN dbo.CorpAccount " +
                    "ON dbo.CorpAdminUsers.AccountID = dbo.CorpAccount.ID " +
                    "WHERE dbo.CorpAccount.AccountName = '" + companyName +
                    "' AND dbo.CorpAdminUsers.Master != 1;";

            stmt = connect.createStatement();
            stmt.executeUpdate(query);
        }
        connect.close();
    }


    @BeforeSuite
    public void ClearCostCentres() throws Exception {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE_CORPORATE"));
        HSSFSheet WCC_CORPORATE_ADMIN_COST_CENTRE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_CORPORATE_COST_CENTRE"));
        int RowCount = xl.getrowcount(WCC_CORPORATE_ADMIN_COST_CENTRE);
        Connection connect;
        connect = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String companyName = xl.Readvalue(WCC_CORPORATE_ADMIN_COST_CENTRE, xlRow, "Company Name");
            Statement stmt = null;
            String query = "DELETE dbo.CorpCostCentres FROM dbo.CorpCostCentres " +
                    "INNER JOIN dbo.CorpAccount " +
                    "ON dbo.CorpCostCentres.AccountID = dbo.CorpAccount.ID " +
                    "WHERE dbo.CorpAccount.AccountName = '" + companyName +
                    "' AND dbo.CorpCostCentres.CostCentreName != 'My Cost Centre';";

            stmt = connect.createStatement();
            stmt.executeUpdate(query);
        }
        connect.close();
    }

    @BeforeSuite
    public void ClearTopUpCards() throws Exception {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE_CORPORATE"));
        HSSFSheet WCC_CORPORATE_ADD_CREDIT_CARD = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_CREDIT_CARD_EDIT"));
        int RowCount = xl.getrowcount(WCC_CORPORATE_ADD_CREDIT_CARD);
        Connection connect;
        connect = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String companyName = xl.Readvalue(WCC_CORPORATE_ADD_CREDIT_CARD, xlRow, "Company Name");
            String cardNumber = xl.Readvalue(WCC_CORPORATE_ADD_CREDIT_CARD, xlRow, "Card_number");
            Statement stmt = null;
            String query = "DELETE dbo.CorpTopUpCards FROM dbo.CorpTopUpCards " +
                    "INNER JOIN dbo.CorpAccount " +
                    "ON dbo.CorpTopUpCards.corporateID = dbo.CorpAccount.ID " +
                    "WHERE dbo.CorpAccount.AccountName = '" + companyName + "' AND " +
                    "dbo.CorpTopUpCards.CardEnds = '" + cardNumber.substring(cardNumber.length() - 4) + "';";

            stmt = connect.createStatement();
            stmt.executeUpdate(query);
        }
        connect.close();
    }

    @BeforeSuite
    public void ClearEmployees() throws Exception {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE_CORPORATE"));
        HSSFSheet WCC_CORPORATE_ADD_EMPLOYEE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_CORPORATE_ADD_EMPLOYEE"));
        int RowCount = xl.getrowcount(WCC_CORPORATE_ADD_EMPLOYEE);
        Connection connect;
        connect = DBHelper.connectToMSSQLServer(DatabaseProperties.Address, DatabaseProperties.DbName);

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String firstName = xl.Readvalue(WCC_CORPORATE_ADD_EMPLOYEE, xlRow, "First_name");
            Statement stmt = null;
            String query = "DELETE FROM dbo.CorpEmployees " +
                    "WHERE dbo.CorpEmployees.FirstName = '" + firstName + "';";

            stmt = connect.createStatement();
            stmt.executeUpdate(query);
        }
        connect.close();
    }
}
