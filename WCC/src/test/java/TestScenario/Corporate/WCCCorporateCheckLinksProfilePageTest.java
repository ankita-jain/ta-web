package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

public class WCCCorporateCheckLinksProfilePageTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateCheckLinksProfilePageTest.class);
    String corp_email, corp_password;

    @Test
    public void checkAllLinksProfilePage() throws IOException {
        StartTest("", "Checking corporate links");

        try {
            NavigationHelper.openWestMinster(_driver);
            
            corp_email = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
            corp_password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
            
            LoginHelper.loginCorporateWestminster(corp_email, corp_password, _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickTopup(), "Clicking on Top Up");

            WCCCorporateTopupPage wcccorporatetopuppage = new WCCCorporateTopupPage(_driver);
            assertTrue(wcccorporatetopuppage.getTopupPageTitle().contains("Funds"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickChange(), "Clicking on Change");

            WCCCorporateAlertsPage wcccorporatealertspage = new WCCCorporateAlertsPage(_driver);
            assertTrue(wcccorporatealertspage.getPagetTitle().contains("Alerts"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickEditEmployees(), "Clicking on Edit Employees");

            WCCCorporateEmployeesPage wcccorporateemployeespage = new WCCCorporateEmployeesPage(_driver);
            assertTrue(wcccorporateemployeespage.getEmployeePageTitle().contains("Employees"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickRequestCallBack(), "Clicking on Request Callback");

            WCCCorporateRequestCallBackPage wcccorporaterequestcallbackpage = new WCCCorporateRequestCallBackPage(_driver);
            assertTrue(wcccorporaterequestcallbackpage.getRequestPageTitle().contains("Want to call us?"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickSendEmployeesPIN(), "Clicking on Send Employees PIN");

            WCCCorporateEmployeePINPage wcccorporateemployeepinpage = new WCCCorporateEmployeePINPage(_driver);
            assertTrue(wcccorporateemployeepinpage.getEmployeePINPageTitle().contains("Employee PIN"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickContactUs(), "Clicking on Contact us");

            WCCCorporateContactUsPage wcccorporatecontactuspage = new WCCCorporateContactUsPage(_driver);
            assertTrue(wcccorporatecontactuspage.getContactUsPageTitle().contains("Contact Us"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCostCentres(), "Clicking on Cost Centres");

            WCCCorporateCostCenterPage wcccorporatecostcentrepage = new WCCCorporateCostCenterPage(_driver);
            assertTrue(wcccorporatecostcentrepage.getCostCenterPageTitle().contains("Cost Centres"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCostCentresAdd(), "Clicking on Cost Centres Add link");

            WCCCorporateAddCostCentrePage wcccorporateaddcostcentrepage = new WCCCorporateAddCostCentrePage(_driver);
            assertTrue(wcccorporateaddcostcentrepage.getCostCentrePageTitle().contains("Add Corporate Cost Centre"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickAdministrators(), "Clicking on Administrators");

            WccCorporateAdministrator wcccorporateadministrator = new WccCorporateAdministrator(_driver);
            assertTrue(wcccorporateadministrator.getCorportaeAdministratorPageTitle().contains("Corporate Administrators"));

            CheckResult(wcccoporateprofilepage.clickGoBackToPreviousPage(), "Clicking on Back to previous page");
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickAdministratorsAdd(), "Clicking on Administrators Add link");

            WCCCorporateAdminAddPage wcccorporateadminaddpage = new WCCCorporateAdminAddPage(_driver);
            assertTrue("Corporate Admin Add Page Title check", wcccorporateadminaddpage.getPageTitle().contains("Add Corporate Administrator"));

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
