package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class Cancel_Delete_Administrators extends RingGoScenario {
    private String adminEmail;
    private String email;
    private String password;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void deleteAdminCancelTest(String TCID, Map<Object, Object> Data) {
        StartTest(TCID, "Verify Home > Corporate Home > Setup > Cancel Delete Administrators");

        email = Data.get("email").toString();
        password = Data.get("password").toString();
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckContains(wccCorporateProfilePage.getCorporateProfilePageTitle(), "Welcome to your RingGo Corporate Account",
                "Checking Corporate Account page title");
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        String corporateHomePageTitle = wccCorporateHomePage.getCoporateHomePageTitle();
        CheckContains(corporateHomePageTitle, "Corporate Account", "Checking Home page title");
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        String setupPageTitle = wccSetupPage.getSetUpPageTitle();
        CheckContains(setupPageTitle, "Setup", "Checking Setup page title");
        CheckResult(wccSetupPage.clickAdministrator(), "Clicking on Administrator");

        WccCorporateAdministrator wccCorporateAdministrator = new WccCorporateAdministrator(_driver);
        String corporateAdminPageTitle = wccCorporateAdministrator.getCorportaeAdministratorPageTitle();
        CheckContains(corporateAdminPageTitle, "Corporate Administrators", "Checking Admin page title");

        CheckResult(wccCorporateAdministrator.clickAddAdminButton(), "Clicking on Add Administrator button");

        adminEmail = randomAlphabetic(6) + "@gmail.com";
        String firstName = "FirstName" + randomAlphabetic(6);
        String surname = "Surname" + randomAlphabetic(6);
        String password = "1Password" + randomAlphabetic(6);
        CorporateHelper.addNewAdminProcess(adminEmail, firstName, surname, password, _driver);

        wccCorporateAdministrator.clickAdminDelete(adminEmail);

        WCCCorporateDeleteConfirmationPage wccCorporateDeleteConfirmationPage = new WCCCorporateDeleteConfirmationPage(_driver);
        CheckContains(wccCorporateDeleteConfirmationPage.getPageTitle(), "Confirm Deletion", "Checking Confirmation Deletion page title");
        CheckResult(wccCorporateDeleteConfirmationPage.clickDeleteCancelButton(), "Click on Cancel Delete button");

        CheckContains(wccCorporateAdministrator.getNoticeMessage(), "Delete action cancelled", "Checking message text");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }

    @AfterMethod
    public void deleteAdminMethod() {
        CorporateHelper.deleteCorporateAdministratorMethod(email, password, adminEmail, _driver);
    }
}
