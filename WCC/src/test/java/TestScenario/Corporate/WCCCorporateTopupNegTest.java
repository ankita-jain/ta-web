package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class WCCCorporateTopupNegTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void checkingErrorMessage(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Top Up with Credit/Debit card - Negative Test");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue(corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

            WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
            assertTrue(wcccorporatefundpage.getFundPageTitle().contains("Funds"));
            wcccorporatefundpage.clickTopup();

            WCCCorporateTopupPage wcccorporatetopuppage = new WCCCorporateTopupPage(_driver);
            assertTrue(wcccorporatetopuppage.getTopupPageTitle().contains("Funds"));
            CheckResult(wcccorporatetopuppage.enterTopupAmountField(data.get("top_amount")), "Enter top up amount");
            CheckResult(wcccorporatetopuppage.selectTopupPaymentMethod(data.get("payment_method")), "Select top up method");
            CheckResult(wcccorporatetopuppage.clickNextButton(), "Click next button");

            String amount = wcccorporatetopuppage.getTopupLimitOnUserAccount().replaceAll("[-+.^:,]", ""); //Remove all special characters
            String err_message = data.get("error_message") + amount;

            assertTrue("Checking error messages", wcccorporatetopuppage.getErrorMessage().contains(err_message));


            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
