package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

public class Parking_History extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Parking_History.class);
    String corp_email, corp_password;
    @Test
    public void corporateParkingHistory() throws IOException {
        StartTest("415", "Verify Home > Corporate Home > Reports > Parking History");


        try {
        	NavigationHelper.openWestMinster(_driver);
            
            corp_email = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
    	    corp_password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
    	    
            LoginHelper.loginCorporateWestminster(corp_email, corp_password, _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickReports(), "Clicking on Reports");

            WCCCorporateReportsPage wcccorporatereportpage = new WCCCorporateReportsPage(_driver);
            assertTrue(wcccorporatereportpage.getReportPageTitle().contains("Reports"));
            wcccorporatereportpage.clickParkingHistory();

            WCCCorporateParkingTransactionsHistoryPage wcccorporateparkingtransactionhistorypage = new WCCCorporateParkingTransactionsHistoryPage(_driver);
            assertTrue("Checking Parking History page title", wcccorporateparkingtransactionhistorypage.getParkingTransactionHistoryPageTitle().contains("Parking Transactions History"));
            wcccorporateparkingtransactionhistorypage.searchParkingTransactionHistory();
            /*
             * Given the user is in the 'Parking Transaction page',
             * When he changes the filters values and hit 'Search' button,
             * Then result should be shown in the table
             * And if the result table is not visible check whether 'No record found' message displayed.
             * */

            if (!(wcccorporateparkingtransactionhistorypage.isDataTableVisible() && wcccorporateparkingtransactionhistorypage.isSearchResultCountVisible())) {
                assertTrue("Checking message 'No record found'", wcccorporateparkingtransactionhistorypage.getEmptySearchResult().contains("No records found."));
                LogPass("Parking_history");
            } else {

                LogPass("Parking_history");

            }

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
