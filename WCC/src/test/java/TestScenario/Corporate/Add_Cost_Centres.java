package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;


public class Add_Cost_Centres extends RingGoScenario {
    private String costCenterName;
    private String login;
    private String password;
    private String costCenterNumber;
    private String costCenterOwner;

    @Test
    public void addCostCentreTest() throws IOException {
        login = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
        costCenterName = RandomStringUtils.randomAlphabetic(6);
        costCenterNumber = RandomStringUtils.randomNumeric(6);
        costCenterOwner = RandomStringUtils.randomAlphabetic(6);

        StartTest("421", "Verify Home > Corporate Home > Setup > Cost Centres");

        LogStep("Step - 1", "Log into corporate account and navigate to Home > Corporate Home >Setup > Cost Centres");
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(login, password, _driver);

        LogStep("Step - 2", "Click on Add a new cost centre link to add the new Corporate cost centre and Save");
        CorporateHelper.addCostCenter(costCenterName, costCenterNumber, costCenterOwner, _driver);

        WCCCorporateCostCenterPage wccCorporateCostCenterPage = new WCCCorporateCostCenterPage(_driver);
        CheckContains(wccCorporateCostCenterPage.getCostCentreSuccessNotification(), "Cost Centre was successfully added", "Checking corporate add cost centre page message");
    }

    @AfterMethod
    public void deleteCostCenter() {
        Log.add(LogStatus.INFO, "After method - Delete cost center");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");

        LoginHelper.loginCorporateWestminster(login, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        CheckResult(wccSetupPage.clickCostCentres(), "Clicking on Cost Centres");

        WCCCorporateCostCenterPage wccCorporateCostCenterPage = new WCCCorporateCostCenterPage(_driver);
        CheckResult(wccCorporateCostCenterPage.deleteCostCenter(costCenterName), String.format("Delete cost center -> %s", costCenterName));

        WCCCorporateCostCentreDeleteConfirmationPage wccCorporateCostCentreDeleteConfirmationPage = new WCCCorporateCostCentreDeleteConfirmationPage(_driver);
        CheckResult(wccCorporateCostCentreDeleteConfirmationPage.clickYes(), String.format("Delete cost center -> %s", costCenterName));

    }
}
