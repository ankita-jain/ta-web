package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Apply_Trade_Permit extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Apply_Trade_Permit.class);
    String oldTab;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void applyTradePermit(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Permits > Corporate Permit Applications > Apply Trade Permit");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickPermit(), "Clicking on Permit");

            WCCCorporatePermitPage wcccorporatepermitpage = new WCCCorporatePermitPage(_driver);
            assertTrue("Corporate permit page title", wcccorporatepermitpage.getCorporatePageTitle().contains("Corporate Permit Applications for Westminster City Council"));
            wcccorporatepermitpage.clickTradePermit();

            WCCCorporateTradePermitPage wcccorporatetradepermit = new WCCCorporateTradePermitPage(_driver);
            assertTrue("Checking Trade permit page title", wcccorporatetradepermit.getTradePermitPageTitle().contains("Trades Permit Application"));
            wcccorporatetradepermit.applyTradePermit(data.get("employee_name"),
            										 data.get("employee_mob"),
            										 data.get("employee_email"),
            										 data.get("employee_email"),
            										 data.get("location"),
            										 data.get("reason_for_trade"),
            										 data.get("vehicle"),
            										 data.get("duration"));

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
