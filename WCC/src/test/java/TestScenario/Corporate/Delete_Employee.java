package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateAddEmployeePage;
import PageObjects.Corporate.WCCCorporateDeleteConfirmationPage;
import PageObjects.Corporate.WCCCorporateEmployeesPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import static Utilities.TimerUtils.delay;

import java.util.Map;

public class Delete_Employee extends RingGoScenario {
    private String email;
    private String password;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void deleteEmployee(String TCID, Map<Object, Object> Data) {
        StartTest(TCID, "Verify Home > Corporate Home > Setup >Employees > Delete employee");

        email = Data.get("email").toString();
        password = Data.get("password").toString();
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        String name = "F" + RandomStringUtils.randomAlphabetic(6);
        String secondName = "S" + RandomStringUtils.randomAlphabetic(7);
        String emailId = RandomStringUtils.randomAlphabetic(5) + "@ctt.com";

        CorporateHelper.addNewEmployee(name, secondName, emailId, _driver);

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        CheckContains(wccCorporateEmployeesPage.getEmployeePageTitle(), "Employees", "Employees page title");
        CheckResult(wccCorporateEmployeesPage.enterName(name),String.format("Search delete employee -> %s", name));
        CheckResult(wccCorporateEmployeesPage.search(),"Click search button");
        
        delay(3000);
        name = name.substring(0, 1).toUpperCase() + name.substring(1); // Change first letter of the name to capital letter
        CheckResult(wccCorporateEmployeesPage.clickEmployeeDeleteButton(name), String.format("Click delete employee -> %s", name));

        WCCCorporateDeleteConfirmationPage wcccorporatedeleteconfirmationpage = new WCCCorporateDeleteConfirmationPage(_driver);
        CheckContains(wcccorporatedeleteconfirmationpage.getPageTitle(), "Confirm Deletion", "Confirm Delete page title");
        CheckResult(wcccorporatedeleteconfirmationpage.clickDeleteConfirmButton(), "Click on Delete Confirm button");

        String successNotificationMessage = "Employee " + WCCCorporateAddEmployeePage.Title.MR.toString() + " " +
                name + " " + secondName + " was successfully deleted";
        CheckContains(wccCorporateEmployeesPage.getSuccessNotification(), successNotificationMessage, "Notification message");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }

}
