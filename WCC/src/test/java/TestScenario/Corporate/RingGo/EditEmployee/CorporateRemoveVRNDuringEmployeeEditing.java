package TestScenario.Corporate.RingGo.EditEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateAddEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

public class CorporateRemoveVRNDuringEmployeeEditing extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeePhone;
    private String employeeSurname;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeePhone = RandomStringUtils.randomNumeric(11);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
    }


    @Test
    public void removeSpecifiedVRNDuringEmployeeEditingTest() {

        StartTest("103650", "Remove specified VRN during Employee adding");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");

        LogStep("Step - 3", "Choose any Employee and Click on kebab menu and Click on 'Edit' menu item.");
        CheckResult(corporateEmployeesPage.clickOnKebabMenu(employeeName).clickOnEditItem(employeeName), "Click on Edit Employee");
        CorporateEditEmployeePage editEmployeePage = new CorporateEditEmployeePage(_driver);
        CheckValue(editEmployeePage.getPageTitleText(), "Edit Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 4", "Click 'Specified' near 'Vehicles employee can book parking with' field.");
        CheckResult(editEmployeePage.clickOnSpecifiedVrn(), "Click on Specified VRN button");
        enterSeveralVrn(editEmployeePage, 3);
        CheckBool(!editEmployeePage.getSpecifiedVrnsText().isEmpty(), "Verify if VRNs are added");

        LogStep("Step - 5", "Remove one VRN using 'X' button.");
        int allVrnAmount = editEmployeePage.getSpecifiedVrnsText().size();
        Random random = new Random();
        String vrn = editEmployeePage.getSpecifiedVrnsText().get(random.nextInt(allVrnAmount));
        CheckResult(editEmployeePage.removeSpecifiedVrn(vrn), "Remove specified vrn");
        CheckValue(editEmployeePage.getSpecifiedVrnsText().size(), allVrnAmount - 1, "Verified all clis size");
        CheckBool(!editEmployeePage.getSpecifiedVrnsText().contains(vrn), "Verifiy if cli is removed");
    }

    private void enterSeveralVrn(CorporateAddEditEmployeePage corporateAddEditEmployeePage, int vrnAmount) {
        for (int i = 0; i < vrnAmount; i++) {
            CheckResult(corporateAddEditEmployeePage.enterVrn(RandomStringUtils.randomAlphanumeric(9)), "Enter Employee CLI");
            CheckResult(corporateAddEditEmployeePage.clickOnAddVrn(), "Click on Add CLI");
        }
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
