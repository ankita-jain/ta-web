package TestScenario.Corporate.RingGo.EditEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateVerifyTooltipsOnEditEmployeePage extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeePhone;
    private String employeeSurname;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeePhone = RandomStringUtils.randomNumeric(11);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
    }

    @Test
    public void verifyToolTipsOnEditEmployeePage() {

        StartTest("103601", "Verify tooltips on Edit Employee page");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");

        LogStep("Step - 3", "Choose any Employee and Click on kebab menu and Click on 'Edit' menu item.");
        CheckResult(corporateEmployeesPage.clickOnKebabMenu(employeeName).clickOnEditItem(employeeName), "Click on Edit Employee");
        CorporateEditEmployeePage editEmployeePage = new CorporateEditEmployeePage(_driver);
        CheckValue(editEmployeePage.getPageTitleText(), "Edit Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 4", "Hover over 'Phone number' field");
        CheckValue(editEmployeePage.getCliTooltipText(), "'Any Number' enables this employee to pay for parking from any mobile number, as long as they use a vehicle from the list below.", "Verify CLI ToolTip Text");

        LogStep("Step - 5", "Hover over 'Vehicles employee can book parking with' field");
        CheckValue(editEmployeePage.getVrnTooltipText(), "'Any Vehicle' allows this employee to pay for parking from any vehicle, as long as they use a telephone from the list above.", "Verify VRN ToolTip Text");

        LogStep("Step - 6", "Hover over 'Can employee book parking?'");
        CheckValue(editEmployeePage.getCanParkTooltipText(), "This employee will NOT be able to park unless this is set to yes.", "Verify Can Employee Park Tooltip Text");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
