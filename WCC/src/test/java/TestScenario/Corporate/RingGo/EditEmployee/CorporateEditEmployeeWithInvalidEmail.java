package TestScenario.Corporate.RingGo.EditEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateEditEmployeeWithInvalidEmail extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeePhone;
    private String employeeSurname;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeePhone = RandomStringUtils.randomNumeric(11);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
    }

    @Test
    public void editEmployeeWithInvalidEmailTest() {

        StartTest("103648", "Edit Employee by entering invalid email format");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");

        LogStep("Step - 3", "Choose any Employee and Click on kebab menu and Click on 'Edit' menu item.");
        CheckResult(corporateEmployeesPage.clickOnKebabMenu(employeeName).clickOnEditItem(employeeName), "Click on Edit Employee");

        LogStep("Step - 4", "Enter Email in incorrect format and click on 'Save changes' button");
        CorporateEditEmployeePage editEmployeePage = new CorporateEditEmployeePage(_driver);
        CheckValue(editEmployeePage.getPageTitleText(), "Edit Employee", "Verify RingGo Corporate Add Employee page title");
        CheckResult(editEmployeePage.enterEmail(RandomStringUtils.randomAlphabetic(10)), "Enter Employee Invalid Email");
        CheckResult(editEmployeePage.clickOnSaveEmployee(), "Click on Add Employee");
        CheckValue(editEmployeePage.getEmailErrorMessageText(), "Email is an invalid format", "Verify Email Error Message");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}

