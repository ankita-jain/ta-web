package TestScenario.Corporate.RingGo.EditEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;

public class CorporateVerifyAnyCLIAndAnyVRNForEmployeeEditing extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeePhone;
    private String employeeSurname;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeePhone = RandomStringUtils.randomNumeric(11);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
    }

    public void chooseAnyCLIOrAnyVRNTest() {

        StartTest("103607", "Verify 'Any CLI' and 'Any VRN' options for Employee Editing");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");

        LogStep("Step - 3", "Choose any Employee and Click on kebab menu and Click on 'Edit' menu item.");
        CheckResult(corporateEmployeesPage.clickOnKebabMenu(employeeName).clickOnEditItem(employeeName), "Click on Edit Employee");
        CorporateEditEmployeePage editEmployeePage = new CorporateEditEmployeePage(_driver);
        CheckValue(editEmployeePage.getPageTitleText(), "Edit Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 4", "Select 'Any' Phone number option.");
        CheckResult(editEmployeePage.clickOnAnyCliButton(), "Click on 'Any' Phone number option");
        CheckBool(editEmployeePage.isAnyVrnButtonDisable(), "Verify if 'Any' VRN option is not clickable.");

        LogStep("Step - 5", "Hover on 'Any' VRN");
        CheckValue(editEmployeePage.getAnyVrnTooltipText(), "Cannot select any phone number and any vehicle at the same time, please amend the selection to enable this setting.", "Verify if correct tooltip is displayed");

        LogStep("Step - 6", "Click on 'Specified' phone number option.");
        CheckResult(editEmployeePage.clickOnSpecifiedCli(), "Click on 'Specified' Phone number");
        CheckBool(!editEmployeePage.isAnyVrnButtonDisable(), "Verify if 'Any' VRN option is clickable.");

        LogStep("Step - 7", "Click on 'Any' VRN option.");
        CheckResult(editEmployeePage.clickOnAnyVrn(), "Click on 'Any' VRN option");
        CheckBool(editEmployeePage.isAnyCliDisable(), "Verify if 'Any' Phone number option is not clickable.");

        LogStep("Step - 8", "Hover on 'Any' Phone number");
        CheckValue(editEmployeePage.getAnyCliTooltipText(), "Cannot select any phone number and any vehicle at the same time, please amend the selection to enable this setting.", "Verify if correct tooltip is displayed");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
