package TestScenario.Corporate.RingGo.EditEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateEditEmployeePositiveTest extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private String employeePhone;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeePhone = RandomStringUtils.randomNumeric(11);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
    }

    @Test
    public void editEmployeeTest() {

        StartTest("103603", "Edit Employee positive test");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");

        LogStep("Step - 3", "Choose any Employee and Click on kebab menu and Click on 'Edit' menu item.");
        CheckResult(corporateEmployeesPage.clickOnKebabMenu(employeeName).clickOnEditItem(employeeName), "Click on Edit Employee");

        LogStep("Step - 4", "Fill Valid data for Employee creation.");
        CorporateEditEmployeePage corporateEditEmployeePage = new CorporateEditEmployeePage(_driver);
        CheckValue(corporateEditEmployeePage.getPageTitleText(), "Edit Employee", "Verify RingGo Corporate Add Employee page title");
        String updatedName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        String updatedSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        CheckResult(corporateEditEmployeePage.enterFirstName(updatedName), "Enter Employee First Name");
        CheckResult(corporateEditEmployeePage.enterSurname(updatedSurname), "Enter Employee Surname");

        LogStep("Step - 5", "Click on 'Save changes' button.");
        CheckResult(corporateEditEmployeePage.clickOnSaveEmployee(), "Click on Add Employee");
        CheckBool(corporateEmployeesPage.isEmployeeRecordPresentByName(updatedName.concat(" ").concat(updatedSurname)),
                "Verify if New Employee is added to the table by Name");
        CheckValue(corporateEmployeesPage.getNotificationText(), String.format("Employee %s %s was successfully edited", employeeName, employeeSurname), "Verify Notification Text");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
