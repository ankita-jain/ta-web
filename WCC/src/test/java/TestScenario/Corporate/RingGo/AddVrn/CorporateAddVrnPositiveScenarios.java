package TestScenario.Corporate.RingGo.AddVrn;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.AddEditVrnPopup;
import PageObjects.Corporate.RingGo.modules.AddVrnPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateAddVrnPositiveScenarios extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;


    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8);
    }

    @Test
    public void verifyAddVrn() {
        StartTest("107610", "Add VRN");

        navigateToAddVrnPopup();

        LogStep("Step - 6", "Enter Number plate, select colour, type and make. Click Save");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
        CheckResult(addVrnPopup.enterVrnNumber(vrnNumber), "Enter Vehicle Number");
        String type = addEditVrnPopup.getVrnType();
        String make = addEditVrnPopup.getVrnMake();
        String colour = addEditVrnPopup.getVrnColour();
        CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make");
        CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckBool(vehiclePage.isVrnPresent(vrnNumber), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void verifyAddVrnWithOnlyNumberPlate() {
        StartTest("107730", "Add VRN with only Number plate set");

        navigateToAddVrnPopup();

        LogStep("Step - 6", "Enter Number plate. Click Save");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
        CheckResult(addVrnPopup.enterVrnNumber(vrnNumber), "Enter Vehicle Number");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckBool(vehiclePage.isVrnPresent(vrnNumber), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void verifyCancelAddVrn() {
        StartTest("C107729", "Cancel adding VRN");

        navigateToAddVrnPopup();

        LogStep("Step - 6", "Enter Number plate, select colour, type and make. Click Save");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
        CheckResult(addVrnPopup.enterVrnNumber(vrnNumber), "Enter Vehicle Number");
        String type = addEditVrnPopup.getVrnType();
        String make = addEditVrnPopup.getVrnMake();
        String colour = addEditVrnPopup.getVrnColour();
        CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make");
        CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour");
        CheckResult(addEditVrnPopup.clickOnCancelButton(), "Click on Add Vehicle Button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckBool(!vehiclePage.isVrnPresent(vrnNumber), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    private void navigateToAddVrnPopup() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");

        LogStep("Step - 5", "Click Add Vehicle");
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
    }
}

