package TestScenario.Corporate.RingGo.AddVrn;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.AddEditVrnPopup;
import PageObjects.Corporate.RingGo.modules.AddVrnPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Optional;

public class CorporateAddVrnWithNegativeScenarios extends RingGoScenario {

    private String corporateEmail1, corporateEmail2;
    private String corporatePassword1, corporatePassword2;
    private String vrnNumber;

    @BeforeClass
    public void loginData() throws IOException {
        corporateEmail1 = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporateEmail2 = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword1 = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        corporatePassword2 = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8);
    }

    @Test(dataProvider = "invalidVrns")
    public void verifyAddVrnWithInvalidNumberPlate(String vrnNumber) {
        StartTest("107728", "Add VRN with incorrect length for Number plate");

        navigateToAddVrnPopup();

        LogStep("Step - 6", "Enter Number plate from Test Data, select colour, type and make. Click Save");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
        CheckResult(addVrnPopup.enterVrnNumber(vrnNumber), "Enter Vehicle Number");
        String type = addEditVrnPopup.getVrnType();
        String make = addEditVrnPopup.getVrnMake();
        String colour = addEditVrnPopup.getVrnColour();
        CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make");
        CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CheckValue(addVrnPopup.getNumberPlateError(), "Invalid number plate format", "Verify Error Message if invalid Number Plate is entered");

        CheckResult(addEditVrnPopup.clickOnCloseButton(), "Click On Close Button");
        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void verifyAddVrnWithoutNumberPlate() {
        StartTest("107611", "Add VRN without Number plate");

        navigateToAddVrnPopup();

        LogStep("Step - 6", "Select colour, type and make. Click Save");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
        String type = addEditVrnPopup.getVrnType();
        String make = addEditVrnPopup.getVrnMake();
        String colour = addEditVrnPopup.getVrnColour();
        CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make");
        CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CheckValue(addVrnPopup.getNumberPlateError(), "Number plate is required", "Verify Error Message if empty Number Plate is entered");

        CheckResult(addEditVrnPopup.clickOnCloseButton(), "Click On Close Button");
        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void addVrnFromAnotherCorporateAccount() {
        StartTest("107727", "Add VRN which is added to another corporate account");

        navigateToAddVrnPopup(corporateEmail1, corporatePassword1);
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);

        navigateToAddVrnPopup(corporateEmail2, corporatePassword2);
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);


        LogStep("Step - 6", "Enter Number plate of vehicle added to another corporate account. Click Save");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
        CheckValue(addVrnPopup.getNumberPlateError(), "Vehicle registration number is already linked to another corporate account",
                "Verify Error Message if vrn is used in another account");
        CheckResult(addEditVrnPopup.clickOnCloseButton(), "Click On Close Button");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @DataProvider
    public Object[][] invalidVrns() {
        return new Object[][]{{RandomStringUtils.randomAlphanumeric(2)}, {RandomStringUtils.randomAlphanumeric(10)}};
    }

    private void navigateToAddVrnPopup() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail1), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword1), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");

        LogStep("Step - 5", "Click Add Vehicle");
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
    }


    private void navigateToAddVrnPopup(String email, String password) {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(email), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(password), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");

        LogStep("Step - 5", "Click Add Vehicle");
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
    }
}


