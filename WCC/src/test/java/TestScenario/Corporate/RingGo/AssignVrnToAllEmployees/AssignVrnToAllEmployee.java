package TestScenario.Corporate.RingGo.AssignVrnToAllEmployees;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static SeleniumHelpers.JavaScriptHelpers.acceptAlert;
import static SeleniumHelpers.JavaScriptHelpers.dismissAlert;
import static Utilities.TimerUtils.delay;

public class AssignVrnToAllEmployee extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;
    private String employeeName1;
    private String employeeSurname1;
    private String employeeName2;
    private String employeeSurname2;
    private String employeePhone;


    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        employeePhone = RandomStringUtils.randomNumeric(11);
        employeeName1 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname1 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeName2 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname2 = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
    }

    @Test
    public void assignVrnToAllEmployees() {
        StartTest("107748", "Assign VRN to all Employees");

        loginToCorporate();
        addVrnWithEmployee();
        navigateToVehiclesPage();

        LogStep("Step - 5", "Click on Assign to all Employees in kebab menu");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickOnKebabMenu(vrnNumber), "Click on Kebab Menu");
        CheckResult(vehiclePage.clickAssignToAllEmployeesButton(vrnNumber), "Click on 'Assign to all employees' button");

        LogStep("Step - 6", "Click Ok on confirmation message");
        CheckResult(acceptAlert(_driver, "Are you sure you wish to assign this vehicle to all employees?"), "Accept alert");
        CheckValue(vehiclePage.successNotification.getText(), String.format("%s has been successfully assigned to all employees", vrnNumber), "Verify notification text");
        String employeeFullName1 = employeeName1.concat(" ").concat(employeeSurname1);
        String employeeFullName2 = employeeName2.concat(" ").concat(employeeSurname2);
        CheckBool(vehiclePage.isVrnAssignedToEmployee(vrnNumber, employeeFullName1), "Verify if VRN is Added");
        CheckBool(vehiclePage.isVrnAssignedToEmployee(vrnNumber, employeeFullName2), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void cancelAssignVrnToAllEmployees() {
        StartTest("107749", "Cancel assigning VRN to all Employees");

        loginToCorporate();
        addVrnWithEmployee();
        navigateToVehiclesPage();

        LogStep("Step - 5", "Click on Assign to all Employees in kebab menu");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickOnKebabMenu(vrnNumber), "Click on Kebab Menu");
        CheckResult(vehiclePage.clickAssignToAllEmployeesButton(vrnNumber), "Click on 'Assign to all employees' button");

        LogStep("Step - 6", "Click Cancel on confirmation message");
        CheckResult(dismissAlert(_driver, "Are you sure you wish to assign this vehicle to all employees?"), "Cancel alert");
        String employeeFullName1 = employeeName1.concat(" ").concat(employeeSurname1);
        String employeeFullName2 = employeeName2.concat(" ").concat(employeeSurname2);
        CheckBool(!vehiclePage.isVrnAssignedToEmployee(vrnNumber, employeeFullName1), "Verify if VRN is Added");
        CheckBool(vehiclePage.isVrnAssignedToEmployee(vrnNumber, employeeFullName2), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }


    @Test
    public void assignVrnWithoutAnyEmployeeToAllEmployees() {
        StartTest("107750", "Assign VRN which is not associated to any Employee to All Employees");

        loginToCorporate();
        addVrnWithEmployee();
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);
        delay(10000);
        navigateToVehiclesPage();

        LogStep("Step - 5", "Click on Assign to all Employees in kebab menu");
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckResult(vehiclePage.clickOnKebabMenu(vrnNumber), "Click on Kebab Menu");
        CheckResult(vehiclePage.clickAssignToAllEmployeesButton(vrnNumber), "Click on 'Assign to all employees' button");

        LogStep("Step - 6", "Click Ok on confirmation message");
        CheckResult(acceptAlert(_driver, "Are you sure you wish to assign this vehicle to all employees?"), "Accept alert");
        CheckValue(vehiclePage.successNotification.getText(), String.format("%s has been successfully assigned to all employees", vrnNumber), "Verify notification text");
        CheckResult(vehiclePage.clickAssignedTab(), "ClickAssigned Tab");
        String employeeFullName1 = employeeName1.concat(" ").concat(employeeSurname1);
        String employeeFullName2 = employeeName2.concat(" ").concat(employeeSurname2);
        CheckBool(vehiclePage.isVrnAssignedToEmployee(vrnNumber, employeeFullName1), "Verify if VRN is Added");
        CheckBool(vehiclePage.isVrnAssignedToEmployee(vrnNumber, employeeFullName2), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    private void loginToCorporate() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");
    }

    private void navigateToVehiclesPage() {
        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
    }

    private void addVrnWithEmployee() {
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName1, employeeSurname1, employeePhone, Collections.singletonList(employeePhone), Collections.singletonList(RandomStringUtils.randomAlphabetic(8)), true, _driver);
        CorporateHelper.addEmployee(employeeName2, employeeSurname2, "", Collections.singletonList(employeePhone), Collections.singletonList(vrnNumber), true, _driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
