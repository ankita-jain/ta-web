package TestScenario.Corporate.RingGo.RemoveFromAllEmployees;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.Alert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class RemoveVrmFromAllEmployees extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private String reference;
    private List<String> employeePhone;
    private List<String> employeeVrn;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);

        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone = Arrays.asList(new String[]{RandomStringUtils.randomNumeric(11)});
        employeeVrn = Arrays.asList(new String[]{RandomStringUtils.randomAlphanumeric(7).toUpperCase()});
        reference = RandomStringUtils.randomAlphabetic(10);
    }

    @Test
    public void removeVrnFromAllEmployees() {
        StartTest("107754", "Remove VRN from all Employees");

        goToVehicles();

        LogStep("Step - 5", "Click on Remove from all Employees in kebab menu");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickOnKebabMenu(employeeVrn.get(0)), "Verify click on kebab menu");
        CheckResult(vehiclePage.clickOnRemoveFromAllEmployees(employeeVrn.get(0)), "Verify click on 'Remove From All Employees' link");

        LogStep("Step - 6", "Click on OK button");
        Alert javascriptAlert = _driver.switchTo().alert();
        CheckValue(javascriptAlert.getText(), String.format("Removing the vehicle from all employees may mean some employees cannot park " +
                        "(if they do not have another vehicle assigned)\n" +
                        "Are you sure you wish to remove %s from all employees?", employeeVrn.get(0)),
                "Checking confirmation message");
        javascriptAlert.accept();

        CheckResult(vehiclePage.clickUnassignedTab(), "Checking click on Unassigned tab");
        CheckValue(vehiclePage.vrnTabel.getRowByCellValue(employeeVrn.get(0), CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE).isDisplayed(),
                true, "Checking Vehicle is displayed");

        LogStep("Step - 7", "Go to Manage > Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        int rowIndex = employeeDashboardPage.employeeTable.getRowIndexByCellValue(reference,
                EmployeeDashboardPage.EmployeeTableColumns.REFERENCE);
        CheckValue(employeeDashboardPage.employeeTable.getCell(rowIndex, EmployeeDashboardPage.EmployeeTableColumns.VEHICLES.getPosition()).getText(),
                "",
                "Checking Vehicles column");
      }

    @Test
    public void removeSelectedVrnFromAllEmployees() {
        StartTest("107756", "Remove VRN from all Employees");

        goToVehicles();

        LogStep("Step - 5", "Click on checkbox for added vehicle");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.selectCheckbox(employeeVrn.get(0)), "Checking click on check box");

        LogStep("Step - 6", "Click on Remove from all Employees button");
        CheckResult(vehiclePage.clickOnRemoveFromAllEmployeesButton(), "Clicking on Remove From All Employees button");

        LogStep("Step - 7", "Click on OK button");
        Alert javascriptAlert = _driver.switchTo().alert();
        CheckValue(javascriptAlert.getText(), "Removing the selected vehicle(s) from all employees may mean " +
                        "some employees cannot park (if they do not have another vehicle assigned)\n" +
                        "Are you sure you wish to remove the selected vehicle(s) from all employees?",
                "Checking confirmation message");
        javascriptAlert.accept();

        CheckResult(vehiclePage.clickUnassignedTab(), "Checking click on Unassigned tab");
        CheckValue(vehiclePage.vrnTabel.getRowByCellValue(employeeVrn.get(0), CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE).isDisplayed(),
                true, "Checking Vehicle is displayed");

        LogStep("Step - 8", "Go to Manage > Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
        int rowIndex = employeeDashboardPage.employeeTable.getRowIndexByCellValue(reference,
                EmployeeDashboardPage.EmployeeTableColumns.REFERENCE);
        CheckValue(employeeDashboardPage.employeeTable.getCell(rowIndex, EmployeeDashboardPage.EmployeeTableColumns.VEHICLES.getPosition()).getText(),
                "",
                "Checking Vehicles column");
    }

    private void goToVehicles(){
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);

        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, reference, employeePhone, employeeVrn, true, _driver);

        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");

    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
