package TestScenario.Corporate.RingGo.EditVrn;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.AddEditVrnPopup;
import PageObjects.Corporate.RingGo.modules.EditVrnPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Optional;

public class CorporateEditVrnTest extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;

    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
    }

    @Test
    public void editVrn() {
        StartTest("107731", "Edit VRN");

        navigateToVehiclesPage();
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        String type = addEditVrnPopup.getVrnType();
        String make = addEditVrnPopup.getVrnMake();
        String colour = addEditVrnPopup.getVrnColour();
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);

        LogStep("Step - 5", "Click edit for added vehicle");
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckResult(vehiclePage.clickOnEditVrn(vrnNumber), "Verify click on 'Edit vehicle' link");
        EditVrnPopup editVrnPopup = new EditVrnPopup(_driver);
        CheckBool(!editVrnPopup.isVrnNumberFieldEditable(), "Verify if VRN Number field is not editable");

        LogStep("Step - 6", "Change type or make and click Save");
        CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make");
        CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CheckBool(vehiclePage.isVrnPresent(vrnNumber, type, make, colour), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void editVrnEmployeeAssigning() {
        StartTest("107746", "Edit VRN which do not have employee assigned");

        navigateToVehiclesPage();
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);
        String employeePhone = RandomStringUtils.randomNumeric(9);
        String employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        String employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");

        LogStep("Step - 5", "Click edit for added vehicle");
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckResult(vehiclePage.clickOnEditVrn(vrnNumber), "Verify click on 'Edit vehicle' link");
        EditVrnPopup editVrnPopup = new EditVrnPopup(_driver);
        CheckBool(!editVrnPopup.isVrnNumberFieldEditable(), "Verify if VRN Number field is not editable");

        LogStep("Step - 6", "Add employee to vehicle and click Save");
        String employeeFullName = employeeName.concat(" ").concat(employeeSurname);
        CheckResult(editVrnPopup.enterEmployee(employeeFullName), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CheckResult(vehiclePage.clickAssignedTab(), "ClickAssigned Tab");
        CheckBool(vehiclePage.isVrnPresent(vrnNumber, employeeFullName), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void cancelVrn() {
        StartTest("107735", "Cancel editing VRN");

        navigateToVehiclesPage();
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        String type = addEditVrnPopup.getVrnType();
        String make = addEditVrnPopup.getVrnMake();
        String colour = addEditVrnPopup.getVrnColour();
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);

        LogStep("Step - 5", "Click edit for added vehicle");
        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckResult(vehiclePage.clickOnEditVrn(vrnNumber), "Verify click on 'Edit vehicle' link");
        EditVrnPopup editVrnPopup = new EditVrnPopup(_driver);
        CheckBool(!editVrnPopup.isVrnNumberFieldEditable(), "Verify if VRN Number field is not editable");

        LogStep("Step - 6", "Change make and click Cancel");
        CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make");
        CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour");
        CheckResult(addEditVrnPopup.clickOnCancelButton(), "Click on Add Vehicle Button");
        CheckBool(!vehiclePage.isVrnPresent(vrnNumber, type, make, colour), "Verify if VRN is Added");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    private void navigateToVehiclesPage() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
    }
}
