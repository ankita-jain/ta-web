package TestScenario.Corporate.RingGo.VrnTable;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CorporateVrnTableSorting extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber1;
    private String vrnNumber2;
    private String vrnNumber3;

    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber1 = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        vrnNumber2 = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        vrnNumber3 = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
    }

    @Test
    public void verifySortingByVrnNumber() {
        StartTest("106026", "VRN Table sorting");

        navigateToVehiclesPage();

        LogStep("Step - 5", "Click on arrow next to Number plate");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
        CheckResult(vehiclePage.clickOnNumberPlate(), "Click on 'Number plate' to verify from A ro Z sorting");
        List<List<WebElement>> rows = vehiclePage.vrnTabel.getRows();
        List<String> vrns = rows.stream().map(row -> row.get(CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE.getPosition()).getText()).collect(Collectors.toList());
        List<String> vrnsSorted = vrns.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());

        CheckBool(vrns.equals(vrnsSorted), "Checking sorting by 'Number plate'");

        LogStep("Step - 6", "Click on arrow next to Number plate one more time");
        CheckResult(vehiclePage.clickOnNumberPlate(), "Click on 'Number plate' to verify from Z ro A sorting");
        rows = vehiclePage.vrnTabel.getRows();
        vrns = rows.stream().map(row -> row.get(CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE.getPosition()).getText()).collect(Collectors.toList());
        vrnsSorted = vrns.stream().sorted().collect(Collectors.toList());

        CheckBool(vrns.equals(vrnsSorted), "Checking sorting by 'Number plate'");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }


    private void navigateToVehiclesPage() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        addEmployeeWithVrns();

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
    }

    private void addEmployeeWithVrns() {
        String employeePhone = RandomStringUtils.randomNumeric(11);
        String employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        String employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateHelper.addEmployee(employeeName, employeeSurname, "", Collections.singletonList(employeePhone), Arrays.asList(vrnNumber1, vrnNumber2, vrnNumber3), true, _driver);
    }
}
