package TestScenario.Corporate.RingGo.VrnTable;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.VehiclesFilterPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateVehicleTableFilterByColourMake extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;
    private String vrnNumber2;

    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        vrnNumber2 = RandomStringUtils.randomAlphanumeric(8).toUpperCase();

        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");

    }

    @Test
    public void vehicleTableFilterByMake() {
        StartTest("124733", "Vehicles table filter by make");

        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CorporateHelper.addVehicle(vrnNumber, "Audi", "Car", "Black", _driver);
        CorporateHelper.addVehicle(vrnNumber2, "BMW", "Bus", "White", _driver);
        CheckResult(vehiclePage.clickUnassignedTab(), "Clicking on Unassigned tab");
        CheckResult(vehiclePage.clickOnFilter(), "Clicking on Filter button");

        VehiclesFilterPopup vehiclesFilterPopup = new VehiclesFilterPopup(_driver);
        CheckResult(vehiclesFilterPopup.clickMake(), "Clicking on Make tab");
        CheckResult(vehiclesFilterPopup.selectMake("Audi"), "Select Make");
        CheckResult(vehiclesFilterPopup.clickOnApplyButton(), "Clicking on Apply button");

        CheckValue(vehiclePage.vrnTabel.IsRowWithValueExists(vrnNumber, CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE),
                true, "Checking if row is displayed");
        CheckValue(vehiclePage.vrnTabel.IsRowWithValueExists(vrnNumber2, CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE),
                false, "Checking if row is displayed");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void vehicleTableFilterByColour() {
        StartTest("124734", "Vehicles table filter by colour");

        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CorporateHelper.addVehicle(vrnNumber, "Audi", "Car", "Black", _driver);
        CorporateHelper.addVehicle(vrnNumber2, "BMW", "Bus", "White", _driver);
        CheckResult(vehiclePage.clickUnassignedTab(), "Clicking on Unassigned tab");
        CheckResult(vehiclePage.clickOnFilter(), "Clicking on Filter button");

        VehiclesFilterPopup vehiclesFilterPopup = new VehiclesFilterPopup(_driver);
        CheckResult(vehiclesFilterPopup.clickColour(), "Clicking on Colour tab");
        CheckResult(vehiclesFilterPopup.selectColour("Black"), "Select Colour");
        CheckResult(vehiclesFilterPopup.clickOnApplyButton(), "Clicking on Apply button");

        CheckValue(vehiclePage.vrnTabel.IsRowWithValueExists(vrnNumber, CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE),
                true, "Checking if row is displayed");
        CheckValue(vehiclePage.vrnTabel.IsRowWithValueExists(vrnNumber2, CorporateVehiclePage.VehiclesTableColumns.NUMBER_PLATE),
                false, "Checking if row is displayed");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }


}
