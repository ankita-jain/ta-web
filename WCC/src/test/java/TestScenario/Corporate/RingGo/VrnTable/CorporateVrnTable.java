package TestScenario.Corporate.RingGo.VrnTable;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.AddEditVrnPopup;
import PageObjects.Corporate.RingGo.modules.EditVrnPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Optional;

import static Utilities.TimerUtils.delay;

public class CorporateVrnTable extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;
    private String employeeFullName;

    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
    }

    @Test
    public void verifyRowSelection() {
        StartTest("106027", "VRN Table row selection");

        navigateToVehiclesPage();
        addVrnWithEmployee();

        LogStep("Step - 5", "Click on checkbox for any row in the table");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.selectCheckbox(vrnNumber), "Select vrn");
        CheckBool(!vehiclePage.addVrnButton.isDisplayed(), "'Add Vehicle' button is hidden");
        CheckBool(!vehiclePage.filterVrnButton.isDisplayed(), "'Filter' button is hidden");
        CheckBool(vehiclePage.deleteButton.isDisplayed(), "'Delete' button is displayed");
        CheckBool(vehiclePage.assignToAllEmployeeButton.isDisplayed(), "'Assign to all employees' button is displayed");
        CheckBool(vehiclePage.removeFromAllEmployeeButton.isDisplayed(), "'Remove from all employees' button is displayed");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void verifyKebabMenu() {
        StartTest("106028", "VRN Table kebab menu");

        navigateToVehiclesPage();
        addVrnWithEmployee();

        LogStep("Step - 5", "Click on kebab menu for any vehicle");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);

        WebElement vrnRow = vehiclePage.getVrnRow(vrnNumber);
        vehiclePage.kebabMenuLink = new CorporateVehiclePage.KebabMenuLink(vrnRow);
        CheckResult( vehiclePage.kebabMenuLink.clickOnKebabMenu() , "Click on Kebab Menu");

        vehiclePage.kebabMenu = new CorporateVehiclePage.KebabMenu(vrnRow);
        CheckBool(vehiclePage.kebabMenu.editLink.isDisplayed(), "'Edit' link is present in Kebab Menu");
        CheckBool(vehiclePage.kebabMenu.deleteLink.isDisplayed(), "'Delete' link is present in Kebab Menu");
        CheckBool(vehiclePage.kebabMenu.assignToAllEmployeeLink.isDisplayed(), "'Assign to all Employees' link is present in Kebab Menu");
        CheckBool(vehiclePage.kebabMenu.removeFromAllEmployeeLink.isDisplayed(), "'Remove from all Employees' link is present in Kebab Menu");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    @Test
    public void verifyVrnTableLayout() {
        StartTest("106042", "VRN Table layout");

        navigateToVehiclesPage();
        addVrnWithEmployee();

        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckBool(vehiclePage.addVrnButton.isDisplayed(), "'Add Vehicle' button is hidden");
        CheckBool(vehiclePage.filterVrnButton.isDisplayed(), "'Filter' button is hidden");
        CheckBool(vehiclePage.isVrnTableContainsAllColumns(), "Verify if all columns are present in Vehicle table");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    private void navigateToVehiclesPage() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
    }

    private void addVrnWithEmployee() {
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);
        String employeePhone = RandomStringUtils.randomNumeric(11);
        delay(10000);
        String employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        String employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");

        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckResult(vehiclePage.clickOnEditVrn(vrnNumber), "Verify click on 'Edit vehicle' link");
        EditVrnPopup editVrnPopup = new EditVrnPopup(_driver);
        CheckBool(!editVrnPopup.isVrnNumberFieldEditable(), "Verify if VRN Number field is not editable");

        employeeFullName = employeeName.concat(" ").concat(employeeSurname);
        CheckResult(editVrnPopup.enterEmployee(employeeFullName), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CheckResult(vehiclePage.clickAssignedTab(), "ClickAssigned Tab");
    }
}
