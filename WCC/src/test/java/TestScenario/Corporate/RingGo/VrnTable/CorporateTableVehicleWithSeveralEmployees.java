package TestScenario.Corporate.RingGo.VrnTable;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.EmployeesPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CorporateTableVehicleWithSeveralEmployees extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;
    private String employeeFullName;
    private List<String> employeeNames;

    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
        employeeNames = new ArrayList<>();
    }

    @Test
    public void verifyVehicleWithSeveralEmployees() {
        StartTest("107049", "Vehicles with several employees assigned");

        navigateToCorporateHomePage();
        addVrnForSeveralEmployees();

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");

        LogStep("Step - 5", "Click on '...' link");
        CheckResult(vehiclePage.clickOnGetMoreEmployees(vrnNumber), "Click on get more employees for added VRN");
        EmployeesPopup employeesPopup = new EmployeesPopup(_driver);
        CheckBool(employeesPopup.isVrnAssignedToAppropriateEmployees(employeeNames), "Verify is VRN is assigned for all neccessary ");
        CheckResult(employeesPopup.clickOnCloseButton(), "Close 'Employees' popup");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    private void navigateToCorporateHomePage() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");
    }

    private void addVrnForSeveralEmployees() {
        for (int i = 0; i < 4; i++) {
            String employeePhone = RandomStringUtils.randomNumeric(11);
            String employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
            String employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
            if (i == 0) {
                CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
                CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
            }
            CorporateHelper.addEmployee(employeeName, employeeSurname, "", Collections.singletonList(employeePhone), Collections.singletonList(vrnNumber), true, _driver);
            employeeFullName = employeeName.concat(" ").concat(employeeSurname);
            employeeNames.add(employeeFullName);
        }
    }
}
