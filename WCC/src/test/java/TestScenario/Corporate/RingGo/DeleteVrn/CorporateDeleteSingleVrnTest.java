package TestScenario.Corporate.RingGo.DeleteVrn;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.AddEditVrnPopup;
import PageObjects.Corporate.RingGo.modules.EditVrnPopup;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Optional;

import static SeleniumHelpers.JavaScriptHelpers.acceptAlert;
import static SeleniumHelpers.JavaScriptHelpers.dismissAlert;
import static Utilities.TimerUtils.delay;

public class CorporateDeleteSingleVrnTest extends RingGoScenario {
    private String corporateEmail;
    private String corporatePassword;
    private String vrnNumber;
    private String employeeFullName;

    @BeforeClass
    public void readLoginData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @BeforeMethod
    public void prepareData() {
        vrnNumber = RandomStringUtils.randomAlphanumeric(8).toUpperCase();
    }

    @Test
    public void deleteVrn() {
        StartTest("107737", "Delete VRN");

        navigateToVehiclesPage();
        addVrnWithEmployee();

        LogStep("Step - 5", "Click on Delete in kebab menu for added VRN");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickOnKebabMenu(vrnNumber), "Click on Kebab Menu");
        CheckResult(vehiclePage.clickOnDeleteButton(vrnNumber), "Click on Delete Button");

        LogStep("Step - 6", "Click Ok on confirmation popup");
        CheckResult(acceptAlert(_driver, String.format("The vehicle will be removed from any associated employees.  Once this action has been confirmed it cannot be undone.\n" +
                "Are you sure that you want to delete %s?", vrnNumber)), "Accept alert");
        CheckBool(!vehiclePage.isVrnPresent(vrnNumber), "Verify if VRN is Removed");

        LogStep("Step - 7", "Go to Manage > Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckBool(corporateEmployeesPage.isVrnRemovedFromEmployee(vrnNumber), "Verify if VRN is removed from all employees");
    }

    @Test
    public void cancelVrnDeleting() {
        StartTest("107738", "Cancel deleting VRN");

        navigateToVehiclesPage();
        addVrnWithEmployee();

        LogStep("Step - 5", "Click on Delete in kebab menu for added VRN");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickOnKebabMenu(vrnNumber), "Click on Kebab Menu");
        CheckResult(vehiclePage.clickOnDeleteButton(vrnNumber), "Click on Delete Button");

        LogStep("Step - 6", "Click Cancel on confirmation popup");
        CheckResult(dismissAlert(_driver, String.format("The vehicle will be removed from any associated employees.  Once this action has been confirmed it cannot be undone.\n" +
                "Are you sure that you want to delete %s?", vrnNumber)), "Cancel alert");
        CheckBool(vehiclePage.isVrnPresent(vrnNumber), "Verify if VRN is not Removed");

        LogStep("Step - 7", "Go to Manage > Employees");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckBool(corporateEmployeesPage.isEmployeeRecordPresentByName(employeeFullName, vrnNumber), "Verify if VRN is not removed employees");
    }

    private void navigateToVehiclesPage() {
        LogStep("Step - 1", "Go to MyRingGo site");
        NavigationHelper.openMyRingo(_driver);
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        LogStep("Step - 2", "Navigate to Login -> Corporate");
        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 3", "Enter login details for corporate admin");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateEmail), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corporatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");

        LogStep("Step - 4", "Navigate to Manage -> Vehicles");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckContains(vehiclePage.getCurrentTitle(), "Vehicles", "Verify RingGo Corporate Vehicles page title");
    }

    private void addVrnWithEmployee() {
        CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
        CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button");
        AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
        CorporateHelper.addVrnToCorporateAccount(vrnNumber, Optional.empty(), Optional.empty(), Optional.empty(), _driver);
        delay(10000);
        String employeePhone = RandomStringUtils.randomNumeric(11);
        String employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        String employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        CorporateHelper.addEmployee(employeeName, employeeSurname, employeePhone, _driver);
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAssets(), "Click on 'Vehicles' button");

        CheckResult(vehiclePage.clickUnassignedTab(), "Click Unassigned Tab");
        CheckResult(vehiclePage.clickOnEditVrn(vrnNumber), "Verify click on 'Edit vehicle' link");
        EditVrnPopup editVrnPopup = new EditVrnPopup(_driver);
        CheckBool(!editVrnPopup.isVrnNumberFieldEditable(), "Verify if VRN Number field is not editable");

        employeeFullName = employeeName.concat(" ").concat(employeeSurname);
        CheckResult(editVrnPopup.enterEmployee(employeeFullName), "Select Vehicle Type");
        CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button");
        CheckResult(vehiclePage.clickAssignedTab(), "ClickAssigned Tab");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
