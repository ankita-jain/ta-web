package TestScenario.Corporate.RingGo.AddEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateAddEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import PageObjects.myringo.CorporateAccountPage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateAddEmployeePositiveTest extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;
    private String employeeName;
    private String employeeSurname;
    private String employeePhone;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
        employeeName = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeeSurname = RandomStringUtils.randomAlphabetic(1).toUpperCase().concat(RandomStringUtils.randomAlphabetic(10));
        employeePhone = RandomStringUtils.randomNumeric(11);
    }

    @Test
    public void addEmployeeTest() {

        StartTest("103048", "Add Employee positive test");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees -> Add Employee.");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");
        CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee");

        LogStep("Step - 3", "Fill Valid data for Employee creation.");
        CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
        CheckValue(corporateAddEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title");
        CheckResult(corporateAddEmployeePage.enterFirstName(employeeName), "Enter Employee First Name");
        CheckResult(corporateAddEmployeePage.enterSurname(employeeSurname), "Enter Employee Surname");
        CheckResult(corporateAddEmployeePage.clickOnSpecifiedCli(), "Click on Specified CLI button");
        CheckResult(corporateAddEmployeePage.enterCli(employeePhone), "Enter Employee CLI");
        CheckResult(corporateAddEmployeePage.clickOnAddCli(), "Click on Add CLI");
        CheckResult(corporateAddEmployeePage.clickOnNoWelcomeEmail(), "Click on 'No' Send Welcome email");
        CheckResult(corporateAddEmployeePage.clickOnAnyVrn(), "Click On Any VRN");

        LogStep("Step - 4", "Click on 'Add Employee' button.");
        CheckResult(corporateAddEmployeePage.clickOnSaveEmployee(), "Click on Add Employee");
        CheckBool(corporateEmployeesPage.isEmployeeRecordPresentByName(employeeName.concat(" ").concat(employeeSurname)),
                "Verify if New Employee is added to the table by Name");
        CheckValue(corporateEmployeesPage.getNotificationText(), String.format("Employee %s %s was successfully added", employeeName, employeeSurname), "Verify Notification Text");
    }

    @AfterMethod
    public void deleteEmployee(){
        CorporateHelper.deleteAllCorporateEmployees(_driver);
        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
