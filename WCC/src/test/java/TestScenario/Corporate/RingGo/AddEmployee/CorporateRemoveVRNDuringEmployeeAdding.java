package TestScenario.Corporate.RingGo.AddEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateAddEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import PageObjects.myringo.CorporateAccountPage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Random;

public class CorporateRemoveVRNDuringEmployeeAdding extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @Test
    public void removeSpecifiedVRNDuringEmployeeAddingTest() {

        StartTest("103592", "Remove specified VRN during Employee adding");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees -> Add Employee.");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");
        CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee");
        CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
        CheckValue(corporateAddEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 3", "Click 'Specified' near 'Vehicles employee can book parking with' field.");
        CheckResult(corporateAddEmployeePage.clickOnSpecifiedVrn(), "Click on Specified VRN button");

        LogStep("Step - 4", "Enter at least two VRNs into displayed form.");
        enterSeveralVrn(corporateAddEmployeePage, 3);
        CheckBool(!corporateAddEmployeePage.getSpecifiedVrnsText().isEmpty(), "Verify if VRNs are added");

        LogStep("Step - 5", "Remove one VRN using 'X' button.");
        int allVrnAmount = corporateAddEmployeePage.getSpecifiedVrnsText().size();
        Random random = new Random();
        String vrn = corporateAddEmployeePage.getSpecifiedVrnsText().get(random.nextInt(allVrnAmount));
        CheckResult(corporateAddEmployeePage.removeSpecifiedVrn(vrn), "Remove specified vrn");
        CheckValue(corporateAddEmployeePage.getSpecifiedVrnsText().size(), allVrnAmount - 1, "Verified all clis size");
        CheckBool(!corporateAddEmployeePage.getSpecifiedVrnsText().contains(vrn), "Verifiy if cli is removed");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }

    private void enterSeveralVrn(CorporateAddEmployeePage corporateAddEmployeePage, int vrnAmount) {
        for (int i = 0; i < vrnAmount; i++) {
            CheckResult(corporateAddEmployeePage.enterVrn(RandomStringUtils.randomAlphanumeric(9)), "Enter Employee CLI");
            CheckResult(corporateAddEmployeePage.clickOnAddVrn(), "Click on Add CLI");
        }
    }
}
