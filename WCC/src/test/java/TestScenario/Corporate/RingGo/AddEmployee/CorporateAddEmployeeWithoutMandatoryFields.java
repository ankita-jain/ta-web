package TestScenario.Corporate.RingGo.AddEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateAddEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import PageObjects.myringo.CorporateAccountPage;
import Utilities.PropertyUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateAddEmployeeWithoutMandatoryFields extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }


    @Test
    public void addEmployeeWithoutMandatoryFieldsTest(){

        StartTest("103049", "Add Employee without mandatory fields");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees -> Add Employee.");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");
        CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee");
        CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
        CheckValue(corporateAddEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 3", "Click on 'Add Employee' button without filling mandatory fields.");
        CheckResult(corporateAddEmployeePage.clickOnSaveEmployee(), "Click on Add Employee");
        CheckValue(corporateAddEmployeePage.getFirstNameErrorMessageText(), "Required", "Verify First Name Error Message");
        CheckValue(corporateAddEmployeePage.getSurnameErrorMessageText(), "Required", "Verify Surname Error Message");
        CheckValue(corporateAddEmployeePage.getCliErrorMessageText(), "Required", "Verify CLI Error Message");
        CheckValue(corporateAddEmployeePage.getVrnErrorMessageText(), "Required", "Verify VRN Error Message");
        CheckValue(corporateAddEmployeePage.getWarningNotificationText(), "There is an error in the form. Please check for details below.", "Verify Warning Notification");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
