package TestScenario.Corporate.RingGo.AddEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateAddEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import PageObjects.myringo.CorporateAccountPage;
import Utilities.PropertyUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateVerifyTooltipsOnAddEmployeePage extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @Test
    public void verifyToolTipsOnAddEmployeePage() {

        StartTest("103051", "Verify tooltips on Employees page");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees -> Add Employee.");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");
        CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee");
        CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
        CheckValue(corporateAddEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 3", "Hover over 'Phone number' field");
        CheckValue(corporateAddEmployeePage.getCliTooltipText(), "'Any Number' enables this employee to pay for parking from any mobile number, as long as they use a vehicle from the list below.", "Verify CLI ToolTip Text");
        LogStep("Step - 4", "Hover over 'Vehicles employee can book parking with' field");
        CheckValue(corporateAddEmployeePage.getVrnTooltipText(), "'Any Vehicle' allows this employee to pay for parking from any vehicle, as long as they use a telephone from the list above.", "Verify VRN ToolTip Text");
        LogStep("Step - 5", "Hover over 'Can employee book parking?'");
        CheckValue(corporateAddEmployeePage.getCanParkTooltipText(), "This employee will NOT be able to park unless this is set to yes.", "Verify Can Employee Park Tooltip Text");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
