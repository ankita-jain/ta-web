package TestScenario.Corporate.RingGo.AddEmployee;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.RingGo.CorporateAddEmployeePage;
import PageObjects.Corporate.RingGo.CorporateEmployeesPage;
import PageObjects.Corporate.RingGo.CorporateHomePage;
import PageObjects.myringo.CorporateAccountPage;
import Utilities.PropertyUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class CorporateVerifyAnyCLIAndAnyVRNForEmployeeAdding extends RingGoScenario {

    private String corporateEmail;
    private String corporatePassword;

    @BeforeMethod
    public void prepareData() throws IOException {
        corporateEmail = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_LOGIN", WCC_PROPERTIES);
        corporatePassword = PropertyUtils.ReadProperty("REBRANDED_CORPORATE_PASSWORD", WCC_PROPERTIES);
    }

    @Test
    public void chooseAnyCLIOrAnyVRNTest() {

        StartTest("103532", "Verify 'Any CLI' and 'Any VRN' options for Employee Adding.");

        LogStep("Step - 1", "Login to the RingGo Corporate account.");
        NavigationHelper.openMyRingo(_driver);
        LoginHelper.loginMyRingoCorporate(corporateEmail, corporatePassword, _driver);
        CorporateHomePage corporateHomePage = new CorporateHomePage(_driver);
        CheckContains(corporateHomePage.getCurrentTitle(), "Welcome to your RingGo Corporate Account", "Verify RingGo Corporate Home page title");

        LogStep("Step - 2", "Go to the Setup -> Add or Edit Employees -> Add Employee.");
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees");
        CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
        CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title");
        CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee");
        CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
        CheckValue(corporateAddEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title");

        LogStep("Step - 3", "Select 'Any' Phone number option.");
        CheckResult(corporateAddEmployeePage.clickOnAnyCliButton(), "Click on 'Any' Phone number option");
        CheckBool(corporateAddEmployeePage.isAnyVrnButtonDisable(), "Verify if 'Any' VRN option is not clickable.");

        LogStep("Step - 4", "Hover on 'Any' VRN");
        CheckValue(corporateAddEmployeePage.getAnyVrnTooltipText(), "Cannot select any phone number and any vehicle at the same time, please amend the selection to enable this setting.", "Verify if correct tooltip is displayed");

        LogStep("Step - 5", "Click on 'Specified' phone number option.");
        CheckResult(corporateAddEmployeePage.clickOnSpecifiedCli(), "Click on 'Specified' Phone number");
        CheckBool(!corporateAddEmployeePage.isAnyVrnButtonDisable(), "Verify if 'Any' VRN option is clickable.");

        LogStep("Step - 6", "Click on 'Any' VRN option.");
        CheckResult(corporateAddEmployeePage.clickOnAnyVrn(), "Click on 'Any' VRN option");
        CheckBool(corporateAddEmployeePage.isAnyCliDisable(), "Verify if 'Any' Phone number option is not clickable.");

        LogStep("Step - 7", "Hover on 'Any' Phone number");
        CheckValue(corporateAddEmployeePage.getAnyCliTooltipText(), "Cannot select any phone number and any vehicle at the same time, please amend the selection to enable this setting.", "Verify if correct tooltip is displayed");

        LoginHelper.logoutMyRingoCorporate(_driver);
    }
}
