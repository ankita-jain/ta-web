package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Change_Password extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Change_Password.class);
    
    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void editCompanyPassword(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Account -> Security Options - Change Password");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickAccounts(), "Clicking on Accounts");

            WCCCorporateAccountPage wcccorporateaccountpage = new WCCCorporateAccountPage(_driver);
            assertTrue("Account page title", wcccorporateaccountpage.getAccountPageTitle().contains("Account"));
            wcccorporateaccountpage.clickSecurityOptions();

            WCCCorporatePasswordResetPage wcccorporatepasswordresetpage = new WCCCorporatePasswordResetPage(_driver);
            assertTrue("Change password page title", wcccorporatepasswordresetpage.getPasswordResetPageTitle().contains("Change My Password"));
            wcccorporatepasswordresetpage.resetPassword(data.get("password"), data.get("new_password"), data.get("new_password"));

            assertTrue("Successfully saved new password", wcccorporatepasswordresetpage.getPasswordChangeMessage().contains("Your password was changed"));

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
