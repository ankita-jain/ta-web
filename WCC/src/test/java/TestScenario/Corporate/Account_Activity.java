package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

public class Account_Activity extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Account_Activity.class);
    String corporateUserName, corporatePassword;

    @Test
    public void corporateAccountActivity() throws IOException {
        StartTest("417", "Verify Home > Corporate Home > Reports > Account Activity");

        try {
        	NavigationHelper.openWestMinster(_driver);
            
            corporateUserName = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
            corporatePassword = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
    		
            LoginHelper.loginCorporateWestminster(corporateUserName, corporatePassword, _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickReports(), "Clicking on Reports");

            WCCCorporateReportsPage wcccorporatereportpage = new WCCCorporateReportsPage(_driver);
            assertTrue("Report page title", wcccorporatereportpage.getReportPageTitle().contains("Reports"));
            wcccorporatereportpage.clickAccountActivity();

            WCCCorporateAccountActivityPage wcccorporateaccountactivitypage = new WCCCorporateAccountActivityPage(_driver);
            assertTrue("Account Activity page title", wcccorporateaccountactivitypage.getAccountActivityPageTitle().contains("Account Activity"));
            wcccorporateaccountactivitypage.searchAccountActivity();

            /*
             * Given the user is in the 'Cost Centre Summary' page,
             * When he changes the filters values and hit 'Search' button,
             * Then result should be shown in the table
             * And if the result table is not visible check whether 'No record found' message displayed.
             * */

            if (wcccorporateaccountactivitypage.isDataTableVisible()) {
                LogPass("Account_Activity");
            } else
                LogError("Account_Activity");

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
