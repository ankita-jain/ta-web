package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Apply_Skip_Licence_Through_Corporate_Funds extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Apply_Skip_Licence_Through_Corporate_Funds.class);
    String oldTab;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void corporateSkipLicence(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Permits > Corporate Permit Applications > Apply Skip Licence through Corporate Funds");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickPermit(), "Clicking on Permit");

            WCCCorporatePermitPage wcccorporatepermitpage = new WCCCorporatePermitPage(_driver);
            assertTrue("Corporate permit page title", wcccorporatepermitpage.getCorporatePageTitle().contains("Corporate Permit Applications for Westminster City Council"));
            wcccorporatepermitpage.clickSkipLicencePermit();

            WCCCorporateSkipLicencePermitApplicationPage wcccorporateskiplicenceapplicationpage = new WCCCorporateSkipLicencePermitApplicationPage(_driver);
            assertTrue("Skip Licence Permit page title", wcccorporateskiplicenceapplicationpage.getSkipLicencePermitPageTitle().contains("Skip Licence Permit Application"));
            wcccorporateskiplicenceapplicationpage.applySkipLicenceProcess(data.get("first_name"), 
            															   data.get("last_name"), 
            															   data.get("primary_email"), 
            															   data.get("primary_email"), 
            															   data.get("road_name"), 
            															   data.get("skips_num"), 
            															   data.get("skip_material"));

            WCCCorporatePermitPaymentPage wcccorporatepermitpayment = new WCCCorporatePermitPaymentPage(_driver);
            wcccorporatepermitpayment.selectCorporatePaymentOption();
            if (wcccorporateskiplicenceapplicationpage.isCorporateAccountHasEnoughBalanace()) {
                System.out.println("here");
                wcccorporatepermitpayment.clickNextButton();
                assertTrue("Checking SKIP LICENCE apply success notification", wcccorporatepermitpayment.getPermitSuccessText().contains("Thank you for your application."));
                wcccorporatepermitpayment.clickFinishButton();
            } else {
                wcccorporatepermitpayment.clickNextButton();
                assertTrue("Insufficent Corp-Balance message", wcccorporatepermitpayment.getErrorNotification().contains("there were not enough funds available."));
                LogError("Corporate_SKIP_Licence");
                Assert.fail("No sufficient balance in the corporate account for SKIP LICENCE");
            }
            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
