package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class VAT_Invoices extends RingGoScenario {

    static Logger LOG = Logger.getLogger(VAT_Invoices.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void corporateVATInvoice(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Reports >VAT Invoices");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickReports(), "Clicking on Reports");

            WCCCorporateReportsPage wcccorporatereportpage = new WCCCorporateReportsPage(_driver);
            assertTrue("Report page title", wcccorporatereportpage.getReportPageTitle().contains("Reports"));
            wcccorporatereportpage.clickVATInvoices();

            /*
             * Given the user is in the 'VAT Invoices' page,
             * When he changes the filters values and hit 'Search' button,
             * Then result should be shown in the table
             * And if the result table is not visible check whether 'No record found' message displayed.
             * */
            
            WCCCorporateVATInvoicePage wcccorporatevatinvoicepage = new WCCCorporateVATInvoicePage(_driver);
            assertTrue("Checking VAT invoices page title", wcccorporatevatinvoicepage.getVATInvoicesPageTitle().contains("Invoices"));


            LogPass("VAT_Invoice");

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
