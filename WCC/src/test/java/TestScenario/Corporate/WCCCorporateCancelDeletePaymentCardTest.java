package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.enums.Month;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import SeleniumHelpers.WaitMethods;
import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;


public class WCCCorporateCancelDeletePaymentCardTest extends RingGoScenario {

    private String email;
    private String password;
    private String lastFourDigitOfCardNumber;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void cancelDeletePaymentCard(String TCID, Map<String, String> data) {
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Cancel deleting a card");

        String cardNumber = data.get("cardNumber");
        String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        int expMonth = RandomUtils.nextInt(1, 12);
        lastFourDigitOfCardNumber = cardNumber.substring(12);           
        String expYearLastTwoDigits = expYear.substring(2);            
        String formattedMonth=((expMonth < 10)? String.format("%02d", expMonth):Integer.toString(expMonth));
        String monthAndYearOfCard = formattedMonth +expYearLastTwoDigits;
       

        email = data.get("email");
        password = data.get("password");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckContains(wccCorporateProfilePage.getCorporateProfilePageTitle(),
                "Welcome to your RingGo Corporate Account",
                "Check corporate profile page title");
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        CheckContains(wcccorporatehomepage.getCoporateHomePageTitle(),
                "Corporate Account",
                "Check corporate page title");
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckContains(wcccorporatefundpage.getFundPageTitle(), "Funds", "Funds page title");
        CheckResult(wcccorporatefundpage.clickPaymentCards(), "Click 'Payment card'");
       
        CorporateHelper.addPaymentCardToCorporate(cardNumber, monthAndYearOfCard,  data.get("cardCountry"), _driver);
        
        WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);

        CheckResult(wcccorporatepaymentcardpage.clickPaymentCardDelete(lastFourDigitOfCardNumber), "Click delete button");

        WCCCorporatePaymentCardDeleteConfirmationPage wcccorporatepaymentcarddeleteconfirmationpage = new WCCCorporatePaymentCardDeleteConfirmationPage(_driver);
        CheckResult(wcccorporatepaymentcarddeleteconfirmationpage.clickDeleteConfirmationNoButton(),
                "Click cancel button");

        CheckValue(wcccorporatepaymentcardpage.getNoticeNotification(), "Delete action cancelled", "Delete cancel notification");

        CheckBool(Objects.nonNull(WaitMethods.WaitExists(_driver, 3,
                wcccorporatepaymentcardpage.getCardNumberAsLocator(lastFourDigitOfCardNumber))),
                "Check card wasn't deleted");
    }

    @AfterMethod
    public void deleteCard() {
        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");

        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);


        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckResult(wcccorporatefundpage.clickPaymentCards(), "Click 'Payment card'");

        CheckResult(wcccorporatepaymentcardpage.clickPaymentCardDelete(lastFourDigitOfCardNumber), "Click delete button");

        WCCCorporatePaymentCardDeleteConfirmationPage wcccorporatepaymentcarddeleteconfirmationpage = new WCCCorporatePaymentCardDeleteConfirmationPage(_driver);
        CheckResult(wcccorporatepaymentcarddeleteconfirmationpage.clickDeleteConfirmationYesButton(), "Click confirm button");
    }

}
