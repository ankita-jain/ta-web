package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class WCCCorporateSearchEmployeeTest extends RingGoScenario {

    static Logger LOG = Logger.getLogger(WCCCorporateSearchEmployeeTest.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void addEmployee(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Setup >Employees");

        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Checking corporate profile page title", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking corporate home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickSetUp(), "Clicking on Set up");

            WCCSetupPage wccsetuppage = new WCCSetupPage(_driver);
            String setupPageTitle = wccsetuppage.getSetUpPageTitle();
            assertTrue("Checking corporate setup page title", setupPageTitle.contains("Setup"));
            /*
             * Delete an Employee based on the Name
             *
             */
            CheckResult(wccsetuppage.clickEmployees(), "Clicking on Employees");

            WCCCorporateEmployeesPage wcccorporateemployeepage = new WCCCorporateEmployeesPage(_driver);
            assertTrue("Checking Employee page title", wcccorporateemployeepage.getEmployeePageTitle().contains("Employees"));

            CheckResult(wcccorporateemployeepage.selectCostCentre(data.get("cost_centre")), String.format("Select cost center -> %s", data.get("cost_centre")));
            CheckResult(wcccorporateemployeepage.selectVehicleFilter(data.get("vehicle")), String.format("Select vehicle -> %s", data.get("vehicle")));
            CheckResult(wcccorporateemployeepage.enterName(data.get("Employee_name")), String.format("Enter name -> %s", data.get("Employee_name")));
            CheckResult(wcccorporateemployeepage.canParkFilter(data.get("Can_park")), String.format("Can Park filter -> %s", data.get("Can_park")));
            CheckResult(wcccorporateemployeepage.search(), "Click search");

            if (wcccorporateemployeepage.isDataTableVisible()) {
                LogPass("Account_Activity");
            } else
                LogError("Account_Activity");


            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
