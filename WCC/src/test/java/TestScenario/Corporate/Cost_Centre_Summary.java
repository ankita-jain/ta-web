package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Cost_Centre_Summary extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Cost_Centre_Summary.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void corporateCostCentre(String TCID, Map<Object, Object> data) throws IOException {
        StartTest("416", "Verify Home > Corporate Home > Reports > Cost Centre Summary");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email").toString(), data.get("password").toString(), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickReports(), "Clicking on Reports");

            WCCCorporateReportsPage wcccorporatereportpage = new WCCCorporateReportsPage(_driver);
            assertTrue("Report page title", wcccorporatereportpage.getReportPageTitle().contains("Reports"));
            wcccorporatereportpage.clickCostCentreSummary();

            WCCCorporateCostCentreSummary wcccorporatecostcentresummary = new WCCCorporateCostCentreSummary(_driver);
            assertTrue("Cost Centre Summary page title", wcccorporatecostcentresummary.getCostCentrePageTitle().contains("Cost Centre Summary"));
            wcccorporatecostcentresummary.costCentreSummarySearch(data.get("start_date").toString(), data.get("end_date").toString(), data.get("report_type").toString());

            /*
             * Given the user is in the 'Cost Centre Summary' page,
             * When he changes the filters values and hit 'Search' button,
             * Then result should be shown in the table
             * And if the result table is not visible check whether 'No record found' message displayed.
             * */

            if (wcccorporatecostcentresummary.isDataTableVisible()) {
                LogPass("Cost_Centre");
            } else
                LogError("Cost_Centre");

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
