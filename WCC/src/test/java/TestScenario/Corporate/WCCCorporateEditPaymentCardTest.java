package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.enums.Month;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;


public class WCCCorporateEditPaymentCardTest extends RingGoScenario {

    private String email;
    private String password;
    private String lastFourDigitOfCardNumber;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class, enabled=false)
    public void editPaymentCard(String TCID, Map<String, String> data) {
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Payment Cards section");

        String cardNumber = data.get("cardNumber");
        String expYearForCreatingCard = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        Month expMonthForCreatingCard = Month.values()[RandomUtils.nextInt(0, 12)];
        lastFourDigitOfCardNumber = cardNumber.substring(12);

        String expYearForEditingCard = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        Month expMonthForEditingCard = Month.values()[RandomUtils.nextInt(0, 12)];
        String monthAndYearOfCardForEditedCard = expMonthForEditingCard.getMonthNumber() + "/" + expYearForEditingCard;

        email = data.get("email");
        password = data.get("password");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckContains(wccCorporateProfilePage.getCorporateProfilePageTitle(),
                "Welcome to your RingGo Corporate Account",
                "Check corporate profile page title");
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        CheckContains(wcccorporatehomepage.getCoporateHomePageTitle(),
                "Corporate Account",
                "Check corporate page title");
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckContains(wcccorporatefundpage.getFundPageTitle(), "Funds", "Funds page title");
        CheckResult(wcccorporatefundpage.clickPaymentCards(), "Click 'Payment card'");

        //addPaymentCardToCorporate(cardNumber, expMonthForCreatingCard.getMonthName(), expYearForCreatingCard, data.get("cardCountry"));

        WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);

        //CheckResult(wcccorporatepaymentcardpage.clickPaymentCardEdit(lastFourDigitOfCardNumber), "Click 'Edit card number''");

        WCCCorporateAddPaymentCardDetailsPage wcccorporateaddpaymentcarddetailspage = new WCCCorporateAddPaymentCardDetailsPage(_driver);

        CheckResult(wcccorporateaddpaymentcarddetailspage.selectCardExpiryMonth(expMonthForEditingCard.getMonthName()), "Select card expiry month");
        CheckResult(wcccorporateaddpaymentcarddetailspage.selectCardExpiryYear(expYearForEditingCard), "Select card expiry year");
        CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardTown(randomAlphabetic(6)), "Enter card town");
        CheckResult(wcccorporateaddpaymentcarddetailspage.selectCardCounty(data.get("cardCountry")), "Enter card country");
        CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardPostcode(randomAlphabetic(6)), "Enter card postcode");
        CheckResult(wcccorporateaddpaymentcarddetailspage.clickSave(), "Click save button");

        CheckContains(wcccorporatepaymentcardpage.getSuccessNotification(),
                "Visa card ending " + lastFourDigitOfCardNumber + " was successfully edited",
                "Check successful message after creating");

        CheckContains(wcccorporatepaymentcardpage.getCardExpDate(lastFourDigitOfCardNumber),
                monthAndYearOfCardForEditedCard,
                "Check month and year of card number");
    }

    @AfterMethod
    public void deleteCard() {
        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");

        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);


        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckResult(wcccorporatefundpage.clickPaymentCards(), "Click 'Payment card'");

        CheckResult(wcccorporatepaymentcardpage.clickPaymentCardDelete(lastFourDigitOfCardNumber), "Click delete button");

        WCCCorporatePaymentCardDeleteConfirmationPage wcccorporatepaymentcarddeleteconfirmationpage = new WCCCorporatePaymentCardDeleteConfirmationPage(_driver);
        CheckResult(wcccorporatepaymentcarddeleteconfirmationpage.clickDeleteConfirmationYesButton(), "Click confirm button");
    }
}
