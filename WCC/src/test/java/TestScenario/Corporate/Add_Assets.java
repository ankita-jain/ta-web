package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.AssertJUnit.assertTrue;

public class Add_Assets extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Add_Assets.class);
    String login, password;

    @Test
    public void addAlertTest() throws IOException {
        StartTest("", "Corporate Add Assets");
        try {
        	NavigationHelper.openWestMinster(_driver);
            
            login = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
            password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
            
            LoginHelper.loginCorporateWestminster(login, password, _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Checking corporate profile page title", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking corporate home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickSetUp(), "Clicking on Set up");

            WCCSetupPage wccsetuppage = new WCCSetupPage(_driver);
            String setupPageTitle = wccsetuppage.getSetUpPageTitle();
            assertTrue("Checking corporate setup page title", setupPageTitle.contains("Setup"));
            CheckResult(wccsetuppage.clickAssets(), "Clicking on Assets");

            WCCCorporateAssetsPage wcccorporateassetspage = new WCCCorporateAssetsPage(_driver);
            assertTrue("Checking Corporate Assets page title", wcccorporateassetspage.getAssetsPageTitle().contains("Manage Assets"));

            String cli = "07" + RandomStringUtils.randomNumeric(9);
            String vrn = RandomStringUtils.randomAlphanumeric(7);

            CheckResult(wcccorporateassetspage.enterCli(cli), String.format("Enter cli -> %s", cli));
            CheckResult(wcccorporateassetspage.addCli(), "Click add Cli");
            CheckResult(wcccorporateassetspage.enterVRN(vrn), String.format("Enter vrn -> %s", vrn));
            CheckResult(wcccorporateassetspage.addVrn(), "Click add vrn");
            CheckResult(wcccorporateassetspage.save(), "Click save");

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
