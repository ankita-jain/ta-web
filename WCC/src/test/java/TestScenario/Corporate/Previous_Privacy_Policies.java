package TestScenario.Corporate;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.WCCCorporateAccountPage;
import PageObjects.Corporate.WCCCorporateDocumentationPage;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporateOnlineTermsPage;
import PageObjects.Corporate.WCCCorporateProfilePage;
import Utilities.PropertyUtils;

import com.relevantcodes.extentreports.LogStatus;

public class Previous_Privacy_Policies extends RingGoScenario {

    String oldTab;
    String corp_email, corp_password;

    @Test
    public void corporateOnlineTerms() throws IOException {
        StartTest("401", "Verify Account -> Documentation");
        
        
        corp_email = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        corp_password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(corp_email, corp_password, _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickAccounts(), "Clicking on Accounts");

            WCCCorporateAccountPage wcccorporateaccountpage = new WCCCorporateAccountPage(_driver);
            assertTrue("Account page title", wcccorporateaccountpage.getAccountPageTitle().contains("Account"));
            wcccorporateaccountpage.clickDocumentation();

            WCCCorporateDocumentationPage wcccorporatedocumentationpage = new WCCCorporateDocumentationPage(_driver);
            assertTrue("Corporate Documentation title", wcccorporatedocumentationpage.getCorporateDocumentationPageTitle().contains("Corporate Documentation"));
			 
		//Saving old tab 
		 oldTab = _driver.getWindowHandle();
		 wcccorporatedocumentationpage.clickOnlineTerms();
		/*
		 * Clicking 'Documentation' link will open in a new window, 
		 * so we have to switch the focus to the new window do the operation and return back to the main tab.
		 
		*/
		ArrayList<String> newTab = new ArrayList<String>(_driver.getWindowHandles());
	    newTab.remove(oldTab);
	    _driver.switchTo().window(newTab.get(0)); //This statement will locate the focus to new tab
		
	    WCCCorporateOnlineTermsPage wcccorporateonlinetermspage = new WCCCorporateOnlineTermsPage(_driver);
	    assertTrue("Terms and Condition page",wcccorporateonlinetermspage.getOnlinePageTitle().contains("Service Terms & Conditions"));
	    wcccorporateonlinetermspage.clickPrivacyLink();
	    assertTrue("Terms and Condition page",wcccorporateonlinetermspage.getOnlinePageTitle().contains("RingGo Privacy Notice"));
	    wcccorporateonlinetermspage.clickPrivacyPolicies();
	    assertTrue("Previous policies",wcccorporateonlinetermspage.getOnlinePageTitle().contains("Previous Privacy Policies (NOT CURRENT)"));
	    _driver.close(); //closing the newly opened tab
	    
	    _driver.switchTo().window(oldTab); //Switch back to the old tab
	  
	    
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
