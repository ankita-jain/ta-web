package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;

public class Top_up_with_BACS extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Top_up_with_BACS.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void topupBACSTest(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Top Up with BACS");


        try {
            NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email").toString(), data.get("password").toString(), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue(corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

            WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
            assertTrue(wcccorporatefundpage.getFundPageTitle().contains("Funds"));
            wcccorporatefundpage.clickTopup();

            WCCCorporateTopupPage wcccorporatetopuppage = new WCCCorporateTopupPage(_driver);
            assertTrue(wcccorporatetopuppage.getTopupPageTitle().contains("Funds"));
       
            if (!wcccorporatetopuppage.isAccountReachedMaximumTopup()) {
                CheckResult(wcccorporatetopuppage.enterTopupAmountField(data.get("topup_amount").toString()), "Enter top up amount");
                CheckResult(wcccorporatetopuppage.selectTopupPaymentMethod(data.get("topup_method").toString()), "Select top up method");
                CheckResult(wcccorporatetopuppage.clickPaymentRequest(), "Click payment request");
                CheckResult(wcccorporatetopuppage.clickNextButton(), "Click next button");
                CheckResult(wcccorporatetopuppage.clickBACSPaymentFinishButon(), "Click BACS payment finish button");
            } else {
                LogError("Corporate_topup_BACS");
            }

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
