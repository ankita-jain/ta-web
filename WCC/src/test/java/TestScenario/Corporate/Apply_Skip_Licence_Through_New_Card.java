package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.WCCCardCVVBookingSession;
import PageObjects.Corporate.*;
import com.relevantcodes.extentreports.LogStatus;

import org.apache.commons.lang3.RandomUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.testng.AssertJUnit.assertTrue;

public class Apply_Skip_Licence_Through_New_Card extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Apply_Skip_Licence_Through_New_Card.class);
    String oldTab;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = DataProviders.CorporateDP.class, groups = {"corporateskiplicence"})
    public void corporateSkipLicence(String TCID, Map<String, String> data) throws IOException {
    	
    	String cardNumber = data.get("cardNumber");
    	String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        int expMonth = RandomUtils.nextInt(1, 12);
        String lastFourDigitOfCardNumber = cardNumber.substring(12);
        String expYearLastTwoDigits = expYear.substring(2);
        String maskedCardNumber = "************" + lastFourDigitOfCardNumber;
        String formattedMonthAndYear=expMonth+"/"+expYearLastTwoDigits;        
        String formattedMonth=((expMonth < 10)? String.format("%02d", expMonth):Integer.toString(expMonth));
        String monthAndYearOfCard = formattedMonth +expYearLastTwoDigits;
    	
        StartTest(TCID, "Verify Home > Corporate Home > Permits > Corporate Permit Applications > Apply Skip Licence through New Card");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickPermit(), "Clicking on Permit");

            WCCCorporatePermitPage wcccorporatepermitpage = new WCCCorporatePermitPage(_driver);
            assertTrue("Corporate permit page title", wcccorporatepermitpage.getCorporatePageTitle().contains("Corporate Permit Applications for Westminster City Council"));
            wcccorporatepermitpage.clickSkipLicencePermit();

            WCCCorporateSkipLicencePermitApplicationPage wcccorporateskiplicenceapplicationpage = new WCCCorporateSkipLicencePermitApplicationPage(_driver);
            assertTrue("Skip Licence Permit page title", wcccorporateskiplicenceapplicationpage.getSkipLicencePermitPageTitle().contains("Skip Licence Permit Application"));
            wcccorporateskiplicenceapplicationpage.applySkipLicenceProcess(data.get("first_name"), 
            															   data.get("last_name"), 
            															   data.get("primary_email"), 
            															   data.get("primary_email"), 
            															   data.get("road_name"), 
            															   data.get("skips_num"), 
            															   data.get("skip_material"));

            WCCCorporatePermitPaymentPage wcccorporatepermitpayment = new WCCCorporatePermitPaymentPage(_driver);            
            wcccorporatepermitpayment.payPermitViaNewCard();
            
            WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
            CheckResult(wccaddpaymentcardpopup.enterCardNumber(cardNumber), String.format("Enter card number -> %s", cardNumber));
            CheckResult(wccaddpaymentcardpopup.enterCardExpires(monthAndYearOfCard), String.format("Enter card expires month and year -> %s", monthAndYearOfCard));
            CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
            
            WCCCorporateAddPaymentCardDetailsPage wcccorporateaddpaymentcarddetailspage = new WCCCorporateAddPaymentCardDetailsPage(_driver);
            CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardAddress(randomAlphabetic(6)), "Enter card address");
            CheckResult(wcccorporateaddpaymentcarddetailspage.selectCardCountyCoporatePermit(data.get("cardCountry")), "Enter card country");
            CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardPostcodeCorporatePermit(randomAlphabetic(6)), "Enter card postcode");
            CheckResult(wcccorporateaddpaymentcarddetailspage.clickAddPaymentCardNextButton(), "Click save button");
            
            wcccorporatepermitpayment.clickExistingPaymentCardEnding();
            wcccorporatepermitpayment.clickNextButton();
            
            WCCCardCVVBookingSession wccCardCVVBookingSession = new WCCCardCVVBookingSession(_driver);
            CheckResult(wccCardCVVBookingSession.enterCVV(data.get("cv2")), "Enter CVV2 code");
            CheckResult(wccCardCVVBookingSession.clickPay(), "Click pay confirmation button");
            
            assertTrue("Checking SKIP LICENCE apply success notification", wcccorporatepermitpayment.getPermitSuccessText().contains("Thank you for your application."));
            wcccorporatepermitpayment.clickFinishButton();

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
