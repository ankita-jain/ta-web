package TestScenario.Corporate;

import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporateParkEmployee;
import PageObjects.Corporate.WCCCorporateParkEmployeeConfirmationPage;
import PageObjects.Corporate.WCCCorporateParkEmployeeSelectPermitPage;
import PageObjects.Corporate.WCCCorporateParkEmployeeSelectTariffPage;
import PageObjects.Corporate.WCCCorporateParkEmployeeSelectVRNPage;
import PageObjects.Corporate.WCCCorporateParkEmployeeSelectZonePage;
import PageObjects.Corporate.WCCCorporateProfilePage;

public class Verify_Corporate_Account_User_Can_Park_Same_Employees_SAME_VRN_And_TIME extends RingGoScenario {
	
    
    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void sameEmployee(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify corporate account user can Park the same employees:- at the same/overlapping period of time- with the same VRN");
        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickPark(), "Clicking on Park");

            WCCCorporateParkEmployee wcccorporateparkemployee = new WCCCorporateParkEmployee(_driver);
            assertTrue("Parking Employee page title", wcccorporateparkemployee.getParkEmployeePageTitle().contains("Park an Employee"));
            wcccorporateparkemployee.selectemployeeAndClickNext(data.get("employee_name"));

            WCCCorporateParkEmployeeSelectPermitPage wcccorporateparkemployeeselectpermitpage = new WCCCorporateParkEmployeeSelectPermitPage(_driver);
            assertTrue("Parking Employee page title", wcccorporateparkemployeeselectpermitpage.getParkEmployeePageTitle().contains("Park an Employee"));
            wcccorporateparkemployeeselectpermitpage.selectStandardParkingSessionAndClickNextButon();

            WCCCorporateParkEmployeeSelectVRNPage wcccorporateparkemployeeselectvrnpage = new WCCCorporateParkEmployeeSelectVRNPage(_driver);
            assertTrue("Parking Employee page title", wcccorporateparkemployeeselectvrnpage.getParkEmployeePageTitle().contains("Park an Employee"));
            wcccorporateparkemployeeselectvrnpage.selectEmployeeCLIAndVRN(data.get("employee_cli"), data.get("employee_vrn"));

            WCCCorporateParkEmployeeSelectZonePage wcccroporateemployeeselectzonepage = new WCCCorporateParkEmployeeSelectZonePage(_driver);
            assertTrue("Parking Employee page title", wcccroporateemployeeselectzonepage.getParkEmployeePageTitle().contains("Park an Employee"));
            wcccroporateemployeeselectzonepage.enterZoneAndClickNext(data.get("location"));

            WCCCorporateParkEmployeeSelectTariffPage wcccorporateparkemployeeselecttariffpage = new WCCCorporateParkEmployeeSelectTariffPage(_driver);
            assertTrue("Parking Employee page title", wcccorporateparkemployeeselecttariffpage.getParkEmployeePageTitle().contains("Park an Employee"));
            wcccorporateparkemployeeselecttariffpage.selectTariffDurationAndClickNext(data.get("duration"));

            WCCCorporateParkEmployeeConfirmationPage wcccorporateparkemployeeconfirmationpage = new WCCCorporateParkEmployeeConfirmationPage(_driver);
            assertTrue("Parking Employee page title", wcccorporateparkemployeeconfirmationpage.getParkEmployeePageTitle().contains("Park an Employee"));
            wcccorporateparkemployeeconfirmationpage.clickConfirmButton();
            wcccorporateparkemployeeconfirmationpage.clickFinishButton();
       
            delay(180000);
            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }



}

}
