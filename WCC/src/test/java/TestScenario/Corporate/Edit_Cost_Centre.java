package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;
import com.relevantcodes.extentreports.LogStatus;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class Edit_Cost_Centre extends RingGoScenario {

    private String costCenterName;
    private String login;
    private String password;
    private String costCenterNumber;
    private String costCenterOwner;

    @Test
    public void editCostCentreTest() throws IOException {
        login = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
        costCenterName = RandomStringUtils.randomAlphabetic(6);
        costCenterNumber = RandomStringUtils.randomNumeric(6);
        costCenterOwner = RandomStringUtils.randomAlphabetic(6);

        StartTest("475", "Verify Home > Corporate Home > Setup > Edit Cost Centres");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(login, password, _driver);

        CorporateHelper.addCostCenter(costCenterName, costCenterNumber, costCenterOwner, _driver);

        WCCCorporateCostCenterPage wccCorporateCostCenterPage = new WCCCorporateCostCenterPage(_driver);
        CheckResult(wccCorporateCostCenterPage.editCostCenter(costCenterName), String.format("Click edit cost center -> %s", costCenterName));

        costCenterName = RandomStringUtils.randomAlphabetic(6);
        WCCCorporateAddCostCentrePage wccCorporateAddCostCentrePage = new WCCCorporateAddCostCentrePage(_driver);
        CheckResult(wccCorporateAddCostCentrePage.enterCostCentreName(costCenterName), String.format("Enter center name -> %s", costCenterName));
        CheckResult(wccCorporateAddCostCentrePage.enterCostCentreNumber(RandomStringUtils.randomNumeric(6)), String.format("Enter center number -> %s", costCenterNumber));
        CheckResult(wccCorporateAddCostCentrePage.enterCostCentreOwner(RandomStringUtils.randomAlphabetic(6)), String.format("Enter center owner -> %s", costCenterOwner));
        CheckResult(wccCorporateAddCostCentrePage.save(), "Click save");

        CheckContains(wccCorporateCostCenterPage.getCostCentreSuccessNotification(), "Cost Centre was successfully edited", "Check success notification message");
    }

    @AfterMethod
    public void deleteCostCenter() {
        Log.add(LogStatus.INFO, "After method - Delete cost center");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");

        LoginHelper.loginCorporateWestminster(login, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        CheckResult(wccSetupPage.clickCostCentres(), "Clicking on Cost Centres");

        WCCCorporateCostCenterPage wccCorporateCostCenterPage = new WCCCorporateCostCenterPage(_driver);
        CheckResult(wccCorporateCostCenterPage.deleteCostCenter(costCenterName), String.format("Delete cost center -> %s", costCenterName));

        WCCCorporateCostCentreDeleteConfirmationPage wccCorporateCostCentreDeleteConfirmationPage = new WCCCorporateCostCentreDeleteConfirmationPage(_driver);
        CheckResult(wccCorporateCostCentreDeleteConfirmationPage.clickYes(), String.format("Delete cost center -> %s", costCenterName));
    }
}


