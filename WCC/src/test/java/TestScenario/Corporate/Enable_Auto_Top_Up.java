package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import Utilities.PropertyUtils;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import java.io.IOException;

import static Utilities.TimerUtils.delay;

public class Enable_Auto_Top_Up extends RingGoScenario {
	private String login;
    private String password;

    static Logger LOG = Logger.getLogger(Enable_Auto_Top_Up.class);

    @Test
    public void autoTopUpEnable() throws IOException {
        StartTest("406", "Verify Home > Corporate Home > Funds > Manage your Auto Tops > Enable auto top up and test it");

        NavigationHelper.openWestMinster(_driver);
        
        login = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
        
        LoginHelper.loginCorporateWestminster(login, password, _driver);

        WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
        String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
        CheckBool(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"), "Corporate profile page title verify");
        CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
        CheckBool(corporateHomePageTitle.contains("Corporate Account"), "Corporate Login Home title verify");
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");


        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckBool(wcccorporatefundpage.getFundPageTitle().contains("Funds"), "Corporate top-up page title verify");
        CheckResult(wcccorporatefundpage.clickAutoTopup(), "Clicking on Auto TopUp");

        WCCCorporateAutoTopupPage wcccorporateautotopuppage = new WCCCorporateAutoTopupPage(_driver);
        CheckBool(wcccorporateautotopuppage.getAutoTopupPageTile().contains("Manage Auto Topups"), "Corporate top-up page title verify");
        CheckResult(wcccorporateautotopuppage.selectAutoTopupStatus("Enabled"), "Select auto top up status");
        CheckResult(wcccorporateautotopuppage.enterAutoTopUpAmount("200"), "Enter auto top up amount");
        CheckResult(wcccorporateautotopuppage.enterAutoTopUpNotifyAmount("50"), "Enter auto top up notify");
        CheckResult(wcccorporateautotopuppage.clickSaveButton(), "Click save button");
        CheckBool(wcccorporateautotopuppage.getautoTopUpSuccessNotification().contains("Auto topup now enabled"), "Corporate Auto top up notification ");

        /*
         * In this test, 'Thread.sleep(5000)' is used because, the whole page is controlled by
         * 'Robot' which refresh the content of the page.
         * And there is a time lag between system page update and robot to update the page.
         * So in this page, test will wait for 5 seconds and do a Hard page refresh and search for the 'Top up' status.
         */

        delay(5000);

        CheckBool(wcccorporateautotopuppage.getAutoTopupEnableStatus().contains("Enabled"), "Corporate top-up disable check");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }
}
