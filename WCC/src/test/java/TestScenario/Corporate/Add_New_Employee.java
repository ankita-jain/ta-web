package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static Utilities.TimerUtils.delay;

import java.util.Map;

public class Add_New_Employee extends RingGoScenario {
    private String email;
    private String password;
    private String name;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void addEmployee(String TCID, Map<Object, Object> Data) {
        StartTest(TCID, "Verify Home > Corporate Home > Setup >Employees > Add new employee");

        NavigationHelper.openWestMinster(_driver);
        email = Data.get("email").toString();
        password = Data.get("password").toString();
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        String corporateProfilePageTitle = wccCorporateProfilePage.getCorporateProfilePageTitle();
        CheckContains(corporateProfilePageTitle, "Welcome to your RingGo Corporate Account", "Ringo Corporate Account page title");
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        String corporateHomePageTitle = wccCorporateHomePage.getCoporateHomePageTitle();
        CheckContains(corporateHomePageTitle, "Corporate Account", "Corporate Account page title");
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        String setupPageTitle = wccSetupPage.getSetUpPageTitle();
        CheckContains(setupPageTitle, "Setup", "Setup page title");
       
        CheckResult(wccSetupPage.clickAssets(), "Clicking on Assets");

        WCCCorporateAssetsPage wcccorporateassetspage = new WCCCorporateAssetsPage(_driver);
        CheckContains(wcccorporateassetspage.getAssetsPageTitle(), "Manage Assets", "Manage Assets page title");

        String cli = "07" + RandomStringUtils.randomNumeric(9);
        String vrn = RandomStringUtils.randomAlphanumeric(7);
      
        CheckResult(wcccorporateassetspage.enterCli(cli), String.format("Enter cli -> %s", cli));
        CheckResult(wcccorporateassetspage.addCli(), "Click add Cli");
        CheckResult(wcccorporateassetspage.enterVRN(vrn), String.format("Enter vrn -> %s", vrn));
        CheckResult(wcccorporateassetspage.addVrn(), "Click add vrn");
        CheckResult(wcccorporateassetspage.save(), "Click save");
        
        delay(2000);
        CheckResult(wcccorporateassetspage.clickSetup(), "Click Setup");

        CheckResult(wccSetupPage.clickEmployees(), "Clicking on Employees");

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        CheckContains(wccCorporateEmployeesPage.getEmployeePageTitle(), "Employees", "Employees page title");
        CheckResult(wccCorporateEmployeesPage.addNewEmployee(), "Click Add new employee");

        WCCCorporateAddEmployeePage wcccorporateaddemployeepage = new WCCCorporateAddEmployeePage(_driver);
        CheckContains(wcccorporateaddemployeepage.getAddEmployeePageTitle(), "Add Employee", "Add Employee page title");

        name = RandomStringUtils.randomAlphabetic(6);
        String secondName = RandomStringUtils.randomAlphabetic(7);
        String emailId = RandomStringUtils.randomAlphabetic(5) + "@ctt.com";
        CheckResult(wcccorporateaddemployeepage.selectTitle(WCCCorporateAddEmployeePage.Title.MR), String.format("Select title -> %s", WCCCorporateAddEmployeePage.Title.MR.toString()));
        CheckResult(wcccorporateaddemployeepage.enterFirstName(name), String.format("Enter first name -> %s", name));
        CheckResult(wcccorporateaddemployeepage.enterSurname(secondName), String.format("Enter second name -> %s", secondName));
        CheckResult(wcccorporateaddemployeepage.enterEmail(emailId), String.format("Enter email -> %s", emailId));
        CheckResult(wcccorporateaddemployeepage.tickWelcomeEmail(), "Tick Welcome email");
        CheckResult(wcccorporateaddemployeepage.enterPhoneNumber(cli), String.format("Enter cli -> %s", cli));
        CheckResult(wcccorporateaddemployeepage.addCli(), "Click Add cli");
        CheckResult(wcccorporateaddemployeepage.enterVehicle(vrn), String.format("Enter vrn -> %s", vrn));
        CheckResult(wcccorporateaddemployeepage.addVrn(), "Click add vrn");
        CheckResult(wcccorporateaddemployeepage.tickCanEmployeePark(), "Tick can park");
        CheckResult(wcccorporateaddemployeepage.clickSave(), "Click save");

        WCCCorporateEmployeesPage employeesPage = new WCCCorporateEmployeesPage(_driver);
        String successNotificationMessage = "Employee " + name + " " + secondName + " was successfully added";
        CheckContains(employeesPage.getSuccessNotification().toLowerCase(), successNotificationMessage.toLowerCase(),
                "Checking success notification message");
    }

    @AfterMethod
    public void deleteEmployee() {
        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");

        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
        CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
        CheckResult(wccSetupPage.clickEmployees(), "Clicking on Employees");

        WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
        CheckResult(wccCorporateEmployeesPage.enterName(name),String.format("Search delete employee -> %s", name));
        CheckResult(wccCorporateEmployeesPage.search(),"Click search button");
        
        delay(3000);
        name = name.substring(0, 1).toUpperCase() + name.substring(1); // Change first letter of the name to capital letter
        CheckResult(wccCorporateEmployeesPage.clickEmployeeDeleteButton(name), String.format("Click delete employee -> %s", name));

        WCCCorporateDeleteConfirmationPage wcccorporatedeleteconfirmationpage = new WCCCorporateDeleteConfirmationPage(_driver);
        CheckResult(wcccorporatedeleteconfirmationpage.clickDeleteConfirmButton(), "Click on Delete Confirm button");
    }
}
