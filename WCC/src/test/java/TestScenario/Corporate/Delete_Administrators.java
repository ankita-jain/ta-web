package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import org.testng.annotations.Test;

import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

public class Delete_Administrators extends RingGoScenario {
    private String adminEmail;
    private String email;
    private String password;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void deleteAdminTest(String TCID, Map<Object, Object> Data) {
        StartTest(TCID, "Verify Home > Corporate Home > Setup > Delete Administrators ");

        email = Data.get("email").toString();
        password = Data.get("password").toString();
        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
        CheckContains(wcccoporateprofilepage.getCorporateProfilePageTitle(), "Welcome to your RingGo Corporate Account",
                "Checking Corporate Account page title");
        CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
        CheckContains(corporateHomePageTitle, "Corporate Account", "Checking Home page title");
        CheckResult(wcccorporatehomepage.clickSetUp(), "Clicking on Set up");

        WCCSetupPage wccsetuppage = new WCCSetupPage(_driver);
        String setupPageTitle = wccsetuppage.getSetUpPageTitle();
        CheckContains(setupPageTitle, "Setup", "Checking Setup page title");
        CheckResult(wccsetuppage.clickAdministrator(), "Clicking on Administrator");

        WccCorporateAdministrator wcccorporateadministrator = new WccCorporateAdministrator(_driver);
        String corporateAdminPageTitle = wcccorporateadministrator.getCorportaeAdministratorPageTitle();
        CheckContains(corporateAdminPageTitle, "Corporate Administrators", "Checking Admin page title");

        CheckResult(wcccorporateadministrator.clickAddAdminButton(), "Clicking on Add Administrator button");

        adminEmail = randomAlphabetic(6) + "@gmail.com";
        String firstName = "FirstName" + randomAlphabetic(6);
        String surname = "Surname" + randomAlphabetic(6);
        String password = "1Password" + randomAlphabetic(6);
        CorporateHelper.addNewAdminProcess(adminEmail, firstName, surname, password, _driver);

        wcccorporateadministrator.clickAdminDelete(adminEmail);

        WCCCorporateDeleteConfirmationPage wcccorporatedeleteconfirmationpage = new WCCCorporateDeleteConfirmationPage(_driver);
        CheckContains(wcccorporatedeleteconfirmationpage.getPageTitle(), "Confirm Deletion", "Checking Confirm Deletion page title");
        CheckResult(wcccorporatedeleteconfirmationpage.clickDeleteConfirmButton(), "Click on Delete Confirm button");

        String successNotficationMessage = "Administrator " + firstName + " " + surname + " was successfully deleted";
        CheckContains(wcccorporateadministrator.getSuccessMessage(), successNotficationMessage, "Checking notification message");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }

}
