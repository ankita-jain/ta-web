package TestScenario.Corporate;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateEditDetailsPage;
import PageObjects.Corporate.WCCCorporateProfilePage;

import com.relevantcodes.extentreports.LogStatus;

public class Corporate_Profile extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Corporate_Profile.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void editCompanyDetails(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "Verify Account -> Profile");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email").toString(), data.get("password").toString(), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickEditCompanyInformation(), "Clicking on Edit Company Information");

            WCCCorporateEditDetailsPage wcccorporateeditdetailspage = new WCCCorporateEditDetailsPage(_driver);
            assertTrue("Corporate account details", wcccorporateeditdetailspage.getEditDetailsPageTitle().contains("Manage Account Details"));
            wcccorporateeditdetailspage.editCompanyDetails( data.get("account_name").toString(), 
										            		data.get("company_name").toString(),
										            		data.get("department").toString(), 
										            		data.get("company_number").toString(), 
										            		data.get("registration_number").toString(), 
										            		data.get("business_address").toString(), 
										            		data.get("postcode").toString(), 
										            		data.get("country").toString(), 
										            		data.get("no_of_enployees").toString(), 
										            		data.get("telephone").toString());
            assertTrue("Edit company successfully", wcccoporateprofilepage.getSuccessNotification().contains("successfully edited"));

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
