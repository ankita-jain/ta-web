package TestScenario.Corporate;

import static Utilities.DateUtils.TimeStamp;
import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateFundPage;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporateProfilePage;
import PageObjects.Corporate.WCCCorporateTopupPage;
import PageObjects.Insight.InsightCorporate;
import PageObjects.Insight.InsightSearch;
import PageObjects.Insight.SupportPage;
import Utilities.PropertyUtils;

public class WCCCorporateBACSRejectTest extends RingGoScenario {
	
	String insightUserName, insightPassword;
	String corp_email,corp_password;
	String top_up_amount = "10";
	String top_up_method = "BACS";
	
	
	@BeforeMethod
	public void topupCorporatewithBACS() throws IOException {
		
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_LOGIN", WCC_PROPERTIES);
	    insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);	    
	    
	    corp_email = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
	    corp_password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);	    

		NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(corp_email, corp_password, _driver);

        WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
        String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
        assertTrue(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
        CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
        assertTrue(corporateHomePageTitle.contains("Corporate Account"));
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        assertTrue(wcccorporatefundpage.getFundPageTitle().contains("Funds"));
        wcccorporatefundpage.clickTopup();

        WCCCorporateTopupPage wcccorporatetopuppage = new WCCCorporateTopupPage(_driver);
        assertTrue(wcccorporatetopuppage.getTopupPageTitle().contains("Funds"));
   
        if (!wcccorporatetopuppage.isAccountReachedMaximumTopup()) {
            CheckResult(wcccorporatetopuppage.enterTopupAmountField(top_up_amount), "Enter top up amount");
            CheckResult(wcccorporatetopuppage.selectTopupPaymentMethod(top_up_method), "Select top up method");
            CheckResult(wcccorporatetopuppage.clickPaymentRequest(), "Click payment request");
            CheckResult(wcccorporatetopuppage.clickNextButton(), "Click next button");
            CheckResult(wcccorporatetopuppage.clickBACSPaymentFinishButon(), "Click BACS payment finish button");
        } else {
            LogError("Corporate_topup_BACS");
        }

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
		
	}
	
	@Test
	public void rejectBACSfromInsight() {
				  
		   StartTest("29663", "Reject a corporate account bank transfer request");
		   
		   LogStep("1", "Open Insight");
		   NavigationHelper.openInsight(_driver);
		   
	       LogStep("2", "Log on to Insight");
	   	   LoginHelper.loginInsight(insightUserName, insightPassword, _driver);
		   
		   delay(5000);
		   
		   InsightSearch insightsearch = new InsightSearch(_driver);
		   CheckResult(insightsearch.clickSupport(), "Support menu clicked");
		   
		   LogStep("3", "Go to 'Support' > 'Corporate' > 'Bank Transfer'");
		   SupportPage supportpage = new SupportPage(_driver);
	       CheckResult(supportpage.getLeftSideBar().clickCorporate(), "Corporate menu selected");
	       
	       InsightCorporate insightcorporate = new InsightCorporate(_driver);
	       CheckResult(insightcorporate.clickCorporateBankTransfer(), "Click Bank Transfer link");
	       
	       CheckResult(insightcorporate.getBankTransfer().selectStatus("PENDING"), "Select status of the BACS");
	       CheckResult(insightcorporate.getBankTransfer().selectCorporateAccount("Corporate Company - 660043"), "Select Corporate company");
	       insightcorporate.getBankTransfer().enterBACSTranferDate(TimeStamp("dd MMM YYYY"));
	       
	       delay(3000);
	       CheckResult(insightcorporate.getBankTransfer().clickFilter(), "Click 'Filter' button");
		
	}

}
