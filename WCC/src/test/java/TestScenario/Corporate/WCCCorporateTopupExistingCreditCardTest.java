package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;

import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static Utilities.TimerUtils.delay;
import static java.lang.Double.parseDouble;

public class WCCCorporateTopupExistingCreditCardTest extends RingGoScenario {
	
    private String email;
    private String password;
    private String top_amount;
    private String topup_method;
    private String cardNumber;
    private String expMonth;
    private String expYear;
    private String CV2;

	 @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void topupExistingCardTest(String TCID, Map<Object, Object> Data) throws IOException {
		 
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Top Up with Credit/Debit card - Existing card");
        
        email = Data.get("email").toString();
        password = Data.get("password").toString();
        top_amount = Data.get("top_amount").toString();
        topup_method = Data.get("top_method").toString();
        cardNumber = Data.get("card_number").toString();
        expMonth = Data.get("expMonth").toString();
        expYear = Data.get("expYear").toString();
        CV2 = Data.get("CV2").toString();
        
        
        String lastFourDigitOfCardNumber = cardNumber.substring(12);

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckContains(wccCorporateProfilePage.getCorporateProfilePageTitle(), "Welcome to your RingGo Corporate Account",
                "Checking Home page title");

        double currentBalanceOnCorporateAccount = Double.parseDouble(wccCorporateProfilePage.getCurrentFundBalance());
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
        CheckContains(corporateHomePageTitle, "Corporate Account", "Checking Home page title");
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckContains(wcccorporatefundpage.getFundPageTitle(), "Funds", "Check Funds page title");
        CheckResult(wcccorporatefundpage.clickTopup(), "Top up button");

        WCCCorporateTopupPage wcccorporatetopuppage = new WCCCorporateTopupPage(_driver);
        CheckContains(wcccorporatetopuppage.getTopupPageTitle(), "Funds", "Checking Funds page title");

        if (!wcccorporatetopuppage.isAccountReachedMaximumTopup()) {
            CheckResult(wcccorporatetopuppage.enterTopupAmountField(top_amount), "Enter top up amount");
            CheckResult(wcccorporatetopuppage.selectTopupPaymentMethod(topup_method), "Select top up method");
            CheckResult(wcccorporatetopuppage.clickNextButton(), "Click next button");

            WCCCorporateTopupByCardPage wcccorporatetopupbycardpage = new WCCCorporateTopupByCardPage(_driver);
            CheckContains(wcccorporatetopupbycardpage.getCorporateTopupByCardPageTitle(), "Funds", "Verify  fund page title");
           // CheckResult(wcccorporatetopupbycardpage.selectCard(lastFourDigitOfCardNumber, expMonth, expYear.substring(2)), "Select specific card");
            wcccorporatetopupbycardpage.selectPaymentCard(lastFourDigitOfCardNumber, expMonth, expYear.substring(2));
            CheckResult(wcccorporatetopupbycardpage.clickPayButton(), "Click pay button");
            CheckResult(wcccorporatetopupbycardpage.enterCV2(CV2), "Enter CVV2 code");
            CheckResult(wcccorporatetopupbycardpage.clickPayOnPopUpForm(), "Click pa6y button on pop up form");
            CheckResult(wcccorporatetopupbycardpage.clickFinalButton(), "Click final button");
            /*
             * In this test, 'Thread.sleep(3000)' is used because, the whole page is controlled by
             * 'Robot' which refresh the content of the page.
             * And there is a time lag between system page update and robot to update the page.
             * So in this page, test will wait for 3 seconds and do a Hard page refresh and search for the 'Top up' status.
             */
            delay(3000);

            CheckResult(wccCorporateProfilePage.clickGoToProfilePage(), "Click pay confirmation button");

            double balanceAfterTopUp = parseDouble(wccCorporateProfilePage.getCurrentFundBalance());
            double topAmt = parseDouble(top_amount);
            double finalAmt = currentBalanceOnCorporateAccount + topAmt;


            CheckBool(balanceAfterTopUp == finalAmt, "Comparing balance before top up and after");
        } else {
            LogError("Corporate_topup_BACS");
        }

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }

}
