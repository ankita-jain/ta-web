package TestScenario.Corporate;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporateProfilePage;
import PageObjects.Corporate.WCCCorporateSuspensionPage;

import com.relevantcodes.extentreports.LogStatus;

public class Book_Suspension_and_Dispensation extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Book_Suspension_and_Dispensation.class);
    String oldTab;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void checkAllLinksProfilePage(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Book a Suspension / Dispensation");

        try {
        	NavigationHelper.openWestMinster(_driver);
            
            LoginHelper.loginCorporateWestminster(data.get("email"),
            						  data.get("password"), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            assertTrue("Checking Suspesnion and Dispensation link", wcccorporatehomepage.getSuspensionLink().contains("https://www.westminster.gov.uk/suspensions-dispensations-and-skips"));

            oldTab = _driver.getWindowHandle();
            CheckResult(wcccorporatehomepage.clickSuspensionLink(), "Clicking on Suspension Link");


            /*
             * Clicking 'Documentation' link will open in a new window,
             *  so we have to switch the focus to the new window do the operation and return back to the main tab.
             * */


            ArrayList<String> newTab = new ArrayList<String>(_driver.getWindowHandles());
            newTab.remove(oldTab);
            _driver.switchTo().window(newTab.get(0)); //This statement will locate the focus to new tab

            WCCCorporateSuspensionPage wcccorporatesuspensionpage = new WCCCorporateSuspensionPage(_driver);
            assertTrue("Checking suspension page title", wcccorporatesuspensionpage.getSuspensionPageTitle().contains("Suspensions, dispensations and skips"));

            for (String handle : _driver.getWindowHandles()) {
                if (!handle.equals(oldTab)) {
                    _driver.switchTo().window(handle);
                    _driver.close();
                }
            }


            _driver.switchTo().window(oldTab); //Switch back to the old tab

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
