package TestScenario.Corporate;

import java.io.IOException;

import org.testng.annotations.Test;

import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import Utilities.PropertyUtils;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;

public class Verify_Existing_User_Able_to_Login_Successfully extends RingGoScenario {
	
	public String corporateUserName,corpatePassword;
	
	@Test
	public void existingCorporateAccountLogin() throws IOException {
		
		corporateUserName = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
		corpatePassword = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
		
		StartTest("390", "Verify existing user is able to log in successfully");
		
		LogStep("Step - 1", "Launch Corporate website test link.");
        NavigationHelper.openMyRingo(_driver);
        
        HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

        CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login");
        CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate");

        LogStep("Step - 2", "Enter the existing user's valid login id and Password details");
        MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
        CheckResult(corporateLogin.enterEmailOrMobile(corporateUserName), "Input email address");
        CheckResult(corporateLogin.enterPasswordOrPin(corpatePassword), "Input password");
        CheckResult(corporateLogin.clickLogin(), "Click Login");
        
        CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
        CheckBool(corporateAccountPage.isWelcomeTitleDisplayed(), "Checking Welcome page is displayed");
        
        LogStep("Step - 3", "Click on Account - Log out");
        LoginHelper.logoutMyRingoCorporate(_driver);
	}

}
