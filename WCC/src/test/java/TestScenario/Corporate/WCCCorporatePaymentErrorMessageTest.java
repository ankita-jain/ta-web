package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.Corporate.*;
import com.relevantcodes.extentreports.LogStatus;

import DataProviders.CorporateDP;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

import static org.testng.AssertJUnit.assertTrue;


public class WCCCorporatePaymentErrorMessageTest extends RingGoScenario {


    static Logger LOG = Logger.getLogger(WCCCorporatePaymentErrorMessageTest.class);

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = DataProviders.CorporateDP.class, groups = {"corporateAddPaymentCard"})
    public void paymentCardErrorCheck(String TCID, Map<String, String> data) throws IOException {
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Payment Cards section - Negative Test - Verify error messages");

        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email"), data.get("password"), _driver);
            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue(corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

            WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
            assertTrue(wcccorporatefundpage.getFundPageTitle().contains("Funds"));
            wcccorporatefundpage.clickPaymentCards();

            WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);
            assertTrue(wcccorporatepaymentcardpage.getPaymentCardTitle().contains("Payment Details"));

            /*
             * If the 'Add a new card' is available,
             * Then click link and add new card details
             *
             * */

            if (wcccorporatepaymentcardpage.isAddNewCardButtonDisplayed()) {                               
                CheckResult(wcccorporatepaymentcardpage.clickAddNewCardButton(), "Click 'Add new card'");
              
                WCCCorporateAddPaymentCardDetailsPage wcccorporateaddpaymentcarddetailspage = new WCCCorporateAddPaymentCardDetailsPage(_driver);
                wcccorporateaddpaymentcarddetailspage.clickAddPaymentCard();
                
                WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
                CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button");
                CheckContains(wccaddpaymentcardpopup.getCardNumberErrorMessage(), "Please enter a valid card number","Checking error messages");
                CheckContains(wccaddpaymentcardpopup.getCardExpiryErrorMessage(), "Invalid date","Checking error messages on expires");
                wccaddpaymentcardpopup.clickAddPaymentPopupWindowClose();
              

            }

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }
}

