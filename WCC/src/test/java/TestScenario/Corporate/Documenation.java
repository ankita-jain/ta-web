package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.*;

import com.relevantcodes.extentreports.LogStatus;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import static Utilities.TimerUtils.delay;
import static org.testng.AssertJUnit.assertTrue;

public class Documenation extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Documenation.class);
    String oldTab;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void corporateOnlineTerms(String TCID, Map<Object, Object> data) throws IOException {
        StartTest(TCID, "Verify Account -> Documentation");


        try {
        	NavigationHelper.openWestMinster(_driver);
            LoginHelper.loginCorporateWestminster(data.get("email").toString(), data.get("password").toString(), _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue("Corporate Profile page title check", corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue("Checking Corporate Home page title", corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickAccounts(), "Clicking on Accounts");

            WCCCorporateAccountPage wcccorporateaccountpage = new WCCCorporateAccountPage(_driver);
            assertTrue("Account page title", wcccorporateaccountpage.getAccountPageTitle().contains("Account"));
            wcccorporateaccountpage.clickDocumentation();

            WCCCorporateDocumentationPage wcccorporatedocumentationpage = new WCCCorporateDocumentationPage(_driver);
            assertTrue("Corporate Documentation title", wcccorporatedocumentationpage.getCorporateDocumentationPageTitle().contains("Corporate Documentation"));
			 
		//Saving old tab 
		 oldTab = _driver.getWindowHandle();
		 wcccorporatedocumentationpage.clickOnlineTerms();
		/*
		 * Clicking 'Documentation' link will open in a new window, 
		 * so we have to switch the focus to the new window do the operation and return back to the main tab.
		 
		*/
		ArrayList<String> newTab = new ArrayList<String>(_driver.getWindowHandles());
	    newTab.remove(oldTab);
	    _driver.switchTo().window(newTab.get(0)); //This statement will locate the focus to new tab
		
	    WCCCorporateOnlineTermsPage wcccorporateonlinetermspage = new WCCCorporateOnlineTermsPage(_driver);
	    assertTrue("Terms and Condition page",wcccorporateonlinetermspage.getOnlinePageTitle().contains("Service Terms & Conditions"));
	    wcccorporateonlinetermspage.clickPrivacyLink();
	    assertTrue("Terms and Condition page",wcccorporateonlinetermspage.getOnlinePageTitle().contains("RingGo Privacy Notice"));
	    _driver.close(); //closing the newly opened tab
	    
	    _driver.switchTo().window(oldTab); //Switch back to the old tab
	  
	    
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }


    }

}
