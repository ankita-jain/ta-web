package TestScenario.Corporate;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateCostCenterPage;
import PageObjects.Corporate.WCCCorporateCostCentreDeleteConfirmationPage;
import Utilities.PropertyUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import java.io.IOException;

public class Delete_Cost_Centre extends RingGoScenario {

    private String costCenterName;
    private String login;
    private String password;
    private String costCenterNumber;
    private String costCenterOwner;

    @Test
    public void deleteCostCentreTest() throws IOException {
    	
        login = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
        password = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
        
        costCenterName = RandomStringUtils.randomAlphabetic(6);
        costCenterNumber = RandomStringUtils.randomNumeric(6);
        costCenterOwner = RandomStringUtils.randomAlphabetic(6);

        StartTest("476", "Verify Home > Corporate Home > Setup > Delete Cost Centres");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(login, password, _driver);

        CorporateHelper.addCostCenter(costCenterName, costCenterNumber, costCenterOwner, _driver);

        WCCCorporateCostCenterPage wccCorporateCostCenterPage = new WCCCorporateCostCenterPage(_driver);
        wccCorporateCostCenterPage.deleteCostCenter(costCenterName);

        WCCCorporateCostCentreDeleteConfirmationPage wccCorporateCostCentreDeleteConfirmationPage = new WCCCorporateCostCentreDeleteConfirmationPage(_driver);
        CheckResult(wccCorporateCostCentreDeleteConfirmationPage.clickYes(), "Click 'yes'");
        CheckContains(wccCorporateCostCenterPage.getCostCentreSuccessNotification(), "Cost Centre was successfully deleted", "Check success message");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }
}
