package TestScenario.Corporate;

import DataProviders.CorporateDP;
import GenericComponents.RingGoScenario;
import GenericComponents.enums.Month;
import GenericComponents.helper.CorporateHelper;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Corporate.*;
import SeleniumHelpers.WaitMethods;
import org.apache.commons.lang3.RandomUtils;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;


public class Delete_Payment_Cards_Section extends RingGoScenario {

    private String email;
    private String password;
    private String lastFourDigitOfCardNumber;

    @Test(dataProvider = "corporateGetJSONData", dataProviderClass = CorporateDP.class)
    public void deletePaymentCard(String TCID, Map<String, String> data) {
        StartTest(TCID, "Verify Home > Corporate Home > Funds > Delete Payment Cards section");

        String cardNumber = data.get("cardNumber");
        String expYear = Integer.toString(RandomUtils.nextInt(LocalDate.now().getYear() + 1, LocalDate.now().getYear() + 5));
        int expMonth = RandomUtils.nextInt(1, 12);
        lastFourDigitOfCardNumber = cardNumber.substring(12);           
        String expYearLastTwoDigits = expYear.substring(2);            
        String formattedMonth=((expMonth < 10)? String.format("%02d", expMonth):Integer.toString(expMonth));
        String monthAndYearOfCard = formattedMonth +expYearLastTwoDigits;
       

        email = data.get("email");
        password = data.get("password");

        NavigationHelper.openWestMinster(_driver);
        LoginHelper.loginCorporateWestminster(email, password, _driver);

        WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
        CheckContains(wccCorporateProfilePage.getCorporateProfilePageTitle(),
                "Welcome to your RingGo Corporate Account",
                "Check corporate profile page title");
        CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home");

        WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
        CheckContains(wcccorporatehomepage.getCoporateHomePageTitle(),
                "Corporate Account",
                "Check corporate page title");
        CheckResult(wcccorporatehomepage.clickFunds(), "Clicking on Funds");

        WCCCorporateFundPage wcccorporatefundpage = new WCCCorporateFundPage(_driver);
        CheckContains(wcccorporatefundpage.getFundPageTitle(), "Funds", "Funds page title");
        CheckResult(wcccorporatefundpage.clickPaymentCards(), "Click 'Payment card'");

        WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);

        // Check if card already exists and if not then add it
        if (Objects.isNull(WaitMethods.WaitExists(_driver, 3, wcccorporatepaymentcardpage.getCardNumberAsLocator(lastFourDigitOfCardNumber))))
        	CorporateHelper.addPaymentCardToCorporate(cardNumber, monthAndYearOfCard,  data.get("cardCountry"), _driver);

        CheckResult(wcccorporatepaymentcardpage.clickPaymentCardDelete(lastFourDigitOfCardNumber), "Click delete button");

        WCCCorporatePaymentCardDeleteConfirmationPage wcccorporatepaymentcarddeleteconfirmationpage = new WCCCorporatePaymentCardDeleteConfirmationPage(_driver);
        CheckResult(wcccorporatepaymentcarddeleteconfirmationpage.clickDeleteConfirmationYesButton(), "Click confirm button");

        CheckContains(wcccorporatepaymentcardpage.getSuccessNotification(), "successfully deleted", "Delete success message");

        CheckBool(Objects.isNull(WaitMethods.WaitExists(_driver, 3, wcccorporatepaymentcardpage.getCardNumberAsLocator(lastFourDigitOfCardNumber))), "Check Card is no longer listed");

        CorporateMenu corporatemenu = new CorporateMenu(_driver);
        CheckResult(corporatemenu.clickAccount(), "Click account menu");
        CheckResult(corporatemenu.clickLogout(), "Click logout link");
    }
}
