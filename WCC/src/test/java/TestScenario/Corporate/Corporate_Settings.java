package TestScenario.Corporate;

import static org.testng.AssertJUnit.assertTrue;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import Logging.Log;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporateProfilePage;
import PageObjects.Corporate.WCCCorporateSettingPage;
import PageObjects.Corporate.WCCSetupPage;
import Utilities.PropertyUtils;

public class Corporate_Settings extends RingGoScenario {

    static Logger LOG = Logger.getLogger(Corporate_Settings.class);
    String corporateUserName, corporatePassword;
    @Test
    public void enableEmployeesToPark() throws IOException {
        StartTest("424", "Verify Home > Corporate Home > Setup > Settings");


        try {
        	NavigationHelper.openWestMinster(_driver);
            
            corporateUserName = PropertyUtils.ReadProperty("CORPORATE_LOGIN", WCC_PROPERTIES);
    		corporatePassword = PropertyUtils.ReadProperty("CORPORATE_PASSWORD", WCC_PROPERTIES);
    		
            LoginHelper.loginCorporateWestminster(corporateUserName, corporatePassword, _driver);

            WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
            String corporateprofilePage = wcccoporateprofilepage.getCorporateProfilePageTitle();
            assertTrue(corporateprofilePage.contains("Welcome to your RingGo Corporate Account"));
            CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home");

            WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
            String corporateHomePageTitle = wcccorporatehomepage.getCoporateHomePageTitle();
            assertTrue(corporateHomePageTitle.contains("Corporate Account"));
            CheckResult(wcccorporatehomepage.clickSetUp(), "Clicking on Set up");

            WCCSetupPage wccsetuppage = new WCCSetupPage(_driver);
            String setupPageTitle = wccsetuppage.getSetUpPageTitle();
            assertTrue(setupPageTitle.contains("Setup"));
            CheckResult(wccsetuppage.clickSettings(), "Clicking on Settings");

            WCCCorporateSettingPage wcccorporatesettingpage = new WCCCorporateSettingPage(_driver);
            String settingsPageTitle = wcccorporatesettingpage.getSettingPageTitle();
            assertTrue(settingsPageTitle.contains("Settings"));
            wcccorporatesettingpage.clickSaveButton();
            String savesuccessMessgae = wcccorporatesettingpage.getSaveSuccessMessage();
            assertTrue(savesuccessMessgae.contains("Thank you, your settings have been updated"));

            CorporateMenu corporatemenu = new CorporateMenu(_driver);
            CheckResult(corporatemenu.clickAccount(), "Click account menu");
            CheckResult(corporatemenu.clickLogout(), "Click logout link");

        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
