package TestScenario;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import PageObjects.RingGoMobileHomePage;
import PageObjects.RingGoMobileIndexPage;
import PageObjects.RingGoParkingSessionMobilePage;
import PageObjects.WCCLoginPage;

public class RingGoMobileBuySessionWithNoCV2Test extends RingGoScenario {

    private static final String TIME = "1 Hour";
    private static final String ZONE_ID = "23856";

    private String carVrn;

	@BeforeMethod
	public void setUp() throws Exception {
		
		createUser("islingtonNewUser.json");
        carVrn = vehicleVrn;
       
	}
	// TODO - remove hardcoded data - should be injected
	
	@Test
	public void buyMobileSessionWithNoCV2() {
		
		 StartTest("134205", "Buy a session with no CV2");
		 
		 NavigationHelper.openRingoGoMobile(_driver);
	     LogStep("1", "Go to mobile version of MyRingGo and Login");

	     RingGoMobileIndexPage ringgomobileindexpage = new RingGoMobileIndexPage(_driver);
	     ringgomobileindexpage.clickLogin();

	     WCCLoginPage wccloginpage = new WCCLoginPage(_driver);

	     CheckResult(wccloginpage.inputEmailOrPhonenumer(email), "Enter email or phone number");
	     CheckResult(wccloginpage.enterPassword(password), "Enter password");
	     CheckResult(wccloginpage.clickLoginButton(), "Click login button");

	     LogStep("2", "Go to Park and Try to Book a session with no CV2 needed");
	     RingGoMobileHomePage ringgomobilehomepage = new RingGoMobileHomePage(_driver);
	     CheckResult(ringgomobilehomepage.clickParking(), "Click parking button");

	     RingGoParkingSessionMobilePage ringgoparkingsessionmobilepage = new RingGoParkingSessionMobilePage(_driver);

	     CheckResult(ringgoparkingsessionmobilepage.enterZone(ZONE_ID), "Enter zoneID");
	     CheckResult(ringgoparkingsessionmobilepage.selectVehicle(carVrn), "Click select vehicle");
	     CheckResult(ringgoparkingsessionmobilepage.clickNextButton(), "Click next button");
	     CheckResult(ringgoparkingsessionmobilepage.selectDuration(TIME), "Select duration");
	     CheckResult(ringgoparkingsessionmobilepage.selectSMSConfirm(), "Select SMS confirm");
	     CheckResult(ringgoparkingsessionmobilepage.selectReminderSMS(), "Select reminder SMS");
	     CheckResult(ringgoparkingsessionmobilepage.clickDurationNextButton(), "Click duration next button");

	     CheckResult(ringgoparkingsessionmobilepage.clickCardSelectNextButton(), "Click card select next button");
	     CheckResult(ringgoparkingsessionmobilepage.clickPay(), ":Click pay button");
	
	     CheckResult(ringgoparkingsessionmobilepage.clickFinish(), "Click finish button");
	}
	
}
