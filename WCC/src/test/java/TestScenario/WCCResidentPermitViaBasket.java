package TestScenario;

import static org.testng.AssertJUnit.assertTrue;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import Logging.Log;
import PageObjects.PaymentBasketFinishPage;
import PageObjects.WCCBasketConfirmationPage;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCResidentPermitInfoPage;
import PageObjects.WCCResidentPermitPage;
import PageObjects.WCCResidentPermitProofPage;
import PageObjects.WCCResidentPermitViaBasketPage;
import PageObjects.POHelpers.PaymentBasketTable;
import TestScenario.Corporate.WCCCorporateTopupNegTest;
import Utilities.MapUtils;

public class WCCResidentPermitViaBasket extends RingGoScenario {
    
    public String vehicleVrn;

	static Logger LOG = Logger.getLogger(WCCCorporateTopupNegTest.class);
	
	@BeforeMethod()
    public void setUp() throws Exception {
		
		createUser("defaultNewUser.json");	
        vehicleVrn = RestApiHelper.addNewVehicleNoVRM(mobile, password, RestApiHelper.defaultNewVehicle());
        vehicleVrn = vehicleVrn.toUpperCase()+" WHITE BMW";
		
    }

	@SuppressWarnings("unchecked")
	@Test(dataProvider="wcc_Permits_Portal", dataProviderClass=DataProviders.PermitDP.class)
	public void residentPermitApplicationViaBasket(String TCID, Map<Object,Object> Data)
	{
    StartTest("15166", "WCC- Resident Permit- Basket");

        try {
        	NavigationHelper.openWestMinster(_driver);
            WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
            CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

            WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
            String pageTitle = wccloginpage.getPageTitle();
            assertTrue(pageTitle.contains("Log in"));

            CheckResult(wccloginpage.inputEmailOrPhonenumer(email), "Enter email or phone number");
            CheckResult(wccloginpage.enterPassword(password), "Enter password");
            CheckResult(wccloginpage.clickLoginButton(), "Click login button");

            WCCHome wcchome = new WCCHome(_driver);
            String homePageTitle = wcchome.getHomePageTitle();
            assertTrue(homePageTitle.contains("Welcome"));
            CheckResult(wcchome.clickPermit(), "Click permit button");
        

            WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);
            String permitPageTitle = wccpermitpage.getPermitPageTitle();
            assertTrue(permitPageTitle.contains("Permits"));
            wccpermitpage.clickResidentPermit();

            WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);
            String residentPermitInfoPageTitle = wccresidentpermitinfopage.getResidentPermitInfoPageTitle();
            assertTrue(residentPermitInfoPageTitle.contains("Resident permit application: eligibility and process"));
            wccresidentpermitinfopage.clickStandardResidentPermit();

            WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
            String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
            assertTrue(residentPermitPageTitle.contains("Resident permit application"));
            wccresidentpermitpage.applyResidentPermitApplication(Data.get("Postcode").toString(), Data.get("Address").toString(),Data.get("Parking_zone").toString() );

            WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
            CheckContains(wccresidentpermitdetailspage.getResidentPermitDetailsPageTitle(), "Resident permit application", "My Details Permit TitleCheck");
            wccresidentpermitdetailspage.enterUserDetailsPermitDetails(Data.get("Title").toString(), Data.get("FirstName").toString(), Data.get("LastName").toString());
            wccresidentpermitdetailspage.fillPermitApplicationDetails(Data.get("Residency_type").toString(), vehicleVrn, Data.get("Ownership").toString());

            WCCResidentPermitProofPage wccresidentpermitproofpage = new WCCResidentPermitProofPage(_driver);
            wccresidentpermitproofpage.completePermitApplicationProofs(Data.get("Address_proof_type1").toString(), 
            		Data.get("Address_proof1").toString(), 
            		Data.get("Address_proof_type2").toString(), 
            		Data.get("Address_proof2").toString(), 
            		Data.get("Car_insurance_schedule_proof").toString(), 
            		Data.get("Certificate_of_insurance").toString(), 
            		Data.get("Vehicle_registration_document").toString());

            WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
            wccpermitdetailsconfirmationpage.confirmPermitDetails();
           
            
            WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
            String purchasepermitpagetitle = wccpurchasepermitpaymenttype.getPageTitle();
            assertTrue(purchasepermitpagetitle.contains("Purchase a resident permit"));
            wccpurchasepermitpaymenttype.payViaBasket();
          
            WCCResidentPermitViaBasketPage wccresidentpermitviabasket = new WCCResidentPermitViaBasketPage(_driver);
            CheckContains(wccresidentpermitviabasket.getBasketPageTitle(), "Your Basket", "Basket Page Title");
            
            Map<Object,Object> Exp_Res = MapUtils.GetChild(Data, "Exp_Results");
            Map<Object, Object> headings = MapUtils.GetChild(Exp_Res, "Heading");            
            CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(0).toString(), headings.get("Qty"), "Basket Headers : Qty");
            CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(1).toString(), headings.get("Item"), "Basket Headers : Item");
            CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(2).toString(), headings.get("Cost"), "Basket Headers : Cost");
            CheckValue(wccresidentpermitviabasket.basketTableHeaders().get(3).toString(), headings.get("Total"), "Basket Headers : Total ");
            
            PaymentBasketTable[] tableContents = PaymentBasketTable.Parse((List<Map<Object,Object>>)Exp_Res.get("Content"));
            for(int i = 0; i < tableContents.length; i++) {
            	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Qty, tableContents[i].Qty, "Basket Content : Qty");
            	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Item, tableContents[i].Item, "Basket Content : Item");
            	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Cost, tableContents[i].Cost, "Basket Content : Cost");
            	CheckValue(wccresidentpermitviabasket.basketContents().get(i).Total, tableContents[i].Total, "Basket Content : Total");
            }

            CheckResult(wccresidentpermitviabasket.purchaseViaBasket(), "Purchase Via Basket");
            
            
        
            WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
            String purchasecarddetailspagetitle = wccpaymentcarddetailspage.getPageTitle();
            CheckValue(purchasecarddetailspagetitle, "Pay your Basket", "Asserting Page Header");
            wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();

            WCCBasketConfirmationPage wccconfirmbasketpayment = new WCCBasketConfirmationPage(_driver);
            String wccconfirmbasketpaymenttitle = wccconfirmbasketpayment.getConfirmBasketPageTitle();
            CheckValue(wccconfirmbasketpaymenttitle, "Pay your Basket", "Confirm Basket Payment");
            wccconfirmbasketpayment.confirmPayViaBasket();          
            
            
            WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
            String paymentcardcvvpagetitle = wccpaymentcardcvvpage.getPageTitle();
            assertTrue(paymentcardcvvpagetitle.contains("Confirm payment"));
            wccpaymentcardcvvpage.enterCV2AndProceedPermitBasketPayment(Data.get("CV2").toString());

            PaymentBasketFinishPage wccpermitpurchasefinishpage = new PaymentBasketFinishPage(_driver);
            String wccpermitpurchasefinishpagetitle = wccpermitpurchasefinishpage.getPageTitle();
            assertTrue(wccpermitpurchasefinishpagetitle.contains("Pay your Basket"));
            wccpermitpurchasefinishpage.clickFinish();

            //user logout
            CheckResult(wcchome.logout(), "Click logout button");
        } catch (AssertionError | Exception e) {
            LogError("Failed due -> " + e.getMessage());
            Log.add(LogStatus.FAIL, "Test is failed");
            Assert.fail("Failed as verification failed -'" + e.getMessage());
        }

    }

}
