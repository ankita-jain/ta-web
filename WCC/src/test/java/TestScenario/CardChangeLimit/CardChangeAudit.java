package TestScenario.CardChangeLimit;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.InsightCardAuditPage;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;

public class CardChangeAudit extends RingGoScenario {
	
	    @BeforeMethod
	    public void setUp() throws Exception {
	        createUser("defaultNewUser.json");
	    }

	    @Test
	    public void cardchangeaudit() {
	        StartTest("311178", "View Card Audit Page Exists");

	        LogStep("Step - 1", "Go to Insight");
	        NavigationHelper.openInsight(_driver);
	        LogStep("Step - 2", "Enter login and password");
	        LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);

	        LogStep("Step - 3", "Select Telephone Number from Search dropdown and enter {random_cli} to Search Term");
	        InsightSearch insightSearch = new InsightSearch(_driver);
	        CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI");
	        LogStep("Step - 4", "Click \"Search button\"");
	        CheckResult(insightSearch.clickSubmit(), "Click 'Search' button");
	        
	        LogStep("Step - 5", "Click on 'View Card Audit' link");
	        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
	        CheckResult(insightProfilePage.clickOnCardAudit(), "Click on 'View Card Audit' link");
	        LogStep("Step - 6", "Check the Header for Card Audit History");
	        
	        InsightCardAuditPage cardauditPage = new InsightCardAuditPage(_driver);
	        CheckContains(cardauditPage.getCardAuditHeader(), "Card Audit History", "Header text");
	    }
}
