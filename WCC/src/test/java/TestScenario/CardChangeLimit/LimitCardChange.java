package TestScenario.CardChangeLimit;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.PaymentCards;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Map;
import static Utilities.TimerUtils.delay;

public class LimitCardChange extends RingGoScenario{

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("islingtonNewUser.json");
    }
    
    @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
    public void multipleCardEdits(String TCID, Map<String, String> data) throws Exception {
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Second_card"), data.get("Second_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Third_card"), data.get("Third_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Fourth_card"), data.get("Fourth_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Fifth_card"), data.get("Fifth_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Sixth_card"), data.get("Sixth_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Seventh_card"), data.get("Seventh_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Eighth_card"), data.get("Eighth_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Ninth_card"), data.get("Ninth_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Tenth_card"), data.get("Tenth_card_expiries"));
    	RestApiHelper.addNewPaymentCard(email, password, data.get("Eleventh_card"), data.get("Eleventh_card_expiries"));
    	
    	StartTest(TCID, "open RingGoWeb");
    	NavigationHelper.openMyRingo(_driver);
    	LogStep("Step - 2", "Enter login and password");
    	LoginHelper.loginMyRingo(email, password, _driver);
    	LogStep("Step - 3", "Click on Account Menu");
    	HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().clickLogin(), "Account link");
        LogStep("Step - 4", "Click on Payment Cards");
        CheckResult(homePageMyRinggo.getUpperRightNavigationMenu().getAccountToggleMenu().clickPaymentCards(), "Click PaymentCard Link");
        LogStep("Step - 5", "Click on Edit Payment Card");
        PaymentCards paymentCardEdit = new PaymentCards(_driver);
        CheckResult(paymentCardEdit.clickEdit(), "Click on Edit");
        LogStep("Step - 6", "Click on Save Payment Card");
        delay(50000);
        PaymentCards saveCardEdit = new PaymentCards(_driver);
        LogStep("Step - 7", "Check for Appropriate Error Message");
        CheckResult(saveCardEdit.clickSave(), "Click on Save");
        CheckValue(saveCardEdit.getErrorMessageText(), "The maximum number of card changes has been reached on your account. Please contact our customer care team for further assistance.", "Error message");
       
        
    }
   
    
}

  

   
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  
  
  
  
  
  
  
  
  

  
  

  
  

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 