package GenericComponents;

import Utilities.PropertyUtils;
import java.io.IOException;

import static GenericComponents.RingGoScenario.WCC_PROPERTIES;
import static GenericComponents.constants.Symbols.POUND;

public class UtilityClass {

    public static String getCurrentBalanceFromText(String currentBalText) {
        String[] arrayOfStrings = currentBalText.split("\\s", -1);
        String currentBalance = null;
        for (String words : arrayOfStrings) {

            if (words.contains(POUND.getAsUTF8())) {
                currentBalance = words.replaceAll(String.format("[-+^:,%s]", POUND.getAsUTF8()), "");
                break;
            }

        }
        return currentBalance;

    }

    /*
     * Function 'getCurrentBalanceFromText' will take a string and
     * Returns current balance from text, For instance, if the string is ""Current funds balance: £7,771.70 Top Up"
     * Then, it will return '7771.70' from the string.
     */

    public String reading_properties(String sKey) throws IOException {
        try {
        	return PropertyUtils.ReadProperty(sKey, WCC_PROPERTIES);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getFeatureToggleState(){
        try {
            return PropertyUtils.ReadProperty("FEATURE_TOGGLE", WCC_PROPERTIES);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
