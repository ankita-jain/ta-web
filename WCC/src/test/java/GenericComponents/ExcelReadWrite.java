package GenericComponents;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import Utilities.OSUtils;

public class ExcelReadWrite {

    FileInputStream fis;

    /**
     * The wb.
     */
    HSSFWorkbook wb;


    /**
     * Instantiates a new excel read write.
     *
     * @param xlPath the xl path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public ExcelReadWrite(String xlPath) throws IOException {

		// Obtain the path to use if system property is set (enable build environment switching)
		String fullTestDataPath = OSUtils.getFullTestDataPath();
		
        fis = new FileInputStream(fullTestDataPath + xlPath);

        //workbook object
        wb = new HSSFWorkbook(fis);

    }


    /**
     * Setsheet.
     *
     * @param sheetname the sheetname
     * @return the HSSF sheet
     */
    public HSSFSheet Setsheet(String sheetname) {
        HSSFSheet Sheet = wb.getSheet(sheetname);
        return Sheet;

    }


    /**
     * Gets the rowcount.
     *
     * @param Sheet the sheet
     * @return the rowcount
     */
    public int getrowcount(HSSFSheet Sheet) {
        int lastRowNum = Sheet.getLastRowNum();
        return lastRowNum;

    }

    /**
     * Gets the colcount.
     *
     * @param Sheet    the sheet
     * @param rowIndex the row index
     * @return the colcount
     */
    public int getcolcount(HSSFSheet Sheet, int rowIndex) {
        int lastcolnum = Sheet.getRow(rowIndex).getLastCellNum();
        return lastcolnum;

    }

    /**
     * Readvalue.
     *
     * @param Sheet    the sheet
     * @param rowIndex the row index
     * @param cellnum  the cellnum
     * @return the string
     */
    public String Readvalue(HSSFSheet Sheet, int rowIndex, int cellnum) {
        HSSFCell cell = Sheet.getRow(rowIndex).getCell(cellnum);

        String celltext = null;

        if (cell == null)
            celltext = "";

        else if (cell.getCellType() == Cell.CELL_TYPE_STRING)
            celltext = cell.getStringCellValue();

        else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC)
            celltext = String.valueOf(cell.getNumericCellValue());

        else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
            celltext = "";

        return celltext;
    }


    /**
     * Readvalue.
     *
     * @param Sheet    the sheet
     * @param rowIndex the row index
     * @param colname  the colname
     * @return the string
     */
    public String Readvalue(HSSFSheet Sheet, int rowIndex, String colname) {

        int colindex = 0;
        for (int i = 0; i < getcolcount(Sheet, 0); i++) {


            //System.out.println(row.getCell(i).getStringCellValue().trim());
            if (Sheet.getRow(0).getCell(i).getStringCellValue().trim().equalsIgnoreCase(colname))
                colindex = i;
        }


        return Readvalue(Sheet, rowIndex, colindex);


    }

    /**
     * Writecell.
     *
     * @param Sheet    the sheet
     * @param rowIndex the row index
     * @param cellnum  the cellnum
     * @param wvalue   the wvalue
     */
    public void writecell(HSSFSheet Sheet, int rowIndex, int cellnum, String wvalue) {
        //writing the cell
        HSSFCell writecell = Sheet.getRow(rowIndex).getCell(cellnum);
        if (writecell == null) {
            writecell = Sheet.getRow(rowIndex).createCell(cellnum);
        }

        writecell.setCellValue(wvalue);
    }

    /**
     * Writecell.
     *
     * @param Sheet    the sheet
     * @param rowIndex the row index
     * @param colname  the colname
     * @param wvalue   the wvalue
     */
    public void writecell(HSSFSheet Sheet, int rowIndex, String colname, String wvalue) {
        int colindex = 0;
        for (int i = 0; i < getcolcount(Sheet, 0); i++) {


            //System.out.println(row.getCell(i).getStringCellValue().trim());
            if (Sheet.getRow(0).getCell(i).getStringCellValue().trim().equalsIgnoreCase(colname))
                colindex = i;
        }


        writecell(Sheet, rowIndex, colindex, wvalue);


    }

    /**
     * Save excel.
     *
     * @param xlPath the xl path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void save_excel(String xlPath) throws IOException {
        fis.close();

		// Obtain the path to use if system property is set (enable build environment switching)
		String fullTestDataPath = OSUtils.getFullTestDataPath();
		
        FileOutputStream fos = new FileOutputStream(fullTestDataPath + xlPath);
        wb.write(fos);
        fos.close();

    }

}
