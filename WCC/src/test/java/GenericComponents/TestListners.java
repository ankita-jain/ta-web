package GenericComponents;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class TestListners implements ITestListener {
    static Logger log = Logger.getLogger(TestListners.class);

    @Override
    public void onFinish(ITestContext arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStart(ITestContext arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestSkipped(ITestResult arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test started running:" + result.getMethod().getMethodName() + " at:" + result.getStartMillis());

    }

    @Override
    public void onTestSuccess(ITestResult result) {

        log.info("Test Success:" + result.getMethod().getMethodName() + " at:" + result.getStartMillis());

    }


}
