package GenericComponents.complexwebobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class Table {
    private WebElement table;

    public Table(WebElement table) {
        this.table = table;
    }

    public List<WebElement> getHeaders() {
        return table.findElements(By.xpath("//th"));
    }

    public List<List<WebElement>> getRows() {
        return table
                .findElements(By.xpath(".//tr"))
                .stream()
                .map(rowElement -> rowElement.findElements(By.xpath(".//td")))
                .filter(row -> row.size() > 0)
                .collect(toList());
    }

    public List<WebElement> getCellsInARow(int index) {
        return getRows().get(index);
    }

    public int getHeaderIndex(String header) {
        List<WebElement> listHeaders = getHeaders();
        for (int i = 0; i < listHeaders.size(); i++) {
            if (listHeaders.get(i).getText().equals(header))
                return i;
        }
        throw new RuntimeException(String.format("There is no %s header", header));
    }


    public <T> String getCellValue(int rowIndex, T header) {
        WebElement cellsList = table.findElements(By.xpath("//tr")).get(rowIndex);

        WebElement searchedCell = cellsList.findElements(By.xpath(".//td")).get(getHeaderIndex(header.toString()));

        return searchedCell.getText();
    }

    public <T> WebElement getCell(int rowIndex, T header) {
        WebElement cellsList = table.findElements(By.xpath("//tr")).get(rowIndex);

        return cellsList.findElements(By.xpath(".//td")).get(getHeaderIndex(header.toString()));
    }

    public WebElement getCell(int rowIndex, int cellIndex) {
        WebElement cellsList = table.findElements(By.xpath("//tr")).get(rowIndex);

        return cellsList.findElements(By.xpath(".//td")).get(cellIndex);
    }

    /**
     * @param value  is a cell content text
     * @param header is a name of a table header
     * @param <T>    variable of this type converting to String represent the name of header
     * @return -1 if there is not such row or row index >= 1
     */
    public <T> int getRowIndexByCellValue(String value, T header) {
        int cellIndex = getHeaderIndex(header.toString());

        Optional<List<WebElement>> rowElements = getRows()
                .stream()
                .filter(x -> x.get(cellIndex).getText().equals(value))
                .findFirst();

        return rowElements.isPresent() ? getRows().indexOf(rowElements.get()) + 1 : -1;
    }

    public <T> WebElement getRowByCellValue(String value, T header) {
        int rowIndex = getRowIndexByCellValue(value, header);
        return table.findElements(By.xpath("//tbody/tr")).get(rowIndex - 1);
    }

    public WebElement getTableAsAnElement() {
        return table;
    }

    public <T> boolean IsRowWithValueExists(String value, T header) {
        int rowNumber = getRows().size();
        int i = 1;
        do {
            if (getCellValue(i, header).equals(value))
                return true;
            i++;
        }
        while (i <= rowNumber);
        return false;
    }
}

