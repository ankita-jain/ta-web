package GenericComponents;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.testng.annotations.BeforeSuite;

import GenericComponents.helper.RestApiHelper;
import SeleniumHelpers.Web_Scenario;
import Utilities.PropertyUtils;

public class RingGoScenario extends Web_Scenario {

	public static final String WCC_PROPERTIES = "WCCTest.properties";
    public static final String ENV_PROPERTIES = "Environment.properties"; // TODO - this is redundant - see superclass
    
    // Properties and variables inherited by all WCC web tests
	// (only initialized on suite start, so must be static)
	protected static Properties wccproperties;
    protected static String insightUserLogin;
    protected static String insightPermitUser;
    protected static String insightCallCentreLogin;
    protected static String insightPassword;
    protected static String corporateLogin;
    protected static String corporatePassword;
    protected static String newCorporateEmail;
    protected static String newCorporatePassword;

    // TODO - start to eliminate the member variables for newuserJson, email, password, mobile, userVehicles, vrm, vehicleVrn, userCardNumber, etc

    // New user properties (per instance) - these are used by most methods/sub-classes
    protected Map<Object,Object> newUserJson=null;
    protected String email="";
    protected String password="";
    protected String mobile="";
    protected List<Map<Object, Object>> userVehicles;
    protected String vrm;
    protected String vehicleVrn;
    protected String userCardNumber;
    
	//@BeforeClass
	@BeforeSuite
	public void startUp() throws Exception
	{
		//initialise the (static) properties object
		wccproperties = PropertyUtils.Load(WCC_PROPERTIES);

		insightUserLogin = wccproperties.getProperty("INSIGHT_LOGIN", "");		
		insightPermitUser = wccproperties.getProperty("INSIGHT_PERMIT_USER_LOGIN", "");
		insightCallCentreLogin = wccproperties.getProperty("INSIGHT_CALL_CENTRE_USER_LOGIN", "");
		insightPassword = wccproperties.getProperty("INSIGHT_PASSWORD", "");
        corporateLogin = wccproperties.getProperty("CORPORATE_LOGIN", "");
        corporatePassword = wccproperties.getProperty("CORPORATE_PASSWORD", "");
        newCorporateEmail = wccproperties.getProperty("REBRANDED_CORPORATE_LOGIN", "");
        newCorporatePassword = wccproperties.getProperty("REBRANDED_CORPORATE_PASSWORD", "");
	}

    protected static final String RESIDENT_PERMIT_TXT_FILE = "testdata\\residentpermit.txt";
    protected static final String RESIDENT_PERMIT_ECO_TXT_FILE = "testdata\\residentecopermit.txt";
    protected static final String RESIDENT_PERMIT_MOTO_TXT_FILE = "testdata\\residentmotorcyclepermit.txt";
    protected static final String RESIDENT_DISABLE_PERMIT_TXT_FILE = "testdata\\residentdisablepermit.txt";
    protected static final String RESIDENT_TRADE_PERMIT_TXT_FILE = "testdata\\residenttradepermit.txt";
    protected static final String RESIDENT_WHITE_AND_BLUE_TXT_FILE = "testdata\\residentdisablepermitwhitebludebadge.txt";
    protected static final String RESIDENT_WHITE_BADGE_TXT_FILE = "testdata\\residentdisablepermitwhitebadge.txt";
    protected static final String RESIDENT_INSIGHT_PERMIT = "testdata\\vrnchangeepermit.txt";

    
    /**
     * Create a new user, using the default new user structure
     * This currently sets various member variables in RingGoScenario (to be eliminated)
     * @throws Exception
     * @return Map<Object,Object> new user json structure
     */
    public Map<Object,Object> createUser() throws Exception
    {
		return createUser("defaultNewUser.json");
	}

    /**
     * Create a new user, using the JSON filename provided (from the test data folder)
     * This currently sets various member variables in RingGoScenario (to be eliminated)
     * @param jsonFilename
     * @throws Exception
     * @return Map<Object,Object> new user json structure
     */
    @SuppressWarnings("unchecked")
	public Map<Object,Object> createUser(String jsonFilename) throws Exception
    {
    	newUserJson = RestApiHelper.createNewUser(RestApiHelper.getNewUserData(getSuiteStartDate(),jsonFilename));
        email = (String) newUserJson.get("Email");
        password = (String) newUserJson.get("Password");
    	mobile = (String) newUserJson.get("Mobile");

    	userVehicles=null;
		try {
			userVehicles = (List<Map<Object, Object>>) newUserJson.get("Vehicles");
	        vrm = userVehicles.get(0).get("VRM").toString();
	        vehicleVrn = vrm.toUpperCase();
		} catch (Exception e) {
			vrm=null;
			vehicleVrn=null;
		}
        try
        {
        	userCardNumber = (String) ((List<Map<Object,Object>>) newUserJson.get("Cards")).get(0).get("CardNumber");
        } catch (Exception ex) {
        	userCardNumber=null;
        }
        
        return newUserJson;
    }

    public void switchToANewTabAndReturnBack(String pageUrl) {
        List<String> tabs2 = new ArrayList<>(_driver.getWindowHandles());
        _driver.switchTo().window(tabs2.get(1));
        CheckBool(_driver.getCurrentUrl().equals(pageUrl), String.format("New page should get opened -> %s", pageUrl));
        _driver.close();
        _driver.switchTo().window(tabs2.get(0));
    }

}