package GenericComponents;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import Utilities.OSUtils;

public class BaseClass {

    public static String emailReport;
    public static String filePathReport = null;
    public static int failCount = 0;
    public static int passCount = 0;
    /**
     * Author: Sabya The log.
     */
    static Logger log = Logger.getLogger(BaseClass.class);
    public WebDriver driver;
    public FileReader fileReader;
    public UtilityClass utilityclass = new UtilityClass();
    /**
     * The report.
     */
    protected ExtentReports report;
    /**
     * The logger.
     */
    protected ExtentTest logger;
    /**
     * The fail counter.
     */
    int failCounter = 0;
    /**
     * The date.
     */
    Date date = new Date();
    /**
     * The dateformat.
     */
    SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
    final String filePath = System.getProperty("user.dir") + "\\testreports\\WCC_" + dateformat.format(date) + ".html";

    /**
     * Gets the suite name.
     *
     * @param context the context
     * @return the suite name
     */
    @BeforeSuite
    public static void getSuiteName(ITestContext context) {
        emailReport = context.getCurrentXmlTest().getSuite().getName();
    }

    public static String dateEngine(String date_period) throws ParseException {

        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String DateToStr;

        Calendar c = Calendar.getInstance();
        c.setTime(curDate);

        if (date_period.equals("today")) {
            c.add(Calendar.DATE, 0); //Current date
        } else if (date_period.equals("week")) {
            c.add(Calendar.DATE, 6); //next week date
        } else if (date_period.equals("next_year")) {
            c.add(Calendar.DATE, 364); //next year date
        } else if (date_period.equals("temp_interim")) {
            c.add(Calendar.DATE, 41); //Adding 42 days from the date of application
        } else if (date_period.equals("three_years")) {
            c.add(Calendar.DATE, 1095); //Adding 36 months from the date of application
        }


        Date currentDatePlusOne = c.getTime();
        DateToStr = format.format(currentDatePlusOne);

        return DateToStr;
    }

    @Parameters("device")
    @BeforeMethod
    public void initBrowser(@Optional("CH") String device) throws IOException {

        System.out.println("driver going to init()");
        if (device.equals("CH")) {
            initChrome();
        } else if (device.equals("FF")) {
            initFirefox();
        } else if (device.equals("IE")) {
            initInternetExplorer();
        }
    }

    private void initInternetExplorer() throws IOException {

        System.setProperty("webdriver.ie.driver", System.getProperty("user.dir") + utilityclass.reading_properties("IE_DRIVER_PATH"));
        System.out.println("IE init---->>>>");
        driver = new InternetExplorerDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(utilityclass.reading_properties("URL"));
    }

    private void initFirefox() throws IOException {

        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + utilityclass.reading_properties("FIREFOX_GECKO_DRIVER"));
        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "false");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(utilityclass.reading_properties("URL"));
    }

    private void initChrome() throws IOException {

        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + utilityclass.reading_properties("CHROME_DRIVER_PATH"));

        ChromeOptions option = new ChromeOptions();
        option.addArguments("disable-infobars");

        driver = new ChromeDriver(option);
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.get(utilityclass.reading_properties("URL"));

    }

    @AfterMethod
    public void tearDown() {
        System.out.println("driver going to quit");
        driver.close();
    }

    public String takeSnapshot(String testname) throws IOException {

        String screenshotpath;
        Date date = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
        File file = new File(dateformat.format(date) + ".png");


        TakesScreenshot screenshot = (TakesScreenshot) driver;
        File screenshotAs = screenshot.getScreenshotAs(OutputType.FILE);
        String filepath = System.getProperty("user.dir") + utilityclass.reading_properties("SCREENSHOT_PATH");
        System.out.println("Screenshot path" + filepath);

        File screenshotDir = new File(filepath);
        if (!screenshotDir.exists()) {
            screenshotDir.mkdir();
        }

        screenshotpath = System.getProperty("user.dir") + utilityclass.reading_properties("SCREENSHOT_PATH") + testname + "-" + file;
        FileUtils.copyFile(screenshotAs, new File(screenshotpath));
        System.out.println(screenshotpath + "Screenshot path------>>");

        return screenshotpath;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public String getScreenshotPath() {
        String screenshotpath = null;
        try {
            screenshotpath = utilityclass.reading_properties("SCREENSHOT_PATH");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return screenshotpath;
    }

    /**
     * After method.
     *
     * @param result the result
     */
    @AfterMethod
    protected void afterMethod(ITestResult result) {

        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed " + result.getThrowable());
            System.out.println("Here....");
            failCounter = failCounter + 1;
        } else if (result.getStatus() == ITestResult.SKIP) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test skipped " + result.getThrowable());

        }
        if (!result.isSuccess()) {
            //Increment Fail Count
            failCount++;
        } else {
            passCount++;
        }


        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());
        ExtentManager.getReporter().flush();


    }

    /**
     * Check result.
     *
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @AfterSuite
    protected void checkResult() throws IOException {
        System.out.println("Inside check result-failure-" + ITestResult.FAILURE + "Inside check result-Pass-" + ITestResult.SUCCESS_PERCENTAGE_FAILURE);
        System.out.println(emailReport);
        //int strAssertRes=ITestResult.FAILURE;
        System.out.println("Fail Total" + failCount);
        System.out.println("Pass Total" + passCount);
        //UtilityClass strUtil= new UtilityClass();
        // if(failCounter>0 || failCount>0)
        // {
         	/*final String username = strUtil.reading_properties("mailUsername");
     	    final String password = strUtil.reading_properties("mailPassword");
     	    String strToEmail=strUtil.reading_properties("mailTo");
     	    String strSubject=strUtil.reading_properties("mailSubject")+"  :: "+emailReport;*/
        //sendEmail(username,  password,strToEmail,  strSubject,filePathReport);
        // }

    }

    /**
     * Gets the stack trace.
     *
     * @param t the t
     * @return the stack trace
     */
    protected String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        return sw.toString();
    }

    public void writeToFile(String permittype, String permitid) {

        try {
            if (deleteFile(permittype))

            {
                FileWriter fileWriter = new FileWriter(OSUtils.getFullTestDataPath() + utilityclass.reading_properties("LOCAL_STORAGE_FILE_" + permittype));
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(permitid);
                bufferedWriter.close();
            }
        } catch (IOException ex) {
            System.out.println(">>> Can't write to File >>>");
        }
    }

    /*
     * File Read - Write function for writing the permit id to a local file and reading from it.
     * For instance, when user successfully apply for
     */

    public String readFromFile(String permittype) {
        String permitnumber = null;
        try {
            FileReader fileReader = new FileReader(OSUtils.getFullTestDataPath() + utilityclass.reading_properties("LOCAL_STORAGE_FILE_" + permittype));
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            if ((permitnumber = bufferedReader.readLine()) != null) {
                System.out.println("Pemit Number >>>" + permitnumber);
            }

            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file >>>");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return permitnumber;
    }

    public boolean deleteFile(String permittype) {
        boolean fileDelete = false;
        try {
            File file = new File(OSUtils.getFullTestDataPath() + utilityclass.reading_properties("LOCAL_STORAGE_FILE_" + permittype));

            if (!file.exists()) {
                fileDelete = true;
            } else if (file.exists() && file.delete()) {
                System.out.println(file.getName() + " is deleted!");
                fileDelete = true;
            } else {
                System.out.println("Delete operation is failed.");
                fileDelete = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return fileDelete;
    }

    public String convertInsightDateFormatToStandardFormat(String date) throws ParseException {
        return new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("dd MMM yyyy").parse(date));

    }

    /**
     * The Class ExtentTestManager.
     */
    public static class ExtentTestManager {

        /**
         * The extent test map.
         */
        static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();

        /**
         * The report.
         */
        static ExtentReports report = ExtentManager.getReporter();

        /**
         * Gets the test.
         *
         * @return the test
         */
        public synchronized static ExtentTest getTest() {
            return extentTestMap.get((int) (Thread.currentThread().getId()));

        }

        /**
         * End test.
         */
        public static synchronized void endTest() {
            report.endTest(extentTestMap.get((int) (Thread.currentThread().getId())));
        }

        /**
         * Start test.
         *
         * @param testName the test name
         * @return the extent test
         */
        public static synchronized ExtentTest startTest(String testName) {
            return startTest(testName, "");
        }


        /**
         * Start test.
         *
         * @param testName the test name
         * @param desc     the desc
         * @return the extent test
         */
        public static synchronized ExtentTest startTest(String testName, String desc) {
            ExtentTest test = report.startTest(testName, desc);
            extentTestMap.put((int) (Thread.currentThread().getId()), test);

            return test;
        }
    }

    /**
     * The Class ExtentManager.
     */
    public static class ExtentManager {

        final static String folderPath = System.getProperty("user.dir") + "\\testreports\\";
        /**
         * The report.
         */
        static ExtentReports report;
        /**
         * The date.
         */
        static Date date = new Date();

        /** The str util REP. */
        //static Utility_Class strUtilREP= new Utility_Class();
        /**
         * The dateformat.
         */
        static SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
        /**
         * The Constant filePath.
         */
        final static String filePath = System.getProperty("user.dir") + "\\testreports\\" + emailReport + dateformat.format(date) + ".html";

        /**
         * Gets the reporter.
         *
         * @return the reporter
         */
        @SuppressWarnings("deprecation")
        public synchronized static ExtentReports getReporter() {
            if (report == null) {

                File screenshotDir = new File(folderPath);
                if (!screenshotDir.exists()) {
                    screenshotDir.mkdir();
                }

                report = new ExtentReports(filePath, true);
                report
                        .addSystemInfo("Host Name", "WCC")
                        .addSystemInfo("Environment", "Preprod");
                report.config().reportName("WCC");
                report.config().reportHeadline("[ " + emailReport + " ]");
            }

            filePathReport = filePath;
            return report;
        }
    }


}
