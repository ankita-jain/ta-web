package GenericComponents.enums;

public enum Month {
    JAN("Jan", "01"),
    FEB("Feb", "02"),
    MAR("Mar", "03"),
    APR("Apr", "04"),
    MAY("May", "05"),
    JUN("Jun", "06"),
    JUL("Jul", "07"),
    AUG("Aug", "08"),
    SEP("Sep", "09"),
    OCT("Oct", "10"),
    NOV("Nov", "11"),
    DEC("Dec", "12");

    private String monthName;

    private String monthNumber;

    Month(String name, String value) {
        monthName = name;
        monthNumber = value;
    }

    @Override
    public String toString() {
        return monthName;
    }

    public String getMonthNumber() {
        return monthNumber;
    }

    public String getMonthName() {
        return monthName;
    }
}
