package GenericComponents.constants;

public enum Symbols {
    POUND("\u00A3");

    private final String name;

    Symbols(String s) {
        name = s;
    }

    public String getAsUTF8() {
        return name;
    }
}