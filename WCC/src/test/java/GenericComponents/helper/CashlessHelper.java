package GenericComponents.helper;

import static Utilities.TimerUtils.delay;

import org.openqa.selenium.WebDriver;

import PageObjects.WCCAddVehiclePage;
import PageObjects.Insight.EditAVehicleLookupPage;
import PageObjects.Insight.InsightWCCHome;
import PageObjects.Insight.SupportPage;
import PageObjects.Insight.VRNLookupTablePage;
import PageObjects.myringo.BookParkingPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;
import SeleniumHelpers.Logging;
import Utilities.TimerUtils;

public class CashlessHelper{

	public static void createSession(String email, String password, String zone, String duration, String code, String carVrn, WebDriver _driver) {
		HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
		LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);

		Logging.LogStepPass("Pre-condition", "===Pre-condition starts===");

		Logging.LogStepPass("Step - 1", "Go to MyRingo");
		NavigationHelper.openMyRingo(_driver);

		Logging.LogStepPass("Step - 2", "Click 'Login'");
		Logging.CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login", _driver);
		Logging.CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link", _driver);

		Logging.LogStepPass("Step - 3", "Input 'Mobile number or E-mail' & password and click 'Log In'");
		Logging.CheckResult(loginRegisterPage.enterMobileOrEmail(email), String.format("Input 'Mobile number or E-mail -> %s' ", email), _driver);
		Logging.CheckResult(loginRegisterPage.enterPassword(password), String.format("Input 'Password' -> %s", password), _driver);
		Logging.CheckResult(loginRegisterPage.clickLogin(), "Click 'Log in' button", _driver);

		Logging.CheckResult(homePage.getMyRingGoMainMenuNavigation().clickPark(), "Click on 'Park' menu", _driver);

		BookParkingPage bookParkingPage = new BookParkingPage(_driver);
		Logging.CheckResult(bookParkingPage.getBookBlock().enterZone(zone), "Enter text on 'Zone' input", _driver);
		Logging.CheckResult(bookParkingPage.getBookBlock().selectVRN(carVrn), "Select vehicle", _driver);

		Logging.CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'", _driver);

		Logging.CheckResult(bookParkingPage.getBookBlock().selectNowRadioButton(), "Click on 'Now' radio button", _driver);
		Logging.CheckResult(bookParkingPage.getBookBlock().clickNext(), "Click on 'Next button'", _driver);

		Logging.CheckResult(bookParkingPage.getDurationSessionBlock().selectDuration(duration), String.format("Select '%s' from dropdown", duration), _driver);
		Logging.CheckResult(bookParkingPage.getDurationSessionBlock().clickNext(), "Click 'Next' button", _driver);
		Logging.CheckResult(bookParkingPage.getPayBlock().clickNext(), "Click on 'Next' button", _driver);

		Logging.CheckResult(bookParkingPage.getPayBlock().clickPay(), "'Click on 'Pay' button", _driver);
		Logging.CheckResult(bookParkingPage.getPayBlock().enterCvvCode(code), "Enter CVV code", _driver);
		Logging.CheckResult(bookParkingPage.getPayBlock().clickPayConfirmation(), "'Click on 'Pay Confirmation' button", _driver);

		delay(3000);
		Logging.CheckResult(bookParkingPage.getFinishBlock().clickFinish(), "Click on ''Finish button", _driver);

		Logging.LogStepPass("Pre-condition", "===Pre-condition ends===");
	}

	public static void changeCarPetrolType(String userName, String password,
			String VRN, String yearOfManufacture, String CO2,
			String engineSize, String vehicleType, String fuelType, WebDriver _driver) {
		NavigationHelper.openInsight(_driver);

		LoginHelper.loginInsight(userName, password, _driver);

		InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
		Logging.CheckResult(insightWCCHome.clickSupportMenu(), "Support menu selected", _driver);

		SupportPage supportPage = new SupportPage(_driver);
		Logging.CheckResult(supportPage.getLeftSideBar().clickVrnLookup(), "Click on lookup link ", _driver);

		VRNLookupTablePage vrnLookupTablePage = new VRNLookupTablePage(_driver);
		Logging.CheckResult(vrnLookupTablePage.enterVrn(VRN), "Enter VRN number", _driver);
		Logging.CheckResult(vrnLookupTablePage.clickSubmit(), "Click on submit button", _driver);
		Logging.CheckResult(vrnLookupTablePage.clickEditThisVehicle(), "Click on 'Edit this vehicle'", _driver);

		EditAVehicleLookupPage editAVehicleLookupPage = new EditAVehicleLookupPage(_driver);
		Logging.CheckResult(editAVehicleLookupPage.enterYearOfManufacture(yearOfManufacture), "Enter Year Of Manufacture", _driver);
		Logging.CheckResult(editAVehicleLookupPage.enterCO2(CO2), "Enter car's CO2", _driver);
		Logging.CheckResult(editAVehicleLookupPage.enterEngineSize(engineSize), "Enter car's engine size", _driver);
		Logging.CheckResult(editAVehicleLookupPage.selectVehicleType(vehicleType), "Select car's vehicle type", _driver);
		Logging.CheckResult(editAVehicleLookupPage.selectFuelFuelType(fuelType), "Select Fuel Type", _driver);
		Logging.CheckResult(editAVehicleLookupPage.clickSave(), "Click on Save button", _driver);

		delay(120000);
	}

	public static void fillMandatoryFieldsAddVehicle(String numberPlate, String colour, String make, String type, WebDriver driver) {
	    WCCAddVehiclePage addVehiclePage = new WCCAddVehiclePage(driver);
	    Logging.CheckResult(addVehiclePage.enterNumberPlate(numberPlate), "Enter " + numberPlate + " to Number plate",driver);
	    Logging.CheckResult(addVehiclePage.selectColour(colour), "Select " + colour + " from Colour dropdown",driver);
	    Logging.CheckResult(addVehiclePage.selectMake(make), "Select " + make + " from Make dropdown",driver);
	    TimerUtils.delay(1000);
	    Logging.CheckResult(addVehiclePage.selectType(type), "Select " + type + " from Type dropdown",driver);
	    TimerUtils.delay(1000);
	}
}
