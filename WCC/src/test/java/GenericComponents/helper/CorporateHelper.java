package GenericComponents.helper;

import static GenericComponents.UtilityClass.getFeatureToggleState;
import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.Corporate.WCCCorporateAddCostCentrePage;
import PageObjects.Corporate.WCCCorporateAddEmployeePage;
import PageObjects.Corporate.WCCCorporateAddPaymentCardDetailsPage;
import PageObjects.Corporate.WCCCorporateAdminAddPage;
import PageObjects.Corporate.WCCCorporateAssetsPage;
import PageObjects.Corporate.WCCCorporateCostCenterPage;
import PageObjects.Corporate.WCCCorporateDeleteConfirmationPage;
import PageObjects.Corporate.WCCCorporateEmployeesPage;
import PageObjects.Corporate.WCCCorporateHomePage;
import PageObjects.Corporate.WCCCorporatePaymentCardPage;
import PageObjects.Corporate.WCCCorporateProfilePage;
import PageObjects.Corporate.WCCSetupPage;
import PageObjects.Corporate.WccCorporateAdministrator;
import PageObjects.Corporate.RingGo.CorporateAddEditEmployeePage;
import PageObjects.Corporate.RingGo.CorporateAddEmployeePage;
import PageObjects.Corporate.RingGo.CorporateVehiclePage;
import PageObjects.Corporate.RingGo.modules.AddEditVrnPopup;
import PageObjects.Corporate.RingGo.modules.AddVrnPopup;
import PageObjects.NewCorporate.EmployeeDashboardPage;
import PageObjects.NewCorporate.EmployeeDashboardPage.CheckBox;
import PageObjects.NewCorporate.EmployeeDeleteConfirmationPage;
import PageObjects.myringo.AddNewEmployeePage;
import PageObjects.myringo.CorporateAccountPage;
import PageObjects.myringo.CorporateEmployeesPage;
import PageObjects.myringo.CorporateManageAssets;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.MyRingoCorporateLogin;
import SeleniumHelpers.Logging;

public class CorporateHelper {

	public static void addUserToCorporate(String corporateUserName, String password, String carVRN, String mobileNumber, WebDriver _driver) {
		NavigationHelper.openMyRingo(_driver);
		HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);

		Logging.CheckResult(homePage.getUpperRightNavigationMenu().clickLogin(), "Click Login", _driver);
		Logging.CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Corporate", _driver);

		MyRingoCorporateLogin corporateLogin = new MyRingoCorporateLogin(_driver);
		Logging.CheckResult(corporateLogin.enterEmailOrMobile(corporateUserName), "Input email address", _driver);
		Logging.CheckResult(corporateLogin.enterPasswordOrPin(password), "Input password", _driver);
		Logging.CheckResult(corporateLogin.clickLogin(), "Click Login", _driver);

		CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);

		if (getFeatureToggleState().equalsIgnoreCase("on")) {
			Logging.CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Add or Edit Employees link", _driver);
			addEmployee(randomAlphabetic(5), randomAlphabetic(5), "",  Arrays.asList(new String[]{mobileNumber}),  Arrays.asList(new String[]{carVRN}), true, _driver);
		} else {
			Logging.CheckResult(corporateAccountPage.clickOnAssets(), "Click Assets In Use", _driver);
			CorporateManageAssets corporateManageAssets = new CorporateManageAssets(_driver);
			Logging.CheckResult(corporateManageAssets.enterMobileNumber(mobileNumber), "Enter mobile number in Add number Plate", _driver);
			Logging.CheckResult(corporateManageAssets.clickMobileNumberButton(), "Add Mobile Number button", _driver);
			Logging.CheckResult(corporateManageAssets.enterNumberPlate(carVRN), "Enter car VRN in Add number Plate", _driver);
			Logging.CheckResult(corporateManageAssets.clickAddNumberPlate(), "Add Number Plate button", _driver);
			Logging.CheckResult(corporateManageAssets.clickSaveButton(), "Save button", _driver);
			Logging.CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Add or Edit Employees link", _driver);
			CorporateEmployeesPage corporateEmployeesPage = new CorporateEmployeesPage(_driver);
			Logging.CheckResult(corporateEmployeesPage.clickAddNewEmployee(), "Add New Employee link", _driver);
			AddNewEmployeePage addNewEmployeePage = new AddNewEmployeePage(_driver);
			Logging.CheckResult(addNewEmployeePage.enterFirstName(randomAlphabetic(5)), "First Name input", _driver);
			Logging.CheckResult(addNewEmployeePage.enterSurname(randomAlphabetic(5)), "Surname input", _driver);
			Logging.CheckResult(addNewEmployeePage.uncheckSendWelcomeEmail(), "Uncheck Send Welcome Email", _driver);
			Logging.CheckResult(addNewEmployeePage.addAddNewMobileInput(mobileNumber), "Input " + mobileNumber + " to Add new phone number", _driver);
			Logging.CheckResult(addNewEmployeePage.clickAddNewMobile(), "Add New Mobile button", _driver);
			Logging.CheckResult(addNewEmployeePage.addNewCarInput(carVRN), "Input " + carVRN + " to Add new vehicle", _driver);
			Logging.CheckResult(addNewEmployeePage.clickAddNewVehicle(), "Add New Vehicle button", _driver);
			Logging.CheckResult(addNewEmployeePage.clickSaveButton(), "Save button", _driver);
		}
		LoginHelper.logoutMyRingoCorporate(_driver);
	}

	private static void addEmployeeEnterFields(String firstName, String secondName, String reference, List<String> cli, List<String> vehicles, Boolean canPark, WebDriver _driver) {
		PageObjects.Corporate.RingGo.CorporateEmployeesPage corporateEmployeesPage = new PageObjects.Corporate.RingGo.CorporateEmployeesPage(_driver);
		Logging.CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee", _driver);

		CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
		Logging.CheckValue(corporateAddEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title", _driver);
		Logging.CheckResult(corporateAddEmployeePage.enterFirstName(firstName), "Enter Employee First Name", _driver);
		Logging.CheckResult(corporateAddEmployeePage.enterSurname(secondName), "Enter Employee Surname", _driver);
		Logging.CheckResult(corporateAddEmployeePage.enterReference(reference), "Enter Employee reference", _driver);

		if (cli.get(0).equals("Any"))
			Logging.CheckResult(corporateAddEmployeePage.clickOnAnyCliButton(), "Click On Any CLI", _driver);
		else {

			Logging.CheckResult(corporateAddEmployeePage.clickOnSpecifiedCli(), "Click on Specified CLI button", _driver);

			for (int i = 0; i < cli.size(); i++) {
				Logging.CheckResult(corporateAddEmployeePage.enterCli(cli.get(i)), "Enter Employee CLI", _driver);
				Logging.CheckResult(corporateAddEmployeePage.clickOnAddCli(), "Click on Add CLI", _driver);
			}
		}

		Logging.CheckResult(corporateAddEmployeePage.clickOnNoWelcomeEmail(), "Click on 'No' Send Welcome email", _driver);

		if (vehicles.get(0).equals("Any"))
			Logging.CheckResult(corporateAddEmployeePage.clickOnAnyVrn(), "Click On Any CLI", _driver);
		else {

			Logging.CheckResult(corporateAddEmployeePage.clickOnSpecifiedVrn(), "Click on Specified CLI button", _driver);

			for (int i = 0; i < vehicles.size(); i++) {
				Logging.CheckResult(corporateAddEmployeePage.enterVrn(vehicles.get(i)), "Enter Employee vehicle", _driver);
				Logging.CheckResult(corporateAddEmployeePage.clickOnAddVrn(), "Click on Add vehicle", _driver);
			}
		}

		if (!canPark)
			corporateAddEmployeePage.setCanParkOff();
	}

	public static void addNewAdminProcess(String email, String firstname, String surname, String password, WebDriver _driver) {
		WCCCorporateAdminAddPage wccCorporateAdminAddPage = new WCCCorporateAdminAddPage(_driver);
		Logging.CheckResult(wccCorporateAdminAddPage.enterAdminEmailId(email), "Enter " + email + " to Admin Email", _driver);
		Logging.CheckResult(wccCorporateAdminAddPage.enterAdminFirstName(firstname), "Enter " + firstname + " to First Name", _driver);
		Logging.CheckResult(wccCorporateAdminAddPage.enterAdminSurName(surname), "Enter " + surname + " to Surname", _driver);
		Logging.CheckResult(wccCorporateAdminAddPage.enterAdminPassword(password), "Enter " + password + " to Password", _driver);
		Logging.CheckResult(wccCorporateAdminAddPage.clickSaveButton(), "Click Save button", _driver);
	}

	public static void addCostCenter(String costCenterName, String costCenterNumber, String costCenterOwner, WebDriver _driver) {
		WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
		Logging.CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home", _driver);

		WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
		Logging.CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up", _driver);

		WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
		Logging.CheckResult(wccSetupPage.clickCostCentres(), "Clicking on Cost Centres", _driver);

		WCCCorporateCostCenterPage wccCorporateCostCenterPage = new WCCCorporateCostCenterPage(_driver);
		Logging.CheckResult(wccCorporateCostCenterPage.addCostCentre(), "Click add cost center", _driver);

		WCCCorporateAddCostCentrePage wccCorporateAddCostCentrePage = new WCCCorporateAddCostCentrePage(_driver);
		Logging.CheckResult(wccCorporateAddCostCentrePage.enterCostCentreName(costCenterName), String.format("Enter center name -> %s", costCenterName), _driver);
		Logging.CheckResult(wccCorporateAddCostCentrePage.enterCostCentreNumber(costCenterNumber), String.format("Enter center number -> %s", costCenterNumber), _driver);
		Logging.CheckResult(wccCorporateAddCostCentrePage.enterCostCentreOwner(costCenterOwner), String.format("Enter center owner -> %s", costCenterOwner), _driver);
		Logging.CheckResult(wccCorporateAddCostCentrePage.save(), "Click save", _driver);
	}

	public static void addNewEmployee(String name, String surname, String email, WebDriver _driver) {
		WCCCorporateProfilePage wccCorporateProfilePage = new WCCCorporateProfilePage(_driver);
		Logging.CheckResult(wccCorporateProfilePage.clickCoporateHome(), "Clicking on Corporate Home", _driver);

		WCCCorporateHomePage wccCorporateHomePage = new WCCCorporateHomePage(_driver);
		Logging.CheckResult(wccCorporateHomePage.clickSetUp(), "Clicking on Set up", _driver);

		WCCSetupPage wccSetupPage = new WCCSetupPage(_driver);
		Logging.CheckResult(wccSetupPage.clickAssets(), "Clicking on Assets", _driver);

		WCCCorporateAssetsPage wcccorporateassetspage = new WCCCorporateAssetsPage(_driver);

		String cli = "07" + RandomStringUtils.randomNumeric(9);
		String vrn = RandomStringUtils.randomAlphanumeric(7);

		Logging.CheckResult(wcccorporateassetspage.enterCli(cli), String.format("Enter cli -> %s", cli), _driver);
		Logging.CheckResult(wcccorporateassetspage.addCli(), "Click add Cli", _driver);
		Logging.CheckResult(wcccorporateassetspage.enterVRN(vrn), String.format("Enter vrn -> %s", vrn), _driver);
		Logging.CheckResult(wcccorporateassetspage.addVrn(), "Click add vrn", _driver);
		Logging.CheckResult(wcccorporateassetspage.save(), "Click save", _driver);

		delay(2000);
		Logging.CheckResult(wcccorporateassetspage.clickSetup(), "Click Setup", _driver);

		Logging.CheckResult(wccSetupPage.clickEmployees(), "Clicking on Employees", _driver);

		WCCCorporateEmployeesPage wccCorporateEmployeesPage = new WCCCorporateEmployeesPage(_driver);
		Logging.CheckResult(wccCorporateEmployeesPage.addNewEmployee(), "Click Add new employee", _driver);

		WCCCorporateAddEmployeePage wcccorporateaddemployeepage = new WCCCorporateAddEmployeePage(_driver);
		Logging.CheckContains(wcccorporateaddemployeepage.getAddEmployeePageTitle(), "Add Employee", "Add Employee page title", _driver);

		Logging.CheckResult(wcccorporateaddemployeepage.selectTitle(WCCCorporateAddEmployeePage.Title.MR), String.format("Select title -> %s", WCCCorporateAddEmployeePage.Title.MR.toString()), _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.enterFirstName(name), String.format("Enter first name -> %s", name), _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.enterSurname(surname), String.format("Enter second name -> %s", surname), _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.enterEmail(email), String.format("Enter email -> %s", email), _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.tickWelcomeEmail(), "Tick Welcome email", _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.enterPhoneNumber(cli), String.format("Enter cli -> %s", cli), _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.addCli(), "Click Add cli", _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.enterVehicle(vrn), String.format("Enter vrn -> %s", vrn), _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.addVrn(), "Click add vrn", _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.tickCanEmployeePark(), "Tick can park", _driver);
		Logging.CheckResult(wcccorporateaddemployeepage.clickSave(), "Click save", _driver);

	}

	public static void deleteCorporateAdministratorMethod(String email, String password, String adminEmail, WebDriver _driver) {
		LoginHelper.loginCorporateWestminster(email, password, _driver);

		WCCCorporateProfilePage wcccoporateprofilepage = new WCCCorporateProfilePage(_driver);
		Logging.CheckResult(wcccoporateprofilepage.clickCoporateHome(), "Clicking on Corporate Home", _driver);

		WCCCorporateHomePage wcccorporatehomepage = new WCCCorporateHomePage(_driver);
		Logging.CheckResult(wcccorporatehomepage.clickSetUp(), "Clicking on Set up", _driver);

		WCCSetupPage wccsetuppage = new WCCSetupPage(_driver);
		Logging.CheckResult(wccsetuppage.clickAdministrator(), "Clicking on Administrator", _driver);

		WccCorporateAdministrator wccCorporateAdministrator = new WccCorporateAdministrator(_driver);
		Logging.CheckResult(wccCorporateAdministrator.clickAdminDelete(adminEmail), "Click Delete for Admin " + adminEmail, _driver);

		WCCCorporateDeleteConfirmationPage wcccorporatedeleteconfirmationpage = new WCCCorporateDeleteConfirmationPage(_driver);
		Logging.CheckResult(wcccorporatedeleteconfirmationpage.clickDeleteConfirmButton(), "Click on Delete Confirm button", _driver);
	}

	public static void addPaymentCardToCorporate(String cardNumber, String expiries, String cardCountry, WebDriver _driver) {
		WCCCorporatePaymentCardPage wcccorporatepaymentcardpage = new WCCCorporatePaymentCardPage(_driver);
		Logging.CheckContains(wcccorporatepaymentcardpage.getPaymentCardTitle(), "Payment Details", "Payment card title", _driver);

		Logging.CheckResult(wcccorporatepaymentcardpage.clickAddNewCardButton(), "Click 'Add new card'",_driver);

		WCCCorporateAddPaymentCardDetailsPage wcccorporateaddpaymentcarddetailspage = new WCCCorporateAddPaymentCardDetailsPage(_driver);
		wcccorporateaddpaymentcarddetailspage.clickAddPaymentCard();

		WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
		Logging.CheckResult(wccaddpaymentcardpopup.enterCardNumber(cardNumber), String.format("Enter card number -> %s", cardNumber),_driver);
		Logging.CheckResult(wccaddpaymentcardpopup.enterCardExpires(expiries), String.format("Enter card expires month and year -> %s", expiries),_driver);
		Logging.CheckResult(wccaddpaymentcardpopup.clickAddButton(), "Click save button",_driver);

		Logging.CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardAddress(randomAlphabetic(6)), "Enter card address",_driver);
		Logging.CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardTown(randomAlphabetic(6)), "Enter card town",_driver);
		Logging.CheckResult(wcccorporateaddpaymentcarddetailspage.selectCardCounty(cardCountry), "Enter card country",_driver);
		Logging.CheckResult(wcccorporateaddpaymentcarddetailspage.enterCardPostcode(randomAlphabetic(6)), "Enter card postcode",_driver);
		Logging.CheckResult(wcccorporateaddpaymentcarddetailspage.clickSave(), "Click save button",_driver);
	}

	public static void deleteAllCorporateEmployees(WebDriver _driver){
	    CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
	    Logging.CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees", _driver);
	
	    EmployeeDashboardPage employeeDashboardPage = new EmployeeDashboardPage(_driver);
	    employeeDashboardPage.checkbox = new EmployeeDashboardPage.CheckBox();
	    if(employeeDashboardPage.allEmployeesCheckbox.isDisplayed()) {
	        Logging.CheckResult(employeeDashboardPage.clickAllEmployeesCheckbox(), "Select All Employees", _driver);
	        Logging.CheckResult(employeeDashboardPage.clickDeleteButton(), "ClickDeleteButton", _driver);
	        EmployeeDeleteConfirmationPage employeeDeleteConfirmationPage = new EmployeeDeleteConfirmationPage(_driver);
	        Logging.CheckResult(employeeDeleteConfirmationPage.clickYes(), "Clicking on Yes button", _driver);
	    }
	}

	public static void addVehicle(String numberPlate, String make, String type, String colour, WebDriver _driver) {
	    CorporateVehiclePage vehiclePage = new CorporateVehiclePage(_driver);
	    Logging.CheckResult(vehiclePage.clickAddVrnButton(), "Verify click on 'Add vehicle' button", _driver);
	
	    AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
	    AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
	    Logging.CheckResult(addVrnPopup.enterVrnNumber(numberPlate), "Enter Vehicle Number", _driver);
	    Logging.CheckResult(addEditVrnPopup.selectVrnType(type), "Select Vehicle Type", _driver);
	    Logging.CheckResult(addEditVrnPopup.selectVrnMake(make), "Select Vehicle Make", _driver);
	    Logging.CheckResult(addEditVrnPopup.selectVrnColour(colour), "Select Vehicle Colour", _driver);
	    Logging.CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button", _driver);
	
	    delay(5000);
	}

	public static void addEmployeeWithCostCentre(String firstName, String secondName, String reference, List<String> cli,
	                                      List<String> vehicles, Boolean canPark, String costCentre, WebDriver _driver){
	    CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
	    addEmployeeEnterFields(firstName, secondName, reference, cli, vehicles, canPark, _driver);
	    Logging.CheckResult(corporateAddEmployeePage.selectCostCentre(costCentre), "Select cost centre", _driver);
	    Logging.CheckResult(corporateAddEmployeePage.clickOnSaveEmployee(), "Click on Add Employee", _driver);
	}

	public static void addEmployee(String firstName, String secondName, String reference, List<String> cli, List<String> vehicles, Boolean canPark, WebDriver _driver){
	    CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
	    addEmployeeEnterFields(firstName, secondName, reference, cli, vehicles, canPark, _driver);
	    Logging.CheckResult(corporateAddEmployeePage.clickOnSaveEmployee(), "Click on Add Employee", _driver);
	}

	public static void addVrnToCorporateAccount(String vrnNumber, Optional<String> type, Optional<String> make, Optional<String> colour, WebDriver _driver) {
	    AddEditVrnPopup addEditVrnPopup = new AddEditVrnPopup(_driver);
	    AddVrnPopup addVrnPopup = new AddVrnPopup(_driver);
	    Logging.CheckResult(addVrnPopup.enterVrnNumber(vrnNumber), "Enter Vehicle Number", _driver);
	    type.ifPresent(s -> Logging.CheckResult(addEditVrnPopup.selectVrnType(s), "Select Vehicle Type", _driver));
	    make.ifPresent(s -> Logging.CheckResult(addEditVrnPopup.selectVrnMake(s), "Select Vehicle Make", _driver));
	    colour.ifPresent(s -> Logging.CheckResult(addEditVrnPopup.selectVrnColour(s), "Select Vehicle Colour", _driver));
	    Logging.CheckResult(addEditVrnPopup.clickOnAddButton(), "Click on Add Vehicle Button", _driver);
	}

	public static void addEmployee(String name, String surname, String cli, WebDriver _driver) {
	    CorporateAccountPage corporateAccountPage = new CorporateAccountPage(_driver);
	    Logging.CheckResult(corporateAccountPage.clickOnAddEditEmployee(), "Click on Add or Edit Employees", _driver);
	    PageObjects.Corporate.RingGo.CorporateEmployeesPage corporateEmployeesPage = new PageObjects.Corporate.RingGo.CorporateEmployeesPage(_driver);
	    Logging.CheckContains(corporateEmployeesPage.getCurrentTitle(), "Employees", "Verify RingGo Corporate Employees page title", _driver);
	    Logging.CheckResult(corporateEmployeesPage.clickOnAddEmployeeButton(), "Click on Add Employee", _driver);
	    CorporateAddEditEmployeePage corporateAddEditEmployeePage = new CorporateAddEditEmployeePage(_driver);
	    Logging.CheckValue(corporateAddEditEmployeePage.getPageTitleText(), "Add Employee", "Verify RingGo Corporate Add Employee page title", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.enterFirstName(name), "Enter Employee First Name", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.enterSurname(surname), "Enter Employee Surname", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.clickOnSpecifiedCli(), "Click on Specified CLI button", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.enterCli(cli), "Enter Employee CLI", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.clickOnAddCli(), "Click on Add CLI", _driver);
	    CorporateAddEmployeePage corporateAddEmployeePage = new CorporateAddEmployeePage(_driver);
	    Logging.CheckResult(corporateAddEmployeePage.clickOnNoWelcomeEmail(), "Click on 'No' Send Welcome email", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.clickOnAnyVrn(), "Click On Any VRN", _driver);
	    Logging.CheckResult(corporateAddEditEmployeePage.clickOnSaveEmployee(), "Click on Add Employee", _driver);
	    Logging.CheckBool(corporateEmployeesPage.isEmployeeRecordPresentByName(name.concat(" ").concat(surname)),
	            "Verify if New Employee is added to the table by Name", _driver);
	    Logging.CheckValue(corporateEmployeesPage.getNotificationText(), String.format("Employee %s %s was successfully added", name, surname), "Verify Notification Text", _driver);
	}

}
