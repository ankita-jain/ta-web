package GenericComponents.helper;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

import Logging.Log;
import PageObjects.WCCAddPaymentCardPopUp;
import PageObjects.WCCEcoPermitPurchaseFinishPage;
import PageObjects.WCCHome;
import PageObjects.WCCPaymentCardCVVPage;
import PageObjects.WCCPaymentCardDetailsPage;
import PageObjects.WCCPaymentDetailsPage;
import PageObjects.WCCPermitDetailsConfirmationPage;
import PageObjects.WCCPermitPage;
import PageObjects.WCCPermitPurchaseFinishPage;
import PageObjects.WCCPurchasePermitPaymentType;
import PageObjects.WCCResidentPermitDetailsPage;
import PageObjects.WCCResidentPermitInfoPage;
import PageObjects.WCCResidentPermitPage;
import PageObjects.Insight.InsightEndPermitSessionPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightPermitApplicationPage;
import PageObjects.Insight.InsightPermitWorkflowQueuePage;
import PageObjects.Insight.InsightServiceMenuPage;
import PageObjects.Insight.InsightWCCHome;
import PageObjects.Permit.WCCResidentPermitProofOfAddressPage;
import PageObjects.Permit.WCCResidentPermitProofOfVehiclesUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfAddressUploadPage;
import PageObjects.Permit.WCCResidentialPermitProofOfVehiclesPage;
import SeleniumHelpers.Logging;
import Utilities.PropertyUtils;

public class PermitsHelper {

	private static final String WCC_PROPERTIES = "WCCTest.properties";
	
	// TODO - start to eliminate hard-coded data and inject into the methods
	private static final String POSTCODE = "EC4A2AH";
	private static final String ADDRESS = "George Attenborough & Son 193 Fleet Street London EC4A";
	private static final String ZONE = "65007";
	private static final String TITLE = "Mr";
	private static final String FIRSTNAME = "ANY";
	private static final String SURNAME = "GUY";
	private static String[] ResidentType = {"Standard Residence", "Houseboat", "Armed Forces", "Diplomats"};

	private static String[] OwnershipType = {"Private Owned Vehicle", "Company Owned Vehicle", "Private Lease Vehicle",
			"Company Lease Vehicle", "Foreign Company Vehicle", "Chauffeur Vehicle", "Diplomat Vehicle", "Foreign Vehicle"};

	private static String proof = "1.jpg";
	private static String permitStatus = "Show All";
	private static String insightUserName;
	private static String insightPassword;

	private static String email="";
	private static String password="";
	private static String cardNumber = "5453010000070789";
	private static String expiryDate = "0128";

	/**
	 * Function to setup a residential permit, this will be used for existing users.
	 *
	 * @param ResidentTypeKey
	 * @param OwnershipTypeKey
	 * @param vehicle
	 */
	public static void setUpResidencialPermit(int ResidentTypeKey, int OwnershipTypeKey, String vehicle, WebDriver _driver) {
		WCCHome wcchomepage = new WCCHome(_driver);
		Logging.CheckResult(wcchomepage.clickPermit(), "Click permit tile", _driver);

		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);

		wccpermitpage.clickResidentPermit();
		WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);

		Logging.CheckResult(wccresidentpermitinfopage.clickStandardResidentPermit(), "Click standard resident permit", _driver);
		WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
		wccresidentpermitpage.applyResidentPermitApplication(POSTCODE, ADDRESS, ZONE);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.enterUserDetailsPermitDetails(TITLE, FIRSTNAME, SURNAME);
		wccresidentpermitdetailspage.fillPermitApplicationDetails(ResidentType[ResidentTypeKey], vehicle, OwnershipType[OwnershipTypeKey]);
	}

	/**
	 * Function to setup a resident permit to add on vehicles for exisiting users.
	 *
	 * @param vehicle
	 */
	public static void setUpPermitToAddOnVehicleForExistingUser(String vehicle, WebDriver _driver) {
		setUpResidencialPermit('0', '0', vehicle, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
	}

	/**
	 * Set up a resident permit for multiple vehicles
	 *
	 * @param ResidentTypeKey
	 * @param OwnershipTypeKey
	 * @param VRM2
	 */
	public static void setUpResidencialPermitWithTwoVehicles(int ResidentTypeKey, int OwnershipTypeKey, String VRM2, WebDriver _driver) {
		WCCHome wcchomepage = new WCCHome(_driver);
		Logging.CheckResult(wcchomepage.clickPermit(), "Click permit tile", _driver);

		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);

		wccpermitpage.clickResidentPermit();
		WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);

		Logging.CheckResult(wccresidentpermitinfopage.clickStandardResidentPermit(), "Click standard resident permit", _driver);
		WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
		wccresidentpermitpage.applyResidentPermitApplication(POSTCODE, ADDRESS, ZONE);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.fillPermitApplicationDetailsForMultipleVehicles(ResidentType[ResidentTypeKey], randomAlphabetic(6), VRM2, OwnershipType[OwnershipTypeKey]);
	}

	/**
	 * Function to setup a resident permit with two vehicles
	 *
	 * @param VRM2
	 */
	public static void setUpPermitWithSecondVehicle(String VRM2, WebDriver _driver) {
		setUpResidencialPermitWithTwoVehicles('0', '0', VRM2, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
	}

	/**
	 * Function to create an interim permit, this will select the partial vehicle proofs options and upload docs..
	 *
	 * @param proofonepath
	 */
	public static void createInterimPermit(String proofonepath, WebDriver _driver) {

		WCCResidentialPermitProofOfVehiclesPage wccresidentpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		Logging.CheckContains(wccresidentpermitproofofvehiclespage.getquestions_pack(), "I can only provide", "Question for choosing Packs", _driver);
		wccresidentpermitproofofvehiclespage.selectFourthProofPack();
		wccresidentpermitproofofvehiclespage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehicleuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		wccresidentpermitproofofvehicleuploadpage.selectDocTypeAndUploadFirstProof(proofonepath);
	}

	/**
	 * Function to complete payment of a resident permit
	 *
	 * @param CV2
	 */
	public static void completePaymentOfAPermit(String CV2, WebDriver _driver) {
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehicleuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		wccresidentpermitproofofvehicleuploadpage.clickNextButton();
		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
		wccpermitdetailsconfirmationpage.confirmPermitDetails();
		WCCPurchasePermitPaymentType wccpurchasepermitpaymenttype = new WCCPurchasePermitPaymentType(_driver);
		wccpurchasepermitpaymenttype.clickTakePaymentNow();
		wccpurchasepermitpaymenttype.clickNextButton();
		WCCPaymentCardDetailsPage wccpaymentcarddetailspage = new WCCPaymentCardDetailsPage(_driver);
		wccpaymentcarddetailspage.proceedToPaymentWithExistingCard();

		WCCPaymentCardCVVPage wccpaymentcardcvvpage = new WCCPaymentCardCVVPage(_driver);
		wccpaymentcardcvvpage.enterCV2AndProceed(CV2);
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		wccpermitpurchasefinishpage.clickFinish();
	}

	/**
	 * Function to authorise a permit in insight
	 *
	 * @param username
	 * @param password
	 * @param operatorstatus
	 * @param operator
	 * @param VRM
	 * @param permitstatus
	 * @param changePermitStatus
	 */
	public static void authorisePermitInsight(String username, String password, String operatorstatus, String operator, String VRM, String permitstatus, String changePermitStatus, WebDriver _driver) {

		NavigationHelper.openInsight(_driver);
		LoginHelper.loginInsight(username, password, _driver);

		InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
		insightservicemenupage.loadPermitSearchForm();
		InsightWCCHome insightwcchome = new InsightWCCHome(_driver);
		Logging.CheckValue(insightwcchome.getPageTitle(), "Permit Applications",
				"Checking the page title for Insight WCC homepage is correct", _driver);

		InsightHelper.searchPermitAndEdit(VRM, permitstatus, _driver);
		InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
		Logging.CheckValue(insightpermitapplicationpage.getPageTitle(), "Edit a Resident Permit Application",
				"Checking the page title for edit permit insight page is correct", _driver);
		insightpermitapplicationpage.editPermitStatusAndSave(changePermitStatus);
		insightpermitapplicationpage.clickSavePermit();
	}

	public static void createATemporaryEcoPermit(WebDriver _driver) {
		Logging.LogStepPass("Step - 1", "Select ownership type, T's & C's when creating an eco permit");
		selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		Logging.CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
				"Checking the page title for resident permit proof of address page is correct", _driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		Logging.LogStepPass("Step - 2", "Complete the proof for the categories, address & vehicle");
		Logging.CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
				"Checking the page title for resident permit proof of address upload page is correct", _driver);
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proof);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();

		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		wccresidentialpermitproofofvehiclespage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclespage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);

		Logging.CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
				"Checking the page title for resident permit proof of vehicle upload page is correct", _driver);

		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(proof);
		wccresidentpermitproofofvehiclesuploadpage.clickNextButton();
		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
		wccpermitdetailsconfirmationpage.confirmPermitDetails();
		WCCEcoPermitPurchaseFinishPage wccecopermitpurchasefinishpage = new WCCEcoPermitPurchaseFinishPage(_driver);
		wccecopermitpurchasefinishpage.clickConfirm();
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		wccpermitpurchasefinishpage.clickFinish();
	}

	public static void createAFullEcoPermit(WebDriver _driver) {
		Logging.LogStepPass("Step - 1", "Select ownership type, T's & C's when creating an eco permit");
		selectOwnershipTypeAndTsAndCs(0, 0, _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		Logging.CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
				"Checking the page title for resident permit proof of address page is correct", _driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(_driver);
		Logging.LogStepPass("Step - 2", "Complete the proof for the categories, address & vehicle");
		Logging.CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
				"Checking the page title for resident permit proof of address upload page is correct", _driver);
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proof);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();

		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(_driver);
		wccresidentialpermitproofofvehiclespage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclespage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);

		Logging.CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(), "Proof of vehicle ownership",
				"Checking the page title for resident permit proof of vehicle upload page is correct", _driver);

		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(proof);
		wccresidentpermitproofofvehiclesuploadpage.clickNextButton();
		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(_driver);
		wccpermitdetailsconfirmationpage.confirmPermitDetails();
		WCCEcoPermitPurchaseFinishPage wccecopermitpurchasefinishpage = new WCCEcoPermitPurchaseFinishPage(_driver);
		wccecopermitpurchasefinishpage.clickConfirm();
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		wccpermitpurchasefinishpage.clickFinish();
	}

	public static void AuthorisePermitApplication(String vrn, WebDriver _driver) throws IOException {
		NavigationHelper.openInsight(_driver);
		insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
		insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);

		LoginHelper.loginInsight(insightUserName, insightPassword, _driver);

		InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
		insightservicemenupage.loadPermitSearchForm();
		InsightHelper.searchPermitAndEdit(vrn, permitStatus, _driver);

		InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
		Logging.CheckContains(insightpermitapplicationpage.getPageTitle(), "Edit a Resident",
				"Checking the page title for permit insight page is correct", _driver);
		insightpermitapplicationpage.editPermitStatusAndSave("Authorised");
	}

	public static void createAFullMotorCyclePermit(WebDriver _driver) {
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(
				_driver);
		Logging.CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
				"Checking the page title for resident permit proof of address page is correct", _driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(
				_driver);
		Logging.CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
				"Checking the page title for resident permit proof of address upload page is correct", _driver);
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proof);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();

		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(
				_driver);
		wccresidentialpermitproofofvehiclespage.selectFirstProofPack();
		wccresidentialpermitproofofvehiclespage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		Logging.CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(),
				"Proof of vehicle ownership",
				"Checking the page title for resident permit proof of vehicle upload page is correct", _driver);

		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(proof);
		wccresidentpermitproofofvehiclesuploadpage.clickNextButton();
		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(
				_driver);
		wccpermitdetailsconfirmationpage.confirmPermitDetails();
		WCCPermitPurchaseFinishPage wccpermitpurchasefinishpage = new WCCPermitPurchaseFinishPage(_driver);
		wccpermitpurchasefinishpage.clickFinish();
	}

	public static void createATemporaryMotorCyclePermit(WebDriver _driver) {
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(
				_driver);
		Logging.CheckValue(wccresidentpermitproofofaddresspage.getProofOfAddressPageTitle(), "Proof of Address",
				"Checking the page title for resident permit proof of address page is correct", _driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
		WCCResidentialPermitProofOfAddressUploadPage wccresidentialpermitproofofaddressuploadpage = new WCCResidentialPermitProofOfAddressUploadPage(
				_driver);
		Logging.CheckValue(wccresidentialpermitproofofaddressuploadpage.getCouncilTaxPageHeader(), "Council Tax Bill",
				"Checking the page title for resident permit proof of address upload page is correct", _driver);
		wccresidentialpermitproofofaddressuploadpage.sendUploadPathToCouncilTax(proof);
		wccresidentialpermitproofofaddressuploadpage.clickNextButton();

		WCCResidentialPermitProofOfVehiclesPage wccresidentialpermitproofofvehiclespage = new WCCResidentialPermitProofOfVehiclesPage(
				_driver);
		wccresidentialpermitproofofvehiclespage.selectFourthProofPack();
		wccresidentialpermitproofofvehiclespage.navigateNextPage();
		WCCResidentPermitProofOfVehiclesUploadPage wccresidentpermitproofofvehiclesuploadpage = new WCCResidentPermitProofOfVehiclesUploadPage(_driver);
		Logging.CheckValue(wccresidentpermitproofofvehiclesuploadpage.getVehicleUploadPageHeader(),
				"Proof of vehicle ownership",
				"Checking the page title for resident permit proof of vehicle upload page is correct", _driver);

		wccresidentpermitproofofvehiclesuploadpage.selectDocTypeAndUploadFirstProof(proof);
		wccresidentpermitproofofvehiclesuploadpage.clickNextButton();
		WCCPermitDetailsConfirmationPage wccpermitdetailsconfirmationpage = new WCCPermitDetailsConfirmationPage(
				_driver);
		wccpermitdetailsconfirmationpage.confirmPermitDetails();
	}

	public static void CancelPermitApplication(String vrn, WebDriver _driver) throws IOException {
		try {
			NavigationHelper.openInsight(_driver);
			insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
			insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);

			LoginHelper.loginInsight(insightUserName, insightPassword, _driver);


			InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
			Logging.CheckValue(insightservicemenupage.getCurrentPageTitle(), "Insight - Parking Zones", "Checking the page title for insight service menu is correct", _driver);
			insightservicemenupage.loadPermitSearchForm();
			InsightHelper.searchPermitAndEdit(vrn, permitStatus, _driver);

			InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
			Logging.CheckContains(insightpermitapplicationpage.getPageTitle(), "Edit a Resident",
					"Checking the page title for permit insight page is correct", _driver);
			insightpermitapplicationpage.editPermitStatus("Cancelled");
			insightpermitapplicationpage.selectCancellationReason("Refunded");
			insightpermitapplicationpage.fillOutResponseToCustomer("This has to be cancelled");
			insightpermitapplicationpage.clickSavePermit();
			InsightEndPermitSessionPage insightendpermitsessionpage = new InsightEndPermitSessionPage(_driver);
			Logging.CheckContains(insightendpermitsessionpage.getPageTitle(), "End Permit Sessions?", "Checking the page title is correct", _driver);
			insightendpermitsessionpage.clickEndSession();

			InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
			Logging.CheckContains(insightpermitworkflowqueuepage.getPageTitle(), "Queue", "Checking the page title is correct", _driver);
			InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
			insightnavmenu.clickAccount();

			InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
			insightlogoutpage.clickLogout();
		} catch (AssertionError | Exception e) {
			Logging.LogError("Failed due -> " + e.getMessage(), _driver);
			Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(PermitsHelper.class.getName())).findFirst().ifPresent(System.out::println);
			Log.add(LogStatus.FAIL, "Test is failed");
			Assert.fail("Failed as verification failed -'" + e.getMessage());
		}
	}

	public static void expirePermitApplication(String VRN, WebDriver _driver) throws IOException {
		try {
			NavigationHelper.openInsight(_driver);
			insightUserName = PropertyUtils.ReadProperty("INSIGHT_PERMIT_USER_LOGIN", WCC_PROPERTIES);
			insightPassword = PropertyUtils.ReadProperty("INSIGHT_PASSWORD", WCC_PROPERTIES);

			LoginHelper.loginInsight(insightUserName, insightPassword, _driver);


			InsightServiceMenuPage insightservicemenupage = new InsightServiceMenuPage(_driver);
			Logging.CheckValue(insightservicemenupage.getCurrentPageTitle(), "Insight - Parking Zones", "Checking the page title for insight service menu is correct", _driver);
			insightservicemenupage.loadPermitSearchForm();
			InsightHelper.searchPermitAndEdit(VRN, permitStatus, _driver);

			InsightPermitApplicationPage insightpermitapplicationpage = new InsightPermitApplicationPage(_driver);
			Logging.CheckContains(insightpermitapplicationpage.getPageTitle(), "Edit a Resident",
					"Checking the page title for permit insight page is correct", _driver);
			insightpermitapplicationpage.editPermitStatusAndSave("Expired");
			InsightEndPermitSessionPage insightendpermitsessionpage = new InsightEndPermitSessionPage(_driver);
			Logging.CheckContains(insightendpermitsessionpage.getPageTitle(), "End Permit Sessions?", "Checking the page title is correct", _driver);
			insightendpermitsessionpage.clickEndSession();

			InsightPermitWorkflowQueuePage insightpermitworkflowqueuepage = new InsightPermitWorkflowQueuePage(_driver);
			Logging.CheckContains(insightpermitworkflowqueuepage.getPageTitle(), "Queue", "Checking the page title is correct", _driver);
			InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
			insightnavmenu.clickAccount();

			InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
			insightlogoutpage.clickLogout();

		} catch (AssertionError | Exception e) {
			Logging.LogError("Failed due -> " + e.getMessage(), _driver);
			Arrays.stream(e.getStackTrace()).filter(x -> x.getClassName().equals(PermitsHelper.class.getName())).findFirst().ifPresent(System.out::println);
			Log.add(LogStatus.FAIL, "Test is failed");
			Assert.fail("Failed as verification failed -'" + e.getMessage());
		}
	}

	public static void selectOwnershipTypeAndTsAndCs(int ResidentArrayIndex, int OwnershipArrayIndex, WebDriver _driver) {
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.selectResidentType(ResidentType[ResidentArrayIndex]);
		wccresidentpermitdetailspage.selectOwnershipType(OwnershipType[OwnershipArrayIndex]);
		wccresidentpermitdetailspage.selectTermAndConditions();
		wccresidentpermitdetailspage.clickNextButton();
	}

	/**
	 * Method to create a standard residence permit for a new user.
	 *
	 * @param ResidentType
	 * @param OwnershipType
	 */
	public static String setUpResidencialPermitForANewUser(Date baseDate, WebDriver _driver) {
		String VRN;
		try {
			createUserForPermits(baseDate);
		} catch (Exception e) {
			Logging.LogError("The following exception occured when calling createUser: " + e.getMessage(), _driver);
		}
		NavigationHelper.openWestMinster(_driver);
		LoginHelper.loginWestminster(email, password, _driver);
		addNewCardToAccount(_driver);

		WCCHome wcchomepage = new WCCHome(_driver);
		Logging.CheckResult(wcchomepage.clickPermit(), "Click permit tile", _driver);

		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);

		wccpermitpage.clickResidentPermit();
		WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);

		Logging.CheckResult(wccresidentpermitinfopage.clickStandardResidentPermit(), "Click standard resident permit", _driver);
		WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
		wccresidentpermitpage.applyResidentPermitApplication(POSTCODE, ADDRESS, ZONE);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.enterUserDetailsPermitDetails(TITLE, FIRSTNAME, SURNAME);
		VRN = randomAlphabetic(6).toUpperCase();
		wccresidentpermitdetailspage.addRegistration(VRN);
		return VRN;
	}

	/**
	 * Function to set up a residential permit for a new user.
	 */
	public static void setUpPermitToAddOnVehicle(Date baseDate, WebDriver _driver) {
		setUpResidencialPermitForANewUser(baseDate, _driver);
		selectOwnershipTypeAndTsAndCs('0', '0', _driver);
		WCCResidentPermitProofOfAddressPage wccresidentpermitproofofaddresspage = new WCCResidentPermitProofOfAddressPage(_driver);
		wccresidentpermitproofofaddresspage.selectYesToSuperPack();
		wccresidentpermitproofofaddresspage.navigateNextPage();
	}

	public static String setUpEcoPermitForNewUser(String Zone, Date baseDate, WebDriver _driver) {
		String VRN;
		try {
			createUserForPermits(baseDate);
		} catch (Exception e) {
			Logging.LogError("The following exception occured when calling createUser: " + e.getMessage(), _driver);
		}
		NavigationHelper.openWestMinster(_driver);
		LoginHelper.loginWestminster(email, password, _driver);
		addNewCardToAccount(_driver);
		WCCHome wcchomepage = new WCCHome(_driver);
		Logging.CheckResult(wcchomepage.clickPermit(), "Click permit tile", null);

		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);

		wccpermitpage.clickResidentPermit();
		WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);

		wccresidentpermitinfopage.clickEcoPermit();
		WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
		Logging.CheckValue(wccresidentpermitpage.getResidentPermitPageTitle(), "Resident Low Emission permit application",
				"Checking the page title for resident permit page is correct.", _driver);

		String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();

		Logging.CheckValue(residentPermitPageTitle, "Resident Low Emission permit application", "Checking the title of the permit page", _driver);


		wccresidentpermitpage.applyResidentPermitApplication(POSTCODE, ADDRESS, Zone);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.enterUserDetailsPermitDetails(TITLE, FIRSTNAME, SURNAME);
		VRN = randomAlphabetic(6).toUpperCase();
		wccresidentpermitdetailspage.addRegistration(VRN);
		return VRN;
	}

	public static Map<String, String> setUpMotorcyclePermitForNewUser(int OwnershipArrayIndex, Date baseDate, WebDriver _driver) throws IOException {
		try {
			createUserForPermits(baseDate);
		} catch (Exception e) {
			Logging.LogError("The following exception occured when calling createUser: " + e.getMessage(), _driver);
		}
		NavigationHelper.openWestMinster(_driver);
		LoginHelper.loginWestminster(email, password, _driver);
		addNewCardToAccount(_driver);
		
		Map<String, String> VRNs = new HashMap<>();
		String FirstVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewMotorCycleVehicle()).toUpperCase();
		String SecondVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewMotorCycleVehicle()).toUpperCase();
		VRNs.put("First VRN", FirstVRN);
		VRNs.put("Second VRN", SecondVRN);
		
		WCCHome wcchomepage = new WCCHome(_driver);
		Logging.CheckResult(wcchomepage.clickPermit(), "Click permit tile", _driver);
		WCCPermitPage wccpermitpage = new WCCPermitPage(_driver);

		wccpermitpage.clickResidentPermit();
		WCCResidentPermitInfoPage wccresidentpermitinfopage = new WCCResidentPermitInfoPage(_driver);

		wccresidentpermitinfopage.clickMotorCyclePermit();
		WCCResidentPermitPage wccresidentpermitpage = new WCCResidentPermitPage(_driver);
		Logging.CheckValue(wccresidentpermitpage.getResidentPermitPageTitle(), "Resident Motorcycle permit application",
				"Checking the page title for resident permit page is correct.", _driver);

		String residentPermitPageTitle = wccresidentpermitpage.getResidentPermitPageTitle();
		Logging.CheckValue(residentPermitPageTitle, "Resident Motorcycle permit application", "Checking if the page title is correct.", _driver);
		wccresidentpermitpage.applyResidentPermitApplication(POSTCODE, ADDRESS, ZONE);
		WCCResidentPermitDetailsPage wccresidentpermitdetailspage = new WCCResidentPermitDetailsPage(_driver);
		wccresidentpermitdetailspage.enterUserDetailsPermitDetails(TITLE, FIRSTNAME, SURNAME);
		wccresidentpermitdetailspage.selectRegistration(FirstVRN + " WHITE BMW");
		wccresidentpermitdetailspage.selectOwnershipType(OwnershipType[OwnershipArrayIndex]);
		wccresidentpermitdetailspage.selectTermAndConditions();
		wccresidentpermitdetailspage.clickNextButton();
		
		return VRNs;
	}

	public static void addNewCardToAccount(WebDriver _driver) {

		WCCHome wcchomepage = new WCCHome(_driver);
		Logging.CheckResult(wcchomepage.clickPayment(), "Click on Payment link", _driver);

		WCCPaymentDetailsPage wccpaymentdetailspage = new WCCPaymentDetailsPage(_driver);
		wccpaymentdetailspage.clickAddNewCard();
		WCCAddPaymentCardPopUp wccaddpaymentcardpopup = new WCCAddPaymentCardPopUp(_driver);
		wccaddpaymentcardpopup.enterCardNumber(cardNumber);
		wccaddpaymentcardpopup.enterCardExpires(expiryDate);
		wccaddpaymentcardpopup.clickAddButton();
		delay(3000);
		wccpaymentdetailspage.clickHomeBreadCrumb();
	}

	private static void createUserForPermits(Date baseDate) throws Exception {
		Map<Object, Object> newUserJson = RestApiHelper.createNewUser(RestApiHelper.getNewUserData(baseDate,"defaultNewUser.json"));
		email = (String) newUserJson.get("Email");
		password = (String) newUserJson.get("Password");
	}
}
