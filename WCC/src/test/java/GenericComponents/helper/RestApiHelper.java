package GenericComponents.helper;

import BusinessObjects.*;
import Logging.Log;
import ServiceObjects.*;
import Services.ActiveClientToken;
import Services.ActivePasswordToken;
import Services.IToken;
import Services.Rest;
import Services.oAuth2;
import Utilities.DynamicDataUtils;
import Utilities.MapUtils;
import Utilities.OSUtils;
import Utilities.PropertyUtils;
import io.restassured.path.json.JsonPath;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * <b>RestApiHelper</B> utility class provides various helper methods to access RingGo API end-points. 
 * <p>
 * This helps tests within the framework to perform actions such as creating users, booking sessions,
 * obtaining user/vehicle/session details, adding vehicles/payment cards/addresses, change security details, etc
 * <p>
 * <b>NOTE:</B> Currently there are several variations of createUser and associated JSON data load methods. This is due to 
 * supporting older tests whilst changing over to a new approach. You should be using the following form:
 * <p>
 *  <code>Map&lt;Object,Object&gt; newUserJson = createNewUser(getNewUserData(getSuiteStartDate(),"defaultNewUser.json"));</code>
 *  <code>Map&lt;Object,Object&gt; newUserJson = createNewUser(getNewUserData(getSuiteStartDate(),"islingtonNewUser.json"));</code>
 * <p>  
 *  This will return the original JSON structure, with the additional details added (token, userdetails, etc)
 * 
 */
public final class RestApiHelper {

    static {
        try {
            properties = PropertyUtils.Load("Environment.properties");
        } catch (IOException e) {
            Log.logError(e.getMessage());
        }
        // Set RestAssured timeout in separate try/catch block to catch all Exceptions
        try
        {
        	Rest.SetTimeout(60);
        } catch (Exception ex) {
            Log.logError(ex.getMessage());
        }
    }

    private static Properties properties;
    private static String client_id = properties.getProperty("CLIENT_ID");
    private static String client_secret = properties.getProperty("CLIENT_SECRET");
    private static String client_Url = properties.getProperty("OAUTH2_CLIENT_URL");
    private static String baseUrl = properties.getProperty("BASE_URL");
    private static String password_Url = properties.getProperty("OAUTH2_PASSWORD_URL");


    private RestApiHelper() {
    }

    /**
     * Obtain user data JSON from given filename and update any embedded dynamic data tags
     * 
     * @param baseDate - the reference date to be used (typically suite start date/time)
     * @param filename - the JSON filename ie defaultNewUser.json
     * @return Map&lt;Object,Object&gt; - updated JSON
     */
    public static Map<Object, Object> getNewUserData(Date baseDate, String filename)
    { 
    	//String timeStamp;
		// Obtain the path to use if system property is set (enable build environment switching)
		String testDataPath = OSUtils.getSystemValueAsPath(OSUtils.ValueType.TESTDATAPATH);

		//load the supplied json file into a JsonPath object
    	JsonPath jFile = new JsonPath(new File(testDataPath + "testdata/" + filename));
    	
    	//now get the hash map
    	Map<Object,Object> data = jFile.get("NewUser");
    	
    	// Replace the dynamic data (this includes Mobile, Email & VRN)
    	data  = DynamicDataUtils.updateDynamicValues(baseDate,data);
    	
    	return data;
    }

    /**
     * Creates a new user via the API from the given JSON and returns the updated JSON (token, etc)
     * @param json - user JSON data structure to create new user
     * @return Map&lt;Object,Object&gt; - user JSON data structure updated with token, updated details, etc
     * @throws Exception
     */
	public static Map<Object, Object> createNewUser(Map<Object, Object> json) throws Exception
    {
    	Map<Object, Object> user = null;

    	// If null is passed in then throw an error
    	if (json==null)
			throw new Exception("Failed to create user: null user details");
    	
    	// Get new token
    	IToken iToken = new ActiveClientToken(
                oAuth2.getClientToken(client_id, client_secret, client_Url),
                client_id, client_secret, client_Url);

    	// Create a new user
    	user = RS_User.CreateNewUser(baseUrl, json, iToken);
    	
		if(user != null)
		{
			// Set user security question
			changeUsersecurityQuestion(user.getOrDefault("Mobile","").toString(),user.getOrDefault("Password","").toString(),userSecurityQuestion(user.getOrDefault("UserID","").toString()) );
			return user;
		}
		else
			throw new Exception("Failed to create user: " + json.getOrDefault("Email", "unknown-user-data").toString());    	
    }

    
    public static Map<Object, Object> userSecurityQuestion(String userid) {
    	
		// Obtain the path to use if system property is set (enable build environment switching)
		String testDataPath = OSUtils.getSystemValueAsPath(OSUtils.ValueType.TESTDATAPATH);

    	JsonPath jFile = new JsonPath(new File(testDataPath + "testdata/defaultUserSQ.json"));
    	HashMap<Object,Object> data = jFile.get("Input") ;
    	data.put("UserId", userid);
    	return data;
    	
    }

    
    /**
     * @deprecated only for backward support
     * @param baseDate - date to be used as suite constant date (dynamic data)
     * @return Map&lt;Object,Object&gt; user data from islingtonNewUser.json
     */
    @Deprecated
	public static Map<Object, Object> defaultNewUserData()
    {
    	return getNewUserData(new Date(),"defaultNewUser.json");
    }
    
    /**
     * @deprecated only for backward support
     * @param baseDate - date to be used as suite constant date (dynamic data)
     * @return Map&lt;Object,Object&gt; user data from islingtonNewUser.json
     */
    @Deprecated
	public static Map<Object, Object> defaultNewUserData(Date baseDate)
    {
    	return getNewUserData(baseDate,"defaultNewUser.json");
    }

    /**
     * @deprecated only for backward support
     * @param baseDate - date to be used as suite constant date (dynamic data)
     * @return Map&lt;Object,Object&gt; user data from islingtonNewUser.json
     */
    @Deprecated
	public static Map<Object, Object> islingtonNewUserData(Date baseDate)
    {
    	return getNewUserData(baseDate,"islingtonNewUser.json");
    }

    /**
     * @deprecated only for backward support
     * @param json - JSON data to create new user
     * @return NewUser - user data from newly created user
     * @throws Exception
     */
    @Deprecated
	public static NewUser createUser(Map<Object,Object> json) throws Exception
    {   
    	NewUser user = null;
    	IToken iToken = new ActiveClientToken(
                oAuth2.getClientToken(client_id, client_secret, client_Url),
                client_id, client_secret, client_Url);
    	
    	int count = 0;
    	int maxTries = 2;
    	
    	/*  
    	 * 	Try - catch - Retry mechanism to call 'createNewUser()' 
    	 *  when function returns empty object 	
    	 */
    	
    	while(count<=maxTries && user == null) {
    	    try {
    	    	   user = RS_User.CreateNewUser(iToken, baseUrl, json);
    	    	   
    	    	   if(user!=null && (user.UserDetails==null || user.UserDetails.Mobile == null))
    	    	   {
    	    		   throw new Exception("Empty user details");
    	    	   }
    	    	       	    	     	    	 
    	    } catch (Exception e) {
    	    	user=null;
    	    	if (count > maxTries) throw e;
    	    }
	    	++count;
	    	if (user==null) delay(2000);
    	}
    	
    	/*
    	 *  Returns user object, if NULL throws new exception
    	 */
    	
		if(user != null) 
		{
			// Set default user security question
			changeUsersecurityQuestion(user.UserDetails.Mobile,user.Password,userSecurityQuestion(user.UserDetails.UserID));
			return user;
		}
		else
			throw new Exception("Failed to create user " + json.getOrDefault("Email", "unknown-user-data").toString());
		
    }
    /**
     * Creates a new user using the islington user data
     * @deprecated only for backward support
     * @return Map<Object,Object> - structure used to create user
     * @throws Exception
     */
    @Deprecated
	public static Map<Object, Object> createUser() throws Exception {
        
        Map<Object, Object> userCreatingData = null;

        synchronized(RestApiHelper.class) {
        	userCreatingData = islingtonNewUserData(new Date());
        }

        return createNewUser(userCreatingData);
    }
 
    public static Map<Object,Object> bookSession(String createdEmailOrMobile, String createdUserPassword, String paymentMethodId, String vehicleId, String zoneId, String hours, String minutes) {
        
    	
    	IToken passwordToken = passwordToken(createdEmailOrMobile, createdUserPassword);
        
        RS_Session service = new RS_Session(passwordToken, baseUrl);
        
        Map<Object,Object> bookingRequest = getBookingRequest(paymentMethodId, vehicleId, zoneId, hours, minutes);
        
        return service.postStartSession(bookingRequest);
    }
    
    public static Map<Object,Object> getBookingRequest(String paymentMethodId, String vehicleId, String zoneId, String hours, String minutes)
    {
    	HashMap<Object,Object> bookingRequest = new HashMap<Object,Object>();
        bookingRequest.put("PaymentMethod", 1);
        bookingRequest.put("PaymentMethodId", paymentMethodId);
        bookingRequest.put("AppVersion", "5.0.129");
        bookingRequest.put("CountryCode", "GB");
        bookingRequest.put("PhoneMake", "Apple");
        bookingRequest.put("PhoneModel", "iPhone");
        bookingRequest.put("PhoneOS", "10.2");
        bookingRequest.put("VehicleId", vehicleId);
        bookingRequest.put("ZoneId", zoneId);
        
        ArrayList<HashMap<Object,Object>> duration = new ArrayList<HashMap<Object,Object>>();
        HashMap<Object,Object> dHours = new HashMap<Object,Object>();
        dHours.put("Unit", "Hour");
        dHours.put("Value", Integer.parseInt(hours));
        duration.add(dHours);
        
        HashMap<Object,Object> dMins = new HashMap<Object,Object>();
        dMins.put("Unit", "Minutes");
        dMins.put("Value", Integer.parseInt(minutes));
        duration.add(dMins);
        
        bookingRequest.put("Duration", duration);
        
        return bookingRequest;
    }

    public static Map<Object, Object> getUserPaymentMethods(String createdEmailOrMobile, String createdUserPassword, String zoneId) {
        IToken passwordToken = passwordToken(createdEmailOrMobile, createdUserPassword);

        RS_PaymentMethods methods = new RS_PaymentMethods(passwordToken, baseUrl);

        List<Map<Object, Object>> paymentMethodList = methods.GetPaymentMethods(zoneId);

        return paymentMethodList.get(0);
    }

    public static void setCarType(String createdEmailOrMobile, String createdUserPassword, String carType) {
        IToken passwordToken = passwordToken(createdEmailOrMobile, createdUserPassword);

        RS_Vehicles responseRetrievingVehicleService = new RS_Vehicles(passwordToken, baseUrl);
        Map<Object,Object> vehicles = responseRetrievingVehicleService.getVehicles();
        Map<Object,Object> vehicle1 = getVehicle(vehicles);
        vehicle1.put("Type", carType);

        responseRetrievingVehicleService.putVehicle(vehicle1, vehicle1.get("VehicleID").toString());

        //return vehicles;
    }
    
    public static String getVehicleId(Map<Object,Object> vehicles)
    {
    	Map<Object,Object> vehicle = getVehicle(vehicles);
    	return vehicle.get("VehicleID").toString();
    }
    
    public static Map<Object,Object> getVehicle(Map<Object,Object> vehicles)
    {
        return getVehicle(0, vehicles);
    }
    
    public static Map<Object,Object> getVehicle(int vehicleIndex, Map<Object,Object> vehicles)
    {
    	List<Map<Object,Object>> vehicleList = MapUtils.GetArray(vehicles, "Vehicles");
        return vehicleList.get(vehicleIndex);
    }

    public static Map<Object,Object> getInfoAboutVehicle(String createdEmailOrMobile, String createdUserPassword) {
        IToken passwordToken = passwordToken(createdEmailOrMobile, createdUserPassword);

        RS_Vehicles responseRetrievingVehicleService = new RS_Vehicles(passwordToken, baseUrl);
        return responseRetrievingVehicleService.getVehicles();
    }

    public static Map<Object,Object> defaultNewVehicle()
    {
    	HashMap<Object,Object> newVehicle = new HashMap<Object,Object>();
        newVehicle.put("Type", "Car");
        newVehicle.put("Colour", "White");
        newVehicle.put("Make", "BMW");
        return newVehicle;
    }
    
    public static Map<Object,Object> buildAddress(Map<Object,Object> Data)
    {
    	HashMap<Object,Object> a = new HashMap<Object,Object>();
    	a.put("AddressLine1", Data.get("addressLine").toString());
    	a.put("County", Data.get("county").toString());
    	a.put("Town", Data.get("town").toString());
        a.put("PostCode", Data.get("postcode").toString());
        a.put("Type", Data.get("addressType").toString());
        return a;
    }
    
    public static IToken passwordToken(String createdEmailOrMobile, String createdUserPassword) {
        return new ActivePasswordToken(
                oAuth2.getPasswordToken(client_id, client_secret, createdUserPassword, createdEmailOrMobile, password_Url),
                client_id, client_secret, createdUserPassword, createdEmailOrMobile, password_Url);
    }

    public static String addNewVehicleNoVRM(String createdEmailOrMobile, String createdUserPassword, Map<Object,Object> newVehicle) {
    	
    	String vrm = randomAlphabetic(6);
    	newVehicle.put("VRM",vrm);
        addNewVehicle(createdEmailOrMobile, createdUserPassword, newVehicle);
        return vrm;
    }

    public static void addNewVehicle(String createdEmailOrMobile, String createdUserPassword, Map<Object,Object> newVehicle) {
        IToken passwordToken = passwordToken(createdEmailOrMobile, createdUserPassword);
        RS_Vehicles srv_Vehicles = new RS_Vehicles(passwordToken, baseUrl);

        newVehicle.put("CountryCode","GB");

        srv_Vehicles.postVehicle(newVehicle);
    }

    public static String addNewPaymentCard(String login, String password, String cardNumber, String cardExpiry) {
        IToken passwordToken = passwordToken(login, password);
        RS_PaymentMethods rs_paymentMethods = new RS_PaymentMethods(passwordToken, baseUrl);
        return rs_paymentMethods.PostPaymentCard(cardNumber, cardExpiry);
    }

    public static void addNewAddress(String login, String password, Map<Object,Object> address) {
        IToken passwordToken = passwordToken(login, password);
        RS_Address rs_address = new RS_Address(passwordToken, baseUrl);
        rs_address.postAddress(address);
    }
    
    public static void changeUsersecurityQuestion(String login, String password, Map<Object,Object> newSQ) {
    	 IToken passwordToken = passwordToken(login, password);
    	 RS_SecurityQuestions rs_security = new RS_SecurityQuestions(passwordToken, baseUrl);
    	 rs_security.PutUserSecurityQuestions(newSQ);
    }
    
    public static Map<Object,Object> defaultNewMotorCycleVehicle()
    {
    	HashMap<Object,Object> newVehicle = new HashMap<Object,Object>();
        newVehicle.put("Type", "Motorcycle");
        newVehicle.put("Colour", "White");
        newVehicle.put("Make", "BMW");
        return newVehicle;
    }
}
