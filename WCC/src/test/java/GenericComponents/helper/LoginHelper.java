package GenericComponents.helper;

import static GenericComponents.UtilityClass.getFeatureToggleState;
import static SeleniumHelpers.WebElementMethods.ClickControl;
import static SeleniumHelpers.WebElementMethods.enterText;

import java.util.Date;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import PageObjects.RingGoMobileIndexPage;
import PageObjects.RingGoMobileLoginPage;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.Corporate.CorporateMenu;
import PageObjects.Corporate.WCCCorporateLoginPage;
import PageObjects.Insight.InsightLogin;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Islington.IslingtonLandingPage;
import PageObjects.Islington.IslingtonLoginPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.LoginRegisterPage;
import SeleniumHelpers.Logging;

public class LoginHelper {

	public static void loginInsight(String username, String Password, WebDriver _driver) {
		InsightLogin insightLogin = new InsightLogin(_driver);

		Logging.CheckResult(insightLogin.enterUsername(username), String.format("Enter %s in User name input", username), _driver);
		Logging.CheckResult(insightLogin.enterPassword(Password), String.format("Enter %s in Password input", Password), _driver);
		Logging.CheckResult(insightLogin.clickSubmitButton(), "Click submit button", _driver);
	}

	public static void loginMyRingo(String emailOrPhone, String password, WebDriver _driver) {
		HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
		homePage.clickOnAcceptButton();
		LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);
		Logging.CheckResult(ClickControl(homePage.getUpperRightNavigationMenu().getLoginLink(), "Login link"), "Click Login", _driver);
		Logging.CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickPersonalLink(), "Click Personal link", _driver);
		Logging.CheckResult(enterText(loginRegisterPage.getMobileOrEmailInput(), emailOrPhone, "Mobile/Email input"), "Input 'Mobile number or E-mail", _driver);
		Logging.CheckResult(enterText(loginRegisterPage.getPasswordOrPinInput(), password, "Password input"), "Input 'Password'", _driver);
		Logging.CheckResult(ClickControl(loginRegisterPage.getLogInButton(), "Login button"), "Click 'Log In'", _driver);
		// TODO - Below change will need to be introduced as part of RGOGM-1056 but merge process might not be clear
		//Logging.CheckBool(loginRegisterPage.getCurrentTitle().equals("RingGo Cashless Parking Solution: Dashboard"), "RingGo user 'home' page should be loaded", _driver);
	}

	public static void loginMyRingoCorporate(String emailOrPhone, String password, WebDriver _driver) {
		HomePageMyRinggo homePage = new HomePageMyRinggo(_driver);
		homePage.clickOnAcceptButton();
		LoginRegisterPage loginRegisterPage = new LoginRegisterPage(_driver);
		Logging.CheckResult(ClickControl(homePage.getUpperRightNavigationMenu().getLoginLink(), "Login link"), "Click Login", _driver);
		if (getFeatureToggleState().equalsIgnoreCase("off") || !_driver.getCurrentUrl().contains("corporate")) {
			Logging.CheckResult(homePage.getUpperRightNavigationMenu().getLoginToggleMenu().clickCorporateLink(), "Click Personal link", _driver);
		}
		Logging.CheckResult(enterText(loginRegisterPage.getEmailInput(), emailOrPhone, "Mobile/Email input"), "Input 'Mobile number or E-mail", _driver);
		Logging.CheckResult(enterText(loginRegisterPage.getPasswordOrPinInput(), password, "Password input"), "Input 'Password'", _driver);
		Logging.CheckResult(ClickControl(loginRegisterPage.getLogInButton(), "Login button"), "Click 'Log In'", _driver);
	}

	public static void logoutMyRingoCorporate(WebDriver _driver) {
		CorporateMenu corporatemenu = new CorporateMenu(_driver);
		Logging.CheckResult(corporatemenu.clickAccount(), "Click account menu", _driver);
		Logging.CheckResult(corporatemenu.clickLogout(), "Click logout link", _driver);
	}

	public static void loginMyRingGoMobile(String email, String password, WebDriver _driver) {
		RingGoMobileIndexPage ringgomobileindexpage = new RingGoMobileIndexPage(_driver);
		Logging.CheckResult(ringgomobileindexpage.clickLogin(), "Click on personal link", _driver);

		RingGoMobileLoginPage ringGoMobileLoginPage = new RingGoMobileLoginPage(_driver);
		Logging.CheckResult(ringGoMobileLoginPage.enterEmailOrPhoneNumber(email), "Enter email or phone number", _driver);
		Logging.CheckResult(ringGoMobileLoginPage.enterPassword(password), "Enter password", _driver);
		Logging.CheckResult(ringGoMobileLoginPage.clickLoginButton(), "Click login button", _driver);
	}
	
	public static void loginWestminster(String user, String password, WebDriver _driver) {
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        Logging.CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link", _driver);

        WCCLoginPage wccloginpage = new WCCLoginPage(_driver);
        Logging.CheckResult(wccloginpage.inputEmailOrPhonenumer(user), "Enter email or phone number", _driver);
        Logging.CheckResult(wccloginpage.enterPassword(password), "Enter password", _driver);
        Logging.CheckResult(wccloginpage.clickLoginButton(), "Click login button", _driver);
    }

    public static void loginCorporateWestminster(String user, String password, WebDriver _driver) {
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        Logging.CheckResult(wccindexpage.clickCorporateFromLogin(), "Click on corporate link", _driver);

        WCCCorporateLoginPage wccloginpage = new WCCCorporateLoginPage(_driver);
        Logging.CheckResult(wccloginpage.enterEmailAddress(user), "Enter email or phone number", _driver);
        Logging.CheckResult(wccloginpage.enterPassword(password), "Enter password", _driver);
        Logging.CheckResult(wccloginpage.clickLoginInButton(), "Click login button", _driver);
    }
    
    public static void loginIslington(Date baseDate, WebDriver _driver) {
		String email = "";
		String password = "";
    	try {
			Map<Object, Object> newUserJson = RestApiHelper.createNewUser(RestApiHelper.getNewUserData(baseDate,"defaultNewUser.json"));
			email = (String) newUserJson.get("Email");
			password = (String) newUserJson.get("Password");
		} catch (Exception e) {
			Logging.LogError("The following exception occured when calling createUser: " + e.getMessage(), _driver);
		}
		NavigationHelper.openIslington(_driver);
		IslingtonLandingPage islingtonlandingpage = new IslingtonLandingPage(_driver);
		Logging.CheckResult(islingtonlandingpage.ClickLogIn(), "Click on personal link", _driver);
		IslingtonLoginPage islingtonloginpage = new IslingtonLoginPage(_driver);
		islingtonloginpage.fillInLoginDetails(email, password);
	}
    
    public static void logoutInsight(WebDriver _driver) {
        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
    }
}
