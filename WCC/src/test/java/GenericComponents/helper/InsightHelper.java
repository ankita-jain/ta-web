package GenericComponents.helper;

import org.openqa.selenium.WebDriver;

import PageObjects.Insight.InsightWCCHome;
import SeleniumHelpers.Logging;

public class InsightHelper {

	public static String searchPermitAndEdit(String vrm, String permitStatus, WebDriver _driver) {
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        String permit_number;
        Logging.CheckResult(insightWCCHome.permitStatusChangeTo(permitStatus), String.format("Change Permit status to %s", permitStatus), _driver);
        Logging.CheckResult(insightWCCHome.enterVRM(vrm), String.format("Enter %s in VRM input", vrm), _driver);
        Logging.CheckResult(insightWCCHome.searchPermit(), "Click search permit", _driver);
        permit_number = insightWCCHome.getPermitNumber();
        Logging.CheckResult(insightWCCHome.clickEditPermit(), "Click edit permit", _driver);
        return permit_number;
    }

    /*
     * Some of the disable resident permit 'disable permit BLUE BADGE' doesn't have VRM(VRN),
     * so in those case we need CLI to find the permit from insight
     */
    public static String searchPermitByCLIAndEdit(String permitStatus, String cli, WebDriver _driver) {
        InsightWCCHome insightWCCHome = new InsightWCCHome(_driver);
        String permit_number;

        Logging.CheckResult(insightWCCHome.permitStatusChangeTo(permitStatus), String.format("Change permit status to %s", permitStatus), _driver);
        Logging.CheckResult(insightWCCHome.enterCLI(cli), String.format("Enter %s in CLI input", cli), _driver);
        Logging.CheckResult(insightWCCHome.searchPermit(), "Click search permit", _driver);
        permit_number = insightWCCHome.getPermitNumber();
        Logging.CheckResult(insightWCCHome.clickEditPermit(), "Click edit permit", _driver);

        return permit_number;
    }

}
