package GenericComponents.helper;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import SeleniumHelpers.Logging;
import Utilities.PropertyUtils;

public class NavigationHelper {
	public static final String ENV_PROPERTIES = "Environment.properties";

	public static void openInsight(WebDriver _driver) {
		try {
			_driver.get(PropertyUtils.ReadProperty("INSIGHT_URL", ENV_PROPERTIES));
		} catch (IOException e) {
			Logging.LogError(e.getMessage(), _driver);
		}
	}

	public static void openCallCentre(WebDriver _driver) {
		try {
			_driver.get(PropertyUtils.ReadProperty("CALL_CENTRE_URL", ENV_PROPERTIES));
		} catch (IOException e) {
			Logging.LogError(e.getMessage(), _driver);
		}
	}

	public static void openMyRingo(WebDriver _driver) {
		try {
			_driver.get(PropertyUtils.ReadProperty("MYRINGO_URL", ENV_PROPERTIES));
		} catch (IOException e) {
			Logging.LogError(e.getMessage(),_driver);
		}
	}

	public static void openRingoGoMobile(WebDriver _driver) {
		try {
			_driver.get(PropertyUtils.ReadProperty("MOBILE_SITE_URL", ENV_PROPERTIES));
		} catch (IOException e) {
			Logging.LogError(e.getMessage(), _driver);
		}
	}

	public static void openWestMinster(WebDriver _driver) {
		try {
			_driver.get(PropertyUtils.ReadProperty("URL", ENV_PROPERTIES));
		} catch (IOException e) {
			Logging.LogError(e.getMessage(), _driver);
		}
	}

	public static void openIslington(WebDriver _driver) {
		try {
			_driver.get(PropertyUtils.ReadProperty("ISLINGTON_URL", ENV_PROPERTIES));
		} catch (IOException e) {
			Logging.LogError(e.getMessage(), _driver);
		}
	}
}
