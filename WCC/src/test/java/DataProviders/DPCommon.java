package DataProviders;

import DataProviderHelpers.JSONDataProvider;

import java.io.IOException;
import java.lang.reflect.Method;

import Utilities.PropertyUtils;

public class DPCommon {
	
	public static Object[][] getJSONData(String pathPropertyName, Method caller) throws IOException
	{
		String path = PropertyUtils.ReadProperty(pathPropertyName, "WCCTest.properties") + caller.getDeclaringClass().getSimpleName() + "_" + caller.getName() + ".json";
		return JSONDataProvider.getJSONData(path);
	}

}
