package DataProviders;

import GenericComponents.ExcelReadWrite;
import GenericComponents.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DP_WCC {

    @DataProvider(name = "dp_wcc_resident_permit")
    public static Iterator<String[]> getData() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Script_name");

            if ((Executeflag.equals("YES")) && Script_name.equals("resident_permit")) {
                String[] arr_login = new String[16];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Car_insurance_schedule_proof");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Certificate_of_insurance");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_registration_document");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }


    @DataProvider(name = "dp_wcc_resident_permit_for_vrn_change")
    public static Iterator<String[]> getDataForVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Script_name");

            if ((Executeflag.equals("YES")) && Script_name.equals("resident_permit_for_vrn_change")) {
                String[] arr_login = new String[16];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Car_insurance_schedule_proof");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Certificate_of_insurance");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_registration_document");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_permit_address_change")
    public static Iterator<String[]> getResidentPermitAddressChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_ADDRESS_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_PERMIT_ADDRESS_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[11];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof_type1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Credit_card");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_month");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_year");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_permit_edit_zone")
    public static Iterator<String[]> getResidentPermitEditZone() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_EDIT_ZONE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_PERMIT_EDIT_ZONE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_EDIT_ZONE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[11];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Address_proof_type1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Address_proof1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Credit_card");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Exp_month");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "Exp_year");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_EDIT_ZONE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit")
    public static Iterator<String[]> getInsightData() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_AUTHORISE_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();
        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Script_name");

            if ((Executeflag.equals("YES")) && Script_name.equals("resident_permit")) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit_for_vrn_change_insight")
    public static Iterator<String[]> getInsightDataForVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_AUTHORISE_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Script_name");

            if ((Executeflag.equals("YES")) && Script_name.equals("resident_permit_for_vrn_change")) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit_auth_from_address_change")
    public static Iterator<String[]> getPermitAuthoriseFromAddressChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_AUTHORISE_FROM_ADDRESS_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit_auth_from_zone_change")
    public static Iterator<String[]> getPermitAuthoriseFromZoenChanged() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_AUTHORISE_FROM_ZONE_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_permit_temp_vrn_change")
    public static Iterator<String[]> getResidentPermitTempVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_TEMP_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[8];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Vehicle_to_edit");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "New_Vrn");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Credit_card");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Exp_month");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Exp_year");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_permit_view_details_after_temp_vrn_change")
    public static Iterator<String[]> getResidentPermitiewDetailsAfterTempVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_TEMP_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[4];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Vehicle_to_edit");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "New_Vrn");


                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_permit_perm_vrn_change")
    public static Iterator<String[]> getResidentPermitPermVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_PERM_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[10];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Permanent_VRN");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Ownership_type");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Proof_type");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Proofs");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Credit_card");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Exp_month");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Exp_year");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "CV2");


                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit_auth_from_vrn_changed")
    public static Iterator<String[]> getPermitAuthoriseFromVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_PERMIT_VRN_CHANGED"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_resident_permit_replacement_request")
    public static Iterator<String[]> getPermitReplacementRequest() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_PERMIT_REQUEST_REPLACEMENT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_PERMIT_REQUEST_REPLACEMENT"));

        int RowCount = xl.getrowcount(WCC_PERMIT_REQUEST_REPLACEMENT);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[4];
                arr_login[0] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Replacement_reason");
                arr_login[3] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Replacement_crime_ref");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit_auth_from_request_replacement")
    public static Iterator<String[]> getPermitAuthoriseFromRequestReplacement() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMIT_AUTH_FROM_REQUEST_REPLACEMENT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_resident_permit_cancel")
    public static Iterator<String[]> getPermitCancel() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_PERMIT_CANCEL_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_PERMIT_CANCEL"));

        int RowCount = xl.getrowcount(WCC_PERMIT_CANCEL_PERMIT);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[3];
                arr_login[0] = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Reason_for_cancel");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_wcc_temp_resident_permit")
    public static Iterator<String[]> getDataTempResidentPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMP_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[17];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_registered_country_UK");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_proof_type2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_proof2");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Credit_card");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_month");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_year");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_insight_temp_resident_permit_details")
    public static Iterator<String[]> getTemporaryPermitDetails() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_INSIGHT_TEMP_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[8];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");
                arr_login[7] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Response_to_customer");
                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_wcc_temp_resident_permit_upload_all_proofs")
    public static Iterator<String[]> getDataTempResdientPermitToPendingTempPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMPORARY_PERMIT_UPLOAD_FULL_PROOFS"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[8];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof_type");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_type_1");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_type_2");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_insight_resident_permit_auth_from_pending_temp_permit")
    public static Iterator<String[]> getPermitAuthoriseFromPendingTempPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_INSIGHT_AUTH_PERMIT_FROM_PENDING_TEMP_PERMIT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_insight_expire_resident_permit")
    public static Iterator<String[]> getPermitExpireFromInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_INSIGHT_EXPIRE_PERMIT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_wcc_renew_expired_permit")
    public static Iterator<String[]> getDataRenewExpiredPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_RENEW_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[8];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof_type");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Credit_card");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_month");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_year");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }


    @DataProvider(name = "dp_wcc_eco_resident_permit")
    public static Iterator<String[]> getEcoPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[18];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "proof_of_vehicle_1");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Car_insurance_schedule_proof");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "proof_of_vehicle_2");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Certificate_of_insurance");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "proof_of_vehicle_3");
                arr_login[17] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_registration_document");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_insight_auth_eco_permit")
    public static Iterator<String[]> getEcoPermitAuthFromInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PEMIT_AUTH_FROM_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_wcc_resident_eco_permit_address_change")
    public static Iterator<String[]> getResidentEcoPermitAddressChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_ADDRESS_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_EDIT_ADDRESS"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[11];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof_type1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Credit_card");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_month");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_year");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_insight_auth_eco_permit_from_address_change")
    public static Iterator<String[]> getEcoPermitAuthFromAddressChangeInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_AUTH_FROM_ADDRESS_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_wcc_resident_eco_permit_zone_change")
    public static Iterator<String[]> getResidentEcoPermitZoneChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_ADDRESS_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_ZONE_CHANGE_ON_PEMRIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[11];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof_type1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Credit_card");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_month");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_year");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data.iterator();

    }


    @DataProvider(name = "dp_insight_auth_eco_permit_from_zone_changed")
    public static Iterator<String[]> getEcoPermitAuthFromZoneChanged() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_AUTH_FROM_ZONE_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_wcc_resident_eco_permit_temp_vrn_change")
    public static Iterator<String[]> getResidentEcoPermitTempVRNChange(Method method) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_TEMP_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Execute");

            System.out.println("Name of the test is" + method.getName());

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[8];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Vehicle_to_edit");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "New_Vrn");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Credit_card");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Exp_month");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Exp_year");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data.iterator();

    }


    @DataProvider(name = "dp_wcc_resident_eco_permit_view_details_after_temp_vrn_change")
    public static Iterator<String[]> getResidentEcoPermitiewDetailsAfterTempVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_TEMP_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[4];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Vehicle_to_edit");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "New_Vrn");


                data.add(arr_login);

            }

        }

        return data.iterator();

    }


    @DataProvider(name = "dp_wcc_eco_resident_permit_perm_vrn_change")
    public static Iterator<String[]> getResidentEcoPermitPermVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_PERM_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[10];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Permanent_VRN");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Ownership_type");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Proof_type");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Proofs");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Credit_card");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Exp_month");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Exp_year");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "CV2");


                data.add(arr_login);

            }

        }

        return data.iterator();

    }

    @DataProvider(name = "dp_insight_eco_resident_permit_auth_from_vrn_changed")
    public static Iterator<String[]> getEcoPermitAuthoriseFromVRNChange() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_AUTH_FROM_VRN_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_resident_eco_permit_replacement_request")
    public static Iterator<String[]> getEcoPermitReplacementRequest() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_PERMIT_REQUEST_REPLACEMENT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_REQUEST_REPLACEMENT"));

        int RowCount = xl.getrowcount(WCC_PERMIT_REQUEST_REPLACEMENT);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[4];
                arr_login[0] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Replacement_reason");
                arr_login[3] = xl.Readvalue(WCC_PERMIT_REQUEST_REPLACEMENT, xlRow, "Replacement_crime_ref");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_insight_eco_resident_permit_auth_from_replacement_req")
    public static Iterator<String[]> getEcoPermitAuthoriseFromReplacementRequest() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_AUTH_FROM_REQUEST_REPLACEMENT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_eco_resident_permit_cancel")
    public static Iterator<String[]> getEcoPermitCancel() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_PERMIT_CANCEL_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_CANCEL"));

        int RowCount = xl.getrowcount(WCC_PERMIT_CANCEL_PERMIT);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[3];
                arr_login[0] = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_PERMIT_CANCEL_PERMIT, xlRow, "Reason_for_cancel");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_insight_eco_resident_permit_cancel_from_insight")
    public static Iterator<String[]> getEcoPermitCancelFromInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_CANCEL_FROM_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[9];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");
                arr_login[7] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Cancellation_reason");
                arr_login[8] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Response_to_customer");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_wcc_temp_eco_resident_permit")
    public static Iterator<String[]> getDataTempResidentEcoPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMP_ECO_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[13];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_registered_country_UK");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_proof_type2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_proof2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_insight_eco_resident_permit_temp_permit_from_insight")
    public static Iterator<String[]> getEcoPermitChangetoTemporaryFromInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMP_ECO_PERMIT_FROM_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[10];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");
                arr_login[7] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Vehicle_country");
                arr_login[8] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Temporary_permit_type");
                arr_login[9] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Response_to_customer");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_wcc_temp_resident_eco_permit_upload_all_proofs")
    public static Iterator<String[]> getDataTempResdientEcoPermitToPendingTempPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMP_ECO_PERMIT_ALL_PROOFS"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[10];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof_type");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof_type_two");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof_two");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_type_1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_1");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_type_2");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "vehicle_proof_2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_insight_eco_resident_permit_auth_from_pending_temporary")
    public static Iterator<String[]> getEcoPermitAuthoriseFromPendingTemporary() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_AUTH_FROM_PEDNING_TEMPORARY"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


    @DataProvider(name = "dp_insight_eco_resident_permit_expired_from_insight")
    public static Iterator<String[]> getEcoPermitExpiredFromInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_PERMIT_EXPIRE_FROM_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }

    @DataProvider(name = "dp_wcc_renew_expired_eco_permit")
    public static Iterator<String[]> getDataRenewExpiredEcoPermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_RENEW_RESIDENT_ECO_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[11];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Resident_type");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "address_proof");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Credit_card");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_month");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_year");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit")
    public static Iterator<String[]> getDataMotorcyclePermit() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_MOTOR_CYCLE_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[21];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_type_1");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_1");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_type_2");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_2");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_type_3");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_3");
                arr_login[17] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Credit_card");
                arr_login[18] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_month");
                arr_login[19] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Exp_year");
                arr_login[20] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }


    @DataProvider(name = "dp_insight_motor_cycle_resident_permit_auth_from_pending")
    public static Iterator<String[]> getMotorcyclePermitAuthoriseFromPending() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_motor_cycle_permit_from_pending");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_motor_cycle_resident_permit_auth_from_address_change")
    public static Iterator<String[]> getMotorcyclePermitAuthoriseFromAddresschange() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_motor_cycle_permit_from_address_change");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_motor_cycle_resident_permit_auth_from_zone_change")
    public static Iterator<String[]> getMotorcyclePermitAuthoriseFromZonechange() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_motor_cycle_permit_from_zone_change");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_motor_cycle_resident_permit_auth_from_vrn_change")
    public static Iterator<String[]> getMotorcyclePermitAuthoriseFromVRNchange() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_motor_cycle_permit_from_vrn_change");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_motor_cycle_resident_permit_auth_from_pending_temp")
    public static Iterator<String[]> getMotorcyclePermitAuthoriseFromPendingTemp() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_motor_cycle_permit_from_pending_temp");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_expire_motor_cycle_resident_permit")
    public static Iterator<String[]> expireMotorcyclePermit() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("expire_motorcycle_permit");
        return Obj.iterator();

    }


    @DataProvider(name = "verify_resident_motor_cycle_permit_details")
    public static Iterator<String[]> verifyPermitDetails() throws IOException {
        List<String[]> Obj = verifyDetailsOfPermit("Checking_Resident_Motorcycle_permit");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_address_change")
    public static Iterator<String[]> changeAddressonMotorcyclePermit() throws IOException {
        List<String[]> Obj = changeAddressOnResidentPermit("change_address_on_motor_cycle_permit");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_zone_change")
    public static Iterator<String[]> changeZonesonMotorcyclePermit() throws IOException {
        List<String[]> Obj = changeAddressOnResidentPermit("change_zones_on_motor_cycle_permit");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_change_vrn_temporarily")
    public static Iterator<String[]> changeVRNonPermitTemporarily() throws IOException {
        List<String[]> Obj = changeVRNonPermitTemporarily("Motor_cycle_permit_temp_VRN_Change");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_change_vrn_permanently")
    public static Iterator<String[]> changeVRNonPermitPermanently() throws IOException {
        List<String[]> Obj = changeVRNonPermitPermanently("Motor_cycle_permit_perm_VRN_Change");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_cancel_permit")
    public static Iterator<String[]> cancelMotorcyclePermit() throws IOException {
        List<String[]> Obj = cancelPermit("Motor_cycle_permit_cancel");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_temp_permit")
    public static Iterator<String[]> applyTempPermit() throws IOException {
        List<String[]> Obj = applyTempPermit("motor_cycle_temp_permit");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_resident_motorcycle_temp_permit_details")
    public static Iterator<String[]> motorcycleTempPermitDetailsInsight() throws IOException {
        List<String[]> Obj = changeStatusFromInsightTempPermit("motor_cycle_temp_permit_details_insight");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_temp_permit_all_proofs")
    public static Iterator<String[]> uploadAllProofsTempPermit() throws IOException {
        List<String[]> Obj = uploadAllProofsForTempPermit("motor_cycle_temp_permit_upload_all_proofs");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_renew")
    public static Iterator<String[]> motorCyclePermitRenew() throws IOException {
        List<String[]> Obj = renewExpiredPermit("motor_cycle_temp_permit_renewal");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_resident_motorcycle_permit_awaiting_payment_details")
    public static Iterator<String[]> motorcyclePermitAwaitingPaymentDetailsInsight() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("checking_motor_cycle_permit_awaiting_payment");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_resident_eco_permit_awaiting_payment_details")
    public static Iterator<String[]> ecoPermitAwaitingPaymentDetailsInsight() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("checking_eco_permit_awaiting_payment");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_resident_permit_awaiting_payment_details")
    public static Iterator<String[]> residentPermitAwaitingPaymentDetailsInsight() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("checking_resident_permit_awaiting_payment");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_wcc_trade_permit_apply")
    public static Iterator<String[]> tradePermit() throws IOException {
        List<String[]> Obj = getTradePermitData("trade_permit_apply");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_trade_permit_details")
    public static Iterator<String[]> tradePermitDetailsInsight() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("trade_permit");
        return Obj.iterator();

    }


    public static List<String[]> changePermitStatusFromInsight(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_INSIGHT_PERMIT_STATUS_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data;
    }


    public static List<String[]> verifyDetailsOfPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHHET_FOR_VERIFYING_PERMIT_DETAILS"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);

        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {

            String Script_name = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Script_name");
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {

                String[] arr_login = new String[2];

                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");

                data.add(arr_login);

            }


        }


        return data;
    }


    public static List<String[]> changeAddressOnResidentPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_ADDRESS_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_CHANGING_ADDRESS_ON_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[11];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof_type1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Address_proof1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Credit_card");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_month");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "Exp_year");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_ADDRESS_CHANGE, xlRow, "CV2");

                insight_data.add(arr_login);

            }

        }

        return insight_data;
    }


    public static List<String[]> changeVRNonPermitTemporarily(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMPORARY_VRN_CHANGE_ON_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[8];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Vehicle_to_edit");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "New_Vrn");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Credit_card");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Exp_month");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "Exp_year");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_TEMP_VRN_CHANGE, xlRow, "CV2");

                insight_data.add(arr_login);

            }

        }

        return insight_data;
    }

    public static List<String[]> changeVRNonPermitPermanently(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PERMANENT_VRN_CHANGE_ON_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[10];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Permanent_VRN");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Ownership_type");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Proof_type");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Proofs");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Credit_card");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Exp_month");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "Exp_year");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_PERM_VRN_CHANGE, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data;
    }

    public static List<String[]> cancelPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_CANCEL_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[3];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Reason_for_cancel");


                data.add(arr_login);

            }

        }

        return data;
    }

    public static List<String[]> applyTempPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TEMP_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[17];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "vehicle_registered_country_UK");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Vehicle_proof_type2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Vehicle_proof2");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Credit_card");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Exp_month");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Exp_year");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data;
    }


    public static List<String[]> uploadAllProofsForTempPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ALL_PROOFS_TEMP_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[10];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "address_proof_type");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "address_proof");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "address_proof_type_two");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "address_proof_two");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "vehicle_proof_type_1");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "vehicle_proof_1");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "vehicle_proof_type_2");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "vehicle_proof_2");

                data.add(arr_login);

            }

        }

        return data;
    }


    public static List<String[]> changeStatusFromInsightTempPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_INSIGHT_PERMIT_STATUS_CHANGE_TEMP_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Script_name");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[8];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Change_Permit_Status_To");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Response_to_customer");

                data.add(arr_login);

            }

        }

        return data;
    }

    public static List<String[]> renewExpiredPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_RENEW_PERMIT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Script_name");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[8];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "address_proof_type");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "address_proof");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Credit_card");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Exp_month");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Exp_year");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data;
    }


    public static List<String[]> getTradePermitData(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_TRADE_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_TRADE_PERMIT"));

        int RowCount = xl.getrowcount(WCC_TRADE_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Script_name");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[10];

                arr_login[0] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "zone");
                arr_login[3] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "reason");
                arr_login[4] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "vehicle");
                arr_login[5] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "duration");
                arr_login[6] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Credit_card");
                arr_login[7] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Exp_month");
                arr_login[8] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "Exp_year");
                arr_login[9] = xl.Readvalue(WCC_TRADE_PERMIT, xlRow, "CV2");

                data.add(arr_login);

            }

        }

        return data;
    }

    @DataProvider(name = "dp_wcc_resident_motorcycle_permit_awaiting_payment")
    public static Iterator<String[]> getDataMotorcyclePermitAwaitingPayment() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_MC_AWAITING_PAYMENT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[17];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_type_1");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_1");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_type_2");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_2");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_type_3");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Proof_of_vehicle_3");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_wcc_eco_resident_permit_awaiting_payment")
    public static Iterator<String[]> getEcoPermitAwaitingPayment() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_ECO_AWAITING_PAYMENT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[18];
                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "proof_of_vehicle_1");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Car_insurance_schedule_proof");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "proof_of_vehicle_2");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Certificate_of_insurance");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "proof_of_vehicle_3");
                arr_login[17] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_registration_document");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_wcc_resident_permit_awaiting_payment")
    public static Iterator<String[]> getResidentPermitAwaitingPayment() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_RESIDENT_PERMIT_AWAITING_PAYMENT"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[15];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Parking_zone");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Residency_type");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Ownership");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type1");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof1");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof_type2");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Address_proof2");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Car_insurance_schedule_proof");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Certificate_of_insurance");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_PERMIT_APPLICATION, xlRow, "Vehicle_registration_document");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }


    @DataProvider(name = "dp_vrn_change_from_insight")
    public static Iterator<String[]> chnageVRNFromInsight() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_CHNAGING_VRN_FROM_INSIGHT"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[13];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "New_vrn");
                arr_login[7] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");
                arr_login[8] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Payment_for");
                arr_login[9] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Card_number");
                arr_login[10] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Exp_month");
                arr_login[11] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Exp_year");
                arr_login[12] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "CV2");

                insight_data.add(arr_login);

            }

        }

        return insight_data.iterator();

    }


}
