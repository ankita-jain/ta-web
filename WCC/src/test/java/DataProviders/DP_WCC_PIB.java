package DataProviders;

import GenericComponents.ExcelReadWrite;
import GenericComponents.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DP_WCC_PIB {

    @DataProvider(name = "dp_wcc_pib")
    public static Iterator<String[]> getData() throws IOException {

        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_PIB = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_PIB"));

        int RowCount = xl.getrowcount(WCC_PIB);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_PIB, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[31];

                arr_login[0] = xl.Readvalue(WCC_PIB, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_PIB, xlRow, "Password");

                arr_login[2] = xl.Readvalue(WCC_PIB, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_PIB, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_PIB, xlRow, "Phone_number");
                arr_login[5] = xl.Readvalue(WCC_PIB, xlRow, "Title");
                arr_login[6] = xl.Readvalue(WCC_PIB, xlRow, "FirstName");
                arr_login[7] = xl.Readvalue(WCC_PIB, xlRow, "Surname");
                arr_login[8] = xl.Readvalue(WCC_PIB, xlRow, "Email");
                arr_login[9] = xl.Readvalue(WCC_PIB, xlRow, "Contact_number");
                arr_login[10] = xl.Readvalue(WCC_PIB, xlRow, "PIB_used_for");
                arr_login[11] = xl.Readvalue(WCC_PIB, xlRow, "Job_title");
                arr_login[12] = xl.Readvalue(WCC_PIB, xlRow, "Employee_no");
                arr_login[13] = xl.Readvalue(WCC_PIB, xlRow, "Job_Department");
                arr_login[14] = xl.Readvalue(WCC_PIB, xlRow, "PIB_Purpose");
                arr_login[15] = xl.Readvalue(WCC_PIB, xlRow, "PIB_Service");
                arr_login[16] = xl.Readvalue(WCC_PIB, xlRow, "PIB_Average_Parked_time");
                arr_login[17] = xl.Readvalue(WCC_PIB, xlRow, "PIB_Parking_zone");

                arr_login[18] = xl.Readvalue(WCC_PIB, xlRow, "PIB_VRN");
                arr_login[19] = xl.Readvalue(WCC_PIB, xlRow, "VRN_Make");
                arr_login[20] = xl.Readvalue(WCC_PIB, xlRow, "VRN_Make");
                arr_login[21] = xl.Readvalue(WCC_PIB, xlRow, "Driver_name");
                arr_login[22] = xl.Readvalue(WCC_PIB, xlRow, "Payment_type");

                arr_login[23] = xl.Readvalue(WCC_PIB, xlRow, "Authroised_signatory");
                arr_login[24] = xl.Readvalue(WCC_PIB, xlRow, "Department");
                arr_login[25] = xl.Readvalue(WCC_PIB, xlRow, "Status");
                arr_login[26] = xl.Readvalue(WCC_PIB, xlRow, "Payment_for");
                arr_login[27] = xl.Readvalue(WCC_PIB, xlRow, "Card_number");
                arr_login[28] = xl.Readvalue(WCC_PIB, xlRow, "Exp_month");
                arr_login[29] = xl.Readvalue(WCC_PIB, xlRow, "Exp_year");
                arr_login[30] = xl.Readvalue(WCC_PIB, xlRow, "CV2");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }


}
