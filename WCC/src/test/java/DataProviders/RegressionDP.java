package DataProviders;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;

public class RegressionDP {

    @DataProvider(name = "regressionJsonData")
    public static Object[][] cashlessJsonData(Method caller) throws IOException {
    	return DPCommon.getJSONData("REGRESSION_JSON_DATA", caller);
    }
}
