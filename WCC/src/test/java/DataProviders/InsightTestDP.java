package DataProviders;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;


public class InsightTestDP {

    @DataProvider(name = "insightTestData")
    public static Object[][] cashlessJsonData(Method caller) throws IOException {
    	return DPCommon.getJSONData("INSIGHT_JSON_DATA", caller);
    }
}
