package DataProviders;

import GenericComponents.ExcelReadWrite;
import GenericComponents.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DP_WCC_DISABLE {

    @DataProvider(name = "dp_wcc_disable_permit")
    public static Iterator<String[]> getData() throws IOException {

        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_DISABLE_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_RESIDENT_DISABLE_PERMIT_BLUE_BADGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] arr_login = new String[29];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Password");

                arr_login[2] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Title");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Firstname");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Surname");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_line_1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Town");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "County");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Gender");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Telephone_number");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "National_insurance_number");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "dob_date");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "dob_month");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "dob_year");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "badge_type");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "disability_type");
                arr_login[17] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "mobility_assessment");

                arr_login[18] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "does_child_need_medical_equipment");
                arr_login[19] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "medical_equipment");
                arr_login[20] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "does_child_need_vehicle_near");
                arr_login[21] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "medical_condition");

                arr_login[22] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add_type_one");
                arr_login[23] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add1");
                arr_login[24] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add_type_two");
                arr_login[25] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add2");
                arr_login[26] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_eligibility");
                arr_login[27] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "other_proof_types");
                arr_login[28] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "other_proofs");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_disable_permit_blue_badge")
    public static Iterator<String[]> disablePermitDetailsInsightBlueBadge() throws IOException {

        List<String[]> Obj = changeStatusFromInsightDisablePermit("auth_resident_disable_permit_from_pending");
        return Obj.iterator();

    }

    @DataProvider(name = "verify_resident_disable_permit_details")
    public static Iterator<String[]> verifyPermitDetails() throws IOException {
        List<String[]> Obj = verifyDetailsOfPermit("Checking_Resident_Disable_permit");
        return Obj.iterator();

    }

    @DataProvider(name = "verify_resident_disable_permit_white_badge_details")
    public static Iterator<String[]> verifyPermitDetailsWhiteBadge() throws IOException {
        List<String[]> Obj = verifyDetailsOfPermit("Checking_Resident_Disable_permit_white_badge");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_disable_resident_permit_white_blue_details")
    public static Iterator<String[]> disablePermitDetailsInsight() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_disbale_resident_permit_white_blue_badge");
        return Obj.iterator();

    }

    @DataProvider(name = "dp_insight_wcc_disable_resident_permit_white_badge")
    public static Iterator<String[]> authDisablePermitFromInsightBlueBadge() throws IOException {
        List<String[]> Obj = changePermitStatusFromInsight("auth_disbale_resident_permit_white_badge");
        return Obj.iterator();

    }


    public static List<String[]> changePermitStatusFromInsight(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_INSIGHT_PERMIT_STATUS_CHANGE"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);
        //Create a List
        List<String[]> insight_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Script_name");
            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[7];
                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "VRM");
                arr_login[5] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Permit_Status");
                arr_login[6] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Change_Permit_Status_To");

                insight_data.add(arr_login);

            }

        }

        return insight_data;
    }


    public static List<String[]> changeStatusFromInsightDisablePermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_PERMIT = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_DISABLE_PERMIT_BLUE_BADGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_PERMIT);
        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Script_name");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[7];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Password");
                arr_login[2] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Operator_Status");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Operator");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Permit_Status");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Change_Permit_Status_To");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_PERMIT, xlRow, "Registered_phone");

                data.add(arr_login);

            }

        }

        return data;
    }

    public static List<String[]> verifyDetailsOfPermit(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_INSIGHT_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHHET_FOR_VERIFYING_PERMIT_DETAILS"));

        int RowCount = xl.getrowcount(WCC_INSIGHT_PERMIT_APPLICATION);

        //Create a List
        List<String[]> data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {

            String Script_name = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Script_name");
            String Executeflag = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Execute");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {

                String[] arr_login = new String[2];

                arr_login[0] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_INSIGHT_PERMIT_APPLICATION, xlRow, "Password");

                data.add(arr_login);

            }


        }


        return data;
    }

    public static List<String[]> disablePermitData(String scriptname) throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_RESIDENT_DISABLE_PERMIT_APPLICATION = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_WCC_RESIDENT_DISABLE_PERMIT_WHITE_BLUE_BADGE"));

        int RowCount = xl.getrowcount(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION);
        System.out.println("No of rows in the DP" + RowCount);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Execute");
            String Script_name = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Script_name");

            if ((scriptname.equals(Script_name)) && Executeflag.equals("YES")) {
                String[] arr_login = new String[38];

                arr_login[0] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Username");
                arr_login[1] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Password");

                arr_login[2] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Title");
                arr_login[3] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Firstname");
                arr_login[4] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Surname");
                arr_login[5] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_line_1");
                arr_login[6] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Town");
                arr_login[7] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "County");
                arr_login[8] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Postcode");
                arr_login[9] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Gender");
                arr_login[10] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Telephone_number");
                arr_login[11] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "National_insurance_number");
                arr_login[12] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "dob_date");
                arr_login[13] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "dob_month");
                arr_login[14] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "dob_year");
                arr_login[15] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "badge_type");
                arr_login[16] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "disability_type");
                arr_login[17] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "mobility_assessment");

                arr_login[18] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "VRN");
                arr_login[19] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "driver_or_passenger");
                arr_login[20] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "driver_name");
                arr_login[21] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_Line_1");
                arr_login[22] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_Line_2");
                arr_login[23] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_Line_3");
                arr_login[24] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_town");
                arr_login[25] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_county");
                arr_login[26] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "Address_postcode");

                arr_login[27] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "does_child_need_medical_equipment");
                arr_login[28] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "medical_equipment");
                arr_login[29] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "does_child_need_vehicle_near");
                arr_login[30] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "medical_condition");

                arr_login[31] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add_type_one");
                arr_login[32] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add1");
                arr_login[33] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add_type_two");
                arr_login[34] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_add2");
                arr_login[35] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "proof_of_eligibility");
                arr_login[36] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "other_proof_types");
                arr_login[37] = xl.Readvalue(WCC_RESIDENT_DISABLE_PERMIT_APPLICATION, xlRow, "other_proofs");

                wcc_data.add(arr_login);

            }

        }

        return wcc_data;
    }


    @DataProvider(name = "dp_wcc_disable_permit_white_blue_badge")
    public static Iterator<String[]> getDataDisableWhiteBlueBadge() throws IOException {

        List<String[]> Obj = disablePermitData("resident_disable_white_and_blue_badge");
        return Obj.iterator();
    }

    @DataProvider(name = "dp_wcc_disable_permit_white_badge")
    public static Iterator<String[]> getDataDisableWhiteBadge() throws IOException {

        List<String[]> Obj = disablePermitData("resident_disable_white_badge");
        return Obj.iterator();
    }


}
