package DataProviders;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;


public class WCCNonResidentDisablePermitDP {

    @DataProvider(name = "wcc_non_resident_disable_permit")
    public static Object[][] cashlessJsonData(Method caller) throws IOException {
    	return DPCommon.getJSONData("PERMITS_JSON_DATA", caller);
    }
}
