package DataProviders;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;


public class WCCResidentPermitCancelFromInsight {

    @DataProvider(name = "wcc_resident_permit_cancel_from_insight")
    public static Object[][] cashlessJsonData(Method caller) throws IOException {
    	return DPCommon.getJSONData("PERMITS_JSON_DATA", caller);
    }
}
