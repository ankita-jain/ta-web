package DataProviders;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;


public class PermitDP {
	   @DataProvider(name = "wcc_Permits_Portal")
	    public static Object[][] cashlessJsonData(Method caller) throws IOException {
		   return DPCommon.getJSONData("PERMITS_JSON_DATA", caller);
	    }
}
