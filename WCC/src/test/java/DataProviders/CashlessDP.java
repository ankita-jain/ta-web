package DataProviders;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;

public class CashlessDP {

    @DataProvider(name = "cashlessJsonData")
    public static Object[][] cashlessJsonData(Method caller) throws IOException {
    	return DPCommon.getJSONData("CASHLESS_JSON_DATA", caller);
    }
}
