package DataProviders;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;


public class CorporateDP {

    @DataProvider(name = "corporateGetJSONData")
    public static Object[][] corporateGetJSONData(Method caller) throws IOException {
    	return DPCommon.getJSONData("CORPORATE_JSON_DATA", caller);
    }
}