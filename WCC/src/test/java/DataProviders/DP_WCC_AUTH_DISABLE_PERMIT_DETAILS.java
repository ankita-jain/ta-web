package DataProviders;

import GenericComponents.ExcelReadWrite;
import GenericComponents.UtilityClass;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DP_WCC_AUTH_DISABLE_PERMIT_DETAILS {

    @DataProvider(name = "dp_wcc_auth_disable_resident_permit_details")
    public static Iterator<String[]> getData() throws IOException {
        UtilityClass utilityclass = new UtilityClass();
        ExcelReadWrite xl = new ExcelReadWrite(utilityclass.reading_properties("TEST_DATA_SOURCE"));
        HSSFSheet WCC_DISABLE_RESIDENT_PERMIT_DETAILS = xl.Setsheet(utilityclass.reading_properties("DATA_SHEET_FOR_AUTH_DISABLE_RESIDENT_PERMIT"));

        int RowCount = xl.getrowcount(WCC_DISABLE_RESIDENT_PERMIT_DETAILS);
        //Create a List
        List<String[]> wcc_data = new ArrayList<String[]>();

        for (int xlRow = 1; xlRow <= RowCount; xlRow++) {
            String Executeflag = xl.Readvalue(WCC_DISABLE_RESIDENT_PERMIT_DETAILS, xlRow, "Execute");

            if ((Executeflag.equals("YES"))) {
                String[] resident_disable_permit_details = new String[2];
                resident_disable_permit_details[0] = xl.Readvalue(WCC_DISABLE_RESIDENT_PERMIT_DETAILS, xlRow, "Username");
                resident_disable_permit_details[1] = xl.Readvalue(WCC_DISABLE_RESIDENT_PERMIT_DETAILS, xlRow, "Password");

                wcc_data.add(resident_disable_permit_details);

            }

        }

        return wcc_data.iterator();

    }

}
