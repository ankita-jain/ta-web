package TestData.Cleanup;

import DataProviderHelpers.DBHelper;
import GenericComponents.UtilityClass;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class ClearPermit {


    @Test
    public void clearPermit() {

        try {

            UtilityClass utilityclass = new UtilityClass();

            String SPsql = "EXECUTE [dbo].[clearPermits]   @memberCLI = ?";
            Connection conn = DBHelper.connectToMSSQLServer(utilityclass.reading_properties("DB_ADDRESS"), utilityclass.reading_properties("DB_NAME"));
            PreparedStatement ps = conn.prepareStatement(SPsql);

            String[] testAccountArray = utilityclass.reading_properties("TEST_ACCOUNTS_CLIs").split("\\,", -1);

            for (String testCLL : testAccountArray) {
                ps.setString(1, testCLL);
                ps.execute();
            }


            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Fail to connect DB.......");
        }

    }

}
