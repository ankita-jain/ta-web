package Insight.CallCentre;

import static Utilities.TimerUtils.delay;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import PageObjects.CallCentre.CallCentreIndexpage;
import PageObjects.CallCentre.CallCentreLoginPage;
import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;

public class CallCentreExtendingSessionTest extends RingGoScenario {
	
	String vehicleVRN,fullVRN;
	
    String zoneId = "127952";
	String hours = "1";
	String minutes = "0";
	
	@BeforeMethod
	public void setUp() throws Exception {		   
		   
    	createUser("defaultNewUser.json");    
	    
	    vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());	
	    fullVRN = "WHITE BMW "+vehicleVRN.toUpperCase();	    
	    
	    Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
	    String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();
	    
	    Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
	    String paymentMethod = (String) usersPayment.get("Id");
	    
	    //Creates a user session
	    RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);	
	}
	   
	   @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	   public void extendSessionViaCallCentrePageTest(String TCID, Map<String, String> data) throws IOException {
		   
           StartTest(TCID, "Insight call centre form - extending a session");
		   
           LogStep("1", "On Insight (http://insight-preprod.ctt.co.uk/operator/helpdesk/) Log in and go to 'Support' > 'Callcentre Form'C");
		   NavigationHelper.openCallCentre(_driver);
		  		   
	       LogStep("2", "Log in to the callcentre form");
		   CallCentreLoginPage callcentreloginpage = new CallCentreLoginPage(_driver);
		   
		   CheckResult(callcentreloginpage.enterUsername(insightCallCentreLogin), "Entering callcentre username");
		   CheckResult(callcentreloginpage.enterPassword(insightPassword), "Entering callcentre Password");
		   CheckResult(callcentreloginpage.clickSubmitButton(), "Click Submit button");
		   
		   delay(180000);
		   

		   LogStep("3", "Enter the same telephone number as used for the previous test case (the account should have an active session)");
		   CallCentreIndexpage callcentreindexpage = new CallCentreIndexpage(_driver);
		   CheckResult(callcentreindexpage.enterCLI(mobile), "Entering mobile");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   

		   LogStep("4", "Select the session you just created and click 'Next'");
		   CheckResult(callcentreindexpage.clickExtendSessionOption(), "Click clickExtendSessionOption button");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		  

		   LogStep("5", "Tick the confirm box, select an hour in the duration drop down Click 'Next'");
		   CheckResult(callcentreindexpage.clickConfirmParkTAndCCheckBox(), "Click TAndC button");
		   CheckResult(callcentreindexpage.selectParkingDuration(data.get("duration")), "Select duration");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");		   

		   LogStep("6", "Select an existing payment card Click 'Next'");
		   CheckResult(callcentreindexpage.selectCard(data.get("card")), "Select Card");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");		   

		   LogStep("7", "Enter a 3 digit number click 'Next'");
		   CheckResult(callcentreindexpage.enterCVV(data.get("cvv")), "Entr CVV");
		   CheckResult(callcentreindexpage.clickConfirmPay(), "Click 'Confirm payment' button");
		   CheckResult(callcentreindexpage.clickConfirmPayment(), "Click Confirm payment button");
		   

		   LogStep("8", "Tick the box and click 'Take payment now'");
		   CheckResult(callcentreindexpage.clickTakePaymentNow(), "Click 'Take Payment Now' button");
		   CheckBool(callcentreindexpage.isParkingExtendSessionStarted(), "Chekcing isParking extend session started");
		   CheckResult(callcentreindexpage.clickFinishButton(), "Click 'Finish' button");	     
		   
	   }
	   
}
