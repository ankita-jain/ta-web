package Insight.CallCentre;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import DataProviders.CashlessDP;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.CallCentre.CallCentreIndexpage;
import PageObjects.CallCentre.CallCentreLoginPage;

public class CallCentreNewSessionTest extends RingGoScenario {
	
	String vehicleVRN,fullVRN;
	
	   @BeforeMethod
	    public void setUp() throws Exception {
		   
	    	createUser("defaultNewUser.json");     
	            	        
	        vehicleVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());	
	        fullVRN = "WHITE BMW "+vehicleVRN.toUpperCase();
	        
	    }
	   
	  @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	   public void createSession(String TCID, Map<String, String> data) throws IOException {
		   
		   StartTest(TCID, "Insight call centre form - creating a new session");
		   
		   LogStep("1", "On Insight (http://insight-preprod.ctt.co.uk/operator/helpdesk/) Log in and go to 'Support' > 'Callcentre Form'C");
		   NavigationHelper.openCallCentre(_driver);
		   
		   
	       LogStep("2", "Log in to the callcentre form");
		   CallCentreLoginPage callcentreloginpage = new CallCentreLoginPage(_driver);
		   CheckResult(callcentreloginpage.enterUsername(insightCallCentreLogin), "Entering callcentre username");
		   CheckResult(callcentreloginpage.enterPassword(insightPassword), "Entering callcentre Password");
		   CheckResult(callcentreloginpage.clickSubmitButton(), "Click Submit button");
		   
		   LogStep("3", "Enter a CLI and click Next button");
		   CallCentreIndexpage callcentreindexpage = new CallCentreIndexpage(_driver);
		   CheckResult(callcentreindexpage.enterCLI(mobile), "Entering mobile");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("4", "Select VRN and click Next button");
		   CheckResult(callcentreindexpage.selectVRN(fullVRN), "Enter VRM");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("5", "Enter Zone and click Next button");
		   CheckResult(callcentreindexpage.enterZone(data.get("zone")), "Enter zone");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("6", "Approve TandC, Parking duration and click Next button");
		   CheckResult(callcentreindexpage.clickConfirmParkTAndCCheckBox(), "Click TAndC button");
		   CheckResult(callcentreindexpage.selectParkingDuration(data.get("duration")), "Select duration");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("6", "Select card from the user account and click Next button");
		   CheckResult(callcentreindexpage.selectCard(data.get("card")), "Select Card");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("7", "Enter CVV and click Confirm payment button");
		   CheckResult(callcentreindexpage.enterCVV(data.get("cvv")), "Entr CVV");
		   CheckResult(callcentreindexpage.clickConfirmPay(), "Click 'Confirm payment' button");
		   
		   LogStep("8", "Click payment confirmation and click take paymentNow button");
		   CheckResult(callcentreindexpage.clickConfirmPayment(), "Click Confirm payment button");
		   CheckResult(callcentreindexpage.clickTakePaymentNow(), "Click 'Take Payment Now' button");
		   
		   LogStep("9", "Check the parking started message and click take FinishButton ");
		   CheckBool(callcentreindexpage.isParkingSessionStarted(), "Checking isParking session starts");
		   CheckResult(callcentreindexpage.clickFinishButton(), "Click 'Finish' button");	   
		   
	   }

	   @Test(dataProvider = "cashlessJsonData", dataProviderClass = CashlessDP.class)
	   public void createSessionNoCVVrequired(String TCID, Map<String, String> data) throws IOException {
		   
		   StartTest(TCID, "Insight call centre form - creating a new session (no CVV needed)");
		   
		   LogStep("1", "On Insight (http://insight-preprod.ctt.co.uk/operator/helpdesk/) Log in and go to 'Support' > 'Callcentre Form'C");
		   NavigationHelper.openCallCentre(_driver);
		   
	       LogStep("2", "Log in to the callcentre form");
		   CallCentreLoginPage callcentreloginpage = new CallCentreLoginPage(_driver);
		   CheckResult(callcentreloginpage.enterUsername(insightCallCentreLogin), "Entering callcentre username");
		   CheckResult(callcentreloginpage.enterPassword(insightPassword), "Entering callcentre Password");
		   CheckResult(callcentreloginpage.clickSubmitButton(), "Click Submit button");
		   
		   LogStep("3", "Enter a CLI and click Next button");
		   CallCentreIndexpage callcentreindexpage = new CallCentreIndexpage(_driver);
		   CheckResult(callcentreindexpage.enterCLI(mobile), "Entering mobile");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("4", "Select VRN and click Next button");
		   CheckResult(callcentreindexpage.selectVRN(fullVRN), "Enter VRM");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("5", "Enter Zone and click Next button");
		   CheckResult(callcentreindexpage.enterZone(data.get("zone")), "Enter zone");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("6", "Approve TandC, Parking duration and click Next button");
		   CheckResult(callcentreindexpage.clickConfirmParkTAndCCheckBox(), "Click TAndC button");
		   CheckResult(callcentreindexpage.selectParkingDuration(data.get("duration")), "Select duration");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("6", "Select card from the user account and click Next button");
		   CheckResult(callcentreindexpage.selectCard(data.get("card")), "Select Card");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("7", "Click payment confirmation and click take paymentNow button");
		   CheckResult(callcentreindexpage.clickConfirmPayment(), "Click Confirm payment button");
		   CheckResult(callcentreindexpage.clickTakePaymentNow(), "Click 'Take Payment Now' button");
		   
		   LogStep("8", "Check the parking started message and click take FinishButton ");
		   CheckBool(callcentreindexpage.isParkingSessionStarted(), "Checking isParking session starts");
		   CheckResult(callcentreindexpage.clickFinishButton(), "Click 'Finish' button");	   
		   
	   }

}
