package Insight.CallCentre;

import static Utilities.TimerUtils.delay;

import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import PageObjects.CallCentre.CallCentreIndexpage;
import PageObjects.CallCentre.CallCentreLoginPage;
import GenericComponents.RingGoScenario;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;

public class CallCentreAmendVehicleOnActiveSessionTest extends RingGoScenario {
	
	String secondVRN,fullVRN;
	
    String zoneId = "127952";
	String hours = "1";
	String minutes = "0";
	
	@BeforeMethod
	public void setUp() throws Exception {
		   
    	createUser("defaultNewUser.json");  
     
        RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
        //Create second VRN on the user account for changing VRN
        String secondVRN = RestApiHelper.addNewVehicleNoVRM(email, password, RestApiHelper.defaultNewVehicle());
        fullVRN = secondVRN.toUpperCase()+" WHITE BMW";	  
        
        Map<Object,Object> vehicles = RestApiHelper.getInfoAboutVehicle(email, password);
        String vehicleID = RestApiHelper.getVehicle(1,vehicles).get("VehicleID").toString();
        
        Map<Object, Object> usersPayment = RestApiHelper.getUserPaymentMethods(email, password, zoneId);
        String paymentMethod = (String) usersPayment.get("Id");
        
        //Creates a user session
        RestApiHelper.bookSession(email, password, paymentMethod, vehicleID, zoneId, hours, minutes);	
    }
	   
	   
	   @Test
	   public void amendVehicleOnSessionViaCallCentre() {
		   
		   StartTest("8025", "Insight call centre form - Amend a vehicle a session");
		   
		   LogStep("1", "On Insight (http://insight-preprod.ctt.co.uk/operator/helpdesk/) Log in and go to 'Support' > 'Callcentre Form'C");
		   NavigationHelper.openCallCentre(_driver);
		  		   
	       LogStep("2", "Log in to the callcentre form");
		   CallCentreLoginPage callcentreloginpage = new CallCentreLoginPage(_driver);
		   
		   CheckResult(callcentreloginpage.enterUsername(insightCallCentreLogin), "Entering callcentre username");
		   CheckResult(callcentreloginpage.enterPassword(insightPassword), "Entering callcentre Password");
		   CheckResult(callcentreloginpage.clickSubmitButton(), "Click Submit button");
		   
		   delay(60000);
		   

		   LogStep("3", "Enter the same telephone number as used for the previous test case (the account should have an active session)");
		   CallCentreIndexpage callcentreindexpage = new CallCentreIndexpage(_driver);
		   CheckResult(callcentreindexpage.enterCLI(mobile), "Entering mobile");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("4", "Select the session you just created and select Amend vehicle checkbox and click 'Next'");
		   CheckResult(callcentreindexpage.clickExtendSessionOption(), "Click clickExtendSessionOption button");
		   CheckResult(callcentreindexpage.clickAmendVehicleonSession(), "Click amend vehicle session button");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("5", "Select a new VRN from the dropdown and click 'Next'");
		   CheckResult(callcentreindexpage.selectVRNForAmendSession(fullVRN), "Select the second VRN on the account");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
		   LogStep("6", "Vehice change message on session and click 'Next'");
		   CheckBool(callcentreindexpage.isVRNOnSessionChangedMessage(), "Chekcing is Vehicle on the session changed");
		   CheckResult(callcentreindexpage.clickNextButton(), "Click Next button");
		   
	   }


}
