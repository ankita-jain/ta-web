package Insight.Authentication.WebUI;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;
import PageObjects.myringo.ContactDetailsPage;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.pagecomplexmodules.UpperRightNavigationMenu;

public class LoginAsManagerFromIsightToMyRinGo extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("defaultNewUser.json");
    }
    //TODO - remove hardcoded test data
    @Test
    public void loginAsManagerFromInsightToMyRinGo() {
        StartTest("68289", "Login as a Manager from Insight to MyRingGo");

        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);
        LogStep("Step - 2", "Enter login and password");

        LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);

        LogStep("Step - 3", "Select Telephone Number from Search dropdown and enter {random_cli} to Search Term");
        InsightSearch insightSearch = new InsightSearch(_driver);
        CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI");
        LogStep("Step - 4", "Click \"Search button\"");
        CheckResult(insightSearch.clickSubmit(), "Click 'Search' button");

        LogStep("Step - 5", "Click Login as Manager");
        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
        CheckResult(insightProfilePage.clickOnLoginAsUserButton(), "Click on 'Login As a User' button");

        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
         CheckResult(homePageMyRinggo.getMyDetailsBlock().clickOnEditDetails(),
                "Click on 'Edit details'");

        ContactDetailsPage detailsPage = new ContactDetailsPage(_driver);
        CheckValue(detailsPage.getMobileNumber(), mobile, "Verify if user is authorized");

        UpperRightNavigationMenu upperRightNavigationMenu = new UpperRightNavigationMenu(_driver);
        upperRightNavigationMenu.getAccountToggleMenu().logout();
    }
}