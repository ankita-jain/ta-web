package Insight.Authentication.WebUI;

import static Utilities.TimerUtils.delay;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCHome;
import PageObjects.WCCIndexPage;
import PageObjects.WCCLoginPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;
import PageObjects.myringo.pagecomplexmodules.UpperRightNavigationMenu;

public class UnlockAccountFromInsight extends RingGoScenario {

    private String invalidPassword = RandomStringUtils.randomAlphabetic(10);
    private final int stepsRepeatAmount = 3;

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("defaultNewUser.json");
        NavigationHelper.openWestMinster(_driver);
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");
        WCCLoginPage wccLoginPage = new WCCLoginPage(_driver);
        enterInvalidLoginSeveralTimes(wccLoginPage, invalidPassword, stepsRepeatAmount);
    }
    //TODO - remove hardcoded test data
    @Test
    public void lockAccountFromInsight() {
        StartTest("68291", "Lock account from Insight");

        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);
        LogStep("Step - 2", "Enter login and password");
        LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);

        LogStep("Step - 3", "Select Telephone Number from Search dropdown and enter {random_cli} to Search Term");
        InsightSearch insightSearch = new InsightSearch(_driver);
        CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI");
        LogStep("Step - 4", "Click \"Search button\"");
        CheckResult(insightSearch.clickSubmit(), "Click 'Search' button");

        LogStep("Step - 5", "Click Edit for Account Status field");
        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
        CheckResult(insightProfilePage.clickOnEditAccountStatusButton(), "Click on Edit Account Status");

        LogStep("Step - 6", "Select 'Unlocked' from Account Status dropdown");
        CheckResult(insightProfilePage.selectAccountStatus("Unlocked"), "Select 'Locked' from Account Status dropdown");

        LogStep("Step - 7", "Click Submit button");
        CheckResult(insightProfilePage.clickOnSubmitAccountStatusButton(), "Click Submit button");
        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
        delay(60000);

        LogStep("Step - 8", "Go to https://westminster.myrgo-preprod.ctt.co.uk");
        NavigationHelper.openWestMinster(_driver);

        LogStep("Step - 9", "Select Log in -> Personal");
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 10", "Enter {random_cli} and {random_password}. Click on LOG IN");
        LoginHelper.loginWestminster(mobile, password, _driver);
        WCCHome wccHome = new WCCHome(_driver);
        CheckContains(wccHome.getHomePageTitle(), "Welcome", "Header text");
        UpperRightNavigationMenu upperRightNavigationMenu = new UpperRightNavigationMenu(_driver);
        upperRightNavigationMenu.getAccountToggleMenu().logout();
    }

    private void enterInvalidLoginSeveralTimes(WCCLoginPage wccLoginPage, String invalidPassword, int attemptAmount) {
        for (int index = 0; index < attemptAmount; index++) {
            CheckResult(wccLoginPage.inputEmailOrPhonenumer(mobile), "Enter email or phone number");
            CheckResult(wccLoginPage.enterPassword(invalidPassword), "Enter password");
            CheckResult(wccLoginPage.clickLoginButton(), "Click login button");
        }
    }
}