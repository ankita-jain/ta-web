package Insight.Authentication.WebUI;

import static Utilities.TimerUtils.delay;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import GenericComponents.helper.RestApiHelper;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;
import Services.IToken;

public class InsightChangeUserPin extends RingGoScenario {

    private String newPin;

    @BeforeMethod
    public void setUp() throws Exception {

    	createUser("islingtonNewUser.json");
        newPin = randomNumeric(4);
    }
    //TODO - remove hardcoded test data
    @Test
    public void changeUsersPinViaInsight() {
        StartTest("37664", "Change user's PIN via Insight");

        LogStep("Step - 1", "Go to Insight");

        NavigationHelper.openInsight(_driver);
        LoginHelper.loginInsight(insightPermitUser, insightPassword, _driver);

        LogStep("Step - 2", "Enter {email} or {CLI} into \"Search term\"");
        InsightSearch insightSearch = new InsightSearch(_driver);
        CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI");
        LogStep("Step - 3", "Click \"Search button\"");
        CheckResult(insightSearch.clickSubmit(), "Click 'Search' button");

        LogStep("Step - 4", "In the \"Customer\" section look for PIN value and click pencil icon");
        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
        CheckResult(insightProfilePage.clickOnEditPinButton(), "Click on 'Edit' button");
        LogStep("Step - 5", "Enter new pin different from existing. Save new PIN value - {newpin}");
        CheckResult(insightProfilePage.enterNewPin(newPin), "Enter new pin");

        LogStep("Step - 6", "Click \"Submit\" button");
        CheckResult(insightProfilePage.clickOnSubmitPinButton(), "Click on 'Submit' button");

        LogStep("Step - 7", "Next to 'Password set?' click the delete icon next to 'yes'\n" +
                "(this will say 'no' if no password is not set)");
        CheckResult(insightProfilePage.clickDeletePasswordButton(), "Click icon next to 'Password set?'");
        LogStep("Step - 8", "Click 'yes'");
        CheckResult(insightProfilePage.clickOnSubmitDeletePassword(), "Click 'Yes' button");
        delay(6000);
        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
        LogStep("Step - 9", "Login user with CLI or email and changed PIN via Westminster API");
        IToken token = RestApiHelper.passwordToken(mobile, newPin);

        CheckBool(token.getAccessToken() != null, "Check if user login with new PIN");
    }
}
