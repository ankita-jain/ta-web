package Insight.Authentication.WebUI;

import static Utilities.TimerUtils.delay;
import static java.lang.String.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;
import PageObjects.myringo.HomePageMyRinggo;
import PageObjects.myringo.SecurityOptionsPage;
import PageObjects.myringo.SetPasswordPage;
import PageObjects.myringo.pagecomplexmodules.UpperRightNavigationMenu;

public class ChangeUserPinViaMyRingGo extends RingGoScenario {
    private String newPin;
    private String newPassword;

    @BeforeMethod
    public void setUp() throws Exception {

        createUser("islingtonNewUser.json");
        newPin = randomNumeric(4);
    }
    //TODO - remove hardcoded test data
    @Test
    public void changeUserPinViaMyRingGo() {
        StartTest("37657", "Change user's PIN via MyRingGo");

        LogStep("1", "Go to Insight");
        NavigationHelper.openInsight(_driver);

        LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);

        LogStep("2", "Enter {email} or {CLI} into \"Search term\"");
        InsightSearch insightSearch = new InsightSearch(_driver);
        insightSearch.enterSearchTerm(mobile);
        LogStep("3", "Click \"Search button\"");
        insightSearch.clickSubmit();

        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);

        LogStep("4", "In the \"Customer\" section look for PIN value and save it - {pin}");
        String currentUserPin = insightProfilePage.getMemberPIN();

        LogStep("5", "Go to MyRingGo");

        NavigationHelper.openMyRingo(_driver);
        LogStep("6", "Click \"Login\" in the right corner -> click \"Personal\"");
        LogStep("7", "Enter {email} or {CLI} in \"Phone or email\" input");
        LogStep("8", "Enter {password}");
        LoginHelper.loginMyRingo(mobile, password, _driver);

        UpperRightNavigationMenu upperRightNavigationMenu = new UpperRightNavigationMenu(_driver);
        LogStep("9", "Click \"Account\" in the right corner -> click \"Security Options\"");
        CheckResult(upperRightNavigationMenu.getAccountToggleMenu().clickAccount(), "Click 'Account'");
        CheckResult(upperRightNavigationMenu.getAccountToggleMenu().clickSecurityOptions(), "Click 'Security options'");

        SecurityOptionsPage securityOptionsPage = new SecurityOptionsPage(_driver);
        LogStep("10", "Enter {pin} into \"Old PIN\" input");
        CheckResult(securityOptionsPage.enterOldPin(currentUserPin), "Enter Old PIN");
        LogStep("11", "Enter new pin - {newpin} to the \"New PIN\" input");
        CheckResult(securityOptionsPage.enterNewPin(newPin), "Enter new PIN");
        LogStep("12", "Enter new pin - {newpin} to the \"Confirm New PIN\" input");
        CheckResult(securityOptionsPage.enterNewPinConfirm(newPin), "Enter Confirm PIN");
        LogStep("13", "Click \"Save\" button");
        CheckResult(securityOptionsPage.clicksaveButton(), "Click 'Save' button");

        LogStep("14", "Go to Insight");
        NavigationHelper.openInsight(_driver);

        LogStep("15", "Enter {email} or {CLI} into \"Search term\"");
        insightSearch.enterSearchTerm(mobile);
        insightSearch.clickSubmit();

        LogStep("16", "In the \"Customer\" section look for PIN value");
        CheckBool(newPin.equals(insightProfilePage.getMemberPIN()), "New generated PIN on MyRingGo is the same on Insight");

        LogStep("17", "Next to 'Password set?' click the delete icon next to 'yes'");
        CheckResult(insightProfilePage.clickDeletePasswordButton(), "Click icon next to 'Password set?'");
        LogStep("18", "Click 'yes'");
        CheckResult(insightProfilePage.clickOnSubmitDeletePassword(), "Click 'Yes' button");
        delay(3000);

        LogStep("19", "Go to MyRingGo");
        NavigationHelper.openMyRingo(_driver);
        CheckResult(upperRightNavigationMenu.getAccountToggleMenu().clickAccount(), "Click 'Account'");
        CheckResult(upperRightNavigationMenu.getAccountToggleMenu().clickOnLogoutLink(), "Click 'Logout'");

        LogStep("20", "Click \"Login\" in the right corner -> click \"Personal\"");
        LogStep("21", "Enter {email} or {CLI}l in \"Phone or email\" input");
        LogStep("22", "Enter {newpin} in \"Account PIN/Password\" input");
        LogStep("23", "Click \"Log in\" button");
        LoginHelper.loginMyRingo(mobile, newPin, _driver);

        LogStep("24", "Generate random value for password - {newpassword}");
        newPassword = randomAlphanumeric(9);

        SetPasswordPage setPasswordPage = new SetPasswordPage(_driver);
        LogStep("25", "Enter {newpassword} in \"Password\" input");
        CheckResult(setPasswordPage.enterNewPassword(newPassword), "Enter new password");
        LogStep("26", "Enter {newpassword} in \"Confirm Password\" input");
        CheckResult(setPasswordPage.enterConfirmNewPassword(newPassword), "Enter confirm new password");
        LogStep("27", "Click \"Save\"");
        CheckResult(setPasswordPage.clickNextButton(), "Click 'Next' button");

        HomePageMyRinggo homePageMyRinggo = new HomePageMyRinggo(_driver);
        CheckContains(homePageMyRinggo.getMyDetailsBlock().getMobileNumberText().getText(),
                format("Mobile: %s", mobile), "Check mobile number");
    }
}
