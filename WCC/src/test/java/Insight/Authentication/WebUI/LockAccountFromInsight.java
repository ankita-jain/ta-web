package Insight.Authentication.WebUI;

import static Utilities.TimerUtils.delay;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import GenericComponents.RingGoScenario;
import GenericComponents.helper.LoginHelper;
import GenericComponents.helper.NavigationHelper;
import PageObjects.WCCAccountLockedPage;
import PageObjects.WCCIndexPage;
import PageObjects.Insight.InsightLogoutPage;
import PageObjects.Insight.InsightNavMenu;
import PageObjects.Insight.InsightProfilePage;
import PageObjects.Insight.InsightSearch;

public class LockAccountFromInsight extends RingGoScenario {

    @BeforeMethod
    public void setUp() throws Exception {
        createUser("defaultNewUser.json");
    }
    //TODO - remove hardcoded test data
    @Test
    public void lockAccountFromInsight() {
        StartTest("68290", "Lock account from Insight");

        LogStep("Step - 1", "Go to Insight");
        NavigationHelper.openInsight(_driver);
        LogStep("Step - 2", "Enter login and password");

        LoginHelper.loginInsight(insightUserLogin, insightPassword, _driver);

        LogStep("Step - 3", "Select Telephone Number from Search dropdown and enter {random_cli} to Search Term");
        InsightSearch insightSearch = new InsightSearch(_driver);
        CheckResult(insightSearch.enterSearchTerm(mobile), "Enter CLI");
        LogStep("Step - 4", "Click \"Search button\"");
        CheckResult(insightSearch.clickSubmit(), "Click 'Search' button");

        LogStep("Step - 5", "Click Edit for Account Status field");
        InsightProfilePage insightProfilePage = new InsightProfilePage(_driver);
        CheckResult(insightProfilePage.clickOnEditAccountStatusButton(), "Click on Edit Account Status");

        LogStep("Step - 6", "Select 'Locked' from Account Status dropdown");
        CheckResult(insightProfilePage.selectAccountStatus("Locked"), "Select 'Locked' from Account Status dropdown");

        LogStep("Step - 7", "Click Submit button");
        CheckResult(insightProfilePage.clickOnSubmitAccountStatusButton(), "Click Submit button");
        InsightNavMenu insightnavmenu = new InsightNavMenu(_driver);
        insightnavmenu.clickAccount();
        InsightLogoutPage insightlogoutpage = new InsightLogoutPage(_driver);
        insightlogoutpage.clickLogout();
        delay(60000);

        LogStep("Step - 8", "Go to https://westminster.myrgo-preprod.ctt.co.uk");
        NavigationHelper.openWestMinster(_driver);

        LogStep("Step - 9", "Select Log in -> Personal");
        WCCIndexPage wccindexpage = new WCCIndexPage(_driver);
        CheckResult(wccindexpage.clickPersonalFromLogin(), "Click on personal link");

        LogStep("Step - 10", "Enter {random_cli} and {random_password}. Click on LOG IN");
        LoginHelper.loginWestminster(mobile, password, _driver);
        WCCAccountLockedPage accountLockedPage = new WCCAccountLockedPage(_driver);
        CheckContains(accountLockedPage.getAccountLockedHeader(), "Account Locked", "Header text");
    }
}