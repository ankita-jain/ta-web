# RingGo and Westminister web UI test suite

The RingGo Web UI test suites are based on Java with Maven, TestNG, Selenium, web drivers (Chrome, Firefox, 
IE, etc), POI (excel), Appium, RestAssured, JDBC (MSSQL, MySQL)

## Setting up the test suites

You will need the following installed on your machine to get started:

- Java 1.8 (latest) SDK
- Git (<https://git-scm.com/>)
- Eclipse for Java (latest version ie <https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2019-03/R/eclipse-inst-win64.exe>)
    - TestNG plugin - follow install instructions at <https://testng.org/doc/download.html>
- MS SQL Management Studio
- WinSCP / Putty

An eclipse workspace should be created and the following projects imported using the 
Eclipse File>Import wizard in the following order.

- TA_Base
- TA_Services
- TA-Web

You need to select the branch to run against the environment required (always pull the 
latest changes before running):

- master -> pre-prod
- develop -> staging

Once the test environment is built you may need to edit some of the configuration files.

- In RingGo.REST edit the POM.XML file to add any testrunid's then select the Maven profile (dev, staging, preprod).
- In WCC project edit the POM.XML file to add relevant settings then select the Maven profile (dev, staging, preprod):
    - Ensure the testRunId is correct (or set to NOTESTRAIL if not using a test run).

If you wish to change the environment.properties file (such as setting the below settings), 
you must edit the template version in the src/test/resources/<environment>/Properties 
folder.
    - You may also wish to add webDriverWait=60 if the system under test is performing 
      poorly
    - You may wish to enable headless test execution (browser-less)

You can now execute the tests as described in the following sections.

## Cashless

Provided the TestNG Eclipse add-in is correctly installed you can use the Eclipse UI 
to drive the tests, which will provide interactive view of the progress/results:

Right-click on the cashless.xml file and choose RunAs -> TestNG Suite

You can use Maven to start the tests, but is less intuitive.

Once the test has completed you can refresh the testreports folder and open the latest 
report in an external browser window.

## Corporate

Provided the TestNG Eclipse add-in is correctly installed you can use the Eclipse UI 
to drive the tests, which will provide interactive view of the progress/results:

Right-click on the Corporate.xml file and choose RunAs -> TestNG Suite

You can use Maven to start the tests, but is less intuitive.

Once the test has completed you can refresh the testreports folder and open the latest 
report in an external browser window.

## WCC Permits

Before running the permits suite you need to ensure the users do not have any existing 
permits (and if the test suite fails for any reason you will need to do this before 
re-running the tests). 

To do this you can run a stored procedure against the pre-prod (or staging) database to 
remove any existing permits. You can execute the following SQL statements:  

- exec clearPermits @memberCLI='07900224345'
- exec clearPermits @memberCLI='07900367288'
- exec clearPermits @memberCLI='07900298373'

At the moment the test suite is based around the old proof's subsystem (which is also 
the current live deployment). However, the CI build deploys staging/pre-prod with the 
new proof's sub-system. To switch back you must manually edit a config file as follows.

- Notify the relevant chat room that the proof's system is being switched for regression
- Login to staging/pre-prod front-end (ie 200.3 or 200.1)
- Edit the override file:

	> nano /etc/config/ringgo/override.ini

- Change the below line:

	> PROOF_SUBSYSTEM_OPERATOR_PERMIT_TYPES = ''

- Save the file

You can now work through each of the current (older) WCC Permits and Disabled test suites.

- Permits_WCC_Resident.xml
- Permits_WCC_ECO.xml
- Permits_WCC_Motorcycle.xml
- Permits_WCC_Trade.xml
- Permits.xml
- WCC_DISABLED_BLUE_BADGE.xml
- WCC_DISABLED_WHITE_BADGE.xml
- WCC_DISABLED_WHITE_BLUE_BADGE.xml

If the new permits workflow needs to be tested, then the override.ini setting needs 
to be reverted and the following test can be run:

- PermitProofs.xml

## Insight

Provided the TestNG Eclipse add-in is correctly installed you can use the Eclipse UI 
to drive the tests, which will provide interactive view of the progress/results:

Right-click on the insightRegressionTests.xml file and choose RunAs -> TestNG Suite

You can use Maven to start the tests, but is less intuitive.

Once the test has completed you can refresh the testreports folder and open the latest 
report in an external browser window.

